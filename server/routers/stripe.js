const { createPaymentIntent } = require("../controllers/stripe.actions");

const rootPath = 'stripe';

const Router = app => {
  app.post(`/${rootPath}/create-payment-intent`, createPaymentIntent);
}

module.exports = Router;