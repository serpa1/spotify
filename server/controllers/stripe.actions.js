const stripe = require("stripe")(process.env.STRIPE_SECRET_KEY);

const calculateOrderAmountAndItems = async (items, paymentValue, paymentCurrency, _paymentMethods) => {
  if(paymentValue && paymentCurrency) {
    return {
      amount: paymentValue,
      currency: paymentCurrency,
      payment_method_types: _paymentMethods
    }
  }
  const amountAndItems = {
    amount: 0,
    metadata: {},
    currency: 'mxn',
    payment_method_types: _paymentMethods
  }
  for (let index = 0; index < items.length; index++) {
    const element = items[index];
    if(element.id) {
      const product = await stripe.products.retrieve(element.id);
      if(product) {
        amountAndItems.items[product.name] = product.id
        const price = await stripe.prices.retrieve(product.default_price);
        if(price) {
          amountAndItems.amount += price.unit_amount
          amountAndItems.currency = price.currency
        }
      }
    }
  }
  return amountAndItems;
};

const createPaymentIntent = async (req, res) => {
  try {
    const { paymentMethods, items, paymentValue, paymentCurrency } = req.body;
    const _paymentMethods = paymentMethods ? paymentMethods : ['card']
    const paymentIntentContent = await calculateOrderAmountAndItems(items, paymentValue, paymentCurrency, _paymentMethods);
    const paymentIntent = await stripe.paymentIntents.create(paymentIntentContent);
    res.send({ clientSecret: paymentIntent.client_secret });
  } catch (error) {
    res.status(500).send(error.message);
  }
}

module.exports = {
  createPaymentIntent
}
