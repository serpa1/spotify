import React from 'react';
import ReactDOM from "react-dom/client";
import './index.css';
import App from './App';
import {
  Router,
  Switch,
  Route
} from "react-router-dom";
import reportWebVitals from './reportWebVitals';
import Auth0ProviderWithHistory from 'Components/Auth0Provider';
import { AppProvider } from "context/AppContext";
import { initialState, allStates} from 'reducer/appReducer';
import OverlaySwapProvider from 'Components/OverlaySwapProvider';
import { createBrowserHistory } from 'history';
import SingletoneNavigation from 'Class/SingletoneNavigation';
import * as Sentry from "@sentry/react";
import { BrowserTracing } from "@sentry/tracing";
const container = document.getElementById('root');
            
Sentry.init({
  integrations: [new BrowserTracing()],
  tracesSampleRate: 0.6,
});


// Create a root.
const root = ReactDOM.createRoot(container);
export const history = createBrowserHistory();
SingletoneNavigation.CreateInstance();
const AppMemo = React.memo(()=>{
  return (<App />)
})
const AppWithProvier = ({withUser}) => {
  
  return(
    <Router history={history}> 
      <AppProvider initialState={initialState} reducer={allStates} withUser={withUser}>
        <AppMemo />
      </AppProvider> 
    </Router> 
)};
root.render(
  <React.StrictMode>
    
    { ( process.env.REACT_APP_AUTH0_DOMAIN && process.env.REACT_APP_AUTH0_CLIENT_ID ) ? 
    <Auth0ProviderWithHistory> 
      <AppWithProvier withUser={true} />
    </Auth0ProviderWithHistory> : 
    <AppWithProvier withUser={false}  />
    }
  </React.StrictMode>
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();