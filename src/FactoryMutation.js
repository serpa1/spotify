import {
    commitMutation,    
  } from 'react-relay'
import Environment from './Environment';

const factoryMutation = (variables, mutation, callback) => {
 
    
    commitMutation(
        Environment,
        {
          mutation,
          variables,
          onCompleted: (data) => {
            callback(null, data)
          },
          onError: err => {callback(err, null) },
        },
      )
}

const MutationPromise = (variables, mutation)=>{
  return new Promise((resolve, reject)=>{
    factoryMutation(variables, mutation, (e, data)=>{
      if(e)
        reject(e);
      else
        resolve(data);
    })
  })  
}
export default MutationPromise
