import { Environment, Network, RecordSource, Store } from 'relay-runtime';


async function fetchGraphQL(query, variables) {
     // Fetch data from GitHub's GraphQL API:
      const response = await fetch(`${process.env.REACT_APP_GRAPHQL_URL}`, {
        method: 'POST',
        headers: {"Content-Type":"application/json","x-hasura-admin-secret": `${process.env.REACT_APP_GRAPHQL_SECRET}`},
        body: JSON.stringify({
          query,
          variables,
        }),
      });
   
  
    // Get the response as JSON
    return await response.json();
}

async function fetchRelay(params, variables) {  
  return fetchGraphQL(params.text, variables);
}

export default new Environment({
  network: Network.create(fetchRelay),
  store: new Store(new RecordSource()),
});