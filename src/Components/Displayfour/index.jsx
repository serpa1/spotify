import React, { Suspense } from 'react';
import PropTypes from 'prop-types';
import * as Contexts  from 'Contexts';
import { OverlaySwapContext } from 'Components/OverlaySwapProvider';
import CorebooksAnimation from 'Components/CorebooksAnimation';
import CorebooksAnimationSingle from 'Components/CorebooksAnimationSingle';

import * as UtilsScripts  from 'utils/utils';
import { Route, useHistory, useParams } from "react-router-dom";
import SingletoneNavigation from 'Class/SingletoneNavigation';

import LoadingComponent from "Components/Loading";


import { Transition } from "react-transition-group";
import { CSSTransition } from "react-transition-group";
import { useAppContext, useSessionContext } from 'context/AppContext';
import './Displayfour.css'





const Displayfour = (props)=>{
    const [state,dispatch] = useAppContext();
    const [, setNameSelectedComponent] = React.useContext(OverlaySwapContext);
    
    const nodeRef = React.useRef(null);
    const history = useHistory();
    let RouterContext = useParams();
    
    
    
    
    
    
    const [globalStateForm, dispatchForm] = React.useState({});
    
    const [ _in, setIn ] = React.useState(false)
    const [ transaction, setTransaction ] = React.useState({}) 
    const [ animation, setAnimation ] = React.useState('')

    
    
    
    
    
    
    React.useEffect(()=>{
        
        
        
    },[]);
    
    
    return (
        <>
  <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['displayfour']?.animationClass || {}}>

    <div id="id_sixtwoosix_oneseventhreefour" ref={nodeRef} className={` ${ props.onClick ? 'cursor' : '' } displayfour C_sixtwoosix_oneseventhreefour ${ props.cssClass } ${ transaction['displayfour']?.type ? transaction['displayfour']?.type.toLowerCase() : '' }`} style={ { ...{ ...{}, transitionDuration: transaction['displayfour']?.duration, transitionTimingFunction: transaction['displayfour']?.timingFunction }, ...props.style }} onClick={ props.DisplayfouronClick } onMouseEnter={ props.DisplayfouronMouseEnter } onMouseOver={ props.DisplayfouronMouseOver } onKeyPress={ props.DisplayfouronKeyPress } onDrag={ props.DisplayfouronDrag } onMouseLeave={ props.DisplayfouronMouseLeave } onMouseUp={ props.DisplayfouronMouseUp } onMouseDown={ props.DisplayfouronMouseDown } onKeyDown={ props.DisplayfouronKeyDown } onChange={ props.DisplayfouronChange } ondelay={ props.Displayfourondelay }>
      {
      props.children ?
      props.children :
      <>
        <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['displayfourtext']?.animationClass || {}}>

          <span id="id_sixtwoosix_oneseventhreethree"  className={` text displayfourtext    ${ props.onClick ? 'cursor' : ''}  ${ transaction['displayfourtext']?.type ? transaction['displayfourtext']?.type.toLowerCase() : '' }`} style={{ ...{},  ...props.DisplayfourTextStyle , transitionDuration: transaction['displayfourtext']?.duration, transitionTimingFunction: transaction['displayfourtext']?.timingFunction }} onClick={ props.DisplayfourTextonClick } onMouseEnter={ props.DisplayfourTextonMouseEnter } onMouseOver={ props.DisplayfourTextonMouseOver } onKeyPress={ props.DisplayfourTextonKeyPress } onDrag={ props.DisplayfourTextonDrag } onMouseLeave={ props.DisplayfourTextonMouseLeave } onMouseUp={ props.DisplayfourTextonMouseUp } onMouseDown={ props.DisplayfourTextonMouseDown } onKeyDown={ props.DisplayfourTextonKeyDown } onChange={ props.DisplayfourTextonChange } ondelay={ props.DisplayfourTextondelay } >{props.DisplayfourText0 || `Display 4`}</span>

        </CSSTransition>

      </>
      }
    </div>

  </CSSTransition>
</>
    ) 
}

Displayfour.propTypes = {
    style: PropTypes.any,
DisplayfourText0: PropTypes.any,
DisplayfouronClick: PropTypes.any,
DisplayfouronMouseEnter: PropTypes.any,
DisplayfouronMouseOver: PropTypes.any,
DisplayfouronKeyPress: PropTypes.any,
DisplayfouronDrag: PropTypes.any,
DisplayfouronMouseLeave: PropTypes.any,
DisplayfouronMouseUp: PropTypes.any,
DisplayfouronMouseDown: PropTypes.any,
DisplayfouronKeyDown: PropTypes.any,
DisplayfouronChange: PropTypes.any,
Displayfourondelay: PropTypes.any,
DisplayfourTextonClick: PropTypes.any,
DisplayfourTextonMouseEnter: PropTypes.any,
DisplayfourTextonMouseOver: PropTypes.any,
DisplayfourTextonKeyPress: PropTypes.any,
DisplayfourTextonDrag: PropTypes.any,
DisplayfourTextonMouseLeave: PropTypes.any,
DisplayfourTextonMouseUp: PropTypes.any,
DisplayfourTextonMouseDown: PropTypes.any,
DisplayfourTextonKeyDown: PropTypes.any,
DisplayfourTextonChange: PropTypes.any,
DisplayfourTextondelay: PropTypes.any
}
export default Displayfour;