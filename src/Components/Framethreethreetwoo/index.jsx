import React, { Suspense } from 'react';
import PropTypes from 'prop-types';
import * as Contexts  from 'Contexts';
import { OverlaySwapContext } from 'Components/OverlaySwapProvider';
import CorebooksAnimation from 'Components/CorebooksAnimation';
import CorebooksAnimationSingle from 'Components/CorebooksAnimationSingle';

import * as UtilsScripts  from 'utils/utils';
import { Route, useHistory, useParams } from "react-router-dom";
import SingletoneNavigation from 'Class/SingletoneNavigation';

import LoadingComponent from "Components/Loading";


import { Transition } from "react-transition-group";
import { CSSTransition } from "react-transition-group";
import { useAppContext, useSessionContext } from 'context/AppContext';
import './Framethreethreetwoo.css'





const Framethreethreetwoo = (props)=>{
    const [state,dispatch] = useAppContext();
    const [, setNameSelectedComponent] = React.useContext(OverlaySwapContext);
    
    const nodeRef = React.useRef(null);
    const history = useHistory();
    let RouterContext = useParams();
    
    
    
    
    
    
    const [globalStateForm, dispatchForm] = React.useState({});
    
    const [ _in, setIn ] = React.useState(false)
    const [ transaction, setTransaction ] = React.useState({}) 
    const [ animation, setAnimation ] = React.useState('')

    
    
    
    
    
    
    React.useEffect(()=>{
        
        
        
    },[]);
    
    
    return (
        <>
  <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['framethreethreetwoo']?.animationClass || {}}>

    <div id="id_fourthreefour_oneonefivetwoo" ref={nodeRef} className={` ${ props.onClick ? 'cursor' : '' } framethreethreetwoo C_fourthreefour_oneonefivetwoo ${ props.cssClass } ${ transaction['framethreethreetwoo']?.type ? transaction['framethreethreetwoo']?.type.toLowerCase() : '' }`} style={ { ...{ ...{}, transitionDuration: transaction['framethreethreetwoo']?.duration, transitionTimingFunction: transaction['framethreethreetwoo']?.timingFunction }, ...props.style }} onClick={ props.FramethreethreetwooonClick } onMouseEnter={ props.FramethreethreetwooonMouseEnter } onMouseOver={ props.FramethreethreetwooonMouseOver } onKeyPress={ props.FramethreethreetwooonKeyPress } onDrag={ props.FramethreethreetwooonDrag } onMouseLeave={ props.FramethreethreetwooonMouseLeave } onMouseUp={ props.FramethreethreetwooonMouseUp } onMouseDown={ props.FramethreethreetwooonMouseDown } onKeyDown={ props.FramethreethreetwooonKeyDown } onChange={ props.FramethreethreetwooonChange } ondelay={ props.Framethreethreetwooondelay }>
      {
      props.children ?
      props.children :
      <>
        <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['honetext']?.animationClass || {}}>

          <span id="id_fourthreefour_oneonefournigth"  className={` text honetext    ${ props.onClick ? 'cursor' : ''}  ${ transaction['honetext']?.type ? transaction['honetext']?.type.toLowerCase() : '' }`} style={{ ...{},  ...props.HoneTextStyle , transitionDuration: transaction['honetext']?.duration, transitionTimingFunction: transaction['honetext']?.timingFunction }} onClick={ props.HoneTextonClick } onMouseEnter={ props.HoneTextonMouseEnter } onMouseOver={ props.HoneTextonMouseOver } onKeyPress={ props.HoneTextonKeyPress } onDrag={ props.HoneTextonDrag } onMouseLeave={ props.HoneTextonMouseLeave } onMouseUp={ props.HoneTextonMouseUp } onMouseDown={ props.HoneTextonMouseDown } onKeyDown={ props.HoneTextonKeyDown } onChange={ props.HoneTextonChange } ondelay={ props.HoneTextondelay } >{props.HoneText0 || `¿Tienes preguntas? ¡Nuestra comunidad mundial de expertos te puede ayudar!`}</span>

        </CSSTransition>

      </>
      }
    </div>

  </CSSTransition>
</>
    ) 
}

Framethreethreetwoo.propTypes = {
    style: PropTypes.any,
HoneText0: PropTypes.any,
FramethreethreetwooonClick: PropTypes.any,
FramethreethreetwooonMouseEnter: PropTypes.any,
FramethreethreetwooonMouseOver: PropTypes.any,
FramethreethreetwooonKeyPress: PropTypes.any,
FramethreethreetwooonDrag: PropTypes.any,
FramethreethreetwooonMouseLeave: PropTypes.any,
FramethreethreetwooonMouseUp: PropTypes.any,
FramethreethreetwooonMouseDown: PropTypes.any,
FramethreethreetwooonKeyDown: PropTypes.any,
FramethreethreetwooonChange: PropTypes.any,
Framethreethreetwooondelay: PropTypes.any,
HoneTextonClick: PropTypes.any,
HoneTextonMouseEnter: PropTypes.any,
HoneTextonMouseOver: PropTypes.any,
HoneTextonKeyPress: PropTypes.any,
HoneTextonDrag: PropTypes.any,
HoneTextonMouseLeave: PropTypes.any,
HoneTextonMouseUp: PropTypes.any,
HoneTextonMouseDown: PropTypes.any,
HoneTextonKeyDown: PropTypes.any,
HoneTextonChange: PropTypes.any,
HoneTextondelay: PropTypes.any
}
export default Framethreethreetwoo;