import React, { Suspense } from 'react';
import PropTypes from 'prop-types';
import * as Contexts  from 'Contexts';
import { OverlaySwapContext } from 'Components/OverlaySwapProvider';
import CorebooksAnimation from 'Components/CorebooksAnimation';
import CorebooksAnimationSingle from 'Components/CorebooksAnimationSingle';

import * as UtilsScripts  from 'utils/utils';
import { Route, useHistory, useParams } from "react-router-dom";
import SingletoneNavigation from 'Class/SingletoneNavigation';

import LoadingComponent from "Components/Loading";


import { Transition } from "react-transition-group";
import { CSSTransition } from "react-transition-group";
import { useAppContext, useSessionContext } from 'context/AppContext';
import './Framefourzerofive.css'





const Framefourzerofive = (props)=>{
    const [state,dispatch] = useAppContext();
    const [, setNameSelectedComponent] = React.useContext(OverlaySwapContext);
    
    const nodeRef = React.useRef(null);
    const history = useHistory();
    let RouterContext = useParams();
    
    
    
    
    
    
    const [globalStateForm, dispatchForm] = React.useState({});
    
    const [ _in, setIn ] = React.useState(false)
    const [ transaction, setTransaction ] = React.useState({}) 
    const [ animation, setAnimation ] = React.useState('')

    
    
    
    
    
    
    React.useEffect(()=>{
        
        
        
    },[]);
    
    
    return (
        <>
  <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['framefourzerofive']?.animationClass || {}}>

    <div id="id_fivefivenigth_onenigthnigthone" ref={nodeRef} className={` ${ props.onClick ? 'cursor' : '' } framefourzerofive ${ props.cssClass } ${ transaction['framefourzerofive']?.type ? transaction['framefourzerofive']?.type.toLowerCase() : '' }`} style={ { ...{ ...{}, transitionDuration: transaction['framefourzerofive']?.duration, transitionTimingFunction: transaction['framefourzerofive']?.timingFunction }, ...props.style }} onClick={ props.FramefourzerofiveonClick } onMouseEnter={ props.FramefourzerofiveonMouseEnter } onMouseOver={ props.FramefourzerofiveonMouseOver } onKeyPress={ props.FramefourzerofiveonKeyPress } onDrag={ props.FramefourzerofiveonDrag } onMouseLeave={ props.FramefourzerofiveonMouseLeave } onMouseUp={ props.FramefourzerofiveonMouseUp } onMouseDown={ props.FramefourzerofiveonMouseDown } onKeyDown={ props.FramefourzerofiveonKeyDown } onChange={ props.FramefourzerofiveonChange } ondelay={ props.Framefourzerofiveondelay }>
      {
      props.children ?
      props.children :
      <>
        <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['group']?.animationClass || {}}>

          <div id="id_fivefivenigth_onenigthnigthtwoo" className={` group group ${ props.onClick ? 'cursor' : '' } ${ transaction['group']?.type ? transaction['group']?.type.toLowerCase() : '' }`} style={{ ...{},  ...props.GroupStyle , transitionDuration: transaction['group']?.duration, transitionTimingFunction: transaction['group']?.timingFunction }} onClick={ props.GrouponClick } onMouseEnter={ props.GrouponMouseEnter } onMouseOver={ props.GrouponMouseOver } onKeyPress={ props.GrouponKeyPress } onDrag={ props.GrouponDrag } onMouseLeave={ props.GrouponMouseLeave } onMouseUp={ props.GrouponMouseUp } onMouseDown={ props.GrouponMouseDown } onKeyDown={ props.GrouponKeyDown } onChange={ props.GrouponChange } ondelay={ props.Groupondelay }>
            <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['group']?.animationClass || {}}>

              <div id="id_fivefivenigth_onenigthnigththree" className={` group group ${ props.onClick ? 'cursor' : '' } ${ transaction['group']?.type ? transaction['group']?.type.toLowerCase() : '' }`} style={{ ...{},  ...props.GroupStyle , transitionDuration: transaction['group']?.duration, transitionTimingFunction: transaction['group']?.timingFunction }} onClick={ props.GrouponClick } onMouseEnter={ props.GrouponMouseEnter } onMouseOver={ props.GrouponMouseOver } onKeyPress={ props.GrouponKeyPress } onDrag={ props.GrouponDrag } onMouseLeave={ props.GrouponMouseLeave } onMouseUp={ props.GrouponMouseUp } onMouseDown={ props.GrouponMouseDown } onKeyDown={ props.GrouponKeyDown } onChange={ props.GrouponChange } ondelay={ props.Groupondelay }>
                <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['group']?.animationClass || {}}>

                  <div id="id_fivefivenigth_onenigthnigthfour" className={` group group ${ props.onClick ? 'cursor' : '' } ${ transaction['group']?.type ? transaction['group']?.type.toLowerCase() : '' }`} style={{ ...{},  ...props.GroupStyle , transitionDuration: transaction['group']?.duration, transitionTimingFunction: transaction['group']?.timingFunction }} onClick={ props.GrouponClick } onMouseEnter={ props.GrouponMouseEnter } onMouseOver={ props.GrouponMouseOver } onKeyPress={ props.GrouponKeyPress } onDrag={ props.GrouponDrag } onMouseLeave={ props.GrouponMouseLeave } onMouseUp={ props.GrouponMouseUp } onMouseDown={ props.GrouponMouseDown } onKeyDown={ props.GrouponKeyDown } onChange={ props.GrouponChange } ondelay={ props.Groupondelay }>
                    <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['vector']?.animationClass || {}}>
                      <svg id="id_fivefivenigth_onenigthnigthfive" className="vector  " style={{}} onClick={ props.VectoronClick } onMouseEnter={ props.VectoronMouseEnter } onMouseOver={ props.VectoronMouseOver } onKeyPress={ props.VectoronKeyPress } onDrag={ props.VectoronDrag } onMouseLeave={ props.VectoronMouseLeave } onMouseUp={ props.VectoronMouseUp } onMouseDown={ props.VectoronMouseDown } onKeyDown={ props.VectoronKeyDown } onChange={ props.VectoronChange } ondelay={ props.Vectorondelay } width="120" height="40.000244140625">
                        <path d="M110.444 0.00013L9.56145 0.00013C9.19372 0.00013 8.8304 0.00013 8.46364 0.00213C8.15663 0.00413 7.85207 0.00994 7.54211 0.01483C6.86874 0.0227318 6.19695 0.0818063 5.53259 0.19154C4.86915 0.303663 4.2265 0.515046 3.62637 0.81854C3.02697 1.1246 2.47929 1.5223 2.00318 1.9972C1.52457 2.47077 1.12561 3.01815 0.82165 3.61829C0.516847 4.21724 0.305496 4.85907 0.194896 5.52161C0.083244 6.18332 0.0231632 6.85265 0.0151925 7.52361C0.00588648 7.83021 0.00490373 8.13783 0 8.44447L0 31.5587C0.00490373 31.8692 0.00588648 32.17 0.0151925 32.4806C0.0231657 33.1516 0.0832464 33.8209 0.194896 34.4825C0.30519 35.1455 0.516554 35.7877 0.82165 36.3868C1.12548 36.985 1.52449 37.5302 2.00318 38.0011C2.47748 38.4781 3.02552 38.8761 3.62637 39.1798C4.2265 39.4841 4.86908 39.6968 5.53259 39.8106C6.19707 39.9195 6.86879 39.9786 7.54211 39.9874C7.85207 39.9942 8.15663 39.9981 8.46364 39.9981C8.83039 40.0001 9.19374 40.0001 9.56145 40.0001L110.444 40.0001C110.804 40.0001 111.171 40.0001 111.531 39.9981C111.837 39.9981 112.15 39.9942 112.455 39.9874C113.128 39.9791 113.798 39.92 114.461 39.8106C115.127 39.696 115.772 39.4834 116.375 39.1798C116.975 38.8759 117.522 38.478 117.996 38.0011C118.474 37.5284 118.874 36.9836 119.181 36.3868C119.484 35.7872 119.693 35.1451 119.802 34.4825C119.914 33.8208 119.976 33.1516 119.988 32.4806C119.992 32.17 119.992 31.8692 119.992 31.5587C120 31.1954 120 30.8341 120 30.4649L120 9.53626C120 9.17005 120 8.80677 119.992 8.44447C119.992 8.13783 119.992 7.83021 119.988 7.52357C119.976 6.85255 119.914 6.18337 119.802 5.52157C119.693 4.85941 119.484 4.21763 119.181 3.61825C118.562 2.41533 117.581 1.43616 116.375 0.81845C115.772 0.515697 115.127 0.30437 114.461 0.19145C113.798 0.0812328 113.128 0.0221378 112.455 0.01469C112.15 0.00981 111.837 0.00395 111.531 0.002C111.171 0 110.804 0.00013 110.444 0.00013Z" />
                      </svg>
                    </CSSTransition>
                    <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['vector']?.animationClass || {}}>
                      <svg id="id_fivefivenigth_onenigthnigthsix" className="vector  " style={{}} onClick={ props.VectoronClick } onMouseEnter={ props.VectoronMouseEnter } onMouseOver={ props.VectoronMouseOver } onKeyPress={ props.VectoronKeyPress } onDrag={ props.VectoronDrag } onMouseLeave={ props.VectoronMouseLeave } onMouseUp={ props.VectoronMouseUp } onMouseDown={ props.VectoronMouseDown } onKeyDown={ props.VectoronKeyDown } onChange={ props.VectoronChange } ondelay={ props.Vectorondelay } width="118.2490234375" height="38.251953125">
                        <path d="M7.59308 38.25C7.28754 38.25 6.98939 38.2461 6.68625 38.2393C6.05827 38.2311 5.43176 38.1766 4.81186 38.0762C4.23383 37.9769 3.67388 37.7923 3.15047 37.5283C2.63186 37.2665 2.15885 36.9233 1.74955 36.5117C1.33433 36.105 0.988634 35.6332 0.726173 35.1152C0.460819 34.5938 0.277179 34.0349 0.181649 33.458C0.0784841 32.8381 0.0226679 32.2113 0.0146811 31.583C0.00832333 31.3721 0 30.6699 0 30.6699L0 7.56934C0 7.56934 0.00886486 6.87793 0.0147313 6.6748C0.0223785 6.04748 0.0778702 5.42165 0.180726 4.80273C0.276433 4.22425 0.460215 3.66375 0.725712 3.14063C0.987208 2.62294 1.33098 2.15086 1.7437 1.74268C2.15596 1.33062 2.63049 0.985604 3.14998 0.72021C3.67218 0.457094 4.23108 0.273733 4.80794 0.17627C5.42988 0.0748361 6.05856 0.0199961 6.68871 0.01221L7.59357 0L110.651 0L111.566 0.0127C112.191 0.0200992 112.814 0.07445 113.43 0.17529C114.013 0.273975 114.577 0.458616 115.105 0.72314C116.146 1.25799 116.993 2.10416 117.527 3.14307C117.789 3.66258 117.969 4.21851 118.064 4.79199C118.168 5.41599 118.226 6.04674 118.238 6.6792C118.241 6.9624 118.241 7.2666 118.241 7.56934C118.249 7.94434 118.249 8.30127 118.249 8.66113L118.249 29.5898C118.249 29.9531 118.249 30.3076 118.241 30.665C118.241 30.9902 118.241 31.2881 118.237 31.5947C118.226 32.2159 118.168 32.8354 118.066 33.4482C117.972 34.0293 117.79 34.5925 117.524 35.1182C117.26 35.6306 116.916 36.0983 116.506 36.5039C116.096 36.9177 115.622 37.2629 115.103 37.5264C114.576 37.7924 114.012 37.9777 113.43 38.0762C112.81 38.1772 112.183 38.2317 111.555 38.2393C111.262 38.2461 110.954 38.25 110.656 38.25L109.568 38.252L7.59308 38.25Z" />
                      </svg>
                    </CSSTransition>
                  </div>

                </CSSTransition>
                <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['group']?.animationClass || {}}>

                  <div id="id_fivefivenigth_onenigthnigthseven" className={` group group ${ props.onClick ? 'cursor' : '' } ${ transaction['group']?.type ? transaction['group']?.type.toLowerCase() : '' }`} style={{ ...{},  ...props.GroupStyle , transitionDuration: transaction['group']?.duration, transitionTimingFunction: transaction['group']?.timingFunction }} onClick={ props.GrouponClick } onMouseEnter={ props.GrouponMouseEnter } onMouseOver={ props.GrouponMouseOver } onKeyPress={ props.GrouponKeyPress } onDrag={ props.GrouponDrag } onMouseLeave={ props.GrouponMouseLeave } onMouseUp={ props.GrouponMouseUp } onMouseDown={ props.GrouponMouseDown } onKeyDown={ props.GrouponKeyDown } onChange={ props.GrouponChange } ondelay={ props.Groupondelay }>
                    <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['group']?.animationClass || {}}>

                      <div id="id_fivefivenigth_onenigthnigtheight" className={` group group ${ props.onClick ? 'cursor' : '' } ${ transaction['group']?.type ? transaction['group']?.type.toLowerCase() : '' }`} style={{ ...{},  ...props.GroupStyle , transitionDuration: transaction['group']?.duration, transitionTimingFunction: transaction['group']?.timingFunction }} onClick={ props.GrouponClick } onMouseEnter={ props.GrouponMouseEnter } onMouseOver={ props.GrouponMouseOver } onKeyPress={ props.GrouponKeyPress } onDrag={ props.GrouponDrag } onMouseLeave={ props.GrouponMouseLeave } onMouseUp={ props.GrouponMouseUp } onMouseDown={ props.GrouponMouseDown } onKeyDown={ props.GrouponKeyDown } onChange={ props.GrouponChange } ondelay={ props.Groupondelay }>
                        <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['group']?.animationClass || {}}>

                          <div id="id_fivefivenigth_onenigthnigthnigth" className={` group group ${ props.onClick ? 'cursor' : '' } ${ transaction['group']?.type ? transaction['group']?.type.toLowerCase() : '' }`} style={{ ...{},  ...props.GroupStyle , transitionDuration: transaction['group']?.duration, transitionTimingFunction: transaction['group']?.timingFunction }} onClick={ props.GrouponClick } onMouseEnter={ props.GrouponMouseEnter } onMouseOver={ props.GrouponMouseOver } onKeyPress={ props.GrouponKeyPress } onDrag={ props.GrouponDrag } onMouseLeave={ props.GrouponMouseLeave } onMouseUp={ props.GrouponMouseUp } onMouseDown={ props.GrouponMouseDown } onKeyDown={ props.GrouponKeyDown } onChange={ props.GrouponChange } ondelay={ props.Groupondelay }>
                            <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['path']?.animationClass || {}}>
                              <svg id="id_fivefivenigth_twoozerozerozero" className="path  " style={{}} onClick={ props.PathonClick } onMouseEnter={ props.PathonMouseEnter } onMouseOver={ props.PathonMouseOver } onKeyPress={ props.PathonKeyPress } onDrag={ props.PathonDrag } onMouseLeave={ props.PathonMouseLeave } onMouseUp={ props.PathonMouseUp } onMouseDown={ props.PathonMouseDown } onKeyDown={ props.PathonKeyDown } onChange={ props.PathonChange } ondelay={ props.Pathondelay } width="17.766357421875" height="16.523193359375">
                                <path d="M14.8389 6.32767C14.8496 5.49301 15.072 4.67463 15.4851 3.94868C15.8983 3.22273 16.4889 2.61279 17.202 2.17561C16.749 1.53045 16.1514 0.999512 15.4566 0.624959C14.7619 0.250407 13.9891 0.042563 13.1997 0.0179334C11.5157 -0.158327 9.88319 1.02276 9.02508 1.02276C8.15036 1.02276 6.82916 0.0354333 5.40645 0.0646233C4.4862 0.0942714 3.58934 0.361125 2.80326 0.839183C2.01718 1.31724 1.36869 1.9902 0.920971 2.79249C-1.01846 6.14091 0.428181 11.062 2.28599 13.7686C3.21549 15.0939 4.30182 16.5744 5.72324 16.5219C7.1142 16.4643 7.63369 15.6374 9.31269 15.6374C10.9761 15.6374 11.4635 16.5219 12.9138 16.4885C14.4063 16.4643 15.3467 15.1573 16.2436 13.8194C16.9115 12.875 17.4254 11.8313 17.7663 10.7268C16.8992 10.3611 16.1592 9.74894 15.6386 8.96664C15.118 8.18434 14.8399 7.26654 14.8389 6.32767Z" />
                              </svg>
                            </CSSTransition>
                            <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['path']?.animationClass || {}}>
                              <svg id="id_fivefivenigth_twoozerozeroone" className="path  " style={{}} onClick={ props.PathonClick } onMouseEnter={ props.PathonMouseEnter } onMouseOver={ props.PathonMouseOver } onKeyPress={ props.PathonKeyPress } onDrag={ props.PathonDrag } onMouseLeave={ props.PathonMouseLeave } onMouseUp={ props.PathonMouseUp } onMouseDown={ props.PathonMouseDown } onKeyDown={ props.PathonKeyDown } onChange={ props.PathonChange } ondelay={ props.Pathondelay } width="4.3876953125" height="5.021240234375">
                                <path d="M3.25567 3.49062C4.06948 2.51641 4.47041 1.26423 4.37332 0C3.13001 0.13022 1.98153 0.722779 1.15673 1.65961C0.753464 2.11728 0.444603 2.64971 0.247806 3.22648C0.0510102 3.80325 -0.0298611 4.41304 0.00981456 5.021C0.631695 5.02739 1.24692 4.89297 1.80916 4.62788C2.37139 4.3628 2.86598 3.97395 3.25567 3.49062Z" />
                              </svg>
                            </CSSTransition>
                          </div>

                        </CSSTransition>
                      </div>

                    </CSSTransition>
                    <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['group']?.animationClass || {}}>

                      <div id="id_fivefivenigth_twoozerozerotwoo" className={` group group ${ props.onClick ? 'cursor' : '' } ${ transaction['group']?.type ? transaction['group']?.type.toLowerCase() : '' }`} style={{ ...{},  ...props.GroupStyle , transitionDuration: transaction['group']?.duration, transitionTimingFunction: transaction['group']?.timingFunction }} onClick={ props.GrouponClick } onMouseEnter={ props.GrouponMouseEnter } onMouseOver={ props.GrouponMouseOver } onKeyPress={ props.GrouponKeyPress } onDrag={ props.GrouponDrag } onMouseLeave={ props.GrouponMouseLeave } onMouseUp={ props.GrouponMouseUp } onMouseDown={ props.GrouponMouseDown } onKeyDown={ props.GrouponKeyDown } onChange={ props.GrouponChange } ondelay={ props.Groupondelay }>
                        <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['vector']?.animationClass || {}}>
                          <svg id="id_fivefivenigth_twoozerozerothree" className="vector  " style={{}} onClick={ props.VectoronClick } onMouseEnter={ props.VectoronMouseEnter } onMouseOver={ props.VectoronMouseOver } onKeyPress={ props.VectoronKeyPress } onDrag={ props.VectoronDrag } onMouseLeave={ props.VectoronMouseLeave } onMouseUp={ props.VectoronMouseUp } onMouseDown={ props.VectoronMouseDown } onKeyDown={ props.VectoronKeyDown } onChange={ props.VectoronChange } ondelay={ props.Vectorondelay } width="11.080810546875" height="12.41796875">
                            <path d="M7.89711 9.06155L3.15042 9.06155L2.01051 12.418L0 12.418L4.49599 0L6.58484 0L11.0808 12.418L9.03603 12.418L7.89711 9.06155ZM3.64203 7.51272L7.40456 7.51272L5.54976 2.06545L5.49785 2.06545L3.64203 7.51272Z" />
                          </svg>
                        </CSSTransition>
                        <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['vector']?.animationClass || {}}>
                          <svg id="id_fivefivenigth_twoozerozerofour" className="vector  " style={{}} onClick={ props.VectoronClick } onMouseEnter={ props.VectoronMouseEnter } onMouseOver={ props.VectoronMouseOver } onKeyPress={ props.VectoronKeyPress } onDrag={ props.VectoronDrag } onMouseLeave={ props.VectoronMouseLeave } onMouseUp={ props.VectoronMouseUp } onMouseDown={ props.VectoronMouseDown } onKeyDown={ props.VectoronKeyDown } onChange={ props.VectoronChange } ondelay={ props.Vectorondelay } width="8.55224609375" height="12.145263671875">
                            <path d="M8.55229 4.62375C8.55229 7.43723 7.0422 9.24484 4.76337 9.24484C4.18609 9.27495 3.61199 9.14235 3.10678 8.86221C2.60157 8.58207 2.18569 8.16572 1.90674 7.66084L1.86362 7.66084L1.86362 12.1452L0 12.1452L0 0.0963995L1.80391 0.0963995L1.80391 1.60226L1.83818 1.60226C2.12996 1.09979 2.55287 0.685603 3.06192 0.403757C3.57098 0.121911 4.14714 -0.0170459 4.72909 0.00166915C7.03335 0.00167915 8.55229 1.81808 8.55229 4.62375ZM6.63677 4.62375C6.63677 2.79075 5.68684 1.58566 4.23747 1.58566C2.81357 1.58566 1.8558 2.81613 1.8558 4.62375C1.8558 6.44797 2.81357 7.66965 4.23747 7.66965C5.68685 7.66965 6.63677 6.47336 6.63677 4.62375Z" />
                          </svg>
                        </CSSTransition>
                        <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['vector']?.animationClass || {}}>
                          <svg id="id_fivefivenigth_twoozerozerofive" className="vector  " style={{}} onClick={ props.VectoronClick } onMouseEnter={ props.VectoronMouseEnter } onMouseOver={ props.VectoronMouseOver } onKeyPress={ props.VectoronKeyPress } onDrag={ props.VectoronDrag } onMouseLeave={ props.VectoronMouseLeave } onMouseUp={ props.VectoronMouseUp } onMouseDown={ props.VectoronMouseDown } onKeyDown={ props.VectoronKeyDown } onChange={ props.VectoronChange } ondelay={ props.Vectorondelay } width="8.55224609375" height="12.145263671875">
                            <path d="M8.55229 4.62374C8.55229 7.43722 7.0422 9.24483 4.76337 9.24483C4.18609 9.27494 3.61199 9.14234 3.10678 8.8622C2.60157 8.58206 2.18569 8.16572 1.90674 7.66083L1.86362 7.66083L1.86362 12.1452L0 12.1452L0 0.0963905L1.80387 0.0963905L1.80387 1.60225L1.83815 1.60225C2.12992 1.09979 2.55283 0.685596 3.06189 0.403752C3.57095 0.121908 4.14711 -0.0170482 4.72906 0.00166964C7.03339 0.00166964 8.55229 1.81807 8.55229 4.62374ZM6.63677 4.62374C6.63677 2.79074 5.68684 1.58565 4.23747 1.58565C2.81357 1.58565 1.8558 2.81612 1.8558 4.62374C1.8558 6.44796 2.81357 7.66964 4.23747 7.66964C5.68685 7.66964 6.63677 6.47335 6.63677 4.62374Z" />
                          </svg>
                        </CSSTransition>
                        <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['vector']?.animationClass || {}}>
                          <svg id="id_fivefivenigth_twoozerozerosix" className="vector  " style={{}} onClick={ props.VectoronClick } onMouseEnter={ props.VectoronMouseEnter } onMouseOver={ props.VectoronMouseOver } onKeyPress={ props.VectoronKeyPress } onDrag={ props.VectoronDrag } onMouseLeave={ props.VectoronMouseLeave } onMouseUp={ props.VectoronMouseUp } onMouseDown={ props.VectoronMouseDown } onKeyDown={ props.VectoronKeyDown } onChange={ props.VectoronChange } ondelay={ props.Vectorondelay } width="9.55419921875" height="12.83203125">
                            <path d="M1.90771 9.16509C2.0458 10.3965 3.24546 11.2051 4.8848 11.2051C6.45561 11.2051 7.58572 10.3965 7.58572 9.28614C7.58572 8.32227 6.90412 7.74514 5.29023 7.34962L3.67635 6.96192C1.38967 6.41114 0.328097 5.34473 0.328097 3.61426C0.328097 1.47168 2.20053 0 4.85934 0C7.4907 0 9.2946 1.47168 9.35532 3.61426L7.47406 3.61426C7.36144 2.375 6.33415 1.62696 4.83287 1.62696C3.3316 1.62696 2.30431 2.3838 2.30431 3.48536C2.30431 4.36329 2.96045 4.87989 4.56552 5.27536L5.93752 5.6113C8.49252 6.21384 9.5541 7.2373 9.5541 9.05368C9.5541 11.3769 7.69831 12.832 4.74669 12.832C1.98505 12.832 0.120457 11.4111 0 9.165L1.90771 9.16509Z" />
                          </svg>
                        </CSSTransition>
                        <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['vector']?.animationClass || {}}>
                          <svg id="id_fivefivenigth_twoozerozeroseven" className="vector  " style={{}} onClick={ props.VectoronClick } onMouseEnter={ props.VectoronMouseEnter } onMouseOver={ props.VectoronMouseOver } onKeyPress={ props.VectoronKeyPress } onDrag={ props.VectoronDrag } onMouseLeave={ props.VectoronMouseLeave } onMouseUp={ props.VectoronMouseUp } onMouseDown={ props.VectoronMouseDown } onKeyDown={ props.VectoronKeyDown } onChange={ props.VectoronChange } ondelay={ props.Vectorondelay } width="4.9189453125" height="11.248779296875">
                            <path d="M3.19252 0L3.19252 2.14258L4.91904 2.14258L4.91904 3.61426L3.19252 3.61426L3.19252 8.60547C3.19252 9.38086 3.53822 9.74219 4.29718 9.74219C4.50214 9.73863 4.70678 9.72427 4.91022 9.69919L4.91022 11.1621C4.569 11.2257 4.22217 11.2545 3.8751 11.248C2.03695 11.248 1.32009 10.5595 1.32009 8.80368L1.32009 3.61426L0 3.61426L0 2.14258L1.32007 2.14258L1.32007 0L3.19252 0Z" />
                          </svg>
                        </CSSTransition>
                        <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['vector']?.animationClass || {}}>
                          <svg id="id_fivefivenigth_twoozerozeroeight" className="vector  " style={{}} onClick={ props.VectoronClick } onMouseEnter={ props.VectoronMouseEnter } onMouseOver={ props.VectoronMouseOver } onKeyPress={ props.VectoronKeyPress } onDrag={ props.VectoronDrag } onMouseLeave={ props.VectoronMouseLeave } onMouseUp={ props.VectoronMouseUp } onMouseDown={ props.VectoronMouseDown } onKeyDown={ props.VectoronKeyDown } onChange={ props.VectoronChange } ondelay={ props.Vectorondelay } width="8.613037109375" height="9.27734375">
                            <path d="M0 4.63867C0 1.79004 1.68244 0 4.306 0C6.93837 0 8.61298 1.79 8.61298 4.63867C8.61298 7.49512 6.94718 9.27734 4.306 9.27734C1.66575 9.27734 0 7.49511 0 4.63867ZM6.7141 4.63867C6.7141 2.68457 5.81608 1.53125 4.30599 1.53125C2.7959 1.53125 1.89886 2.69336 1.89886 4.63867C1.89886 6.60058 2.7959 7.74512 4.30599 7.74512C5.81608 7.74512 6.7141 6.60058 6.7141 4.63867Z" />
                          </svg>
                        </CSSTransition>
                        <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['vector']?.animationClass || {}}>
                          <svg id="id_fivefivenigth_twoozerozeronigth" className="vector  " style={{}} onClick={ props.VectoronClick } onMouseEnter={ props.VectoronMouseEnter } onMouseOver={ props.VectoronMouseOver } onKeyPress={ props.VectoronKeyPress } onDrag={ props.VectoronDrag } onMouseLeave={ props.VectoronMouseLeave } onMouseUp={ props.VectoronMouseUp } onMouseDown={ props.VectoronMouseDown } onKeyDown={ props.VectoronKeyDown } onChange={ props.VectoronChange } ondelay={ props.Vectorondelay } width="4.642822265625" height="9.14990234375">
                            <path d="M7.65081e-06 0.0963152L1.77744 0.0963152L1.77744 1.63731L1.82056 1.63731C1.94082 1.15603 2.22357 0.730697 2.62127 0.432812C3.01897 0.134926 3.50738 -0.017352 4.00441 0.0015734C4.21918 0.000825633 4.43335 0.0240858 4.64292 0.0709151L4.64292 1.80919C4.37179 1.72658 4.08896 1.68864 3.80558 1.69689C3.53484 1.68594 3.26493 1.73354 3.01439 1.83643C2.76384 1.93932 2.53859 2.09505 2.3541 2.29294C2.16961 2.49083 2.03025 2.72619 1.94559 2.98286C1.86092 3.23953 1.83296 3.51143 1.86362 3.77989L1.86362 9.15001L0 9.15001L7.65081e-06 0.0963152Z" />
                          </svg>
                        </CSSTransition>
                        <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['vector']?.animationClass || {}}>
                          <svg id="id_fivefivenigth_twoozeroonezero" className="vector  " style={{}} onClick={ props.VectoronClick } onMouseEnter={ props.VectoronMouseEnter } onMouseOver={ props.VectoronMouseOver } onKeyPress={ props.VectoronKeyPress } onDrag={ props.VectoronDrag } onMouseLeave={ props.VectoronMouseLeave } onMouseUp={ props.VectoronMouseUp } onMouseDown={ props.VectoronMouseDown } onKeyDown={ props.VectoronKeyDown } onChange={ props.VectoronChange } ondelay={ props.Vectorondelay } width="8.293701171875" height="9.27734375">
                            <path d="M8.18992 6.50586C7.93922 8.14941 6.33414 9.27734 4.28054 9.27734C1.63935 9.27734 0 7.51269 0 4.68164C0 1.8418 1.64816 0 4.20219 0C6.71411 0 8.29373 1.7207 8.29373 4.46582L8.29373 5.10254L1.88125 5.10254L1.88125 5.21484C1.85165 5.54804 1.89348 5.88371 2.00397 6.19955C2.11446 6.51539 2.29107 6.80417 2.52207 7.04672C2.75308 7.28927 3.03318 7.48003 3.3438 7.60633C3.65441 7.73264 3.98842 7.79159 4.32363 7.77929C4.76403 7.82044 5.20598 7.71873 5.58375 7.48929C5.96151 7.25985 6.25492 6.91493 6.42032 6.50585L8.18992 6.50586ZM1.89006 3.80371L6.42913 3.80371C6.44586 3.50411 6.40031 3.2043 6.29534 2.92309C6.19037 2.64188 6.02824 2.38535 5.81913 2.16958C5.61002 1.95381 5.35845 1.78348 5.0802 1.66926C4.80195 1.55505 4.50303 1.49941 4.20219 1.50586C3.89871 1.50405 3.59788 1.5622 3.31705 1.67694C3.03622 1.79168 2.78096 1.96075 2.566 2.17438C2.35104 2.38802 2.18063 2.64199 2.06462 2.92164C1.9486 3.20129 1.88928 3.50107 1.89006 3.80371Z" />
                          </svg>
                        </CSSTransition>
                      </div>

                    </CSSTransition>
                  </div>

                </CSSTransition>
              </div>

            </CSSTransition>
            <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['group']?.animationClass || {}}>

              <div id="id_fivefivenigth_twoozerooneone" className={` group group ${ props.onClick ? 'cursor' : '' } ${ transaction['group']?.type ? transaction['group']?.type.toLowerCase() : '' }`} style={{ ...{},  ...props.GroupStyle , transitionDuration: transaction['group']?.duration, transitionTimingFunction: transaction['group']?.timingFunction }} onClick={ props.GrouponClick } onMouseEnter={ props.GrouponMouseEnter } onMouseOver={ props.GrouponMouseOver } onKeyPress={ props.GrouponKeyPress } onDrag={ props.GrouponDrag } onMouseLeave={ props.GrouponMouseLeave } onMouseUp={ props.GrouponMouseUp } onMouseDown={ props.GrouponMouseDown } onKeyDown={ props.GrouponKeyDown } onChange={ props.GrouponChange } ondelay={ props.Groupondelay }>
                <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['group']?.animationClass || {}}>

                  <div id="id_fivefivenigth_twoozeroonetwoo" className={` group group ${ props.onClick ? 'cursor' : '' } ${ transaction['group']?.type ? transaction['group']?.type.toLowerCase() : '' }`} style={{ ...{},  ...props.GroupStyle , transitionDuration: transaction['group']?.duration, transitionTimingFunction: transaction['group']?.timingFunction }} onClick={ props.GrouponClick } onMouseEnter={ props.GrouponMouseEnter } onMouseOver={ props.GrouponMouseOver } onKeyPress={ props.GrouponKeyPress } onDrag={ props.GrouponDrag } onMouseLeave={ props.GrouponMouseLeave } onMouseUp={ props.GrouponMouseUp } onMouseDown={ props.GrouponMouseDown } onKeyDown={ props.GrouponKeyDown } onChange={ props.GrouponChange } ondelay={ props.Groupondelay }>
                    <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['vector']?.animationClass || {}}>
                      <svg id="id_fivefivenigth_twoozeroonethree" className="vector  " style={{}} onClick={ props.VectoronClick } onMouseEnter={ props.VectoronMouseEnter } onMouseOver={ props.VectoronMouseOver } onKeyPress={ props.VectoronKeyPress } onDrag={ props.VectoronDrag } onMouseLeave={ props.VectoronMouseLeave } onMouseUp={ props.VectoronMouseUp } onMouseDown={ props.VectoronMouseDown } onKeyDown={ props.VectoronKeyDown } onChange={ props.VectoronChange } ondelay={ props.Vectorondelay } width="5.183349609375" height="6.26611328125">
                        <path d="M0 3.13329C0 1.19823 1.03219 0.00292111 2.69504 0.00292111C3.29437 -0.026679 3.88295 0.169533 4.34392 0.552607C4.8049 0.93568 5.10458 1.4776 5.18346 2.07079L4.25802 2.07079C4.18038 1.71925 3.9813 1.40611 3.6956 1.1861C3.4099 0.966087 3.05575 0.853204 2.69504 0.867181C1.62075 0.867181 0.948946 1.73974 0.948946 3.13329C0.948946 4.52294 1.62075 5.39501 2.69897 5.39501C3.04648 5.42095 3.39221 5.32537 3.67673 5.12469C3.96124 4.92402 4.16678 4.63079 4.25802 4.2954L5.18346 4.2954C5.09319 4.87447 4.78726 5.39829 4.32673 5.76232C3.86619 6.12635 3.28493 6.3038 2.69897 6.25927C1.03615 6.25927 0 5.06444 0 3.13329Z" />
                      </svg>
                    </CSSTransition>
                    <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['vector']?.animationClass || {}}>
                      <svg id="id_fivefivenigth_twoozeroonefour" className="vector  " style={{}} onClick={ props.VectoronClick } onMouseEnter={ props.VectoronMouseEnter } onMouseOver={ props.VectoronMouseOver } onKeyPress={ props.VectoronKeyPress } onDrag={ props.VectoronDrag } onMouseLeave={ props.VectoronMouseLeave } onMouseUp={ props.VectoronMouseUp } onMouseDown={ props.VectoronMouseDown } onKeyDown={ props.VectoronKeyDown } onChange={ props.VectoronChange } ondelay={ props.Vectorondelay } width="4.279052734375" height="4.6767578125">
                        <path d="M0.0100733 2.33638C-0.0183153 2.04046 0.0156289 1.74189 0.109733 1.45982C0.203837 1.17775 0.356022 0.918411 0.556525 0.698429C0.757028 0.478447 1.00142 0.302681 1.27404 0.182405C1.54665 0.06213 1.84147 1.77636e-15 2.13957 0C2.43767 1.77636e-15 2.73248 0.06213 3.0051 0.182405C3.27771 0.302681 3.52211 0.478447 3.72261 0.698429C3.92312 0.918411 4.0753 1.17775 4.1694 1.45982C4.26351 1.74189 4.29745 2.04046 4.26907 2.33638C4.29799 2.6326 4.26445 2.93158 4.1706 3.21411C4.07674 3.49663 3.92465 3.75645 3.7241 3.97686C3.52354 4.19728 3.27896 4.37341 3.00607 4.49395C2.73319 4.61448 2.43803 4.67675 2.13957 4.67675C1.84111 4.67675 1.54595 4.61448 1.27307 4.49395C1.00018 4.37341 0.755599 4.19728 0.555044 3.97686C0.35449 3.75645 0.202398 3.49663 0.108543 3.21411C0.0146882 2.93158 -0.018854 2.6326 0.0100733 2.33638ZM3.35243 2.33638C3.35243 1.36031 2.91272 0.789509 2.14104 0.789509C1.36641 0.789509 0.93065 1.36031 0.93065 2.33639C0.93065 3.32028 1.36644 3.88668 2.14104 3.88668C2.91273 3.88667 3.35243 3.31636 3.35243 2.33638Z" />
                      </svg>
                    </CSSTransition>
                    <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['vector']?.animationClass || {}}>
                      <svg id="id_fivefivenigth_twoozeroonefive" className="vector  " style={{}} onClick={ props.VectoronClick } onMouseEnter={ props.VectoronMouseEnter } onMouseOver={ props.VectoronMouseOver } onKeyPress={ props.VectoronKeyPress } onDrag={ props.VectoronDrag } onMouseLeave={ props.VectoronMouseLeave } onMouseUp={ props.VectoronMouseUp } onMouseDown={ props.VectoronMouseDown } onKeyDown={ props.VectoronKeyDown } onChange={ props.VectoronChange } ondelay={ props.Vectorondelay } width="3.850830078125" height="4.59423828125">
                        <path d="M0 0.0913533L0.85787 0.0913533L0.85787 0.806682L0.924467 0.806682C1.03744 0.549762 1.22798 0.33439 1.46953 0.190584C1.71107 0.0467781 1.99159 -0.0182962 2.27199 0.00443254C2.4917 -0.0120404 2.71232 0.0209952 2.91749 0.101091C3.12266 0.181187 3.30713 0.306292 3.45724 0.467135C3.60734 0.627979 3.71923 0.82044 3.78462 1.03026C3.85 1.24008 3.8672 1.46188 3.83495 1.67923L3.83495 4.59423L2.94379 4.59423L2.94379 1.90239C2.94379 1.17876 2.62846 0.818893 1.96938 0.818893C1.82021 0.811965 1.6713 0.837283 1.53285 0.893113C1.39441 0.948943 1.2697 1.03397 1.16729 1.14235C1.06488 1.25074 0.987181 1.37992 0.93952 1.52105C0.891859 1.66218 0.875366 1.81192 0.891167 1.96L0.891167 4.59428L0 4.59428L0 0.0913533Z" />
                      </svg>
                    </CSSTransition>
                    <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['vector']?.animationClass || {}}>
                      <svg id="id_fivefivenigth_twoozeroonesix" className="vector  " style={{}} onClick={ props.VectoronClick } onMouseEnter={ props.VectoronMouseEnter } onMouseOver={ props.VectoronMouseOver } onKeyPress={ props.VectoronKeyPress } onDrag={ props.VectoronDrag } onMouseLeave={ props.VectoronMouseLeave } onMouseUp={ props.VectoronMouseUp } onMouseDown={ props.VectoronMouseDown } onKeyDown={ props.VectoronKeyDown } onChange={ props.VectoronChange } ondelay={ props.Vectorondelay } width="3.724365234375" height="4.6767578125">
                        <path d="M1.87047 0C2.88601 0 3.54997 0.47119 3.63713 1.26514L2.7822 1.26514C2.69997 0.93457 2.37579 0.723639 1.87047 0.723639C1.37298 0.723639 0.995015 0.95899 0.995015 1.31055C0.995015 1.57955 1.22319 1.74903 1.71284 1.86084L2.46295 2.03418C3.3218 2.23291 3.72429 2.60107 3.72429 3.2627C3.72429 4.11036 2.93207 4.67676 1.85382 4.67676C0.779521 4.67676 0.0793221 4.19287 0 3.39502L0.892177 3.39502C0.969053 3.58492 1.10768 3.74355 1.2858 3.84547C1.46393 3.94738 1.67122 3.98667 1.87442 3.95702C2.42968 3.95702 2.82435 3.70902 2.82435 3.34911C2.82435 3.08056 2.61282 2.90673 2.16038 2.79931L1.37301 2.61718C0.51416 2.41454 0.116568 2.03027 0.116568 1.36034C0.116538 0.56201 0.85006 0 1.87047 0Z" />
                      </svg>
                    </CSSTransition>
                    <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['vector']?.animationClass || {}}>
                      <svg id="id_fivefivenigth_twoozerooneseven" className="vector  " style={{}} onClick={ props.VectoronClick } onMouseEnter={ props.VectoronMouseEnter } onMouseOver={ props.VectoronMouseOver } onKeyPress={ props.VectoronKeyPress } onDrag={ props.VectoronDrag } onMouseLeave={ props.VectoronMouseLeave } onMouseUp={ props.VectoronMouseUp } onMouseDown={ props.VectoronMouseDown } onKeyDown={ props.VectoronKeyDown } onChange={ props.VectoronChange } ondelay={ props.Vectorondelay } width="2.0859375" height="6.67822265625">
                        <path d="M0 1.42236L1.041 0L2.08593 0L0.920546 1.42236L0 1.42236ZM0.0920584 2.17529L0.979312 2.17529L0.979312 6.67822L0.0920584 6.67822L0.0920584 2.17529Z" />
                      </svg>
                    </CSSTransition>
                    <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['vector']?.animationClass || {}}>
                      <svg id="id_fivefivenigth_twoozerooneeight" className="vector  " style={{}} onClick={ props.VectoronClick } onMouseEnter={ props.VectoronMouseEnter } onMouseOver={ props.VectoronMouseOver } onKeyPress={ props.VectoronKeyPress } onDrag={ props.VectoronDrag } onMouseLeave={ props.VectoronMouseLeave } onMouseUp={ props.VectoronMouseUp } onMouseDown={ props.VectoronMouseDown } onKeyDown={ props.VectoronKeyDown } onChange={ props.VectoronChange } ondelay={ props.Vectorondelay } width="4.216796875" height="6.298828125">
                        <path d="M0.178237 5.02927L1.08997 5.02927C1.16538 5.35593 1.54241 5.56687 2.1437 5.56687C2.88601 5.56687 3.32572 5.21531 3.32572 4.62009L3.32572 3.75583L3.25912 3.75583C3.11934 3.99775 2.91521 4.19644 2.66928 4.32991C2.42335 4.46339 2.1452 4.52647 1.86557 4.51218C0.712934 4.51218 0 3.62351 0 2.27536C0 0.902356 0.720768 0.000945479 1.87439 0.000945479C2.16102 -0.00900775 2.44487 0.0597824 2.69495 0.199801C2.94503 0.339819 3.15171 0.545681 3.29242 0.794896L3.36292 0.794896L3.36292 0.0756554L4.21688 0.0756554L4.21688 4.61566C4.21688 5.64545 3.40797 6.29877 2.13095 6.29877C1.00281 6.29883 0.273223 5.7944 0.178237 5.02927ZM3.34236 2.27097C3.34236 1.37397 2.87719 0.799285 2.11824 0.799285C1.35144 0.799285 0.920546 1.374 0.920546 2.27097C0.920546 3.16843 1.35144 3.74314 2.11824 3.74314C2.8821 3.74313 3.34236 3.17283 3.34236 2.27097Z" />
                      </svg>
                    </CSSTransition>
                    <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['vector']?.animationClass || {}}>
                      <svg id="id_fivefivenigth_twoozeroonenigth" className="vector  " style={{}} onClick={ props.VectoronClick } onMouseEnter={ props.VectoronMouseEnter } onMouseOver={ props.VectoronMouseOver } onKeyPress={ props.VectoronKeyPress } onDrag={ props.VectoronDrag } onMouseLeave={ props.VectoronMouseLeave } onMouseUp={ props.VectoronMouseUp } onMouseDown={ props.VectoronMouseDown } onKeyDown={ props.VectoronKeyDown } onChange={ props.VectoronChange } ondelay={ props.Vectorondelay } width="3.864013671875" height="4.595458984375">
                        <path d="M3.86401 4.50293L3.00516 4.50293L3.00516 3.7876L2.93465 3.7876C2.82049 4.04719 2.6273 4.26437 2.38245 4.40835C2.13761 4.55234 1.85353 4.61584 1.57048 4.58985C1.35207 4.60531 1.13296 4.5718 0.929227 4.49179C0.725495 4.41178 0.542308 4.2873 0.393119 4.12748C0.24393 3.96766 0.13252 3.77655 0.0670644 3.56819C0.00160869 3.35982 -0.0162297 3.13949 0.0148552 2.92335L0.0148552 0L0.907498 0L0.907498 2.69189C0.907498 3.41943 1.20132 3.76709 1.85644 3.76709C2.00835 3.78146 2.16154 3.76184 2.30487 3.70966C2.4482 3.65748 2.57805 3.57404 2.68493 3.46545C2.79181 3.35686 2.87303 3.22584 2.92267 3.08197C2.97231 2.93809 2.98912 2.78497 2.97186 2.63379L2.97186 0L3.86401 0L3.86401 4.50293Z" />
                      </svg>
                    </CSSTransition>
                    <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['vector']?.animationClass || {}}>
                      <svg id="id_fivefivenigth_twoozerotwoozero" className="vector  " style={{}} onClick={ props.VectoronClick } onMouseEnter={ props.VectoronMouseEnter } onMouseOver={ props.VectoronMouseOver } onKeyPress={ props.VectoronKeyPress } onDrag={ props.VectoronDrag } onMouseLeave={ props.VectoronMouseLeave } onMouseUp={ props.VectoronMouseUp } onMouseDown={ props.VectoronMouseDown } onKeyDown={ props.VectoronKeyDown } onChange={ props.VectoronChange } ondelay={ props.Vectorondelay } width="4.11572265625" height="4.6875">
                        <path d="M4.06182 3.37411C3.94075 3.78565 3.67841 4.14163 3.32072 4.37978C2.96303 4.61793 2.53276 4.72308 2.10518 4.67684C1.80768 4.68467 1.51203 4.62785 1.23877 4.51032C0.9655 4.39279 0.721155 4.21737 0.522689 3.99623C0.324224 3.77509 0.176396 3.51353 0.0894621 3.22971C0.00252803 2.94589 -0.0214278 2.6466 0.0192582 2.35262C-0.0203576 2.05773 0.00402539 1.7578 0.0907551 1.47313C0.177485 1.18845 0.324535 0.925686 0.521955 0.702613C0.719375 0.47954 0.962552 0.301368 1.23503 0.180161C1.5075 0.0589531 1.80291 -0.00246148 2.10125 7.55155e-05C3.3577 7.55155e-05 4.11568 0.856076 4.11568 2.27008L4.11568 2.58018L0.92705 2.58018L0.92705 2.62998C0.913096 2.79522 0.93392 2.96155 0.988195 3.1183C1.04247 3.27505 1.12899 3.41876 1.24222 3.54022C1.35544 3.66167 1.49287 3.75819 1.64569 3.82359C1.79851 3.88899 1.96335 3.92182 2.12964 3.91998C2.34281 3.9455 2.55877 3.90722 2.75007 3.81001C2.94137 3.7128 3.09935 3.56106 3.20393 3.37408L4.06182 3.37411ZM0.92705 1.92294L3.20784 1.92294C3.21908 1.77181 3.19848 1.62001 3.14738 1.47729C3.09628 1.33457 3.01581 1.20409 2.91113 1.09423C2.80645 0.984377 2.67988 0.897572 2.53956 0.839397C2.39923 0.781221 2.24826 0.752962 2.09633 0.756436C1.9422 0.754502 1.78924 0.783358 1.64646 0.841301C1.50369 0.899245 1.374 0.985104 1.26502 1.09382C1.15605 1.20253 1.07001 1.3319 1.01196 1.4743C0.953913 1.61669 0.925046 1.76924 0.92705 1.92294Z" />
                      </svg>
                    </CSSTransition>
                    <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['vector']?.animationClass || {}}>
                      <svg id="id_fivefivenigth_twoozerotwooone" className="vector  " style={{}} onClick={ props.VectoronClick } onMouseEnter={ props.VectoronMouseEnter } onMouseOver={ props.VectoronMouseOver } onKeyPress={ props.VectoronKeyPress } onDrag={ props.VectoronDrag } onMouseLeave={ props.VectoronMouseLeave } onMouseUp={ props.VectoronMouseUp } onMouseDown={ props.VectoronMouseDown } onKeyDown={ props.VectoronKeyDown } onChange={ props.VectoronChange } ondelay={ props.Vectorondelay } width="0.89111328125" height="6.2607421875">
                        <path d="M0 0L0.891159 0L0.891159 6.26074L0 6.26074L0 0Z" />
                      </svg>
                    </CSSTransition>
                    <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['vector']?.animationClass || {}}>
                      <svg id="id_fivefivenigth_twoozerotwootwoo" className="vector  " style={{}} onClick={ props.VectoronClick } onMouseEnter={ props.VectoronMouseEnter } onMouseOver={ props.VectoronMouseOver } onKeyPress={ props.VectoronKeyPress } onDrag={ props.VectoronDrag } onMouseLeave={ props.VectoronMouseLeave } onMouseUp={ props.VectoronMouseUp } onMouseDown={ props.VectoronMouseDown } onKeyDown={ props.VectoronKeyDown } onChange={ props.VectoronChange } ondelay={ props.Vectorondelay } width="4.279052734375" height="4.6767578125">
                        <path d="M0.0100738 2.33638C-0.0183147 2.04046 0.0156295 1.74189 0.109733 1.45982C0.203837 1.17775 0.356022 0.918411 0.556525 0.698429C0.757029 0.478447 1.00143 0.302681 1.27404 0.182405C1.54666 0.06213 1.84147 0 2.13957 0C2.43767 0 2.73249 0.06213 3.0051 0.182405C3.27771 0.302681 3.52211 0.478447 3.72262 0.698429C3.92312 0.918411 4.0753 1.17775 4.16941 1.45982C4.26351 1.74189 4.29746 2.04046 4.26907 2.33638C4.29799 2.6326 4.26446 2.93158 4.1706 3.21411C4.07675 3.49663 3.92465 3.75645 3.7241 3.97686C3.52354 4.19728 3.27897 4.37341 3.00608 4.49395C2.73319 4.61448 2.43803 4.67675 2.13957 4.67675C1.84111 4.67675 1.54596 4.61448 1.27307 4.49395C1.00018 4.37341 0.755596 4.19728 0.555041 3.97686C0.354487 3.75645 0.202395 3.49663 0.10854 3.21411C0.0146849 2.93158 -0.0188535 2.6326 0.0100738 2.33638ZM3.35243 2.33638C3.35243 1.36031 2.91272 0.789509 2.14104 0.789509C1.36641 0.789509 0.93065 1.36031 0.93065 2.33639C0.93065 3.32028 1.36644 3.88668 2.14104 3.88668C2.91274 3.88667 3.35243 3.31636 3.35243 2.33638Z" />
                      </svg>
                    </CSSTransition>
                    <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['vector']?.animationClass || {}}>
                      <svg id="id_fivefivenigth_twoozerotwoothree" className="vector  " style={{}} onClick={ props.VectoronClick } onMouseEnter={ props.VectoronMouseEnter } onMouseOver={ props.VectoronMouseOver } onKeyPress={ props.VectoronKeyPress } onDrag={ props.VectoronDrag } onMouseLeave={ props.VectoronMouseLeave } onMouseUp={ props.VectoronMouseUp } onMouseDown={ props.VectoronMouseDown } onKeyDown={ props.VectoronKeyDown } onChange={ props.VectoronChange } ondelay={ props.Vectorondelay } width="4.11572265625" height="4.6875">
                        <path d="M4.06182 3.37411C3.94075 3.78565 3.67842 4.14163 3.32073 4.37978C2.96304 4.61793 2.53276 4.72308 2.10518 4.67684C1.80768 4.68467 1.51204 4.62785 1.23877 4.51032C0.965507 4.39279 0.721155 4.21737 0.522689 3.99623C0.324224 3.77509 0.176396 3.51353 0.0894621 3.22971C0.00252803 2.94589 -0.0214278 2.6466 0.0192582 2.35262C-0.0203576 2.05773 0.00402539 1.7578 0.0907551 1.47313C0.177485 1.18845 0.324535 0.925686 0.521955 0.702613C0.719375 0.47954 0.962552 0.301368 1.23503 0.180161C1.5075 0.0589531 1.80291 -0.00246148 2.10126 7.55155e-05C3.35771 7.55155e-05 4.11569 0.856076 4.11569 2.27008L4.11569 2.58018L0.927081 2.58018L0.927081 2.62998C0.913126 2.79522 0.933951 2.96155 0.988226 3.1183C1.0425 3.27505 1.12903 3.41876 1.24226 3.54022C1.35548 3.66167 1.49291 3.75819 1.64573 3.82359C1.79855 3.88899 1.96338 3.92182 2.12967 3.91998C2.34284 3.9455 2.5588 3.90722 2.7501 3.81001C2.9414 3.7128 3.09938 3.56106 3.20396 3.37408L4.06182 3.37411ZM0.92705 1.92294L3.20792 1.92294C3.21915 1.77181 3.19855 1.62001 3.14745 1.47729C3.09635 1.33457 3.01588 1.20409 2.9112 1.09423C2.80653 0.984377 2.67996 0.897572 2.53963 0.839397C2.39931 0.781221 2.24833 0.752962 2.0964 0.756436C1.94227 0.754497 1.7893 0.783349 1.64652 0.84129C1.50375 0.899231 1.37405 0.985088 1.26507 1.0938C1.15609 1.20252 1.07005 1.33189 1.012 1.47429C0.953948 1.61669 0.925046 1.76923 0.92705 1.92294Z" />
                      </svg>
                    </CSSTransition>
                    <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['vector']?.animationClass || {}}>
                      <svg id="id_fivefivenigth_twoozerotwoofour" className="vector  " style={{}} onClick={ props.VectoronClick } onMouseEnter={ props.VectoronMouseEnter } onMouseOver={ props.VectoronMouseOver } onKeyPress={ props.VectoronKeyPress } onDrag={ props.VectoronDrag } onMouseLeave={ props.VectoronMouseLeave } onMouseUp={ props.VectoronMouseUp } onMouseDown={ props.VectoronMouseDown } onKeyDown={ props.VectoronKeyDown } onChange={ props.VectoronChange } ondelay={ props.Vectorondelay } width="3.850830078125" height="4.59423828125">
                        <path d="M0 0.0913533L0.85787 0.0913533L0.85787 0.806682L0.924471 0.806682C1.03744 0.549762 1.22797 0.33439 1.46952 0.190584C1.71107 0.0467781 1.9916 -0.0182962 2.27199 0.00443254C2.49171 -0.0120404 2.71232 0.0209952 2.91749 0.101091C3.12266 0.181187 3.30714 0.306292 3.45724 0.467135C3.60734 0.627979 3.71923 0.82044 3.78462 1.03026C3.85 1.24008 3.8672 1.46188 3.83495 1.67923L3.83495 4.59423L2.94379 4.59423L2.94379 1.90239C2.94379 1.17876 2.62846 0.818893 1.96938 0.818893C1.82021 0.811965 1.67129 0.837283 1.53285 0.893113C1.3944 0.948943 1.2697 1.03397 1.16729 1.14235C1.06488 1.25074 0.987181 1.37992 0.93952 1.52105C0.891859 1.66218 0.875366 1.81192 0.891167 1.96L0.891167 4.59428L0 4.59428L0 0.0913533Z" />
                      </svg>
                    </CSSTransition>
                    <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['vector']?.animationClass || {}}>
                      <svg id="id_fivefivenigth_twoozerotwoofive" className="vector  " style={{}} onClick={ props.VectoronClick } onMouseEnter={ props.VectoronMouseEnter } onMouseOver={ props.VectoronMouseOver } onKeyPress={ props.VectoronKeyPress } onDrag={ props.VectoronDrag } onMouseLeave={ props.VectoronMouseLeave } onMouseUp={ props.VectoronMouseUp } onMouseDown={ props.VectoronMouseDown } onKeyDown={ props.VectoronKeyDown } onChange={ props.VectoronChange } ondelay={ props.Vectorondelay } width="4.11572265625" height="4.6875">
                        <path d="M4.06182 3.37411C3.94075 3.78565 3.67842 4.14163 3.32073 4.37978C2.96304 4.61793 2.53276 4.72308 2.10518 4.67684C1.80768 4.68467 1.51204 4.62785 1.23877 4.51032C0.965507 4.39279 0.721163 4.21737 0.522697 3.99623C0.324231 3.77509 0.176396 3.51353 0.0894621 3.22971C0.00252803 2.94589 -0.0214278 2.6466 0.0192582 2.35262C-0.0203576 2.05773 0.00402539 1.7578 0.0907551 1.47313C0.177485 1.18845 0.324535 0.925686 0.521955 0.702613C0.719375 0.47954 0.962559 0.301368 1.23503 0.180161C1.50751 0.0589531 1.80291 -0.00246148 2.10126 7.55155e-05C3.35771 7.55155e-05 4.11569 0.856076 4.11569 2.27008L4.11569 2.58018L0.927081 2.58018L0.927081 2.62998C0.913126 2.79522 0.933959 2.96155 0.988234 3.1183C1.04251 3.27505 1.12903 3.41876 1.24226 3.54022C1.35548 3.66167 1.49291 3.75819 1.64573 3.82359C1.79855 3.88899 1.96338 3.92182 2.12967 3.91998C2.34284 3.9455 2.5588 3.90722 2.7501 3.81001C2.9414 3.7128 3.09938 3.56106 3.20396 3.37408L4.06182 3.37411ZM0.92705 1.92294L3.20785 1.92294C3.21908 1.77181 3.19848 1.62001 3.14738 1.47729C3.09628 1.33457 3.01581 1.20409 2.91114 1.09423C2.80646 0.984377 2.67989 0.897572 2.53957 0.839397C2.39924 0.781221 2.24826 0.752962 2.09633 0.756436C1.9422 0.754506 1.78925 0.783364 1.64648 0.84131C1.50371 0.899255 1.37402 0.985115 1.26505 1.09383C1.15608 1.20254 1.07003 1.33191 1.01199 1.4743C0.953945 1.6167 0.925046 1.76924 0.92705 1.92294Z" />
                      </svg>
                    </CSSTransition>
                    <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['vector']?.animationClass || {}}>
                      <svg id="id_fivefivenigth_twoozerotwoosix" className="vector  " style={{}} onClick={ props.VectoronClick } onMouseEnter={ props.VectoronMouseEnter } onMouseOver={ props.VectoronMouseOver } onKeyPress={ props.VectoronKeyPress } onDrag={ props.VectoronDrag } onMouseLeave={ props.VectoronMouseLeave } onMouseUp={ props.VectoronMouseUp } onMouseDown={ props.VectoronMouseDown } onKeyDown={ props.VectoronKeyDown } onChange={ props.VectoronChange } ondelay={ props.Vectorondelay } width="0.89111328125" height="6.2607421875">
                        <path d="M0 0L0.891167 0L0.891167 6.26074L0 6.26074L0 0Z" />
                      </svg>
                    </CSSTransition>
                  </div>

                </CSSTransition>
              </div>

            </CSSTransition>
          </div>

        </CSSTransition>

      </>
      }
    </div>

  </CSSTransition>
</>
    ) 
}

Framefourzerofive.propTypes = {
    style: PropTypes.any,
FramefourzerofiveonClick: PropTypes.any,
FramefourzerofiveonMouseEnter: PropTypes.any,
FramefourzerofiveonMouseOver: PropTypes.any,
FramefourzerofiveonKeyPress: PropTypes.any,
FramefourzerofiveonDrag: PropTypes.any,
FramefourzerofiveonMouseLeave: PropTypes.any,
FramefourzerofiveonMouseUp: PropTypes.any,
FramefourzerofiveonMouseDown: PropTypes.any,
FramefourzerofiveonKeyDown: PropTypes.any,
FramefourzerofiveonChange: PropTypes.any,
Framefourzerofiveondelay: PropTypes.any,
GrouponClick: PropTypes.any,
GrouponMouseEnter: PropTypes.any,
GrouponMouseOver: PropTypes.any,
GrouponKeyPress: PropTypes.any,
GrouponDrag: PropTypes.any,
GrouponMouseLeave: PropTypes.any,
GrouponMouseUp: PropTypes.any,
GrouponMouseDown: PropTypes.any,
GrouponKeyDown: PropTypes.any,
GrouponChange: PropTypes.any,
Groupondelay: PropTypes.any,
VectoronClick: PropTypes.any,
VectoronMouseEnter: PropTypes.any,
VectoronMouseOver: PropTypes.any,
VectoronKeyPress: PropTypes.any,
VectoronDrag: PropTypes.any,
VectoronMouseLeave: PropTypes.any,
VectoronMouseUp: PropTypes.any,
VectoronMouseDown: PropTypes.any,
VectoronKeyDown: PropTypes.any,
VectoronChange: PropTypes.any,
Vectorondelay: PropTypes.any,
PathonClick: PropTypes.any,
PathonMouseEnter: PropTypes.any,
PathonMouseOver: PropTypes.any,
PathonKeyPress: PropTypes.any,
PathonDrag: PropTypes.any,
PathonMouseLeave: PropTypes.any,
PathonMouseUp: PropTypes.any,
PathonMouseDown: PropTypes.any,
PathonKeyDown: PropTypes.any,
PathonChange: PropTypes.any,
Pathondelay: PropTypes.any
}
export default Framefourzerofive;