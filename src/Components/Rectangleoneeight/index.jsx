import React, { Suspense } from 'react';
import PropTypes from 'prop-types';
import * as Contexts  from 'Contexts';
import { OverlaySwapContext } from 'Components/OverlaySwapProvider';
import CorebooksAnimation from 'Components/CorebooksAnimation';
import CorebooksAnimationSingle from 'Components/CorebooksAnimationSingle';

import * as UtilsScripts  from 'utils/utils';
import { Route, useHistory, useParams } from "react-router-dom";
import SingletoneNavigation from 'Class/SingletoneNavigation';

import LoadingComponent from "Components/Loading";


import { Transition } from "react-transition-group";
import { CSSTransition } from "react-transition-group";
import { useAppContext, useSessionContext } from 'context/AppContext';
import './Rectangleoneeight.css'





const Rectangleoneeight = (props)=>{
    const [state,dispatch] = useAppContext();
    const [, setNameSelectedComponent] = React.useContext(OverlaySwapContext);
    
    const nodeRef = React.useRef(null);
    const history = useHistory();
    let RouterContext = useParams();
    
    
    
    
    
    
    const [globalStateForm, dispatchForm] = React.useState({});
    
    const [ _in, setIn ] = React.useState(false)
    const [ transaction, setTransaction ] = React.useState({}) 
    const [ animation, setAnimation ] = React.useState('')

    
    
    
    
    
    
    React.useEffect(()=>{
        
        
        
    },[]);
    
    
    return (
        <>
  <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['rectangleoneeight']?.animationClass || {}}>

    <div id="id_fivefivenigth_twoozeroseveneight" ref={nodeRef} className={` ${ props.onClick ? 'cursor' : '' } rectangleoneeight ${ props.cssClass } ${ transaction['rectangleoneeight']?.type ? transaction['rectangleoneeight']?.type.toLowerCase() : '' }`} style={ { ...{ ...{}, transitionDuration: transaction['rectangleoneeight']?.duration, transitionTimingFunction: transaction['rectangleoneeight']?.timingFunction }, ...props.style }} onClick={ props.RectangleoneeightonClick } onMouseEnter={ props.RectangleoneeightonMouseEnter } onMouseOver={ props.RectangleoneeightonMouseOver } onKeyPress={ props.RectangleoneeightonKeyPress } onDrag={ props.RectangleoneeightonDrag } onMouseLeave={ props.RectangleoneeightonMouseLeave } onMouseUp={ props.RectangleoneeightonMouseUp } onMouseDown={ props.RectangleoneeightonMouseDown } onKeyDown={ props.RectangleoneeightonKeyDown } onChange={ props.RectangleoneeightonChange } ondelay={ props.Rectangleoneeightondelay }>
      {
      props.children ?
      props.children :
      <>


      </>
      }
    </div>

  </CSSTransition>
</>
    ) 
}

Rectangleoneeight.propTypes = {
    style: PropTypes.any,
RectangleoneeightonClick: PropTypes.any,
RectangleoneeightonMouseEnter: PropTypes.any,
RectangleoneeightonMouseOver: PropTypes.any,
RectangleoneeightonKeyPress: PropTypes.any,
RectangleoneeightonDrag: PropTypes.any,
RectangleoneeightonMouseLeave: PropTypes.any,
RectangleoneeightonMouseUp: PropTypes.any,
RectangleoneeightonMouseDown: PropTypes.any,
RectangleoneeightonKeyDown: PropTypes.any,
RectangleoneeightonChange: PropTypes.any,
Rectangleoneeightondelay: PropTypes.any
}
export default Rectangleoneeight;