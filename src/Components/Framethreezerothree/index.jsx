import React, { Suspense } from 'react';
import PropTypes from 'prop-types';
import * as Contexts  from 'Contexts';
import { OverlaySwapContext } from 'Components/OverlaySwapProvider';
import CorebooksAnimation from 'Components/CorebooksAnimation';
import CorebooksAnimationSingle from 'Components/CorebooksAnimationSingle';

import * as UtilsScripts  from 'utils/utils';
import { Route, useHistory, useParams } from "react-router-dom";
import SingletoneNavigation from 'Class/SingletoneNavigation';

import LoadingComponent from "Components/Loading";


import { Transition } from "react-transition-group";
import { CSSTransition } from "react-transition-group";
import { useAppContext, useSessionContext } from 'context/AppContext';
import './Framethreezerothree.css'





const Framethreezerothree = (props)=>{
    const [state,dispatch] = useAppContext();
    const [, setNameSelectedComponent] = React.useContext(OverlaySwapContext);
    
    const nodeRef = React.useRef(null);
    const history = useHistory();
    let RouterContext = useParams();
    
    
    
    
    
    
    const [globalStateForm, dispatchForm] = React.useState({});
    
    const [ _in, setIn ] = React.useState(false)
    const [ transaction, setTransaction ] = React.useState({}) 
    const [ animation, setAnimation ] = React.useState('')

    
    
    
    
    
    
    React.useEffect(()=>{
        
        
        
    },[]);
    
    
    return (
        <>
  <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['framethreezerothree']?.animationClass || {}}>

    <div id="id_threeonefour_onefiveonesix" ref={nodeRef} className={` ${ props.onClick ? 'cursor' : '' } framethreezerothree C_threeonefour_onefiveonesix ${ props.cssClass } ${ transaction['framethreezerothree']?.type ? transaction['framethreezerothree']?.type.toLowerCase() : '' }`} style={ { ...{ ...{}, transitionDuration: transaction['framethreezerothree']?.duration, transitionTimingFunction: transaction['framethreezerothree']?.timingFunction }, ...props.style }} onClick={ props.FramethreezerothreeonClick } onMouseEnter={ props.FramethreezerothreeonMouseEnter } onMouseOver={ props.FramethreezerothreeonMouseOver } onKeyPress={ props.FramethreezerothreeonKeyPress } onDrag={ props.FramethreezerothreeonDrag } onMouseLeave={ props.FramethreezerothreeonMouseLeave } onMouseUp={ props.FramethreezerothreeonMouseUp } onMouseDown={ props.FramethreezerothreeonMouseDown } onKeyDown={ props.FramethreezerothreeonKeyDown } onChange={ props.FramethreezerothreeonChange } ondelay={ props.Framethreezerothreeondelay }>
      {
      props.children ?
      props.children :
      <>
        <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['frame']?.animationClass || {}}>

          <div id="id_threeonefour_onefiveoneone" className={` frame frame ${ props.onClick ? 'cursor' : '' } ${ transaction['frame']?.type ? transaction['frame']?.type.toLowerCase() : '' }`} style={ { ...{}, ...props.FrameStyle , transitionDuration: transaction['frame']?.duration, transitionTimingFunction: transaction['frame']?.timingFunction } } onClick={ props.FrameonClick } onMouseEnter={ props.FrameonMouseEnter } onMouseOver={ props.FrameonMouseOver } onKeyPress={ props.FrameonKeyPress } onDrag={ props.FrameonDrag } onMouseLeave={ props.FrameonMouseLeave } onMouseUp={ props.FrameonMouseUp } onMouseDown={ props.FrameonMouseDown } onKeyDown={ props.FrameonKeyDown } onChange={ props.FrameonChange } ondelay={ props.Frameondelay }>
            <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['vector']?.animationClass || {}}>
              <svg id="id_threeonefour_onefiveonetwoo" className="vector  " style={{}} onClick={ props.VectoronClick } onMouseEnter={ props.VectoronMouseEnter } onMouseOver={ props.VectoronMouseOver } onKeyPress={ props.VectoronKeyPress } onDrag={ props.VectoronDrag } onMouseLeave={ props.VectoronMouseLeave } onMouseUp={ props.VectoronMouseUp } onMouseDown={ props.VectoronMouseDown } onKeyDown={ props.VectoronKeyDown } onChange={ props.VectoronChange } ondelay={ props.Vectorondelay } width="17.669921875" height="14.1400146484375">

              </svg>
            </CSSTransition>
          </div>

        </CSSTransition>
        <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['escuchamsicasinanuncios']?.animationClass || {}}>

          <span id="id_threeonefour_onefiveonethree"  className={` text escuchamsicasinanuncios    ${ props.onClick ? 'cursor' : ''}  ${ transaction['escuchamsicasinanuncios']?.type ? transaction['escuchamsicasinanuncios']?.type.toLowerCase() : '' }`} style={{ ...{},  ...props.EscuchamsicasinanunciosStyle , transitionDuration: transaction['escuchamsicasinanuncios']?.duration, transitionTimingFunction: transaction['escuchamsicasinanuncios']?.timingFunction }} onClick={ props.EscuchamsicasinanunciosonClick } onMouseEnter={ props.EscuchamsicasinanunciosonMouseEnter } onMouseOver={ props.EscuchamsicasinanunciosonMouseOver } onKeyPress={ props.EscuchamsicasinanunciosonKeyPress } onDrag={ props.EscuchamsicasinanunciosonDrag } onMouseLeave={ props.EscuchamsicasinanunciosonMouseLeave } onMouseUp={ props.EscuchamsicasinanunciosonMouseUp } onMouseDown={ props.EscuchamsicasinanunciosonMouseDown } onKeyDown={ props.EscuchamsicasinanunciosonKeyDown } onChange={ props.EscuchamsicasinanunciosonChange } ondelay={ props.Escuchamsicasinanunciosondelay } >{props.Escuchamsicasinanuncios0 || `Escucha música sin anuncios`}</span>

        </CSSTransition>

      </>
      }
    </div>

  </CSSTransition>
</>
    ) 
}

Framethreezerothree.propTypes = {
    style: PropTypes.any,
Escuchamsicasinanuncios0: PropTypes.any,
FramethreezerothreeonClick: PropTypes.any,
FramethreezerothreeonMouseEnter: PropTypes.any,
FramethreezerothreeonMouseOver: PropTypes.any,
FramethreezerothreeonKeyPress: PropTypes.any,
FramethreezerothreeonDrag: PropTypes.any,
FramethreezerothreeonMouseLeave: PropTypes.any,
FramethreezerothreeonMouseUp: PropTypes.any,
FramethreezerothreeonMouseDown: PropTypes.any,
FramethreezerothreeonKeyDown: PropTypes.any,
FramethreezerothreeonChange: PropTypes.any,
Framethreezerothreeondelay: PropTypes.any,
FrameonClick: PropTypes.any,
FrameonMouseEnter: PropTypes.any,
FrameonMouseOver: PropTypes.any,
FrameonKeyPress: PropTypes.any,
FrameonDrag: PropTypes.any,
FrameonMouseLeave: PropTypes.any,
FrameonMouseUp: PropTypes.any,
FrameonMouseDown: PropTypes.any,
FrameonKeyDown: PropTypes.any,
FrameonChange: PropTypes.any,
Frameondelay: PropTypes.any,
VectoronClick: PropTypes.any,
VectoronMouseEnter: PropTypes.any,
VectoronMouseOver: PropTypes.any,
VectoronKeyPress: PropTypes.any,
VectoronDrag: PropTypes.any,
VectoronMouseLeave: PropTypes.any,
VectoronMouseUp: PropTypes.any,
VectoronMouseDown: PropTypes.any,
VectoronKeyDown: PropTypes.any,
VectoronChange: PropTypes.any,
Vectorondelay: PropTypes.any,
EscuchamsicasinanunciosonClick: PropTypes.any,
EscuchamsicasinanunciosonMouseEnter: PropTypes.any,
EscuchamsicasinanunciosonMouseOver: PropTypes.any,
EscuchamsicasinanunciosonKeyPress: PropTypes.any,
EscuchamsicasinanunciosonDrag: PropTypes.any,
EscuchamsicasinanunciosonMouseLeave: PropTypes.any,
EscuchamsicasinanunciosonMouseUp: PropTypes.any,
EscuchamsicasinanunciosonMouseDown: PropTypes.any,
EscuchamsicasinanunciosonKeyDown: PropTypes.any,
EscuchamsicasinanunciosonChange: PropTypes.any,
Escuchamsicasinanunciosondelay: PropTypes.any
}
export default Framethreezerothree;