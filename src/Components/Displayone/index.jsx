import React, { Suspense } from 'react';
import PropTypes from 'prop-types';
import * as Contexts  from 'Contexts';
import { OverlaySwapContext } from 'Components/OverlaySwapProvider';
import CorebooksAnimation from 'Components/CorebooksAnimation';
import CorebooksAnimationSingle from 'Components/CorebooksAnimationSingle';

import * as UtilsScripts  from 'utils/utils';
import { Route, useHistory, useParams } from "react-router-dom";
import SingletoneNavigation from 'Class/SingletoneNavigation';

import LoadingComponent from "Components/Loading";


import { Transition } from "react-transition-group";
import { CSSTransition } from "react-transition-group";
import { useAppContext, useSessionContext } from 'context/AppContext';
import './Displayone.css'





const Displayone = (props)=>{
    const [state,dispatch] = useAppContext();
    const [, setNameSelectedComponent] = React.useContext(OverlaySwapContext);
    
    const nodeRef = React.useRef(null);
    const history = useHistory();
    let RouterContext = useParams();
    
    
    
    
    
    
    const [globalStateForm, dispatchForm] = React.useState({});
    
    const [ _in, setIn ] = React.useState(false)
    const [ transaction, setTransaction ] = React.useState({}) 
    const [ animation, setAnimation ] = React.useState('')

    
    
    
    
    
    
    React.useEffect(()=>{
        
        
        
    },[]);
    
    
    return (
        <>
  <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['displayone']?.animationClass || {}}>

    <div id="id_sixtwoosix_onesixfivenigth" ref={nodeRef} className={` ${ props.onClick ? 'cursor' : '' } displayone C_sixtwoosix_onesixfivenigth ${ props.cssClass } ${ transaction['displayone']?.type ? transaction['displayone']?.type.toLowerCase() : '' }`} style={ { ...{ ...{}, transitionDuration: transaction['displayone']?.duration, transitionTimingFunction: transaction['displayone']?.timingFunction }, ...props.style }} onClick={ props.DisplayoneonClick } onMouseEnter={ props.DisplayoneonMouseEnter } onMouseOver={ props.DisplayoneonMouseOver } onKeyPress={ props.DisplayoneonKeyPress } onDrag={ props.DisplayoneonDrag } onMouseLeave={ props.DisplayoneonMouseLeave } onMouseUp={ props.DisplayoneonMouseUp } onMouseDown={ props.DisplayoneonMouseDown } onKeyDown={ props.DisplayoneonKeyDown } onChange={ props.DisplayoneonChange } ondelay={ props.Displayoneondelay }>
      {
      props.children ?
      props.children :
      <>
        <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['displayonetext']?.animationClass || {}}>

          <span id="id_sixtwoosix_onesixfiveeight"  className={` text displayonetext    ${ props.onClick ? 'cursor' : ''}  ${ transaction['displayonetext']?.type ? transaction['displayonetext']?.type.toLowerCase() : '' }`} style={{ ...{},  ...props.DisplayoneTextStyle , transitionDuration: transaction['displayonetext']?.duration, transitionTimingFunction: transaction['displayonetext']?.timingFunction }} onClick={ props.DisplayoneTextonClick } onMouseEnter={ props.DisplayoneTextonMouseEnter } onMouseOver={ props.DisplayoneTextonMouseOver } onKeyPress={ props.DisplayoneTextonKeyPress } onDrag={ props.DisplayoneTextonDrag } onMouseLeave={ props.DisplayoneTextonMouseLeave } onMouseUp={ props.DisplayoneTextonMouseUp } onMouseDown={ props.DisplayoneTextonMouseDown } onKeyDown={ props.DisplayoneTextonKeyDown } onChange={ props.DisplayoneTextonChange } ondelay={ props.DisplayoneTextondelay } >{props.DisplayoneText0 || `Display 1`}</span>

        </CSSTransition>

      </>
      }
    </div>

  </CSSTransition>
</>
    ) 
}

Displayone.propTypes = {
    style: PropTypes.any,
DisplayoneText0: PropTypes.any,
DisplayoneonClick: PropTypes.any,
DisplayoneonMouseEnter: PropTypes.any,
DisplayoneonMouseOver: PropTypes.any,
DisplayoneonKeyPress: PropTypes.any,
DisplayoneonDrag: PropTypes.any,
DisplayoneonMouseLeave: PropTypes.any,
DisplayoneonMouseUp: PropTypes.any,
DisplayoneonMouseDown: PropTypes.any,
DisplayoneonKeyDown: PropTypes.any,
DisplayoneonChange: PropTypes.any,
Displayoneondelay: PropTypes.any,
DisplayoneTextonClick: PropTypes.any,
DisplayoneTextonMouseEnter: PropTypes.any,
DisplayoneTextonMouseOver: PropTypes.any,
DisplayoneTextonKeyPress: PropTypes.any,
DisplayoneTextonDrag: PropTypes.any,
DisplayoneTextonMouseLeave: PropTypes.any,
DisplayoneTextonMouseUp: PropTypes.any,
DisplayoneTextonMouseDown: PropTypes.any,
DisplayoneTextonKeyDown: PropTypes.any,
DisplayoneTextonChange: PropTypes.any,
DisplayoneTextondelay: PropTypes.any
}
export default Displayone;