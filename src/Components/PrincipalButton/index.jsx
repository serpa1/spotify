import React, { Suspense } from 'react';
import PropTypes from 'prop-types';
import * as Contexts  from 'Contexts';
import { OverlaySwapContext } from 'Components/OverlaySwapProvider';
import CorebooksAnimation from 'Components/CorebooksAnimation';
import CorebooksAnimationSingle from 'Components/CorebooksAnimationSingle';

import * as UtilsScripts  from 'utils/utils';
import { Route, useHistory, useParams } from "react-router-dom";
import SingletoneNavigation from 'Class/SingletoneNavigation';

import LoadingComponent from "Components/Loading";


import { Transition } from "react-transition-group";
import { CSSTransition } from "react-transition-group";
import { useAppContext, useSessionContext } from 'context/AppContext';
import './PrincipalButton.css'





const PrincipalButton = (props)=>{
    const [state,dispatch] = useAppContext();
    const [, setNameSelectedComponent] = React.useContext(OverlaySwapContext);
    
    const nodeRefpropertyonedefault = React.useRef(null);
const nodeRefpropertyonevarianttwoo = React.useRef(null);
    const history = useHistory();
    let RouterContext = useParams();
    
    
    
    
    
    
    const [globalStateForm, dispatchForm] = React.useState({});
    const [ variant, setvariant] = React.useState(props.variant || 'Property 1=Default')
    const [ _in, setIn ] = React.useState(false)
    const [ transaction, setTransaction ] = React.useState({}) 
    const [ animation, setAnimation ] = React.useState('')

    
    
    
    
    
    
    React.useEffect(()=>{
        
        
        
    },[]);
    
    
    React.useEffect(()=>{
        
        setIn(false);
        setTimeout(()=>setIn(true))
           
      },[variant]);
    const switchVariant = (value)=>{
        switch (value) {
            case 'Property 1=Default':{
            return (
            <>

  <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['propertyonedefault']?.animationClass || {}}>

    <div id="id_onesixtwoo_fournigtheighttwoo" ref={nodeRefpropertyonedefault} className={` cursor ${ props.onClick ? 'cursor' : '' } principalbuttonpropertyonedefault C_onesixtwoo_fournigtheighttwoo ${ props.cssClass } ${ transaction['propertyonedefault']?.type ? transaction['propertyonedefault']?.type.toLowerCase() : '' }`} style={ { ...{ ...{"minWidth":"250px"}, transitionDuration: transaction['propertyonedefault']?.duration, transitionTimingFunction: transaction['propertyonedefault']?.timingFunction }, ...props.style }} onClick={ props.PrincipalButtononClick } onMouseEnter={ props.PrincipalButtononMouseEnter } onMouseOver={ props.PrincipalButtononMouseOver || function(e){ setTransaction({ }); setvariant('Property 1=Variant2'); }} onKeyPress={ props.PrincipalButtononKeyPress } onDrag={ props.PrincipalButtononDrag } onMouseLeave={ props.PrincipalButtononMouseLeave } onMouseUp={ props.PrincipalButtononMouseUp } onMouseDown={ props.PrincipalButtononMouseDown } onKeyDown={ props.PrincipalButtononKeyDown } onChange={ props.PrincipalButtononChange } ondelay={ props.PrincipalButtonondelay }>
      {
      props.children ?
      props.children :
      <>
        <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['principalbuttontext']?.animationClass || {}}>

          <span id="id_onesixtwoo_fournigtheightone"  className={` text principalbuttonprincipalbuttontext    ${ props.onClick ? 'cursor' : ''}  ${ transaction['principalbuttontext']?.type ? transaction['principalbuttontext']?.type.toLowerCase() : '' }`} style={{ ...{"height":"14px"},  ...props.PrincipalButtonTextStyle , transitionDuration: transaction['principalbuttontext']?.duration, transitionTimingFunction: transaction['principalbuttontext']?.timingFunction }} onClick={ props.PrincipalButtonTextonClick } onMouseEnter={ props.PrincipalButtonTextonMouseEnter } onMouseOver={ props.PrincipalButtonTextonMouseOver } onKeyPress={ props.PrincipalButtonTextonKeyPress } onDrag={ props.PrincipalButtonTextonDrag } onMouseLeave={ props.PrincipalButtonTextonMouseLeave } onMouseUp={ props.PrincipalButtonTextonMouseUp } onMouseDown={ props.PrincipalButtonTextonMouseDown } onKeyDown={ props.PrincipalButtonTextonKeyDown } onChange={ props.PrincipalButtonTextonChange } ondelay={ props.PrincipalButtonTextondelay } >{props.PrincipalButtonText0 || `Iniciar Sesión`}</span>

        </CSSTransition>

      </>
      }
    </div>

  </CSSTransition>

</>
            )
        }
case 'Property 1=Variant2':{
            return (
            <>

  <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['propertyonevarianttwoo']?.animationClass || {}}>

    <div id="id_onesixtwoo_fournigtheightfour" ref={nodeRefpropertyonevarianttwoo} className={` cursor ${ props.onClick ? 'cursor' : '' } principalbuttonpropertyonevarianttwoo C_onesixtwoo_fournigtheightfour ${ props.cssClass } ${ transaction['propertyonevarianttwoo']?.type ? transaction['propertyonevarianttwoo']?.type.toLowerCase() : '' }`} style={ { ...{ ...{"minWidth":"250px"}, transitionDuration: transaction['propertyonevarianttwoo']?.duration, transitionTimingFunction: transaction['propertyonevarianttwoo']?.timingFunction }, ...props.style }} onClick={ props.PrincipalButtononClick } onMouseEnter={ props.PrincipalButtononMouseEnter } onMouseOver={ props.PrincipalButtononMouseOver } onKeyPress={ props.PrincipalButtononKeyPress } onDrag={ props.PrincipalButtononDrag } onMouseLeave={ props.PrincipalButtononMouseLeave || function(e){ setTransaction({ }); setvariant('Property 1=Default'); }} onMouseUp={ props.PrincipalButtononMouseUp } onMouseDown={ props.PrincipalButtononMouseDown } onKeyDown={ props.PrincipalButtononKeyDown } onChange={ props.PrincipalButtononChange } ondelay={ props.PrincipalButtonondelay }>
      {
      props.children ?
      props.children :
      <>
        <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['principalbuttontext']?.animationClass || {}}>

          <span id="id_onesixtwoo_fournigtheightfive"  className={` text principalbuttonprincipalbuttontext    ${ props.onClick ? 'cursor' : ''}  ${ transaction['principalbuttontext']?.type ? transaction['principalbuttontext']?.type.toLowerCase() : '' }`} style={{ ...{"height":"14px"},  ...props.PrincipalButtonTextStyle , transitionDuration: transaction['principalbuttontext']?.duration, transitionTimingFunction: transaction['principalbuttontext']?.timingFunction }} onClick={ props.PrincipalButtonTextonClick } onMouseEnter={ props.PrincipalButtonTextonMouseEnter } onMouseOver={ props.PrincipalButtonTextonMouseOver } onKeyPress={ props.PrincipalButtonTextonKeyPress } onDrag={ props.PrincipalButtonTextonDrag } onMouseLeave={ props.PrincipalButtonTextonMouseLeave } onMouseUp={ props.PrincipalButtonTextonMouseUp } onMouseDown={ props.PrincipalButtonTextonMouseDown } onKeyDown={ props.PrincipalButtonTextonKeyDown } onChange={ props.PrincipalButtonTextonChange } ondelay={ props.PrincipalButtonTextondelay } >{props.PrincipalButtonText0 || `Iniciar Sesión`}</span>

        </CSSTransition>

      </>
      }
    </div>

  </CSSTransition>

</>
            )
        }
        }
    }

    return (
        <>
            {switchVariant(variant)}
        </>
    )
    
}

PrincipalButton.propTypes = {
    style: PropTypes.any,
variant: PropTypes.any,
PrincipalButtonText0: PropTypes.any,
PrincipalButtononClick: PropTypes.any,
PrincipalButtononMouseEnter: PropTypes.any,
PrincipalButtononMouseOver: PropTypes.any,
PrincipalButtononKeyPress: PropTypes.any,
PrincipalButtononDrag: PropTypes.any,
PrincipalButtononMouseLeave: PropTypes.any,
PrincipalButtononMouseUp: PropTypes.any,
PrincipalButtononMouseDown: PropTypes.any,
PrincipalButtononKeyDown: PropTypes.any,
PrincipalButtononChange: PropTypes.any,
PrincipalButtonondelay: PropTypes.any,
PrincipalButtonTextonClick: PropTypes.any,
PrincipalButtonTextonMouseEnter: PropTypes.any,
PrincipalButtonTextonMouseOver: PropTypes.any,
PrincipalButtonTextonKeyPress: PropTypes.any,
PrincipalButtonTextonDrag: PropTypes.any,
PrincipalButtonTextonMouseLeave: PropTypes.any,
PrincipalButtonTextonMouseUp: PropTypes.any,
PrincipalButtonTextonMouseDown: PropTypes.any,
PrincipalButtonTextonKeyDown: PropTypes.any,
PrincipalButtonTextonChange: PropTypes.any,
PrincipalButtonTextondelay: PropTypes.any
}
export default PrincipalButton;