import React, { Suspense } from 'react';
import PropTypes from 'prop-types';
import * as Contexts  from 'Contexts';
import { OverlaySwapContext } from 'Components/OverlaySwapProvider';
import CorebooksAnimation from 'Components/CorebooksAnimation';
import CorebooksAnimationSingle from 'Components/CorebooksAnimationSingle';

import * as UtilsScripts  from 'utils/utils';
import { Route, useHistory, useParams } from "react-router-dom";
import SingletoneNavigation from 'Class/SingletoneNavigation';

import LoadingComponent from "Components/Loading";


import { Transition } from "react-transition-group";
import { CSSTransition } from "react-transition-group";
import { useAppContext, useSessionContext } from 'context/AppContext';
import './ButtonGreenPropertyoneDefault.css'





const PropertyoneDefault = (props)=>{
    const [state,dispatch] = useAppContext();
    const [, setNameSelectedComponent] = React.useContext(OverlaySwapContext);
    
    const nodeRef = React.useRef(null);
    const history = useHistory();
    let RouterContext = useParams();
    
    
    
    
    
    
    const [globalStateForm, dispatchForm] = React.useState({});
    
    const [ _in, setIn ] = React.useState(false)
    const [ transaction, setTransaction ] = React.useState({}) 
    const [ animation, setAnimation ] = React.useState('')

    
    
    
    
    
    
    React.useEffect(()=>{
        
        
        
    },[]);
    
    
    return (
        <>
  <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['propertyonedefault']?.animationClass || {}}>

    <div id="id_fourthreefour_oneonesixthree" ref={nodeRef} className={` cursor ${ props.onClick ? 'cursor' : '' } propertyonedefault C_fourthreefour_oneonesixthree ${ props.cssClass } ${ transaction['propertyonedefault']?.type ? transaction['propertyonedefault']?.type.toLowerCase() : '' }`} style={ { ...{ ...{"minWidth":"250px"}, transitionDuration: transaction['propertyonedefault']?.duration, transitionTimingFunction: transaction['propertyonedefault']?.timingFunction }, ...props.style }} onClick={ props.ButtonGreenonClick } onMouseEnter={ props.ButtonGreenonMouseEnter } onMouseOver={ props.ButtonGreenonMouseOver || function(e){ setTransaction({ }); setvariant('Property 1=Variant2'); }} onKeyPress={ props.ButtonGreenonKeyPress } onDrag={ props.ButtonGreenonDrag } onMouseLeave={ props.ButtonGreenonMouseLeave } onMouseUp={ props.ButtonGreenonMouseUp } onMouseDown={ props.ButtonGreenonMouseDown } onKeyDown={ props.ButtonGreenonKeyDown } onChange={ props.ButtonGreenonChange } ondelay={ props.ButtonGreenondelay }>
      {
      props.children ?
      props.children :
      <>
        <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['principalbuttontext']?.animationClass || {}}>

          <span id="id_fourthreefour_oneonesixtwoo"  className={` text principalbuttontext    ${ props.onClick ? 'cursor' : ''}  ${ transaction['principalbuttontext']?.type ? transaction['principalbuttontext']?.type.toLowerCase() : '' }`} style={{ ...{"height":"14px"},  ...props.PrincipalButtonTextStyle , transitionDuration: transaction['principalbuttontext']?.duration, transitionTimingFunction: transaction['principalbuttontext']?.timingFunction }} onClick={ props.PrincipalButtonTextonClick } onMouseEnter={ props.PrincipalButtonTextonMouseEnter } onMouseOver={ props.PrincipalButtonTextonMouseOver } onKeyPress={ props.PrincipalButtonTextonKeyPress } onDrag={ props.PrincipalButtonTextonDrag } onMouseLeave={ props.PrincipalButtonTextonMouseLeave } onMouseUp={ props.PrincipalButtonTextonMouseUp } onMouseDown={ props.PrincipalButtonTextonMouseDown } onKeyDown={ props.PrincipalButtonTextonKeyDown } onChange={ props.PrincipalButtonTextonChange } ondelay={ props.PrincipalButtonTextondelay } >{props.PrincipalButtonText0 || `Ir a la Comunidad`}</span>

        </CSSTransition>

      </>
      }
    </div>

  </CSSTransition>
</>
    ) 
}

PropertyoneDefault.propTypes = {
    style: PropTypes.any,
PrincipalButtonText0: PropTypes.any,
ButtonGreenonClick: PropTypes.any,
ButtonGreenonMouseEnter: PropTypes.any,
ButtonGreenonMouseOver: PropTypes.any,
ButtonGreenonKeyPress: PropTypes.any,
ButtonGreenonDrag: PropTypes.any,
ButtonGreenonMouseLeave: PropTypes.any,
ButtonGreenonMouseUp: PropTypes.any,
ButtonGreenonMouseDown: PropTypes.any,
ButtonGreenonKeyDown: PropTypes.any,
ButtonGreenonChange: PropTypes.any,
ButtonGreenondelay: PropTypes.any,
PrincipalButtonTextonClick: PropTypes.any,
PrincipalButtonTextonMouseEnter: PropTypes.any,
PrincipalButtonTextonMouseOver: PropTypes.any,
PrincipalButtonTextonKeyPress: PropTypes.any,
PrincipalButtonTextonDrag: PropTypes.any,
PrincipalButtonTextonMouseLeave: PropTypes.any,
PrincipalButtonTextonMouseUp: PropTypes.any,
PrincipalButtonTextonMouseDown: PropTypes.any,
PrincipalButtonTextonKeyDown: PropTypes.any,
PrincipalButtonTextonChange: PropTypes.any,
PrincipalButtonTextondelay: PropTypes.any
}
export default PropertyoneDefault;