import React, { Suspense } from 'react';
import PropTypes from 'prop-types';
import * as Contexts  from 'Contexts';
import { OverlaySwapContext } from 'Components/OverlaySwapProvider';
import CorebooksAnimation from 'Components/CorebooksAnimation';
import CorebooksAnimationSingle from 'Components/CorebooksAnimationSingle';

import * as UtilsScripts  from 'utils/utils';
import { Route, useHistory, useParams } from "react-router-dom";
import SingletoneNavigation from 'Class/SingletoneNavigation';

import LoadingComponent from "Components/Loading";


import { Transition } from "react-transition-group";
import { CSSTransition } from "react-transition-group";
import { useAppContext, useSessionContext } from 'context/AppContext';
import './LanguageButtonPropertyoneVarianttwoo.css'





const PropertyoneVarianttwoo = (props)=>{
    const [state,dispatch] = useAppContext();
    const [, setNameSelectedComponent] = React.useContext(OverlaySwapContext);
    
    const nodeRef = React.useRef(null);
    const history = useHistory();
    let RouterContext = useParams();
    
    
    
    
    
    
    const [globalStateForm, dispatchForm] = React.useState({});
    
    const [ _in, setIn ] = React.useState(false)
    const [ transaction, setTransaction ] = React.useState({}) 
    const [ animation, setAnimation ] = React.useState('')

    
    
    
    
    
    
    React.useEffect(()=>{
        
        
        
    },[]);
    
    
    return (
        <>
  <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['propertyonevarianttwoo']?.animationClass || {}}>

    <div id="id_one_eightsixthree" ref={nodeRef} className={` cursor ${ props.onClick ? 'cursor' : '' } propertyonevarianttwoo C_one_eightsixthree ${ props.cssClass } ${ transaction['propertyonevarianttwoo']?.type ? transaction['propertyonevarianttwoo']?.type.toLowerCase() : '' }`} style={ { ...{ ...{}, transitionDuration: transaction['propertyonevarianttwoo']?.duration, transitionTimingFunction: transaction['propertyonevarianttwoo']?.timingFunction }, ...props.style }} onClick={ props.LanguageButtononClick } onMouseEnter={ props.LanguageButtononMouseEnter } onMouseOver={ props.LanguageButtononMouseOver } onKeyPress={ props.LanguageButtononKeyPress } onDrag={ props.LanguageButtononDrag } onMouseLeave={ props.LanguageButtononMouseLeave || function(e){ setTransaction({ }); setvariant('Property 1=Default'); }} onMouseUp={ props.LanguageButtononMouseUp } onMouseDown={ props.LanguageButtononMouseDown } onKeyDown={ props.LanguageButtononKeyDown } onChange={ props.LanguageButtononChange } ondelay={ props.LanguageButtonondelay }>
      {
      props.children ?
      props.children :
      <>
        <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['languageicon']?.animationClass || {}}>

          <div id="id_one_eightsixfour" className={` frame languageicon ${ props.onClick ? 'cursor' : '' } ${ transaction['languageicon']?.type ? transaction['languageicon']?.type.toLowerCase() : '' }`} style={ { ...{}, ...props.LanguageIconStyle , transitionDuration: transaction['languageicon']?.duration, transitionTimingFunction: transaction['languageicon']?.timingFunction } } onClick={ props.LanguageIcononClick } onMouseEnter={ props.LanguageIcononMouseEnter } onMouseOver={ props.LanguageIcononMouseOver } onKeyPress={ props.LanguageIcononKeyPress } onDrag={ props.LanguageIcononDrag } onMouseLeave={ props.LanguageIcononMouseLeave } onMouseUp={ props.LanguageIcononMouseUp } onMouseDown={ props.LanguageIcononMouseDown } onKeyDown={ props.LanguageIcononKeyDown } onChange={ props.LanguageIcononChange } ondelay={ props.LanguageIconondelay }>
            <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['vector']?.animationClass || {}}>
              <svg id="id_one_eightsixfive" className="vector  " style={{"width":"16px","height":"16px"}} onClick={ props.VectoronClick } onMouseEnter={ props.VectoronMouseEnter } onMouseOver={ props.VectoronMouseOver } onKeyPress={ props.VectoronKeyPress } onDrag={ props.VectoronDrag } onMouseLeave={ props.VectoronMouseLeave } onMouseUp={ props.VectoronMouseUp } onMouseDown={ props.VectoronMouseDown } onKeyDown={ props.VectoronKeyDown } onChange={ props.VectoronChange } ondelay={ props.Vectorondelay } width="16" height="15.9996337890625">
                <path d="M8.076 15.9996L7.924 15.9996C5.80895 15.9795 3.78793 15.1227 2.30298 13.6164C0.818043 12.1102 -0.00995669 10.0771 9.03774e-05 7.962C0.0101374 5.84687 0.857413 3.82181 2.3566 2.32973C3.85578 0.837647 5.88485 -4.996e-16 8 0C10.1151 -1.16573e-15 12.1442 0.837647 13.6434 2.32973C15.1426 3.82181 15.9899 5.84687 15.9999 7.962C16.01 10.0771 15.182 12.1102 13.697 13.6164C12.2121 15.1227 10.1911 15.9795 8.076 15.9996ZM7.666 1.79764C7.44 2.07064 7.203 2.51064 6.989 3.12064C6.62 4.17564 6.363 5.61664 6.302 7.24964L9.849 7.24964C9.789 5.61664 9.531 4.17564 9.162 3.12064C8.949 2.51064 8.712 2.07064 8.486 1.79764C8.292 1.56264 8.16 1.51264 8.101 1.50164L8.057 1.50164C8.002 1.50864 7.867 1.55364 7.666 1.79764ZM4.801 7.24964C4.863 5.47864 5.141 3.86364 5.574 2.62564C5.673 2.34164 5.782 2.07164 5.903 1.81964C4.72232 2.20675 3.67716 2.9238 2.89107 3.88604C2.10497 4.84828 1.61081 6.01547 1.467 7.24964L4.801 7.24964ZM1.467 8.74964C1.61081 9.9838 2.10497 11.151 2.89107 12.1132C3.67716 13.0755 4.72232 13.7925 5.903 14.1796C5.7784 13.9172 5.66824 13.6481 5.573 13.3736C5.14 12.1356 4.863 10.5206 4.801 8.74964L1.467 8.74964ZM6.302 8.74964C6.363 10.3826 6.62 11.8236 6.989 12.8786C7.203 13.4886 7.44 13.9286 7.666 14.2016C7.868 14.4456 8.002 14.4916 8.057 14.4986L8.101 14.4976C8.161 14.4876 8.291 14.4366 8.486 14.2016C8.712 13.9286 8.949 13.4886 9.162 12.8786C9.532 11.8236 9.788 10.3826 9.849 8.74964L6.302 8.74964ZM11.35 8.74964C11.289 10.5206 11.011 12.1356 10.578 13.3736C10.496 13.6086 10.407 13.8336 10.31 14.0476C11.4042 13.6152 12.3607 12.8941 13.0775 11.9611C13.7944 11.0282 14.2449 9.91831 14.381 8.74964L11.351 8.74964L11.35 8.74964ZM14.381 7.24964C14.2448 6.081 13.7943 4.97116 13.0774 4.03823C12.3605 3.1053 11.4041 2.38419 10.31 1.95164C10.407 2.16564 10.496 2.39164 10.578 2.62564C11.011 3.86364 11.289 5.47864 11.35 7.24964L14.381 7.24964Z" />
              </svg>
            </CSSTransition>
          </div>

        </CSSTransition>
        <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['languagebuttontext']?.animationClass || {}}>

          <span id="id_one_eightsixsix"  className={` text languagebuttontext    ${ props.onClick ? 'cursor' : ''}  ${ transaction['languagebuttontext']?.type ? transaction['languagebuttontext']?.type.toLowerCase() : '' }`} style={{ ...{},  ...props.LanguageButtonTextStyle , transitionDuration: transaction['languagebuttontext']?.duration, transitionTimingFunction: transaction['languagebuttontext']?.timingFunction }} onClick={ props.LanguageButtonTextonClick } onMouseEnter={ props.LanguageButtonTextonMouseEnter } onMouseOver={ props.LanguageButtonTextonMouseOver } onKeyPress={ props.LanguageButtonTextonKeyPress } onDrag={ props.LanguageButtonTextonDrag } onMouseLeave={ props.LanguageButtonTextonMouseLeave } onMouseUp={ props.LanguageButtonTextonMouseUp } onMouseDown={ props.LanguageButtonTextonMouseDown } onKeyDown={ props.LanguageButtonTextonKeyDown } onChange={ props.LanguageButtonTextonChange } ondelay={ props.LanguageButtonTextondelay } >{props.LanguageButtonText0 || `Español de Latinoamérica`}</span>

        </CSSTransition>

      </>
      }
    </div>

  </CSSTransition>
</>
    ) 
}

PropertyoneVarianttwoo.propTypes = {
    style: PropTypes.any,
LanguageButtonText0: PropTypes.any,
LanguageButtononClick: PropTypes.any,
LanguageButtononMouseEnter: PropTypes.any,
LanguageButtononMouseOver: PropTypes.any,
LanguageButtononKeyPress: PropTypes.any,
LanguageButtononDrag: PropTypes.any,
LanguageButtononMouseLeave: PropTypes.any,
LanguageButtononMouseUp: PropTypes.any,
LanguageButtononMouseDown: PropTypes.any,
LanguageButtononKeyDown: PropTypes.any,
LanguageButtononChange: PropTypes.any,
LanguageButtonondelay: PropTypes.any,
LanguageIcononClick: PropTypes.any,
LanguageIcononMouseEnter: PropTypes.any,
LanguageIcononMouseOver: PropTypes.any,
LanguageIcononKeyPress: PropTypes.any,
LanguageIcononDrag: PropTypes.any,
LanguageIcononMouseLeave: PropTypes.any,
LanguageIcononMouseUp: PropTypes.any,
LanguageIcononMouseDown: PropTypes.any,
LanguageIcononKeyDown: PropTypes.any,
LanguageIcononChange: PropTypes.any,
LanguageIconondelay: PropTypes.any,
VectoronClick: PropTypes.any,
VectoronMouseEnter: PropTypes.any,
VectoronMouseOver: PropTypes.any,
VectoronKeyPress: PropTypes.any,
VectoronDrag: PropTypes.any,
VectoronMouseLeave: PropTypes.any,
VectoronMouseUp: PropTypes.any,
VectoronMouseDown: PropTypes.any,
VectoronKeyDown: PropTypes.any,
VectoronChange: PropTypes.any,
Vectorondelay: PropTypes.any,
LanguageButtonTextonClick: PropTypes.any,
LanguageButtonTextonMouseEnter: PropTypes.any,
LanguageButtonTextonMouseOver: PropTypes.any,
LanguageButtonTextonKeyPress: PropTypes.any,
LanguageButtonTextonDrag: PropTypes.any,
LanguageButtonTextonMouseLeave: PropTypes.any,
LanguageButtonTextonMouseUp: PropTypes.any,
LanguageButtonTextonMouseDown: PropTypes.any,
LanguageButtonTextonKeyDown: PropTypes.any,
LanguageButtonTextonChange: PropTypes.any,
LanguageButtonTextondelay: PropTypes.any
}
export default PropertyoneVarianttwoo;