import React, { Suspense } from 'react';
import PropTypes from 'prop-types';
import * as Contexts  from 'Contexts';
import { OverlaySwapContext } from 'Components/OverlaySwapProvider';
import CorebooksAnimation from 'Components/CorebooksAnimation';
import CorebooksAnimationSingle from 'Components/CorebooksAnimationSingle';

import * as UtilsScripts  from 'utils/utils';
import { Route, useHistory, useParams } from "react-router-dom";
import SingletoneNavigation from 'Class/SingletoneNavigation';

import LoadingComponent from "Components/Loading";


import { Transition } from "react-transition-group";
import { CSSTransition } from "react-transition-group";
import HFive from 'Components/HFive'
import { useAppContext, useSessionContext } from 'context/AppContext';
import './MenuHomePropertyoneDefault.css'





const PropertyoneDefault = (props)=>{
    const [state,dispatch] = useAppContext();
    const [, setNameSelectedComponent] = React.useContext(OverlaySwapContext);
    
    const nodeRef = React.useRef(null);
    const history = useHistory();
    let RouterContext = useParams();
    
    
    
    
    
    
    const [globalStateForm, dispatchForm] = React.useState({});
    
    const [ _in, setIn ] = React.useState(false)
    const [ transaction, setTransaction ] = React.useState({}) 
    const [ animation, setAnimation ] = React.useState('')

    
    
    
    
    
    
    React.useEffect(()=>{
        
        
        
    },[]);
    
    
    return (
        <>
  <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['propertyonedefault']?.animationClass || {}}>

    <div id="id_onefivesix_fourzeronigthnigth" ref={nodeRef} className={` ${ props.onClick ? 'cursor' : '' } propertyonedefault C_onefivesix_fourzeronigthnigth ${ props.cssClass } ${ transaction['propertyonedefault']?.type ? transaction['propertyonedefault']?.type.toLowerCase() : '' }`} style={ { ...{ ...{}, transitionDuration: transaction['propertyonedefault']?.duration, transitionTimingFunction: transaction['propertyonedefault']?.timingFunction }, ...props.style }} onClick={ props.MenuHomeonClick } onMouseEnter={ props.MenuHomeonMouseEnter } onMouseOver={ props.MenuHomeonMouseOver } onKeyPress={ props.MenuHomeonKeyPress } onDrag={ props.MenuHomeonDrag } onMouseLeave={ props.MenuHomeonMouseLeave } onMouseUp={ props.MenuHomeonMouseUp } onMouseDown={ props.MenuHomeonMouseDown } onKeyDown={ props.MenuHomeonKeyDown } onChange={ props.MenuHomeonChange } ondelay={ props.MenuHomeondelay }>
      {
      props.children ?
      props.children :
      <>
        <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['iconhome']?.animationClass || {}}>

          <div id="id_fourthreeeight_fouronetwoothree" className={` frame iconhome ${ props.onClick ? 'cursor' : '' } ${ transaction['iconhome']?.type ? transaction['iconhome']?.type.toLowerCase() : '' }`} style={ { ...{}, ...props.IconHomeStyle , transitionDuration: transaction['iconhome']?.duration, transitionTimingFunction: transaction['iconhome']?.timingFunction } } onClick={ props.IconHomeonClick } onMouseEnter={ props.IconHomeonMouseEnter } onMouseOver={ props.IconHomeonMouseOver } onKeyPress={ props.IconHomeonKeyPress } onDrag={ props.IconHomeonDrag } onMouseLeave={ props.IconHomeonMouseLeave } onMouseUp={ props.IconHomeonMouseUp } onMouseDown={ props.IconHomeonMouseDown } onKeyDown={ props.IconHomeonKeyDown } onChange={ props.IconHomeonChange } ondelay={ props.IconHomeondelay }>
            <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['vector']?.animationClass || {}}>
              <svg id="id_fourthreeeight_fouronetwoofour" className="vector  " style={{}} onClick={ props.VectoronClick } onMouseEnter={ props.VectoronMouseEnter } onMouseOver={ props.VectoronMouseOver } onKeyPress={ props.VectoronKeyPress } onDrag={ props.VectoronDrag } onMouseLeave={ props.VectoronMouseLeave } onMouseUp={ props.VectoronMouseUp } onMouseDown={ props.VectoronMouseDown } onKeyDown={ props.VectoronKeyDown } onChange={ props.VectoronChange } ondelay={ props.Vectorondelay } width="20" height="20.886962890625">
                <path d="M11.5 0.401924C11.0439 0.138619 10.5266 4.44089e-16 10 0C9.47339 -2.22045e-16 8.95606 0.138619 8.5 0.401924L1 4.73192C0.695969 4.90746 0.443498 5.15992 0.267962 5.46395C0.0924258 5.76798 8.91844e-06 6.11286 0 6.46392L0 19.8869C4.44089e-16 20.1521 0.105357 20.4065 0.292893 20.594C0.48043 20.7816 0.734784 20.8869 1 20.8869L7 20.8869C7.26522 20.8869 7.51957 20.7816 7.70711 20.594C7.89464 20.4065 8 20.1521 8 19.8869L8 13.8869L12 13.8869L12 19.8869C12 20.1521 12.1054 20.4065 12.2929 20.594C12.4804 20.7816 12.7348 20.8869 13 20.8869L19 20.8869C19.2652 20.8869 19.5196 20.7816 19.7071 20.594C19.8946 20.4065 20 20.1521 20 19.8869L20 6.46392C20 6.11286 19.9076 5.76798 19.732 5.46395C19.5565 5.15992 19.304 4.90746 19 4.73192L11.5 0.401924Z" />
              </svg>
            </CSSTransition>
          </div>

        </CSSTransition>
        <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['hfive']?.animationClass || {}}>
          <HFive { ...{ ...props, style:false } } HfiveText0={ props.HfiveText0 || "Inicio" } HfiveTextStyle={{"fontSize":"0.875rem"}} cssClass={"C_onesixzero_fourtwoooneeight "}  />
    </CSSTransition >
            
            </>
        }
        </div>
    
    </CSSTransition >
            </>
        
    ) 
}

PropertyoneDefault.propTypes = {
    style: PropTypes.any,
HfiveText0: PropTypes.any,
MenuHomeonClick: PropTypes.any,
MenuHomeonMouseEnter: PropTypes.any,
MenuHomeonMouseOver: PropTypes.any,
MenuHomeonKeyPress: PropTypes.any,
MenuHomeonDrag: PropTypes.any,
MenuHomeonMouseLeave: PropTypes.any,
MenuHomeonMouseUp: PropTypes.any,
MenuHomeonMouseDown: PropTypes.any,
MenuHomeonKeyDown: PropTypes.any,
MenuHomeonChange: PropTypes.any,
MenuHomeondelay: PropTypes.any,
IconHomeonClick: PropTypes.any,
IconHomeonMouseEnter: PropTypes.any,
IconHomeonMouseOver: PropTypes.any,
IconHomeonKeyPress: PropTypes.any,
IconHomeonDrag: PropTypes.any,
IconHomeonMouseLeave: PropTypes.any,
IconHomeonMouseUp: PropTypes.any,
IconHomeonMouseDown: PropTypes.any,
IconHomeonKeyDown: PropTypes.any,
IconHomeonChange: PropTypes.any,
IconHomeondelay: PropTypes.any,
VectoronClick: PropTypes.any,
VectoronMouseEnter: PropTypes.any,
VectoronMouseOver: PropTypes.any,
VectoronKeyPress: PropTypes.any,
VectoronDrag: PropTypes.any,
VectoronMouseLeave: PropTypes.any,
VectoronMouseUp: PropTypes.any,
VectoronMouseDown: PropTypes.any,
VectoronKeyDown: PropTypes.any,
VectoronChange: PropTypes.any,
Vectorondelay: PropTypes.any
}
export default PropertyoneDefault;