import React, { Suspense } from 'react';
import PropTypes from 'prop-types';
import * as Contexts  from 'Contexts';
import { OverlaySwapContext } from 'Components/OverlaySwapProvider';
import CorebooksAnimation from 'Components/CorebooksAnimation';
import CorebooksAnimationSingle from 'Components/CorebooksAnimationSingle';

import * as UtilsScripts  from 'utils/utils';
import { Route, useHistory, useParams } from "react-router-dom";
import SingletoneNavigation from 'Class/SingletoneNavigation';

import LoadingComponent from "Components/Loading";


import { Transition } from "react-transition-group";
import { CSSTransition } from "react-transition-group";
import ColorCard from 'Components/ColorCard'
import { useAppContext, useSessionContext } from 'context/AppContext';
import './ColorPalette.css'





const ColorPalette = (props)=>{
    const [state,dispatch] = useAppContext();
    const [, setNameSelectedComponent] = React.useContext(OverlaySwapContext);
    
    const nodeRef = React.useRef(null);
    const history = useHistory();
    let RouterContext = useParams();
    
    
    
    
    
    
    const [globalStateForm, dispatchForm] = React.useState({});
    
    const [ _in, setIn ] = React.useState(false)
    const [ transaction, setTransaction ] = React.useState({}) 
    const [ animation, setAnimation ] = React.useState('')

    
    
    
    
    
    
    React.useEffect(()=>{
        
        
        
    },[]);
    
    
    return (
        <>
  <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['colorpalette']?.animationClass || {}}>

    <div id="id_fivefourone_onesixfivesix" ref={nodeRef} className={` ${ props.onClick ? 'cursor' : '' } colorpalette ${ props.cssClass } ${ transaction['colorpalette']?.type ? transaction['colorpalette']?.type.toLowerCase() : '' }`} style={ { ...{ ...{}, transitionDuration: transaction['colorpalette']?.duration, transitionTimingFunction: transaction['colorpalette']?.timingFunction }, ...props.style }} onClick={ props.ColorPaletteonClick } onMouseEnter={ props.ColorPaletteonMouseEnter } onMouseOver={ props.ColorPaletteonMouseOver } onKeyPress={ props.ColorPaletteonKeyPress } onDrag={ props.ColorPaletteonDrag } onMouseLeave={ props.ColorPaletteonMouseLeave } onMouseUp={ props.ColorPaletteonMouseUp } onMouseDown={ props.ColorPaletteonMouseDown } onKeyDown={ props.ColorPaletteonKeyDown } onChange={ props.ColorPaletteonChange } ondelay={ props.ColorPaletteondelay }>
      {
      props.children ?
      props.children :
      <>
        <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['header']?.animationClass || {}}>

          <div id="id_fivefourone_onesixfiveeight" className={` instance header ${ props.onClick ? 'cursor' : '' } ${ transaction['header']?.type ? transaction['header']?.type.toLowerCase() : '' }`} style={{ ...{},  ...props.HeaderStyle , transitionDuration: transaction['header']?.duration, transitionTimingFunction: transaction['header']?.timingFunction }} onClick={ props.HeaderonClick } onMouseEnter={ props.HeaderonMouseEnter } onMouseOver={ props.HeaderonMouseOver } onKeyPress={ props.HeaderonKeyPress } onDrag={ props.HeaderonDrag } onMouseLeave={ props.HeaderonMouseLeave } onMouseUp={ props.HeaderonMouseUp } onMouseDown={ props.HeaderonMouseDown } onKeyDown={ props.HeaderonKeyDown } onChange={ props.HeaderonChange } ondelay={ props.Headerondelay }>
            <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['title']?.animationClass || {}}>

              <span id="id_fivefourone_onesixfiveeight_threetwoofourfive_foursixfivezerozero"  className={` text title    ${ props.onClick ? 'cursor' : ''}  ${ transaction['title']?.type ? transaction['title']?.type.toLowerCase() : '' }`} style={{ ...{},  ...props.TitleStyle , transitionDuration: transaction['title']?.duration, transitionTimingFunction: transaction['title']?.timingFunction }} onClick={ props.TitleonClick } onMouseEnter={ props.TitleonMouseEnter } onMouseOver={ props.TitleonMouseOver } onKeyPress={ props.TitleonKeyPress } onDrag={ props.TitleonDrag } onMouseLeave={ props.TitleonMouseLeave } onMouseUp={ props.TitleonMouseUp } onMouseDown={ props.TitleonMouseDown } onKeyDown={ props.TitleonKeyDown } onChange={ props.TitleonChange } ondelay={ props.Titleondelay } >{props.Title0 || `Background`}</span>

            </CSSTransition>
          </div>

        </CSSTransition>
        <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['header']?.animationClass || {}}>

          <div id="id_fivefourone_onesixfivenigth" className={` instance header ${ props.onClick ? 'cursor' : '' } ${ transaction['header']?.type ? transaction['header']?.type.toLowerCase() : '' }`} style={{ ...{},  ...props.HeaderStyle , transitionDuration: transaction['header']?.duration, transitionTimingFunction: transaction['header']?.timingFunction }} onClick={ props.HeaderonClick } onMouseEnter={ props.HeaderonMouseEnter } onMouseOver={ props.HeaderonMouseOver } onKeyPress={ props.HeaderonKeyPress } onDrag={ props.HeaderonDrag } onMouseLeave={ props.HeaderonMouseLeave } onMouseUp={ props.HeaderonMouseUp } onMouseDown={ props.HeaderonMouseDown } onKeyDown={ props.HeaderonKeyDown } onChange={ props.HeaderonChange } ondelay={ props.Headerondelay }>
            <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['title']?.animationClass || {}}>

              <span id="id_fivefourone_onesixfivenigth_threetwoofourfive_foursixfivezerozero"  className={` text title    ${ props.onClick ? 'cursor' : ''}  ${ transaction['title']?.type ? transaction['title']?.type.toLowerCase() : '' }`} style={{ ...{},  ...props.TitleStyle , transitionDuration: transaction['title']?.duration, transitionTimingFunction: transaction['title']?.timingFunction }} onClick={ props.TitleonClick } onMouseEnter={ props.TitleonMouseEnter } onMouseOver={ props.TitleonMouseOver } onKeyPress={ props.TitleonKeyPress } onDrag={ props.TitleonDrag } onMouseLeave={ props.TitleonMouseLeave } onMouseUp={ props.TitleonMouseUp } onMouseDown={ props.TitleonMouseDown } onKeyDown={ props.TitleonKeyDown } onChange={ props.TitleonChange } ondelay={ props.Titleondelay } >{props.Title1 || `Icons`}</span>

            </CSSTransition>
          </div>

        </CSSTransition>
        <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['header']?.animationClass || {}}>

          <div id="id_fivefourone_onesixsixzero" className={` instance header ${ props.onClick ? 'cursor' : '' } ${ transaction['header']?.type ? transaction['header']?.type.toLowerCase() : '' }`} style={{ ...{},  ...props.HeaderStyle , transitionDuration: transaction['header']?.duration, transitionTimingFunction: transaction['header']?.timingFunction }} onClick={ props.HeaderonClick } onMouseEnter={ props.HeaderonMouseEnter } onMouseOver={ props.HeaderonMouseOver } onKeyPress={ props.HeaderonKeyPress } onDrag={ props.HeaderonDrag } onMouseLeave={ props.HeaderonMouseLeave } onMouseUp={ props.HeaderonMouseUp } onMouseDown={ props.HeaderonMouseDown } onKeyDown={ props.HeaderonKeyDown } onChange={ props.HeaderonChange } ondelay={ props.Headerondelay }>
            <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['title']?.animationClass || {}}>

              <span id="id_fivefourone_onesixsixzero_threetwoofourfive_foursixfivezerozero"  className={` text title    ${ props.onClick ? 'cursor' : ''}  ${ transaction['title']?.type ? transaction['title']?.type.toLowerCase() : '' }`} style={{ ...{},  ...props.TitleStyle , transitionDuration: transaction['title']?.duration, transitionTimingFunction: transaction['title']?.timingFunction }} onClick={ props.TitleonClick } onMouseEnter={ props.TitleonMouseEnter } onMouseOver={ props.TitleonMouseOver } onKeyPress={ props.TitleonKeyPress } onDrag={ props.TitleonDrag } onMouseLeave={ props.TitleonMouseLeave } onMouseUp={ props.TitleonMouseUp } onMouseDown={ props.TitleonMouseDown } onKeyDown={ props.TitleonKeyDown } onChange={ props.TitleonChange } ondelay={ props.Titleondelay } >{props.Title2 || `Text`}</span>

            </CSSTransition>
          </div>

        </CSSTransition>
        <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['header']?.animationClass || {}}>

          <div id="id_fivefourone_onesixsixone" className={` instance header ${ props.onClick ? 'cursor' : '' } ${ transaction['header']?.type ? transaction['header']?.type.toLowerCase() : '' }`} style={{ ...{},  ...props.HeaderStyle , transitionDuration: transaction['header']?.duration, transitionTimingFunction: transaction['header']?.timingFunction }} onClick={ props.HeaderonClick } onMouseEnter={ props.HeaderonMouseEnter } onMouseOver={ props.HeaderonMouseOver } onKeyPress={ props.HeaderonKeyPress } onDrag={ props.HeaderonDrag } onMouseLeave={ props.HeaderonMouseLeave } onMouseUp={ props.HeaderonMouseUp } onMouseDown={ props.HeaderonMouseDown } onKeyDown={ props.HeaderonKeyDown } onChange={ props.HeaderonChange } ondelay={ props.Headerondelay }>
            <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['title']?.animationClass || {}}>

              <span id="id_fivefourone_onesixsixone_threetwoofourfive_foursixfivezerozero"  className={` text title    ${ props.onClick ? 'cursor' : ''}  ${ transaction['title']?.type ? transaction['title']?.type.toLowerCase() : '' }`} style={{ ...{},  ...props.TitleStyle , transitionDuration: transaction['title']?.duration, transitionTimingFunction: transaction['title']?.timingFunction }} onClick={ props.TitleonClick } onMouseEnter={ props.TitleonMouseEnter } onMouseOver={ props.TitleonMouseOver } onKeyPress={ props.TitleonKeyPress } onDrag={ props.TitleonDrag } onMouseLeave={ props.TitleonMouseLeave } onMouseUp={ props.TitleonMouseUp } onMouseDown={ props.TitleonMouseDown } onKeyDown={ props.TitleonKeyDown } onChange={ props.TitleonChange } ondelay={ props.Titleondelay } >{props.Title3 || `Buttons`}</span>

            </CSSTransition>
          </div>

        </CSSTransition>
        <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['header']?.animationClass || {}}>

          <div id="id_fivefourone_onesixsixtwoo" className={` instance header ${ props.onClick ? 'cursor' : '' } ${ transaction['header']?.type ? transaction['header']?.type.toLowerCase() : '' }`} style={{ ...{},  ...props.HeaderStyle , transitionDuration: transaction['header']?.duration, transitionTimingFunction: transaction['header']?.timingFunction }} onClick={ props.HeaderonClick } onMouseEnter={ props.HeaderonMouseEnter } onMouseOver={ props.HeaderonMouseOver } onKeyPress={ props.HeaderonKeyPress } onDrag={ props.HeaderonDrag } onMouseLeave={ props.HeaderonMouseLeave } onMouseUp={ props.HeaderonMouseUp } onMouseDown={ props.HeaderonMouseDown } onKeyDown={ props.HeaderonKeyDown } onChange={ props.HeaderonChange } ondelay={ props.Headerondelay }>
            <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['title']?.animationClass || {}}>

              <span id="id_fivefourone_onesixsixtwoo_threetwoofourfive_foursixfivezerozero"  className={` text title    ${ props.onClick ? 'cursor' : ''}  ${ transaction['title']?.type ? transaction['title']?.type.toLowerCase() : '' }`} style={{ ...{},  ...props.TitleStyle , transitionDuration: transaction['title']?.duration, transitionTimingFunction: transaction['title']?.timingFunction }} onClick={ props.TitleonClick } onMouseEnter={ props.TitleonMouseEnter } onMouseOver={ props.TitleonMouseOver } onKeyPress={ props.TitleonKeyPress } onDrag={ props.TitleonDrag } onMouseLeave={ props.TitleonMouseLeave } onMouseUp={ props.TitleonMouseUp } onMouseDown={ props.TitleonMouseDown } onKeyDown={ props.TitleonKeyDown } onChange={ props.TitleonChange } ondelay={ props.Titleondelay } >{props.Title4 || `Warning`}</span>

            </CSSTransition>
          </div>

        </CSSTransition>
        <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['descrip']?.animationClass || {}}>

          <span id="id_fivefourone_onesixsixfour"  className={` text descrip    ${ props.onClick ? 'cursor' : ''}  ${ transaction['descrip']?.type ? transaction['descrip']?.type.toLowerCase() : '' }`} style={{ ...{},  ...props.DescripStyle , transitionDuration: transaction['descrip']?.duration, transitionTimingFunction: transaction['descrip']?.timingFunction }} onClick={ props.DescriponClick } onMouseEnter={ props.DescriponMouseEnter } onMouseOver={ props.DescriponMouseOver } onKeyPress={ props.DescriponKeyPress } onDrag={ props.DescriponDrag } onMouseLeave={ props.DescriponMouseLeave } onMouseUp={ props.DescriponMouseUp } onMouseDown={ props.DescriponMouseDown } onKeyDown={ props.DescriponKeyDown } onChange={ props.DescriponChange } ondelay={ props.Descripondelay } >{props.Descrip0 || `descrip`}</span>

        </CSSTransition>
        <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['descrip']?.animationClass || {}}>

          <span id="id_fivefourone_onesixsixfive"  className={` text descrip    ${ props.onClick ? 'cursor' : ''}  ${ transaction['descrip']?.type ? transaction['descrip']?.type.toLowerCase() : '' }`} style={{ ...{},  ...props.DescripStyle , transitionDuration: transaction['descrip']?.duration, transitionTimingFunction: transaction['descrip']?.timingFunction }} onClick={ props.DescriponClick } onMouseEnter={ props.DescriponMouseEnter } onMouseOver={ props.DescriponMouseOver } onKeyPress={ props.DescriponKeyPress } onDrag={ props.DescriponDrag } onMouseLeave={ props.DescriponMouseLeave } onMouseUp={ props.DescriponMouseUp } onMouseDown={ props.DescriponMouseDown } onKeyDown={ props.DescriponKeyDown } onChange={ props.DescriponChange } ondelay={ props.Descripondelay } >{props.Descrip1 || `descrip`}</span>

        </CSSTransition>
        <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['descrip']?.animationClass || {}}>

          <span id="id_fivefourone_onesixsixsix"  className={` text descrip    ${ props.onClick ? 'cursor' : ''}  ${ transaction['descrip']?.type ? transaction['descrip']?.type.toLowerCase() : '' }`} style={{ ...{},  ...props.DescripStyle , transitionDuration: transaction['descrip']?.duration, transitionTimingFunction: transaction['descrip']?.timingFunction }} onClick={ props.DescriponClick } onMouseEnter={ props.DescriponMouseEnter } onMouseOver={ props.DescriponMouseOver } onKeyPress={ props.DescriponKeyPress } onDrag={ props.DescriponDrag } onMouseLeave={ props.DescriponMouseLeave } onMouseUp={ props.DescriponMouseUp } onMouseDown={ props.DescriponMouseDown } onKeyDown={ props.DescriponKeyDown } onChange={ props.DescriponChange } ondelay={ props.Descripondelay } >{props.Descrip2 || `descrip`}</span>

        </CSSTransition>
        <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['backgroundsappliedtocomponentsscreens']?.animationClass || {}}>

          <span id="id_fivefourone_onesixsevenzero"  className={` text backgroundsappliedtocomponentsscreens    ${ props.onClick ? 'cursor' : ''}  ${ transaction['backgroundsappliedtocomponentsscreens']?.type ? transaction['backgroundsappliedtocomponentsscreens']?.type.toLowerCase() : '' }`} style={{ ...{},  ...props.BackgroundsappliedtocomponentsscreensStyle , transitionDuration: transaction['backgroundsappliedtocomponentsscreens']?.duration, transitionTimingFunction: transaction['backgroundsappliedtocomponentsscreens']?.timingFunction }} onClick={ props.BackgroundsappliedtocomponentsscreensonClick } onMouseEnter={ props.BackgroundsappliedtocomponentsscreensonMouseEnter } onMouseOver={ props.BackgroundsappliedtocomponentsscreensonMouseOver } onKeyPress={ props.BackgroundsappliedtocomponentsscreensonKeyPress } onDrag={ props.BackgroundsappliedtocomponentsscreensonDrag } onMouseLeave={ props.BackgroundsappliedtocomponentsscreensonMouseLeave } onMouseUp={ props.BackgroundsappliedtocomponentsscreensonMouseUp } onMouseDown={ props.BackgroundsappliedtocomponentsscreensonMouseDown } onKeyDown={ props.BackgroundsappliedtocomponentsscreensonKeyDown } onChange={ props.BackgroundsappliedtocomponentsscreensonChange } ondelay={ props.Backgroundsappliedtocomponentsscreensondelay } >{props.Backgroundsappliedtocomponentsscreens0 || `Backgrounds applied to components & screens`}</span>

        </CSSTransition>
        <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['samples']?.animationClass || {}}>

          <div id="id_fivefourone_onesixeightone" className={` frame samples ${ props.onClick ? 'cursor' : '' } ${ transaction['samples']?.type ? transaction['samples']?.type.toLowerCase() : '' }`} style={ { ...{}, ...props.SamplesStyle , transitionDuration: transaction['samples']?.duration, transitionTimingFunction: transaction['samples']?.timingFunction } } onClick={ props.SamplesonClick } onMouseEnter={ props.SamplesonMouseEnter } onMouseOver={ props.SamplesonMouseOver } onKeyPress={ props.SamplesonKeyPress } onDrag={ props.SamplesonDrag } onMouseLeave={ props.SamplesonMouseLeave } onMouseUp={ props.SamplesonMouseUp } onMouseDown={ props.SamplesonMouseDown } onKeyDown={ props.SamplesonKeyDown } onChange={ props.SamplesonChange } ondelay={ props.Samplesondelay }>
            <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['colorcard']?.animationClass || {}}>
              <ColorCard { ...{ ...props, style:false } } ColorDataTitle0={ props.ColorDataTitle0 || "Icon Off" } ColorHexText0={ props.ColorHexText0 || "A7A7A7" } ColorCardBlock0={ props.ColorCardBlock0 || "" } cssClass={"C_sixonezero_onesixoneone "}  />
    </CSSTransition >
<CSSTransition 
        in={_in}
        appear={true} 
        timeout={100}
        classNames={transaction['colorcard']?.animationClass || {}}
    >
    <ColorCard { ...{ ...props, style:false } }   ColorHexText0={ props.ColorHexText1 || " F5F5F5"} ColorDataTitle0={ props.ColorDataTitle1 || "Icon Select / Hover" } ColorCardBlock0={ props.ColorCardBlock1 || "" } cssClass={"C_sixonezero_onesixoneeight "}  />
    </CSSTransition >
<CSSTransition 
        in={_in}
        appear={true} 
        timeout={100}
        classNames={transaction['colorcard']?.animationClass || {}}
    >
    <ColorCard { ...{ ...props, style:false } }   ColorDataTitle0={ props.ColorDataTitle2 || " Background Icon 1"} ColorHexText0={ props.ColorHexText2 || "292929" } ColorCardBlock0={ props.ColorCardBlock2 || "" } cssClass={"C_sixonezero_onesixtwoofive "}  />
    </CSSTransition >
<CSSTransition 
        in={_in}
        appear={true} 
        timeout={100}
        classNames={transaction['colorcard']?.animationClass || {}}
    >
    <ColorCard { ...{ ...props, style:false } }   ColorDataTitle0={ props.ColorDataTitle3 || " Background Icon 2"} ColorHexText0={ props.ColorHexText3 || "181818" } ColorCardBlock0={ props.ColorCardBlock3 || "" } ColorHexText0={ props.ColorHexText3 || "181818" } cssClass={"C_sixonezero_onesixthreefour "}  />
    </CSSTransition >
                    </div>
               
    </CSSTransition >
<CSSTransition 
        in={_in}
        appear={true} 
        timeout={100}
        classNames={transaction['samples']?.animationClass || {}}
    >
    
                    <div id="id_fivefourone_onesixnigthfour" className={` frame samples ${ props.onClick ? 'cursor' : '' } ${ transaction['samples']?.type ? transaction['samples']?.type.toLowerCase() : '' }`} style={ { ...{}, ...props.SamplesStyle , transitionDuration: transaction['samples']?.duration, transitionTimingFunction: transaction['samples']?.timingFunction } } onClick={ props.SamplesonClick } onMouseEnter={ props.SamplesonMouseEnter } onMouseOver={ props.SamplesonMouseOver } onKeyPress={ props.SamplesonKeyPress } onDrag={ props.SamplesonDrag } onMouseLeave={ props.SamplesonMouseLeave } onMouseUp={ props.SamplesonMouseUp } onMouseDown={ props.SamplesonMouseDown } onKeyDown={ props.SamplesonKeyDown } onChange={ props.SamplesonChange } ondelay={ props.Samplesondelay }>
                <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['colorcard']?.animationClass || {}}>
                  <ColorCard { ...{ ...props, style:false } } ColorHexText0={ props.ColorHexText4 || "A7A7A7" } ColorDataTitle0={ props.ColorDataTitle4 || "Text Basic / Off" } ColorCardBlock0={ props.ColorCardBlock4 || "" } cssClass={"C_sixonezero_onesixfourone "}  />
    </CSSTransition >
<CSSTransition 
        in={_in}
        appear={true} 
        timeout={100}
        classNames={transaction['colorcard']?.animationClass || {}}
    >
    <ColorCard { ...{ ...props, style:false } }   ColorDataTitle0={ props.ColorDataTitle5 || " Text Menú  Select / Hover"} ColorHexText0={ props.ColorHexText5 || "FFFFFF" } ColorCardBlock0={ props.ColorCardBlock5 || "" } cssClass={"C_sixonezero_onesixfoureight "}  />
    </CSSTransition >
<CSSTransition 
        in={_in}
        appear={true} 
        timeout={100}
        classNames={transaction['colorcard']?.animationClass || {}}
    >
    <ColorCard { ...{ ...props, style:false } }   ColorDataTitle0={ props.ColorDataTitle6 || " Text Link"} ColorHexText0={ props.ColorHexText6 || "1DB954" } ColorCardBlock0={ props.ColorCardBlock6 || "" } cssClass={"C_sixonezero_onesixfivefive "}  />
    </CSSTransition >
<CSSTransition 
        in={_in}
        appear={true} 
        timeout={100}
        classNames={transaction['colorcard']?.animationClass || {}}
    >
    <ColorCard { ...{ ...props, style:false } }   ColorDataTitle0={ props.ColorDataTitle7 || " Text Link Hover"} ColorHexText0={ props.ColorHexText7 || "64D06D" } ColorCardBlock0={ props.ColorCardBlock7 || "" } cssClass={"C_sixonezero_onesixsixtwoo "}  />
    </CSSTransition >
<CSSTransition 
        in={_in}
        appear={true} 
        timeout={100}
        classNames={transaction['colorcard']?.animationClass || {}}
    >
    <ColorCard { ...{ ...props, style:false } }   ColorHexText0={ props.ColorHexText8 || " 000000"} ColorDataTitle0={ props.ColorDataTitle8 || "Text Description" } ColorCardBlock0={ props.ColorCardBlock8 || "" } cssClass={"C_sixonezero_onesixsixnigth "}  />
    </CSSTransition >
                    </div>
               
    </CSSTransition >
<CSSTransition 
        in={_in}
        appear={true} 
        timeout={100}
        classNames={transaction['samples']?.animationClass || {}}
    >
    
                    <div id="id_fivefourone_onesevenzeroseven" className={` frame samples ${ props.onClick ? 'cursor' : '' } ${ transaction['samples']?.type ? transaction['samples']?.type.toLowerCase() : '' }`} style={ { ...{}, ...props.SamplesStyle , transitionDuration: transaction['samples']?.duration, transitionTimingFunction: transaction['samples']?.timingFunction } } onClick={ props.SamplesonClick } onMouseEnter={ props.SamplesonMouseEnter } onMouseOver={ props.SamplesonMouseOver } onKeyPress={ props.SamplesonKeyPress } onDrag={ props.SamplesonDrag } onMouseLeave={ props.SamplesonMouseLeave } onMouseUp={ props.SamplesonMouseUp } onMouseDown={ props.SamplesonMouseDown } onKeyDown={ props.SamplesonKeyDown } onChange={ props.SamplesonChange } ondelay={ props.Samplesondelay }>
                    <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['colorcard']?.animationClass || {}}>
                      <ColorCard { ...{ ...props, style:false } } ColorDataTitle0={ props.ColorDataTitle9 || "Principal Button 1" } ColorHexText0={ props.ColorHexText9 || "FFFFFF" } ColorCardBlock0={ props.ColorCardBlock9 || "" } cssClass={"C_sixoneeight_onefiveeighttwoo "}  />
    </CSSTransition >
<CSSTransition 
        in={_in}
        appear={true} 
        timeout={100}
        classNames={transaction['colorcard']?.animationClass || {}}
    >
    <ColorCard { ...{ ...props, style:false } }   ColorDataTitle0={ props.ColorDataTitle10 || " Principal Button 1 Hover"} ColorHexText0={ props.ColorHexText10 || "A7A7A7" } ColorCardBlock0={ props.ColorCardBlock10 || "" } cssClass={"C_sixoneeight_onefiveeightnigth "}  />
    </CSSTransition >
<CSSTransition 
        in={_in}
        appear={true} 
        timeout={100}
        classNames={transaction['colorcard']?.animationClass || {}}
    >
    <ColorCard { ...{ ...props, style:false } }   ColorDataTitle0={ props.ColorDataTitle11 || " Language Button"} ColorHexText0={ props.ColorHexText11 || "sin fill" } ColorCardBlock0={ props.ColorCardBlock11 || "" } cssClass={"C_sixoneeight_onefivenigthsix "}  />
    </CSSTransition >
<CSSTransition 
        in={_in}
        appear={true} 
        timeout={100}
        classNames={transaction['colorcard']?.animationClass || {}}
    >
    <ColorCard { ...{ ...props, style:false } }   ColorHexText0={ props.ColorHexText12 || " 727272"} ColorDataTitle0={ props.ColorDataTitle12 || "Language Button Stroke" } ColorCardBlock0={ props.ColorCardBlock12 || "" } cssClass={"C_sixoneeight_onesixzerothree "}  />
    </CSSTransition >
<CSSTransition 
        in={_in}
        appear={true} 
        timeout={100}
        classNames={transaction['colorcard']?.animationClass || {}}
    >
    <ColorCard { ...{ ...props, style:false } }   ColorHexText0={ props.ColorHexText13 || " SIN FILL"} ColorDataTitle0={ props.ColorDataTitle13 || "Principal Button 2" } ColorCardBlock0={ props.ColorCardBlock13 || "" } cssClass={"C_sixoneeight_onesixonezero "}  />
    </CSSTransition >
<CSSTransition 
        in={_in}
        appear={true} 
        timeout={100}
        classNames={transaction['colorcard']?.animationClass || {}}
    >
    <ColorCard { ...{ ...props, style:false } }   ColorDataTitle0={ props.ColorDataTitle14 || " Principal Button 2 Hover"} ColorHexText0={ props.ColorHexText14 || "3367C1" } ColorCardBlock0={ props.ColorCardBlock14 || "" } cssClass={"C_sixtwoozero_onesixonenigth "}  />
    </CSSTransition >
<CSSTransition 
        in={_in}
        appear={true} 
        timeout={100}
        classNames={transaction['colorcard']?.animationClass || {}}
    >
    <ColorCard { ...{ ...props, style:false } }   ColorDataTitle0={ props.ColorDataTitle15 || " Principal Button 3"} ColorHexText0={ props.ColorHexText15 || "1ED760" } ColorCardBlock0={ props.ColorCardBlock15 || "" } cssClass={"C_sixtwoozero_onesixonetwoo "}  />
    </CSSTransition >
<CSSTransition 
        in={_in}
        appear={true} 
        timeout={100}
        classNames={transaction['colorcard']?.animationClass || {}}
    >
    <ColorCard { ...{ ...props, style:false } }   ColorDataTitle0={ props.ColorDataTitle16 || " Principal Button 3 Hover"} ColorHexText0={ props.ColorHexText16 || "69DC72" } ColorCardBlock0={ props.ColorCardBlock16 || "" } cssClass={"C_sixtwoozero_onesixtwoosix "}  />
    </CSSTransition >
                    </div>
               
    </CSSTransition >
<CSSTransition 
        in={_in}
        appear={true} 
        timeout={100}
        classNames={transaction['samples']?.animationClass || {}}
    >
    
                    <div id="id_fivefourone_onesevenfivesix" className={` frame samples ${ props.onClick ? 'cursor' : '' } ${ transaction['samples']?.type ? transaction['samples']?.type.toLowerCase() : '' }`} style={ { ...{}, ...props.SamplesStyle , transitionDuration: transaction['samples']?.duration, transitionTimingFunction: transaction['samples']?.timingFunction } } onClick={ props.SamplesonClick } onMouseEnter={ props.SamplesonMouseEnter } onMouseOver={ props.SamplesonMouseOver } onKeyPress={ props.SamplesonKeyPress } onDrag={ props.SamplesonDrag } onMouseLeave={ props.SamplesonMouseLeave } onMouseUp={ props.SamplesonMouseUp } onMouseDown={ props.SamplesonMouseDown } onKeyDown={ props.SamplesonKeyDown } onChange={ props.SamplesonChange } ondelay={ props.Samplesondelay }>
                        <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['framefourtwoonigth']?.animationClass || {}}>

                          <div id="id_fivenigthtwoo_onesixzerofour" className={` frame framefourtwoonigth ${ props.onClick ? 'cursor' : '' } ${ transaction['framefourtwoonigth']?.type ? transaction['framefourtwoonigth']?.type.toLowerCase() : '' }`} style={ { ...{}, ...props.FramefourtwoonigthStyle , transitionDuration: transaction['framefourtwoonigth']?.duration, transitionTimingFunction: transaction['framefourtwoonigth']?.timingFunction } } onClick={ props.FramefourtwoonigthonClick } onMouseEnter={ props.FramefourtwoonigthonMouseEnter } onMouseOver={ props.FramefourtwoonigthonMouseOver } onKeyPress={ props.FramefourtwoonigthonKeyPress } onDrag={ props.FramefourtwoonigthonDrag } onMouseLeave={ props.FramefourtwoonigthonMouseLeave } onMouseUp={ props.FramefourtwoonigthonMouseUp } onMouseDown={ props.FramefourtwoonigthonMouseDown } onKeyDown={ props.FramefourtwoonigthonKeyDown } onChange={ props.FramefourtwoonigthonChange } ondelay={ props.Framefourtwoonigthondelay }>
                            <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['colorcard']?.animationClass || {}}>
                              <ColorCard { ...{ ...props, style:false } } cssClass={"C_fourthreeeight_sevenonethreeeight "}  />
    </CSSTransition >
<CSSTransition 
        in={_in}
        appear={true} 
        timeout={100}
        classNames={transaction['colorcard']?.animationClass || {}}
    >
    <ColorCard { ...{ ...props, style:false } }   ColorHexText0={ props.ColorHexText18 || " 121212"} ColorDataTitle0={ props.ColorDataTitle18 || "Primary Background" } ColorCardBlock0={ props.ColorCardBlock18 || "" } cssClass={"C_fivefourone_twoofivefivesix "}  />
    </CSSTransition >
<CSSTransition 
        in={_in}
        appear={true} 
        timeout={100}
        classNames={transaction['colorcard']?.animationClass || {}}
    >
    <ColorCard { ...{ ...props, style:false } }   ColorHexText0={ props.ColorHexText19 || " 242424"} ColorDataTitle0={ props.ColorDataTitle19 || "Background Section Menu" } ColorHexText0={ props.ColorHexText19 || "242424" } ColorCardBlock0={ props.ColorCardBlock19 || "" } cssClass={"C_fivefourone_twoofivesixfour "}  />
    </CSSTransition >
<CSSTransition 
        in={_in}
        appear={true} 
        timeout={100}
        classNames={transaction['colorcard']?.animationClass || {}}
    >
    <ColorCard { ...{ ...props, style:false } }   ColorHexText0={ props.ColorHexText20 || " 1D75DE"} ColorDataTitle0={ props.ColorDataTitle20 || "Background Screen 1" } ColorHexText0={ props.ColorHexText20 || "1D75DE" } ColorCardBlock0={ props.ColorCardBlock20 || "" } cssClass={"C_fivenigthtwoo_onefiveeighttwoo "}  />
    </CSSTransition >
                    </div>
               
    </CSSTransition >
<CSSTransition 
        in={_in}
        appear={true} 
        timeout={100}
        classNames={transaction['framefourthreezero']?.animationClass || {}}
    >
    
                    <div id="id_fivenigthtwoo_onesixzerofive" className={` frame framefourthreezero ${ props.onClick ? 'cursor' : '' } ${ transaction['framefourthreezero']?.type ? transaction['framefourthreezero']?.type.toLowerCase() : '' }`} style={ { ...{}, ...props.FramefourthreezeroStyle , transitionDuration: transaction['framefourthreezero']?.duration, transitionTimingFunction: transaction['framefourthreezero']?.timingFunction } } onClick={ props.FramefourthreezeroonClick } onMouseEnter={ props.FramefourthreezeroonMouseEnter } onMouseOver={ props.FramefourthreezeroonMouseOver } onKeyPress={ props.FramefourthreezeroonKeyPress } onDrag={ props.FramefourthreezeroonDrag } onMouseLeave={ props.FramefourthreezeroonMouseLeave } onMouseUp={ props.FramefourthreezeroonMouseUp } onMouseDown={ props.FramefourthreezeroonMouseDown } onKeyDown={ props.FramefourthreezeroonKeyDown } onChange={ props.FramefourthreezeroonChange } ondelay={ props.Framefourthreezeroondelay }>
                                <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['colorcard']?.animationClass || {}}>
                                  <ColorCard { ...{ ...props, style:false } } ColorHexText0={ props.ColorHexText21 || "FFFFFF" } ColorDataTitle0={ props.ColorDataTitle21 || "Background Screen 2" } ColorHexText0={ props.ColorHexText21 || "FFFFFF" } ColorCardBlock0={ props.ColorCardBlock21 || "" } cssClass={"C_fivenigthtwoo_onefiveeightnigth "}  />
    </CSSTransition >
<CSSTransition 
        in={_in}
        appear={true} 
        timeout={100}
        classNames={transaction['colorcard']?.animationClass || {}}
    >
    <ColorCard { ...{ ...props, style:false } }   ColorHexText0={ props.ColorHexText22 || " 19E68C"} ColorDataTitle0={ props.ColorDataTitle22 || "Background Screen 3" } ColorHexText0={ props.ColorHexText22 || "19E68C" } ColorCardBlock0={ props.ColorCardBlock22 || "" } cssClass={"C_fivenigthtwoo_onefivenigthseven "}  />
    </CSSTransition >
<CSSTransition 
        in={_in}
        appear={true} 
        timeout={100}
        classNames={transaction['colorcard']?.animationClass || {}}
    >
    <ColorCard { ...{ ...props, style:false } }   ColorHexText0={ props.ColorHexText23 || " 131313 #1C1C1C (80%)"} ColorDataTitle0={ props.ColorDataTitle23 || "Background Gradient 1" } ColorCardBlock0={ props.ColorCardBlock23 || "" } cssClass={"C_fivefourone_fivethreefivesix "}  />
    </CSSTransition >
<CSSTransition 
        in={_in}
        appear={true} 
        timeout={100}
        classNames={transaction['colorcard']?.animationClass || {}}
    >
    <ColorCard { ...{ ...props, style:false } }   ColorDataTitle0={ props.ColorDataTitle24 || " Background Gradient 2"} ColorHexText0={ props.ColorHexText24 || "1A1A1A #000000" } ColorCardBlock0={ props.ColorCardBlock24 || "" } cssClass={"C_fivenigthtwoo_onesixzeroseven "}  />
    </CSSTransition >
<CSSTransition 
        in={_in}
        appear={true} 
        timeout={100}
        classNames={transaction['colorcard']?.animationClass || {}}
    >
    <ColorCard { ...{ ...props, style:false } }   ColorHexText0={ props.ColorHexText25 || " 1ED760 #000000"} ColorDataTitle0={ props.ColorDataTitle25 || "Background Gradient 3" } ColorCardBlock0={ props.ColorCardBlock25 || "" } cssClass={"C_fiveeightsix_oneeightsevenzero "}  />
    </CSSTransition >
                    </div>
               
    </CSSTransition >
                    </div>
               
    </CSSTransition >
<CSSTransition 
        in={_in}
        appear={true} 
        timeout={100}
        classNames={transaction['title']?.animationClass || {}}
    >
    
                <div id="id_sixtwoozero_onesixthreefour" className={` group title ${ props.onClick ? 'cursor' : '' } ${ transaction['title']?.type ? transaction['title']?.type.toLowerCase() : '' }`} style={{ ...{},  ...props.TitleStyle , transitionDuration: transaction['title']?.duration, transitionTimingFunction: transaction['title']?.timingFunction }} onClick={ props.TitleonClick } onMouseEnter={ props.TitleonMouseEnter } onMouseOver={ props.TitleonMouseOver } onKeyPress={ props.TitleonKeyPress } onDrag={ props.TitleonDrag } onMouseLeave={ props.TitleonMouseLeave } onMouseUp={ props.TitleonMouseUp } onMouseDown={ props.TitleonMouseDown } onKeyDown={ props.TitleonKeyDown } onChange={ props.TitleonChange } ondelay={ props.Titleondelay }>
                                    <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['typography']?.animationClass || {}}>

                                      <span id="id_sixtwoozero_onesixthreefive"  className={` text typography    ${ props.onClick ? 'cursor' : ''}  ${ transaction['typography']?.type ? transaction['typography']?.type.toLowerCase() : '' }`} style={{ ...{},  ...props.TypographyStyle , transitionDuration: transaction['typography']?.duration, transitionTimingFunction: transaction['typography']?.timingFunction }} onClick={ props.TypographyonClick } onMouseEnter={ props.TypographyonMouseEnter } onMouseOver={ props.TypographyonMouseOver } onKeyPress={ props.TypographyonKeyPress } onDrag={ props.TypographyonDrag } onMouseLeave={ props.TypographyonMouseLeave } onMouseUp={ props.TypographyonMouseUp } onMouseDown={ props.TypographyonMouseDown } onKeyDown={ props.TypographyonKeyDown } onChange={ props.TypographyonChange } ondelay={ props.Typographyondelay } >{props.Typography0 || `Typography`}</span>

                                    </CSSTransition>
                                    <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['httpsdeveloperap']?.animationClass || {}}>

                                      <span id="id_sixtwoozero_onesixthreesix"  className={` text httpsdeveloperap    ${ props.onClick ? 'cursor' : ''}  ${ transaction['httpsdeveloperap']?.type ? transaction['httpsdeveloperap']?.type.toLowerCase() : '' }`} style={{ ...{},  ...props.HttpsdeveloperapStyle , transitionDuration: transaction['httpsdeveloperap']?.duration, transitionTimingFunction: transaction['httpsdeveloperap']?.timingFunction }} onClick={ props.HttpsdeveloperaponClick } onMouseEnter={ props.HttpsdeveloperaponMouseEnter } onMouseOver={ props.HttpsdeveloperaponMouseOver } onKeyPress={ props.HttpsdeveloperaponKeyPress } onDrag={ props.HttpsdeveloperaponDrag } onMouseLeave={ props.HttpsdeveloperaponMouseLeave } onMouseUp={ props.HttpsdeveloperaponMouseUp } onMouseDown={ props.HttpsdeveloperaponMouseDown } onKeyDown={ props.HttpsdeveloperaponKeyDown } onChange={ props.HttpsdeveloperaponChange } ondelay={ props.Httpsdeveloperapondelay } >{props.Httpsdeveloperap0 || `https://developer.apple.com/fonts/`}</span>

                                    </CSSTransition>
                                    <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['rectangle']?.animationClass || {}}>
                                      <div id="id_sixtwoozero_onesixthreeseven" className={` rectangle rectangle ${ props.onClick ? 'cursor' : '' } ${ transaction['rectangle']?.type ? transaction['rectangle']?.type.toLowerCase() : '' }`} style={{ ...{},  ...props.RectangleStyle , transitionDuration: transaction['rectangle']?.duration, transitionTimingFunction: transaction['rectangle']?.timingFunction }} onClick={ props.RectangleonClick } onMouseEnter={ props.RectangleonMouseEnter } onMouseOver={ props.RectangleonMouseOver } onKeyPress={ props.RectangleonKeyPress } onDrag={ props.RectangleonDrag } onMouseLeave={ props.RectangleonMouseLeave } onMouseUp={ props.RectangleonMouseUp } onMouseDown={ props.RectangleonMouseDown } onKeyDown={ props.RectangleonKeyDown } onChange={ props.RectangleonChange } ondelay={ props.Rectangleondelay }></div>
                                    </CSSTransition>
                          </div>

                        </CSSTransition>

      </>
      }
    </div>

  </CSSTransition>
</>
    ) 
}

ColorPalette.propTypes = {
    style: PropTypes.any,
Title0: PropTypes.any,
Title1: PropTypes.any,
Title2: PropTypes.any,
Title3: PropTypes.any,
Title4: PropTypes.any,
Descrip0: PropTypes.any,
Descrip1: PropTypes.any,
Descrip2: PropTypes.any,
Backgroundsappliedtocomponentsscreens0: PropTypes.any,
ColorDataTitle0: PropTypes.any,
ColorHexText0: PropTypes.any,
ColorCardBlock0: PropTypes.any,
ColorCardBlockundefined: PropTypes.any,
ColorHexText1: PropTypes.any,
ColorDataTitle1: PropTypes.any,
ColorCardBlock1: PropTypes.any,
ColorDataTitle2: PropTypes.any,
ColorHexText2: PropTypes.any,
ColorCardBlock2: PropTypes.any,
ColorDataTitle3: PropTypes.any,
ColorHexText3: PropTypes.any,
ColorCardBlock3: PropTypes.any,
ColorHexText4: PropTypes.any,
ColorDataTitle4: PropTypes.any,
ColorCardBlock4: PropTypes.any,
ColorDataTitle5: PropTypes.any,
ColorHexText5: PropTypes.any,
ColorCardBlock5: PropTypes.any,
ColorDataTitle6: PropTypes.any,
ColorHexText6: PropTypes.any,
ColorCardBlock6: PropTypes.any,
ColorDataTitle7: PropTypes.any,
ColorHexText7: PropTypes.any,
ColorCardBlock7: PropTypes.any,
ColorHexText8: PropTypes.any,
ColorDataTitle8: PropTypes.any,
ColorCardBlock8: PropTypes.any,
ColorDataTitle9: PropTypes.any,
ColorHexText9: PropTypes.any,
ColorCardBlock9: PropTypes.any,
ColorDataTitle10: PropTypes.any,
ColorHexText10: PropTypes.any,
ColorCardBlock10: PropTypes.any,
ColorDataTitle11: PropTypes.any,
ColorHexText11: PropTypes.any,
ColorCardBlock11: PropTypes.any,
ColorHexText12: PropTypes.any,
ColorDataTitle12: PropTypes.any,
ColorCardBlock12: PropTypes.any,
ColorHexText13: PropTypes.any,
ColorDataTitle13: PropTypes.any,
ColorCardBlock13: PropTypes.any,
ColorDataTitle14: PropTypes.any,
ColorHexText14: PropTypes.any,
ColorCardBlock14: PropTypes.any,
ColorDataTitle15: PropTypes.any,
ColorHexText15: PropTypes.any,
ColorCardBlock15: PropTypes.any,
ColorDataTitle16: PropTypes.any,
ColorHexText16: PropTypes.any,
ColorCardBlock16: PropTypes.any,
ColorHexText18: PropTypes.any,
ColorDataTitle18: PropTypes.any,
ColorCardBlock18: PropTypes.any,
ColorHexText19: PropTypes.any,
ColorDataTitle19: PropTypes.any,
ColorCardBlock19: PropTypes.any,
ColorHexText20: PropTypes.any,
ColorDataTitle20: PropTypes.any,
ColorCardBlock20: PropTypes.any,
ColorHexText21: PropTypes.any,
ColorDataTitle21: PropTypes.any,
ColorCardBlock21: PropTypes.any,
ColorHexText22: PropTypes.any,
ColorDataTitle22: PropTypes.any,
ColorCardBlock22: PropTypes.any,
ColorHexText23: PropTypes.any,
ColorDataTitle23: PropTypes.any,
ColorCardBlock23: PropTypes.any,
ColorDataTitle24: PropTypes.any,
ColorHexText24: PropTypes.any,
ColorCardBlock24: PropTypes.any,
ColorHexText25: PropTypes.any,
ColorDataTitle25: PropTypes.any,
ColorCardBlock25: PropTypes.any,
Typography0: PropTypes.any,
Httpsdeveloperap0: PropTypes.any,
Rectangle0: PropTypes.any,
ColorPaletteonClick: PropTypes.any,
ColorPaletteonMouseEnter: PropTypes.any,
ColorPaletteonMouseOver: PropTypes.any,
ColorPaletteonKeyPress: PropTypes.any,
ColorPaletteonDrag: PropTypes.any,
ColorPaletteonMouseLeave: PropTypes.any,
ColorPaletteonMouseUp: PropTypes.any,
ColorPaletteonMouseDown: PropTypes.any,
ColorPaletteonKeyDown: PropTypes.any,
ColorPaletteonChange: PropTypes.any,
ColorPaletteondelay: PropTypes.any,
HeaderonClick: PropTypes.any,
HeaderonMouseEnter: PropTypes.any,
HeaderonMouseOver: PropTypes.any,
HeaderonKeyPress: PropTypes.any,
HeaderonDrag: PropTypes.any,
HeaderonMouseLeave: PropTypes.any,
HeaderonMouseUp: PropTypes.any,
HeaderonMouseDown: PropTypes.any,
HeaderonKeyDown: PropTypes.any,
HeaderonChange: PropTypes.any,
Headerondelay: PropTypes.any,
TitleonClick: PropTypes.any,
TitleonMouseEnter: PropTypes.any,
TitleonMouseOver: PropTypes.any,
TitleonKeyPress: PropTypes.any,
TitleonDrag: PropTypes.any,
TitleonMouseLeave: PropTypes.any,
TitleonMouseUp: PropTypes.any,
TitleonMouseDown: PropTypes.any,
TitleonKeyDown: PropTypes.any,
TitleonChange: PropTypes.any,
Titleondelay: PropTypes.any,
DescriponClick: PropTypes.any,
DescriponMouseEnter: PropTypes.any,
DescriponMouseOver: PropTypes.any,
DescriponKeyPress: PropTypes.any,
DescriponDrag: PropTypes.any,
DescriponMouseLeave: PropTypes.any,
DescriponMouseUp: PropTypes.any,
DescriponMouseDown: PropTypes.any,
DescriponKeyDown: PropTypes.any,
DescriponChange: PropTypes.any,
Descripondelay: PropTypes.any,
BackgroundsappliedtocomponentsscreensonClick: PropTypes.any,
BackgroundsappliedtocomponentsscreensonMouseEnter: PropTypes.any,
BackgroundsappliedtocomponentsscreensonMouseOver: PropTypes.any,
BackgroundsappliedtocomponentsscreensonKeyPress: PropTypes.any,
BackgroundsappliedtocomponentsscreensonDrag: PropTypes.any,
BackgroundsappliedtocomponentsscreensonMouseLeave: PropTypes.any,
BackgroundsappliedtocomponentsscreensonMouseUp: PropTypes.any,
BackgroundsappliedtocomponentsscreensonMouseDown: PropTypes.any,
BackgroundsappliedtocomponentsscreensonKeyDown: PropTypes.any,
BackgroundsappliedtocomponentsscreensonChange: PropTypes.any,
Backgroundsappliedtocomponentsscreensondelay: PropTypes.any,
SamplesonClick: PropTypes.any,
SamplesonMouseEnter: PropTypes.any,
SamplesonMouseOver: PropTypes.any,
SamplesonKeyPress: PropTypes.any,
SamplesonDrag: PropTypes.any,
SamplesonMouseLeave: PropTypes.any,
SamplesonMouseUp: PropTypes.any,
SamplesonMouseDown: PropTypes.any,
SamplesonKeyDown: PropTypes.any,
SamplesonChange: PropTypes.any,
Samplesondelay: PropTypes.any,
FramefourtwoonigthonClick: PropTypes.any,
FramefourtwoonigthonMouseEnter: PropTypes.any,
FramefourtwoonigthonMouseOver: PropTypes.any,
FramefourtwoonigthonKeyPress: PropTypes.any,
FramefourtwoonigthonDrag: PropTypes.any,
FramefourtwoonigthonMouseLeave: PropTypes.any,
FramefourtwoonigthonMouseUp: PropTypes.any,
FramefourtwoonigthonMouseDown: PropTypes.any,
FramefourtwoonigthonKeyDown: PropTypes.any,
FramefourtwoonigthonChange: PropTypes.any,
Framefourtwoonigthondelay: PropTypes.any,
FramefourthreezeroonClick: PropTypes.any,
FramefourthreezeroonMouseEnter: PropTypes.any,
FramefourthreezeroonMouseOver: PropTypes.any,
FramefourthreezeroonKeyPress: PropTypes.any,
FramefourthreezeroonDrag: PropTypes.any,
FramefourthreezeroonMouseLeave: PropTypes.any,
FramefourthreezeroonMouseUp: PropTypes.any,
FramefourthreezeroonMouseDown: PropTypes.any,
FramefourthreezeroonKeyDown: PropTypes.any,
FramefourthreezeroonChange: PropTypes.any,
Framefourthreezeroondelay: PropTypes.any,
TypographyonClick: PropTypes.any,
TypographyonMouseEnter: PropTypes.any,
TypographyonMouseOver: PropTypes.any,
TypographyonKeyPress: PropTypes.any,
TypographyonDrag: PropTypes.any,
TypographyonMouseLeave: PropTypes.any,
TypographyonMouseUp: PropTypes.any,
TypographyonMouseDown: PropTypes.any,
TypographyonKeyDown: PropTypes.any,
TypographyonChange: PropTypes.any,
Typographyondelay: PropTypes.any,
HttpsdeveloperaponClick: PropTypes.any,
HttpsdeveloperaponMouseEnter: PropTypes.any,
HttpsdeveloperaponMouseOver: PropTypes.any,
HttpsdeveloperaponKeyPress: PropTypes.any,
HttpsdeveloperaponDrag: PropTypes.any,
HttpsdeveloperaponMouseLeave: PropTypes.any,
HttpsdeveloperaponMouseUp: PropTypes.any,
HttpsdeveloperaponMouseDown: PropTypes.any,
HttpsdeveloperaponKeyDown: PropTypes.any,
HttpsdeveloperaponChange: PropTypes.any,
Httpsdeveloperapondelay: PropTypes.any,
RectangleonClick: PropTypes.any,
RectangleonMouseEnter: PropTypes.any,
RectangleonMouseOver: PropTypes.any,
RectangleonKeyPress: PropTypes.any,
RectangleonDrag: PropTypes.any,
RectangleonMouseLeave: PropTypes.any,
RectangleonMouseUp: PropTypes.any,
RectangleonMouseDown: PropTypes.any,
RectangleonKeyDown: PropTypes.any,
RectangleonChange: PropTypes.any,
Rectangleondelay: PropTypes.any
}
export default ColorPalette;