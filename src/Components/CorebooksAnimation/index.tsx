import React, { Children, cloneElement, isValidElement, ReactNode } from "react";
import {
  AnimationType,
  Direction,
  getAnimationClass,
} from "./animations";
import "./CorebooksAnimation.css";
import styled from "styled-components";

type Easing = "ease" | "linear" | "ease-in" | "ease-out" | "ease-in-out";

interface CorebooksAnimationProps {
  animationType: AnimationType;
  children: JSX.Element | JSX.Element[];
  active: boolean;
  easing?: Easing;
  duration?: number | string;
  direction?: Direction;
  origin?: boolean;
}

interface AnimationContainerInterface {
  duration?: number | string;
  easing?: Easing;
}

const AnimationContainer = styled.div`
  & > * {
    transition-duration: ${(props: AnimationContainerInterface) =>
      props.duration ? props.duration : "500ms"};
    transition-timing-function: ${(props) =>
      props.easing ? props.easing : "ease"};
  }
`;

const CorebooksAnimation = (props: CorebooksAnimationProps) => {
  const [_active, setActive] = React.useState(false);
  const { animationType, direction, active, easing, duration, children } =
    props;

 
  React.useEffect(()=>{
    if(active){
      setActive(false)
      setTimeout(()=>setActive(active),5);
    }
    
  },[children])
  return (
    <AnimationContainer
      className={`animation-container ${getAnimationClass(
        animationType,
        _active,
        Children.count(children) === 1,
        direction
      )}`}
      duration={duration}
      easing={easing}
    >
      {children}
    </AnimationContainer>
  );
};

export default CorebooksAnimation;
