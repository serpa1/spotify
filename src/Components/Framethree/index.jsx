import React, { Suspense } from 'react';
import PropTypes from 'prop-types';
import * as Contexts  from 'Contexts';
import { OverlaySwapContext } from 'Components/OverlaySwapProvider';
import CorebooksAnimation from 'Components/CorebooksAnimation';
import CorebooksAnimationSingle from 'Components/CorebooksAnimationSingle';

import * as UtilsScripts  from 'utils/utils';
import { Route, useHistory, useParams } from "react-router-dom";
import SingletoneNavigation from 'Class/SingletoneNavigation';

import LoadingComponent from "Components/Loading";


import { Transition } from "react-transition-group";
import { CSSTransition } from "react-transition-group";
import { useAppContext, useSessionContext } from 'context/AppContext';
import './Framethree.css'





const Framethree = (props)=>{
    const [state,dispatch] = useAppContext();
    const [, setNameSelectedComponent] = React.useContext(OverlaySwapContext);
    
    const nodeRef = React.useRef(null);
    const history = useHistory();
    let RouterContext = useParams();
    
    
    
    
    
    
    const [globalStateForm, dispatchForm] = React.useState({});
    
    const [ _in, setIn ] = React.useState(false)
    const [ transaction, setTransaction ] = React.useState({}) 
    const [ animation, setAnimation ] = React.useState('')

    
    
    
    
    
    
    React.useEffect(()=>{
        
        
        
    },[]);
    
    
    return (
        <>
  <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['framethree']?.animationClass || {}}>

    <div id="id_fourthreeeight_oneeightfourfour" ref={nodeRef} className={` ${ props.onClick ? 'cursor' : '' } framethree ${ props.cssClass } ${ transaction['framethree']?.type ? transaction['framethree']?.type.toLowerCase() : '' }`} style={ { ...{ ...{}, transitionDuration: transaction['framethree']?.duration, transitionTimingFunction: transaction['framethree']?.timingFunction }, ...props.style }} onClick={ props.FramethreeonClick } onMouseEnter={ props.FramethreeonMouseEnter } onMouseOver={ props.FramethreeonMouseOver } onKeyPress={ props.FramethreeonKeyPress } onDrag={ props.FramethreeonDrag } onMouseLeave={ props.FramethreeonMouseLeave } onMouseUp={ props.FramethreeonMouseUp } onMouseDown={ props.FramethreeonMouseDown } onKeyDown={ props.FramethreeonKeyDown } onChange={ props.FramethreeonChange } ondelay={ props.Framethreeondelay }>
      {
      props.children ?
      props.children :
      <>
        <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['vector']?.animationClass || {}}>
          <svg id="id_fourthreeeight_oneeightfourfive" className="vector  " style={{}} onClick={ props.VectoronClick } onMouseEnter={ props.VectoronMouseEnter } onMouseOver={ props.VectoronMouseOver } onKeyPress={ props.VectoronKeyPress } onDrag={ props.VectoronDrag } onMouseLeave={ props.VectoronMouseLeave } onMouseUp={ props.VectoronMouseUp } onMouseDown={ props.VectoronMouseDown } onKeyDown={ props.VectoronKeyDown } onChange={ props.VectoronChange } ondelay={ props.Vectorondelay } width="15" height="15.5">
            <path d="M2 0L2 2L1.23541e-06 2L1.23541e-06 3.5L2 3.5L2 5.5L3.5 5.5L3.5 3.5L5.5 3.5L5.5 2L3.5 2L3.5 0L2 0ZM13.5 2.5L8.244 2.5C8.2217 1.98949 8.12813 1.4846 7.966 1L15 1L15 12.75C15 13.2939 14.8387 13.8256 14.5365 14.2778C14.2344 14.7301 13.8049 15.0825 13.3024 15.2907C12.7999 15.4988 12.247 15.5533 11.7135 15.4472C11.1801 15.3411 10.6901 15.0791 10.3055 14.6945C9.92086 14.3099 9.65895 13.8199 9.55284 13.2865C9.44673 12.753 9.50119 12.2001 9.70933 11.6976C9.91747 11.1951 10.2699 10.7656 10.7222 10.4635C11.1744 10.1613 11.7061 10 12.25 10L13.5 10L13.5 2.5ZM13.5 11.5L12.25 11.5C12.0028 11.5 11.7611 11.5733 11.5555 11.7107C11.35 11.848 11.1898 12.0432 11.0952 12.2716C11.0005 12.5001 10.9758 12.7514 11.024 12.9939C11.0723 13.2363 11.1913 13.4591 11.3661 13.6339C11.5409 13.8087 11.7637 13.9278 12.0061 13.976C12.2486 14.0242 12.4999 13.9995 12.7284 13.9048C12.9568 13.8102 13.152 13.65 13.2893 13.4445C13.4267 13.2389 13.5 12.9972 13.5 12.75L13.5 11.5ZM4 8.107C4.52658 7.98461 5.03208 7.78477 5.5 7.514L5.5 12.75C5.5 13.2939 5.33872 13.8256 5.03654 14.2778C4.73437 14.7301 4.30488 15.0825 3.80238 15.2907C3.29988 15.4988 2.74695 15.5533 2.2135 15.4472C1.68006 15.3411 1.19005 15.0791 0.805458 14.6945C0.420863 14.3099 0.158951 13.8199 0.0528417 13.2865C-0.0532676 12.753 0.00119154 12.2001 0.209333 11.6976C0.417473 11.1951 0.769948 10.7656 1.22218 10.4635C1.67442 10.1613 2.2061 10 2.75 10L4 10L4 8.107ZM4 11.5L2.75 11.5C2.50277 11.5 2.2611 11.5733 2.05554 11.7107C1.84998 11.848 1.68976 12.0432 1.59515 12.2716C1.50054 12.5001 1.47579 12.7514 1.52402 12.9939C1.57225 13.2363 1.6913 13.4591 1.86612 13.6339C2.04093 13.8087 2.26366 13.9278 2.50614 13.976C2.74861 14.0242 2.99995 13.9995 3.22836 13.9048C3.45676 13.8102 3.65199 13.65 3.78934 13.4445C3.92669 13.2389 4 12.9972 4 12.75L4 11.5Z" />
          </svg>
        </CSSTransition>

      </>
      }
    </div>

  </CSSTransition>
</>
    ) 
}

Framethree.propTypes = {
    style: PropTypes.any,
FramethreeonClick: PropTypes.any,
FramethreeonMouseEnter: PropTypes.any,
FramethreeonMouseOver: PropTypes.any,
FramethreeonKeyPress: PropTypes.any,
FramethreeonDrag: PropTypes.any,
FramethreeonMouseLeave: PropTypes.any,
FramethreeonMouseUp: PropTypes.any,
FramethreeonMouseDown: PropTypes.any,
FramethreeonKeyDown: PropTypes.any,
FramethreeonChange: PropTypes.any,
Framethreeondelay: PropTypes.any,
VectoronClick: PropTypes.any,
VectoronMouseEnter: PropTypes.any,
VectoronMouseOver: PropTypes.any,
VectoronKeyPress: PropTypes.any,
VectoronDrag: PropTypes.any,
VectoronMouseLeave: PropTypes.any,
VectoronMouseUp: PropTypes.any,
VectoronMouseDown: PropTypes.any,
VectoronKeyDown: PropTypes.any,
VectoronChange: PropTypes.any,
Vectorondelay: PropTypes.any
}
export default Framethree;