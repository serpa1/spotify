import React, { Suspense } from 'react';
import PropTypes from 'prop-types';
import * as Contexts  from 'Contexts';
import { OverlaySwapContext } from 'Components/OverlaySwapProvider';
import CorebooksAnimation from 'Components/CorebooksAnimation';
import CorebooksAnimationSingle from 'Components/CorebooksAnimationSingle';

import * as UtilsScripts  from 'utils/utils';
import { Route, useHistory, useParams } from "react-router-dom";
import SingletoneNavigation from 'Class/SingletoneNavigation';

import LoadingComponent from "Components/Loading";


import { Transition } from "react-transition-group";
import { CSSTransition } from "react-transition-group";
import { useAppContext, useSessionContext } from 'context/AppContext';
import './Rectangleseven.css'





const Rectangleseven = (props)=>{
    const [state,dispatch] = useAppContext();
    const [, setNameSelectedComponent] = React.useContext(OverlaySwapContext);
    
    const nodeRefpropertyonedefault = React.useRef(null);
const nodeRefpropertyonevarianttwoo = React.useRef(null);
    const history = useHistory();
    let RouterContext = useParams();
    
    
    
    
    
    
    const [globalStateForm, dispatchForm] = React.useState({});
    const [ variant, setvariant] = React.useState(props.variant || 'Property 1=Default')
    const [ _in, setIn ] = React.useState(false)
    const [ transaction, setTransaction ] = React.useState({}) 
    const [ animation, setAnimation ] = React.useState('')

    
    
    
    
    
    
    React.useEffect(()=>{
        
        
        
    },[]);
    
    
    React.useEffect(()=>{
        
        setIn(false);
        setTimeout(()=>setIn(true))
           
      },[variant]);
    const switchVariant = (value)=>{
        switch (value) {
            case 'Property 1=Default':{
            return (
            <>

  <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['propertyonedefault']?.animationClass || {}}>

    <div id="id_fourthreeeight_fourtwoooneeight" ref={nodeRefpropertyonedefault} className={` cursor ${ props.onClick ? 'cursor' : '' } rectanglesevenpropertyonedefault C_fourthreeeight_fourtwoooneeight ${ props.cssClass } ${ transaction['propertyonedefault']?.type ? transaction['propertyonedefault']?.type.toLowerCase() : '' }`} style={ { ...{ ...{}, transitionDuration: transaction['propertyonedefault']?.duration, transitionTimingFunction: transaction['propertyonedefault']?.timingFunction }, ...props.style }} onClick={ props.RectanglesevenonClick } onMouseEnter={ props.RectanglesevenonMouseEnter } onMouseOver={ props.RectanglesevenonMouseOver || function(e){ setTransaction({ }); setvariant('Property 1=Variant2'); }} onKeyPress={ props.RectanglesevenonKeyPress } onDrag={ props.RectanglesevenonDrag } onMouseLeave={ props.RectanglesevenonMouseLeave } onMouseUp={ props.RectanglesevenonMouseUp } onMouseDown={ props.RectanglesevenonMouseDown } onKeyDown={ props.RectanglesevenonKeyDown } onChange={ props.RectanglesevenonChange } ondelay={ props.Rectanglesevenondelay }>
      {
      props.children ?
      props.children :
      <>
        <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['rectangleseven']?.animationClass || {}}>
          <img id="id_fourthreeeight_fourtwoozeroeight" className={` rectangle rectanglesevenrectangleseven ${ props.onClick ? 'cursor' : '' } ${ transaction['rectangleseven']?.type ? transaction['rectangleseven']?.type.toLowerCase() : '' }`} style={{ ...{},  ...props.RectanglesevenStyle , transitionDuration: transaction['rectangleseven']?.duration, transitionTimingFunction: transaction['rectangleseven']?.timingFunction }} onClick={ props.RectanglesevenonClick } onMouseEnter={ props.RectanglesevenonMouseEnter } onMouseOver={ props.RectanglesevenonMouseOver } onKeyPress={ props.RectanglesevenonKeyPress } onDrag={ props.RectanglesevenonDrag } onMouseLeave={ props.RectanglesevenonMouseLeave } onMouseUp={ props.RectanglesevenonMouseUp } onMouseDown={ props.RectanglesevenonMouseDown } onKeyDown={ props.RectanglesevenonKeyDown } onChange={ props.RectanglesevenonChange } ondelay={ props.Rectanglesevenondelay } src={props.Rectangleseven0 || "https://cdn.uncodie.com/tACQBrKCV97e0EqPE1XNLJ/546fe21b25fa391853465ec8c881f4ae670c0ce2.png" } />
        </CSSTransition>

      </>
      }
    </div>

  </CSSTransition>

</>
            )
        }
case 'Property 1=Variant2':{
            return (
            <>

  <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['propertyonevarianttwoo']?.animationClass || {}}>

    <div id="id_fourthreeeight_fourtwootwoozero" ref={nodeRefpropertyonevarianttwoo} className={` cursor ${ props.onClick ? 'cursor' : '' } rectanglesevenpropertyonevarianttwoo C_fourthreeeight_fourtwootwoozero ${ props.cssClass } ${ transaction['propertyonevarianttwoo']?.type ? transaction['propertyonevarianttwoo']?.type.toLowerCase() : '' }`} style={ { ...{ ...{}, transitionDuration: transaction['propertyonevarianttwoo']?.duration, transitionTimingFunction: transaction['propertyonevarianttwoo']?.timingFunction }, ...props.style }} onClick={ props.RectanglesevenonClick } onMouseEnter={ props.RectanglesevenonMouseEnter } onMouseOver={ props.RectanglesevenonMouseOver } onKeyPress={ props.RectanglesevenonKeyPress } onDrag={ props.RectanglesevenonDrag } onMouseLeave={ props.RectanglesevenonMouseLeave || function(e){ setTransaction({ }); setvariant('Property 1=Default'); }} onMouseUp={ props.RectanglesevenonMouseUp } onMouseDown={ props.RectanglesevenonMouseDown } onKeyDown={ props.RectanglesevenonKeyDown } onChange={ props.RectanglesevenonChange } ondelay={ props.Rectanglesevenondelay }>
      {
      props.children ?
      props.children :
      <>
        <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['rectangleseven']?.animationClass || {}}>
          <img id="id_fourthreeeight_fourtwootwooone" className={` rectangle rectanglesevenrectangleseven ${ props.onClick ? 'cursor' : '' } ${ transaction['rectangleseven']?.type ? transaction['rectangleseven']?.type.toLowerCase() : '' }`} style={{ ...{},  ...props.RectanglesevenStyle , transitionDuration: transaction['rectangleseven']?.duration, transitionTimingFunction: transaction['rectangleseven']?.timingFunction }} onClick={ props.RectanglesevenonClick } onMouseEnter={ props.RectanglesevenonMouseEnter } onMouseOver={ props.RectanglesevenonMouseOver } onKeyPress={ props.RectanglesevenonKeyPress } onDrag={ props.RectanglesevenonDrag } onMouseLeave={ props.RectanglesevenonMouseLeave } onMouseUp={ props.RectanglesevenonMouseUp } onMouseDown={ props.RectanglesevenonMouseDown } onKeyDown={ props.RectanglesevenonKeyDown } onChange={ props.RectanglesevenonChange } ondelay={ props.Rectanglesevenondelay } src={props.Rectangleseven0 || "https://cdn.uncodie.com/tACQBrKCV97e0EqPE1XNLJ/b277caa3368781c7e54813b9d26655128939215b.png" } />
        </CSSTransition>

      </>
      }
    </div>

  </CSSTransition>

</>
            )
        }
        }
    }

    return (
        <>
            {switchVariant(variant)}
        </>
    )
    
}

Rectangleseven.propTypes = {
    style: PropTypes.any,
variant: PropTypes.any,
Rectangleseven0: PropTypes.any,
RectanglesevenonClick: PropTypes.any,
RectanglesevenonMouseEnter: PropTypes.any,
RectanglesevenonMouseOver: PropTypes.any,
RectanglesevenonKeyPress: PropTypes.any,
RectanglesevenonDrag: PropTypes.any,
RectanglesevenonMouseLeave: PropTypes.any,
RectanglesevenonMouseUp: PropTypes.any,
RectanglesevenonMouseDown: PropTypes.any,
RectanglesevenonKeyDown: PropTypes.any,
RectanglesevenonChange: PropTypes.any,
Rectanglesevenondelay: PropTypes.any
}
export default Rectangleseven;