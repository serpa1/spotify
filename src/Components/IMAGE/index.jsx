import React, { Suspense } from 'react';
import PropTypes from 'prop-types';
import * as Contexts  from 'Contexts';
import { OverlaySwapContext } from 'Components/OverlaySwapProvider';
import CorebooksAnimation from 'Components/CorebooksAnimation';
import CorebooksAnimationSingle from 'Components/CorebooksAnimationSingle';

import * as UtilsScripts  from 'utils/utils';
import { Route, useHistory, useParams } from "react-router-dom";
import SingletoneNavigation from 'Class/SingletoneNavigation';

import LoadingComponent from "Components/Loading";


import { Transition } from "react-transition-group";
import { CSSTransition } from "react-transition-group";
import { useAppContext, useSessionContext } from 'context/AppContext';
import './IMAGE.css'





const IMAGE = (props)=>{
    const [state,dispatch] = useAppContext();
    const [, setNameSelectedComponent] = React.useContext(OverlaySwapContext);
    
    const nodeRef = React.useRef(null);
    const history = useHistory();
    let RouterContext = useParams();
    
    
    
    
    
    
    const [globalStateForm, dispatchForm] = React.useState({});
    
    const [ _in, setIn ] = React.useState(false)
    const [ transaction, setTransaction ] = React.useState({}) 
    const [ animation, setAnimation ] = React.useState('')

    
    
    
    
    
    
    React.useEffect(()=>{
        
        
        
    },[]);
    
    
    return (
        <>
  <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['image']?.animationClass || {}}>

    <div id="id_fivefivenigth_onenigthtwooone" ref={nodeRef} className={` ${ props.onClick ? 'cursor' : '' } image ${ props.cssClass } ${ transaction['image']?.type ? transaction['image']?.type.toLowerCase() : '' }`} style={ { ...{ ...{}, transitionDuration: transaction['image']?.duration, transitionTimingFunction: transaction['image']?.timingFunction }, ...props.style }} onClick={ props.IMAGEonClick } onMouseEnter={ props.IMAGEonMouseEnter } onMouseOver={ props.IMAGEonMouseOver } onKeyPress={ props.IMAGEonKeyPress } onDrag={ props.IMAGEonDrag } onMouseLeave={ props.IMAGEonMouseLeave } onMouseUp={ props.IMAGEonMouseUp } onMouseDown={ props.IMAGEonMouseDown } onKeyDown={ props.IMAGEonKeyDown } onChange={ props.IMAGEonChange } ondelay={ props.IMAGEondelay }>
      {
      props.children ?
      props.children :
      <>
        <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['pathfivefive']?.animationClass || {}}>
          <svg id="id_fivefivenigth_onenigthtwootwoo" className="pathfivefive  " style={{}} onClick={ props.PathfivefiveonClick } onMouseEnter={ props.PathfivefiveonMouseEnter } onMouseOver={ props.PathfivefiveonMouseOver } onKeyPress={ props.PathfivefiveonKeyPress } onDrag={ props.PathfivefiveonDrag } onMouseLeave={ props.PathfivefiveonMouseLeave } onMouseUp={ props.PathfivefiveonMouseUp } onMouseDown={ props.PathfivefiveonMouseDown } onKeyDown={ props.PathfivefiveonKeyDown } onChange={ props.PathfivefiveonChange } ondelay={ props.Pathfivefiveondelay } width="8.09228515625" height="6.122314453125">

          </svg>
        </CSSTransition>

      </>
      }
    </div>

  </CSSTransition>
</>
    ) 
}

IMAGE.propTypes = {
    style: PropTypes.any,
IMAGEonClick: PropTypes.any,
IMAGEonMouseEnter: PropTypes.any,
IMAGEonMouseOver: PropTypes.any,
IMAGEonKeyPress: PropTypes.any,
IMAGEonDrag: PropTypes.any,
IMAGEonMouseLeave: PropTypes.any,
IMAGEonMouseUp: PropTypes.any,
IMAGEonMouseDown: PropTypes.any,
IMAGEonKeyDown: PropTypes.any,
IMAGEonChange: PropTypes.any,
IMAGEondelay: PropTypes.any,
PathfivefiveonClick: PropTypes.any,
PathfivefiveonMouseEnter: PropTypes.any,
PathfivefiveonMouseOver: PropTypes.any,
PathfivefiveonKeyPress: PropTypes.any,
PathfivefiveonDrag: PropTypes.any,
PathfivefiveonMouseLeave: PropTypes.any,
PathfivefiveonMouseUp: PropTypes.any,
PathfivefiveonMouseDown: PropTypes.any,
PathfivefiveonKeyDown: PropTypes.any,
PathfivefiveonChange: PropTypes.any,
Pathfivefiveondelay: PropTypes.any
}
export default IMAGE;