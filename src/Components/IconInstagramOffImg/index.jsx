import React, { Suspense } from 'react';
import PropTypes from 'prop-types';
import * as Contexts  from 'Contexts';
import { OverlaySwapContext } from 'Components/OverlaySwapProvider';
import CorebooksAnimation from 'Components/CorebooksAnimation';
import CorebooksAnimationSingle from 'Components/CorebooksAnimationSingle';

import * as UtilsScripts  from 'utils/utils';
import { Route, useHistory, useParams } from "react-router-dom";
import SingletoneNavigation from 'Class/SingletoneNavigation';

import LoadingComponent from "Components/Loading";


import { Transition } from "react-transition-group";
import { CSSTransition } from "react-transition-group";
import { useAppContext, useSessionContext } from 'context/AppContext';
import './IconInstagramOffImg.css'





const IconInstagramOffImg = (props)=>{
    const [state,dispatch] = useAppContext();
    const [, setNameSelectedComponent] = React.useContext(OverlaySwapContext);
    
    const nodeRefpropertyonedefault = React.useRef(null);
const nodeRefpropertyonevarianttwoo = React.useRef(null);
    const history = useHistory();
    let RouterContext = useParams();
    
    
    
    
    
    
    const [globalStateForm, dispatchForm] = React.useState({});
    const [ variant, setvariant] = React.useState(props.variant || 'Property 1=Default')
    const [ _in, setIn ] = React.useState(false)
    const [ transaction, setTransaction ] = React.useState({}) 
    const [ animation, setAnimation ] = React.useState('')

    
    
    
    
    
    
    React.useEffect(()=>{
        
        
        
    },[]);
    
    
    React.useEffect(()=>{
        
        setIn(false);
        setTimeout(()=>setIn(true))
           
      },[variant]);
    const switchVariant = (value)=>{
        switch (value) {
            case 'Property 1=Default':{
            return (
            <>

  <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['propertyonedefault']?.animationClass || {}}>

    <div id="id_onesix_oneeightfive" ref={nodeRefpropertyonedefault} className={` cursor ${ props.onClick ? 'cursor' : '' } iconinstagramoffimgpropertyonedefault C_onesix_oneeightfive ${ props.cssClass } ${ transaction['propertyonedefault']?.type ? transaction['propertyonedefault']?.type.toLowerCase() : '' }`} style={ { ...{ ...{}, transitionDuration: transaction['propertyonedefault']?.duration, transitionTimingFunction: transaction['propertyonedefault']?.timingFunction }, ...props.style }} onClick={ props.IconInstagramOffImgonClick } onMouseEnter={ props.IconInstagramOffImgonMouseEnter } onMouseOver={ props.IconInstagramOffImgonMouseOver || function(e){ setTransaction({ }); setvariant('Property 1=Variant2'); }} onKeyPress={ props.IconInstagramOffImgonKeyPress } onDrag={ props.IconInstagramOffImgonDrag } onMouseLeave={ props.IconInstagramOffImgonMouseLeave } onMouseUp={ props.IconInstagramOffImgonMouseUp } onMouseDown={ props.IconInstagramOffImgonMouseDown } onKeyDown={ props.IconInstagramOffImgonKeyDown } onChange={ props.IconInstagramOffImgonChange } ondelay={ props.IconInstagramOffImgondelay }>
      {
      props.children ?
      props.children :
      <>
        <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['iconinstagramoffimg']?.animationClass || {}}>
          <img id="id_onesix_oneeighttwoo" className={` rectangle iconinstagramoffimgiconinstagramoffimg ${ props.onClick ? 'cursor' : '' } ${ transaction['iconinstagramoffimg']?.type ? transaction['iconinstagramoffimg']?.type.toLowerCase() : '' }`} style={{ ...{},  ...props.IconInstagramOffImgStyle , transitionDuration: transaction['iconinstagramoffimg']?.duration, transitionTimingFunction: transaction['iconinstagramoffimg']?.timingFunction }} onClick={ props.IconInstagramOffImgonClick } onMouseEnter={ props.IconInstagramOffImgonMouseEnter } onMouseOver={ props.IconInstagramOffImgonMouseOver } onKeyPress={ props.IconInstagramOffImgonKeyPress } onDrag={ props.IconInstagramOffImgonDrag } onMouseLeave={ props.IconInstagramOffImgonMouseLeave } onMouseUp={ props.IconInstagramOffImgonMouseUp } onMouseDown={ props.IconInstagramOffImgonMouseDown } onKeyDown={ props.IconInstagramOffImgonKeyDown } onChange={ props.IconInstagramOffImgonChange } ondelay={ props.IconInstagramOffImgondelay } src={props.IconInstagramOffImg0 || "https://cdn.uncodie.com/tACQBrKCV97e0EqPE1XNLJ/aa5e38cc3adc8a07d8ca20ddf83e756f5be57d6f.png" } />
        </CSSTransition>

      </>
      }
    </div>

  </CSSTransition>

</>
            )
        }
case 'Property 1=Variant2':{
            return (
            <>

  <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['propertyonevarianttwoo']?.animationClass || {}}>

    <div id="id_onesix_oneeightseven" ref={nodeRefpropertyonevarianttwoo} className={` cursor ${ props.onClick ? 'cursor' : '' } iconinstagramoffimgpropertyonevarianttwoo C_onesix_oneeightseven ${ props.cssClass } ${ transaction['propertyonevarianttwoo']?.type ? transaction['propertyonevarianttwoo']?.type.toLowerCase() : '' }`} style={ { ...{ ...{}, transitionDuration: transaction['propertyonevarianttwoo']?.duration, transitionTimingFunction: transaction['propertyonevarianttwoo']?.timingFunction }, ...props.style }} onClick={ props.IconInstagramOffImgonClick } onMouseEnter={ props.IconInstagramOffImgonMouseEnter } onMouseOver={ props.IconInstagramOffImgonMouseOver } onKeyPress={ props.IconInstagramOffImgonKeyPress } onDrag={ props.IconInstagramOffImgonDrag } onMouseLeave={ props.IconInstagramOffImgonMouseLeave || function(e){ setTransaction({ }); setvariant('Property 1=Default'); }} onMouseUp={ props.IconInstagramOffImgonMouseUp } onMouseDown={ props.IconInstagramOffImgonMouseDown } onKeyDown={ props.IconInstagramOffImgonKeyDown } onChange={ props.IconInstagramOffImgonChange } ondelay={ props.IconInstagramOffImgondelay }>
      {
      props.children ?
      props.children :
      <>
        <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['iconinstagramoffimg']?.animationClass || {}}>
          <img id="id_onesix_oneeighteight" className={` rectangle iconinstagramoffimgiconinstagramoffimg ${ props.onClick ? 'cursor' : '' } ${ transaction['iconinstagramoffimg']?.type ? transaction['iconinstagramoffimg']?.type.toLowerCase() : '' }`} style={{ ...{},  ...props.IconInstagramOffImgStyle , transitionDuration: transaction['iconinstagramoffimg']?.duration, transitionTimingFunction: transaction['iconinstagramoffimg']?.timingFunction }} onClick={ props.IconInstagramOffImgonClick } onMouseEnter={ props.IconInstagramOffImgonMouseEnter } onMouseOver={ props.IconInstagramOffImgonMouseOver } onKeyPress={ props.IconInstagramOffImgonKeyPress } onDrag={ props.IconInstagramOffImgonDrag } onMouseLeave={ props.IconInstagramOffImgonMouseLeave } onMouseUp={ props.IconInstagramOffImgonMouseUp } onMouseDown={ props.IconInstagramOffImgonMouseDown } onKeyDown={ props.IconInstagramOffImgonKeyDown } onChange={ props.IconInstagramOffImgonChange } ondelay={ props.IconInstagramOffImgondelay } src={props.IconInstagramOffImg0 || "https://cdn.uncodie.com/tACQBrKCV97e0EqPE1XNLJ/b798b5280dd95b22b8ec7f8c7bca9887731abfe8.png" } />
        </CSSTransition>

      </>
      }
    </div>

  </CSSTransition>

</>
            )
        }
        }
    }

    return (
        <>
            {switchVariant(variant)}
        </>
    )
    
}

IconInstagramOffImg.propTypes = {
    style: PropTypes.any,
variant: PropTypes.any,
IconInstagramOffImg0: PropTypes.any,
IconInstagramOffImgonClick: PropTypes.any,
IconInstagramOffImgonMouseEnter: PropTypes.any,
IconInstagramOffImgonMouseOver: PropTypes.any,
IconInstagramOffImgonKeyPress: PropTypes.any,
IconInstagramOffImgonDrag: PropTypes.any,
IconInstagramOffImgonMouseLeave: PropTypes.any,
IconInstagramOffImgonMouseUp: PropTypes.any,
IconInstagramOffImgonMouseDown: PropTypes.any,
IconInstagramOffImgonKeyDown: PropTypes.any,
IconInstagramOffImgonChange: PropTypes.any,
IconInstagramOffImgondelay: PropTypes.any
}
export default IconInstagramOffImg;