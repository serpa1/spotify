import React, { Suspense } from 'react';
import PropTypes from 'prop-types';
import * as Contexts  from 'Contexts';
import { OverlaySwapContext } from 'Components/OverlaySwapProvider';
import CorebooksAnimation from 'Components/CorebooksAnimation';
import CorebooksAnimationSingle from 'Components/CorebooksAnimationSingle';

import * as UtilsScripts  from 'utils/utils';
import { Route, useHistory, useParams } from "react-router-dom";
import SingletoneNavigation from 'Class/SingletoneNavigation';

import LoadingComponent from "Components/Loading";


import { Transition } from "react-transition-group";
import { CSSTransition } from "react-transition-group";
import { useAppContext, useSessionContext } from 'context/AppContext';
import './Icons.css'





const Icons = (props)=>{
    const [state,dispatch] = useAppContext();
    const [, setNameSelectedComponent] = React.useContext(OverlaySwapContext);
    
    const nodeRef = React.useRef(null);
    const history = useHistory();
    let RouterContext = useParams();
    
    
    
    
    
    
    const [globalStateForm, dispatchForm] = React.useState({});
    
    const [ _in, setIn ] = React.useState(false)
    const [ transaction, setTransaction ] = React.useState({}) 
    const [ animation, setAnimation ] = React.useState('')

    
    
    
    
    
    
    React.useEffect(()=>{
        
        
        
    },[]);
    
    
    return (
        <>
  <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['icons']?.animationClass || {}}>

    <div id="id_one_eightsixseven" ref={nodeRef} className={` ${ props.onClick ? 'cursor' : '' } icons ${ props.cssClass } ${ transaction['icons']?.type ? transaction['icons']?.type.toLowerCase() : '' }`} style={ { ...{ ...{}, transitionDuration: transaction['icons']?.duration, transitionTimingFunction: transaction['icons']?.timingFunction }, ...props.style }} onClick={ props.IconsonClick } onMouseEnter={ props.IconsonMouseEnter } onMouseOver={ props.IconsonMouseOver } onKeyPress={ props.IconsonKeyPress } onDrag={ props.IconsonDrag } onMouseLeave={ props.IconsonMouseLeave } onMouseUp={ props.IconsonMouseUp } onMouseDown={ props.IconsonMouseDown } onKeyDown={ props.IconsonKeyDown } onChange={ props.IconsonChange } ondelay={ props.Iconsondelay }>
      {
      props.children ?
      props.children :
      <>
        <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['iconhomehoverimg']?.animationClass || {}}>
          <img id="id_one_eightseventwoo" className={` rectangle iconhomehoverimg ${ props.onClick ? 'cursor' : '' } ${ transaction['iconhomehoverimg']?.type ? transaction['iconhomehoverimg']?.type.toLowerCase() : '' }`} style={{ ...{},  ...props.IconHomeHoverImgStyle , transitionDuration: transaction['iconhomehoverimg']?.duration, transitionTimingFunction: transaction['iconhomehoverimg']?.timingFunction }} onClick={ props.IconHomeHoverImgonClick } onMouseEnter={ props.IconHomeHoverImgonMouseEnter } onMouseOver={ props.IconHomeHoverImgonMouseOver } onKeyPress={ props.IconHomeHoverImgonKeyPress } onDrag={ props.IconHomeHoverImgonDrag } onMouseLeave={ props.IconHomeHoverImgonMouseLeave } onMouseUp={ props.IconHomeHoverImgonMouseUp } onMouseDown={ props.IconHomeHoverImgonMouseDown } onKeyDown={ props.IconHomeHoverImgonKeyDown } onChange={ props.IconHomeHoverImgonChange } ondelay={ props.IconHomeHoverImgondelay } src={props.IconHomeHoverImg0 || "https://cdn.uncodie.com/tACQBrKCV97e0EqPE1XNLJ/30e3640bf22c1a9d6bdac3b75c82cd258539c52d.png" } />
        </CSSTransition>
        <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['iconhomeoffimg']?.animationClass || {}}>
          <img id="id_one_eightseventhree" className={` rectangle iconhomeoffimg ${ props.onClick ? 'cursor' : '' } ${ transaction['iconhomeoffimg']?.type ? transaction['iconhomeoffimg']?.type.toLowerCase() : '' }`} style={{ ...{},  ...props.IconHomeOffImgStyle , transitionDuration: transaction['iconhomeoffimg']?.duration, transitionTimingFunction: transaction['iconhomeoffimg']?.timingFunction }} onClick={ props.IconHomeOffImgonClick } onMouseEnter={ props.IconHomeOffImgonMouseEnter } onMouseOver={ props.IconHomeOffImgonMouseOver } onKeyPress={ props.IconHomeOffImgonKeyPress } onDrag={ props.IconHomeOffImgonDrag } onMouseLeave={ props.IconHomeOffImgonMouseLeave } onMouseUp={ props.IconHomeOffImgonMouseUp } onMouseDown={ props.IconHomeOffImgonMouseDown } onKeyDown={ props.IconHomeOffImgonKeyDown } onChange={ props.IconHomeOffImgonChange } ondelay={ props.IconHomeOffImgondelay } src={props.IconHomeOffImg0 || "https://cdn.uncodie.com/tACQBrKCV97e0EqPE1XNLJ/1d5fe79f64d3bd4027116d758fac821a986b91f0.png" } />
        </CSSTransition>
        <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['iconhomeonimg']?.animationClass || {}}>
          <img id="id_one_eightsevenfour" className={` rectangle iconhomeonimg ${ props.onClick ? 'cursor' : '' } ${ transaction['iconhomeonimg']?.type ? transaction['iconhomeonimg']?.type.toLowerCase() : '' }`} style={{ ...{},  ...props.IconHomeOnImgStyle , transitionDuration: transaction['iconhomeonimg']?.duration, transitionTimingFunction: transaction['iconhomeonimg']?.timingFunction }} onClick={ props.IconHomeOnImgonClick } onMouseEnter={ props.IconHomeOnImgonMouseEnter } onMouseOver={ props.IconHomeOnImgonMouseOver } onKeyPress={ props.IconHomeOnImgonKeyPress } onDrag={ props.IconHomeOnImgonDrag } onMouseLeave={ props.IconHomeOnImgonMouseLeave } onMouseUp={ props.IconHomeOnImgonMouseUp } onMouseDown={ props.IconHomeOnImgonMouseDown } onKeyDown={ props.IconHomeOnImgonKeyDown } onChange={ props.IconHomeOnImgonChange } ondelay={ props.IconHomeOnImgondelay } src={props.IconHomeOnImg0 || "https://cdn.uncodie.com/tACQBrKCV97e0EqPE1XNLJ/7b2a59aaea29e05364302357785445049a834fdd.png" } />
        </CSSTransition>
        <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['iconsearchhoverimg']?.animationClass || {}}>
          <img id="id_one_eightsevenfive" className={` rectangle iconsearchhoverimg ${ props.onClick ? 'cursor' : '' } ${ transaction['iconsearchhoverimg']?.type ? transaction['iconsearchhoverimg']?.type.toLowerCase() : '' }`} style={{ ...{},  ...props.IconSearchHoverImgStyle , transitionDuration: transaction['iconsearchhoverimg']?.duration, transitionTimingFunction: transaction['iconsearchhoverimg']?.timingFunction }} onClick={ props.IconSearchHoverImgonClick } onMouseEnter={ props.IconSearchHoverImgonMouseEnter } onMouseOver={ props.IconSearchHoverImgonMouseOver } onKeyPress={ props.IconSearchHoverImgonKeyPress } onDrag={ props.IconSearchHoverImgonDrag } onMouseLeave={ props.IconSearchHoverImgonMouseLeave } onMouseUp={ props.IconSearchHoverImgonMouseUp } onMouseDown={ props.IconSearchHoverImgonMouseDown } onKeyDown={ props.IconSearchHoverImgonKeyDown } onChange={ props.IconSearchHoverImgonChange } ondelay={ props.IconSearchHoverImgondelay } src={props.IconSearchHoverImg0 || "https://cdn.uncodie.com/tACQBrKCV97e0EqPE1XNLJ/96d2240661b8f8598c5cc23e218d674939486db0.png" } />
        </CSSTransition>
        <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['iconsearchoffimg']?.animationClass || {}}>
          <img id="id_one_eightsevensix" className={` rectangle iconsearchoffimg ${ props.onClick ? 'cursor' : '' } ${ transaction['iconsearchoffimg']?.type ? transaction['iconsearchoffimg']?.type.toLowerCase() : '' }`} style={{ ...{},  ...props.IconSearchOffImgStyle , transitionDuration: transaction['iconsearchoffimg']?.duration, transitionTimingFunction: transaction['iconsearchoffimg']?.timingFunction }} onClick={ props.IconSearchOffImgonClick } onMouseEnter={ props.IconSearchOffImgonMouseEnter } onMouseOver={ props.IconSearchOffImgonMouseOver } onKeyPress={ props.IconSearchOffImgonKeyPress } onDrag={ props.IconSearchOffImgonDrag } onMouseLeave={ props.IconSearchOffImgonMouseLeave } onMouseUp={ props.IconSearchOffImgonMouseUp } onMouseDown={ props.IconSearchOffImgonMouseDown } onKeyDown={ props.IconSearchOffImgonKeyDown } onChange={ props.IconSearchOffImgonChange } ondelay={ props.IconSearchOffImgondelay } src={props.IconSearchOffImg0 || "https://cdn.uncodie.com/tACQBrKCV97e0EqPE1XNLJ/b3ded32c629681455af7d021e462a98b4e8d49d0.png" } />
        </CSSTransition>
        <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['iconsearchonimg']?.animationClass || {}}>
          <img id="id_one_eightsevenseven" className={` rectangle iconsearchonimg ${ props.onClick ? 'cursor' : '' } ${ transaction['iconsearchonimg']?.type ? transaction['iconsearchonimg']?.type.toLowerCase() : '' }`} style={{ ...{},  ...props.IconSearchOnImgStyle , transitionDuration: transaction['iconsearchonimg']?.duration, transitionTimingFunction: transaction['iconsearchonimg']?.timingFunction }} onClick={ props.IconSearchOnImgonClick } onMouseEnter={ props.IconSearchOnImgonMouseEnter } onMouseOver={ props.IconSearchOnImgonMouseOver } onKeyPress={ props.IconSearchOnImgonKeyPress } onDrag={ props.IconSearchOnImgonDrag } onMouseLeave={ props.IconSearchOnImgonMouseLeave } onMouseUp={ props.IconSearchOnImgonMouseUp } onMouseDown={ props.IconSearchOnImgonMouseDown } onKeyDown={ props.IconSearchOnImgonKeyDown } onChange={ props.IconSearchOnImgonChange } ondelay={ props.IconSearchOnImgondelay } src={props.IconSearchOnImg0 || "https://cdn.uncodie.com/tACQBrKCV97e0EqPE1XNLJ/92ccdad0bd76a25b94fe88c04e08a0cec3b242e3.png" } />
        </CSSTransition>
        <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['iconlibraryhoverimg']?.animationClass || {}}>
          <img id="id_one_eightseveneight" className={` rectangle iconlibraryhoverimg ${ props.onClick ? 'cursor' : '' } ${ transaction['iconlibraryhoverimg']?.type ? transaction['iconlibraryhoverimg']?.type.toLowerCase() : '' }`} style={{ ...{},  ...props.IconLibraryHoverImgStyle , transitionDuration: transaction['iconlibraryhoverimg']?.duration, transitionTimingFunction: transaction['iconlibraryhoverimg']?.timingFunction }} onClick={ props.IconLibraryHoverImgonClick } onMouseEnter={ props.IconLibraryHoverImgonMouseEnter } onMouseOver={ props.IconLibraryHoverImgonMouseOver } onKeyPress={ props.IconLibraryHoverImgonKeyPress } onDrag={ props.IconLibraryHoverImgonDrag } onMouseLeave={ props.IconLibraryHoverImgonMouseLeave } onMouseUp={ props.IconLibraryHoverImgonMouseUp } onMouseDown={ props.IconLibraryHoverImgonMouseDown } onKeyDown={ props.IconLibraryHoverImgonKeyDown } onChange={ props.IconLibraryHoverImgonChange } ondelay={ props.IconLibraryHoverImgondelay } src={props.IconLibraryHoverImg0 || "https://cdn.uncodie.com/tACQBrKCV97e0EqPE1XNLJ/c23ab32e3c885ac3aee269073a23e747336c5268.png" } />
        </CSSTransition>
        <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['iconcreatehoverimg']?.animationClass || {}}>
          <img id="id_one_eightsevennigth" className={` rectangle iconcreatehoverimg ${ props.onClick ? 'cursor' : '' } ${ transaction['iconcreatehoverimg']?.type ? transaction['iconcreatehoverimg']?.type.toLowerCase() : '' }`} style={{ ...{},  ...props.IconCreateHoverImgStyle , transitionDuration: transaction['iconcreatehoverimg']?.duration, transitionTimingFunction: transaction['iconcreatehoverimg']?.timingFunction }} onClick={ props.IconCreateHoverImgonClick } onMouseEnter={ props.IconCreateHoverImgonMouseEnter } onMouseOver={ props.IconCreateHoverImgonMouseOver } onKeyPress={ props.IconCreateHoverImgonKeyPress } onDrag={ props.IconCreateHoverImgonDrag } onMouseLeave={ props.IconCreateHoverImgonMouseLeave } onMouseUp={ props.IconCreateHoverImgonMouseUp } onMouseDown={ props.IconCreateHoverImgonMouseDown } onKeyDown={ props.IconCreateHoverImgonKeyDown } onChange={ props.IconCreateHoverImgonChange } ondelay={ props.IconCreateHoverImgondelay } src={props.IconCreateHoverImg0 || "https://cdn.uncodie.com/tACQBrKCV97e0EqPE1XNLJ/99cf9c260dd85de481166d39044fa809120f34b6.png" } />
        </CSSTransition>
        <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['iconcreateoffimg']?.animationClass || {}}>
          <img id="id_one_eighteightzero" className={` rectangle iconcreateoffimg ${ props.onClick ? 'cursor' : '' } ${ transaction['iconcreateoffimg']?.type ? transaction['iconcreateoffimg']?.type.toLowerCase() : '' }`} style={{ ...{},  ...props.IconCreateOffImgStyle , transitionDuration: transaction['iconcreateoffimg']?.duration, transitionTimingFunction: transaction['iconcreateoffimg']?.timingFunction }} onClick={ props.IconCreateOffImgonClick } onMouseEnter={ props.IconCreateOffImgonMouseEnter } onMouseOver={ props.IconCreateOffImgonMouseOver } onKeyPress={ props.IconCreateOffImgonKeyPress } onDrag={ props.IconCreateOffImgonDrag } onMouseLeave={ props.IconCreateOffImgonMouseLeave } onMouseUp={ props.IconCreateOffImgonMouseUp } onMouseDown={ props.IconCreateOffImgonMouseDown } onKeyDown={ props.IconCreateOffImgonKeyDown } onChange={ props.IconCreateOffImgonChange } ondelay={ props.IconCreateOffImgondelay } src={props.IconCreateOffImg0 || "https://cdn.uncodie.com/tACQBrKCV97e0EqPE1XNLJ/f186fabe645acd79116b8dd29a75d7520244e7d7.png" } />
        </CSSTransition>
        <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['iconleftoffimg']?.animationClass || {}}>
          <img id="id_one_eighteighttwoo" className={` rectangle iconleftoffimg ${ props.onClick ? 'cursor' : '' } ${ transaction['iconleftoffimg']?.type ? transaction['iconleftoffimg']?.type.toLowerCase() : '' }`} style={{ ...{},  ...props.IconLeftOffImgStyle , transitionDuration: transaction['iconleftoffimg']?.duration, transitionTimingFunction: transaction['iconleftoffimg']?.timingFunction }} onClick={ props.IconLeftOffImgonClick } onMouseEnter={ props.IconLeftOffImgonMouseEnter } onMouseOver={ props.IconLeftOffImgonMouseOver } onKeyPress={ props.IconLeftOffImgonKeyPress } onDrag={ props.IconLeftOffImgonDrag } onMouseLeave={ props.IconLeftOffImgonMouseLeave } onMouseUp={ props.IconLeftOffImgonMouseUp } onMouseDown={ props.IconLeftOffImgonMouseDown } onKeyDown={ props.IconLeftOffImgonKeyDown } onChange={ props.IconLeftOffImgonChange } ondelay={ props.IconLeftOffImgondelay } src={props.IconLeftOffImg0 || "https://cdn.uncodie.com/tACQBrKCV97e0EqPE1XNLJ/cec3b16681c6d1265aafb2abdb4c4c103947cddd.png" } />
        </CSSTransition>
        <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['iconlefthoverimg']?.animationClass || {}}>
          <img id="id_one_eighteightthree" className={` rectangle iconlefthoverimg ${ props.onClick ? 'cursor' : '' } ${ transaction['iconlefthoverimg']?.type ? transaction['iconlefthoverimg']?.type.toLowerCase() : '' }`} style={{ ...{},  ...props.IconLeftHoverImgStyle , transitionDuration: transaction['iconlefthoverimg']?.duration, transitionTimingFunction: transaction['iconlefthoverimg']?.timingFunction }} onClick={ props.IconLeftHoverImgonClick } onMouseEnter={ props.IconLeftHoverImgonMouseEnter } onMouseOver={ props.IconLeftHoverImgonMouseOver } onKeyPress={ props.IconLeftHoverImgonKeyPress } onDrag={ props.IconLeftHoverImgonDrag } onMouseLeave={ props.IconLeftHoverImgonMouseLeave } onMouseUp={ props.IconLeftHoverImgonMouseUp } onMouseDown={ props.IconLeftHoverImgonMouseDown } onKeyDown={ props.IconLeftHoverImgonKeyDown } onChange={ props.IconLeftHoverImgonChange } ondelay={ props.IconLeftHoverImgondelay } src={props.IconLeftHoverImg0 || "https://cdn.uncodie.com/tACQBrKCV97e0EqPE1XNLJ/1ea6c4affe32b977dd30de6b85f9272c458773d3.png" } />
        </CSSTransition>
        <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['iconlrigthoffimg']?.animationClass || {}}>
          <img id="id_one_eighteightfour" className={` rectangle iconlrigthoffimg ${ props.onClick ? 'cursor' : '' } ${ transaction['iconlrigthoffimg']?.type ? transaction['iconlrigthoffimg']?.type.toLowerCase() : '' }`} style={{ ...{},  ...props.IconLRigthOffImgStyle , transitionDuration: transaction['iconlrigthoffimg']?.duration, transitionTimingFunction: transaction['iconlrigthoffimg']?.timingFunction }} onClick={ props.IconLRigthOffImgonClick } onMouseEnter={ props.IconLRigthOffImgonMouseEnter } onMouseOver={ props.IconLRigthOffImgonMouseOver } onKeyPress={ props.IconLRigthOffImgonKeyPress } onDrag={ props.IconLRigthOffImgonDrag } onMouseLeave={ props.IconLRigthOffImgonMouseLeave } onMouseUp={ props.IconLRigthOffImgonMouseUp } onMouseDown={ props.IconLRigthOffImgonMouseDown } onKeyDown={ props.IconLRigthOffImgonKeyDown } onChange={ props.IconLRigthOffImgonChange } ondelay={ props.IconLRigthOffImgondelay } src={props.IconLRigthOffImg0 || "https://cdn.uncodie.com/tACQBrKCV97e0EqPE1XNLJ/7cef8d788abd2463bca0186d06a1a90f235d07a5.png" } />
        </CSSTransition>
        <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['iconinstagramoffimg']?.animationClass || {}}>
          <img id="id_one_eighteightfive" className={` rectangle iconinstagramoffimg ${ props.onClick ? 'cursor' : '' } ${ transaction['iconinstagramoffimg']?.type ? transaction['iconinstagramoffimg']?.type.toLowerCase() : '' }`} style={{ ...{},  ...props.IconInstagramOffImgStyle , transitionDuration: transaction['iconinstagramoffimg']?.duration, transitionTimingFunction: transaction['iconinstagramoffimg']?.timingFunction }} onClick={ props.IconInstagramOffImgonClick } onMouseEnter={ props.IconInstagramOffImgonMouseEnter } onMouseOver={ props.IconInstagramOffImgonMouseOver } onKeyPress={ props.IconInstagramOffImgonKeyPress } onDrag={ props.IconInstagramOffImgonDrag } onMouseLeave={ props.IconInstagramOffImgonMouseLeave } onMouseUp={ props.IconInstagramOffImgonMouseUp } onMouseDown={ props.IconInstagramOffImgonMouseDown } onKeyDown={ props.IconInstagramOffImgonKeyDown } onChange={ props.IconInstagramOffImgonChange } ondelay={ props.IconInstagramOffImgondelay } src={props.IconInstagramOffImg0 || "https://cdn.uncodie.com/tACQBrKCV97e0EqPE1XNLJ/aa5e38cc3adc8a07d8ca20ddf83e756f5be57d6f.png" } />
        </CSSTransition>
        <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['iconinstagramhoverimg']?.animationClass || {}}>
          <img id="id_one_eighteightsix" className={` rectangle iconinstagramhoverimg ${ props.onClick ? 'cursor' : '' } ${ transaction['iconinstagramhoverimg']?.type ? transaction['iconinstagramhoverimg']?.type.toLowerCase() : '' }`} style={{ ...{},  ...props.IconInstagramHoverImgStyle , transitionDuration: transaction['iconinstagramhoverimg']?.duration, transitionTimingFunction: transaction['iconinstagramhoverimg']?.timingFunction }} onClick={ props.IconInstagramHoverImgonClick } onMouseEnter={ props.IconInstagramHoverImgonMouseEnter } onMouseOver={ props.IconInstagramHoverImgonMouseOver } onKeyPress={ props.IconInstagramHoverImgonKeyPress } onDrag={ props.IconInstagramHoverImgonDrag } onMouseLeave={ props.IconInstagramHoverImgonMouseLeave } onMouseUp={ props.IconInstagramHoverImgonMouseUp } onMouseDown={ props.IconInstagramHoverImgonMouseDown } onKeyDown={ props.IconInstagramHoverImgonKeyDown } onChange={ props.IconInstagramHoverImgonChange } ondelay={ props.IconInstagramHoverImgondelay } src={props.IconInstagramHoverImg0 || "https://cdn.uncodie.com/tACQBrKCV97e0EqPE1XNLJ/c7d318c81d57cc613e8e6548fa776460ee5c00e7.png" } />
        </CSSTransition>
        <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['icontwitteroffimg']?.animationClass || {}}>
          <img id="id_one_eighteightseven" className={` rectangle icontwitteroffimg ${ props.onClick ? 'cursor' : '' } ${ transaction['icontwitteroffimg']?.type ? transaction['icontwitteroffimg']?.type.toLowerCase() : '' }`} style={{ ...{},  ...props.IconTwitterOffImgStyle , transitionDuration: transaction['icontwitteroffimg']?.duration, transitionTimingFunction: transaction['icontwitteroffimg']?.timingFunction }} onClick={ props.IconTwitterOffImgonClick } onMouseEnter={ props.IconTwitterOffImgonMouseEnter } onMouseOver={ props.IconTwitterOffImgonMouseOver } onKeyPress={ props.IconTwitterOffImgonKeyPress } onDrag={ props.IconTwitterOffImgonDrag } onMouseLeave={ props.IconTwitterOffImgonMouseLeave } onMouseUp={ props.IconTwitterOffImgonMouseUp } onMouseDown={ props.IconTwitterOffImgonMouseDown } onKeyDown={ props.IconTwitterOffImgonKeyDown } onChange={ props.IconTwitterOffImgonChange } ondelay={ props.IconTwitterOffImgondelay } src={props.IconTwitterOffImg0 || "https://cdn.uncodie.com/tACQBrKCV97e0EqPE1XNLJ/42e64194ff06ae769736a3e9bba2a916968fbe51.png" } />
        </CSSTransition>
        <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['icontwitterhoverimg']?.animationClass || {}}>
          <img id="id_one_eighteighteight" className={` rectangle icontwitterhoverimg ${ props.onClick ? 'cursor' : '' } ${ transaction['icontwitterhoverimg']?.type ? transaction['icontwitterhoverimg']?.type.toLowerCase() : '' }`} style={{ ...{},  ...props.IconTwitterHoverImgStyle , transitionDuration: transaction['icontwitterhoverimg']?.duration, transitionTimingFunction: transaction['icontwitterhoverimg']?.timingFunction }} onClick={ props.IconTwitterHoverImgonClick } onMouseEnter={ props.IconTwitterHoverImgonMouseEnter } onMouseOver={ props.IconTwitterHoverImgonMouseOver } onKeyPress={ props.IconTwitterHoverImgonKeyPress } onDrag={ props.IconTwitterHoverImgonDrag } onMouseLeave={ props.IconTwitterHoverImgonMouseLeave } onMouseUp={ props.IconTwitterHoverImgonMouseUp } onMouseDown={ props.IconTwitterHoverImgonMouseDown } onKeyDown={ props.IconTwitterHoverImgonKeyDown } onChange={ props.IconTwitterHoverImgonChange } ondelay={ props.IconTwitterHoverImgondelay } src={props.IconTwitterHoverImg0 || "https://cdn.uncodie.com/tACQBrKCV97e0EqPE1XNLJ/4ab02f2676e507a0173a1489912afb28a43f1789.png" } />
        </CSSTransition>
        <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['icontwitteroffimg']?.animationClass || {}}>
          <img id="id_one_eighteightnigth" className={` rectangle icontwitteroffimg ${ props.onClick ? 'cursor' : '' } ${ transaction['icontwitteroffimg']?.type ? transaction['icontwitteroffimg']?.type.toLowerCase() : '' }`} style={{ ...{},  ...props.IconTwitterOffImgStyle , transitionDuration: transaction['icontwitteroffimg']?.duration, transitionTimingFunction: transaction['icontwitteroffimg']?.timingFunction }} onClick={ props.IconTwitterOffImgonClick } onMouseEnter={ props.IconTwitterOffImgonMouseEnter } onMouseOver={ props.IconTwitterOffImgonMouseOver } onKeyPress={ props.IconTwitterOffImgonKeyPress } onDrag={ props.IconTwitterOffImgonDrag } onMouseLeave={ props.IconTwitterOffImgonMouseLeave } onMouseUp={ props.IconTwitterOffImgonMouseUp } onMouseDown={ props.IconTwitterOffImgonMouseDown } onKeyDown={ props.IconTwitterOffImgonKeyDown } onChange={ props.IconTwitterOffImgonChange } ondelay={ props.IconTwitterOffImgondelay } src={props.IconTwitterOffImg1 || "https://cdn.uncodie.com/tACQBrKCV97e0EqPE1XNLJ/6ecaabf4da8cb119aca1992cbeccb0934205d4c7.png" } />
        </CSSTransition>
        <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['icontwitterhoverimg']?.animationClass || {}}>
          <img id="id_one_eightnigthzero" className={` rectangle icontwitterhoverimg ${ props.onClick ? 'cursor' : '' } ${ transaction['icontwitterhoverimg']?.type ? transaction['icontwitterhoverimg']?.type.toLowerCase() : '' }`} style={{ ...{},  ...props.IconTwitterHoverImgStyle , transitionDuration: transaction['icontwitterhoverimg']?.duration, transitionTimingFunction: transaction['icontwitterhoverimg']?.timingFunction }} onClick={ props.IconTwitterHoverImgonClick } onMouseEnter={ props.IconTwitterHoverImgonMouseEnter } onMouseOver={ props.IconTwitterHoverImgonMouseOver } onKeyPress={ props.IconTwitterHoverImgonKeyPress } onDrag={ props.IconTwitterHoverImgonDrag } onMouseLeave={ props.IconTwitterHoverImgonMouseLeave } onMouseUp={ props.IconTwitterHoverImgonMouseUp } onMouseDown={ props.IconTwitterHoverImgonMouseDown } onKeyDown={ props.IconTwitterHoverImgonKeyDown } onChange={ props.IconTwitterHoverImgonChange } ondelay={ props.IconTwitterHoverImgondelay } src={props.IconTwitterHoverImg1 || "https://cdn.uncodie.com/tACQBrKCV97e0EqPE1XNLJ/7a820c31cf28fcf35f0a3040cdb36d231dd6c6c7.png" } />
        </CSSTransition>
        <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['iconrigthhoverimg']?.animationClass || {}}>
          <img id="id_one_eightnigthone" className={` rectangle iconrigthhoverimg ${ props.onClick ? 'cursor' : '' } ${ transaction['iconrigthhoverimg']?.type ? transaction['iconrigthhoverimg']?.type.toLowerCase() : '' }`} style={{ ...{},  ...props.IconRigthHoverImgStyle , transitionDuration: transaction['iconrigthhoverimg']?.duration, transitionTimingFunction: transaction['iconrigthhoverimg']?.timingFunction }} onClick={ props.IconRigthHoverImgonClick } onMouseEnter={ props.IconRigthHoverImgonMouseEnter } onMouseOver={ props.IconRigthHoverImgonMouseOver } onKeyPress={ props.IconRigthHoverImgonKeyPress } onDrag={ props.IconRigthHoverImgonDrag } onMouseLeave={ props.IconRigthHoverImgonMouseLeave } onMouseUp={ props.IconRigthHoverImgonMouseUp } onMouseDown={ props.IconRigthHoverImgonMouseDown } onKeyDown={ props.IconRigthHoverImgonKeyDown } onChange={ props.IconRigthHoverImgonChange } ondelay={ props.IconRigthHoverImgondelay } src={props.IconRigthHoverImg0 || "https://cdn.uncodie.com/tACQBrKCV97e0EqPE1XNLJ/fee05d01267dae2d48a642806fa5c3043ff36378.png" } />
        </CSSTransition>
        <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['iconlibraryoffimg']?.animationClass || {}}>
          <img id="id_one_eightnigththree" className={` rectangle iconlibraryoffimg ${ props.onClick ? 'cursor' : '' } ${ transaction['iconlibraryoffimg']?.type ? transaction['iconlibraryoffimg']?.type.toLowerCase() : '' }`} style={{ ...{},  ...props.IconLibraryOffImgStyle , transitionDuration: transaction['iconlibraryoffimg']?.duration, transitionTimingFunction: transaction['iconlibraryoffimg']?.timingFunction }} onClick={ props.IconLibraryOffImgonClick } onMouseEnter={ props.IconLibraryOffImgonMouseEnter } onMouseOver={ props.IconLibraryOffImgonMouseOver } onKeyPress={ props.IconLibraryOffImgonKeyPress } onDrag={ props.IconLibraryOffImgonDrag } onMouseLeave={ props.IconLibraryOffImgonMouseLeave } onMouseUp={ props.IconLibraryOffImgonMouseUp } onMouseDown={ props.IconLibraryOffImgonMouseDown } onKeyDown={ props.IconLibraryOffImgonKeyDown } onChange={ props.IconLibraryOffImgonChange } ondelay={ props.IconLibraryOffImgondelay } src={props.IconLibraryOffImg0 || "https://cdn.uncodie.com/tACQBrKCV97e0EqPE1XNLJ/3102d00a43fe3575716917fa6ba9b8bd7c02e078.png" } />
        </CSSTransition>
        <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['iconlibraryonimg']?.animationClass || {}}>
          <img id="id_one_eightnigthfour" className={` rectangle iconlibraryonimg ${ props.onClick ? 'cursor' : '' } ${ transaction['iconlibraryonimg']?.type ? transaction['iconlibraryonimg']?.type.toLowerCase() : '' }`} style={{ ...{},  ...props.IconLibraryOnImgStyle , transitionDuration: transaction['iconlibraryonimg']?.duration, transitionTimingFunction: transaction['iconlibraryonimg']?.timingFunction }} onClick={ props.IconLibraryOnImgonClick } onMouseEnter={ props.IconLibraryOnImgonMouseEnter } onMouseOver={ props.IconLibraryOnImgonMouseOver } onKeyPress={ props.IconLibraryOnImgonKeyPress } onDrag={ props.IconLibraryOnImgonDrag } onMouseLeave={ props.IconLibraryOnImgonMouseLeave } onMouseUp={ props.IconLibraryOnImgonMouseUp } onMouseDown={ props.IconLibraryOnImgonMouseDown } onKeyDown={ props.IconLibraryOnImgonKeyDown } onChange={ props.IconLibraryOnImgonChange } ondelay={ props.IconLibraryOnImgondelay } src={props.IconLibraryOnImg0 || "https://cdn.uncodie.com/tACQBrKCV97e0EqPE1XNLJ/01ce4f44c706e19c82d03ae6b712f164bda92d17.png" } />
        </CSSTransition>
        <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['iconlibraryonimg']?.animationClass || {}}>
          <img id="id_fourthreeeight_threenigthsevenzero" className={` rectangle iconlibraryonimg ${ props.onClick ? 'cursor' : '' } ${ transaction['iconlibraryonimg']?.type ? transaction['iconlibraryonimg']?.type.toLowerCase() : '' }`} style={{ ...{},  ...props.IconLibraryOnImgStyle , transitionDuration: transaction['iconlibraryonimg']?.duration, transitionTimingFunction: transaction['iconlibraryonimg']?.timingFunction }} onClick={ props.IconLibraryOnImgonClick } onMouseEnter={ props.IconLibraryOnImgonMouseEnter } onMouseOver={ props.IconLibraryOnImgonMouseOver } onKeyPress={ props.IconLibraryOnImgonKeyPress } onDrag={ props.IconLibraryOnImgonDrag } onMouseLeave={ props.IconLibraryOnImgonMouseLeave } onMouseUp={ props.IconLibraryOnImgonMouseUp } onMouseDown={ props.IconLibraryOnImgonMouseDown } onKeyDown={ props.IconLibraryOnImgonKeyDown } onChange={ props.IconLibraryOnImgonChange } ondelay={ props.IconLibraryOnImgondelay } src={props.IconLibraryOnImg1 || "https://cdn.uncodie.com/tACQBrKCV97e0EqPE1XNLJ/01ce4f44c706e19c82d03ae6b712f164bda92d17.png" } />
        </CSSTransition>
        <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['palyiconoff']?.animationClass || {}}>

          <div id="id_twoozero_twooeightone" className={` frame palyiconoff ${ props.onClick ? 'cursor' : '' } ${ transaction['palyiconoff']?.type ? transaction['palyiconoff']?.type.toLowerCase() : '' }`} style={ { ...{}, ...props.PalyIconOffStyle , transitionDuration: transaction['palyiconoff']?.duration, transitionTimingFunction: transaction['palyiconoff']?.timingFunction } } onClick={ props.PalyIconOffonClick } onMouseEnter={ props.PalyIconOffonMouseEnter } onMouseOver={ props.PalyIconOffonMouseOver } onKeyPress={ props.PalyIconOffonKeyPress } onDrag={ props.PalyIconOffonDrag } onMouseLeave={ props.PalyIconOffonMouseLeave } onMouseUp={ props.PalyIconOffonMouseUp } onMouseDown={ props.PalyIconOffonMouseDown } onKeyDown={ props.PalyIconOffonKeyDown } onChange={ props.PalyIconOffonChange } ondelay={ props.PalyIconOffondelay }>
            <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['playiconvectoroff']?.animationClass || {}}>
              <svg id="id_twoozero_twooeighttwoo" className="playiconvectoroff  " style={{}} onClick={ props.PlayIconVectorOffonClick } onMouseEnter={ props.PlayIconVectorOffonMouseEnter } onMouseOver={ props.PlayIconVectorOffonMouseOver } onKeyPress={ props.PlayIconVectorOffonKeyPress } onDrag={ props.PlayIconVectorOffonDrag } onMouseLeave={ props.PlayIconVectorOffonMouseLeave } onMouseUp={ props.PlayIconVectorOffonMouseUp } onMouseDown={ props.PlayIconVectorOffonMouseDown } onKeyDown={ props.PlayIconVectorOffonKeyDown } onChange={ props.PlayIconVectorOffonChange } ondelay={ props.PlayIconVectorOffondelay } width="15" height="17">
                <path d="M1.05778 0.0939172L14.6478 7.89313C14.7549 7.95468 14.8438 8.04316 14.9056 8.14969C14.9675 8.25621 15 8.37702 15 8.5C15 8.62298 14.9675 8.74379 14.9056 8.85032C14.8438 8.95684 14.7549 9.04532 14.6478 9.10687L1.05778 16.9061C0.950598 16.9676 0.829012 17 0.705244 17C0.581476 17 0.459885 16.9676 0.35269 16.9061C0.245494 16.8446 0.156469 16.7562 0.0945598 16.6496C0.0326508 16.5431 3.8503e-05 16.4222 0 16.2992L0 0.70079C3.8503e-05 0.577755 0.0326508 0.456898 0.0945598 0.350362C0.156469 0.243826 0.245494 0.155363 0.35269 0.0938628C0.459885 0.0323623 0.581476 -9.5717e-06 0.705244 2.12294e-09C0.829012 9.57595e-06 0.950598 0.0324001 1.05778 0.0939172Z" />
              </svg>
            </CSSTransition>
          </div>

        </CSSTransition>
        <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['palyiconon']?.animationClass || {}}>

          <div id="id_twooone_twooeightseven" className={` frame palyiconon ${ props.onClick ? 'cursor' : '' } ${ transaction['palyiconon']?.type ? transaction['palyiconon']?.type.toLowerCase() : '' }`} style={ { ...{}, ...props.PalyIconOnStyle , transitionDuration: transaction['palyiconon']?.duration, transitionTimingFunction: transaction['palyiconon']?.timingFunction } } onClick={ props.PalyIconOnonClick } onMouseEnter={ props.PalyIconOnonMouseEnter } onMouseOver={ props.PalyIconOnonMouseOver } onKeyPress={ props.PalyIconOnonKeyPress } onDrag={ props.PalyIconOnonDrag } onMouseLeave={ props.PalyIconOnonMouseLeave } onMouseUp={ props.PalyIconOnonMouseUp } onMouseDown={ props.PalyIconOnonMouseDown } onKeyDown={ props.PalyIconOnonKeyDown } onChange={ props.PalyIconOnonChange } ondelay={ props.PalyIconOnondelay }>
            <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['playiconvectoron']?.animationClass || {}}>
              <svg id="id_twooone_twooeighteight" className="playiconvectoron  " style={{}} onClick={ props.PlayIconVectorOnonClick } onMouseEnter={ props.PlayIconVectorOnonMouseEnter } onMouseOver={ props.PlayIconVectorOnonMouseOver } onKeyPress={ props.PlayIconVectorOnonKeyPress } onDrag={ props.PlayIconVectorOnonDrag } onMouseLeave={ props.PlayIconVectorOnonMouseLeave } onMouseUp={ props.PlayIconVectorOnonMouseUp } onMouseDown={ props.PlayIconVectorOnonMouseDown } onKeyDown={ props.PlayIconVectorOnonKeyDown } onChange={ props.PlayIconVectorOnonChange } ondelay={ props.PlayIconVectorOnondelay } width="15" height="17">
                <path d="M1.05778 0.0939172L14.6478 7.89313C14.7549 7.95468 14.8438 8.04316 14.9056 8.14969C14.9675 8.25621 15 8.37702 15 8.5C15 8.62298 14.9675 8.74379 14.9056 8.85032C14.8438 8.95684 14.7549 9.04532 14.6478 9.10687L1.05778 16.9061C0.950598 16.9676 0.829012 17 0.705244 17C0.581476 17 0.459885 16.9676 0.35269 16.9061C0.245494 16.8446 0.156469 16.7562 0.0945598 16.6496C0.0326508 16.5431 3.8503e-05 16.4222 0 16.2992L0 0.70079C3.8503e-05 0.577755 0.0326508 0.456898 0.0945598 0.350362C0.156469 0.243826 0.245494 0.155363 0.35269 0.0938628C0.459885 0.0323623 0.581476 -9.5717e-06 0.705244 2.12294e-09C0.829012 9.57595e-06 0.950598 0.0324001 1.05778 0.0939172Z" />
              </svg>
            </CSSTransition>
          </div>

        </CSSTransition>
        <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['framethreethreeone']?.animationClass || {}}>

          <div id="id_fourthreethree_onezerofivesix" className={` frame framethreethreeone ${ props.onClick ? 'cursor' : '' } ${ transaction['framethreethreeone']?.type ? transaction['framethreethreeone']?.type.toLowerCase() : '' }`} style={ { ...{}, ...props.FramethreethreeoneStyle , transitionDuration: transaction['framethreethreeone']?.duration, transitionTimingFunction: transaction['framethreethreeone']?.timingFunction } } onClick={ props.FramethreethreeoneonClick } onMouseEnter={ props.FramethreethreeoneonMouseEnter } onMouseOver={ props.FramethreethreeoneonMouseOver } onKeyPress={ props.FramethreethreeoneonKeyPress } onDrag={ props.FramethreethreeoneonDrag } onMouseLeave={ props.FramethreethreeoneonMouseLeave } onMouseUp={ props.FramethreethreeoneonMouseUp } onMouseDown={ props.FramethreethreeoneonMouseDown } onKeyDown={ props.FramethreethreeoneonKeyDown } onChange={ props.FramethreethreeoneonChange } ondelay={ props.Framethreethreeoneondelay }>
            <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['vector']?.animationClass || {}}>
              <svg id="id_fourthreethree_onezerofivefive" className="vector  " style={{}} onClick={ props.VectoronClick } onMouseEnter={ props.VectoronMouseEnter } onMouseOver={ props.VectoronMouseOver } onKeyPress={ props.VectoronKeyPress } onDrag={ props.VectoronDrag } onMouseLeave={ props.VectoronMouseLeave } onMouseUp={ props.VectoronMouseUp } onMouseDown={ props.VectoronMouseDown } onKeyDown={ props.VectoronKeyDown } onChange={ props.VectoronChange } ondelay={ props.Vectorondelay } width="10.92626953125" height="19.0120849609375">
                <path d="M0.305288 0.292786C0.117817 0.480314 0.0125018 0.734622 0.0125018 0.999786C0.0125018 1.26495 0.117817 1.51926 0.305288 1.70679L8.09829 9.49979L0.305288 17.2928C0.209778 17.385 0.133596 17.4954 0.0811869 17.6174C0.0287779 17.7394 0.00119157 17.8706 3.77571e-05 18.0034C-0.00111606 18.1362 0.0241854 18.2678 0.0744663 18.3907C0.124747 18.5136 0.199001 18.6253 0.292893 18.7192C0.386786 18.8131 0.498438 18.8873 0.621334 18.9376C0.744231 18.9879 0.87591 19.0132 1.00869 19.012C1.14147 19.0109 1.27269 18.9833 1.39469 18.9309C1.5167 18.8785 1.62704 18.8023 1.71929 18.7068L10.9263 9.49979L1.71929 0.292786C1.53176 0.105315 1.27745 0 1.01229 0C0.747124 0 0.492816 0.105315 0.305288 0.292786Z" />
              </svg>
            </CSSTransition>
          </div>

        </CSSTransition>
        <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['frame']?.animationClass || {}}>

          <div id="id_fourthreefour_onefourfourone" className={` frame frame ${ props.onClick ? 'cursor' : '' } ${ transaction['frame']?.type ? transaction['frame']?.type.toLowerCase() : '' }`} style={ { ...{}, ...props.FrameStyle , transitionDuration: transaction['frame']?.duration, transitionTimingFunction: transaction['frame']?.timingFunction } } onClick={ props.FrameonClick } onMouseEnter={ props.FrameonMouseEnter } onMouseOver={ props.FrameonMouseOver } onKeyPress={ props.FrameonKeyPress } onDrag={ props.FrameonDrag } onMouseLeave={ props.FrameonMouseLeave } onMouseUp={ props.FrameonMouseUp } onMouseDown={ props.FrameonMouseDown } onKeyDown={ props.FrameonKeyDown } onChange={ props.FrameonChange } ondelay={ props.Frameondelay }>
            <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['vector']?.animationClass || {}}>
              <svg id="id_fourthreefour_onefourfourtwoo" className="vector  " style={{}} onClick={ props.VectoronClick } onMouseEnter={ props.VectoronMouseEnter } onMouseOver={ props.VectoronMouseOver } onKeyPress={ props.VectoronKeyPress } onDrag={ props.VectoronDrag } onMouseLeave={ props.VectoronMouseLeave } onMouseUp={ props.VectoronMouseUp } onMouseDown={ props.VectoronMouseDown } onKeyDown={ props.VectoronKeyDown } onChange={ props.VectoronChange } ondelay={ props.Vectorondelay } width="9.5999755859375" height="9.3953857421875">
                <path d="M9.6 2.0454C9.6 1.3363 9.5364 0.6545 9.4182 0L0 0L0 3.8681L5.3818 3.8681C5.15 5.1181 4.4455 6.1772 3.3864 6.8863L3.3864 9.3954L6.6182 9.3954C8.5091 7.6545 9.6 5.0909 9.6 2.0454Z" />
              </svg>
            </CSSTransition>
            <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['vector']?.animationClass || {}}>
              <svg id="id_fourthreefour_onefourfourthree" className="vector  " style={{}} onClick={ props.VectoronClick } onMouseEnter={ props.VectoronMouseEnter } onMouseOver={ props.VectoronMouseOver } onKeyPress={ props.VectoronKeyPress } onDrag={ props.VectoronDrag } onMouseLeave={ props.VectoronMouseLeave } onMouseUp={ props.VectoronMouseUp } onMouseDown={ props.VectoronMouseDown } onKeyDown={ props.VectoronKeyDown } onChange={ props.VectoronChange } ondelay={ props.Vectorondelay } width="15.554443359375" height="8.0999755859375">
                <path d="M8.9364 8.1C11.6364 8.1 13.9 7.2045 15.5545 5.6773L12.3227 3.1682C11.4273 3.7682 10.2818 4.1227 8.9364 4.1227C6.33182 4.1227 4.12727 2.3636 3.34091 0L0 0L0 2.5909C1.64545 5.8591 5.02727 8.1 8.9364 8.1Z" />
              </svg>
            </CSSTransition>
            <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['vector']?.animationClass || {}}>
              <svg id="id_fourthreefour_onefourfourfour" className="vector  " style={{}} onClick={ props.VectoronClick } onMouseEnter={ props.VectoronMouseEnter } onMouseOver={ props.VectoronMouseOver } onKeyPress={ props.VectoronKeyPress } onDrag={ props.VectoronDrag } onMouseLeave={ props.VectoronMouseLeave } onMouseUp={ props.VectoronMouseUp } onMouseDown={ props.VectoronMouseDown } onKeyDown={ props.VectoronKeyDown } onChange={ props.VectoronChange } ondelay={ props.Vectorondelay } width="4.404541015625" height="8.9818115234375">
                <path d="M4.40455 6.39094C4.20455 5.79094 4.09091 5.15004 4.09091 4.49094C4.09091 3.83184 4.20455 3.19094 4.40455 2.59094L4.40455 0L1.06364 0C0.38636 1.35 0 2.87724 0 4.49094C0 6.10454 0.38636 7.63184 1.06364 8.98184L4.40455 6.39094Z" />
              </svg>
            </CSSTransition>
            <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['vector']?.animationClass || {}}>
              <svg id="id_fourthreefour_onefourfourfive" className="vector  " style={{}} onClick={ props.VectoronClick } onMouseEnter={ props.VectoronMouseEnter } onMouseOver={ props.VectoronMouseOver } onKeyPress={ props.VectoronKeyPress } onDrag={ props.VectoronDrag } onMouseLeave={ props.VectoronMouseLeave } onMouseUp={ props.VectoronMouseUp } onMouseDown={ props.VectoronMouseDown } onKeyDown={ props.VectoronKeyDown } onChange={ props.VectoronChange } ondelay={ props.Vectorondelay } width="15.6273193359375" height="8.0999755859375">
                <path d="M8.9364 3.97727C10.4045 3.97727 11.7227 4.48182 12.7591 5.47273L15.6273 2.60455C13.8955 0.99091 11.6318 0 8.9364 0C5.02727 0 1.64545 2.24091 0 5.50909L3.34091 8.1C4.12727 5.73636 6.33182 3.97727 8.9364 3.97727Z" />
              </svg>
            </CSSTransition>
          </div>

        </CSSTransition>
        <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['iconhome']?.animationClass || {}}>

          <div id="id_fourthreeeight_threenigthsevenone" className={` frame iconhome ${ props.onClick ? 'cursor' : '' } ${ transaction['iconhome']?.type ? transaction['iconhome']?.type.toLowerCase() : '' }`} style={ { ...{}, ...props.IconHomeStyle , transitionDuration: transaction['iconhome']?.duration, transitionTimingFunction: transaction['iconhome']?.timingFunction } } onClick={ props.IconHomeonClick } onMouseEnter={ props.IconHomeonMouseEnter } onMouseOver={ props.IconHomeonMouseOver } onKeyPress={ props.IconHomeonKeyPress } onDrag={ props.IconHomeonDrag } onMouseLeave={ props.IconHomeonMouseLeave } onMouseUp={ props.IconHomeonMouseUp } onMouseDown={ props.IconHomeonMouseDown } onKeyDown={ props.IconHomeonKeyDown } onChange={ props.IconHomeonChange } ondelay={ props.IconHomeondelay }>
            <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['vector']?.animationClass || {}}>
              <svg id="id_fourthreeeight_threenigthseventwoo" className="vector  " style={{}} onClick={ props.VectoronClick } onMouseEnter={ props.VectoronMouseEnter } onMouseOver={ props.VectoronMouseOver } onKeyPress={ props.VectoronKeyPress } onDrag={ props.VectoronDrag } onMouseLeave={ props.VectoronMouseLeave } onMouseUp={ props.VectoronMouseUp } onMouseDown={ props.VectoronMouseDown } onKeyDown={ props.VectoronKeyDown } onChange={ props.VectoronChange } ondelay={ props.Vectorondelay } width="20" height="20.886962890625">
                <path d="M11.5 0.401924C11.0439 0.138619 10.5266 4.44089e-16 10 0C9.47339 -2.22045e-16 8.95606 0.138619 8.5 0.401924L1 4.73192C0.695969 4.90746 0.443498 5.15992 0.267962 5.46395C0.0924258 5.76798 8.91844e-06 6.11286 0 6.46392L0 19.8869C4.44089e-16 20.1521 0.105357 20.4065 0.292893 20.594C0.48043 20.7816 0.734784 20.8869 1 20.8869L7 20.8869C7.26522 20.8869 7.51957 20.7816 7.70711 20.594C7.89464 20.4065 8 20.1521 8 19.8869L8 13.8869L12 13.8869L12 19.8869C12 20.1521 12.1054 20.4065 12.2929 20.594C12.4804 20.7816 12.7348 20.8869 13 20.8869L19 20.8869C19.2652 20.8869 19.5196 20.7816 19.7071 20.594C19.8946 20.4065 20 20.1521 20 19.8869L20 6.46392C20 6.11286 19.9076 5.76798 19.732 5.46395C19.5565 5.15992 19.304 4.90746 19 4.73192L11.5 0.401924Z" />
              </svg>
            </CSSTransition>
          </div>

        </CSSTransition>
        <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['frame']?.animationClass || {}}>

          <div id="id_fourthreeeight_threenigthsevenfive" className={` frame frame ${ props.onClick ? 'cursor' : '' } ${ transaction['frame']?.type ? transaction['frame']?.type.toLowerCase() : '' }`} style={ { ...{}, ...props.FrameStyle , transitionDuration: transaction['frame']?.duration, transitionTimingFunction: transaction['frame']?.timingFunction } } onClick={ props.FrameonClick } onMouseEnter={ props.FrameonMouseEnter } onMouseOver={ props.FrameonMouseOver } onKeyPress={ props.FrameonKeyPress } onDrag={ props.FrameonDrag } onMouseLeave={ props.FrameonMouseLeave } onMouseUp={ props.FrameonMouseUp } onMouseDown={ props.FrameonMouseDown } onKeyDown={ props.FrameonKeyDown } onChange={ props.FrameonChange } ondelay={ props.Frameondelay }>
            <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['vector']?.animationClass || {}}>
              <svg id="id_fourthreeeight_threenigthsevensix" className="vector  " style={{}} onClick={ props.VectoronClick } onMouseEnter={ props.VectoronMouseEnter } onMouseOver={ props.VectoronMouseOver } onKeyPress={ props.VectoronKeyPress } onDrag={ props.VectoronDrag } onMouseLeave={ props.VectoronMouseLeave } onMouseUp={ props.VectoronMouseUp } onMouseDown={ props.VectoronMouseDown } onKeyDown={ props.VectoronKeyDown } onChange={ props.VectoronChange } ondelay={ props.Vectorondelay } width="20" height="20">
                <path d="M1 20C0.734784 20 0.48043 19.8946 0.292893 19.7071C0.105357 19.5196 6.66134e-16 19.2652 0 19L0 1C0 0.734784 0.105357 0.48043 0.292893 0.292893C0.48043 0.105357 0.734784 4.44089e-16 1 0C1.26522 4.44089e-16 1.51957 0.105357 1.70711 0.292893C1.89464 0.48043 2 0.734784 2 1L2 19C2 19.2652 1.89464 19.5196 1.70711 19.7071C1.51957 19.8946 1.26522 20 1 20ZM13.5 0.134C13.348 0.0462328 13.1755 2.66256e-05 13 2.55108e-05C12.8245 2.43959e-05 12.652 0.0462283 12.5 0.133994C12.348 0.221759 12.2218 0.347993 12.134 0.500009C12.0462 0.652025 12 0.824466 12 1L12 19C12 19.2652 12.1054 19.5196 12.2929 19.7071C12.4804 19.8946 12.7348 20 13 20L19 20C19.2652 20 19.5196 19.8946 19.7071 19.7071C19.8946 19.5196 20 19.2652 20 19L20 4.464C20 4.28847 19.9538 4.11603 19.866 3.96401C19.7782 3.812 19.652 3.68577 19.5 3.598L13.5 0.134ZM7 0C6.73478 4.44089e-16 6.48043 0.105357 6.29289 0.292893C6.10536 0.48043 6 0.734784 6 1L6 19C6 19.2652 6.10536 19.5196 6.29289 19.7071C6.48043 19.8946 6.73478 20 7 20C7.26522 20 7.51957 19.8946 7.70711 19.7071C7.89464 19.5196 8 19.2652 8 19L8 1C8 0.734784 7.89464 0.48043 7.70711 0.292893C7.51957 0.105357 7.26522 6.66134e-16 7 0Z" />
              </svg>
            </CSSTransition>
          </div>

        </CSSTransition>
        <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['frame']?.animationClass || {}}>

          <div id="id_fourthreeeight_sixzerofivethree" className={` frame frame ${ props.onClick ? 'cursor' : '' } ${ transaction['frame']?.type ? transaction['frame']?.type.toLowerCase() : '' }`} style={ { ...{}, ...props.FrameStyle , transitionDuration: transaction['frame']?.duration, transitionTimingFunction: transaction['frame']?.timingFunction } } onClick={ props.FrameonClick } onMouseEnter={ props.FrameonMouseEnter } onMouseOver={ props.FrameonMouseOver } onKeyPress={ props.FrameonKeyPress } onDrag={ props.FrameonDrag } onMouseLeave={ props.FrameonMouseLeave } onMouseUp={ props.FrameonMouseUp } onMouseDown={ props.FrameonMouseDown } onKeyDown={ props.FrameonKeyDown } onChange={ props.FrameonChange } ondelay={ props.Frameondelay }>
            <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['vector']?.animationClass || {}}>
              <svg id="id_fourthreeeight_sixzerofivefour" className="vector  " style={{}} onClick={ props.VectoronClick } onMouseEnter={ props.VectoronMouseEnter } onMouseOver={ props.VectoronMouseOver } onKeyPress={ props.VectoronKeyPress } onDrag={ props.VectoronDrag } onMouseLeave={ props.VectoronMouseLeave } onMouseUp={ props.VectoronMouseUp } onMouseDown={ props.VectoronMouseDown } onKeyDown={ props.VectoronKeyDown } onChange={ props.VectoronChange } ondelay={ props.Vectorondelay } width="20" height="20">
                <path d="M1 20C0.734784 20 0.48043 19.8946 0.292893 19.7071C0.105357 19.5196 6.66134e-16 19.2652 0 19L0 1C0 0.734784 0.105357 0.48043 0.292893 0.292893C0.48043 0.105357 0.734784 4.44089e-16 1 0C1.26522 4.44089e-16 1.51957 0.105357 1.70711 0.292893C1.89464 0.48043 2 0.734784 2 1L2 19C2 19.2652 1.89464 19.5196 1.70711 19.7071C1.51957 19.8946 1.26522 20 1 20ZM13.5 0.134C13.348 0.0462328 13.1755 2.66256e-05 13 2.55108e-05C12.8245 2.43959e-05 12.652 0.0462283 12.5 0.133994C12.348 0.221759 12.2218 0.347993 12.134 0.500009C12.0462 0.652025 12 0.824466 12 1L12 19C12 19.2652 12.1054 19.5196 12.2929 19.7071C12.4804 19.8946 12.7348 20 13 20L19 20C19.2652 20 19.5196 19.8946 19.7071 19.7071C19.8946 19.5196 20 19.2652 20 19L20 4.464C20 4.28847 19.9538 4.11603 19.866 3.96401C19.7782 3.812 19.652 3.68577 19.5 3.598L13.5 0.134ZM7 0C6.73478 4.44089e-16 6.48043 0.105357 6.29289 0.292893C6.10536 0.48043 6 0.734784 6 1L6 19C6 19.2652 6.10536 19.5196 6.29289 19.7071C6.48043 19.8946 6.73478 20 7 20C7.26522 20 7.51957 19.8946 7.70711 19.7071C7.89464 19.5196 8 19.2652 8 19L8 1C8 0.734784 7.89464 0.48043 7.70711 0.292893C7.51957 0.105357 7.26522 6.66134e-16 7 0Z" />
              </svg>
            </CSSTransition>
          </div>

        </CSSTransition>
        <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['frame']?.animationClass || {}}>

          <div id="id_fourthreeeight_fourtwoothreeone" className={` frame frame ${ props.onClick ? 'cursor' : '' } ${ transaction['frame']?.type ? transaction['frame']?.type.toLowerCase() : '' }`} style={ { ...{}, ...props.FrameStyle , transitionDuration: transaction['frame']?.duration, transitionTimingFunction: transaction['frame']?.timingFunction } } onClick={ props.FrameonClick } onMouseEnter={ props.FrameonMouseEnter } onMouseOver={ props.FrameonMouseOver } onKeyPress={ props.FrameonKeyPress } onDrag={ props.FrameonDrag } onMouseLeave={ props.FrameonMouseLeave } onMouseUp={ props.FrameonMouseUp } onMouseDown={ props.FrameonMouseDown } onKeyDown={ props.FrameonKeyDown } onChange={ props.FrameonChange } ondelay={ props.Frameondelay }>
            <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['vector']?.animationClass || {}}>
              <svg id="id_fourthreeeight_fourtwoothreetwoo" className="vector  " style={{}} onClick={ props.VectoronClick } onMouseEnter={ props.VectoronMouseEnter } onMouseOver={ props.VectoronMouseOver } onKeyPress={ props.VectoronKeyPress } onDrag={ props.VectoronDrag } onMouseLeave={ props.VectoronMouseLeave } onMouseUp={ props.VectoronMouseUp } onMouseDown={ props.VectoronMouseDown } onKeyDown={ props.VectoronKeyDown } onChange={ props.VectoronChange } ondelay={ props.Vectorondelay } width="6" height="12">
                <path d="M12 0L6 6L0 0L12 0Z" />
              </svg>
            </CSSTransition>
          </div>

        </CSSTransition>
        <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['chevrondownoff']?.animationClass || {}}>

          <div id="id_fourthreeeight_fourtwoothreethree" className={` frame chevrondownoff ${ props.onClick ? 'cursor' : '' } ${ transaction['chevrondownoff']?.type ? transaction['chevrondownoff']?.type.toLowerCase() : '' }`} style={ { ...{}, ...props.ChevronDownOffStyle , transitionDuration: transaction['chevrondownoff']?.duration, transitionTimingFunction: transaction['chevrondownoff']?.timingFunction } } onClick={ props.ChevronDownOffonClick } onMouseEnter={ props.ChevronDownOffonMouseEnter } onMouseOver={ props.ChevronDownOffonMouseOver } onKeyPress={ props.ChevronDownOffonKeyPress } onDrag={ props.ChevronDownOffonDrag } onMouseLeave={ props.ChevronDownOffonMouseLeave } onMouseUp={ props.ChevronDownOffonMouseUp } onMouseDown={ props.ChevronDownOffonMouseDown } onKeyDown={ props.ChevronDownOffonKeyDown } onChange={ props.ChevronDownOffonChange } ondelay={ props.ChevronDownOffondelay }>
            <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['vector']?.animationClass || {}}>
              <svg id="id_fourthreeeight_fourtwoothreefour" className="vector  " style={{}} onClick={ props.VectoronClick } onMouseEnter={ props.VectoronMouseEnter } onMouseOver={ props.VectoronMouseOver } onKeyPress={ props.VectoronKeyPress } onDrag={ props.VectoronDrag } onMouseLeave={ props.VectoronMouseLeave } onMouseUp={ props.VectoronMouseUp } onMouseDown={ props.VectoronMouseDown } onKeyDown={ props.VectoronKeyDown } onChange={ props.VectoronChange } ondelay={ props.Vectorondelay } width="12" height="6">
                <path d="M12 0L6 6L0 0L12 0Z" />
              </svg>
            </CSSTransition>
          </div>

        </CSSTransition>
        <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['chevrondownon']?.animationClass || {}}>

          <div id="id_fourthreeeight_fourtwoofivezero" className={` frame chevrondownon ${ props.onClick ? 'cursor' : '' } ${ transaction['chevrondownon']?.type ? transaction['chevrondownon']?.type.toLowerCase() : '' }`} style={ { ...{}, ...props.ChevronDownONStyle , transitionDuration: transaction['chevrondownon']?.duration, transitionTimingFunction: transaction['chevrondownon']?.timingFunction } } onClick={ props.ChevronDownONonClick } onMouseEnter={ props.ChevronDownONonMouseEnter } onMouseOver={ props.ChevronDownONonMouseOver } onKeyPress={ props.ChevronDownONonKeyPress } onDrag={ props.ChevronDownONonDrag } onMouseLeave={ props.ChevronDownONonMouseLeave } onMouseUp={ props.ChevronDownONonMouseUp } onMouseDown={ props.ChevronDownONonMouseDown } onKeyDown={ props.ChevronDownONonKeyDown } onChange={ props.ChevronDownONonChange } ondelay={ props.ChevronDownONondelay }>
            <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['vector']?.animationClass || {}}>
              <svg id="id_fourthreeeight_fourtwoofiveone" className="vector  " style={{}} onClick={ props.VectoronClick } onMouseEnter={ props.VectoronMouseEnter } onMouseOver={ props.VectoronMouseOver } onKeyPress={ props.VectoronKeyPress } onDrag={ props.VectoronDrag } onMouseLeave={ props.VectoronMouseLeave } onMouseUp={ props.VectoronMouseUp } onMouseDown={ props.VectoronMouseDown } onKeyDown={ props.VectoronKeyDown } onChange={ props.VectoronChange } ondelay={ props.Vectorondelay } width="12" height="6">
                <path d="M12 0L6 6L0 0L12 0Z" />
              </svg>
            </CSSTransition>
          </div>

        </CSSTransition>
        <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['chevronup']?.animationClass || {}}>

          <div id="id_fourthreeeight_fiveonetwootwoo" className={` frame chevronup ${ props.onClick ? 'cursor' : '' } ${ transaction['chevronup']?.type ? transaction['chevronup']?.type.toLowerCase() : '' }`} style={ { ...{}, ...props.ChevronUpStyle , transitionDuration: transaction['chevronup']?.duration, transitionTimingFunction: transaction['chevronup']?.timingFunction } } onClick={ props.ChevronUponClick } onMouseEnter={ props.ChevronUponMouseEnter } onMouseOver={ props.ChevronUponMouseOver } onKeyPress={ props.ChevronUponKeyPress } onDrag={ props.ChevronUponDrag } onMouseLeave={ props.ChevronUponMouseLeave } onMouseUp={ props.ChevronUponMouseUp } onMouseDown={ props.ChevronUponMouseDown } onKeyDown={ props.ChevronUponKeyDown } onChange={ props.ChevronUponChange } ondelay={ props.ChevronUpondelay }>
            <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['vector']?.animationClass || {}}>
              <svg id="id_fourthreeeight_fiveonetwoothree" className="vector  " style={{}} onClick={ props.VectoronClick } onMouseEnter={ props.VectoronMouseEnter } onMouseOver={ props.VectoronMouseOver } onKeyPress={ props.VectoronKeyPress } onDrag={ props.VectoronDrag } onMouseLeave={ props.VectoronMouseLeave } onMouseUp={ props.VectoronMouseUp } onMouseDown={ props.VectoronMouseDown } onKeyDown={ props.VectoronKeyDown } onChange={ props.VectoronChange } ondelay={ props.Vectorondelay } width="12" height="6">
                <path d="M12 6L6 0L0 6L12 6Z" />
              </svg>
            </CSSTransition>
          </div>

        </CSSTransition>
        <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['frame']?.animationClass || {}}>

          <div id="id_fourthreeeight_sixonefiveone" className={` frame frame ${ props.onClick ? 'cursor' : '' } ${ transaction['frame']?.type ? transaction['frame']?.type.toLowerCase() : '' }`} style={ { ...{}, ...props.FrameStyle , transitionDuration: transaction['frame']?.duration, transitionTimingFunction: transaction['frame']?.timingFunction } } onClick={ props.FrameonClick } onMouseEnter={ props.FrameonMouseEnter } onMouseOver={ props.FrameonMouseOver } onKeyPress={ props.FrameonKeyPress } onDrag={ props.FrameonDrag } onMouseLeave={ props.FrameonMouseLeave } onMouseUp={ props.FrameonMouseUp } onMouseDown={ props.FrameonMouseDown } onKeyDown={ props.FrameonKeyDown } onChange={ props.FrameonChange } ondelay={ props.Frameondelay }>
            <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['vector']?.animationClass || {}}>
              <svg id="id_fourthreeeight_sixonefivetwoo" className="vector  " style={{}} onClick={ props.VectoronClick } onMouseEnter={ props.VectoronMouseEnter } onMouseOver={ props.VectoronMouseOver } onKeyPress={ props.VectoronKeyPress } onDrag={ props.VectoronDrag } onMouseLeave={ props.VectoronMouseLeave } onMouseUp={ props.VectoronMouseUp } onMouseDown={ props.VectoronMouseDown } onKeyDown={ props.VectoronKeyDown } onChange={ props.VectoronChange } ondelay={ props.Vectorondelay } width="22" height="20">
                <path d="M0 2C0 1.46957 0.210714 0.960859 0.585786 0.585786C0.960859 0.210714 1.46957 4.44089e-16 2 0L7.155 0C7.6816 1.33777e-05 8.19891 0.138638 8.65496 0.401943C9.111 0.665247 9.4897 1.04395 9.753 1.5L10.619 3L20 3C20.5304 3 21.0391 3.21071 21.4142 3.58579C21.7893 3.96086 22 4.46957 22 5L22 18C22 18.5304 21.7893 19.0391 21.4142 19.4142C21.0391 19.7893 20.5304 20 20 20L2 20C1.46957 20 0.960859 19.7893 0.585786 19.4142C0.210714 19.0391 2.22045e-16 18.5304 0 18L0 2ZM7.155 2L2 2L2 18L20 18L20 5L9.464 5L8.021 2.5C7.93323 2.34798 7.807 2.22175 7.65499 2.13398C7.50297 2.04621 7.33053 2 7.155 2Z" />
              </svg>
            </CSSTransition>
          </div>

        </CSSTransition>
        <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['vector']?.animationClass || {}}>
          <svg id="id_fourthreeeight_sixfivenigthone" className="vector  " style={{}} onClick={ props.VectoronClick } onMouseEnter={ props.VectoronMouseEnter } onMouseOver={ props.VectoronMouseOver } onKeyPress={ props.VectoronKeyPress } onDrag={ props.VectoronDrag } onMouseLeave={ props.VectoronMouseLeave } onMouseUp={ props.VectoronMouseUp } onMouseDown={ props.VectoronMouseDown } onKeyDown={ props.VectoronKeyDown } onChange={ props.VectoronChange } ondelay={ props.Vectorondelay } width="22" height="16">
            <path d="M22 16L0 16L0 13.8182L22 13.8182L22 16ZM22 9.09091L0 9.09091L0 6.90909L22 6.90909L22 9.09091ZM22 2.18182L0 2.18182L0 0L22 0L22 2.18182Z" />
          </svg>
        </CSSTransition>
        <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['frametwoosixnigth']?.animationClass || {}}>

          <div id="id_fourthreeeight_sixfivenigthtwoo" className={` frame frametwoosixnigth ${ props.onClick ? 'cursor' : '' } ${ transaction['frametwoosixnigth']?.type ? transaction['frametwoosixnigth']?.type.toLowerCase() : '' }`} style={ { ...{}, ...props.FrametwoosixnigthStyle , transitionDuration: transaction['frametwoosixnigth']?.duration, transitionTimingFunction: transaction['frametwoosixnigth']?.timingFunction } } onClick={ props.FrametwoosixnigthonClick } onMouseEnter={ props.FrametwoosixnigthonMouseEnter } onMouseOver={ props.FrametwoosixnigthonMouseOver } onKeyPress={ props.FrametwoosixnigthonKeyPress } onDrag={ props.FrametwoosixnigthonDrag } onMouseLeave={ props.FrametwoosixnigthonMouseLeave } onMouseUp={ props.FrametwoosixnigthonMouseUp } onMouseDown={ props.FrametwoosixnigthonMouseDown } onKeyDown={ props.FrametwoosixnigthonKeyDown } onChange={ props.FrametwoosixnigthonChange } ondelay={ props.Frametwoosixnigthondelay }>
            <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['vectortwoo']?.animationClass || {}}>
              <svg id="id_fourthreeeight_sixfivenigththree" className="vectortwoo  " style={{}} onClick={ props.VectortwooonClick } onMouseEnter={ props.VectortwooonMouseEnter } onMouseOver={ props.VectortwooonMouseOver } onKeyPress={ props.VectortwooonKeyPress } onDrag={ props.VectortwooonDrag } onMouseLeave={ props.VectortwooonMouseLeave } onMouseUp={ props.VectortwooonMouseUp } onMouseDown={ props.VectortwooonMouseDown } onKeyDown={ props.VectortwooonKeyDown } onChange={ props.VectortwooonChange } ondelay={ props.Vectortwooondelay } width="18" height="18">
                <path d="M18 1.81286L16.1871 0L9 7.18714L1.81286 0L0 1.81286L7.18714 9L0 16.1871L1.81286 18L9 10.8129L16.1871 18L18 16.1871L10.8129 9L18 1.81286Z" />
              </svg>
            </CSSTransition>
          </div>

        </CSSTransition>

      </>
      }
    </div>

  </CSSTransition>
</>
    ) 
}

Icons.propTypes = {
    style: PropTypes.any,
IconHomeHoverImg0: PropTypes.any,
IconHomeOffImg0: PropTypes.any,
IconHomeOnImg0: PropTypes.any,
IconSearchHoverImg0: PropTypes.any,
IconSearchOffImg0: PropTypes.any,
IconSearchOnImg0: PropTypes.any,
IconLibraryHoverImg0: PropTypes.any,
IconCreateHoverImg0: PropTypes.any,
IconCreateOffImg0: PropTypes.any,
IconLeftOffImg0: PropTypes.any,
IconLeftHoverImg0: PropTypes.any,
IconLRigthOffImg0: PropTypes.any,
IconInstagramOffImg0: PropTypes.any,
IconInstagramHoverImg0: PropTypes.any,
IconTwitterOffImg0: PropTypes.any,
IconTwitterHoverImg0: PropTypes.any,
IconTwitterOffImg1: PropTypes.any,
IconTwitterHoverImg1: PropTypes.any,
IconRigthHoverImg0: PropTypes.any,
IconLibraryOffImg0: PropTypes.any,
IconLibraryOnImg0: PropTypes.any,
IconLibraryOnImg1: PropTypes.any,
IconsonClick: PropTypes.any,
IconsonMouseEnter: PropTypes.any,
IconsonMouseOver: PropTypes.any,
IconsonKeyPress: PropTypes.any,
IconsonDrag: PropTypes.any,
IconsonMouseLeave: PropTypes.any,
IconsonMouseUp: PropTypes.any,
IconsonMouseDown: PropTypes.any,
IconsonKeyDown: PropTypes.any,
IconsonChange: PropTypes.any,
Iconsondelay: PropTypes.any,
IconHomeHoverImgonClick: PropTypes.any,
IconHomeHoverImgonMouseEnter: PropTypes.any,
IconHomeHoverImgonMouseOver: PropTypes.any,
IconHomeHoverImgonKeyPress: PropTypes.any,
IconHomeHoverImgonDrag: PropTypes.any,
IconHomeHoverImgonMouseLeave: PropTypes.any,
IconHomeHoverImgonMouseUp: PropTypes.any,
IconHomeHoverImgonMouseDown: PropTypes.any,
IconHomeHoverImgonKeyDown: PropTypes.any,
IconHomeHoverImgonChange: PropTypes.any,
IconHomeHoverImgondelay: PropTypes.any,
IconHomeOffImgonClick: PropTypes.any,
IconHomeOffImgonMouseEnter: PropTypes.any,
IconHomeOffImgonMouseOver: PropTypes.any,
IconHomeOffImgonKeyPress: PropTypes.any,
IconHomeOffImgonDrag: PropTypes.any,
IconHomeOffImgonMouseLeave: PropTypes.any,
IconHomeOffImgonMouseUp: PropTypes.any,
IconHomeOffImgonMouseDown: PropTypes.any,
IconHomeOffImgonKeyDown: PropTypes.any,
IconHomeOffImgonChange: PropTypes.any,
IconHomeOffImgondelay: PropTypes.any,
IconHomeOnImgonClick: PropTypes.any,
IconHomeOnImgonMouseEnter: PropTypes.any,
IconHomeOnImgonMouseOver: PropTypes.any,
IconHomeOnImgonKeyPress: PropTypes.any,
IconHomeOnImgonDrag: PropTypes.any,
IconHomeOnImgonMouseLeave: PropTypes.any,
IconHomeOnImgonMouseUp: PropTypes.any,
IconHomeOnImgonMouseDown: PropTypes.any,
IconHomeOnImgonKeyDown: PropTypes.any,
IconHomeOnImgonChange: PropTypes.any,
IconHomeOnImgondelay: PropTypes.any,
IconSearchHoverImgonClick: PropTypes.any,
IconSearchHoverImgonMouseEnter: PropTypes.any,
IconSearchHoverImgonMouseOver: PropTypes.any,
IconSearchHoverImgonKeyPress: PropTypes.any,
IconSearchHoverImgonDrag: PropTypes.any,
IconSearchHoverImgonMouseLeave: PropTypes.any,
IconSearchHoverImgonMouseUp: PropTypes.any,
IconSearchHoverImgonMouseDown: PropTypes.any,
IconSearchHoverImgonKeyDown: PropTypes.any,
IconSearchHoverImgonChange: PropTypes.any,
IconSearchHoverImgondelay: PropTypes.any,
IconSearchOffImgonClick: PropTypes.any,
IconSearchOffImgonMouseEnter: PropTypes.any,
IconSearchOffImgonMouseOver: PropTypes.any,
IconSearchOffImgonKeyPress: PropTypes.any,
IconSearchOffImgonDrag: PropTypes.any,
IconSearchOffImgonMouseLeave: PropTypes.any,
IconSearchOffImgonMouseUp: PropTypes.any,
IconSearchOffImgonMouseDown: PropTypes.any,
IconSearchOffImgonKeyDown: PropTypes.any,
IconSearchOffImgonChange: PropTypes.any,
IconSearchOffImgondelay: PropTypes.any,
IconSearchOnImgonClick: PropTypes.any,
IconSearchOnImgonMouseEnter: PropTypes.any,
IconSearchOnImgonMouseOver: PropTypes.any,
IconSearchOnImgonKeyPress: PropTypes.any,
IconSearchOnImgonDrag: PropTypes.any,
IconSearchOnImgonMouseLeave: PropTypes.any,
IconSearchOnImgonMouseUp: PropTypes.any,
IconSearchOnImgonMouseDown: PropTypes.any,
IconSearchOnImgonKeyDown: PropTypes.any,
IconSearchOnImgonChange: PropTypes.any,
IconSearchOnImgondelay: PropTypes.any,
IconLibraryHoverImgonClick: PropTypes.any,
IconLibraryHoverImgonMouseEnter: PropTypes.any,
IconLibraryHoverImgonMouseOver: PropTypes.any,
IconLibraryHoverImgonKeyPress: PropTypes.any,
IconLibraryHoverImgonDrag: PropTypes.any,
IconLibraryHoverImgonMouseLeave: PropTypes.any,
IconLibraryHoverImgonMouseUp: PropTypes.any,
IconLibraryHoverImgonMouseDown: PropTypes.any,
IconLibraryHoverImgonKeyDown: PropTypes.any,
IconLibraryHoverImgonChange: PropTypes.any,
IconLibraryHoverImgondelay: PropTypes.any,
IconCreateHoverImgonClick: PropTypes.any,
IconCreateHoverImgonMouseEnter: PropTypes.any,
IconCreateHoverImgonMouseOver: PropTypes.any,
IconCreateHoverImgonKeyPress: PropTypes.any,
IconCreateHoverImgonDrag: PropTypes.any,
IconCreateHoverImgonMouseLeave: PropTypes.any,
IconCreateHoverImgonMouseUp: PropTypes.any,
IconCreateHoverImgonMouseDown: PropTypes.any,
IconCreateHoverImgonKeyDown: PropTypes.any,
IconCreateHoverImgonChange: PropTypes.any,
IconCreateHoverImgondelay: PropTypes.any,
IconCreateOffImgonClick: PropTypes.any,
IconCreateOffImgonMouseEnter: PropTypes.any,
IconCreateOffImgonMouseOver: PropTypes.any,
IconCreateOffImgonKeyPress: PropTypes.any,
IconCreateOffImgonDrag: PropTypes.any,
IconCreateOffImgonMouseLeave: PropTypes.any,
IconCreateOffImgonMouseUp: PropTypes.any,
IconCreateOffImgonMouseDown: PropTypes.any,
IconCreateOffImgonKeyDown: PropTypes.any,
IconCreateOffImgonChange: PropTypes.any,
IconCreateOffImgondelay: PropTypes.any,
IconLeftOffImgonClick: PropTypes.any,
IconLeftOffImgonMouseEnter: PropTypes.any,
IconLeftOffImgonMouseOver: PropTypes.any,
IconLeftOffImgonKeyPress: PropTypes.any,
IconLeftOffImgonDrag: PropTypes.any,
IconLeftOffImgonMouseLeave: PropTypes.any,
IconLeftOffImgonMouseUp: PropTypes.any,
IconLeftOffImgonMouseDown: PropTypes.any,
IconLeftOffImgonKeyDown: PropTypes.any,
IconLeftOffImgonChange: PropTypes.any,
IconLeftOffImgondelay: PropTypes.any,
IconLeftHoverImgonClick: PropTypes.any,
IconLeftHoverImgonMouseEnter: PropTypes.any,
IconLeftHoverImgonMouseOver: PropTypes.any,
IconLeftHoverImgonKeyPress: PropTypes.any,
IconLeftHoverImgonDrag: PropTypes.any,
IconLeftHoverImgonMouseLeave: PropTypes.any,
IconLeftHoverImgonMouseUp: PropTypes.any,
IconLeftHoverImgonMouseDown: PropTypes.any,
IconLeftHoverImgonKeyDown: PropTypes.any,
IconLeftHoverImgonChange: PropTypes.any,
IconLeftHoverImgondelay: PropTypes.any,
IconLRigthOffImgonClick: PropTypes.any,
IconLRigthOffImgonMouseEnter: PropTypes.any,
IconLRigthOffImgonMouseOver: PropTypes.any,
IconLRigthOffImgonKeyPress: PropTypes.any,
IconLRigthOffImgonDrag: PropTypes.any,
IconLRigthOffImgonMouseLeave: PropTypes.any,
IconLRigthOffImgonMouseUp: PropTypes.any,
IconLRigthOffImgonMouseDown: PropTypes.any,
IconLRigthOffImgonKeyDown: PropTypes.any,
IconLRigthOffImgonChange: PropTypes.any,
IconLRigthOffImgondelay: PropTypes.any,
IconInstagramOffImgonClick: PropTypes.any,
IconInstagramOffImgonMouseEnter: PropTypes.any,
IconInstagramOffImgonMouseOver: PropTypes.any,
IconInstagramOffImgonKeyPress: PropTypes.any,
IconInstagramOffImgonDrag: PropTypes.any,
IconInstagramOffImgonMouseLeave: PropTypes.any,
IconInstagramOffImgonMouseUp: PropTypes.any,
IconInstagramOffImgonMouseDown: PropTypes.any,
IconInstagramOffImgonKeyDown: PropTypes.any,
IconInstagramOffImgonChange: PropTypes.any,
IconInstagramOffImgondelay: PropTypes.any,
IconInstagramHoverImgonClick: PropTypes.any,
IconInstagramHoverImgonMouseEnter: PropTypes.any,
IconInstagramHoverImgonMouseOver: PropTypes.any,
IconInstagramHoverImgonKeyPress: PropTypes.any,
IconInstagramHoverImgonDrag: PropTypes.any,
IconInstagramHoverImgonMouseLeave: PropTypes.any,
IconInstagramHoverImgonMouseUp: PropTypes.any,
IconInstagramHoverImgonMouseDown: PropTypes.any,
IconInstagramHoverImgonKeyDown: PropTypes.any,
IconInstagramHoverImgonChange: PropTypes.any,
IconInstagramHoverImgondelay: PropTypes.any,
IconTwitterOffImgonClick: PropTypes.any,
IconTwitterOffImgonMouseEnter: PropTypes.any,
IconTwitterOffImgonMouseOver: PropTypes.any,
IconTwitterOffImgonKeyPress: PropTypes.any,
IconTwitterOffImgonDrag: PropTypes.any,
IconTwitterOffImgonMouseLeave: PropTypes.any,
IconTwitterOffImgonMouseUp: PropTypes.any,
IconTwitterOffImgonMouseDown: PropTypes.any,
IconTwitterOffImgonKeyDown: PropTypes.any,
IconTwitterOffImgonChange: PropTypes.any,
IconTwitterOffImgondelay: PropTypes.any,
IconTwitterHoverImgonClick: PropTypes.any,
IconTwitterHoverImgonMouseEnter: PropTypes.any,
IconTwitterHoverImgonMouseOver: PropTypes.any,
IconTwitterHoverImgonKeyPress: PropTypes.any,
IconTwitterHoverImgonDrag: PropTypes.any,
IconTwitterHoverImgonMouseLeave: PropTypes.any,
IconTwitterHoverImgonMouseUp: PropTypes.any,
IconTwitterHoverImgonMouseDown: PropTypes.any,
IconTwitterHoverImgonKeyDown: PropTypes.any,
IconTwitterHoverImgonChange: PropTypes.any,
IconTwitterHoverImgondelay: PropTypes.any,
IconRigthHoverImgonClick: PropTypes.any,
IconRigthHoverImgonMouseEnter: PropTypes.any,
IconRigthHoverImgonMouseOver: PropTypes.any,
IconRigthHoverImgonKeyPress: PropTypes.any,
IconRigthHoverImgonDrag: PropTypes.any,
IconRigthHoverImgonMouseLeave: PropTypes.any,
IconRigthHoverImgonMouseUp: PropTypes.any,
IconRigthHoverImgonMouseDown: PropTypes.any,
IconRigthHoverImgonKeyDown: PropTypes.any,
IconRigthHoverImgonChange: PropTypes.any,
IconRigthHoverImgondelay: PropTypes.any,
IconLibraryOffImgonClick: PropTypes.any,
IconLibraryOffImgonMouseEnter: PropTypes.any,
IconLibraryOffImgonMouseOver: PropTypes.any,
IconLibraryOffImgonKeyPress: PropTypes.any,
IconLibraryOffImgonDrag: PropTypes.any,
IconLibraryOffImgonMouseLeave: PropTypes.any,
IconLibraryOffImgonMouseUp: PropTypes.any,
IconLibraryOffImgonMouseDown: PropTypes.any,
IconLibraryOffImgonKeyDown: PropTypes.any,
IconLibraryOffImgonChange: PropTypes.any,
IconLibraryOffImgondelay: PropTypes.any,
IconLibraryOnImgonClick: PropTypes.any,
IconLibraryOnImgonMouseEnter: PropTypes.any,
IconLibraryOnImgonMouseOver: PropTypes.any,
IconLibraryOnImgonKeyPress: PropTypes.any,
IconLibraryOnImgonDrag: PropTypes.any,
IconLibraryOnImgonMouseLeave: PropTypes.any,
IconLibraryOnImgonMouseUp: PropTypes.any,
IconLibraryOnImgonMouseDown: PropTypes.any,
IconLibraryOnImgonKeyDown: PropTypes.any,
IconLibraryOnImgonChange: PropTypes.any,
IconLibraryOnImgondelay: PropTypes.any,
PalyIconOffonClick: PropTypes.any,
PalyIconOffonMouseEnter: PropTypes.any,
PalyIconOffonMouseOver: PropTypes.any,
PalyIconOffonKeyPress: PropTypes.any,
PalyIconOffonDrag: PropTypes.any,
PalyIconOffonMouseLeave: PropTypes.any,
PalyIconOffonMouseUp: PropTypes.any,
PalyIconOffonMouseDown: PropTypes.any,
PalyIconOffonKeyDown: PropTypes.any,
PalyIconOffonChange: PropTypes.any,
PalyIconOffondelay: PropTypes.any,
PlayIconVectorOffonClick: PropTypes.any,
PlayIconVectorOffonMouseEnter: PropTypes.any,
PlayIconVectorOffonMouseOver: PropTypes.any,
PlayIconVectorOffonKeyPress: PropTypes.any,
PlayIconVectorOffonDrag: PropTypes.any,
PlayIconVectorOffonMouseLeave: PropTypes.any,
PlayIconVectorOffonMouseUp: PropTypes.any,
PlayIconVectorOffonMouseDown: PropTypes.any,
PlayIconVectorOffonKeyDown: PropTypes.any,
PlayIconVectorOffonChange: PropTypes.any,
PlayIconVectorOffondelay: PropTypes.any,
PalyIconOnonClick: PropTypes.any,
PalyIconOnonMouseEnter: PropTypes.any,
PalyIconOnonMouseOver: PropTypes.any,
PalyIconOnonKeyPress: PropTypes.any,
PalyIconOnonDrag: PropTypes.any,
PalyIconOnonMouseLeave: PropTypes.any,
PalyIconOnonMouseUp: PropTypes.any,
PalyIconOnonMouseDown: PropTypes.any,
PalyIconOnonKeyDown: PropTypes.any,
PalyIconOnonChange: PropTypes.any,
PalyIconOnondelay: PropTypes.any,
PlayIconVectorOnonClick: PropTypes.any,
PlayIconVectorOnonMouseEnter: PropTypes.any,
PlayIconVectorOnonMouseOver: PropTypes.any,
PlayIconVectorOnonKeyPress: PropTypes.any,
PlayIconVectorOnonDrag: PropTypes.any,
PlayIconVectorOnonMouseLeave: PropTypes.any,
PlayIconVectorOnonMouseUp: PropTypes.any,
PlayIconVectorOnonMouseDown: PropTypes.any,
PlayIconVectorOnonKeyDown: PropTypes.any,
PlayIconVectorOnonChange: PropTypes.any,
PlayIconVectorOnondelay: PropTypes.any,
FramethreethreeoneonClick: PropTypes.any,
FramethreethreeoneonMouseEnter: PropTypes.any,
FramethreethreeoneonMouseOver: PropTypes.any,
FramethreethreeoneonKeyPress: PropTypes.any,
FramethreethreeoneonDrag: PropTypes.any,
FramethreethreeoneonMouseLeave: PropTypes.any,
FramethreethreeoneonMouseUp: PropTypes.any,
FramethreethreeoneonMouseDown: PropTypes.any,
FramethreethreeoneonKeyDown: PropTypes.any,
FramethreethreeoneonChange: PropTypes.any,
Framethreethreeoneondelay: PropTypes.any,
VectoronClick: PropTypes.any,
VectoronMouseEnter: PropTypes.any,
VectoronMouseOver: PropTypes.any,
VectoronKeyPress: PropTypes.any,
VectoronDrag: PropTypes.any,
VectoronMouseLeave: PropTypes.any,
VectoronMouseUp: PropTypes.any,
VectoronMouseDown: PropTypes.any,
VectoronKeyDown: PropTypes.any,
VectoronChange: PropTypes.any,
Vectorondelay: PropTypes.any,
FrameonClick: PropTypes.any,
FrameonMouseEnter: PropTypes.any,
FrameonMouseOver: PropTypes.any,
FrameonKeyPress: PropTypes.any,
FrameonDrag: PropTypes.any,
FrameonMouseLeave: PropTypes.any,
FrameonMouseUp: PropTypes.any,
FrameonMouseDown: PropTypes.any,
FrameonKeyDown: PropTypes.any,
FrameonChange: PropTypes.any,
Frameondelay: PropTypes.any,
IconHomeonClick: PropTypes.any,
IconHomeonMouseEnter: PropTypes.any,
IconHomeonMouseOver: PropTypes.any,
IconHomeonKeyPress: PropTypes.any,
IconHomeonDrag: PropTypes.any,
IconHomeonMouseLeave: PropTypes.any,
IconHomeonMouseUp: PropTypes.any,
IconHomeonMouseDown: PropTypes.any,
IconHomeonKeyDown: PropTypes.any,
IconHomeonChange: PropTypes.any,
IconHomeondelay: PropTypes.any,
ChevronDownOffonClick: PropTypes.any,
ChevronDownOffonMouseEnter: PropTypes.any,
ChevronDownOffonMouseOver: PropTypes.any,
ChevronDownOffonKeyPress: PropTypes.any,
ChevronDownOffonDrag: PropTypes.any,
ChevronDownOffonMouseLeave: PropTypes.any,
ChevronDownOffonMouseUp: PropTypes.any,
ChevronDownOffonMouseDown: PropTypes.any,
ChevronDownOffonKeyDown: PropTypes.any,
ChevronDownOffonChange: PropTypes.any,
ChevronDownOffondelay: PropTypes.any,
ChevronDownONonClick: PropTypes.any,
ChevronDownONonMouseEnter: PropTypes.any,
ChevronDownONonMouseOver: PropTypes.any,
ChevronDownONonKeyPress: PropTypes.any,
ChevronDownONonDrag: PropTypes.any,
ChevronDownONonMouseLeave: PropTypes.any,
ChevronDownONonMouseUp: PropTypes.any,
ChevronDownONonMouseDown: PropTypes.any,
ChevronDownONonKeyDown: PropTypes.any,
ChevronDownONonChange: PropTypes.any,
ChevronDownONondelay: PropTypes.any,
ChevronUponClick: PropTypes.any,
ChevronUponMouseEnter: PropTypes.any,
ChevronUponMouseOver: PropTypes.any,
ChevronUponKeyPress: PropTypes.any,
ChevronUponDrag: PropTypes.any,
ChevronUponMouseLeave: PropTypes.any,
ChevronUponMouseUp: PropTypes.any,
ChevronUponMouseDown: PropTypes.any,
ChevronUponKeyDown: PropTypes.any,
ChevronUponChange: PropTypes.any,
ChevronUpondelay: PropTypes.any,
FrametwoosixnigthonClick: PropTypes.any,
FrametwoosixnigthonMouseEnter: PropTypes.any,
FrametwoosixnigthonMouseOver: PropTypes.any,
FrametwoosixnigthonKeyPress: PropTypes.any,
FrametwoosixnigthonDrag: PropTypes.any,
FrametwoosixnigthonMouseLeave: PropTypes.any,
FrametwoosixnigthonMouseUp: PropTypes.any,
FrametwoosixnigthonMouseDown: PropTypes.any,
FrametwoosixnigthonKeyDown: PropTypes.any,
FrametwoosixnigthonChange: PropTypes.any,
Frametwoosixnigthondelay: PropTypes.any,
VectortwooonClick: PropTypes.any,
VectortwooonMouseEnter: PropTypes.any,
VectortwooonMouseOver: PropTypes.any,
VectortwooonKeyPress: PropTypes.any,
VectortwooonDrag: PropTypes.any,
VectortwooonMouseLeave: PropTypes.any,
VectortwooonMouseUp: PropTypes.any,
VectortwooonMouseDown: PropTypes.any,
VectortwooonKeyDown: PropTypes.any,
VectortwooonChange: PropTypes.any,
Vectortwooondelay: PropTypes.any
}
export default Icons;