import React, { Suspense } from 'react';
import PropTypes from 'prop-types';
import * as Contexts  from 'Contexts';
import { OverlaySwapContext } from 'Components/OverlaySwapProvider';
import CorebooksAnimation from 'Components/CorebooksAnimation';
import CorebooksAnimationSingle from 'Components/CorebooksAnimationSingle';

import * as UtilsScripts  from 'utils/utils';
import { Route, useHistory, useParams } from "react-router-dom";
import SingletoneNavigation from 'Class/SingletoneNavigation';

import LoadingComponent from "Components/Loading";


import { Transition } from "react-transition-group";
import { CSSTransition } from "react-transition-group";
import { useAppContext, useSessionContext } from 'context/AppContext';
import './Displaythree.css'





const Displaythree = (props)=>{
    const [state,dispatch] = useAppContext();
    const [, setNameSelectedComponent] = React.useContext(OverlaySwapContext);
    
    const nodeRef = React.useRef(null);
    const history = useHistory();
    let RouterContext = useParams();
    
    
    
    
    
    
    const [globalStateForm, dispatchForm] = React.useState({});
    
    const [ _in, setIn ] = React.useState(false)
    const [ transaction, setTransaction ] = React.useState({}) 
    const [ animation, setAnimation ] = React.useState('')

    
    
    
    
    
    
    React.useEffect(()=>{
        
        
        
    },[]);
    
    
    return (
        <>
  <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['displaythree']?.animationClass || {}}>

    <div id="id_sixtwoosix_onesevenoneone" ref={nodeRef} className={` ${ props.onClick ? 'cursor' : '' } displaythree C_sixtwoosix_onesevenoneone ${ props.cssClass } ${ transaction['displaythree']?.type ? transaction['displaythree']?.type.toLowerCase() : '' }`} style={ { ...{ ...{}, transitionDuration: transaction['displaythree']?.duration, transitionTimingFunction: transaction['displaythree']?.timingFunction }, ...props.style }} onClick={ props.DisplaythreeonClick } onMouseEnter={ props.DisplaythreeonMouseEnter } onMouseOver={ props.DisplaythreeonMouseOver } onKeyPress={ props.DisplaythreeonKeyPress } onDrag={ props.DisplaythreeonDrag } onMouseLeave={ props.DisplaythreeonMouseLeave } onMouseUp={ props.DisplaythreeonMouseUp } onMouseDown={ props.DisplaythreeonMouseDown } onKeyDown={ props.DisplaythreeonKeyDown } onChange={ props.DisplaythreeonChange } ondelay={ props.Displaythreeondelay }>
      {
      props.children ?
      props.children :
      <>
        <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['displaythreetext']?.animationClass || {}}>

          <span id="id_sixtwoosix_onesevenonezero"  className={` text displaythreetext    ${ props.onClick ? 'cursor' : ''}  ${ transaction['displaythreetext']?.type ? transaction['displaythreetext']?.type.toLowerCase() : '' }`} style={{ ...{},  ...props.DisplaythreeTextStyle , transitionDuration: transaction['displaythreetext']?.duration, transitionTimingFunction: transaction['displaythreetext']?.timingFunction }} onClick={ props.DisplaythreeTextonClick } onMouseEnter={ props.DisplaythreeTextonMouseEnter } onMouseOver={ props.DisplaythreeTextonMouseOver } onKeyPress={ props.DisplaythreeTextonKeyPress } onDrag={ props.DisplaythreeTextonDrag } onMouseLeave={ props.DisplaythreeTextonMouseLeave } onMouseUp={ props.DisplaythreeTextonMouseUp } onMouseDown={ props.DisplaythreeTextonMouseDown } onKeyDown={ props.DisplaythreeTextonKeyDown } onChange={ props.DisplaythreeTextonChange } ondelay={ props.DisplaythreeTextondelay } >{props.DisplaythreeText0 || `Display 3`}</span>

        </CSSTransition>

      </>
      }
    </div>

  </CSSTransition>
</>
    ) 
}

Displaythree.propTypes = {
    style: PropTypes.any,
DisplaythreeText0: PropTypes.any,
DisplaythreeonClick: PropTypes.any,
DisplaythreeonMouseEnter: PropTypes.any,
DisplaythreeonMouseOver: PropTypes.any,
DisplaythreeonKeyPress: PropTypes.any,
DisplaythreeonDrag: PropTypes.any,
DisplaythreeonMouseLeave: PropTypes.any,
DisplaythreeonMouseUp: PropTypes.any,
DisplaythreeonMouseDown: PropTypes.any,
DisplaythreeonKeyDown: PropTypes.any,
DisplaythreeonChange: PropTypes.any,
Displaythreeondelay: PropTypes.any,
DisplaythreeTextonClick: PropTypes.any,
DisplaythreeTextonMouseEnter: PropTypes.any,
DisplaythreeTextonMouseOver: PropTypes.any,
DisplaythreeTextonKeyPress: PropTypes.any,
DisplaythreeTextonDrag: PropTypes.any,
DisplaythreeTextonMouseLeave: PropTypes.any,
DisplaythreeTextonMouseUp: PropTypes.any,
DisplaythreeTextonMouseDown: PropTypes.any,
DisplaythreeTextonKeyDown: PropTypes.any,
DisplaythreeTextonChange: PropTypes.any,
DisplaythreeTextondelay: PropTypes.any
}
export default Displaythree;