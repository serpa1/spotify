import React, { Suspense } from 'react';
import PropTypes from 'prop-types';
import * as Contexts  from 'Contexts';
import { OverlaySwapContext } from 'Components/OverlaySwapProvider';
import CorebooksAnimation from 'Components/CorebooksAnimation';
import CorebooksAnimationSingle from 'Components/CorebooksAnimationSingle';

import * as UtilsScripts  from 'utils/utils';
import { Route, useHistory, useParams } from "react-router-dom";
import SingletoneNavigation from 'Class/SingletoneNavigation';

import LoadingComponent from "Components/Loading";


import { Transition } from "react-transition-group";
import { CSSTransition } from "react-transition-group";
import Paragraphone from 'Components/Paragraphone'
import Paragraphtwoo from 'Components/Paragraphtwoo'
import PlayIconAnimation from 'Components/PlayIconAnimation'
import { useAppContext, useSessionContext } from 'context/AppContext';
import './CardPlaylistImgCont.css'





const CardPlaylistImgCont = (props)=>{
    const [state,dispatch] = useAppContext();
    const [, setNameSelectedComponent] = React.useContext(OverlaySwapContext);
    
    const nodeRef = React.useRef(null);
    const history = useHistory();
    let RouterContext = useParams();
    
    
    
    
    
    
    const [globalStateForm, dispatchForm] = React.useState({});
    
    const [ _in, setIn ] = React.useState(false)
    const [ transaction, setTransaction ] = React.useState({}) 
    const [ animation, setAnimation ] = React.useState('')

    
    
    
    
    
    
    React.useEffect(()=>{
        
        
        
    },[]);
    
    
    return (
        <>
  <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['cardplaylistimgcont']?.animationClass || {}}>

    <div id="id_twooone_threefoursix" ref={nodeRef} className={` ${ props.onClick ? 'cursor' : '' } cardplaylistimgcont C_twooone_threefoursix ${ props.cssClass } ${ transaction['cardplaylistimgcont']?.type ? transaction['cardplaylistimgcont']?.type.toLowerCase() : '' }`} style={ { ...{ ...{"width":"189px"}, transitionDuration: transaction['cardplaylistimgcont']?.duration, transitionTimingFunction: transaction['cardplaylistimgcont']?.timingFunction }, ...props.style }} onClick={ props.CardPlaylistImgContonClick } onMouseEnter={ props.CardPlaylistImgContonMouseEnter } onMouseOver={ props.CardPlaylistImgContonMouseOver } onKeyPress={ props.CardPlaylistImgContonKeyPress } onDrag={ props.CardPlaylistImgContonDrag } onMouseLeave={ props.CardPlaylistImgContonMouseLeave } onMouseUp={ props.CardPlaylistImgContonMouseUp } onMouseDown={ props.CardPlaylistImgContonMouseDown } onKeyDown={ props.CardPlaylistImgContonKeyDown } onChange={ props.CardPlaylistImgContonChange } ondelay={ props.CardPlaylistImgContondelay }>
      {
      props.children ?
      props.children :
      <>
        <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['cardplaylistimg']?.animationClass || {}}>

          <div id="id_twooone_fivesixeight" className={` frame cardplaylistimg ${ props.onClick ? 'cursor' : '' } ${ transaction['cardplaylistimg']?.type ? transaction['cardplaylistimg']?.type.toLowerCase() : '' }`} style={ { ...{}, ...props.CardPlaylistImgStyle , transitionDuration: transaction['cardplaylistimg']?.duration, transitionTimingFunction: transaction['cardplaylistimg']?.timingFunction } } onClick={ props.CardPlaylistImgonClick } onMouseEnter={ props.CardPlaylistImgonMouseEnter } onMouseOver={ props.CardPlaylistImgonMouseOver } onKeyPress={ props.CardPlaylistImgonKeyPress } onDrag={ props.CardPlaylistImgonDrag } onMouseLeave={ props.CardPlaylistImgonMouseLeave } onMouseUp={ props.CardPlaylistImgonMouseUp } onMouseDown={ props.CardPlaylistImgonMouseDown } onKeyDown={ props.CardPlaylistImgonKeyDown } onChange={ props.CardPlaylistImgonChange } ondelay={ props.CardPlaylistImgondelay }>
            <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['imageplayon']?.animationClass || {}}>

              <div id="id_twooone_fivesixnigth" className={` frame imageplayon ${ props.onClick ? 'cursor' : '' } ${ transaction['imageplayon']?.type ? transaction['imageplayon']?.type.toLowerCase() : '' }`} style={ { ...{}, ...props.ImagePlayOnStyle , transitionDuration: transaction['imageplayon']?.duration, transitionTimingFunction: transaction['imageplayon']?.timingFunction } } onClick={ props.ImagePlayOnonClick } onMouseEnter={ props.ImagePlayOnonMouseEnter } onMouseOver={ props.ImagePlayOnonMouseOver } onKeyPress={ props.ImagePlayOnonKeyPress } onDrag={ props.ImagePlayOnonDrag } onMouseLeave={ props.ImagePlayOnonMouseLeave } onMouseUp={ props.ImagePlayOnonMouseUp } onMouseDown={ props.ImagePlayOnonMouseDown } onKeyDown={ props.ImagePlayOnonKeyDown } onChange={ props.ImagePlayOnonChange } ondelay={ props.ImagePlayOnondelay }>
                <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['playiconanimation']?.animationClass || {}}>
                  <PlayIconAnimation { ...{ ...props, style:false } } variant={'Property 1=Default'} cssClass={"C_twooone_fivesevenzero cursor "}  />
    </CSSTransition >
                    </div>
               
    </CSSTransition >
                    </div>
               
    </CSSTransition >
<CSSTransition 
        in={_in}
        appear={true} 
        timeout={100}
        classNames={transaction['cardplaylistdata']?.animationClass || {}}
    >
    
                    <div id="id_onenigth_twoosevenone" className={` frame cardplaylistdata ${ props.onClick ? 'cursor' : '' } ${ transaction['cardplaylistdata']?.type ? transaction['cardplaylistdata']?.type.toLowerCase() : '' }`} style={ { ...{}, ...props.CardPlaylistDataStyle , transitionDuration: transaction['cardplaylistdata']?.duration, transitionTimingFunction: transaction['cardplaylistdata']?.timingFunction } } onClick={ props.CardPlaylistDataonClick } onMouseEnter={ props.CardPlaylistDataonMouseEnter } onMouseOver={ props.CardPlaylistDataonMouseOver } onKeyPress={ props.CardPlaylistDataonKeyPress } onDrag={ props.CardPlaylistDataonDrag } onMouseLeave={ props.CardPlaylistDataonMouseLeave } onMouseUp={ props.CardPlaylistDataonMouseUp } onMouseDown={ props.CardPlaylistDataonMouseDown } onKeyDown={ props.CardPlaylistDataonKeyDown } onChange={ props.CardPlaylistDataonChange } ondelay={ props.CardPlaylistDataondelay }>
                    <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['paragraphtwoo']?.animationClass || {}}>
                      <Paragraphtwoo { ...{ ...props, style:false } } ParagraphtwooText0={ props.ParagraphtwooText0 || "Peacefull Piano" } cssClass={"C_oneseven_twoosixfive "}  />
    </CSSTransition >
<CSSTransition 
        in={_in}
        appear={true} 
        timeout={100}
        classNames={transaction['paragraphone']?.animationClass || {}}
    >
    <Paragraphone { ...{ ...props, style:false } }   ParagraphoneText0={ props.ParagraphoneText0 || " Peaceful piano to help\nyou slow down, breathe"} cssClass={"C_onenigth_twoosixnigth "}  />
    </CSSTransition >
                    </div>
               
    </CSSTransition >
            
            </>
        }
        </div>
    
    </CSSTransition >
            </>
        
    ) 
}

CardPlaylistImgCont.propTypes = {
    style: PropTypes.any,
ParagraphtwooText0: PropTypes.any,
ParagraphoneText0: PropTypes.any,
CardPlaylistImgContonClick: PropTypes.any,
CardPlaylistImgContonMouseEnter: PropTypes.any,
CardPlaylistImgContonMouseOver: PropTypes.any,
CardPlaylistImgContonKeyPress: PropTypes.any,
CardPlaylistImgContonDrag: PropTypes.any,
CardPlaylistImgContonMouseLeave: PropTypes.any,
CardPlaylistImgContonMouseUp: PropTypes.any,
CardPlaylistImgContonMouseDown: PropTypes.any,
CardPlaylistImgContonKeyDown: PropTypes.any,
CardPlaylistImgContonChange: PropTypes.any,
CardPlaylistImgContondelay: PropTypes.any,
CardPlaylistImgonClick: PropTypes.any,
CardPlaylistImgonMouseEnter: PropTypes.any,
CardPlaylistImgonMouseOver: PropTypes.any,
CardPlaylistImgonKeyPress: PropTypes.any,
CardPlaylistImgonDrag: PropTypes.any,
CardPlaylistImgonMouseLeave: PropTypes.any,
CardPlaylistImgonMouseUp: PropTypes.any,
CardPlaylistImgonMouseDown: PropTypes.any,
CardPlaylistImgonKeyDown: PropTypes.any,
CardPlaylistImgonChange: PropTypes.any,
CardPlaylistImgondelay: PropTypes.any,
ImagePlayOnonClick: PropTypes.any,
ImagePlayOnonMouseEnter: PropTypes.any,
ImagePlayOnonMouseOver: PropTypes.any,
ImagePlayOnonKeyPress: PropTypes.any,
ImagePlayOnonDrag: PropTypes.any,
ImagePlayOnonMouseLeave: PropTypes.any,
ImagePlayOnonMouseUp: PropTypes.any,
ImagePlayOnonMouseDown: PropTypes.any,
ImagePlayOnonKeyDown: PropTypes.any,
ImagePlayOnonChange: PropTypes.any,
ImagePlayOnondelay: PropTypes.any,
CardPlaylistDataonClick: PropTypes.any,
CardPlaylistDataonMouseEnter: PropTypes.any,
CardPlaylistDataonMouseOver: PropTypes.any,
CardPlaylistDataonKeyPress: PropTypes.any,
CardPlaylistDataonDrag: PropTypes.any,
CardPlaylistDataonMouseLeave: PropTypes.any,
CardPlaylistDataonMouseUp: PropTypes.any,
CardPlaylistDataonMouseDown: PropTypes.any,
CardPlaylistDataonKeyDown: PropTypes.any,
CardPlaylistDataonChange: PropTypes.any,
CardPlaylistDataondelay: PropTypes.any
}
export default CardPlaylistImgCont;