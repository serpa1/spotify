import React, { Suspense } from 'react';
import PropTypes from 'prop-types';
import * as Contexts  from 'Contexts';
import { OverlaySwapContext } from 'Components/OverlaySwapProvider';
import CorebooksAnimation from 'Components/CorebooksAnimation';
import CorebooksAnimationSingle from 'Components/CorebooksAnimationSingle';

import * as UtilsScripts  from 'utils/utils';
import { Route, useHistory, useParams } from "react-router-dom";
import SingletoneNavigation from 'Class/SingletoneNavigation';

import LoadingComponent from "Components/Loading";


import { Transition } from "react-transition-group";
import { CSSTransition } from "react-transition-group";
import HFive from 'Components/HFive'
import { useAppContext, useSessionContext } from 'context/AppContext';
import './MenuSearchPropertyoneDefault.css'





const PropertyoneDefault = (props)=>{
    const [state,dispatch] = useAppContext();
    const [, setNameSelectedComponent] = React.useContext(OverlaySwapContext);
    
    const nodeRef = React.useRef(null);
    const history = useHistory();
    let RouterContext = useParams();
    
    
    
    
    
    
    const [globalStateForm, dispatchForm] = React.useState({});
    
    const [ _in, setIn ] = React.useState(false)
    const [ transaction, setTransaction ] = React.useState({}) 
    const [ animation, setAnimation ] = React.useState('')

    
    
    
    
    
    
    React.useEffect(()=>{
        
        
        
    },[]);
    
    
    return (
        <>
  <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['propertyonedefault']?.animationClass || {}}>

    <div id="id_onesixzero_fourtwoothreefive" ref={nodeRef} className={` ${ props.onClick ? 'cursor' : '' } propertyonedefault C_onesixzero_fourtwoothreefive ${ props.cssClass } ${ transaction['propertyonedefault']?.type ? transaction['propertyonedefault']?.type.toLowerCase() : '' }`} style={ { ...{ ...{}, transitionDuration: transaction['propertyonedefault']?.duration, transitionTimingFunction: transaction['propertyonedefault']?.timingFunction }, ...props.style }} onClick={ props.MenuSearchonClick } onMouseEnter={ props.MenuSearchonMouseEnter } onMouseOver={ props.MenuSearchonMouseOver } onKeyPress={ props.MenuSearchonKeyPress } onDrag={ props.MenuSearchonDrag } onMouseLeave={ props.MenuSearchonMouseLeave } onMouseUp={ props.MenuSearchonMouseUp } onMouseDown={ props.MenuSearchonMouseDown } onKeyDown={ props.MenuSearchonKeyDown } onChange={ props.MenuSearchonChange } ondelay={ props.MenuSearchondelay }>
      {
      props.children ?
      props.children :
      <>
        <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['frame']?.animationClass || {}}>

          <div id="id_fourthreeeight_threenigthseventhree" className={` frame frame ${ props.onClick ? 'cursor' : '' } ${ transaction['frame']?.type ? transaction['frame']?.type.toLowerCase() : '' }`} style={ { ...{}, ...props.FrameStyle , transitionDuration: transaction['frame']?.duration, transitionTimingFunction: transaction['frame']?.timingFunction } } onClick={ props.FrameonClick } onMouseEnter={ props.FrameonMouseEnter } onMouseOver={ props.FrameonMouseOver } onKeyPress={ props.FrameonKeyPress } onDrag={ props.FrameonDrag } onMouseLeave={ props.FrameonMouseLeave } onMouseUp={ props.FrameonMouseUp } onMouseDown={ props.FrameonMouseDown } onKeyDown={ props.FrameonKeyDown } onChange={ props.FrameonChange } ondelay={ props.Frameondelay }>
            <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['vector']?.animationClass || {}}>
              <svg id="id_fourthreeeight_threenigthsevenfour" className="vector  " style={{}} onClick={ props.VectoronClick } onMouseEnter={ props.VectoronMouseEnter } onMouseOver={ props.VectoronMouseOver } onKeyPress={ props.VectoronKeyPress } onDrag={ props.VectoronDrag } onMouseLeave={ props.VectoronMouseLeave } onMouseUp={ props.VectoronMouseUp } onMouseDown={ props.VectoronMouseDown } onKeyDown={ props.VectoronKeyDown } onChange={ props.VectoronChange } ondelay={ props.Vectorondelay } width="21.38623046875" height="21.1593017578125">
                <path d="M9.407 0.00100005C4.227 0.00100005 0 4.141 0 9.28C0 14.419 4.226 18.559 9.407 18.559C11.641 18.559 13.697 17.789 15.314 16.501L19.667 20.854C19.7592 20.9495 19.8696 21.0257 19.9916 21.0781C20.1136 21.1305 20.2448 21.1581 20.3776 21.1593C20.5104 21.1604 20.6421 21.1351 20.765 21.0848C20.8878 21.0345 20.9995 20.9603 21.0934 20.8664C21.1873 20.7725 21.2615 20.6609 21.3118 20.538C21.3621 20.4151 21.3874 20.2834 21.3863 20.1506C21.3851 20.0178 21.3575 19.8866 21.3051 19.7646C21.2527 19.6426 21.1765 19.5322 21.081 19.44L16.737 15.096C18.0819 13.4563 18.816 11.4007 18.814 9.28C18.814 4.14 14.588 0 9.407 0L9.407 0.00100005ZM2 9.28C2 5.274 5.302 2 9.407 2C13.512 2 16.814 5.274 16.814 9.28C16.814 13.286 13.512 16.559 9.407 16.559C5.302 16.559 2 13.287 2 9.28Z" />
              </svg>
            </CSSTransition>
          </div>

        </CSSTransition>
        <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['hfive']?.animationClass || {}}>
          <HFive { ...{ ...props, style:false } } HfiveText0={ props.HfiveText0 || "Buscar" } HfiveText0={ props.HfiveText0 || "Buscar" } HfiveTextStyle={{"fontSize":"0.875rem"}} cssClass={"C_onesixzero_fourtwoothreeseven "}  />
    </CSSTransition >
            
            </>
        }
        </div>
    
    </CSSTransition >
            </>
        
    ) 
}

PropertyoneDefault.propTypes = {
    style: PropTypes.any,
HfiveText0: PropTypes.any,
MenuSearchonClick: PropTypes.any,
MenuSearchonMouseEnter: PropTypes.any,
MenuSearchonMouseOver: PropTypes.any,
MenuSearchonKeyPress: PropTypes.any,
MenuSearchonDrag: PropTypes.any,
MenuSearchonMouseLeave: PropTypes.any,
MenuSearchonMouseUp: PropTypes.any,
MenuSearchonMouseDown: PropTypes.any,
MenuSearchonKeyDown: PropTypes.any,
MenuSearchonChange: PropTypes.any,
MenuSearchondelay: PropTypes.any,
FrameonClick: PropTypes.any,
FrameonMouseEnter: PropTypes.any,
FrameonMouseOver: PropTypes.any,
FrameonKeyPress: PropTypes.any,
FrameonDrag: PropTypes.any,
FrameonMouseLeave: PropTypes.any,
FrameonMouseUp: PropTypes.any,
FrameonMouseDown: PropTypes.any,
FrameonKeyDown: PropTypes.any,
FrameonChange: PropTypes.any,
Frameondelay: PropTypes.any,
VectoronClick: PropTypes.any,
VectoronMouseEnter: PropTypes.any,
VectoronMouseOver: PropTypes.any,
VectoronKeyPress: PropTypes.any,
VectoronDrag: PropTypes.any,
VectoronMouseLeave: PropTypes.any,
VectoronMouseUp: PropTypes.any,
VectoronMouseDown: PropTypes.any,
VectoronKeyDown: PropTypes.any,
VectoronChange: PropTypes.any,
Vectorondelay: PropTypes.any
}
export default PropertyoneDefault;