import React, { Suspense } from 'react';
import PropTypes from 'prop-types';
import * as Contexts  from 'Contexts';
import { OverlaySwapContext } from 'Components/OverlaySwapProvider';
import CorebooksAnimation from 'Components/CorebooksAnimation';
import CorebooksAnimationSingle from 'Components/CorebooksAnimationSingle';

import * as UtilsScripts  from 'utils/utils';
import { Route, useHistory, useParams } from "react-router-dom";
import SingletoneNavigation from 'Class/SingletoneNavigation';

import LoadingComponent from "Components/Loading";


import { Transition } from "react-transition-group";
import { CSSTransition } from "react-transition-group";
import { useAppContext, useSessionContext } from 'context/AppContext';
import './Framefourzeroseven.css'





const Framefourzeroseven = (props)=>{
    const [state,dispatch] = useAppContext();
    const [, setNameSelectedComponent] = React.useContext(OverlaySwapContext);
    
    const nodeRef = React.useRef(null);
    const history = useHistory();
    let RouterContext = useParams();
    
    
    
    
    
    
    const [globalStateForm, dispatchForm] = React.useState({});
    
    const [ _in, setIn ] = React.useState(false)
    const [ transaction, setTransaction ] = React.useState({}) 
    const [ animation, setAnimation ] = React.useState('')

    
    
    
    
    
    
    React.useEffect(()=>{
        
        
        
    },[]);
    
    
    return (
        <>
  <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['framefourzeroseven']?.animationClass || {}}>

    <div id="id_fivefivenigth_twoozerofourzero" ref={nodeRef} className={` ${ props.onClick ? 'cursor' : '' } framefourzeroseven ${ props.cssClass } ${ transaction['framefourzeroseven']?.type ? transaction['framefourzeroseven']?.type.toLowerCase() : '' }`} style={ { ...{ ...{}, transitionDuration: transaction['framefourzeroseven']?.duration, transitionTimingFunction: transaction['framefourzeroseven']?.timingFunction }, ...props.style }} onClick={ props.FramefourzerosevenonClick } onMouseEnter={ props.FramefourzerosevenonMouseEnter } onMouseOver={ props.FramefourzerosevenonMouseOver } onKeyPress={ props.FramefourzerosevenonKeyPress } onDrag={ props.FramefourzerosevenonDrag } onMouseLeave={ props.FramefourzerosevenonMouseLeave } onMouseUp={ props.FramefourzerosevenonMouseUp } onMouseDown={ props.FramefourzerosevenonMouseDown } onKeyDown={ props.FramefourzerosevenonKeyDown } onChange={ props.FramefourzerosevenonChange } ondelay={ props.Framefourzerosevenondelay }>
      {
      props.children ?
      props.children :
      <>
        <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['imagethree']?.animationClass || {}}>
          <img id="id_fivefivenigth_twoozerofourone" className={` rectangle imagethree ${ props.onClick ? 'cursor' : '' } ${ transaction['imagethree']?.type ? transaction['imagethree']?.type.toLowerCase() : '' }`} style={{ ...{},  ...props.ImagethreeStyle , transitionDuration: transaction['imagethree']?.duration, transitionTimingFunction: transaction['imagethree']?.timingFunction }} onClick={ props.ImagethreeonClick } onMouseEnter={ props.ImagethreeonMouseEnter } onMouseOver={ props.ImagethreeonMouseOver } onKeyPress={ props.ImagethreeonKeyPress } onDrag={ props.ImagethreeonDrag } onMouseLeave={ props.ImagethreeonMouseLeave } onMouseUp={ props.ImagethreeonMouseUp } onMouseDown={ props.ImagethreeonMouseDown } onKeyDown={ props.ImagethreeonKeyDown } onChange={ props.ImagethreeonChange } ondelay={ props.Imagethreeondelay } src={props.Imagethree0 || "https://cdn.uncodie.com/tACQBrKCV97e0EqPE1XNLJ/046b6e86a4953be2d4d1c21d0bdfe829dd491620.png" } />
        </CSSTransition>

      </>
      }
    </div>

  </CSSTransition>
</>
    ) 
}

Framefourzeroseven.propTypes = {
    style: PropTypes.any,
Imagethree0: PropTypes.any,
FramefourzerosevenonClick: PropTypes.any,
FramefourzerosevenonMouseEnter: PropTypes.any,
FramefourzerosevenonMouseOver: PropTypes.any,
FramefourzerosevenonKeyPress: PropTypes.any,
FramefourzerosevenonDrag: PropTypes.any,
FramefourzerosevenonMouseLeave: PropTypes.any,
FramefourzerosevenonMouseUp: PropTypes.any,
FramefourzerosevenonMouseDown: PropTypes.any,
FramefourzerosevenonKeyDown: PropTypes.any,
FramefourzerosevenonChange: PropTypes.any,
Framefourzerosevenondelay: PropTypes.any,
ImagethreeonClick: PropTypes.any,
ImagethreeonMouseEnter: PropTypes.any,
ImagethreeonMouseOver: PropTypes.any,
ImagethreeonKeyPress: PropTypes.any,
ImagethreeonDrag: PropTypes.any,
ImagethreeonMouseLeave: PropTypes.any,
ImagethreeonMouseUp: PropTypes.any,
ImagethreeonMouseDown: PropTypes.any,
ImagethreeonKeyDown: PropTypes.any,
ImagethreeonChange: PropTypes.any,
Imagethreeondelay: PropTypes.any
}
export default Framefourzeroseven;