import React, { Suspense } from 'react';
import PropTypes from 'prop-types';
import * as Contexts  from 'Contexts';
import { OverlaySwapContext } from 'Components/OverlaySwapProvider';
import CorebooksAnimation from 'Components/CorebooksAnimation';
import CorebooksAnimationSingle from 'Components/CorebooksAnimationSingle';

import * as UtilsScripts  from 'utils/utils';
import { Route, useHistory, useParams } from "react-router-dom";
import SingletoneNavigation from 'Class/SingletoneNavigation';

import LoadingComponent from "Components/Loading";


import { Transition } from "react-transition-group";
import { CSSTransition } from "react-transition-group";
import { useAppContext, useSessionContext } from 'context/AppContext';
import './MenuAnvorguesaBibliotecaMusicPropertyoneVarianttwoo.css'





const PropertyoneVarianttwoo = (props)=>{
    const [state,dispatch] = useAppContext();
    const [, setNameSelectedComponent] = React.useContext(OverlaySwapContext);
    
    const nodeRef = React.useRef(null);
    const history = useHistory();
    let RouterContext = useParams();
    
    
    
    
    
    
    const [globalStateForm, dispatchForm] = React.useState({});
    
    const [ _in, setIn ] = React.useState(false)
    const [ transaction, setTransaction ] = React.useState({}) 
    const [ animation, setAnimation ] = React.useState('')

    
    
    
    
    
    
    React.useEffect(()=>{
        
        
        
    },[]);
    
    
    return (
        <>
  <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['propertyonevarianttwoo']?.animationClass || {}}>

    <div id="id_fourthreeeight_oneseveneightseven" ref={nodeRef} className={` cursor ${ props.onClick ? 'cursor' : '' } propertyonevarianttwoo C_fourthreeeight_oneseveneightseven ${ props.cssClass } ${ transaction['propertyonevarianttwoo']?.type ? transaction['propertyonevarianttwoo']?.type.toLowerCase() : '' }`} style={ { ...{ ...{}, transitionDuration: transaction['propertyonevarianttwoo']?.duration, transitionTimingFunction: transaction['propertyonevarianttwoo']?.timingFunction }, ...props.style }} onClick={ props.MenuAnvorguesaBibliotecaMusiconClick } onMouseEnter={ props.MenuAnvorguesaBibliotecaMusiconMouseEnter } onMouseOver={ props.MenuAnvorguesaBibliotecaMusiconMouseOver } onKeyPress={ props.MenuAnvorguesaBibliotecaMusiconKeyPress } onDrag={ props.MenuAnvorguesaBibliotecaMusiconDrag } onMouseLeave={ props.MenuAnvorguesaBibliotecaMusiconMouseLeave || function(e){ setTransaction({ }); setvariant('Property 1=Default'); }} onMouseUp={ props.MenuAnvorguesaBibliotecaMusiconMouseUp } onMouseDown={ props.MenuAnvorguesaBibliotecaMusiconMouseDown } onKeyDown={ props.MenuAnvorguesaBibliotecaMusiconKeyDown } onChange={ props.MenuAnvorguesaBibliotecaMusiconChange } ondelay={ props.MenuAnvorguesaBibliotecaMusicondelay }>
      {
      props.children ?
      props.children :
      <>
        <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['rectanglesix']?.animationClass || {}}>
          <img id="id_fourthreeeight_oneseveneighteight" className={` rectangle rectanglesix ${ props.onClick ? 'cursor' : '' } ${ transaction['rectanglesix']?.type ? transaction['rectanglesix']?.type.toLowerCase() : '' }`} style={{ ...{},  ...props.RectanglesixStyle , transitionDuration: transaction['rectanglesix']?.duration, transitionTimingFunction: transaction['rectanglesix']?.timingFunction }} onClick={ props.RectanglesixonClick } onMouseEnter={ props.RectanglesixonMouseEnter } onMouseOver={ props.RectanglesixonMouseOver } onKeyPress={ props.RectanglesixonKeyPress } onDrag={ props.RectanglesixonDrag } onMouseLeave={ props.RectanglesixonMouseLeave } onMouseUp={ props.RectanglesixonMouseUp } onMouseDown={ props.RectanglesixonMouseDown } onKeyDown={ props.RectanglesixonKeyDown } onChange={ props.RectanglesixonChange } ondelay={ props.Rectanglesixondelay } src={props.Rectanglesix0 || "https://cdn.uncodie.com/tACQBrKCV97e0EqPE1XNLJ/63848221d531c86045c89bdbae1f46b6b56ce051.png" } />
        </CSSTransition>
        <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['framethreeeightsix']?.animationClass || {}}>

          <div id="id_fourthreeeight_oneeightoneeight" className={` frame framethreeeightsix ${ props.onClick ? 'cursor' : '' } ${ transaction['framethreeeightsix']?.type ? transaction['framethreeeightsix']?.type.toLowerCase() : '' }`} style={ { ...{}, ...props.FramethreeeightsixStyle , transitionDuration: transaction['framethreeeightsix']?.duration, transitionTimingFunction: transaction['framethreeeightsix']?.timingFunction } } onClick={ props.FramethreeeightsixonClick } onMouseEnter={ props.FramethreeeightsixonMouseEnter } onMouseOver={ props.FramethreeeightsixonMouseOver } onKeyPress={ props.FramethreeeightsixonKeyPress } onDrag={ props.FramethreeeightsixonDrag } onMouseLeave={ props.FramethreeeightsixonMouseLeave } onMouseUp={ props.FramethreeeightsixonMouseUp } onMouseDown={ props.FramethreeeightsixonMouseDown } onKeyDown={ props.FramethreeeightsixonKeyDown } onChange={ props.FramethreeeightsixonChange } ondelay={ props.Framethreeeightsixondelay }>
            <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['textonesixtxt']?.animationClass || {}}>

              <span id="id_fourthreeeight_oneseveneightnigth"  className={` text textonesixtxt    ${ props.onClick ? 'cursor' : ''}  ${ transaction['textonesixtxt']?.type ? transaction['textonesixtxt']?.type.toLowerCase() : '' }`} style={{ ...{},  ...props.TextonesixTxtStyle , transitionDuration: transaction['textonesixtxt']?.duration, transitionTimingFunction: transaction['textonesixtxt']?.timingFunction }} onClick={ props.TextonesixTxtonClick } onMouseEnter={ props.TextonesixTxtonMouseEnter } onMouseOver={ props.TextonesixTxtonMouseOver } onKeyPress={ props.TextonesixTxtonKeyPress } onDrag={ props.TextonesixTxtonDrag } onMouseLeave={ props.TextonesixTxtonMouseLeave } onMouseUp={ props.TextonesixTxtonMouseUp } onMouseDown={ props.TextonesixTxtonMouseDown } onKeyDown={ props.TextonesixTxtonKeyDown } onChange={ props.TextonesixTxtonChange } ondelay={ props.TextonesixTxtondelay } >{props.TextonesixTxt0 || `Crear nueva playlist`}</span>

            </CSSTransition>
          </div>

        </CSSTransition>

      </>
      }
    </div>

  </CSSTransition>
</>
    ) 
}

PropertyoneVarianttwoo.propTypes = {
    style: PropTypes.any,
Rectanglesix0: PropTypes.any,
TextonesixTxt0: PropTypes.any,
MenuAnvorguesaBibliotecaMusiconClick: PropTypes.any,
MenuAnvorguesaBibliotecaMusiconMouseEnter: PropTypes.any,
MenuAnvorguesaBibliotecaMusiconMouseOver: PropTypes.any,
MenuAnvorguesaBibliotecaMusiconKeyPress: PropTypes.any,
MenuAnvorguesaBibliotecaMusiconDrag: PropTypes.any,
MenuAnvorguesaBibliotecaMusiconMouseLeave: PropTypes.any,
MenuAnvorguesaBibliotecaMusiconMouseUp: PropTypes.any,
MenuAnvorguesaBibliotecaMusiconMouseDown: PropTypes.any,
MenuAnvorguesaBibliotecaMusiconKeyDown: PropTypes.any,
MenuAnvorguesaBibliotecaMusiconChange: PropTypes.any,
MenuAnvorguesaBibliotecaMusicondelay: PropTypes.any,
RectanglesixonClick: PropTypes.any,
RectanglesixonMouseEnter: PropTypes.any,
RectanglesixonMouseOver: PropTypes.any,
RectanglesixonKeyPress: PropTypes.any,
RectanglesixonDrag: PropTypes.any,
RectanglesixonMouseLeave: PropTypes.any,
RectanglesixonMouseUp: PropTypes.any,
RectanglesixonMouseDown: PropTypes.any,
RectanglesixonKeyDown: PropTypes.any,
RectanglesixonChange: PropTypes.any,
Rectanglesixondelay: PropTypes.any,
FramethreeeightsixonClick: PropTypes.any,
FramethreeeightsixonMouseEnter: PropTypes.any,
FramethreeeightsixonMouseOver: PropTypes.any,
FramethreeeightsixonKeyPress: PropTypes.any,
FramethreeeightsixonDrag: PropTypes.any,
FramethreeeightsixonMouseLeave: PropTypes.any,
FramethreeeightsixonMouseUp: PropTypes.any,
FramethreeeightsixonMouseDown: PropTypes.any,
FramethreeeightsixonKeyDown: PropTypes.any,
FramethreeeightsixonChange: PropTypes.any,
Framethreeeightsixondelay: PropTypes.any,
TextonesixTxtonClick: PropTypes.any,
TextonesixTxtonMouseEnter: PropTypes.any,
TextonesixTxtonMouseOver: PropTypes.any,
TextonesixTxtonKeyPress: PropTypes.any,
TextonesixTxtonDrag: PropTypes.any,
TextonesixTxtonMouseLeave: PropTypes.any,
TextonesixTxtonMouseUp: PropTypes.any,
TextonesixTxtonMouseDown: PropTypes.any,
TextonesixTxtonKeyDown: PropTypes.any,
TextonesixTxtonChange: PropTypes.any,
TextonesixTxtondelay: PropTypes.any
}
export default PropertyoneVarianttwoo;