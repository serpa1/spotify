import React, { Suspense } from 'react';
import PropTypes from 'prop-types';
import * as Contexts  from 'Contexts';
import { OverlaySwapContext } from 'Components/OverlaySwapProvider';
import CorebooksAnimation from 'Components/CorebooksAnimation';
import CorebooksAnimationSingle from 'Components/CorebooksAnimationSingle';

import * as UtilsScripts  from 'utils/utils';
import { Route, useHistory, useParams } from "react-router-dom";
import SingletoneNavigation from 'Class/SingletoneNavigation';

import LoadingComponent from "Components/Loading";


import { Transition } from "react-transition-group";
import { CSSTransition } from "react-transition-group";
import { useAppContext, useSessionContext } from 'context/AppContext';
import './MenuAnvorguesaBibliotecaCarpetaPropertyoneDefault.css'





const PropertyoneDefault = (props)=>{
    const [state,dispatch] = useAppContext();
    const [, setNameSelectedComponent] = React.useContext(OverlaySwapContext);
    
    const nodeRef = React.useRef(null);
    const history = useHistory();
    let RouterContext = useParams();
    
    
    
    
    
    
    const [globalStateForm, dispatchForm] = React.useState({});
    
    const [ _in, setIn ] = React.useState(false)
    const [ transaction, setTransaction ] = React.useState({}) 
    const [ animation, setAnimation ] = React.useState('')

    
    
    
    
    
    
    React.useEffect(()=>{
        
        
        
    },[]);
    
    
    return (
        <>
  <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['propertyonedefault']?.animationClass || {}}>

    <div id="id_fourthreeeight_oneeightthreezero" ref={nodeRef} className={` cursor ${ props.onClick ? 'cursor' : '' } propertyonedefault C_fourthreeeight_oneeightthreezero ${ props.cssClass } ${ transaction['propertyonedefault']?.type ? transaction['propertyonedefault']?.type.toLowerCase() : '' }`} style={ { ...{ ...{}, transitionDuration: transaction['propertyonedefault']?.duration, transitionTimingFunction: transaction['propertyonedefault']?.timingFunction }, ...props.style }} onClick={ props.MenuAnvorguesaBibliotecaCarpetaonClick } onMouseEnter={ props.MenuAnvorguesaBibliotecaCarpetaonMouseEnter } onMouseOver={ props.MenuAnvorguesaBibliotecaCarpetaonMouseOver || function(e){ setTransaction({ }); setvariant('Property 1=Variant2'); }} onKeyPress={ props.MenuAnvorguesaBibliotecaCarpetaonKeyPress } onDrag={ props.MenuAnvorguesaBibliotecaCarpetaonDrag } onMouseLeave={ props.MenuAnvorguesaBibliotecaCarpetaonMouseLeave } onMouseUp={ props.MenuAnvorguesaBibliotecaCarpetaonMouseUp } onMouseDown={ props.MenuAnvorguesaBibliotecaCarpetaonMouseDown } onKeyDown={ props.MenuAnvorguesaBibliotecaCarpetaonKeyDown } onChange={ props.MenuAnvorguesaBibliotecaCarpetaonChange } ondelay={ props.MenuAnvorguesaBibliotecaCarpetaondelay }>
      {
      props.children ?
      props.children :
      <>
        <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['rectanglesix']?.animationClass || {}}>
          <img id="id_fourthreeeight_oneeighttwooseven" className={` rectangle rectanglesix ${ props.onClick ? 'cursor' : '' } ${ transaction['rectanglesix']?.type ? transaction['rectanglesix']?.type.toLowerCase() : '' }`} style={{ ...{},  ...props.RectanglesixStyle , transitionDuration: transaction['rectanglesix']?.duration, transitionTimingFunction: transaction['rectanglesix']?.timingFunction }} onClick={ props.RectanglesixonClick } onMouseEnter={ props.RectanglesixonMouseEnter } onMouseOver={ props.RectanglesixonMouseOver } onKeyPress={ props.RectanglesixonKeyPress } onDrag={ props.RectanglesixonDrag } onMouseLeave={ props.RectanglesixonMouseLeave } onMouseUp={ props.RectanglesixonMouseUp } onMouseDown={ props.RectanglesixonMouseDown } onKeyDown={ props.RectanglesixonKeyDown } onChange={ props.RectanglesixonChange } ondelay={ props.Rectanglesixondelay } src={props.Rectanglesix0 || "https://cdn.uncodie.com/tACQBrKCV97e0EqPE1XNLJ/e0cd5c9c57df125ecb91702eb1fac5e217907a67.png" } />
        </CSSTransition>
        <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['framethreeeightfive']?.animationClass || {}}>

          <div id="id_fourthreeeight_oneeighttwooeight" className={` frame framethreeeightfive ${ props.onClick ? 'cursor' : '' } ${ transaction['framethreeeightfive']?.type ? transaction['framethreeeightfive']?.type.toLowerCase() : '' }`} style={ { ...{}, ...props.FramethreeeightfiveStyle , transitionDuration: transaction['framethreeeightfive']?.duration, transitionTimingFunction: transaction['framethreeeightfive']?.timingFunction } } onClick={ props.FramethreeeightfiveonClick } onMouseEnter={ props.FramethreeeightfiveonMouseEnter } onMouseOver={ props.FramethreeeightfiveonMouseOver } onKeyPress={ props.FramethreeeightfiveonKeyPress } onDrag={ props.FramethreeeightfiveonDrag } onMouseLeave={ props.FramethreeeightfiveonMouseLeave } onMouseUp={ props.FramethreeeightfiveonMouseUp } onMouseDown={ props.FramethreeeightfiveonMouseDown } onKeyDown={ props.FramethreeeightfiveonKeyDown } onChange={ props.FramethreeeightfiveonChange } ondelay={ props.Framethreeeightfiveondelay }>
            <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['textonesixtxt']?.animationClass || {}}>

              <span id="id_fourthreeeight_oneeighttwoonigth"  className={` text textonesixtxt    ${ props.onClick ? 'cursor' : ''}  ${ transaction['textonesixtxt']?.type ? transaction['textonesixtxt']?.type.toLowerCase() : '' }`} style={{ ...{},  ...props.TextonesixTxtStyle , transitionDuration: transaction['textonesixtxt']?.duration, transitionTimingFunction: transaction['textonesixtxt']?.timingFunction }} onClick={ props.TextonesixTxtonClick } onMouseEnter={ props.TextonesixTxtonMouseEnter } onMouseOver={ props.TextonesixTxtonMouseOver } onKeyPress={ props.TextonesixTxtonKeyPress } onDrag={ props.TextonesixTxtonDrag } onMouseLeave={ props.TextonesixTxtonMouseLeave } onMouseUp={ props.TextonesixTxtonMouseUp } onMouseDown={ props.TextonesixTxtonMouseDown } onKeyDown={ props.TextonesixTxtonKeyDown } onChange={ props.TextonesixTxtonChange } ondelay={ props.TextonesixTxtondelay } >{props.TextonesixTxt0 || `Crear una carpeta de playlists`}</span>

            </CSSTransition>
          </div>

        </CSSTransition>

      </>
      }
    </div>

  </CSSTransition>
</>
    ) 
}

PropertyoneDefault.propTypes = {
    style: PropTypes.any,
Rectanglesix0: PropTypes.any,
TextonesixTxt0: PropTypes.any,
MenuAnvorguesaBibliotecaCarpetaonClick: PropTypes.any,
MenuAnvorguesaBibliotecaCarpetaonMouseEnter: PropTypes.any,
MenuAnvorguesaBibliotecaCarpetaonMouseOver: PropTypes.any,
MenuAnvorguesaBibliotecaCarpetaonKeyPress: PropTypes.any,
MenuAnvorguesaBibliotecaCarpetaonDrag: PropTypes.any,
MenuAnvorguesaBibliotecaCarpetaonMouseLeave: PropTypes.any,
MenuAnvorguesaBibliotecaCarpetaonMouseUp: PropTypes.any,
MenuAnvorguesaBibliotecaCarpetaonMouseDown: PropTypes.any,
MenuAnvorguesaBibliotecaCarpetaonKeyDown: PropTypes.any,
MenuAnvorguesaBibliotecaCarpetaonChange: PropTypes.any,
MenuAnvorguesaBibliotecaCarpetaondelay: PropTypes.any,
RectanglesixonClick: PropTypes.any,
RectanglesixonMouseEnter: PropTypes.any,
RectanglesixonMouseOver: PropTypes.any,
RectanglesixonKeyPress: PropTypes.any,
RectanglesixonDrag: PropTypes.any,
RectanglesixonMouseLeave: PropTypes.any,
RectanglesixonMouseUp: PropTypes.any,
RectanglesixonMouseDown: PropTypes.any,
RectanglesixonKeyDown: PropTypes.any,
RectanglesixonChange: PropTypes.any,
Rectanglesixondelay: PropTypes.any,
FramethreeeightfiveonClick: PropTypes.any,
FramethreeeightfiveonMouseEnter: PropTypes.any,
FramethreeeightfiveonMouseOver: PropTypes.any,
FramethreeeightfiveonKeyPress: PropTypes.any,
FramethreeeightfiveonDrag: PropTypes.any,
FramethreeeightfiveonMouseLeave: PropTypes.any,
FramethreeeightfiveonMouseUp: PropTypes.any,
FramethreeeightfiveonMouseDown: PropTypes.any,
FramethreeeightfiveonKeyDown: PropTypes.any,
FramethreeeightfiveonChange: PropTypes.any,
Framethreeeightfiveondelay: PropTypes.any,
TextonesixTxtonClick: PropTypes.any,
TextonesixTxtonMouseEnter: PropTypes.any,
TextonesixTxtonMouseOver: PropTypes.any,
TextonesixTxtonKeyPress: PropTypes.any,
TextonesixTxtonDrag: PropTypes.any,
TextonesixTxtonMouseLeave: PropTypes.any,
TextonesixTxtonMouseUp: PropTypes.any,
TextonesixTxtonMouseDown: PropTypes.any,
TextonesixTxtonKeyDown: PropTypes.any,
TextonesixTxtonChange: PropTypes.any,
TextonesixTxtondelay: PropTypes.any
}
export default PropertyoneDefault;