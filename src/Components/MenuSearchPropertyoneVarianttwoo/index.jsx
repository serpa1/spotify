import React, { Suspense } from 'react';
import PropTypes from 'prop-types';
import * as Contexts  from 'Contexts';
import { OverlaySwapContext } from 'Components/OverlaySwapProvider';
import CorebooksAnimation from 'Components/CorebooksAnimation';
import CorebooksAnimationSingle from 'Components/CorebooksAnimationSingle';

import * as UtilsScripts  from 'utils/utils';
import { Route, useHistory, useParams } from "react-router-dom";
import SingletoneNavigation from 'Class/SingletoneNavigation';

import LoadingComponent from "Components/Loading";


import { Transition } from "react-transition-group";
import { CSSTransition } from "react-transition-group";
import HFive from 'Components/HFive'
import { useAppContext, useSessionContext } from 'context/AppContext';
import './MenuSearchPropertyoneVarianttwoo.css'





const PropertyoneVarianttwoo = (props)=>{
    const [state,dispatch] = useAppContext();
    const [, setNameSelectedComponent] = React.useContext(OverlaySwapContext);
    
    const nodeRef = React.useRef(null);
    const history = useHistory();
    let RouterContext = useParams();
    
    
    
    
    
    
    const [globalStateForm, dispatchForm] = React.useState({});
    
    const [ _in, setIn ] = React.useState(false)
    const [ transaction, setTransaction ] = React.useState({}) 
    const [ animation, setAnimation ] = React.useState('')

    
    
    
    
    
    
    React.useEffect(()=>{
        
        
        
    },[]);
    
    
    return (
        <>
  <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['propertyonevarianttwoo']?.animationClass || {}}>

    <div id="id_onesixzero_fourtwoothreeeight" ref={nodeRef} className={` ${ props.onClick ? 'cursor' : '' } propertyonevarianttwoo C_onesixzero_fourtwoothreeeight ${ props.cssClass } ${ transaction['propertyonevarianttwoo']?.type ? transaction['propertyonevarianttwoo']?.type.toLowerCase() : '' }`} style={ { ...{ ...{}, transitionDuration: transaction['propertyonevarianttwoo']?.duration, transitionTimingFunction: transaction['propertyonevarianttwoo']?.timingFunction }, ...props.style }} onClick={ props.MenuSearchonClick } onMouseEnter={ props.MenuSearchonMouseEnter } onMouseOver={ props.MenuSearchonMouseOver } onKeyPress={ props.MenuSearchonKeyPress } onDrag={ props.MenuSearchonDrag } onMouseLeave={ props.MenuSearchonMouseLeave } onMouseUp={ props.MenuSearchonMouseUp } onMouseDown={ props.MenuSearchonMouseDown } onKeyDown={ props.MenuSearchonKeyDown } onChange={ props.MenuSearchonChange } ondelay={ props.MenuSearchondelay }>
      {
      props.children ?
      props.children :
      <>
        <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['menuhomeicon']?.animationClass || {}}>
          <img id="id_onesixzero_fourtwoothreenigth" className={` rectangle menuhomeicon ${ props.onClick ? 'cursor' : '' } ${ transaction['menuhomeicon']?.type ? transaction['menuhomeicon']?.type.toLowerCase() : '' }`} style={{ ...{},  ...props.MenuHomeIconStyle , transitionDuration: transaction['menuhomeicon']?.duration, transitionTimingFunction: transaction['menuhomeicon']?.timingFunction }} onClick={ props.MenuHomeIcononClick } onMouseEnter={ props.MenuHomeIcononMouseEnter } onMouseOver={ props.MenuHomeIcononMouseOver } onKeyPress={ props.MenuHomeIcononKeyPress } onDrag={ props.MenuHomeIcononDrag } onMouseLeave={ props.MenuHomeIcononMouseLeave } onMouseUp={ props.MenuHomeIcononMouseUp } onMouseDown={ props.MenuHomeIcononMouseDown } onKeyDown={ props.MenuHomeIcononKeyDown } onChange={ props.MenuHomeIcononChange } ondelay={ props.MenuHomeIconondelay } src={props.MenuHomeIcon0 || "https://cdn.uncodie.com/tACQBrKCV97e0EqPE1XNLJ/a886373b476d2f70ba203509853c152b6e17f903.png" } />
        </CSSTransition>
        <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['hfive']?.animationClass || {}}>
          <HFive { ...{ ...props, style:false } } HfiveText0={ props.HfiveText0 || "Buscar" } HfiveTextStyle={{"fontSize":"0.875rem"}} cssClass={"C_onesixzero_fourtwoofourzero "}  />
    </CSSTransition >
            
            </>
        }
        </div>
    
    </CSSTransition >
            </>
        
    ) 
}

PropertyoneVarianttwoo.propTypes = {
    style: PropTypes.any,
MenuHomeIcon0: PropTypes.any,
HfiveText0: PropTypes.any,
MenuSearchonClick: PropTypes.any,
MenuSearchonMouseEnter: PropTypes.any,
MenuSearchonMouseOver: PropTypes.any,
MenuSearchonKeyPress: PropTypes.any,
MenuSearchonDrag: PropTypes.any,
MenuSearchonMouseLeave: PropTypes.any,
MenuSearchonMouseUp: PropTypes.any,
MenuSearchonMouseDown: PropTypes.any,
MenuSearchonKeyDown: PropTypes.any,
MenuSearchonChange: PropTypes.any,
MenuSearchondelay: PropTypes.any,
MenuHomeIcononClick: PropTypes.any,
MenuHomeIcononMouseEnter: PropTypes.any,
MenuHomeIcononMouseOver: PropTypes.any,
MenuHomeIcononKeyPress: PropTypes.any,
MenuHomeIcononDrag: PropTypes.any,
MenuHomeIcononMouseLeave: PropTypes.any,
MenuHomeIcononMouseUp: PropTypes.any,
MenuHomeIcononMouseDown: PropTypes.any,
MenuHomeIcononKeyDown: PropTypes.any,
MenuHomeIcononChange: PropTypes.any,
MenuHomeIconondelay: PropTypes.any
}
export default PropertyoneVarianttwoo;