import React, { Suspense } from 'react';
import PropTypes from 'prop-types';
import * as Contexts  from 'Contexts';
import { OverlaySwapContext } from 'Components/OverlaySwapProvider';
import CorebooksAnimation from 'Components/CorebooksAnimation';
import CorebooksAnimationSingle from 'Components/CorebooksAnimationSingle';

import * as UtilsScripts  from 'utils/utils';
import { Route, useHistory, useParams } from "react-router-dom";
import SingletoneNavigation from 'Class/SingletoneNavigation';

import LoadingComponent from "Components/Loading";


import { Transition } from "react-transition-group";
import { CSSTransition } from "react-transition-group";
import { useAppContext, useSessionContext } from 'context/AppContext';
import './onefourpx.css'





const onefourpx = (props)=>{
    const [state,dispatch] = useAppContext();
    const [, setNameSelectedComponent] = React.useContext(OverlaySwapContext);
    
    const nodeRef = React.useRef(null);
    const history = useHistory();
    let RouterContext = useParams();
    
    
    
    
    
    
    const [globalStateForm, dispatchForm] = React.useState({});
    
    const [ _in, setIn ] = React.useState(false)
    const [ transaction, setTransaction ] = React.useState({}) 
    const [ animation, setAnimation ] = React.useState('')

    
    
    
    
    
    
    React.useEffect(()=>{
        
        
        
    },[]);
    
    
    return (
        <>
  <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['onefourpx']?.animationClass || {}}>

    <div id="id_sixthreetwoo_onesevenseventwoo" ref={nodeRef} className={` ${ props.onClick ? 'cursor' : '' } onefourpx ${ props.cssClass } ${ transaction['onefourpx']?.type ? transaction['onefourpx']?.type.toLowerCase() : '' }`} style={ { ...{ ...{}, transitionDuration: transaction['onefourpx']?.duration, transitionTimingFunction: transaction['onefourpx']?.timingFunction }, ...props.style }} onClick={ props.OnefourpxonClick } onMouseEnter={ props.OnefourpxonMouseEnter } onMouseOver={ props.OnefourpxonMouseOver } onKeyPress={ props.OnefourpxonKeyPress } onDrag={ props.OnefourpxonDrag } onMouseLeave={ props.OnefourpxonMouseLeave } onMouseUp={ props.OnefourpxonMouseUp } onMouseDown={ props.OnefourpxonMouseDown } onKeyDown={ props.OnefourpxonKeyDown } onChange={ props.OnefourpxonChange } ondelay={ props.Onefourpxondelay }>
      {
      props.children ?
      props.children :
      <>
        <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['prefieronorecibirpublicidaddespotify']?.animationClass || {}}>

          <span id="id_sixthreetwoo_onesevensevenone"  className={` text prefieronorecibirpublicidaddespotify    ${ props.onClick ? 'cursor' : ''}  ${ transaction['prefieronorecibirpublicidaddespotify']?.type ? transaction['prefieronorecibirpublicidaddespotify']?.type.toLowerCase() : '' }`} style={{ ...{},  ...props.PrefieronorecibirpublicidaddeSpotifyStyle , transitionDuration: transaction['prefieronorecibirpublicidaddespotify']?.duration, transitionTimingFunction: transaction['prefieronorecibirpublicidaddespotify']?.timingFunction }} onClick={ props.PrefieronorecibirpublicidaddeSpotifyonClick } onMouseEnter={ props.PrefieronorecibirpublicidaddeSpotifyonMouseEnter } onMouseOver={ props.PrefieronorecibirpublicidaddeSpotifyonMouseOver } onKeyPress={ props.PrefieronorecibirpublicidaddeSpotifyonKeyPress } onDrag={ props.PrefieronorecibirpublicidaddeSpotifyonDrag } onMouseLeave={ props.PrefieronorecibirpublicidaddeSpotifyonMouseLeave } onMouseUp={ props.PrefieronorecibirpublicidaddeSpotifyonMouseUp } onMouseDown={ props.PrefieronorecibirpublicidaddeSpotifyonMouseDown } onKeyDown={ props.PrefieronorecibirpublicidaddeSpotifyonKeyDown } onChange={ props.PrefieronorecibirpublicidaddeSpotifyonChange } ondelay={ props.PrefieronorecibirpublicidaddeSpotifyondelay } >{props.PrefieronorecibirpublicidaddeSpotify0 || `Prefiero no recibir publicidad de Spotify`}</span>

        </CSSTransition>

      </>
      }
    </div>

  </CSSTransition>
</>
    ) 
}

onefourpx.propTypes = {
    style: PropTypes.any,
PrefieronorecibirpublicidaddeSpotify0: PropTypes.any,
OnefourpxonClick: PropTypes.any,
OnefourpxonMouseEnter: PropTypes.any,
OnefourpxonMouseOver: PropTypes.any,
OnefourpxonKeyPress: PropTypes.any,
OnefourpxonDrag: PropTypes.any,
OnefourpxonMouseLeave: PropTypes.any,
OnefourpxonMouseUp: PropTypes.any,
OnefourpxonMouseDown: PropTypes.any,
OnefourpxonKeyDown: PropTypes.any,
OnefourpxonChange: PropTypes.any,
Onefourpxondelay: PropTypes.any,
PrefieronorecibirpublicidaddeSpotifyonClick: PropTypes.any,
PrefieronorecibirpublicidaddeSpotifyonMouseEnter: PropTypes.any,
PrefieronorecibirpublicidaddeSpotifyonMouseOver: PropTypes.any,
PrefieronorecibirpublicidaddeSpotifyonKeyPress: PropTypes.any,
PrefieronorecibirpublicidaddeSpotifyonDrag: PropTypes.any,
PrefieronorecibirpublicidaddeSpotifyonMouseLeave: PropTypes.any,
PrefieronorecibirpublicidaddeSpotifyonMouseUp: PropTypes.any,
PrefieronorecibirpublicidaddeSpotifyonMouseDown: PropTypes.any,
PrefieronorecibirpublicidaddeSpotifyonKeyDown: PropTypes.any,
PrefieronorecibirpublicidaddeSpotifyonChange: PropTypes.any,
PrefieronorecibirpublicidaddeSpotifyondelay: PropTypes.any
}
export default onefourpx;