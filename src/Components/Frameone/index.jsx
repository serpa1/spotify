import React, { Suspense } from 'react';
import PropTypes from 'prop-types';
import * as Contexts  from 'Contexts';
import { OverlaySwapContext } from 'Components/OverlaySwapProvider';
import CorebooksAnimation from 'Components/CorebooksAnimation';
import CorebooksAnimationSingle from 'Components/CorebooksAnimationSingle';

import * as UtilsScripts  from 'utils/utils';
import { Route, useHistory, useParams } from "react-router-dom";
import SingletoneNavigation from 'Class/SingletoneNavigation';

import LoadingComponent from "Components/Loading";


import { Transition } from "react-transition-group";
import { CSSTransition } from "react-transition-group";
import { useAppContext, useSessionContext } from 'context/AppContext';
import './Frameone.css'





const Frameone = (props)=>{
    const [state,dispatch] = useAppContext();
    const [, setNameSelectedComponent] = React.useContext(OverlaySwapContext);
    
    const nodeRef = React.useRef(null);
    const history = useHistory();
    let RouterContext = useParams();
    
    
    
    
    
    
    const [globalStateForm, dispatchForm] = React.useState({});
    
    const [ _in, setIn ] = React.useState(false)
    const [ transaction, setTransaction ] = React.useState({}) 
    const [ animation, setAnimation ] = React.useState('')

    
    
    
    
    
    
    React.useEffect(()=>{
        
        
        
    },[]);
    
    
    return (
        <>
  <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['frameone']?.animationClass || {}}>

    <div id="id_onesixnigth_nigthsevenzero" ref={nodeRef} className={` ${ props.onClick ? 'cursor' : '' } frameone ${ props.cssClass } ${ transaction['frameone']?.type ? transaction['frameone']?.type.toLowerCase() : '' }`} style={ { ...{ ...{}, transitionDuration: transaction['frameone']?.duration, transitionTimingFunction: transaction['frameone']?.timingFunction }, ...props.style }} onClick={ props.FrameoneonClick } onMouseEnter={ props.FrameoneonMouseEnter } onMouseOver={ props.FrameoneonMouseOver } onKeyPress={ props.FrameoneonKeyPress } onDrag={ props.FrameoneonDrag } onMouseLeave={ props.FrameoneonMouseLeave } onMouseUp={ props.FrameoneonMouseUp } onMouseDown={ props.FrameoneonMouseDown } onKeyDown={ props.FrameoneonKeyDown } onChange={ props.FrameoneonChange } ondelay={ props.Frameoneondelay }>
      {
      props.children ?
      props.children :
      <>
        <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['vector']?.animationClass || {}}>
          <svg id="id_onesixnigth_nigthsevenone" className="vector  " style={{}} onClick={ props.VectoronClick } onMouseEnter={ props.VectoronMouseEnter } onMouseOver={ props.VectoronMouseOver } onKeyPress={ props.VectoronKeyPress } onDrag={ props.VectoronDrag } onMouseLeave={ props.VectoronMouseLeave } onMouseUp={ props.VectoronMouseUp } onMouseDown={ props.VectoronMouseDown } onKeyDown={ props.VectoronKeyDown } onChange={ props.VectoronChange } ondelay={ props.Vectorondelay } width="16" height="16">
            <path d="M16 8C16 8.21949 15.9128 8.42999 15.7576 8.58519C15.6024 8.74039 15.3919 8.82759 15.1724 8.82759L8.82759 8.82759L8.82759 15.1724C8.82759 15.3919 8.74039 15.6024 8.58519 15.7576C8.42999 15.9128 8.21949 16 8 16C7.78051 16 7.57001 15.9128 7.41481 15.7576C7.25961 15.6024 7.17241 15.3919 7.17241 15.1724L7.17241 8.82759L0.827586 8.82759C0.608097 8.82759 0.397597 8.74039 0.242394 8.58519C0.0871919 8.42999 2.45015e-16 8.21949 0 8C1.22507e-16 7.78051 0.0871919 7.57001 0.242394 7.41481C0.397597 7.25961 0.608097 7.17241 0.827586 7.17241L7.17241 7.17241L7.17241 0.827586C7.17241 0.608097 7.25961 0.397597 7.41481 0.242394C7.57001 0.0871919 7.78051 2.45015e-16 8 0C8.21949 1.22507e-16 8.42999 0.0871919 8.58519 0.242394C8.74039 0.397597 8.82759 0.608097 8.82759 0.827586L8.82759 7.17241L15.1724 7.17241C15.3919 7.17241 15.6024 7.25961 15.7576 7.41481C15.9128 7.57001 16 7.78051 16 8Z" />
          </svg>
        </CSSTransition>

      </>
      }
    </div>

  </CSSTransition>
</>
    ) 
}

Frameone.propTypes = {
    style: PropTypes.any,
FrameoneonClick: PropTypes.any,
FrameoneonMouseEnter: PropTypes.any,
FrameoneonMouseOver: PropTypes.any,
FrameoneonKeyPress: PropTypes.any,
FrameoneonDrag: PropTypes.any,
FrameoneonMouseLeave: PropTypes.any,
FrameoneonMouseUp: PropTypes.any,
FrameoneonMouseDown: PropTypes.any,
FrameoneonKeyDown: PropTypes.any,
FrameoneonChange: PropTypes.any,
Frameoneondelay: PropTypes.any,
VectoronClick: PropTypes.any,
VectoronMouseEnter: PropTypes.any,
VectoronMouseOver: PropTypes.any,
VectoronKeyPress: PropTypes.any,
VectoronDrag: PropTypes.any,
VectoronMouseLeave: PropTypes.any,
VectoronMouseUp: PropTypes.any,
VectoronMouseDown: PropTypes.any,
VectoronKeyDown: PropTypes.any,
VectoronChange: PropTypes.any,
Vectorondelay: PropTypes.any
}
export default Frameone;