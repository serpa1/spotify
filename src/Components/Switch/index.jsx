import React, { Suspense } from 'react';
import PropTypes from 'prop-types';
import * as Contexts  from 'Contexts';
import { OverlaySwapContext } from 'Components/OverlaySwapProvider';
import CorebooksAnimation from 'Components/CorebooksAnimation';
import CorebooksAnimationSingle from 'Components/CorebooksAnimationSingle';

import * as UtilsScripts  from 'utils/utils';
import { Route, useHistory, useParams } from "react-router-dom";
import SingletoneNavigation from 'Class/SingletoneNavigation';

import LoadingComponent from "Components/Loading";


import { Transition } from "react-transition-group";
import { CSSTransition } from "react-transition-group";
import { useAppContext, useSessionContext } from 'context/AppContext';
import './Switch.css'





const Switch = (props)=>{
    const [state,dispatch] = useAppContext();
    const [, setNameSelectedComponent] = React.useContext(OverlaySwapContext);
    
    const nodeRefpropertyonedefault = React.useRef(null);
const nodeRefpropertyonevarianttwoo = React.useRef(null);
    const history = useHistory();
    let RouterContext = useParams();
    
    
    
    
    
    
    const [globalStateForm, dispatchForm] = React.useState({});
    const [ variant, setvariant] = React.useState(props.variant || 'Property 1=Default')
    const [ _in, setIn ] = React.useState(false)
    const [ transaction, setTransaction ] = React.useState({}) 
    const [ animation, setAnimation ] = React.useState('')

    
    
    
    
    
    
    React.useEffect(()=>{
        
        
        
    },[]);
    
    
    React.useEffect(()=>{
        
        setIn(false);
        setTimeout(()=>setIn(true))
           
      },[variant]);
    const switchVariant = (value)=>{
        switch (value) {
            case 'Property 1=Default':{
            return (
            <>

  <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['propertyonedefault']?.animationClass || {}}>

    <div id="id_fourthreefour_twoofiveonenigth" ref={nodeRefpropertyonedefault} className={` ${ props.onClick ? 'cursor' : '' } switchpropertyonedefault C_fourthreefour_twoofiveonenigth ${ props.cssClass } ${ transaction['propertyonedefault']?.type ? transaction['propertyonedefault']?.type.toLowerCase() : '' }`} style={ { ...{ ...{"minHeight":"40px"}, transitionDuration: transaction['propertyonedefault']?.duration, transitionTimingFunction: transaction['propertyonedefault']?.timingFunction }, ...props.style }} onClick={ props.SwitchonClick } onMouseEnter={ props.SwitchonMouseEnter } onMouseOver={ props.SwitchonMouseOver } onKeyPress={ props.SwitchonKeyPress } onDrag={ props.SwitchonDrag } onMouseLeave={ props.SwitchonMouseLeave } onMouseUp={ props.SwitchonMouseUp } onMouseDown={ props.SwitchonMouseDown } onKeyDown={ props.SwitchonKeyDown } onChange={ props.SwitchonChange } ondelay={ props.Switchondelay }>
      {
      props.children ?
      props.children :
      <>
        <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['landingiconprimarybuttom']?.animationClass || {}}>

          <div id="id_fourthreefour_twoofiveonethree" className={` frame switchlandingiconprimarybuttom ${ props.onClick ? 'cursor' : '' } ${ transaction['landingiconprimarybuttom']?.type ? transaction['landingiconprimarybuttom']?.type.toLowerCase() : '' }`} style={ { ...{}, ...props.LandingIconPrimaryButtomStyle , transitionDuration: transaction['landingiconprimarybuttom']?.duration, transitionTimingFunction: transaction['landingiconprimarybuttom']?.timingFunction } } onClick={ props.LandingIconPrimaryButtomonClick } onMouseEnter={ props.LandingIconPrimaryButtomonMouseEnter } onMouseOver={ props.LandingIconPrimaryButtomonMouseOver } onKeyPress={ props.LandingIconPrimaryButtomonKeyPress } onDrag={ props.LandingIconPrimaryButtomonDrag } onMouseLeave={ props.LandingIconPrimaryButtomonMouseLeave } onMouseUp={ props.LandingIconPrimaryButtomonMouseUp } onMouseDown={ props.LandingIconPrimaryButtomonMouseDown } onKeyDown={ props.LandingIconPrimaryButtomonKeyDown } onChange={ props.LandingIconPrimaryButtomonChange } ondelay={ props.LandingIconPrimaryButtomondelay }>

          </div>

        </CSSTransition>

      </>
      }
    </div>

  </CSSTransition>

</>
            )
        }
case 'Property 1=Variant2':{
            return (
            <>

  <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['propertyonevarianttwoo']?.animationClass || {}}>

    <div id="id_fourthreefour_twoofivetwooone" ref={nodeRefpropertyonevarianttwoo} className={` ${ props.onClick ? 'cursor' : '' } switchpropertyonevarianttwoo C_fourthreefour_twoofivetwooone ${ props.cssClass } ${ transaction['propertyonevarianttwoo']?.type ? transaction['propertyonevarianttwoo']?.type.toLowerCase() : '' }`} style={ { ...{ ...{"minHeight":"40px"}, transitionDuration: transaction['propertyonevarianttwoo']?.duration, transitionTimingFunction: transaction['propertyonevarianttwoo']?.timingFunction }, ...props.style }} onClick={ props.SwitchonClick } onMouseEnter={ props.SwitchonMouseEnter } onMouseOver={ props.SwitchonMouseOver } onKeyPress={ props.SwitchonKeyPress } onDrag={ props.SwitchonDrag } onMouseLeave={ props.SwitchonMouseLeave } onMouseUp={ props.SwitchonMouseUp } onMouseDown={ props.SwitchonMouseDown } onKeyDown={ props.SwitchonKeyDown } onChange={ props.SwitchonChange } ondelay={ props.Switchondelay }>
      {
      props.children ?
      props.children :
      <>
        <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['landingiconprimarybuttom']?.animationClass || {}}>

          <div id="id_fourthreefour_twoofivetwootwoo" className={` frame switchlandingiconprimarybuttom ${ props.onClick ? 'cursor' : '' } ${ transaction['landingiconprimarybuttom']?.type ? transaction['landingiconprimarybuttom']?.type.toLowerCase() : '' }`} style={ { ...{}, ...props.LandingIconPrimaryButtomStyle , transitionDuration: transaction['landingiconprimarybuttom']?.duration, transitionTimingFunction: transaction['landingiconprimarybuttom']?.timingFunction } } onClick={ props.LandingIconPrimaryButtomonClick } onMouseEnter={ props.LandingIconPrimaryButtomonMouseEnter } onMouseOver={ props.LandingIconPrimaryButtomonMouseOver } onKeyPress={ props.LandingIconPrimaryButtomonKeyPress } onDrag={ props.LandingIconPrimaryButtomonDrag } onMouseLeave={ props.LandingIconPrimaryButtomonMouseLeave } onMouseUp={ props.LandingIconPrimaryButtomonMouseUp } onMouseDown={ props.LandingIconPrimaryButtomonMouseDown } onKeyDown={ props.LandingIconPrimaryButtomonKeyDown } onChange={ props.LandingIconPrimaryButtomonChange } ondelay={ props.LandingIconPrimaryButtomondelay }>

          </div>

        </CSSTransition>

      </>
      }
    </div>

  </CSSTransition>

</>
            )
        }
        }
    }

    return (
        <>
            {switchVariant(variant)}
        </>
    )
    
}

Switch.propTypes = {
    style: PropTypes.any,
variant: PropTypes.any,
SwitchonClick: PropTypes.any,
SwitchonMouseEnter: PropTypes.any,
SwitchonMouseOver: PropTypes.any,
SwitchonKeyPress: PropTypes.any,
SwitchonDrag: PropTypes.any,
SwitchonMouseLeave: PropTypes.any,
SwitchonMouseUp: PropTypes.any,
SwitchonMouseDown: PropTypes.any,
SwitchonKeyDown: PropTypes.any,
SwitchonChange: PropTypes.any,
Switchondelay: PropTypes.any,
LandingIconPrimaryButtomonClick: PropTypes.any,
LandingIconPrimaryButtomonMouseEnter: PropTypes.any,
LandingIconPrimaryButtomonMouseOver: PropTypes.any,
LandingIconPrimaryButtomonKeyPress: PropTypes.any,
LandingIconPrimaryButtomonDrag: PropTypes.any,
LandingIconPrimaryButtomonMouseLeave: PropTypes.any,
LandingIconPrimaryButtomonMouseUp: PropTypes.any,
LandingIconPrimaryButtomonMouseDown: PropTypes.any,
LandingIconPrimaryButtomonKeyDown: PropTypes.any,
LandingIconPrimaryButtomonChange: PropTypes.any,
LandingIconPrimaryButtomondelay: PropTypes.any
}
export default Switch;