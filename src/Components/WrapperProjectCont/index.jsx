import React, { Suspense } from 'react';
import PropTypes from 'prop-types';
import * as Contexts  from 'Contexts';
import { OverlaySwapContext } from 'Components/OverlaySwapProvider';
import CorebooksAnimation from 'Components/CorebooksAnimation';
import CorebooksAnimationSingle from 'Components/CorebooksAnimationSingle';

import * as UtilsScripts  from 'utils/utils';
import { Route, useHistory, useParams } from "react-router-dom";
import SingletoneNavigation from 'Class/SingletoneNavigation';

import LoadingComponent from "Components/Loading";


import { Transition } from "react-transition-group";
import { CSSTransition } from "react-transition-group";
import FooterMobile from 'Components/FooterMobile'
import Footer from 'Components/Footer'
import HeaderM from 'Components/HeaderM'
import Header from 'Components/Header'
import { useAppContext, useSessionContext } from 'context/AppContext';
import './WrapperProjectCont.css'
const Home = React.lazy(() => import('Components/Home'));
const Premium = React.lazy(() => import('Components/Premium'));
const Premium = React.lazy(() => import('Components/Premium'));
const Help = React.lazy(() => import('Components/Help'));
const CrearCuenta = React.lazy(() => import('Components/CrearCuenta'));
const Login = React.lazy(() => import('Components/Login'));



export const WrapperProjectContLocalStateReducer = (state = {}, action) => {

    return {
        ...state,
        [action.payload.key]: action.payload.value,
    };
};
    

const WrapperProjectCont = (props)=>{
    const [state,dispatch] = useAppContext();
    const [, setNameSelectedComponent] = React.useContext(OverlaySwapContext);
    
    const nodeRef = React.useRef(null);
    const history = useHistory();
    let RouterContext = useParams();
    
    
    
    
    
    
    const [globalStateForm, dispatchForm] = React.useState({});
    
    const [ _in, setIn ] = React.useState(false)
    const [ transaction, setTransaction ] = React.useState({}) 
    const [ animation, setAnimation ] = React.useState('')

    const [WrapperProjectContLocalState, dispatchWrapperProjectCont] = React.useReducer(WrapperProjectContLocalStateReducer, { ...{"prueba":"fsfsds"}, ...(props.state || {} ) });
    
    
    
    
    
    React.useEffect(()=>{
        
        
        
    },[]);
    
    
    return (
        <Contexts.WrapperProjectContLocalContext.Provider value={[WrapperProjectContLocalState,dispatchWrapperProjectCont]}>
  <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['wrapperprojectcont']?.animationClass || {}}>

    <div id="id_onefoureight_onezeroeightfour" ref={nodeRef} className={` ${ props.onClick ? 'cursor' : '' } wrapperprojectcont ${ props.cssClass } ${ transaction['wrapperprojectcont']?.type ? transaction['wrapperprojectcont']?.type.toLowerCase() : '' }`} style={ { ...{ ...{"width":"100%","position":"relative","overflow":"scroll"}, transitionDuration: transaction['wrapperprojectcont']?.duration, transitionTimingFunction: transaction['wrapperprojectcont']?.timingFunction }, ...props.style }} onClick={ props.WrapperProjectContonClick } onMouseEnter={ props.WrapperProjectContonMouseEnter } onMouseOver={ props.WrapperProjectContonMouseOver } onKeyPress={ props.WrapperProjectContonKeyPress } onDrag={ props.WrapperProjectContonDrag } onMouseLeave={ props.WrapperProjectContonMouseLeave } onMouseUp={ props.WrapperProjectContonMouseUp } onMouseDown={ props.WrapperProjectContonMouseDown } onKeyDown={ props.WrapperProjectContonKeyDown } onChange={ props.WrapperProjectContonChange } ondelay={ props.WrapperProjectContondelay }>
      {
      props.children ?
      props.children :
      <>
        <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['headers']?.animationClass || {}}>

          <div id="id_twoooneeight_onethreeoneeight" className={` frame headers ${ props.onClick ? 'cursor' : '' } ${ transaction['headers']?.type ? transaction['headers']?.type.toLowerCase() : '' }`} style={ { ...{"width":"100%","position":"sticky","zIndex":"9999999","top":"0px"}, ...props.HeadersStyle , transitionDuration: transaction['headers']?.duration, transitionTimingFunction: transaction['headers']?.timingFunction } } onClick={ props.HeadersonClick } onMouseEnter={ props.HeadersonMouseEnter } onMouseOver={ props.HeadersonMouseOver } onKeyPress={ props.HeadersonKeyPress } onDrag={ props.HeadersonDrag } onMouseLeave={ props.HeadersonMouseLeave } onMouseUp={ props.HeadersonMouseUp } onMouseDown={ props.HeadersonMouseDown } onKeyDown={ props.HeadersonKeyDown } onChange={ props.HeadersonChange } ondelay={ props.Headersondelay }>
            <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['header']?.animationClass || {}}>
              <Header { ...{ ...props, style:false } } style={{"width":"100%"}} cssClass={"C_twoofourfour_nigthfournigth hide-xs hide-s hide-m show-l show-xl"} />
            </CSSTransition>
            <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['headerm']?.animationClass || {}}>
              <HeaderM { ...{ ...props, style:false } } variant={'Property1=HeaderM'} IconMenu0={ props.IconMenu0 || "https://cdn.uncodie.com/tACQBrKCV97e0EqPE1XNLJ/d49df48697b10d4caaddceeab09c71d040761bdf.png" } IconMenuonClick={function(e){ setTransaction({ }); setvariant('Property1=HeaderExtM'); }} style={{"marginBottom":"-100px"}} cssClass={"C_twoofiveeight_onesixfivenigth cursor show-xs show-s show-m hide-l hide-xl"}>
                <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['uncodie_logom']?.animationClass || {}}>
                  <img id="id_twoofiveeight_onesixfivenigth_twoofiveeight_onesixfourone" className={` rectangle uncodie_logom ${ props.onClick ? 'cursor' : '' } ${ transaction['uncodie_logom']?.type ? transaction['uncodie_logom']?.type.toLowerCase() : '' }`} style={{ ...{},  ...props.Uncodie_logoMStyle , transitionDuration: transaction['uncodie_logom']?.duration, transitionTimingFunction: transaction['uncodie_logom']?.timingFunction }} onClick={ props.Uncodie_logoMonClick } onMouseEnter={ props.Uncodie_logoMonMouseEnter } onMouseOver={ props.Uncodie_logoMonMouseOver } onKeyPress={ props.Uncodie_logoMonKeyPress } onDrag={ props.Uncodie_logoMonDrag } onMouseLeave={ props.Uncodie_logoMonMouseLeave } onMouseUp={ props.Uncodie_logoMonMouseUp } onMouseDown={ props.Uncodie_logoMonMouseDown } onKeyDown={ props.Uncodie_logoMonKeyDown } onChange={ props.Uncodie_logoMonChange } ondelay={ props.Uncodie_logoMondelay } src={props.Uncodie_logoM0 || "https://cdn.uncodie.com/tACQBrKCV97e0EqPE1XNLJ/efbb152934f48228779350567e3e051c143ce0f2.png" } />
                </CSSTransition>
                <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['iconmenu']?.animationClass || {}}>
                  <img id="id_twoofiveeight_onesixfivenigth_twoofiveeight_onesixfourtwoo" className={` rectangle iconmenu cursor ${ props.onClick ? 'cursor' : '' } ${ transaction['iconmenu']?.type ? transaction['iconmenu']?.type.toLowerCase() : '' }`} style={{ ...{},  ...props.IconMenuStyle , transitionDuration: transaction['iconmenu']?.duration, transitionTimingFunction: transaction['iconmenu']?.timingFunction }} onClick={ props.IconMenuonClick || function(e){ setTransaction({ }); setvariant('Property1=HeaderExtM'); }} onMouseEnter={ props.IconMenuonMouseEnter } onMouseOver={ props.IconMenuonMouseOver } onKeyPress={ props.IconMenuonKeyPress } onDrag={ props.IconMenuonDrag } onMouseLeave={ props.IconMenuonMouseLeave } onMouseUp={ props.IconMenuonMouseUp } onMouseDown={ props.IconMenuonMouseDown } onKeyDown={ props.IconMenuonKeyDown } onChange={ props.IconMenuonChange } ondelay={ props.IconMenuondelay } src={props.IconMenu0 || "https://cdn.uncodie.com/tACQBrKCV97e0EqPE1XNLJ/d49df48697b10d4caaddceeab09c71d040761bdf.png" } />
                </CSSTransition>
              </HeaderM>
            </CSSTransition>
          </div>

        </CSSTransition>

        <div id="id_onefoureight_onezeroeightfive" className="contentproject  content " style={ props.ContentProjectStyle || {"width":"100%","height":"100%"}} undefined>

          <Suspense fallback={<LoadingComponent />} >
          <Route component={Home} path='/' exact={true} />
          <Route component={Premium} path='/Planes' exact={true} />
          <Route component={Premium} path='/Planes' exact={true} />
          <Route component={Help} path='/help' exact={true} />
          <Route component={CrearCuenta} path='/help' exact={true} />
          <Route component={Login} path='/help' exact={true} />
      </ Suspense>
    </div>

    <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['footers']?.animationClass || {}}>

      <div id="id_twoooneeight_onethreetwooone" className={` frame footers ${ props.onClick ? 'cursor' : '' } ${ transaction['footers']?.type ? transaction['footers']?.type.toLowerCase() : '' }`} style={ { ...{}, ...props.FootersStyle , transitionDuration: transaction['footers']?.duration, transitionTimingFunction: transaction['footers']?.timingFunction } } onClick={ props.FootersonClick } onMouseEnter={ props.FootersonMouseEnter } onMouseOver={ props.FootersonMouseOver } onKeyPress={ props.FootersonKeyPress } onDrag={ props.FootersonDrag } onMouseLeave={ props.FootersonMouseLeave } onMouseUp={ props.FootersonMouseUp } onMouseDown={ props.FootersonMouseDown } onKeyDown={ props.FootersonKeyDown } onChange={ props.FootersonChange } ondelay={ props.Footersondelay }>
        <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['footer']?.animationClass || {}}>
          <Footer { ...{ ...props, style:false } } style={{"width":"100%"}} cssClass={"C_threezerofive_sevenzerotwoo "}  />
    </CSSTransition >
<CSSTransition 
        in={_in}
        appear={true} 
        timeout={100}
        classNames={transaction['footermobile']?.animationClass || {}}
    >
    <FooterMobile { ...{ ...props, style:false } }    cssClass={" C_threethreefive_oneonesevenzero show-xs show-s show-m hide-l hide-xl"} />
        </CSSTransition>
      </div>

    </CSSTransition>

    </>
    }
    </div>

  </CSSTransition>
</Contexts.WrapperProjectContLocalContext.Provider>
    ) 
}

WrapperProjectCont.propTypes = {
    style: PropTypes.any,
IconMenu0: PropTypes.any,
IconMenuundefined: PropTypes.any,
Uncodie_logoM0: PropTypes.any,
WrapperProjectContonClick: PropTypes.any,
WrapperProjectContonMouseEnter: PropTypes.any,
WrapperProjectContonMouseOver: PropTypes.any,
WrapperProjectContonKeyPress: PropTypes.any,
WrapperProjectContonDrag: PropTypes.any,
WrapperProjectContonMouseLeave: PropTypes.any,
WrapperProjectContonMouseUp: PropTypes.any,
WrapperProjectContonMouseDown: PropTypes.any,
WrapperProjectContonKeyDown: PropTypes.any,
WrapperProjectContonChange: PropTypes.any,
WrapperProjectContondelay: PropTypes.any,
HeadersonClick: PropTypes.any,
HeadersonMouseEnter: PropTypes.any,
HeadersonMouseOver: PropTypes.any,
HeadersonKeyPress: PropTypes.any,
HeadersonDrag: PropTypes.any,
HeadersonMouseLeave: PropTypes.any,
HeadersonMouseUp: PropTypes.any,
HeadersonMouseDown: PropTypes.any,
HeadersonKeyDown: PropTypes.any,
HeadersonChange: PropTypes.any,
Headersondelay: PropTypes.any,
Uncodie_logoMonClick: PropTypes.any,
Uncodie_logoMonMouseEnter: PropTypes.any,
Uncodie_logoMonMouseOver: PropTypes.any,
Uncodie_logoMonKeyPress: PropTypes.any,
Uncodie_logoMonDrag: PropTypes.any,
Uncodie_logoMonMouseLeave: PropTypes.any,
Uncodie_logoMonMouseUp: PropTypes.any,
Uncodie_logoMonMouseDown: PropTypes.any,
Uncodie_logoMonKeyDown: PropTypes.any,
Uncodie_logoMonChange: PropTypes.any,
Uncodie_logoMondelay: PropTypes.any,
IconMenuonClick: PropTypes.any,
IconMenuonMouseEnter: PropTypes.any,
IconMenuonMouseOver: PropTypes.any,
IconMenuonKeyPress: PropTypes.any,
IconMenuonDrag: PropTypes.any,
IconMenuonMouseLeave: PropTypes.any,
IconMenuonMouseUp: PropTypes.any,
IconMenuonMouseDown: PropTypes.any,
IconMenuonKeyDown: PropTypes.any,
IconMenuonChange: PropTypes.any,
IconMenuondelay: PropTypes.any,
ContentProjectonClick: PropTypes.any,
ContentProjectonMouseEnter: PropTypes.any,
ContentProjectonMouseOver: PropTypes.any,
ContentProjectonKeyPress: PropTypes.any,
ContentProjectonDrag: PropTypes.any,
ContentProjectonMouseLeave: PropTypes.any,
ContentProjectonMouseUp: PropTypes.any,
ContentProjectonMouseDown: PropTypes.any,
ContentProjectonKeyDown: PropTypes.any,
ContentProjectonChange: PropTypes.any,
ContentProjectondelay: PropTypes.any,
FootersonClick: PropTypes.any,
FootersonMouseEnter: PropTypes.any,
FootersonMouseOver: PropTypes.any,
FootersonKeyPress: PropTypes.any,
FootersonDrag: PropTypes.any,
FootersonMouseLeave: PropTypes.any,
FootersonMouseUp: PropTypes.any,
FootersonMouseDown: PropTypes.any,
FootersonKeyDown: PropTypes.any,
FootersonChange: PropTypes.any,
Footersondelay: PropTypes.any
}
export default WrapperProjectCont;