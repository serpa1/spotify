import React, { Suspense } from 'react';
import PropTypes from 'prop-types';
import * as Contexts  from 'Contexts';
import { OverlaySwapContext } from 'Components/OverlaySwapProvider';
import CorebooksAnimation from 'Components/CorebooksAnimation';
import CorebooksAnimationSingle from 'Components/CorebooksAnimationSingle';

import * as UtilsScripts  from 'utils/utils';
import { Route, useHistory, useParams } from "react-router-dom";
import SingletoneNavigation from 'Class/SingletoneNavigation';

import LoadingComponent from "Components/Loading";


import { Transition } from "react-transition-group";
import { CSSTransition } from "react-transition-group";
import Paragraphone from 'Components/Paragraphone'
import IconInstagramOffImg from 'Components/IconInstagramOffImg'
import IconTwitterOffImg from 'Components/IconTwitterOffImg'
import TextLink from 'Components/TextLink'
import Subtitleone from 'Components/Subtitleone'
import { useAppContext, useSessionContext } from 'context/AppContext';
import './Footer.css'





const Footer = (props)=>{
    const [state,dispatch] = useAppContext();
    const [, setNameSelectedComponent] = React.useContext(OverlaySwapContext);
    
    const nodeRef = React.useRef(null);
    const history = useHistory();
    let RouterContext = useParams();
    
    
    
    
    
    
    const [globalStateForm, dispatchForm] = React.useState({});
    
    const [ _in, setIn ] = React.useState(false)
    const [ transaction, setTransaction ] = React.useState({}) 
    const [ animation, setAnimation ] = React.useState('')

    
    
    
    
    
    
    React.useEffect(()=>{
        
        
        
    },[]);
    
    
    return (
        <>
  <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['footer']?.animationClass || {}}>

    <div id="id_twoofivethree_onetwootwoothree" ref={nodeRef} className={` ${ props.onClick ? 'cursor' : '' } footer C_twoofivethree_onetwootwoothree ${ props.cssClass } ${ transaction['footer']?.type ? transaction['footer']?.type.toLowerCase() : '' }`} style={ { ...{ ...{"width":"100%"}, transitionDuration: transaction['footer']?.duration, transitionTimingFunction: transaction['footer']?.timingFunction }, ...props.style }} onClick={ props.FooteronClick } onMouseEnter={ props.FooteronMouseEnter } onMouseOver={ props.FooteronMouseOver } onKeyPress={ props.FooteronKeyPress } onDrag={ props.FooteronDrag } onMouseLeave={ props.FooteronMouseLeave } onMouseUp={ props.FooteronMouseUp } onMouseDown={ props.FooteronMouseDown } onKeyDown={ props.FooteronKeyDown } onChange={ props.FooteronChange } ondelay={ props.Footerondelay }>
      {
      props.children ?
      props.children :
      <>
        <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['footercont']?.animationClass || {}}>

          <div id="id_onesixseven_nigthzeronigth" className={` frame footercont ${ props.onClick ? 'cursor' : '' } ${ transaction['footercont']?.type ? transaction['footercont']?.type.toLowerCase() : '' }`} style={ { ...{"width":"100%"}, ...props.FooterContStyle , transitionDuration: transaction['footercont']?.duration, transitionTimingFunction: transaction['footercont']?.timingFunction } } onClick={ props.FooterContonClick } onMouseEnter={ props.FooterContonMouseEnter } onMouseOver={ props.FooterContonMouseOver } onKeyPress={ props.FooterContonKeyPress } onDrag={ props.FooterContonDrag } onMouseLeave={ props.FooterContonMouseLeave } onMouseUp={ props.FooterContonMouseUp } onMouseDown={ props.FooterContonMouseDown } onKeyDown={ props.FooterContonKeyDown } onChange={ props.FooterContonChange } ondelay={ props.FooterContondelay }>
            <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['footerdata']?.animationClass || {}}>

              <div id="id_onesixseven_nigthonezero" className={` frame footerdata ${ props.onClick ? 'cursor' : '' } ${ transaction['footerdata']?.type ? transaction['footerdata']?.type.toLowerCase() : '' }`} style={ { ...{}, ...props.FooterDataStyle , transitionDuration: transaction['footerdata']?.duration, transitionTimingFunction: transaction['footerdata']?.timingFunction } } onClick={ props.FooterDataonClick } onMouseEnter={ props.FooterDataonMouseEnter } onMouseOver={ props.FooterDataonMouseOver } onKeyPress={ props.FooterDataonKeyPress } onDrag={ props.FooterDataonDrag } onMouseLeave={ props.FooterDataonMouseLeave } onMouseUp={ props.FooterDataonMouseUp } onMouseDown={ props.FooterDataonMouseDown } onKeyDown={ props.FooterDataonKeyDown } onChange={ props.FooterDataonChange } ondelay={ props.FooterDataondelay }>
                <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['footerdatacompany']?.animationClass || {}}>

                  <div id="id_onesixseven_nigthoneone" className={` frame footerdatacompany ${ props.onClick ? 'cursor' : '' } ${ transaction['footerdatacompany']?.type ? transaction['footerdatacompany']?.type.toLowerCase() : '' }`} style={ { ...{}, ...props.FooterDataCompanyStyle , transitionDuration: transaction['footerdatacompany']?.duration, transitionTimingFunction: transaction['footerdatacompany']?.timingFunction } } onClick={ props.FooterDataCompanyonClick } onMouseEnter={ props.FooterDataCompanyonMouseEnter } onMouseOver={ props.FooterDataCompanyonMouseOver } onKeyPress={ props.FooterDataCompanyonKeyPress } onDrag={ props.FooterDataCompanyonDrag } onMouseLeave={ props.FooterDataCompanyonMouseLeave } onMouseUp={ props.FooterDataCompanyonMouseUp } onMouseDown={ props.FooterDataCompanyonMouseDown } onKeyDown={ props.FooterDataCompanyonKeyDown } onChange={ props.FooterDataCompanyonChange } ondelay={ props.FooterDataCompanyondelay }>
                    <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['subtitleone']?.animationClass || {}}>
                      <Subtitleone { ...{ ...props, style:false } } SubtilteoneText0={ props.SubtilteoneText0 || "Compañía" } cssClass={"C_onesixseven_nigthonetwoo "}  />
    </CSSTransition >
<CSSTransition 
        in={_in}
        appear={true} 
        timeout={100}
        classNames={transaction['footerlinkscompany']?.animationClass || {}}
    >
    
                    <div id="id_onesixseven_nigthonethree" className={` frame footerlinkscompany ${ props.onClick ? 'cursor' : '' } ${ transaction['footerlinkscompany']?.type ? transaction['footerlinkscompany']?.type.toLowerCase() : '' }`} style={ { ...{}, ...props.FooterLinksCompanyStyle , transitionDuration: transaction['footerlinkscompany']?.duration, transitionTimingFunction: transaction['footerlinkscompany']?.timingFunction } } onClick={ props.FooterLinksCompanyonClick } onMouseEnter={ props.FooterLinksCompanyonMouseEnter } onMouseOver={ props.FooterLinksCompanyonMouseOver } onKeyPress={ props.FooterLinksCompanyonKeyPress } onDrag={ props.FooterLinksCompanyonDrag } onMouseLeave={ props.FooterLinksCompanyonMouseLeave } onMouseUp={ props.FooterLinksCompanyonMouseUp } onMouseDown={ props.FooterLinksCompanyonMouseDown } onKeyDown={ props.FooterLinksCompanyonKeyDown } onChange={ props.FooterLinksCompanyonChange } ondelay={ props.FooterLinksCompanyondelay }>
                        <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['textlink']?.animationClass || {}}>
                          <TextLink { ...{ ...props, style:false } } variant={'Property 1=Default'} TextLinkTxt0={ props.TextLinkTxt0 || "Acerca de" } cssClass={"C_onesixseven_nigthonefour "}  />
    </CSSTransition >
<CSSTransition 
        in={_in}
        appear={true} 
        timeout={100}
        classNames={transaction['textlink']?.animationClass || {}}
    >
    <TextLink { ...{ ...props, style:false } }  variant={'Property 1=Default'} TextLinkTxt0={ props.TextLinkTxt1 || " Empleo"} cssClass={"C_onesixseven_nigthonefive "}  />
    </CSSTransition >
<CSSTransition 
        in={_in}
        appear={true} 
        timeout={100}
        classNames={transaction['textlink']?.animationClass || {}}
    >
    <TextLink { ...{ ...props, style:false } }  variant={'Property 1=Default'} TextLinkTxt0={ props.TextLinkTxt2 || " For the Record"} cssClass={"C_onesixseven_nigthonesix "}  />
    </CSSTransition >
                    </div>
               
    </CSSTransition >
                    </div>
               
    </CSSTransition >
<CSSTransition 
        in={_in}
        appear={true} 
        timeout={100}
        classNames={transaction['footerdatacommunity']?.animationClass || {}}
    >
    
                    <div id="id_onesixseven_nigthoneseven" className={` frame footerdatacommunity ${ props.onClick ? 'cursor' : '' } ${ transaction['footerdatacommunity']?.type ? transaction['footerdatacommunity']?.type.toLowerCase() : '' }`} style={ { ...{}, ...props.FooterDataCommunityStyle , transitionDuration: transaction['footerdatacommunity']?.duration, transitionTimingFunction: transaction['footerdatacommunity']?.timingFunction } } onClick={ props.FooterDataCommunityonClick } onMouseEnter={ props.FooterDataCommunityonMouseEnter } onMouseOver={ props.FooterDataCommunityonMouseOver } onKeyPress={ props.FooterDataCommunityonKeyPress } onDrag={ props.FooterDataCommunityonDrag } onMouseLeave={ props.FooterDataCommunityonMouseLeave } onMouseUp={ props.FooterDataCommunityonMouseUp } onMouseDown={ props.FooterDataCommunityonMouseDown } onKeyDown={ props.FooterDataCommunityonKeyDown } onChange={ props.FooterDataCommunityonChange } ondelay={ props.FooterDataCommunityondelay }>
                            <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['subtitleone']?.animationClass || {}}>
                              <Subtitleone { ...{ ...props, style:false } } SubtilteoneText0={ props.SubtilteoneText1 || "Comunidades" } cssClass={"C_onesixseven_nigthoneeight "}  />
    </CSSTransition >
<CSSTransition 
        in={_in}
        appear={true} 
        timeout={100}
        classNames={transaction['footerlinkscommunity']?.animationClass || {}}
    >
    
                    <div id="id_onesixseven_nigthonenigth" className={` frame footerlinkscommunity ${ props.onClick ? 'cursor' : '' } ${ transaction['footerlinkscommunity']?.type ? transaction['footerlinkscommunity']?.type.toLowerCase() : '' }`} style={ { ...{}, ...props.FooterLinksCommunityStyle , transitionDuration: transaction['footerlinkscommunity']?.duration, transitionTimingFunction: transaction['footerlinkscommunity']?.timingFunction } } onClick={ props.FooterLinksCommunityonClick } onMouseEnter={ props.FooterLinksCommunityonMouseEnter } onMouseOver={ props.FooterLinksCommunityonMouseOver } onKeyPress={ props.FooterLinksCommunityonKeyPress } onDrag={ props.FooterLinksCommunityonDrag } onMouseLeave={ props.FooterLinksCommunityonMouseLeave } onMouseUp={ props.FooterLinksCommunityonMouseUp } onMouseDown={ props.FooterLinksCommunityonMouseDown } onKeyDown={ props.FooterLinksCommunityonKeyDown } onChange={ props.FooterLinksCommunityonChange } ondelay={ props.FooterLinksCommunityondelay }>
                                <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['textlink']?.animationClass || {}}>
                                  <TextLink { ...{ ...props, style:false } } variant={'Property 1=Default'} TextLinkTxt0={ props.TextLinkTxt3 || "Para artistas" } cssClass={"C_onesixseven_nigthtwoozero "}  />
    </CSSTransition >
<CSSTransition 
        in={_in}
        appear={true} 
        timeout={100}
        classNames={transaction['textlink']?.animationClass || {}}
    >
    <TextLink { ...{ ...props, style:false } }  variant={'Property 1=Default'} TextLinkTxt0={ props.TextLinkTxt4 || " Desarrolladores"} cssClass={"C_onesixseven_nigthtwooone "}  />
    </CSSTransition >
<CSSTransition 
        in={_in}
        appear={true} 
        timeout={100}
        classNames={transaction['textlink']?.animationClass || {}}
    >
    <TextLink { ...{ ...props, style:false } }  variant={'Property 1=Default'} TextLinkTxt0={ props.TextLinkTxt5 || " Publicidad"} cssClass={"C_onesixseven_nigthtwootwoo "}  />
    </CSSTransition >
<CSSTransition 
        in={_in}
        appear={true} 
        timeout={100}
        classNames={transaction['textlink']?.animationClass || {}}
    >
    <TextLink { ...{ ...props, style:false } }  variant={'Property 1=Default'} TextLinkTxt0={ props.TextLinkTxt6 || " Inversionistas"} cssClass={"C_onesixseven_nigthtwoothree "}  />
    </CSSTransition >
<CSSTransition 
        in={_in}
        appear={true} 
        timeout={100}
        classNames={transaction['textlink']?.animationClass || {}}
    >
    <TextLink { ...{ ...props, style:false } }  variant={'Property 1=Default'} TextLinkTxt0={ props.TextLinkTxt7 || " Proveedores"} cssClass={"C_onesixseven_nigthtwoofour "}  />
    </CSSTransition >
<CSSTransition 
        in={_in}
        appear={true} 
        timeout={100}
        classNames={transaction['textlink']?.animationClass || {}}
    >
    <TextLink { ...{ ...props, style:false } }  variant={'Property 1=Default'} TextLinkTxt0={ props.TextLinkTxt8 || " Spotify for Work"} cssClass={"C_onesixseven_nigthtwoofive "}  />
    </CSSTransition >
                    </div>
               
    </CSSTransition >
                    </div>
               
    </CSSTransition >
<CSSTransition 
        in={_in}
        appear={true} 
        timeout={100}
        classNames={transaction['footerutilitycont']?.animationClass || {}}
    >
    
                    <div id="id_onesixseven_nigthtwoosix" className={` frame footerutilitycont ${ props.onClick ? 'cursor' : '' } ${ transaction['footerutilitycont']?.type ? transaction['footerutilitycont']?.type.toLowerCase() : '' }`} style={ { ...{}, ...props.FooterUtilityContStyle , transitionDuration: transaction['footerutilitycont']?.duration, transitionTimingFunction: transaction['footerutilitycont']?.timingFunction } } onClick={ props.FooterUtilityContonClick } onMouseEnter={ props.FooterUtilityContonMouseEnter } onMouseOver={ props.FooterUtilityContonMouseOver } onKeyPress={ props.FooterUtilityContonKeyPress } onDrag={ props.FooterUtilityContonDrag } onMouseLeave={ props.FooterUtilityContonMouseLeave } onMouseUp={ props.FooterUtilityContonMouseUp } onMouseDown={ props.FooterUtilityContonMouseDown } onKeyDown={ props.FooterUtilityContonKeyDown } onChange={ props.FooterUtilityContonChange } ondelay={ props.FooterUtilityContondelay }>
                                    <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['subtitleone']?.animationClass || {}}>
                                      <Subtitleone { ...{ ...props, style:false } } SubtilteoneText0={ props.SubtilteoneText2 || "Enlaces útiles" } cssClass={"C_onesixseven_nigthtwooseven "}  />
    </CSSTransition >
<CSSTransition 
        in={_in}
        appear={true} 
        timeout={100}
        classNames={transaction['footerlinksutility']?.animationClass || {}}
    >
    
                    <div id="id_onesixseven_nigthtwooeight" className={` frame footerlinksutility ${ props.onClick ? 'cursor' : '' } ${ transaction['footerlinksutility']?.type ? transaction['footerlinksutility']?.type.toLowerCase() : '' }`} style={ { ...{}, ...props.FooterLinksUtilityStyle , transitionDuration: transaction['footerlinksutility']?.duration, transitionTimingFunction: transaction['footerlinksutility']?.timingFunction } } onClick={ props.FooterLinksUtilityonClick } onMouseEnter={ props.FooterLinksUtilityonMouseEnter } onMouseOver={ props.FooterLinksUtilityonMouseOver } onKeyPress={ props.FooterLinksUtilityonKeyPress } onDrag={ props.FooterLinksUtilityonDrag } onMouseLeave={ props.FooterLinksUtilityonMouseLeave } onMouseUp={ props.FooterLinksUtilityonMouseUp } onMouseDown={ props.FooterLinksUtilityonMouseDown } onKeyDown={ props.FooterLinksUtilityonKeyDown } onChange={ props.FooterLinksUtilityonChange } ondelay={ props.FooterLinksUtilityondelay }>
                                        <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['textlink']?.animationClass || {}}>
                                          <TextLink { ...{ ...props, style:false } } variant={'Property 1=Default'} TextLinkTxt0={ props.TextLinkTxt9 || "Ayuda" } cssClass={"C_onesixseven_nigthtwoonigth "}  />
    </CSSTransition >
<CSSTransition 
        in={_in}
        appear={true} 
        timeout={100}
        classNames={transaction['textlink']?.animationClass || {}}
    >
    <TextLink { ...{ ...props, style:false } }  variant={'Property 1=Default'} TextLinkTxt0={ props.TextLinkTxt10 || " App móvil gratis"} cssClass={"C_onesixseven_nigththreezero "}  />
    </CSSTransition >
                    </div>
               
    </CSSTransition >
                    </div>
               
    </CSSTransition >
                    </div>
               
    </CSSTransition >
<CSSTransition 
        in={_in}
        appear={true} 
        timeout={100}
        classNames={transaction['footerdatasocialmedia']?.animationClass || {}}
    >
    
                    <div id="id_onesixseven_nigththreetwoo" className={` frame footerdatasocialmedia ${ props.onClick ? 'cursor' : '' } ${ transaction['footerdatasocialmedia']?.type ? transaction['footerdatasocialmedia']?.type.toLowerCase() : '' }`} style={ { ...{}, ...props.FooterDataSocialMediaStyle , transitionDuration: transaction['footerdatasocialmedia']?.duration, transitionTimingFunction: transaction['footerdatasocialmedia']?.timingFunction } } onClick={ props.FooterDataSocialMediaonClick } onMouseEnter={ props.FooterDataSocialMediaonMouseEnter } onMouseOver={ props.FooterDataSocialMediaonMouseOver } onKeyPress={ props.FooterDataSocialMediaonKeyPress } onDrag={ props.FooterDataSocialMediaonDrag } onMouseLeave={ props.FooterDataSocialMediaonMouseLeave } onMouseUp={ props.FooterDataSocialMediaonMouseUp } onMouseDown={ props.FooterDataSocialMediaonMouseDown } onKeyDown={ props.FooterDataSocialMediaonKeyDown } onChange={ props.FooterDataSocialMediaonChange } ondelay={ props.FooterDataSocialMediaondelay }>
                                            <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['icontwitteroffimg']?.animationClass || {}}>
                                              <IconTwitterOffImg { ...{ ...props, style:false } } variant={'Property 1=Default'} cssClass={"C_onesixseven_nigththreethree "}  />
    </CSSTransition >
<CSSTransition 
        in={_in}
        appear={true} 
        timeout={100}
        classNames={transaction['iconinstagramoffimg']?.animationClass || {}}
    >
    <IconInstagramOffImg { ...{ ...props, style:false } }  variant={'Property 1=Default'}  cssClass={" C_onesixseven_nigththreefour "}  />
    </CSSTransition >
<CSSTransition 
        in={_in}
        appear={true} 
        timeout={100}
        classNames={transaction['icontwitteroffimg']?.animationClass || {}}
    >
    <IconTwitterOffImg { ...{ ...props, style:false } }  variant={'Property 1=Default'}  cssClass={" C_onesixseven_nigththreefive "}  />
    </CSSTransition >
                    </div>
               
    </CSSTransition >
                    </div>
               
    </CSSTransition >
<CSSTransition 
        in={_in}
        appear={true} 
        timeout={100}
        classNames={transaction['footercopyright']?.animationClass || {}}
    >
    
                    <div id="id_onesixseven_nigththreesix" className={` frame footercopyright ${ props.onClick ? 'cursor' : '' } ${ transaction['footercopyright']?.type ? transaction['footercopyright']?.type.toLowerCase() : '' }`} style={ { ...{"width":"100%","borderTop":"1px solid #ededed","display":"flex"}, ...props.FooterCopyrightStyle , transitionDuration: transaction['footercopyright']?.duration, transitionTimingFunction: transaction['footercopyright']?.timingFunction } } onClick={ props.FooterCopyrightonClick } onMouseEnter={ props.FooterCopyrightonMouseEnter } onMouseOver={ props.FooterCopyrightonMouseOver } onKeyPress={ props.FooterCopyrightonKeyPress } onDrag={ props.FooterCopyrightonDrag } onMouseLeave={ props.FooterCopyrightonMouseLeave } onMouseUp={ props.FooterCopyrightonMouseUp } onMouseDown={ props.FooterCopyrightonMouseDown } onKeyDown={ props.FooterCopyrightonKeyDown } onChange={ props.FooterCopyrightonChange } ondelay={ props.FooterCopyrightondelay }>
                                                <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['paragraphone']?.animationClass || {}}>
                                                  <Paragraphone { ...{ ...props, style:false } } ParagraphoneText0={ props.ParagraphoneText0 || "© 2023 Spotify AB" } cssClass={"C_onesixseven_nigththreeseven "}  />
    </CSSTransition >
                    </div>
               
    </CSSTransition >
            
            </>
        }
        </div>
    
    </CSSTransition >
            </>
        
    ) 
}

Footer.propTypes = {
    style: PropTypes.any,
SubtilteoneText0: PropTypes.any,
TextLinkTxt0: PropTypes.any,
TextLinkTxt1: PropTypes.any,
TextLinkTxt2: PropTypes.any,
SubtilteoneText1: PropTypes.any,
TextLinkTxt3: PropTypes.any,
TextLinkTxt4: PropTypes.any,
TextLinkTxt5: PropTypes.any,
TextLinkTxt6: PropTypes.any,
TextLinkTxt7: PropTypes.any,
TextLinkTxt8: PropTypes.any,
SubtilteoneText2: PropTypes.any,
TextLinkTxt9: PropTypes.any,
TextLinkTxt10: PropTypes.any,
ParagraphoneText0: PropTypes.any,
FooteronClick: PropTypes.any,
FooteronMouseEnter: PropTypes.any,
FooteronMouseOver: PropTypes.any,
FooteronKeyPress: PropTypes.any,
FooteronDrag: PropTypes.any,
FooteronMouseLeave: PropTypes.any,
FooteronMouseUp: PropTypes.any,
FooteronMouseDown: PropTypes.any,
FooteronKeyDown: PropTypes.any,
FooteronChange: PropTypes.any,
Footerondelay: PropTypes.any,
FooterContonClick: PropTypes.any,
FooterContonMouseEnter: PropTypes.any,
FooterContonMouseOver: PropTypes.any,
FooterContonKeyPress: PropTypes.any,
FooterContonDrag: PropTypes.any,
FooterContonMouseLeave: PropTypes.any,
FooterContonMouseUp: PropTypes.any,
FooterContonMouseDown: PropTypes.any,
FooterContonKeyDown: PropTypes.any,
FooterContonChange: PropTypes.any,
FooterContondelay: PropTypes.any,
FooterDataonClick: PropTypes.any,
FooterDataonMouseEnter: PropTypes.any,
FooterDataonMouseOver: PropTypes.any,
FooterDataonKeyPress: PropTypes.any,
FooterDataonDrag: PropTypes.any,
FooterDataonMouseLeave: PropTypes.any,
FooterDataonMouseUp: PropTypes.any,
FooterDataonMouseDown: PropTypes.any,
FooterDataonKeyDown: PropTypes.any,
FooterDataonChange: PropTypes.any,
FooterDataondelay: PropTypes.any,
FooterDataCompanyonClick: PropTypes.any,
FooterDataCompanyonMouseEnter: PropTypes.any,
FooterDataCompanyonMouseOver: PropTypes.any,
FooterDataCompanyonKeyPress: PropTypes.any,
FooterDataCompanyonDrag: PropTypes.any,
FooterDataCompanyonMouseLeave: PropTypes.any,
FooterDataCompanyonMouseUp: PropTypes.any,
FooterDataCompanyonMouseDown: PropTypes.any,
FooterDataCompanyonKeyDown: PropTypes.any,
FooterDataCompanyonChange: PropTypes.any,
FooterDataCompanyondelay: PropTypes.any,
FooterLinksCompanyonClick: PropTypes.any,
FooterLinksCompanyonMouseEnter: PropTypes.any,
FooterLinksCompanyonMouseOver: PropTypes.any,
FooterLinksCompanyonKeyPress: PropTypes.any,
FooterLinksCompanyonDrag: PropTypes.any,
FooterLinksCompanyonMouseLeave: PropTypes.any,
FooterLinksCompanyonMouseUp: PropTypes.any,
FooterLinksCompanyonMouseDown: PropTypes.any,
FooterLinksCompanyonKeyDown: PropTypes.any,
FooterLinksCompanyonChange: PropTypes.any,
FooterLinksCompanyondelay: PropTypes.any,
FooterDataCommunityonClick: PropTypes.any,
FooterDataCommunityonMouseEnter: PropTypes.any,
FooterDataCommunityonMouseOver: PropTypes.any,
FooterDataCommunityonKeyPress: PropTypes.any,
FooterDataCommunityonDrag: PropTypes.any,
FooterDataCommunityonMouseLeave: PropTypes.any,
FooterDataCommunityonMouseUp: PropTypes.any,
FooterDataCommunityonMouseDown: PropTypes.any,
FooterDataCommunityonKeyDown: PropTypes.any,
FooterDataCommunityonChange: PropTypes.any,
FooterDataCommunityondelay: PropTypes.any,
FooterLinksCommunityonClick: PropTypes.any,
FooterLinksCommunityonMouseEnter: PropTypes.any,
FooterLinksCommunityonMouseOver: PropTypes.any,
FooterLinksCommunityonKeyPress: PropTypes.any,
FooterLinksCommunityonDrag: PropTypes.any,
FooterLinksCommunityonMouseLeave: PropTypes.any,
FooterLinksCommunityonMouseUp: PropTypes.any,
FooterLinksCommunityonMouseDown: PropTypes.any,
FooterLinksCommunityonKeyDown: PropTypes.any,
FooterLinksCommunityonChange: PropTypes.any,
FooterLinksCommunityondelay: PropTypes.any,
FooterUtilityContonClick: PropTypes.any,
FooterUtilityContonMouseEnter: PropTypes.any,
FooterUtilityContonMouseOver: PropTypes.any,
FooterUtilityContonKeyPress: PropTypes.any,
FooterUtilityContonDrag: PropTypes.any,
FooterUtilityContonMouseLeave: PropTypes.any,
FooterUtilityContonMouseUp: PropTypes.any,
FooterUtilityContonMouseDown: PropTypes.any,
FooterUtilityContonKeyDown: PropTypes.any,
FooterUtilityContonChange: PropTypes.any,
FooterUtilityContondelay: PropTypes.any,
FooterLinksUtilityonClick: PropTypes.any,
FooterLinksUtilityonMouseEnter: PropTypes.any,
FooterLinksUtilityonMouseOver: PropTypes.any,
FooterLinksUtilityonKeyPress: PropTypes.any,
FooterLinksUtilityonDrag: PropTypes.any,
FooterLinksUtilityonMouseLeave: PropTypes.any,
FooterLinksUtilityonMouseUp: PropTypes.any,
FooterLinksUtilityonMouseDown: PropTypes.any,
FooterLinksUtilityonKeyDown: PropTypes.any,
FooterLinksUtilityonChange: PropTypes.any,
FooterLinksUtilityondelay: PropTypes.any,
FooterDataSocialMediaonClick: PropTypes.any,
FooterDataSocialMediaonMouseEnter: PropTypes.any,
FooterDataSocialMediaonMouseOver: PropTypes.any,
FooterDataSocialMediaonKeyPress: PropTypes.any,
FooterDataSocialMediaonDrag: PropTypes.any,
FooterDataSocialMediaonMouseLeave: PropTypes.any,
FooterDataSocialMediaonMouseUp: PropTypes.any,
FooterDataSocialMediaonMouseDown: PropTypes.any,
FooterDataSocialMediaonKeyDown: PropTypes.any,
FooterDataSocialMediaonChange: PropTypes.any,
FooterDataSocialMediaondelay: PropTypes.any,
FooterCopyrightonClick: PropTypes.any,
FooterCopyrightonMouseEnter: PropTypes.any,
FooterCopyrightonMouseOver: PropTypes.any,
FooterCopyrightonKeyPress: PropTypes.any,
FooterCopyrightonDrag: PropTypes.any,
FooterCopyrightonMouseLeave: PropTypes.any,
FooterCopyrightonMouseUp: PropTypes.any,
FooterCopyrightonMouseDown: PropTypes.any,
FooterCopyrightonKeyDown: PropTypes.any,
FooterCopyrightonChange: PropTypes.any,
FooterCopyrightondelay: PropTypes.any
}
export default Footer;