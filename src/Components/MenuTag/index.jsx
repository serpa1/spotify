import React, { Suspense } from 'react';
import PropTypes from 'prop-types';
import * as Contexts  from 'Contexts';
import { OverlaySwapContext } from 'Components/OverlaySwapProvider';
import CorebooksAnimation from 'Components/CorebooksAnimation';
import CorebooksAnimationSingle from 'Components/CorebooksAnimationSingle';

import * as UtilsScripts  from 'utils/utils';
import { Route, useHistory, useParams } from "react-router-dom";
import SingletoneNavigation from 'Class/SingletoneNavigation';

import LoadingComponent from "Components/Loading";


import { Transition } from "react-transition-group";
import { CSSTransition } from "react-transition-group";
import SecundaryButton from 'Components/SecundaryButton'
import { useAppContext, useSessionContext } from 'context/AppContext';
import './MenuTag.css'





const MenuTag = (props)=>{
    const [state,dispatch] = useAppContext();
    const [, setNameSelectedComponent] = React.useContext(OverlaySwapContext);
    
    const nodeRefpropertyonedefault = React.useRef(null);
const nodeRefpropertyonevarianttwoo = React.useRef(null);
    const history = useHistory();
    let RouterContext = useParams();
    
    
    
    
    
    
    const [globalStateForm, dispatchForm] = React.useState({});
    const [ variant, setvariant] = React.useState(props.variant || 'Property 1=Default')
    const [ _in, setIn ] = React.useState(false)
    const [ transaction, setTransaction ] = React.useState({}) 
    const [ animation, setAnimation ] = React.useState('')

    
    
    
    
    
    
    React.useEffect(()=>{
        
        
        
    },[]);
    
    
    React.useEffect(()=>{
        
        setIn(false);
        setTimeout(()=>setIn(true))
           
      },[variant]);
    const switchVariant = (value)=>{
        switch (value) {
            case 'Property 1=Default':{
            return (
            <>

  <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['propertyonedefault']?.animationClass || {}}>

    <div id="id_fourthreeeight_fouronesevenseven" ref={nodeRefpropertyonedefault} className={` cursor ${ props.onClick ? 'cursor' : '' } menutagpropertyonedefault C_fourthreeeight_fouronesevenseven ${ props.cssClass } ${ transaction['propertyonedefault']?.type ? transaction['propertyonedefault']?.type.toLowerCase() : '' }`} style={ { ...{ ...{}, transitionDuration: transaction['propertyonedefault']?.duration, transitionTimingFunction: transaction['propertyonedefault']?.timingFunction }, ...props.style }} onClick={ props.MenuTagonClick } onMouseEnter={ props.MenuTagonMouseEnter } onMouseOver={ props.MenuTagonMouseOver || function(e){ setTransaction({ }); setvariant('Property 1=Variant2'); }} onKeyPress={ props.MenuTagonKeyPress } onDrag={ props.MenuTagonDrag } onMouseLeave={ props.MenuTagonMouseLeave } onMouseUp={ props.MenuTagonMouseUp } onMouseDown={ props.MenuTagonMouseDown } onKeyDown={ props.MenuTagonKeyDown } onChange={ props.MenuTagonChange } ondelay={ props.MenuTagondelay }>
      {
      props.children ?
      props.children :
      <>
        <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['secundarybutton']?.animationClass || {}}>
          <SecundaryButton { ...{ ...props, style:false } } variant={'Property 1=Default'} SecundaryButtonText0={ props.SecundaryButtonText0 || "Playlist" } SecundaryButton0={ props.SecundaryButton0 || "" } SecundaryButtonText0={ props.SecundaryButtonText0 || "Playlist" } cssClass={"C_fourthreeeight_fouronesevenfive cursor "}  />
    </CSSTransition >
            
            </>
        }
        </div>
    
    </CSSTransition >
                    
                </>
                
            )
        }
case 'Property 1=Variant2':{
            return (
            <>

  <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['propertyonevarianttwoo']?.animationClass || {}}>

    <div id="id_fourthreeeight_fouronesevennigth" ref={nodeRefpropertyonevarianttwoo} className={` cursor ${ props.onClick ? 'cursor' : '' } menutagpropertyonevarianttwoo C_fourthreeeight_fouronesevennigth ${ props.cssClass } ${ transaction['propertyonevarianttwoo']?.type ? transaction['propertyonevarianttwoo']?.type.toLowerCase() : '' }`} style={ { ...{ ...{}, transitionDuration: transaction['propertyonevarianttwoo']?.duration, transitionTimingFunction: transaction['propertyonevarianttwoo']?.timingFunction }, ...props.style }} onClick={ props.MenuTagonClick } onMouseEnter={ props.MenuTagonMouseEnter } onMouseOver={ props.MenuTagonMouseOver } onKeyPress={ props.MenuTagonKeyPress } onDrag={ props.MenuTagonDrag } onMouseLeave={ props.MenuTagonMouseLeave || function(e){ setTransaction({ }); setvariant('Property 1=Default'); }} onMouseUp={ props.MenuTagonMouseUp } onMouseDown={ props.MenuTagonMouseDown } onKeyDown={ props.MenuTagonKeyDown } onChange={ props.MenuTagonChange } ondelay={ props.MenuTagondelay }>
      {
      props.children ?
      props.children :
      <>
        <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['secundarybutton']?.animationClass || {}}>
          <SecundaryButton { ...{ ...props, style:false } } variant={'Property 1=Default'} SecundaryButtonText0={ props.SecundaryButtonText0 || "Playlist" } SecundaryButtonText0={ props.SecundaryButtonText0 || "Playlist" } SecundaryButton0={ props.SecundaryButton0 || "" } cssClass={"C_fourthreeeight_fouroneeightzero cursor "}  />
    </CSSTransition >
            
            </>
        }
        </div>
    
    </CSSTransition >
                    
                </>
                
            )
        }
        }
    }

    return (
        <>
            {switchVariant(variant)}
        </>
    )
    
}

MenuTag.propTypes = {
    style: PropTypes.any,
variant: PropTypes.any,
SecundaryButtonText0: PropTypes.any,
SecundaryButton0: PropTypes.any,
SecundaryButtonundefined: PropTypes.any,
MenuTagonClick: PropTypes.any,
MenuTagonMouseEnter: PropTypes.any,
MenuTagonMouseOver: PropTypes.any,
MenuTagonKeyPress: PropTypes.any,
MenuTagonDrag: PropTypes.any,
MenuTagonMouseLeave: PropTypes.any,
MenuTagonMouseUp: PropTypes.any,
MenuTagonMouseDown: PropTypes.any,
MenuTagonKeyDown: PropTypes.any,
MenuTagonChange: PropTypes.any,
MenuTagondelay: PropTypes.any
}
export default MenuTag;