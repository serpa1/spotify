import React, { Suspense } from 'react';
import PropTypes from 'prop-types';
import * as Contexts  from 'Contexts';
import { OverlaySwapContext } from 'Components/OverlaySwapProvider';
import CorebooksAnimation from 'Components/CorebooksAnimation';
import CorebooksAnimationSingle from 'Components/CorebooksAnimationSingle';

import * as UtilsScripts  from 'utils/utils';
import { Route, useHistory, useParams } from "react-router-dom";
import SingletoneNavigation from 'Class/SingletoneNavigation';

import LoadingComponent from "Components/Loading";


import { Transition } from "react-transition-group";
import { CSSTransition } from "react-transition-group";
import PrincipalButton from 'Components/PrincipalButton'
import SecundaryButton from 'Components/SecundaryButton'
import LanguageButton from 'Components/LanguageButton'
import { useAppContext, useSessionContext } from 'context/AppContext';
import './Buttons.css'





const Buttons = (props)=>{
    const [state,dispatch] = useAppContext();
    const [, setNameSelectedComponent] = React.useContext(OverlaySwapContext);
    
    const nodeRef = React.useRef(null);
    const history = useHistory();
    let RouterContext = useParams();
    
    
    
    
    
    
    const [globalStateForm, dispatchForm] = React.useState({});
    
    const [ _in, setIn ] = React.useState(false)
    const [ transaction, setTransaction ] = React.useState({}) 
    const [ animation, setAnimation ] = React.useState('')

    
    
    
    
    
    
    React.useEffect(()=>{
        
        
        
    },[]);
    
    
    return (
        <>
  <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['buttons']?.animationClass || {}}>

    <div id="id_one_eightfivetwoo" ref={nodeRef} className={` ${ props.onClick ? 'cursor' : '' } buttons ${ props.cssClass } ${ transaction['buttons']?.type ? transaction['buttons']?.type.toLowerCase() : '' }`} style={ { ...{ ...{}, transitionDuration: transaction['buttons']?.duration, transitionTimingFunction: transaction['buttons']?.timingFunction }, ...props.style }} onClick={ props.ButtonsonClick } onMouseEnter={ props.ButtonsonMouseEnter } onMouseOver={ props.ButtonsonMouseOver } onKeyPress={ props.ButtonsonKeyPress } onDrag={ props.ButtonsonDrag } onMouseLeave={ props.ButtonsonMouseLeave } onMouseUp={ props.ButtonsonMouseUp } onMouseDown={ props.ButtonsonMouseDown } onKeyDown={ props.ButtonsonKeyDown } onChange={ props.ButtonsonChange } ondelay={ props.Buttonsondelay }>
      {
      props.children ?
      props.children :
      <>
        <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['languagebutton']?.animationClass || {}}>
          <LanguageButton { ...{ ...props, style:false } } cssClass={"C_one_eightfiveeight "}  />
    </CSSTransition >
<CSSTransition 
        in={_in}
        appear={true} 
        timeout={100}
        classNames={transaction['secundarybutton']?.animationClass || {}}
    >
    <SecundaryButton { ...{ ...props, style:false } }    cssClass={" C_onesixtwoo_fournigthfivefive "}  />
    </CSSTransition >
<CSSTransition 
        in={_in}
        appear={true} 
        timeout={100}
        classNames={transaction['principalbutton']?.animationClass || {}}
    >
    <PrincipalButton { ...{ ...props, style:false } }    cssClass={" C_onesixtwoo_fournigtheightthree "}  />
    </CSSTransition >
            
            </>
        }
        </div>
    
    </CSSTransition >
            </>
        
    ) 
}

Buttons.propTypes = {
    style: PropTypes.any,
ButtonsonClick: PropTypes.any,
ButtonsonMouseEnter: PropTypes.any,
ButtonsonMouseOver: PropTypes.any,
ButtonsonKeyPress: PropTypes.any,
ButtonsonDrag: PropTypes.any,
ButtonsonMouseLeave: PropTypes.any,
ButtonsonMouseUp: PropTypes.any,
ButtonsonMouseDown: PropTypes.any,
ButtonsonKeyDown: PropTypes.any,
ButtonsonChange: PropTypes.any,
Buttonsondelay: PropTypes.any
}
export default Buttons;