import React from 'react';
import '../CorebooksAnimation/CorebooksAnimation.css';
import styled from "styled-components";
import { getAnimationClass } from "../CorebooksAnimation/animations";
type Easing = "ease" | "linear" | "ease-in" | "ease-out" | "ease-in-out";
interface IDissolveProps{
    duration?: number | string;
    easing?: Easing;
}
const AnimationContainerSingleDissolve = styled.div`
  & > * {
    
    transition-property: opacity;
    transition-duration: ${(props: IDissolveProps) =>props.duration ? props.duration : "500ms"};
    transition-timing-function: ${(props:IDissolveProps) =>props.easing ? props.easing : "ease"};
  }
`;

const AnimationContainerSingleSmartAnimation = styled.div`
  & > * {
    
    transition-duration: ${(props: any) =>
      props.duration ? props.duration : "500ms"};
    transition-timing-function: ${(props:any) =>
      props.easing ? props.easing : "ease"};
    transition-property: all;
  }
`;
const AnimationWithOut = (props:any)=><>{props.children}</>;
type TypeTransition = "DISSOLVE" | "SMART_ANIMATE";
interface IEasing{
    type: Easing
};
interface ITransition{
    type: TypeTransition, 
    easing: IEasing, 
    duration: number 
}
interface IFromProps{
    width?: number, 
    height?: number, 
    opacity?: number, 
    transform?: string, 
    background?: string
}
interface IAnimationSingleProps{
    fromProps?: IFromProps,
    transition: ITransition,
    children: any
}
const getTransitionStyles = (transition:ITransition, active: boolean, fromProps?:IFromProps)=>{
    /** 
     * width: ${({width}:IFromProps)=>`${width}px`};
    height: ${({height}:IFromProps)=>`${height}px`};
    transform: ${({transform}:IFromProps)=>transform}; 
    background: ${({background}:IFromProps)=>background};  
    */
    switch (transition?.type) {
        case "DISSOLVE":
            return active ? { opacity: 1 } : { opacity: 0 }
        case "SMART_ANIMATE":
            return active ? { } : { ...fromProps }
       
    }
}
const CorebooksAnimationSingle = (props:IAnimationSingleProps)=>{
    const { fromProps, transition, children} = props;
    const [active, setActive] = React.useState(false);
    let Component:any = AnimationWithOut;
    switch (transition?.type) {
        case "DISSOLVE":
            Component = AnimationContainerSingleDissolve;
            break;
        case "SMART_ANIMATE":
            Component = AnimationContainerSingleSmartAnimation;
            break;
        default:
            Component = AnimationWithOut;
            break;
    }
    React.useEffect(()=>{

        setActive(false);
        setTimeout(()=>setActive(true), 40)
       
    },[children])
    const clones = React.Children.map(children, child => {
        return React.cloneElement(child, { style: getTransitionStyles(transition, active, fromProps) });
    });
    return( 
    <Component
        className={`animation-container `}
        duration={transition?.duration ? `${transition.duration * 1000}ms` : `ms`}
        easing={transition?.easing?.type ? transition?.easing?.type.toLowerCase().replace(/_/g,'-'): 'ease'}
        { ...fromProps }
    >
    {clones}
  </Component>)
        

}

export default CorebooksAnimationSingle