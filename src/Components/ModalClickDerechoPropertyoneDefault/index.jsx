import React, { Suspense } from 'react';
import PropTypes from 'prop-types';
import * as Contexts  from 'Contexts';
import { OverlaySwapContext } from 'Components/OverlaySwapProvider';
import CorebooksAnimation from 'Components/CorebooksAnimation';
import CorebooksAnimationSingle from 'Components/CorebooksAnimationSingle';

import * as UtilsScripts  from 'utils/utils';
import { Route, useHistory, useParams } from "react-router-dom";
import SingletoneNavigation from 'Class/SingletoneNavigation';

import LoadingComponent from "Components/Loading";


import { Transition } from "react-transition-group";
import { CSSTransition } from "react-transition-group";
import { useAppContext, useSessionContext } from 'context/AppContext';
import './ModalClickDerechoPropertyoneDefault.css'





const PropertyoneDefault = (props)=>{
    const [state,dispatch] = useAppContext();
    const [, setNameSelectedComponent] = React.useContext(OverlaySwapContext);
    
    const nodeRef = React.useRef(null);
    const history = useHistory();
    let RouterContext = useParams();
    
    
    
    
    
    
    const [globalStateForm, dispatchForm] = React.useState({});
    
    const [ _in, setIn ] = React.useState(false)
    const [ transaction, setTransaction ] = React.useState({}) 
    const [ animation, setAnimation ] = React.useState('')

    
    
    
    
    
    
    React.useEffect(()=>{
        
        
        
    },[]);
    
    
    return (
        <>
  <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['propertyonedefault']?.animationClass || {}}>

    <div id="id_fourthreeeight_threeeightonefour" ref={nodeRef} className={` ${ props.onClick ? 'cursor' : '' } propertyonedefault C_fourthreeeight_threeeightonefour ${ props.cssClass } ${ transaction['propertyonedefault']?.type ? transaction['propertyonedefault']?.type.toLowerCase() : '' }`} style={ { ...{ ...{}, transitionDuration: transaction['propertyonedefault']?.duration, transitionTimingFunction: transaction['propertyonedefault']?.timingFunction }, ...props.style }} onClick={ props.ModalClickDerechoonClick } onMouseEnter={ props.ModalClickDerechoonMouseEnter } onMouseOver={ props.ModalClickDerechoonMouseOver } onKeyPress={ props.ModalClickDerechoonKeyPress } onDrag={ props.ModalClickDerechoonDrag } onMouseLeave={ props.ModalClickDerechoonMouseLeave } onMouseUp={ props.ModalClickDerechoonMouseUp } onMouseDown={ props.ModalClickDerechoonMouseDown } onKeyDown={ props.ModalClickDerechoonKeyDown } onChange={ props.ModalClickDerechoonChange } ondelay={ props.ModalClickDerechoondelay }>
      {
      props.children ?
      props.children :
      <>
        <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['textonesixtxt']?.animationClass || {}}>

          <span id="id_fourthreeeight_threeeightoneone"  className={` text textonesixtxt    ${ props.onClick ? 'cursor' : ''}  ${ transaction['textonesixtxt']?.type ? transaction['textonesixtxt']?.type.toLowerCase() : '' }`} style={{ ...{},  ...props.TextonesixTxtStyle , transitionDuration: transaction['textonesixtxt']?.duration, transitionTimingFunction: transaction['textonesixtxt']?.timingFunction }} onClick={ props.TextonesixTxtonClick } onMouseEnter={ props.TextonesixTxtonMouseEnter } onMouseOver={ props.TextonesixTxtonMouseOver } onKeyPress={ props.TextonesixTxtonKeyPress } onDrag={ props.TextonesixTxtonDrag } onMouseLeave={ props.TextonesixTxtonMouseLeave } onMouseUp={ props.TextonesixTxtonMouseUp } onMouseDown={ props.TextonesixTxtonMouseDown } onKeyDown={ props.TextonesixTxtonKeyDown } onChange={ props.TextonesixTxtonChange } ondelay={ props.TextonesixTxtondelay } >{props.TextonesixTxt0 || `Crear playlist`}</span>

        </CSSTransition>

      </>
      }
    </div>

  </CSSTransition>
</>
    ) 
}

PropertyoneDefault.propTypes = {
    style: PropTypes.any,
TextonesixTxt0: PropTypes.any,
ModalClickDerechoonClick: PropTypes.any,
ModalClickDerechoonMouseEnter: PropTypes.any,
ModalClickDerechoonMouseOver: PropTypes.any,
ModalClickDerechoonKeyPress: PropTypes.any,
ModalClickDerechoonDrag: PropTypes.any,
ModalClickDerechoonMouseLeave: PropTypes.any,
ModalClickDerechoonMouseUp: PropTypes.any,
ModalClickDerechoonMouseDown: PropTypes.any,
ModalClickDerechoonKeyDown: PropTypes.any,
ModalClickDerechoonChange: PropTypes.any,
ModalClickDerechoondelay: PropTypes.any,
TextonesixTxtonClick: PropTypes.any,
TextonesixTxtonMouseEnter: PropTypes.any,
TextonesixTxtonMouseOver: PropTypes.any,
TextonesixTxtonKeyPress: PropTypes.any,
TextonesixTxtonDrag: PropTypes.any,
TextonesixTxtonMouseLeave: PropTypes.any,
TextonesixTxtonMouseUp: PropTypes.any,
TextonesixTxtonMouseDown: PropTypes.any,
TextonesixTxtonKeyDown: PropTypes.any,
TextonesixTxtonChange: PropTypes.any,
TextonesixTxtondelay: PropTypes.any
}
export default PropertyoneDefault;