import React, { Suspense } from 'react';
import PropTypes from 'prop-types';
import * as Contexts  from 'Contexts';
import { OverlaySwapContext } from 'Components/OverlaySwapProvider';
import CorebooksAnimation from 'Components/CorebooksAnimation';
import CorebooksAnimationSingle from 'Components/CorebooksAnimationSingle';

import * as UtilsScripts  from 'utils/utils';
import { Route, useHistory, useParams } from "react-router-dom";
import SingletoneNavigation from 'Class/SingletoneNavigation';

import LoadingComponent from "Components/Loading";


import { Transition } from "react-transition-group";
import { CSSTransition } from "react-transition-group";
import { useAppContext, useSessionContext } from 'context/AppContext';
import './Vector.css'





const Vector = (props)=>{
    const [state,dispatch] = useAppContext();
    const [, setNameSelectedComponent] = React.useContext(OverlaySwapContext);
    
    const nodeRef = React.useRef(null);
    const history = useHistory();
    let RouterContext = useParams();
    
    
    
    
    
    
    const [globalStateForm, dispatchForm] = React.useState({});
    
    const [ _in, setIn ] = React.useState(false)
    const [ transaction, setTransaction ] = React.useState({}) 
    const [ animation, setAnimation ] = React.useState('')

    
    
    
    
    
    
    React.useEffect(()=>{
        
        
        
    },[]);
    
    
    return (
        <>
  <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['vector']?.animationClass || {}}>

    <div id="id_fourthreeeight_sixthreetwoonigth" ref={nodeRef} className={` ${ props.onClick ? 'cursor' : '' } vector ${ props.cssClass } ${ transaction['vector']?.type ? transaction['vector']?.type.toLowerCase() : '' }`} style={ { ...{ ...{}, transitionDuration: transaction['vector']?.duration, transitionTimingFunction: transaction['vector']?.timingFunction }, ...props.style }} onClick={ props.VectoronClick } onMouseEnter={ props.VectoronMouseEnter } onMouseOver={ props.VectoronMouseOver } onKeyPress={ props.VectoronKeyPress } onDrag={ props.VectoronDrag } onMouseLeave={ props.VectoronMouseLeave } onMouseUp={ props.VectoronMouseUp } onMouseDown={ props.VectoronMouseDown } onKeyDown={ props.VectoronKeyDown } onChange={ props.VectoronChange } ondelay={ props.Vectorondelay }>
      {
      props.children ?
      props.children :
      <>


      </>
      }
    </div>

  </CSSTransition>
</>
    ) 
}

Vector.propTypes = {
    style: PropTypes.any,
VectoronClick: PropTypes.any,
VectoronMouseEnter: PropTypes.any,
VectoronMouseOver: PropTypes.any,
VectoronKeyPress: PropTypes.any,
VectoronDrag: PropTypes.any,
VectoronMouseLeave: PropTypes.any,
VectoronMouseUp: PropTypes.any,
VectoronMouseDown: PropTypes.any,
VectoronKeyDown: PropTypes.any,
VectoronChange: PropTypes.any,
Vectorondelay: PropTypes.any
}
export default Vector;