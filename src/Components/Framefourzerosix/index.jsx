import React, { Suspense } from 'react';
import PropTypes from 'prop-types';
import * as Contexts  from 'Contexts';
import { OverlaySwapContext } from 'Components/OverlaySwapProvider';
import CorebooksAnimation from 'Components/CorebooksAnimation';
import CorebooksAnimationSingle from 'Components/CorebooksAnimationSingle';

import * as UtilsScripts  from 'utils/utils';
import { Route, useHistory, useParams } from "react-router-dom";
import SingletoneNavigation from 'Class/SingletoneNavigation';

import LoadingComponent from "Components/Loading";


import { Transition } from "react-transition-group";
import { CSSTransition } from "react-transition-group";
import { useAppContext, useSessionContext } from 'context/AppContext';
import './Framefourzerosix.css'





const Framefourzerosix = (props)=>{
    const [state,dispatch] = useAppContext();
    const [, setNameSelectedComponent] = React.useContext(OverlaySwapContext);
    
    const nodeRef = React.useRef(null);
    const history = useHistory();
    let RouterContext = useParams();
    
    
    
    
    
    
    const [globalStateForm, dispatchForm] = React.useState({});
    
    const [ _in, setIn ] = React.useState(false)
    const [ transaction, setTransaction ] = React.useState({}) 
    const [ animation, setAnimation ] = React.useState('')

    
    
    
    
    
    
    React.useEffect(()=>{
        
        
        
    },[]);
    
    
    return (
        <>
  <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['framefourzerosix']?.animationClass || {}}>

    <div id="id_fivefivenigth_twoozerotwooseven" ref={nodeRef} className={` ${ props.onClick ? 'cursor' : '' } framefourzerosix ${ props.cssClass } ${ transaction['framefourzerosix']?.type ? transaction['framefourzerosix']?.type.toLowerCase() : '' }`} style={ { ...{ ...{}, transitionDuration: transaction['framefourzerosix']?.duration, transitionTimingFunction: transaction['framefourzerosix']?.timingFunction }, ...props.style }} onClick={ props.FramefourzerosixonClick } onMouseEnter={ props.FramefourzerosixonMouseEnter } onMouseOver={ props.FramefourzerosixonMouseOver } onKeyPress={ props.FramefourzerosixonKeyPress } onDrag={ props.FramefourzerosixonDrag } onMouseLeave={ props.FramefourzerosixonMouseLeave } onMouseUp={ props.FramefourzerosixonMouseUp } onMouseDown={ props.FramefourzerosixonMouseDown } onKeyDown={ props.FramefourzerosixonKeyDown } onChange={ props.FramefourzerosixonChange } ondelay={ props.Framefourzerosixondelay }>
      {
      props.children ?
      props.children :
      <>
        <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['grouptwoo']?.animationClass || {}}>

          <div id="id_fivefivenigth_twoozerotwooeight" className={` group grouptwoo ${ props.onClick ? 'cursor' : '' } ${ transaction['grouptwoo']?.type ? transaction['grouptwoo']?.type.toLowerCase() : '' }`} style={{ ...{},  ...props.GrouptwooStyle , transitionDuration: transaction['grouptwoo']?.duration, transitionTimingFunction: transaction['grouptwoo']?.timingFunction }} onClick={ props.GrouptwooonClick } onMouseEnter={ props.GrouptwooonMouseEnter } onMouseOver={ props.GrouptwooonMouseOver } onKeyPress={ props.GrouptwooonKeyPress } onDrag={ props.GrouptwooonDrag } onMouseLeave={ props.GrouptwooonMouseLeave } onMouseUp={ props.GrouptwooonMouseUp } onMouseDown={ props.GrouptwooonMouseDown } onKeyDown={ props.GrouptwooonKeyDown } onChange={ props.GrouptwooonChange } ondelay={ props.Grouptwooondelay }>
            <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['vector']?.animationClass || {}}>
              <svg id="id_fivefivenigth_twoozerotwoonigth" className="vector  " style={{}} onClick={ props.VectoronClick } onMouseEnter={ props.VectoronMouseEnter } onMouseOver={ props.VectoronMouseOver } onKeyPress={ props.VectoronKeyPress } onDrag={ props.VectoronDrag } onMouseLeave={ props.VectoronMouseLeave } onMouseUp={ props.VectoronMouseUp } onMouseDown={ props.VectoronMouseDown } onKeyDown={ props.VectoronKeyDown } onChange={ props.VectoronChange } ondelay={ props.Vectorondelay } width="133" height="40">
                <path d="M128.074 0L4.92593 0C2.20541 0 0 2.23858 0 5L0 35C0 37.7614 2.20541 40 4.92593 40L128.074 40C130.795 40 133 37.7614 133 35L133 5C133 2.23858 130.795 0 128.074 0Z" />
              </svg>
            </CSSTransition>
            <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['vector']?.animationClass || {}}>
              <svg id="id_fivefivenigth_twoozerothreezero" className="vector  " style={{}} onClick={ props.VectoronClick } onMouseEnter={ props.VectoronMouseEnter } onMouseOver={ props.VectoronMouseOver } onKeyPress={ props.VectoronKeyPress } onDrag={ props.VectoronDrag } onMouseLeave={ props.VectoronMouseLeave } onMouseUp={ props.VectoronMouseUp } onMouseDown={ props.VectoronMouseDown } onKeyDown={ props.VectoronKeyDown } onChange={ props.VectoronChange } ondelay={ props.Vectorondelay } width="133" height="40">
                <path d="M128.074 0.8C129.171 0.8 130.224 1.2425 131 2.03015C131.776 2.8178 132.212 3.88609 132.212 5L132.212 35C132.212 36.1139 131.776 37.1822 131 37.9698C130.224 38.7575 129.171 39.2 128.074 39.2L4.92593 39.2C3.82852 39.2 2.77606 38.7575 2.00008 37.9698C1.22409 37.1822 0.788148 36.1139 0.788148 35L0.788148 5C0.788148 3.88609 1.22409 2.8178 2.00008 2.03015C2.77606 1.2425 3.82852 0.8 4.92593 0.8L128.074 0.8ZM128.074 0L4.92593 0C3.61949 1.77636e-15 2.36656 0.526784 1.44277 1.46447C0.51898 2.40215 1.75004e-15 3.67392 0 5L0 35C1.75004e-15 36.3261 0.51898 37.5979 1.44277 38.5355C2.36656 39.4732 3.61949 40 4.92593 40L128.074 40C129.381 40 130.633 39.4732 131.557 38.5355C132.481 37.5979 133 36.3261 133 35L133 5C133 3.67392 132.481 2.40215 131.557 1.46447C130.633 0.526784 129.381 1.77636e-15 128.074 0Z" />
              </svg>
            </CSSTransition>
            <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['vector']?.animationClass || {}}>
              <svg id="id_fivefivenigth_twoozerothreeone" className="vector  " style={{}} onClick={ props.VectoronClick } onMouseEnter={ props.VectoronMouseEnter } onMouseOver={ props.VectoronMouseOver } onKeyPress={ props.VectoronKeyPress } onDrag={ props.VectoronDrag } onMouseLeave={ props.VectoronMouseLeave } onMouseUp={ props.VectoronMouseUp } onMouseDown={ props.VectoronMouseDown } onKeyDown={ props.VectoronKeyDown } onChange={ props.VectoronChange } ondelay={ props.Vectorondelay } width="80.562744140625" height="16.15478515625">
                <path d="M25.8597 4.51725C25.0634 4.521 24.2859 4.76099 23.6256 5.20691C22.9653 5.65283 22.4516 6.28471 22.1494 7.02279C21.8473 7.76086 21.7702 8.57207 21.9278 9.35401C22.0855 10.136 22.4708 10.8536 23.0353 11.4163C23.5997 11.9791 24.318 12.3618 25.0993 12.516C25.8806 12.6703 26.6901 12.5892 27.4255 12.2831C28.1609 11.9769 28.7893 11.4594 29.2313 10.7959C29.6734 10.1323 29.9093 9.35248 29.9093 8.55475C29.9171 8.02079 29.8175 7.49073 29.6164 6.99613C29.4154 6.50153 29.1171 6.0525 28.7393 5.67578C28.3614 5.29906 27.9118 5.00235 27.4171 4.80332C26.9224 4.60429 26.3927 4.507 25.8597 4.51725ZM25.8597 11.0058C25.37 11.0401 24.8812 10.926 24.457 10.6784C24.0328 10.4307 23.6928 10.061 23.4812 9.61727C23.2695 9.17351 23.1961 8.67623 23.2703 8.19009C23.3446 7.70395 23.5631 7.25142 23.8975 6.89134C24.2319 6.53125 24.6667 6.28026 25.1454 6.17099C25.6241 6.06172 26.1245 6.09922 26.5816 6.27863C27.0387 6.45804 27.4314 6.77105 27.7086 7.17697C27.9858 7.58289 28.1347 8.06295 28.1359 8.55475C28.1511 8.86636 28.104 9.17789 27.9971 9.47095C27.8903 9.76401 27.726 10.0327 27.5138 10.2611C27.3017 10.4896 27.0461 10.6731 26.762 10.801C26.4779 10.9289 26.1711 10.9985 25.8597 11.0058ZM17.0302 4.51725C16.2335 4.51913 15.4551 4.75752 14.7935 5.20231C14.1319 5.64709 13.6168 6.27832 13.3132 7.01624C13.0096 7.75416 12.9311 8.56566 13.0877 9.34824C13.2443 10.1308 13.6289 10.8493 14.193 11.413C14.7571 11.9767 15.4753 12.3603 16.2569 12.5154C17.0385 12.6704 17.8484 12.5899 18.5844 12.284C19.3203 11.9781 19.9493 11.4606 20.3917 10.7969C20.8342 10.1331 21.0704 9.35289 21.0704 8.55475C21.0781 8.02079 20.9785 7.49073 20.7775 6.99613C20.5765 6.50153 20.2781 6.0525 19.9003 5.67578C19.5224 5.29906 19.0728 5.00235 18.5781 4.80332C18.0834 4.60429 17.5538 4.507 17.0208 4.51725L17.0302 4.51725ZM17.0302 11.0058C16.5402 11.042 16.0507 10.9296 15.6253 10.6833C15.2 10.4369 14.8585 10.068 14.6454 9.62454C14.4323 9.18108 14.3574 8.68361 14.4304 8.19691C14.5035 7.71021 14.7211 7.25682 15.055 6.89576C15.3889 6.5347 15.8236 6.2827 16.3025 6.17256C16.7814 6.06242 17.2823 6.09925 17.74 6.27826C18.1977 6.45726 18.5909 6.77014 18.8686 7.17618C19.1463 7.58221 19.2955 8.06258 19.2969 8.55475C19.3122 8.86636 19.265 9.17789 19.1582 9.47095C19.0513 9.76401 18.887 10.0327 18.6749 10.2611C18.4627 10.4896 18.2071 10.6731 17.923 10.801C17.6389 10.9289 17.3321 10.9985 17.0208 11.0058L17.0302 11.0058ZM6.51263 5.76175L6.51263 7.47175L10.6097 7.47175C10.5454 8.27716 10.2112 9.03716 9.66127 9.62825C9.25138 10.0465 8.75862 10.3741 8.21471 10.5901C7.67079 10.8061 7.08777 10.9056 6.50314 10.8823C5.29581 10.8823 4.13792 10.4018 3.28421 9.54666C2.43049 8.69149 1.95088 7.53164 1.95088 6.32225C1.95088 5.11286 2.43049 3.95301 3.28421 3.09784C4.13792 2.24268 5.29581 1.76225 6.50314 1.76225C7.65808 1.74338 8.77335 2.1841 9.60437 2.98775L10.8088 1.78125C10.2478 1.21223 9.57852 0.761789 8.84055 0.456635C8.10258 0.151481 7.31097 -0.00218346 6.51263 0.00475027C5.66951 -0.0281177 4.82844 0.109845 4.03982 0.410371C3.2512 0.710898 2.53127 1.1678 1.92315 1.75371C1.31503 2.33961 0.831249 3.04246 0.500792 3.82014C0.170334 4.59781 0 5.43431 6.7387e-15 6.2795C0 7.12469 0.170334 7.96119 0.500792 8.73886C0.831249 9.51654 1.31503 10.2194 1.92315 10.8053C2.53127 11.3912 3.2512 11.8481 4.03982 12.1486C4.82844 12.4492 5.66951 12.5871 6.51263 12.5542C7.3244 12.591 8.13461 12.4532 8.88875 12.15C9.64289 11.8468 10.3235 11.3854 10.8847 10.7968C11.8783 9.70323 12.4088 8.26572 12.3642 6.78775C12.3672 6.42769 12.3355 6.06818 12.2693 5.71425L6.51263 5.76175ZM49.484 7.09175C49.2367 6.36285 48.7742 5.7264 48.1577 5.26666C47.5413 4.80692 46.7999 4.54558 46.0319 4.51725C45.5131 4.51623 44.9997 4.62181 44.5233 4.82746C44.0468 5.0331 43.6176 5.33444 43.262 5.71286C42.9065 6.09127 42.6322 6.53871 42.4561 7.02751C42.2801 7.51632 42.2059 8.03611 42.2384 8.55475C42.2268 9.08572 42.3223 9.61356 42.519 10.1068C42.7156 10.6 43.0095 11.0484 43.3831 11.4252C43.7567 11.8021 44.2023 12.0997 44.6932 12.3002C45.1842 12.5007 45.7104 12.6 46.2406 12.5922C46.904 12.5958 47.5579 12.4346 48.1438 12.1229C48.7297 11.8112 49.2293 11.3588 49.5979 10.8062L48.2227 9.85625C48.0174 10.199 47.7269 10.4825 47.3794 10.679C47.032 10.8755 46.6396 10.9783 46.2406 10.9772C45.8298 10.9929 45.4237 10.8844 45.0753 10.6659C44.7269 10.4473 44.4522 10.1288 44.2869 9.75175L49.6832 7.51925L49.484 7.09175ZM43.9834 8.44075C43.9642 8.14691 44.0037 7.85218 44.0996 7.57382C44.1955 7.29546 44.3459 7.03907 44.542 6.81966C44.738 6.60026 44.9758 6.42224 45.2415 6.29605C45.5071 6.16986 45.7951 6.09802 46.0888 6.08475C46.3947 6.06456 46.6998 6.13483 46.9662 6.28683C47.2326 6.43883 47.4486 6.66587 47.5873 6.93975L43.9834 8.44075ZM39.6018 12.3547L41.3753 12.3547L41.3753 0.47975L39.6018 0.47975L39.6018 12.3547ZM36.6998 5.41975L36.6334 5.41975C36.3661 5.1204 36.0387 4.88102 35.6726 4.71731C35.3065 4.55359 34.9099 4.46922 34.509 4.46975C33.4726 4.52009 32.4953 4.96795 31.7795 5.7205C31.0638 6.47306 30.6646 7.47261 30.6646 8.512C30.6646 9.5514 31.0638 10.5509 31.7795 11.3035C32.4953 12.056 33.4726 12.5039 34.509 12.5542C34.9109 12.561 35.3094 12.4795 35.6764 12.3153C36.0435 12.1512 36.3701 11.9085 36.6334 11.6042L36.6903 11.6042L36.6903 12.1837C36.6903 13.7322 35.8652 14.5587 34.5375 14.5587C34.0952 14.5484 33.6661 14.4064 33.3047 14.151C32.9433 13.8955 32.6659 13.5381 32.5079 13.1243L30.962 13.7703C31.253 14.4793 31.7489 15.0851 32.3859 15.5099C33.0229 15.9348 33.7722 16.1593 34.5375 16.1548C36.6144 16.1548 38.331 14.9292 38.331 11.9462L38.331 4.75475L36.6998 4.75475L36.6998 5.41975ZM34.6702 11.0058C34.0585 10.9517 33.4891 10.6701 33.0744 10.2165C32.6596 9.76278 32.4296 9.16992 32.4296 8.55475C32.4296 7.93958 32.6596 7.34672 33.0744 6.89304C33.4891 6.43935 34.0585 6.15775 34.6702 6.10375C34.9736 6.11934 35.2707 6.19559 35.5441 6.32798C35.8176 6.46038 36.0618 6.64626 36.2624 6.87467C36.4631 7.10308 36.616 7.3694 36.7123 7.65794C36.8086 7.94648 36.8463 8.2514 36.8231 8.55475C36.8489 8.85941 36.8129 9.16616 36.717 9.45645C36.6212 9.74674 36.4675 10.0145 36.2654 10.2436C36.0633 10.4728 35.8168 10.6584 35.541 10.7894C35.2652 10.9204 34.9658 10.994 34.6608 11.0058L34.6702 11.0058ZM57.7919 0.47975L53.5526 0.47975L53.5526 12.3547L55.3261 12.3547L55.3261 7.85175L57.8014 7.85175C58.3068 7.88812 58.8144 7.8198 59.2923 7.65104C59.7702 7.48228 60.2083 7.2167 60.5792 6.87087C60.9502 6.52503 61.246 6.10636 61.4483 5.64094C61.6506 5.17553 61.755 4.67336 61.755 4.16575C61.755 3.65814 61.6506 3.15597 61.4483 2.69056C61.246 2.22515 60.9502 1.80647 60.5792 1.46063C60.2083 1.1148 59.7702 0.84922 59.2923 0.68046C58.8144 0.5117 58.3068 0.443376 57.8014 0.47975L57.7919 0.47975ZM57.7919 6.17975L55.3166 6.17975L55.3166 2.13275L57.8299 2.13275C58.3694 2.13275 58.8868 2.34744 59.2683 2.72959C59.6498 3.11175 59.8641 3.63006 59.8641 4.1705C59.8641 4.71095 59.6498 5.22926 59.2683 5.61141C58.8868 5.99356 58.3694 6.20825 57.8299 6.20825L57.7919 6.17975ZM68.7268 4.46975C68.0812 4.43109 67.4383 4.58223 66.8773 4.90459C66.3162 5.22694 65.8614 5.70648 65.5687 6.28425L67.143 6.93975C67.2982 6.65318 67.5333 6.418 67.8195 6.26283C68.1058 6.10765 68.4309 6.03918 68.7553 6.06575C68.9804 6.03941 69.2085 6.05825 69.4263 6.12116C69.6441 6.18407 69.8472 6.2898 70.0238 6.43217C70.2003 6.57455 70.3468 6.75072 70.4547 6.95041C70.5626 7.1501 70.6297 7.36931 70.652 7.59525L70.652 7.71875C70.0824 7.41665 69.4472 7.26002 68.8027 7.26275C67.1051 7.26275 65.3885 8.21275 65.3885 9.93225C65.4053 10.2989 65.4952 10.6584 65.6531 10.9897C65.811 11.3209 66.0335 11.617 66.3076 11.8606C66.5817 12.1041 66.9018 12.2902 67.2489 12.4077C67.596 12.5252 67.9631 12.5718 68.3285 12.5448C68.7751 12.5769 69.2221 12.488 69.6226 12.2874C70.0231 12.0868 70.3623 11.7819 70.6046 11.4047L70.6615 11.4047L70.6615 12.3547L72.3686 12.3547L72.3686 7.78525C72.3686 5.70475 70.7943 4.49825 68.7742 4.49825L68.7268 4.46975ZM68.5087 10.9772C67.9302 10.9772 67.124 10.6828 67.124 9.97025C67.124 9.02025 68.1293 8.70675 69.0208 8.70675C69.5842 8.69282 70.141 8.83062 70.6331 9.10575C70.5738 9.62008 70.3309 10.0956 69.9493 10.4448C69.5676 10.7939 69.0728 10.9931 68.5561 11.0058L68.5087 10.9772ZM78.59 4.75475L76.5605 9.90375L76.5036 9.90375L74.3982 4.75475L72.5014 4.75475L75.6595 11.9558L73.8576 15.9553L75.7069 15.9553L80.5627 4.75475L78.59 4.75475ZM62.6477 12.3547L64.4211 12.3547L64.4211 0.47975L62.6477 0.47975L62.6477 12.3547Z" />
              </svg>
            </CSSTransition>
            <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['vector']?.animationClass || {}}>
              <svg id="id_fivefivenigth_twoozerothreetwoo" className="vector  " style={{}} onClick={ props.VectoronClick } onMouseEnter={ props.VectoronMouseEnter } onMouseOver={ props.VectoronMouseOver } onKeyPress={ props.VectoronKeyPress } onDrag={ props.VectoronDrag } onMouseLeave={ props.VectoronMouseLeave } onMouseUp={ props.VectoronMouseUp } onMouseDown={ props.VectoronMouseDown } onKeyDown={ props.VectoronKeyDown } onChange={ props.VectoronChange } ondelay={ props.Vectorondelay } width="12.256591796875" height="23.806884765625">
                <path d="M0.439656 0.0665015C0.130544 0.438054 -0.0254776 0.913706 0.00339731 1.3965L0.00339731 22.4105C-0.0254776 22.8933 0.130544 23.3689 0.439656 23.7405L0.506043 23.807L12.2566 12.046L12.2566 11.7705L0.506043 0L0.439656 0.0665015Z" />
              </svg>
            </CSSTransition>
            <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['vector']?.animationClass || {}}>
              <svg id="id_fivefivenigth_twoozerothreethree" className="vector  " style={{}} onClick={ props.VectoronClick } onMouseEnter={ props.VectoronMouseEnter } onMouseOver={ props.VectoronMouseOver } onKeyPress={ props.VectoronKeyPress } onDrag={ props.VectoronDrag } onMouseLeave={ props.VectoronMouseLeave } onMouseUp={ props.VectoronMouseUp } onMouseDown={ props.VectoronMouseDown } onKeyDown={ props.VectoronKeyDown } onChange={ props.VectoronChange } ondelay={ props.Vectorondelay } width="9.626220703125" height="8.132080078125">
                <path d="M3.88839 8.132L0 4.2085L0 3.933L3.88839 0L3.97374 0.0475011L8.63032 2.698C9.95806 3.4485 9.95806 4.6835 8.63032 5.4435L3.99271 8.0845L3.88839 8.132Z" />
              </svg>
            </CSSTransition>
            <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['vector']?.animationClass || {}}>
              <svg id="id_fivefivenigth_twoozerothreefour" className="vector  " style={{}} onClick={ props.VectoronClick } onMouseEnter={ props.VectoronMouseEnter } onMouseOver={ props.VectoronMouseOver } onKeyPress={ props.VectoronKeyPress } onDrag={ props.VectoronDrag } onMouseLeave={ props.VectoronMouseLeave } onMouseUp={ props.VectoronMouseUp } onMouseDown={ props.VectoronMouseDown } onKeyDown={ props.VectoronKeyDown } onChange={ props.VectoronChange } ondelay={ props.Vectorondelay } width="15.82861328125" height="12.222412109375">
                <path d="M15.8191 4.009L11.8169 0L0 11.837C0.270634 12.0753 0.615874 12.2114 0.97606 12.2218C1.33625 12.2322 1.68875 12.1163 1.97265 11.894L15.8286 4.009" />
              </svg>
            </CSSTransition>
            <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['vector']?.animationClass || {}}>
              <svg id="id_fivefivenigth_twoozerothreefive" className="vector  " style={{}} onClick={ props.VectoronClick } onMouseEnter={ props.VectoronMouseEnter } onMouseOver={ props.VectoronMouseOver } onKeyPress={ props.VectoronKeyPress } onDrag={ props.VectoronDrag } onMouseLeave={ props.VectoronMouseLeave } onMouseUp={ props.VectoronMouseUp } onMouseDown={ props.VectoronMouseDown } onKeyDown={ props.VectoronKeyDown } onChange={ props.VectoronChange } ondelay={ props.Vectorondelay } width="15.82861328125" height="12.222412109375">
                <path d="M15.8286 8.21348L1.97265 0.328482C1.68875 0.106196 1.33625 -0.00976336 0.976062 0.000644293C0.615876 0.0110519 0.270634 0.147174 0 0.385484L11.8264 12.2225L15.8286 8.21348Z" />
              </svg>
            </CSSTransition>
            <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['vector']?.animationClass || {}}>
              <svg id="id_fivefivenigth_twoozerothreesix" className="vector  " style={{}} onClick={ props.VectoronClick } onMouseEnter={ props.VectoronMouseEnter } onMouseOver={ props.VectoronMouseOver } onKeyPress={ props.VectoronKeyPress } onDrag={ props.VectoronDrag } onMouseLeave={ props.VectoronMouseLeave } onMouseUp={ props.VectoronMouseUp } onMouseDown={ props.VectoronMouseDown } onKeyDown={ props.VectoronKeyDown } onChange={ props.VectoronChange } ondelay={ props.Vectorondelay } width="15.819091796875" height="8.288818359375">
                <path d="M15.7053 0L1.96316 7.8375C1.68935 8.04257 1.35665 8.15338 1.01477 8.15338C0.672896 8.15338 0.340196 8.04257 0.0663868 7.8375L0 7.904L0.0663868 7.9705C0.339575 8.17703 0.672513 8.28876 1.01477 8.28876C1.35703 8.28876 1.68997 8.17703 1.96316 7.9705L15.8191 0.0855001L15.7053 0Z" />
              </svg>
            </CSSTransition>
            <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['vector']?.animationClass || {}}>
              <svg id="id_fivefivenigth_twoozerothreeseven" className="vector  " style={{}} onClick={ props.VectoronClick } onMouseEnter={ props.VectoronMouseEnter } onMouseOver={ props.VectoronMouseOver } onKeyPress={ props.VectoronKeyPress } onDrag={ props.VectoronDrag } onMouseLeave={ props.VectoronMouseLeave } onMouseUp={ props.VectoronMouseUp } onMouseDown={ props.VectoronMouseDown } onKeyDown={ props.VectoronKeyDown } onChange={ props.VectoronChange } ondelay={ props.Vectorondelay } width="21.835205078125" height="11.846435546875">
                <path d="M0.439656 11.704C0.130544 11.3324 -0.0254776 10.8568 0.00339731 10.374L0.00339731 10.5165C-0.0254776 10.9993 0.130544 11.4749 0.439656 11.8465L0.506043 11.78L0.439656 11.704ZM20.8869 1.235L16.1449 3.9235L16.2303 4.009L20.8869 1.368C21.1511 1.24367 21.3779 1.05173 21.5444 0.811543C21.7109 0.571359 21.8113 0.291428 21.8353 0C21.7819 0.264772 21.6687 0.513772 21.5043 0.727866C21.3399 0.941959 21.1287 1.11545 20.8869 1.235Z" />
              </svg>
            </CSSTransition>
            <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['vector']?.animationClass || {}}>
              <svg id="id_fivefivenigth_twoozerothreeeight" className="vector  " style={{}} onClick={ props.VectoronClick } onMouseEnter={ props.VectoronMouseEnter } onMouseOver={ props.VectoronMouseOver } onKeyPress={ props.VectoronKeyPress } onDrag={ props.VectoronDrag } onMouseLeave={ props.VectoronMouseLeave } onMouseUp={ props.VectoronMouseUp } onMouseDown={ props.VectoronMouseDown } onKeyDown={ props.VectoronKeyDown } onChange={ props.VectoronChange } ondelay={ props.Vectorondelay } width="21.84130859375" height="12.211181640625">
                <path d="M2.4089 0.450245L20.893 10.9762C21.1348 11.0958 21.346 11.2693 21.5104 11.4834C21.6748 11.6975 21.788 11.9465 21.8414 12.2112C21.8173 11.9198 21.717 11.6399 21.5505 11.3997C21.384 11.1595 21.1572 10.9676 20.893 10.8432L2.4089 0.317244C1.08116 -0.433256 0 0.193745 0 1.71375L0 1.85624C0.0284516 0.326745 1.09065 -0.300255 2.4089 0.450245Z" />
              </svg>
            </CSSTransition>
            <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['vector']?.animationClass || {}}>
              <svg id="id_fivefivenigth_twoozerothreenigth" className="vector  " style={{}} onClick={ props.VectoronClick } onMouseEnter={ props.VectoronMouseEnter } onMouseOver={ props.VectoronMouseOver } onKeyPress={ props.VectoronKeyPress } onDrag={ props.VectoronDrag } onMouseLeave={ props.VectoronMouseLeave } onMouseUp={ props.VectoronMouseUp } onMouseDown={ props.VectoronMouseDown } onKeyDown={ props.VectoronKeyDown } onChange={ props.VectoronChange } ondelay={ props.Vectorondelay } width="55.395263671875" height="5.962158203125">
                <path d="M0 5.83867L0 0.138669L1.73555 0.138669C2.11998 0.121283 2.50394 0.182197 2.8642 0.317727C3.22445 0.453257 3.55352 0.660585 3.83148 0.927169C4.09732 1.1984 4.30509 1.52114 4.44208 1.87561C4.57906 2.23008 4.64237 2.60883 4.62813 2.98867C4.64361 3.36783 4.5818 3.74618 4.44646 4.10061C4.31112 4.45505 4.1051 4.77814 3.84097 5.05017C3.56301 5.31675 3.23394 5.52408 2.87368 5.65961C2.51343 5.79514 2.12947 5.85606 1.74503 5.83867L0 5.83867ZM0.730258 5.13567L1.73555 5.13567C2.02121 5.15057 2.30694 5.10779 2.57576 5.00987C2.84458 4.91195 3.09101 4.76089 3.30039 4.56567C3.49908 4.35622 3.6529 4.10827 3.75238 3.83707C3.85187 3.56587 3.89493 3.27715 3.8789 2.98867C3.90324 2.70139 3.86483 2.41224 3.76633 2.14134C3.66784 1.87043 3.51163 1.62426 3.30856 1.41994C3.10548 1.21562 2.86043 1.05805 2.59042 0.958188C2.32041 0.858326 2.03193 0.818564 1.74503 0.841669L0.730258 0.841669L0.730258 5.13567ZM5.65239 5.83867L5.65239 0.138669L6.37316 0.138669L6.37316 5.83867L5.65239 5.83867ZM9.2942 5.96217C8.85724 5.95432 8.43332 5.81167 8.08026 5.55367C7.68749 5.2848 7.40992 4.87791 7.30258 4.41367L7.96645 4.14767C8.04252 4.45423 8.21148 4.72967 8.45013 4.93617C8.67813 5.14472 8.97596 5.25998 9.28471 5.25917C9.57629 5.26883 9.86293 5.18202 10.1003 5.01217C10.2098 4.9359 10.2985 4.83331 10.3582 4.7138C10.4178 4.5943 10.4466 4.4617 10.4417 4.32817C10.4478 4.18772 10.4199 4.0479 10.3604 3.92055C10.301 3.79321 10.2118 3.68211 10.1003 3.59667C9.77302 3.37562 9.41065 3.2118 9.02865 3.11217C8.6036 2.998 8.21079 2.78663 7.8811 2.49467C7.75491 2.37134 7.6553 2.22342 7.58842 2.06002C7.52155 1.89663 7.48882 1.72123 7.49226 1.54467C7.49258 1.34304 7.5359 1.14381 7.61931 0.960312C7.70273 0.776813 7.82431 0.613283 7.97594 0.48067C8.31436 0.17037 8.75968 0.00352067 9.21832 0.0151699C9.64016 -0.00398078 10.0556 0.123557 10.3943 0.376169C10.6649 0.573957 10.8697 0.848695 10.9823 1.16467L10.3185 1.44017C10.2604 1.23805 10.1369 1.06096 9.96755 0.936669C9.75619 0.784364 9.5024 0.702424 9.24203 0.702424C8.98167 0.702424 8.72788 0.784364 8.51652 0.936669C8.42382 1.00391 8.34869 1.09256 8.29751 1.1951C8.24633 1.29764 8.2206 1.41104 8.22252 1.52567C8.22212 1.63132 8.24686 1.73555 8.29469 1.82972C8.34252 1.92389 8.41206 2.00529 8.49755 2.06717C8.74214 2.24207 9.01477 2.37381 9.30368 2.45667C9.54228 2.53246 9.77657 2.62125 10.0055 2.72267C10.2065 2.81664 10.3972 2.93126 10.5745 3.06467C10.7622 3.19859 10.9124 3.37849 11.0108 3.58717C11.1149 3.81672 11.1667 4.06658 11.1625 4.31867C11.1672 4.57596 11.1086 4.83045 10.9918 5.05967C10.8854 5.26617 10.7288 5.44257 10.5366 5.57267C10.3502 5.70085 10.1456 5.80014 9.92961 5.86717C9.72336 5.92877 9.50942 5.96075 9.2942 5.96217ZM13.0119 5.83867L12.2816 5.83867L12.2816 0.138669L14.1784 0.138669C14.6424 0.132677 15.0908 0.306226 15.4303 0.623169C15.5981 0.777979 15.7321 0.96597 15.8237 1.17527C15.9153 1.38457 15.9626 1.61063 15.9626 1.83917C15.9626 2.06771 15.9153 2.29377 15.8237 2.50307C15.7321 2.71237 15.5981 2.90036 15.4303 3.05517C15.0914 3.37296 14.6426 3.54665 14.1784 3.53967L13.0119 3.53967L13.0119 5.83867ZM13.0119 2.82717L14.2353 2.82717C14.374 2.83232 14.5121 2.80691 14.6399 2.75274C14.7677 2.69857 14.8821 2.61696 14.975 2.51367C15.0668 2.42505 15.1398 2.3188 15.1897 2.20128C15.2396 2.08376 15.2653 1.95737 15.2653 1.82967C15.2653 1.70197 15.2396 1.57558 15.1897 1.45806C15.1398 1.34054 15.0668 1.23429 14.975 1.14567C14.8821 1.04238 14.7677 0.960768 14.6399 0.906599C14.5121 0.85243 14.374 0.827022 14.2353 0.832169L13.0119 0.832169L13.0119 2.82717ZM21.5474 5.09767C20.9929 5.64995 20.2428 5.95996 19.4609 5.95996C18.679 5.95996 17.9289 5.64995 17.3745 5.09767C16.8356 4.52627 16.5353 3.76999 16.5353 2.98392C16.5353 2.19785 16.8356 1.44157 17.3745 0.87017C17.6477 0.594426 17.9728 0.375576 18.3309 0.226231C18.689 0.076887 19.073 0 19.4609 0C19.8488 0 20.2328 0.076887 20.5909 0.226231C20.949 0.375576 21.2741 0.594426 21.5474 0.87017C22.0884 1.44048 22.3901 2.19717 22.3901 2.98392C22.3901 3.77067 22.0884 4.52736 21.5474 5.09767ZM17.915 4.62267C18.3256 5.03224 18.8815 5.26218 19.4609 5.26218C20.0403 5.26218 20.5962 5.03224 21.0068 4.62267C21.412 4.17524 21.6364 3.59276 21.6364 2.98867C21.6364 2.38457 21.412 1.8021 21.0068 1.35467C20.5962 0.945103 20.0403 0.715155 19.4609 0.715155C18.8815 0.715155 18.3256 0.945103 17.915 1.35467C17.5098 1.8021 17.2854 2.38457 17.2854 2.98867C17.2854 3.59276 17.5098 4.17524 17.915 4.62267ZM23.4441 5.83867L23.4441 0.138669L24.3356 0.138669L27.1049 4.57517L27.1049 0.138669L27.8352 0.138669L27.8352 5.83867L27.0765 5.83867L24.1839 1.19317L24.1839 5.83867L23.4441 5.83867ZM29.1345 5.83867L29.1345 0.138669L29.8647 0.138669L29.8647 5.83867L29.1345 5.83867ZM31.1261 5.83867L31.1261 0.138669L33.1841 0.138669C33.6114 0.130585 34.0255 0.286995 34.3411 0.575668C34.4978 0.714326 34.6224 0.885688 34.7059 1.07771C34.7895 1.26974 34.8301 1.47776 34.8248 1.68717C34.8292 1.9404 34.7564 2.18897 34.6161 2.39967C34.4769 2.60433 34.283 2.76564 34.0566 2.86517C34.3317 2.95639 34.5729 3.12854 34.7489 3.35917C34.9313 3.59192 35.0284 3.88032 35.0239 4.17617C35.0288 4.39517 34.9854 4.61256 34.8969 4.81287C34.8084 5.01319 34.6769 5.19151 34.5118 5.33517C34.1816 5.6403 33.7471 5.80691 33.2979 5.80067L31.1261 5.83867ZM31.8563 2.58017L33.1841 2.58017C33.3081 2.58535 33.4317 2.56339 33.5464 2.51581C33.661 2.46823 33.7639 2.39618 33.8479 2.30467C33.9313 2.22775 33.9983 2.13481 34.045 2.03139C34.0917 1.92797 34.1171 1.81619 34.1198 1.70272C34.1224 1.58924 34.1023 1.47639 34.0604 1.37089C34.0186 1.2654 33.956 1.16941 33.8764 1.08867C33.7971 0.998869 33.6991 0.927637 33.5893 0.880025C33.4795 0.832413 33.3606 0.809588 33.241 0.813168L31.8563 0.813168L31.8563 2.58017ZM31.8563 5.13567L33.3263 5.13567C33.4548 5.13999 33.5826 5.11583 33.7006 5.06492C33.8187 5.01401 33.924 4.9376 34.0092 4.84117C34.1764 4.66744 34.2713 4.43647 34.2747 4.19517C34.2747 3.94321 34.1748 3.70158 33.9969 3.52342C33.8191 3.34526 33.5779 3.24517 33.3263 3.24517L31.8563 3.24517L31.8563 5.13567ZM36.0008 5.83867L36.0008 0.138669L36.7215 0.138669L36.7215 5.13567L39.1874 5.13567L39.1874 5.83867L36.0008 5.83867ZM43.4456 0.841669L40.847 0.841669L40.847 2.64667L43.1801 2.64667L43.1801 3.33067L40.847 3.33067L40.847 5.13567L43.4361 5.13567L43.4361 5.83867L40.1168 5.83867L40.1168 0.138669L43.4361 0.138669L43.4456 0.841669ZM49.8188 0.841669L47.2297 0.841669L47.2297 2.64667L49.5627 2.64667L49.5627 3.33067L47.2297 3.33067L47.2297 5.13567L49.8188 5.13567L49.8188 5.83867L46.4994 5.83867L46.4994 0.138669L49.8188 0.138669L49.8188 0.841669ZM50.9474 5.83867L50.9474 0.138669L51.8957 0.138669L54.665 4.57517L54.665 0.138669L55.3953 0.138669L55.3953 5.83867L54.6366 5.83867L51.744 1.19317L51.744 5.83867L50.9474 5.83867Z" />
              </svg>
            </CSSTransition>
          </div>

        </CSSTransition>

      </>
      }
    </div>

  </CSSTransition>
</>
    ) 
}

Framefourzerosix.propTypes = {
    style: PropTypes.any,
FramefourzerosixonClick: PropTypes.any,
FramefourzerosixonMouseEnter: PropTypes.any,
FramefourzerosixonMouseOver: PropTypes.any,
FramefourzerosixonKeyPress: PropTypes.any,
FramefourzerosixonDrag: PropTypes.any,
FramefourzerosixonMouseLeave: PropTypes.any,
FramefourzerosixonMouseUp: PropTypes.any,
FramefourzerosixonMouseDown: PropTypes.any,
FramefourzerosixonKeyDown: PropTypes.any,
FramefourzerosixonChange: PropTypes.any,
Framefourzerosixondelay: PropTypes.any,
GrouptwooonClick: PropTypes.any,
GrouptwooonMouseEnter: PropTypes.any,
GrouptwooonMouseOver: PropTypes.any,
GrouptwooonKeyPress: PropTypes.any,
GrouptwooonDrag: PropTypes.any,
GrouptwooonMouseLeave: PropTypes.any,
GrouptwooonMouseUp: PropTypes.any,
GrouptwooonMouseDown: PropTypes.any,
GrouptwooonKeyDown: PropTypes.any,
GrouptwooonChange: PropTypes.any,
Grouptwooondelay: PropTypes.any,
VectoronClick: PropTypes.any,
VectoronMouseEnter: PropTypes.any,
VectoronMouseOver: PropTypes.any,
VectoronKeyPress: PropTypes.any,
VectoronDrag: PropTypes.any,
VectoronMouseLeave: PropTypes.any,
VectoronMouseUp: PropTypes.any,
VectoronMouseDown: PropTypes.any,
VectoronKeyDown: PropTypes.any,
VectoronChange: PropTypes.any,
Vectorondelay: PropTypes.any
}
export default Framefourzerosix;