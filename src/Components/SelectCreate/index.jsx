import React, { Suspense } from 'react';
import PropTypes from 'prop-types';
import * as Contexts  from 'Contexts';
import { OverlaySwapContext } from 'Components/OverlaySwapProvider';
import CorebooksAnimation from 'Components/CorebooksAnimation';
import CorebooksAnimationSingle from 'Components/CorebooksAnimationSingle';

import * as UtilsScripts  from 'utils/utils';
import { Route, useHistory, useParams } from "react-router-dom";
import SingletoneNavigation from 'Class/SingletoneNavigation';

import LoadingComponent from "Components/Loading";


import { Transition } from "react-transition-group";
import { CSSTransition } from "react-transition-group";
import Rectanglefour from 'Components/Rectanglefour'
import { useAppContext, useSessionContext } from 'context/AppContext';
import './SelectCreate.css'





const SelectCreate = (props)=>{
    const [state,dispatch] = useAppContext();
    const [, setNameSelectedComponent] = React.useContext(OverlaySwapContext);
    
    const nodeRef = React.useRef(null);
    const history = useHistory();
    let RouterContext = useParams();
    
    
    
    
    
    
    const [globalStateForm, dispatchForm] = React.useState({});
    
    const [ _in, setIn ] = React.useState(false)
    const [ transaction, setTransaction ] = React.useState({}) 
    const [ animation, setAnimation ] = React.useState('')

    
    
    
    
    
    
    React.useEffect(()=>{
        
        
        
    },[]);
    
    
    return (
        <>
  <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['selectcreate']?.animationClass || {}}>

    <div id="id_fourthreefour_oneeighttwoothree" ref={nodeRef} className={` ${ props.onClick ? 'cursor' : '' } selectcreate C_fourthreefour_oneeighttwoothree ${ props.cssClass } ${ transaction['selectcreate']?.type ? transaction['selectcreate']?.type.toLowerCase() : '' }`} style={ { ...{ ...{}, transitionDuration: transaction['selectcreate']?.duration, transitionTimingFunction: transaction['selectcreate']?.timingFunction }, ...props.style }} onClick={ props.SelectCreateonClick } onMouseEnter={ props.SelectCreateonMouseEnter } onMouseOver={ props.SelectCreateonMouseOver } onKeyPress={ props.SelectCreateonKeyPress } onDrag={ props.SelectCreateonDrag } onMouseLeave={ props.SelectCreateonMouseLeave } onMouseUp={ props.SelectCreateonMouseUp } onMouseDown={ props.SelectCreateonMouseDown } onKeyDown={ props.SelectCreateonKeyDown } onChange={ props.SelectCreateonChange } ondelay={ props.SelectCreateondelay }>
      {
      props.children ?
      props.children :
      <>
        <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['rectanglefive']?.animationClass || {}}>
          <Rectanglefour { ...{ ...props, style:false } } variant={'Property 1=Default'} cssClass={"C_fourthreeeight_onefivetwooeight cursor "}  />
    </CSSTransition >
<CSSTransition 
        in={_in}
        appear={true} 
        timeout={100}
        classNames={transaction['framethreefiveseven']?.animationClass || {}}
    >
    
                    <div id="id_fourthreefour_oneeighttwoozero" className={` frame framethreefiveseven ${ props.onClick ? 'cursor' : '' } ${ transaction['framethreefiveseven']?.type ? transaction['framethreefiveseven']?.type.toLowerCase() : '' }`} style={ { ...{}, ...props.FramethreefivesevenStyle , transitionDuration: transaction['framethreefiveseven']?.duration, transitionTimingFunction: transaction['framethreefiveseven']?.timingFunction } } onClick={ props.FramethreefivesevenonClick } onMouseEnter={ props.FramethreefivesevenonMouseEnter } onMouseOver={ props.FramethreefivesevenonMouseOver } onKeyPress={ props.FramethreefivesevenonKeyPress } onDrag={ props.FramethreefivesevenonDrag } onMouseLeave={ props.FramethreefivesevenonMouseLeave } onMouseUp={ props.FramethreefivesevenonMouseUp } onMouseDown={ props.FramethreefivesevenonMouseDown } onKeyDown={ props.FramethreefivesevenonKeyDown } onChange={ props.FramethreefivesevenonChange } ondelay={ props.Framethreefivesevenondelay }>
            <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['prefieronorecibirpublicidaddespotify']?.animationClass || {}}>

              <span id="id_fourthreefour_oneeighttwooone"  className={` text prefieronorecibirpublicidaddespotify    ${ props.onClick ? 'cursor' : ''}  ${ transaction['prefieronorecibirpublicidaddespotify']?.type ? transaction['prefieronorecibirpublicidaddespotify']?.type.toLowerCase() : '' }`} style={{ ...{},  ...props.PrefieronorecibirpublicidaddeSpotifyStyle , transitionDuration: transaction['prefieronorecibirpublicidaddespotify']?.duration, transitionTimingFunction: transaction['prefieronorecibirpublicidaddespotify']?.timingFunction }} onClick={ props.PrefieronorecibirpublicidaddeSpotifyonClick } onMouseEnter={ props.PrefieronorecibirpublicidaddeSpotifyonMouseEnter } onMouseOver={ props.PrefieronorecibirpublicidaddeSpotifyonMouseOver } onKeyPress={ props.PrefieronorecibirpublicidaddeSpotifyonKeyPress } onDrag={ props.PrefieronorecibirpublicidaddeSpotifyonDrag } onMouseLeave={ props.PrefieronorecibirpublicidaddeSpotifyonMouseLeave } onMouseUp={ props.PrefieronorecibirpublicidaddeSpotifyonMouseUp } onMouseDown={ props.PrefieronorecibirpublicidaddeSpotifyonMouseDown } onKeyDown={ props.PrefieronorecibirpublicidaddeSpotifyonKeyDown } onChange={ props.PrefieronorecibirpublicidaddeSpotifyonChange } ondelay={ props.PrefieronorecibirpublicidaddeSpotifyondelay } >{props.PrefieronorecibirpublicidaddeSpotify0 || `Prefiero no recibir publicidad de Spotify`}</span>

            </CSSTransition>
    </div>

  </CSSTransition>

</>
}
</div>

</CSSTransition>
</>
    ) 
}

SelectCreate.propTypes = {
    style: PropTypes.any,
PrefieronorecibirpublicidaddeSpotify0: PropTypes.any,
SelectCreateonClick: PropTypes.any,
SelectCreateonMouseEnter: PropTypes.any,
SelectCreateonMouseOver: PropTypes.any,
SelectCreateonKeyPress: PropTypes.any,
SelectCreateonDrag: PropTypes.any,
SelectCreateonMouseLeave: PropTypes.any,
SelectCreateonMouseUp: PropTypes.any,
SelectCreateonMouseDown: PropTypes.any,
SelectCreateonKeyDown: PropTypes.any,
SelectCreateonChange: PropTypes.any,
SelectCreateondelay: PropTypes.any,
FramethreefivesevenonClick: PropTypes.any,
FramethreefivesevenonMouseEnter: PropTypes.any,
FramethreefivesevenonMouseOver: PropTypes.any,
FramethreefivesevenonKeyPress: PropTypes.any,
FramethreefivesevenonDrag: PropTypes.any,
FramethreefivesevenonMouseLeave: PropTypes.any,
FramethreefivesevenonMouseUp: PropTypes.any,
FramethreefivesevenonMouseDown: PropTypes.any,
FramethreefivesevenonKeyDown: PropTypes.any,
FramethreefivesevenonChange: PropTypes.any,
Framethreefivesevenondelay: PropTypes.any,
PrefieronorecibirpublicidaddeSpotifyonClick: PropTypes.any,
PrefieronorecibirpublicidaddeSpotifyonMouseEnter: PropTypes.any,
PrefieronorecibirpublicidaddeSpotifyonMouseOver: PropTypes.any,
PrefieronorecibirpublicidaddeSpotifyonKeyPress: PropTypes.any,
PrefieronorecibirpublicidaddeSpotifyonDrag: PropTypes.any,
PrefieronorecibirpublicidaddeSpotifyonMouseLeave: PropTypes.any,
PrefieronorecibirpublicidaddeSpotifyonMouseUp: PropTypes.any,
PrefieronorecibirpublicidaddeSpotifyonMouseDown: PropTypes.any,
PrefieronorecibirpublicidaddeSpotifyonKeyDown: PropTypes.any,
PrefieronorecibirpublicidaddeSpotifyonChange: PropTypes.any,
PrefieronorecibirpublicidaddeSpotifyondelay: PropTypes.any
}
export default SelectCreate;