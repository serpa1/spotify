import React, { Suspense } from 'react';
import PropTypes from 'prop-types';
import * as Contexts  from 'Contexts';
import { OverlaySwapContext } from 'Components/OverlaySwapProvider';
import CorebooksAnimation from 'Components/CorebooksAnimation';
import CorebooksAnimationSingle from 'Components/CorebooksAnimationSingle';

import * as UtilsScripts  from 'utils/utils';
import { Route, useHistory, useParams } from "react-router-dom";
import SingletoneNavigation from 'Class/SingletoneNavigation';

import LoadingComponent from "Components/Loading";


import { Transition } from "react-transition-group";
import { CSSTransition } from "react-transition-group";
import { useAppContext, useSessionContext } from 'context/AppContext';
import './Framefouronetwoo.css'





const Framefouronetwoo = (props)=>{
    const [state,dispatch] = useAppContext();
    const [, setNameSelectedComponent] = React.useContext(OverlaySwapContext);
    
    const nodeRef = React.useRef(null);
    const history = useHistory();
    let RouterContext = useParams();
    
    
    
    
    
    
    const [globalStateForm, dispatchForm] = React.useState({});
    
    const [ _in, setIn ] = React.useState(false)
    const [ transaction, setTransaction ] = React.useState({}) 
    const [ animation, setAnimation ] = React.useState('')

    
    
    
    
    
    
    React.useEffect(()=>{
        
        
        
    },[]);
    
    
    return (
        <>
  <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['framefouronetwoo']?.animationClass || {}}>

    <div id="id_fivefivenigth_twoozerosevenseven" ref={nodeRef} className={` ${ props.onClick ? 'cursor' : '' } framefouronetwoo ${ props.cssClass } ${ transaction['framefouronetwoo']?.type ? transaction['framefouronetwoo']?.type.toLowerCase() : '' }`} style={ { ...{ ...{}, transitionDuration: transaction['framefouronetwoo']?.duration, transitionTimingFunction: transaction['framefouronetwoo']?.timingFunction }, ...props.style }} onClick={ props.FramefouronetwooonClick } onMouseEnter={ props.FramefouronetwooonMouseEnter } onMouseOver={ props.FramefouronetwooonMouseOver } onKeyPress={ props.FramefouronetwooonKeyPress } onDrag={ props.FramefouronetwooonDrag } onMouseLeave={ props.FramefouronetwooonMouseLeave } onMouseUp={ props.FramefouronetwooonMouseUp } onMouseDown={ props.FramefouronetwooonMouseDown } onKeyDown={ props.FramefouronetwooonKeyDown } onChange={ props.FramefouronetwooonChange } ondelay={ props.Framefouronetwooondelay }>
      {
      props.children ?
      props.children :
      <>
        <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['image']?.animationClass || {}}>

          <div id="id_fivefivenigth_twoozerosixtwoo" className={` frame image ${ props.onClick ? 'cursor' : '' } ${ transaction['image']?.type ? transaction['image']?.type.toLowerCase() : '' }`} style={ { ...{}, ...props.IMAGEStyle , transitionDuration: transaction['image']?.duration, transitionTimingFunction: transaction['image']?.timingFunction } } onClick={ props.IMAGEonClick } onMouseEnter={ props.IMAGEonMouseEnter } onMouseOver={ props.IMAGEonMouseOver } onKeyPress={ props.IMAGEonKeyPress } onDrag={ props.IMAGEonDrag } onMouseLeave={ props.IMAGEonMouseLeave } onMouseUp={ props.IMAGEonMouseUp } onMouseDown={ props.IMAGEonMouseDown } onKeyDown={ props.IMAGEonKeyDown } onChange={ props.IMAGEonChange } ondelay={ props.IMAGEondelay }>
            <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['installpage']?.animationClass || {}}>

              <div id="id_fivefivenigth_twoozerosixthree" className={` group installpage ${ props.onClick ? 'cursor' : '' } ${ transaction['installpage']?.type ? transaction['installpage']?.type.toLowerCase() : '' }`} style={{ ...{},  ...props.InstallPageStyle , transitionDuration: transaction['installpage']?.duration, transitionTimingFunction: transaction['installpage']?.timingFunction }} onClick={ props.InstallPageonClick } onMouseEnter={ props.InstallPageonMouseEnter } onMouseOver={ props.InstallPageonMouseOver } onKeyPress={ props.InstallPageonKeyPress } onDrag={ props.InstallPageonDrag } onMouseLeave={ props.InstallPageonMouseLeave } onMouseUp={ props.InstallPageonMouseUp } onMouseDown={ props.InstallPageonMouseDown } onKeyDown={ props.InstallPageonKeyDown } onChange={ props.InstallPageonChange } ondelay={ props.InstallPageondelay }>
                <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['btwoodesktopone']?.animationClass || {}}>

                  <div id="id_fivefivenigth_twoozerosixfour" className={` group btwoodesktopone ${ props.onClick ? 'cursor' : '' } ${ transaction['btwoodesktopone']?.type ? transaction['btwoodesktopone']?.type.toLowerCase() : '' }`} style={{ ...{},  ...props.BtwoodesktoponeStyle , transitionDuration: transaction['btwoodesktopone']?.duration, transitionTimingFunction: transaction['btwoodesktopone']?.timingFunction }} onClick={ props.BtwoodesktoponeonClick } onMouseEnter={ props.BtwoodesktoponeonMouseEnter } onMouseOver={ props.BtwoodesktoponeonMouseOver } onKeyPress={ props.BtwoodesktoponeonKeyPress } onDrag={ props.BtwoodesktoponeonDrag } onMouseLeave={ props.BtwoodesktoponeonMouseLeave } onMouseUp={ props.BtwoodesktoponeonMouseUp } onMouseDown={ props.BtwoodesktoponeonMouseDown } onKeyDown={ props.BtwoodesktoponeonKeyDown } onChange={ props.BtwoodesktoponeonChange } ondelay={ props.Btwoodesktoponeondelay }>
                    <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['laptopanimationlarge']?.animationClass || {}}>

                      <div id="id_fivefivenigth_twoozerosixfive" className={` group laptopanimationlarge ${ props.onClick ? 'cursor' : '' } ${ transaction['laptopanimationlarge']?.type ? transaction['laptopanimationlarge']?.type.toLowerCase() : '' }`} style={{ ...{},  ...props.LaptopanimationlargeStyle , transitionDuration: transaction['laptopanimationlarge']?.duration, transitionTimingFunction: transaction['laptopanimationlarge']?.timingFunction }} onClick={ props.LaptopanimationlargeonClick } onMouseEnter={ props.LaptopanimationlargeonMouseEnter } onMouseOver={ props.LaptopanimationlargeonMouseOver } onKeyPress={ props.LaptopanimationlargeonKeyPress } onDrag={ props.LaptopanimationlargeonDrag } onMouseLeave={ props.LaptopanimationlargeonMouseLeave } onMouseUp={ props.LaptopanimationlargeonMouseUp } onMouseDown={ props.LaptopanimationlargeonMouseDown } onKeyDown={ props.LaptopanimationlargeonKeyDown } onChange={ props.LaptopanimationlargeonChange } ondelay={ props.Laptopanimationlargeondelay }>
                        <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['laptoplarge']?.animationClass || {}}>

                          <div id="id_fivefivenigth_twoozerosixsix" className={` group laptoplarge ${ props.onClick ? 'cursor' : '' } ${ transaction['laptoplarge']?.type ? transaction['laptoplarge']?.type.toLowerCase() : '' }`} style={{ ...{},  ...props.LaptoplargeStyle , transitionDuration: transaction['laptoplarge']?.duration, transitionTimingFunction: transaction['laptoplarge']?.timingFunction }} onClick={ props.LaptoplargeonClick } onMouseEnter={ props.LaptoplargeonMouseEnter } onMouseOver={ props.LaptoplargeonMouseOver } onKeyPress={ props.LaptoplargeonKeyPress } onDrag={ props.LaptoplargeonDrag } onMouseLeave={ props.LaptoplargeonMouseLeave } onMouseUp={ props.LaptoplargeonMouseUp } onMouseDown={ props.LaptoplargeonMouseDown } onKeyDown={ props.LaptoplargeonKeyDown } onChange={ props.LaptoplargeonChange } ondelay={ props.Laptoplargeondelay }>
                            <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['outer']?.animationClass || {}}>
                              <svg id="id_fivefivenigth_twoozerosixseven" className="outer  " style={{}} onClick={ props.OuteronClick } onMouseEnter={ props.OuteronMouseEnter } onMouseOver={ props.OuteronMouseOver } onKeyPress={ props.OuteronKeyPress } onDrag={ props.OuteronDrag } onMouseLeave={ props.OuteronMouseLeave } onMouseUp={ props.OuteronMouseUp } onMouseDown={ props.OuteronMouseDown } onKeyDown={ props.OuteronKeyDown } onChange={ props.OuteronChange } ondelay={ props.Outerondelay } width="112.35302734375" height="74.1185302734375">
                                <path d="M111.026 0L1.3272 0C0.594208 0 0 0.593868 0 1.32644L0 72.792C0 73.5246 0.594208 74.1185 1.3272 74.1185L111.026 74.1185C111.759 74.1185 112.353 73.5246 112.353 72.792L112.353 1.32644C112.353 0.593868 111.759 0 111.026 0Z" />
                              </svg>
                            </CSSTransition>
                            <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['screen']?.animationClass || {}}>
                              <svg id="id_fivefivenigth_twoozerosixeight" className="screen  " style={{}} onClick={ props.ScreenonClick } onMouseEnter={ props.ScreenonMouseEnter } onMouseOver={ props.ScreenonMouseOver } onKeyPress={ props.ScreenonKeyPress } onDrag={ props.ScreenonDrag } onMouseLeave={ props.ScreenonMouseLeave } onMouseUp={ props.ScreenonMouseUp } onMouseDown={ props.ScreenonMouseDown } onKeyDown={ props.ScreenonKeyDown } onChange={ props.ScreenonChange } ondelay={ props.Screenondelay } width="97.864990234375" height="61.2882080078125">
                                <path d="M0 61.2883L97.8649 61.2883L97.8649 0L0 0L0 61.2883Z" />
                              </svg>
                            </CSSTransition>
                            <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['base']?.animationClass || {}}>

                              <div id="id_fivefivenigth_twoozerosixnigth" className={` group base ${ props.onClick ? 'cursor' : '' } ${ transaction['base']?.type ? transaction['base']?.type.toLowerCase() : '' }`} style={{ ...{},  ...props.BaseStyle , transitionDuration: transaction['base']?.duration, transitionTimingFunction: transaction['base']?.timingFunction }} onClick={ props.BaseonClick } onMouseEnter={ props.BaseonMouseEnter } onMouseOver={ props.BaseonMouseOver } onKeyPress={ props.BaseonKeyPress } onDrag={ props.BaseonDrag } onMouseLeave={ props.BaseonMouseLeave } onMouseUp={ props.BaseonMouseUp } onMouseDown={ props.BaseonMouseDown } onKeyDown={ props.BaseonKeyDown } onChange={ props.BaseonChange } ondelay={ props.Baseondelay }>
                                <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['vector']?.animationClass || {}}>
                                  <svg id="id_fivefivenigth_twoozerosevenzero" className="vector  " style={{}} onClick={ props.VectoronClick } onMouseEnter={ props.VectoronMouseEnter } onMouseOver={ props.VectoronMouseOver } onKeyPress={ props.VectoronKeyPress } onDrag={ props.VectoronDrag } onMouseLeave={ props.VectoronMouseLeave } onMouseUp={ props.VectoronMouseUp } onMouseDown={ props.VectoronMouseDown } onKeyDown={ props.VectoronKeyDown } onChange={ props.VectoronChange } ondelay={ props.Vectorondelay } width="135.876953125" height="5.67724609375">
                                    <path d="M0.00105958 0.329771C-0.0133602 0.147643 0.120988 0 0.306972 0L135.606 0C135.79 0 135.91 0.146042 135.869 0.321499C135.869 0.321499 135.011 5.67723 129.381 5.67723L5.81512 5.67723C0.116047 5.67723 0.00105958 0.329771 0.00105958 0.329771Z" />
                                  </svg>
                                </CSSTransition>
                                <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['indent']?.animationClass || {}}>
                                  <svg id="id_fivefivenigth_twoozerosevenone" className="indent  " style={{}} onClick={ props.IndentonClick } onMouseEnter={ props.IndentonMouseEnter } onMouseOver={ props.IndentonMouseOver } onKeyPress={ props.IndentonKeyPress } onDrag={ props.IndentonDrag } onMouseLeave={ props.IndentonMouseLeave } onMouseUp={ props.IndentonMouseUp } onMouseDown={ props.IndentonMouseDown } onKeyDown={ props.IndentonKeyDown } onChange={ props.IndentonChange } ondelay={ props.Indentondelay } width="15.350830078125" height="1.0322265625">
                                    <path d="M15.3507 0L0 0L0 1.03222L15.3507 1.03222L15.3507 0Z" />
                                  </svg>
                                </CSSTransition>
                              </div>

                            </CSSTransition>
                            <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['roundedrectangle']?.animationClass || {}}>
                              <svg id="id_fivefivenigth_twoozeroseventwoo" className="roundedrectangle  " style={{}} onClick={ props.RoundedrectangleonClick } onMouseEnter={ props.RoundedrectangleonMouseEnter } onMouseOver={ props.RoundedrectangleonMouseOver } onKeyPress={ props.RoundedrectangleonKeyPress } onDrag={ props.RoundedrectangleonDrag } onMouseLeave={ props.RoundedrectangleonMouseLeave } onMouseUp={ props.RoundedrectangleonMouseUp } onMouseDown={ props.RoundedrectangleonMouseDown } onKeyDown={ props.RoundedrectangleonKeyDown } onChange={ props.RoundedrectangleonChange } ondelay={ props.Roundedrectangleondelay } width="3.87890625" height="0.5875244140625">
                                <path d="M3.5849 0L0.293937 0C0.131598 0 0 0.131526 0 0.293772C0 0.456019 0.131598 0.587545 0.293937 0.587545L3.5849 0.587545C3.74724 0.587545 3.87884 0.456019 3.87884 0.293772C3.87884 0.131526 3.74724 0 3.5849 0Z" />
                              </svg>
                            </CSSTransition>
                          </div>

                        </CSSTransition>
                      </div>

                    </CSSTransition>
                  </div>

                </CSSTransition>
              </div>

            </CSSTransition>
          </div>

        </CSSTransition>
        <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['image']?.animationClass || {}}>

          <div id="id_fivefivenigth_twoozeroseventhree" className={` frame image ${ props.onClick ? 'cursor' : '' } ${ transaction['image']?.type ? transaction['image']?.type.toLowerCase() : '' }`} style={ { ...{}, ...props.IMAGEStyle , transitionDuration: transaction['image']?.duration, transitionTimingFunction: transaction['image']?.timingFunction } } onClick={ props.IMAGEonClick } onMouseEnter={ props.IMAGEonMouseEnter } onMouseOver={ props.IMAGEonMouseOver } onKeyPress={ props.IMAGEonKeyPress } onDrag={ props.IMAGEonDrag } onMouseLeave={ props.IMAGEonMouseLeave } onMouseUp={ props.IMAGEonMouseUp } onMouseDown={ props.IMAGEonMouseDown } onKeyDown={ props.IMAGEonKeyDown } onChange={ props.IMAGEonChange } ondelay={ props.IMAGEondelay }>
            <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['ovalthreeseven']?.animationClass || {}}>
              <svg id="id_fivefivenigth_twoozerosevenfour" className="ovalthreeseven  " style={{}} onClick={ props.OvalthreesevenonClick } onMouseEnter={ props.OvalthreesevenonMouseEnter } onMouseOver={ props.OvalthreesevenonMouseOver } onKeyPress={ props.OvalthreesevenonKeyPress } onDrag={ props.OvalthreesevenonDrag } onMouseLeave={ props.OvalthreesevenonMouseLeave } onMouseUp={ props.OvalthreesevenonMouseUp } onMouseDown={ props.OvalthreesevenonMouseDown } onKeyDown={ props.OvalthreesevenonKeyDown } onChange={ props.OvalthreesevenonChange } ondelay={ props.Ovalthreesevenondelay } width="31" height="31">

              </svg>
            </CSSTransition>
          </div>

        </CSSTransition>
        <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['image']?.animationClass || {}}>

          <div id="id_fivefivenigth_twoozerosevenfive" className={` frame image ${ props.onClick ? 'cursor' : '' } ${ transaction['image']?.type ? transaction['image']?.type.toLowerCase() : '' }`} style={ { ...{}, ...props.IMAGEStyle , transitionDuration: transaction['image']?.duration, transitionTimingFunction: transaction['image']?.timingFunction } } onClick={ props.IMAGEonClick } onMouseEnter={ props.IMAGEonMouseEnter } onMouseOver={ props.IMAGEonMouseOver } onKeyPress={ props.IMAGEonKeyPress } onDrag={ props.IMAGEonDrag } onMouseLeave={ props.IMAGEonMouseLeave } onMouseUp={ props.IMAGEonMouseUp } onMouseDown={ props.IMAGEonMouseDown } onKeyDown={ props.IMAGEonKeyDown } onChange={ props.IMAGEonChange } ondelay={ props.IMAGEondelay }>
            <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['pathfivefive']?.animationClass || {}}>
              <svg id="id_fivefivenigth_twoozerosevensix" className="pathfivefive  " style={{}} onClick={ props.PathfivefiveonClick } onMouseEnter={ props.PathfivefiveonMouseEnter } onMouseOver={ props.PathfivefiveonMouseOver } onKeyPress={ props.PathfivefiveonKeyPress } onDrag={ props.PathfivefiveonDrag } onMouseLeave={ props.PathfivefiveonMouseLeave } onMouseUp={ props.PathfivefiveonMouseUp } onMouseDown={ props.PathfivefiveonMouseDown } onKeyDown={ props.PathfivefiveonKeyDown } onChange={ props.PathfivefiveonChange } ondelay={ props.Pathfivefiveondelay } width="7" height="6">

              </svg>
            </CSSTransition>
          </div>

        </CSSTransition>

      </>
      }
    </div>

  </CSSTransition>
</>
    ) 
}

Framefouronetwoo.propTypes = {
    style: PropTypes.any,
FramefouronetwooonClick: PropTypes.any,
FramefouronetwooonMouseEnter: PropTypes.any,
FramefouronetwooonMouseOver: PropTypes.any,
FramefouronetwooonKeyPress: PropTypes.any,
FramefouronetwooonDrag: PropTypes.any,
FramefouronetwooonMouseLeave: PropTypes.any,
FramefouronetwooonMouseUp: PropTypes.any,
FramefouronetwooonMouseDown: PropTypes.any,
FramefouronetwooonKeyDown: PropTypes.any,
FramefouronetwooonChange: PropTypes.any,
Framefouronetwooondelay: PropTypes.any,
IMAGEonClick: PropTypes.any,
IMAGEonMouseEnter: PropTypes.any,
IMAGEonMouseOver: PropTypes.any,
IMAGEonKeyPress: PropTypes.any,
IMAGEonDrag: PropTypes.any,
IMAGEonMouseLeave: PropTypes.any,
IMAGEonMouseUp: PropTypes.any,
IMAGEonMouseDown: PropTypes.any,
IMAGEonKeyDown: PropTypes.any,
IMAGEonChange: PropTypes.any,
IMAGEondelay: PropTypes.any,
InstallPageonClick: PropTypes.any,
InstallPageonMouseEnter: PropTypes.any,
InstallPageonMouseOver: PropTypes.any,
InstallPageonKeyPress: PropTypes.any,
InstallPageonDrag: PropTypes.any,
InstallPageonMouseLeave: PropTypes.any,
InstallPageonMouseUp: PropTypes.any,
InstallPageonMouseDown: PropTypes.any,
InstallPageonKeyDown: PropTypes.any,
InstallPageonChange: PropTypes.any,
InstallPageondelay: PropTypes.any,
BtwoodesktoponeonClick: PropTypes.any,
BtwoodesktoponeonMouseEnter: PropTypes.any,
BtwoodesktoponeonMouseOver: PropTypes.any,
BtwoodesktoponeonKeyPress: PropTypes.any,
BtwoodesktoponeonDrag: PropTypes.any,
BtwoodesktoponeonMouseLeave: PropTypes.any,
BtwoodesktoponeonMouseUp: PropTypes.any,
BtwoodesktoponeonMouseDown: PropTypes.any,
BtwoodesktoponeonKeyDown: PropTypes.any,
BtwoodesktoponeonChange: PropTypes.any,
Btwoodesktoponeondelay: PropTypes.any,
LaptopanimationlargeonClick: PropTypes.any,
LaptopanimationlargeonMouseEnter: PropTypes.any,
LaptopanimationlargeonMouseOver: PropTypes.any,
LaptopanimationlargeonKeyPress: PropTypes.any,
LaptopanimationlargeonDrag: PropTypes.any,
LaptopanimationlargeonMouseLeave: PropTypes.any,
LaptopanimationlargeonMouseUp: PropTypes.any,
LaptopanimationlargeonMouseDown: PropTypes.any,
LaptopanimationlargeonKeyDown: PropTypes.any,
LaptopanimationlargeonChange: PropTypes.any,
Laptopanimationlargeondelay: PropTypes.any,
LaptoplargeonClick: PropTypes.any,
LaptoplargeonMouseEnter: PropTypes.any,
LaptoplargeonMouseOver: PropTypes.any,
LaptoplargeonKeyPress: PropTypes.any,
LaptoplargeonDrag: PropTypes.any,
LaptoplargeonMouseLeave: PropTypes.any,
LaptoplargeonMouseUp: PropTypes.any,
LaptoplargeonMouseDown: PropTypes.any,
LaptoplargeonKeyDown: PropTypes.any,
LaptoplargeonChange: PropTypes.any,
Laptoplargeondelay: PropTypes.any,
OuteronClick: PropTypes.any,
OuteronMouseEnter: PropTypes.any,
OuteronMouseOver: PropTypes.any,
OuteronKeyPress: PropTypes.any,
OuteronDrag: PropTypes.any,
OuteronMouseLeave: PropTypes.any,
OuteronMouseUp: PropTypes.any,
OuteronMouseDown: PropTypes.any,
OuteronKeyDown: PropTypes.any,
OuteronChange: PropTypes.any,
Outerondelay: PropTypes.any,
ScreenonClick: PropTypes.any,
ScreenonMouseEnter: PropTypes.any,
ScreenonMouseOver: PropTypes.any,
ScreenonKeyPress: PropTypes.any,
ScreenonDrag: PropTypes.any,
ScreenonMouseLeave: PropTypes.any,
ScreenonMouseUp: PropTypes.any,
ScreenonMouseDown: PropTypes.any,
ScreenonKeyDown: PropTypes.any,
ScreenonChange: PropTypes.any,
Screenondelay: PropTypes.any,
BaseonClick: PropTypes.any,
BaseonMouseEnter: PropTypes.any,
BaseonMouseOver: PropTypes.any,
BaseonKeyPress: PropTypes.any,
BaseonDrag: PropTypes.any,
BaseonMouseLeave: PropTypes.any,
BaseonMouseUp: PropTypes.any,
BaseonMouseDown: PropTypes.any,
BaseonKeyDown: PropTypes.any,
BaseonChange: PropTypes.any,
Baseondelay: PropTypes.any,
VectoronClick: PropTypes.any,
VectoronMouseEnter: PropTypes.any,
VectoronMouseOver: PropTypes.any,
VectoronKeyPress: PropTypes.any,
VectoronDrag: PropTypes.any,
VectoronMouseLeave: PropTypes.any,
VectoronMouseUp: PropTypes.any,
VectoronMouseDown: PropTypes.any,
VectoronKeyDown: PropTypes.any,
VectoronChange: PropTypes.any,
Vectorondelay: PropTypes.any,
IndentonClick: PropTypes.any,
IndentonMouseEnter: PropTypes.any,
IndentonMouseOver: PropTypes.any,
IndentonKeyPress: PropTypes.any,
IndentonDrag: PropTypes.any,
IndentonMouseLeave: PropTypes.any,
IndentonMouseUp: PropTypes.any,
IndentonMouseDown: PropTypes.any,
IndentonKeyDown: PropTypes.any,
IndentonChange: PropTypes.any,
Indentondelay: PropTypes.any,
RoundedrectangleonClick: PropTypes.any,
RoundedrectangleonMouseEnter: PropTypes.any,
RoundedrectangleonMouseOver: PropTypes.any,
RoundedrectangleonKeyPress: PropTypes.any,
RoundedrectangleonDrag: PropTypes.any,
RoundedrectangleonMouseLeave: PropTypes.any,
RoundedrectangleonMouseUp: PropTypes.any,
RoundedrectangleonMouseDown: PropTypes.any,
RoundedrectangleonKeyDown: PropTypes.any,
RoundedrectangleonChange: PropTypes.any,
Roundedrectangleondelay: PropTypes.any,
OvalthreesevenonClick: PropTypes.any,
OvalthreesevenonMouseEnter: PropTypes.any,
OvalthreesevenonMouseOver: PropTypes.any,
OvalthreesevenonKeyPress: PropTypes.any,
OvalthreesevenonDrag: PropTypes.any,
OvalthreesevenonMouseLeave: PropTypes.any,
OvalthreesevenonMouseUp: PropTypes.any,
OvalthreesevenonMouseDown: PropTypes.any,
OvalthreesevenonKeyDown: PropTypes.any,
OvalthreesevenonChange: PropTypes.any,
Ovalthreesevenondelay: PropTypes.any,
PathfivefiveonClick: PropTypes.any,
PathfivefiveonMouseEnter: PropTypes.any,
PathfivefiveonMouseOver: PropTypes.any,
PathfivefiveonKeyPress: PropTypes.any,
PathfivefiveonDrag: PropTypes.any,
PathfivefiveonMouseLeave: PropTypes.any,
PathfivefiveonMouseUp: PropTypes.any,
PathfivefiveonMouseDown: PropTypes.any,
PathfivefiveonKeyDown: PropTypes.any,
PathfivefiveonChange: PropTypes.any,
Pathfivefiveondelay: PropTypes.any
}
export default Framefouronetwoo;