import React, { Suspense } from 'react';
import PropTypes from 'prop-types';
import * as Contexts  from 'Contexts';
import { OverlaySwapContext } from 'Components/OverlaySwapProvider';
import CorebooksAnimation from 'Components/CorebooksAnimation';
import CorebooksAnimationSingle from 'Components/CorebooksAnimationSingle';

import * as UtilsScripts  from 'utils/utils';
import { Route, useHistory, useParams } from "react-router-dom";
import SingletoneNavigation from 'Class/SingletoneNavigation';

import LoadingComponent from "Components/Loading";


import { Transition } from "react-transition-group";
import { CSSTransition } from "react-transition-group";
import { useAppContext, useSessionContext } from 'context/AppContext';
import './InputCreate.css'





const InputCreate = (props)=>{
    const [state,dispatch] = useAppContext();
    const [, setNameSelectedComponent] = React.useContext(OverlaySwapContext);
    
    const nodeRef = React.useRef(null);
    const history = useHistory();
    let RouterContext = useParams();
    
    
    
    const DeploymentContainerContext = React.useContext(Contexts.DeploymentContainer)
    
    
    const [globalStateForm, dispatchForm] = React.useState({});
    
    const [ _in, setIn ] = React.useState(false)
    const [ transaction, setTransaction ] = React.useState({}) 
    const [ animation, setAnimation ] = React.useState('')

    
    
    
    
    
    
    React.useEffect(()=>{
        
        
        
    },[]);
    
    
    return (
        <>
  <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['inputcreate']?.animationClass || {}}>

    <div id="id_fourthreefour_onefivezeroeight" ref={nodeRef} className={` ${ props.onClick ? 'cursor' : '' } inputcreate C_fourthreefour_onefivezeroeight ${ props.cssClass } ${ transaction['inputcreate']?.type ? transaction['inputcreate']?.type.toLowerCase() : '' }`} style={ { ...{ ...{}, transitionDuration: transaction['inputcreate']?.duration, transitionTimingFunction: transaction['inputcreate']?.timingFunction }, ...props.style }} onClick={ props.InputCreateonClick } onMouseEnter={ props.InputCreateonMouseEnter } onMouseOver={ props.InputCreateonMouseOver } onKeyPress={ props.InputCreateonKeyPress } onDrag={ props.InputCreateonDrag } onMouseLeave={ props.InputCreateonMouseLeave } onMouseUp={ props.InputCreateonMouseUp } onMouseDown={ props.InputCreateonMouseDown } onKeyDown={ props.InputCreateonKeyDown } onChange={ props.InputCreateonChange } ondelay={ props.InputCreateondelay }>
      {
      props.children ?
      props.children :
      <>
        <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['deploygitlabel']?.animationClass || {}}>

          <div id="id_fourthreefour_onefivezerofive" className={` frame deploygitlabel ${ props.onClick ? 'cursor' : '' } ${ transaction['deploygitlabel']?.type ? transaction['deploygitlabel']?.type.toLowerCase() : '' }`} style={ { ...{}, ...props.DeployGITLabelStyle , transitionDuration: transaction['deploygitlabel']?.duration, transitionTimingFunction: transaction['deploygitlabel']?.timingFunction } } onClick={ props.DeployGITLabelonClick } onMouseEnter={ props.DeployGITLabelonMouseEnter } onMouseOver={ props.DeployGITLabelonMouseOver } onKeyPress={ props.DeployGITLabelonKeyPress } onDrag={ props.DeployGITLabelonDrag } onMouseLeave={ props.DeployGITLabelonMouseLeave } onMouseUp={ props.DeployGITLabelonMouseUp } onMouseDown={ props.DeployGITLabelonMouseDown } onKeyDown={ props.DeployGITLabelonKeyDown } onChange={ props.DeployGITLabelonChange } ondelay={ props.DeployGITLabelondelay }>
            <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['deploygitlabeltext']?.animationClass || {}}>

              <span id="id_fourthreefour_onefivezerosix"  className={` text deploygitlabeltext    ${ props.onClick ? 'cursor' : ''}  ${ transaction['deploygitlabeltext']?.type ? transaction['deploygitlabeltext']?.type.toLowerCase() : '' }`} style={{ ...{},  ...props.DeployGITLabelTextStyle , transitionDuration: transaction['deploygitlabeltext']?.duration, transitionTimingFunction: transaction['deploygitlabeltext']?.timingFunction }} onClick={ props.DeployGITLabelTextonClick } onMouseEnter={ props.DeployGITLabelTextonMouseEnter } onMouseOver={ props.DeployGITLabelTextonMouseOver } onKeyPress={ props.DeployGITLabelTextonKeyPress } onDrag={ props.DeployGITLabelTextonDrag } onMouseLeave={ props.DeployGITLabelTextonMouseLeave } onMouseUp={ props.DeployGITLabelTextonMouseUp } onMouseDown={ props.DeployGITLabelTextonMouseDown } onKeyDown={ props.DeployGITLabelTextonKeyDown } onChange={ props.DeployGITLabelTextonChange } ondelay={ props.DeployGITLabelTextondelay } >{props.DeployGITLabelText0 || `¿Cuál es tu email?`}</span>

            </CSSTransition>
          </div>

        </CSSTransition>
        <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['deploygitinput']?.animationClass || {}}>
          <input id="id_fourthreefour_onefivezerozero" className="deploygitinput   " onClick={ props.DeployGITInputonClick } onMouseEnter={ props.DeployGITInputonMouseEnter } onMouseOver={ props.DeployGITInputonMouseOver } onKeyPress={ props.DeployGITInputonKeyPress } onDrag={ props.DeployGITInputonDrag } onMouseLeave={ props.DeployGITInputonMouseLeave } onMouseUp={ props.DeployGITInputonMouseUp } onMouseDown={ props.DeployGITInputonMouseDown } onKeyDown={ props.DeployGITInputonKeyDown } onChange={ props.DeployGITInputonChange || function(e){ dispatchForm({ payload: { key: e.target.name, value: e.target.value } }) }} ondelay={ props.DeployGITInputondelay } style={ props.DeployGITInputStyle || {}} type="text" required="true" placeholder="Inserte el group id del su proyecto" value="GroupID" name="x_gitlab_id_group" value={globalStateForm['x_gitlab_id_group']} />
        </CSSTransition>

      </>
      }
    </div>

  </CSSTransition>
</>
    ) 
}

InputCreate.propTypes = {
    style: PropTypes.any,
DeployGITLabelText0: PropTypes.any,
InputCreateonClick: PropTypes.any,
InputCreateonMouseEnter: PropTypes.any,
InputCreateonMouseOver: PropTypes.any,
InputCreateonKeyPress: PropTypes.any,
InputCreateonDrag: PropTypes.any,
InputCreateonMouseLeave: PropTypes.any,
InputCreateonMouseUp: PropTypes.any,
InputCreateonMouseDown: PropTypes.any,
InputCreateonKeyDown: PropTypes.any,
InputCreateonChange: PropTypes.any,
InputCreateondelay: PropTypes.any,
DeployGITLabelonClick: PropTypes.any,
DeployGITLabelonMouseEnter: PropTypes.any,
DeployGITLabelonMouseOver: PropTypes.any,
DeployGITLabelonKeyPress: PropTypes.any,
DeployGITLabelonDrag: PropTypes.any,
DeployGITLabelonMouseLeave: PropTypes.any,
DeployGITLabelonMouseUp: PropTypes.any,
DeployGITLabelonMouseDown: PropTypes.any,
DeployGITLabelonKeyDown: PropTypes.any,
DeployGITLabelonChange: PropTypes.any,
DeployGITLabelondelay: PropTypes.any,
DeployGITLabelTextonClick: PropTypes.any,
DeployGITLabelTextonMouseEnter: PropTypes.any,
DeployGITLabelTextonMouseOver: PropTypes.any,
DeployGITLabelTextonKeyPress: PropTypes.any,
DeployGITLabelTextonDrag: PropTypes.any,
DeployGITLabelTextonMouseLeave: PropTypes.any,
DeployGITLabelTextonMouseUp: PropTypes.any,
DeployGITLabelTextonMouseDown: PropTypes.any,
DeployGITLabelTextonKeyDown: PropTypes.any,
DeployGITLabelTextonChange: PropTypes.any,
DeployGITLabelTextondelay: PropTypes.any,
DeployGITInputonClick: PropTypes.any,
DeployGITInputonMouseEnter: PropTypes.any,
DeployGITInputonMouseOver: PropTypes.any,
DeployGITInputonKeyPress: PropTypes.any,
DeployGITInputonDrag: PropTypes.any,
DeployGITInputonMouseLeave: PropTypes.any,
DeployGITInputonMouseUp: PropTypes.any,
DeployGITInputonMouseDown: PropTypes.any,
DeployGITInputonKeyDown: PropTypes.any,
DeployGITInputonChange: PropTypes.any,
DeployGITInputondelay: PropTypes.any
}
export default InputCreate;