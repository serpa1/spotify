import React, { Suspense } from 'react';
import PropTypes from 'prop-types';
import * as Contexts  from 'Contexts';
import { OverlaySwapContext } from 'Components/OverlaySwapProvider';
import CorebooksAnimation from 'Components/CorebooksAnimation';
import CorebooksAnimationSingle from 'Components/CorebooksAnimationSingle';

import * as UtilsScripts  from 'utils/utils';
import { Route, useHistory, useParams } from "react-router-dom";
import SingletoneNavigation from 'Class/SingletoneNavigation';

import LoadingComponent from "Components/Loading";


import { Transition } from "react-transition-group";
import { CSSTransition } from "react-transition-group";
import { useAppContext, useSessionContext } from 'context/AppContext';
import './Htwoo.css'





const Htwoo = (props)=>{
    const [state,dispatch] = useAppContext();
    const [, setNameSelectedComponent] = React.useContext(OverlaySwapContext);
    
    const nodeRef = React.useRef(null);
    const history = useHistory();
    let RouterContext = useParams();
    
    
    
    
    
    
    const [globalStateForm, dispatchForm] = React.useState({});
    
    const [ _in, setIn ] = React.useState(false)
    const [ transaction, setTransaction ] = React.useState({}) 
    const [ animation, setAnimation ] = React.useState('')

    
    
    
    
    
    
    React.useEffect(()=>{
        
        
        
    },[]);
    
    
    return (
        <>
  <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['htwoo']?.animationClass || {}}>

    <div id="id_three_oneeightfoursix" ref={nodeRef} className={` ${ props.onClick ? 'cursor' : '' } htwoo C_three_oneeightfoursix ${ props.cssClass } ${ transaction['htwoo']?.type ? transaction['htwoo']?.type.toLowerCase() : '' }`} style={ { ...{ ...{}, transitionDuration: transaction['htwoo']?.duration, transitionTimingFunction: transaction['htwoo']?.timingFunction }, ...props.style }} onClick={ props.HtwooonClick } onMouseEnter={ props.HtwooonMouseEnter } onMouseOver={ props.HtwooonMouseOver } onKeyPress={ props.HtwooonKeyPress } onDrag={ props.HtwooonDrag } onMouseLeave={ props.HtwooonMouseLeave } onMouseUp={ props.HtwooonMouseUp } onMouseDown={ props.HtwooonMouseDown } onKeyDown={ props.HtwooonKeyDown } onChange={ props.HtwooonChange } ondelay={ props.Htwooondelay }>
      {
      props.children ?
      props.children :
      <>
        <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['htwootext']?.animationClass || {}}>

          <span id="id_one_eightnigtheight"  className={` text htwootext    ${ props.onClick ? 'cursor' : ''}  ${ transaction['htwootext']?.type ? transaction['htwootext']?.type.toLowerCase() : '' }`} style={{ ...{},  ...props.HtwooTextStyle , transitionDuration: transaction['htwootext']?.duration, transitionTimingFunction: transaction['htwootext']?.timingFunction }} onClick={ props.HtwooTextonClick } onMouseEnter={ props.HtwooTextonMouseEnter } onMouseOver={ props.HtwooTextonMouseOver } onKeyPress={ props.HtwooTextonKeyPress } onDrag={ props.HtwooTextonDrag } onMouseLeave={ props.HtwooTextonMouseLeave } onMouseUp={ props.HtwooTextonMouseUp } onMouseDown={ props.HtwooTextonMouseDown } onKeyDown={ props.HtwooTextonKeyDown } onChange={ props.HtwooTextonChange } ondelay={ props.HtwooTextondelay } >{props.HtwooText0 || `Chill Mix`}</span>

        </CSSTransition>

      </>
      }
    </div>

  </CSSTransition>
</>
    ) 
}

Htwoo.propTypes = {
    style: PropTypes.any,
HtwooText0: PropTypes.any,
HtwooonClick: PropTypes.any,
HtwooonMouseEnter: PropTypes.any,
HtwooonMouseOver: PropTypes.any,
HtwooonKeyPress: PropTypes.any,
HtwooonDrag: PropTypes.any,
HtwooonMouseLeave: PropTypes.any,
HtwooonMouseUp: PropTypes.any,
HtwooonMouseDown: PropTypes.any,
HtwooonKeyDown: PropTypes.any,
HtwooonChange: PropTypes.any,
Htwooondelay: PropTypes.any,
HtwooTextonClick: PropTypes.any,
HtwooTextonMouseEnter: PropTypes.any,
HtwooTextonMouseOver: PropTypes.any,
HtwooTextonKeyPress: PropTypes.any,
HtwooTextonDrag: PropTypes.any,
HtwooTextonMouseLeave: PropTypes.any,
HtwooTextonMouseUp: PropTypes.any,
HtwooTextonMouseDown: PropTypes.any,
HtwooTextonKeyDown: PropTypes.any,
HtwooTextonChange: PropTypes.any,
HtwooTextondelay: PropTypes.any
}
export default Htwoo;