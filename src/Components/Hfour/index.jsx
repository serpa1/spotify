import React, { Suspense } from 'react';
import PropTypes from 'prop-types';
import * as Contexts  from 'Contexts';
import { OverlaySwapContext } from 'Components/OverlaySwapProvider';
import CorebooksAnimation from 'Components/CorebooksAnimation';
import CorebooksAnimationSingle from 'Components/CorebooksAnimationSingle';

import * as UtilsScripts  from 'utils/utils';
import { Route, useHistory, useParams } from "react-router-dom";
import SingletoneNavigation from 'Class/SingletoneNavigation';

import LoadingComponent from "Components/Loading";


import { Transition } from "react-transition-group";
import { CSSTransition } from "react-transition-group";
import { useAppContext, useSessionContext } from 'context/AppContext';
import './Hfour.css'





const Hfour = (props)=>{
    const [state,dispatch] = useAppContext();
    const [, setNameSelectedComponent] = React.useContext(OverlaySwapContext);
    
    const nodeRef = React.useRef(null);
    const history = useHistory();
    let RouterContext = useParams();
    
    
    
    
    
    
    const [globalStateForm, dispatchForm] = React.useState({});
    
    const [ _in, setIn ] = React.useState(false)
    const [ transaction, setTransaction ] = React.useState({}) 
    const [ animation, setAnimation ] = React.useState('')

    
    
    
    
    
    
    React.useEffect(()=>{
        
        
        
    },[]);
    
    
    return (
        <>
  <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['hfour']?.animationClass || {}}>

    <div id="id_three_oneeightfoureight" ref={nodeRef} className={` ${ props.onClick ? 'cursor' : '' } hfour C_three_oneeightfoureight ${ props.cssClass } ${ transaction['hfour']?.type ? transaction['hfour']?.type.toLowerCase() : '' }`} style={ { ...{ ...{}, transitionDuration: transaction['hfour']?.duration, transitionTimingFunction: transaction['hfour']?.timingFunction }, ...props.style }} onClick={ props.HfouronClick } onMouseEnter={ props.HfouronMouseEnter } onMouseOver={ props.HfouronMouseOver } onKeyPress={ props.HfouronKeyPress } onDrag={ props.HfouronDrag } onMouseLeave={ props.HfouronMouseLeave } onMouseUp={ props.HfouronMouseUp } onMouseDown={ props.HfouronMouseDown } onKeyDown={ props.HfouronKeyDown } onChange={ props.HfouronChange } ondelay={ props.Hfourondelay }>
      {
      props.children ?
      props.children :
      <>
        <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['hfourtext']?.animationClass || {}}>

          <span id="id_one_nigthzerozero"  className={` text hfourtext    ${ props.onClick ? 'cursor' : ''}  ${ transaction['hfourtext']?.type ? transaction['hfourtext']?.type.toLowerCase() : '' }`} style={{ ...{},  ...props.HfourTextStyle , transitionDuration: transaction['hfourtext']?.duration, transitionTimingFunction: transaction['hfourtext']?.timingFunction }} onClick={ props.HfourTextonClick } onMouseEnter={ props.HfourTextonMouseEnter } onMouseOver={ props.HfourTextonMouseOver } onKeyPress={ props.HfourTextonKeyPress } onDrag={ props.HfourTextonDrag } onMouseLeave={ props.HfourTextonMouseLeave } onMouseUp={ props.HfourTextonMouseUp } onMouseDown={ props.HfourTextonMouseDown } onKeyDown={ props.HfourTextonKeyDown } onChange={ props.HfourTextonChange } ondelay={ props.HfourTextondelay } >{props.HfourText0 || `SEE ALL`}</span>

        </CSSTransition>

      </>
      }
    </div>

  </CSSTransition>
</>
    ) 
}

Hfour.propTypes = {
    style: PropTypes.any,
HfourText0: PropTypes.any,
HfouronClick: PropTypes.any,
HfouronMouseEnter: PropTypes.any,
HfouronMouseOver: PropTypes.any,
HfouronKeyPress: PropTypes.any,
HfouronDrag: PropTypes.any,
HfouronMouseLeave: PropTypes.any,
HfouronMouseUp: PropTypes.any,
HfouronMouseDown: PropTypes.any,
HfouronKeyDown: PropTypes.any,
HfouronChange: PropTypes.any,
Hfourondelay: PropTypes.any,
HfourTextonClick: PropTypes.any,
HfourTextonMouseEnter: PropTypes.any,
HfourTextonMouseOver: PropTypes.any,
HfourTextonKeyPress: PropTypes.any,
HfourTextonDrag: PropTypes.any,
HfourTextonMouseLeave: PropTypes.any,
HfourTextonMouseUp: PropTypes.any,
HfourTextonMouseDown: PropTypes.any,
HfourTextonKeyDown: PropTypes.any,
HfourTextonChange: PropTypes.any,
HfourTextondelay: PropTypes.any
}
export default Hfour;