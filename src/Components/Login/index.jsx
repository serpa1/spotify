import React, { Suspense } from 'react';
import PropTypes from 'prop-types';
import * as Contexts  from 'Contexts';
import { OverlaySwapContext } from 'Components/OverlaySwapProvider';
import CorebooksAnimation from 'Components/CorebooksAnimation';
import CorebooksAnimationSingle from 'Components/CorebooksAnimationSingle';

import * as UtilsScripts  from 'utils/utils';
import { Route, useHistory, useParams } from "react-router-dom";
import SingletoneNavigation from 'Class/SingletoneNavigation';

import LoadingComponent from "Components/Loading";


import { Transition } from "react-transition-group";
import { CSSTransition } from "react-transition-group";
import TextLinkonesixpx from 'Components/TextLinkonesixpx'
import ButtonGreen from 'Components/ButtonGreen'
import Switch from 'Components/Switch'
import InputLogin from 'Components/InputLogin'
import ButtonLogin from 'Components/ButtonLogin'
import Displaytwoo from 'Components/Displaytwoo'
import { useAppContext, useSessionContext } from 'context/AppContext';
import './Login.css'




export const LoginLocalStateReducer = (state = {}, action) => {

    return {
        ...state,
        [action.payload.key]: action.payload.value,
    };
};
    

const Login = (props)=>{
    const [state,dispatch] = useAppContext();
    const [, setNameSelectedComponent] = React.useContext(OverlaySwapContext);
    
    const nodeRef = React.useRef(null);
    const history = useHistory();
    let RouterContext = useParams();
    
    
    
    const DeploymentContainerContext = React.useContext(Contexts.DeploymentContainer)
    
    
    const [globalStateForm, dispatchForm] = React.useState({});
    
    const [ _in, setIn ] = React.useState(false)
    const [ transaction, setTransaction ] = React.useState({}) 
    const [ animation, setAnimation ] = React.useState('')

    const [LoginLocalState, dispatchLogin] = React.useReducer(LoginLocalStateReducer, { ...{"View":"row","search":0}, ...(props.state || {} ) });
    
    
    
    
    
    React.useEffect(()=>{
        

        
        
    },[]);
    
    
    return (
        <Contexts.LoginLocalContext.Provider value={[LoginLocalState,dispatchLogin]}>
  <CSSTransition nodeRef={nodeRef} in={true} appear={true} timeout={400} classNames={SingletoneNavigation.getTransitionInstance()?.Login?.cssClass || '' }>

    <div id="id_fourthreefour_oneeightseveneight" ref={nodeRef} className={` ${ props.onClick ? 'cursor' : '' } login ${ props.cssClass } ${ transaction['login']?.type ? transaction['login']?.type.toLowerCase() : '' }`} style={ { ...{ ...{"width":"100%"}, transitionDuration: `${((SingletoneNavigation.getTransitionInstance()?.Login?.duration || 0) * 1000).toFixed(0)}ms` }, ...props.style }} onClick={ props.LoginonClick } onMouseEnter={ props.LoginonMouseEnter } onMouseOver={ props.LoginonMouseOver } onKeyPress={ props.LoginonKeyPress } onDrag={ props.LoginonDrag } onMouseLeave={ props.LoginonMouseLeave } onMouseUp={ props.LoginonMouseUp } onMouseDown={ props.LoginonMouseDown } onKeyDown={ props.LoginonKeyDown } onChange={ props.LoginonChange } ondelay={ props.Loginondelay }>
      {
      props.children ?
      props.children :
      <>
        <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['headerlogin']?.animationClass || {}}>

          <div id="id_fourthreefour_twoozerothreethree" className={` frame headerlogin show-xs show-s show-m hide-l hide-xl ${ props.onClick ? 'cursor' : '' } ${ transaction['headerlogin']?.type ? transaction['headerlogin']?.type.toLowerCase() : '' }`} style={ { ...{"marginBottom":"-100px"}, ...props.HeaderlOGINStyle , transitionDuration: transaction['headerlogin']?.duration, transitionTimingFunction: transaction['headerlogin']?.timingFunction } } onClick={ props.HeaderlOGINonClick } onMouseEnter={ props.HeaderlOGINonMouseEnter } onMouseOver={ props.HeaderlOGINonMouseOver } onKeyPress={ props.HeaderlOGINonKeyPress } onDrag={ props.HeaderlOGINonDrag } onMouseLeave={ props.HeaderlOGINonMouseLeave } onMouseUp={ props.HeaderlOGINonMouseUp } onMouseDown={ props.HeaderlOGINonMouseDown } onKeyDown={ props.HeaderlOGINonKeyDown } onChange={ props.HeaderlOGINonChange } ondelay={ props.HeaderlOGINondelay }>
            <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['uncodie_logologin']?.animationClass || {}}>
              <img id="id_fourthreefour_twoozerothreefour" className={` rectangle uncodie_logologin ${ props.onClick ? 'cursor' : '' } ${ transaction['uncodie_logologin']?.type ? transaction['uncodie_logologin']?.type.toLowerCase() : '' }`} style={{ ...{},  ...props.Uncodie_logoLoginStyle , transitionDuration: transaction['uncodie_logologin']?.duration, transitionTimingFunction: transaction['uncodie_logologin']?.timingFunction }} onClick={ props.Uncodie_logoLoginonClick } onMouseEnter={ props.Uncodie_logoLoginonMouseEnter } onMouseOver={ props.Uncodie_logoLoginonMouseOver } onKeyPress={ props.Uncodie_logoLoginonKeyPress } onDrag={ props.Uncodie_logoLoginonDrag } onMouseLeave={ props.Uncodie_logoLoginonMouseLeave } onMouseUp={ props.Uncodie_logoLoginonMouseUp } onMouseDown={ props.Uncodie_logoLoginonMouseDown } onKeyDown={ props.Uncodie_logoLoginonKeyDown } onChange={ props.Uncodie_logoLoginonChange } ondelay={ props.Uncodie_logoLoginondelay } src={props.Uncodie_logoLogin0 || "https://cdn.uncodie.com/tACQBrKCV97e0EqPE1XNLJ/efbb152934f48228779350567e3e051c143ce0f2.png" } />
            </CSSTransition>
          </div>

        </CSSTransition>
        <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['framethreeeightthree']?.animationClass || {}}>

          <div id="id_fourthreefour_twoofivesevenone" className={` frame framethreeeightthree ${ props.onClick ? 'cursor' : '' } ${ transaction['framethreeeightthree']?.type ? transaction['framethreeeightthree']?.type.toLowerCase() : '' }`} style={ { ...{}, ...props.FramethreeeightthreeStyle , transitionDuration: transaction['framethreeeightthree']?.duration, transitionTimingFunction: transaction['framethreeeightthree']?.timingFunction } } onClick={ props.FramethreeeightthreeonClick } onMouseEnter={ props.FramethreeeightthreeonMouseEnter } onMouseOver={ props.FramethreeeightthreeonMouseOver } onKeyPress={ props.FramethreeeightthreeonKeyPress } onDrag={ props.FramethreeeightthreeonDrag } onMouseLeave={ props.FramethreeeightthreeonMouseLeave } onMouseUp={ props.FramethreeeightthreeonMouseUp } onMouseDown={ props.FramethreeeightthreeonMouseDown } onKeyDown={ props.FramethreeeightthreeonKeyDown } onChange={ props.FramethreeeightthreeonChange } ondelay={ props.Framethreeeightthreeondelay }>
            <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['framethreeeighttwoo']?.animationClass || {}}>

              <div id="id_fourthreefour_twoofivesixnigth" className={` frame framethreeeighttwoo ${ props.onClick ? 'cursor' : '' } ${ transaction['framethreeeighttwoo']?.type ? transaction['framethreeeighttwoo']?.type.toLowerCase() : '' }`} style={ { ...{}, ...props.FramethreeeighttwooStyle , transitionDuration: transaction['framethreeeighttwoo']?.duration, transitionTimingFunction: transaction['framethreeeighttwoo']?.timingFunction } } onClick={ props.FramethreeeighttwooonClick } onMouseEnter={ props.FramethreeeighttwooonMouseEnter } onMouseOver={ props.FramethreeeighttwooonMouseOver } onKeyPress={ props.FramethreeeighttwooonKeyPress } onDrag={ props.FramethreeeighttwooonDrag } onMouseLeave={ props.FramethreeeighttwooonMouseLeave } onMouseUp={ props.FramethreeeighttwooonMouseUp } onMouseDown={ props.FramethreeeighttwooonMouseDown } onKeyDown={ props.FramethreeeighttwooonKeyDown } onChange={ props.FramethreeeighttwooonChange } ondelay={ props.Framethreeeighttwooondelay }>
                <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['framethreeeightone']?.animationClass || {}}>

                  <div id="id_fourthreefour_twoofivesixeight" className={` frame framethreeeightone ${ props.onClick ? 'cursor' : '' } ${ transaction['framethreeeightone']?.type ? transaction['framethreeeightone']?.type.toLowerCase() : '' }`} style={ { ...{}, ...props.FramethreeeightoneStyle , transitionDuration: transaction['framethreeeightone']?.duration, transitionTimingFunction: transaction['framethreeeightone']?.timingFunction } } onClick={ props.FramethreeeightoneonClick } onMouseEnter={ props.FramethreeeightoneonMouseEnter } onMouseOver={ props.FramethreeeightoneonMouseOver } onKeyPress={ props.FramethreeeightoneonKeyPress } onDrag={ props.FramethreeeightoneonDrag } onMouseLeave={ props.FramethreeeightoneonMouseLeave } onMouseUp={ props.FramethreeeightoneonMouseUp } onMouseDown={ props.FramethreeeightoneonMouseDown } onKeyDown={ props.FramethreeeightoneonKeyDown } onChange={ props.FramethreeeightoneonChange } ondelay={ props.Framethreeeightoneondelay }>
                    <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['framethreesevenone']?.animationClass || {}}>

                      <div id="id_fourthreefour_twoozerothreesix" className={` frame framethreesevenone ${ props.onClick ? 'cursor' : '' } ${ transaction['framethreesevenone']?.type ? transaction['framethreesevenone']?.type.toLowerCase() : '' }`} style={ { ...{}, ...props.FramethreesevenoneStyle , transitionDuration: transaction['framethreesevenone']?.duration, transitionTimingFunction: transaction['framethreesevenone']?.timingFunction } } onClick={ props.FramethreesevenoneonClick } onMouseEnter={ props.FramethreesevenoneonMouseEnter } onMouseOver={ props.FramethreesevenoneonMouseOver } onKeyPress={ props.FramethreesevenoneonKeyPress } onDrag={ props.FramethreesevenoneonDrag } onMouseLeave={ props.FramethreesevenoneonMouseLeave } onMouseUp={ props.FramethreesevenoneonMouseUp } onMouseDown={ props.FramethreesevenoneonMouseDown } onKeyDown={ props.FramethreesevenoneonKeyDown } onChange={ props.FramethreesevenoneonChange } ondelay={ props.Framethreesevenoneondelay }>
                        <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['displaytwoo']?.animationClass || {}}>
                          <Displaytwoo { ...{ ...props, style:false } } DisplaytwooText0={ props.DisplaytwooText0 || "Inicia sesión en Spotify" } cssClass={"C_sixtwoosix_onesevenzerofive "}  />
    </CSSTransition >
<CSSTransition 
        in={_in}
        appear={true} 
        timeout={100}
        classNames={transaction['framethreefivezero']?.animationClass || {}}
    >
    
                    <div id="id_fourthreefour_twoozerofourone" className={` frame framethreefivezero ${ props.onClick ? 'cursor' : '' } ${ transaction['framethreefivezero']?.type ? transaction['framethreefivezero']?.type.toLowerCase() : '' }`} style={ { ...{}, ...props.FramethreefivezeroStyle , transitionDuration: transaction['framethreefivezero']?.duration, transitionTimingFunction: transaction['framethreefivezero']?.timingFunction } } onClick={ props.FramethreefivezeroonClick } onMouseEnter={ props.FramethreefivezeroonMouseEnter } onMouseOver={ props.FramethreefivezeroonMouseOver } onKeyPress={ props.FramethreefivezeroonKeyPress } onDrag={ props.FramethreefivezeroonDrag } onMouseLeave={ props.FramethreefivezeroonMouseLeave } onMouseUp={ props.FramethreefivezeroonMouseUp } onMouseDown={ props.FramethreefivezeroonMouseDown } onKeyDown={ props.FramethreefivezeroonKeyDown } onChange={ props.FramethreefivezeroonChange } ondelay={ props.Framethreefivezeroondelay }>
                            <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['buttonlogin']?.animationClass || {}}>
                              <ButtonLogin { ...{ ...props, style:false } } variant={'Property 1=Default'} PrincipalButtonText0={ props.PrincipalButtonText0 || "Continuar Con Google" } PrincipalButtonText0={ props.PrincipalButtonText0 || "Continuar Con Google" } PrincipalButtonTextStyle={{"height":"14px"}} cssClass={"C_fourthreefour_twootwoosixsix "}  />
    </CSSTransition >
<CSSTransition 
        in={_in}
        appear={true} 
        timeout={100}
        classNames={transaction['buttonlogin']?.animationClass || {}}
    >
    <ButtonLogin { ...{ ...props, style:false } }  variant={'Property 1=Default'} PrincipalButtonText0={ props.PrincipalButtonText1 || " Continuar Con Facebook"} Rectangleone0={ props.Rectangleone1 || "https://cdn.uncodie.com/tACQBrKCV97e0EqPE1XNLJ/19b6d3e1bf248378cd2739aaaa8efdc1d4e38d29.png" } PrincipalButtonTextStyle={{"height":"14px"}} cssClass={"C_fourthreefour_twootwoofivezero "}  />
    </CSSTransition >
<CSSTransition 
        in={_in}
        appear={true} 
        timeout={100}
        classNames={transaction['buttonlogin']?.animationClass || {}}
    >
    <ButtonLogin { ...{ ...props, style:false } }  variant={'Property 1=Default'} PrincipalButtonText0={ props.PrincipalButtonText2 || " Continuar Con Apple"} Rectangleone0={ props.Rectangleone2 || "https://cdn.uncodie.com/tACQBrKCV97e0EqPE1XNLJ/6025165fd697db9e3392a632ee557e82e940e022.png" } PrincipalButtonTextStyle={{"height":"14px"}} cssClass={"C_fourthreefour_twootwoofivefour "}  />
    </CSSTransition >
<CSSTransition 
        in={_in}
        appear={true} 
        timeout={100}
        classNames={transaction['buttonlogin']?.animationClass || {}}
    >
    <ButtonLogin { ...{ ...props, style:false } }  variant={'Property 1=Default'} PrincipalButtonText0={ props.PrincipalButtonText3 || " Continuar Con Número De Teléfono "}
Rectangleone0={ props.Rectangleone3 || "" }
PrincipalButtonTextStyle={{"height":"14px"}} cssClass={" C_fourthreefour_twootwoosixtwoo "}  />
    </CSSTransition >
                    </div>
               
    </CSSTransition >
                    </div>
               
    </CSSTransition >
<CSSTransition 
        in={_in}
        appear={true} 
        timeout={100}
        classNames={transaction['rectangle']?.animationClass || {}}
    >
    <div id="id_fourthreefour_twootwooeightzero" className={` rectangle rectangle ${ props.onClick ? 'cursor' : '' } ${ transaction['rectangle']?.type ? transaction['rectangle']?.type.toLowerCase() : '' }`} style={{ ...{},  ...props.RectangleStyle , transitionDuration: transaction['rectangle']?.duration, transitionTimingFunction: transaction['rectangle']?.timingFunction }} onClick={ props.RectangleonClick } onMouseEnter={ props.RectangleonMouseEnter } onMouseOver={ props.RectangleonMouseOver } onKeyPress={ props.RectangleonKeyPress } onDrag={ props.RectangleonDrag } onMouseLeave={ props.RectangleonMouseLeave } onMouseUp={ props.RectangleonMouseUp } onMouseDown={ props.RectangleonMouseDown } onKeyDown={ props.RectangleonKeyDown } onChange={ props.RectangleonChange } ondelay={ props.Rectangleondelay }>
                      </div>
                    </CSSTransition>
                  </div>

                </CSSTransition>
                <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['framethreesevensix']?.animationClass || {}}>

                  <div id="id_fourthreefour_twoofivethreezero" className={` frame framethreesevensix ${ props.onClick ? 'cursor' : '' } ${ transaction['framethreesevensix']?.type ? transaction['framethreesevensix']?.type.toLowerCase() : '' }`} style={ { ...{}, ...props.FramethreesevensixStyle , transitionDuration: transaction['framethreesevensix']?.duration, transitionTimingFunction: transaction['framethreesevensix']?.timingFunction } } onClick={ props.FramethreesevensixonClick } onMouseEnter={ props.FramethreesevensixonMouseEnter } onMouseOver={ props.FramethreesevensixonMouseOver } onKeyPress={ props.FramethreesevensixonKeyPress } onDrag={ props.FramethreesevensixonDrag } onMouseLeave={ props.FramethreesevensixonMouseLeave } onMouseUp={ props.FramethreesevensixonMouseUp } onMouseDown={ props.FramethreesevensixonMouseDown } onKeyDown={ props.FramethreesevensixonKeyDown } onChange={ props.FramethreesevensixonChange } ondelay={ props.Framethreesevensixondelay }>
                    <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['framethreesevenfive']?.animationClass || {}}>

                      <div id="id_fourthreefour_twoofivetwoonigth" className={` frame framethreesevenfive ${ props.onClick ? 'cursor' : '' } ${ transaction['framethreesevenfive']?.type ? transaction['framethreesevenfive']?.type.toLowerCase() : '' }`} style={ { ...{}, ...props.FramethreesevenfiveStyle , transitionDuration: transaction['framethreesevenfive']?.duration, transitionTimingFunction: transaction['framethreesevenfive']?.timingFunction } } onClick={ props.FramethreesevenfiveonClick } onMouseEnter={ props.FramethreesevenfiveonMouseEnter } onMouseOver={ props.FramethreesevenfiveonMouseOver } onKeyPress={ props.FramethreesevenfiveonKeyPress } onDrag={ props.FramethreesevenfiveonDrag } onMouseLeave={ props.FramethreesevenfiveonMouseLeave } onMouseUp={ props.FramethreesevenfiveonMouseUp } onMouseDown={ props.FramethreesevenfiveonMouseDown } onKeyDown={ props.FramethreesevenfiveonKeyDown } onChange={ props.FramethreesevenfiveonChange } ondelay={ props.Framethreesevenfiveondelay }>
                        <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['inputlogin']?.animationClass || {}}>
                          <InputLogin { ...{ ...props, style:false } } DeployTextInput0={ props.DeployTextInput0 || "Email o nombre de usuario" } DeployGITLabelText0={ props.DeployGITLabelText0 || "Email o nombre de usuario" } cssClass={"C_fourthreefour_twoothreefivethree "}  />
    </CSSTransition >
<CSSTransition 
        in={_in}
        appear={true} 
        timeout={100}
        classNames={transaction['inputlogin']?.animationClass || {}}
    >
    <InputLogin { ...{ ...props, style:false } }    cssClass={" C_fourthreefour_twoothreesixtwoo "}  />
    </CSSTransition >
                    </div>
               
    </CSSTransition >
<CSSTransition 
        in={_in}
        appear={true} 
        timeout={100}
        classNames={transaction['framethreeseventhree']?.animationClass || {}}
    >
    
                    <div id="id_fourthreefour_twoofivetwoosix" className={` frame framethreeseventhree ${ props.onClick ? 'cursor' : '' } ${ transaction['framethreeseventhree']?.type ? transaction['framethreeseventhree']?.type.toLowerCase() : '' }`} style={ { ...{}, ...props.FramethreeseventhreeStyle , transitionDuration: transaction['framethreeseventhree']?.duration, transitionTimingFunction: transaction['framethreeseventhree']?.timingFunction } } onClick={ props.FramethreeseventhreeonClick } onMouseEnter={ props.FramethreeseventhreeonMouseEnter } onMouseOver={ props.FramethreeseventhreeonMouseOver } onKeyPress={ props.FramethreeseventhreeonKeyPress } onDrag={ props.FramethreeseventhreeonDrag } onMouseLeave={ props.FramethreeseventhreeonMouseLeave } onMouseUp={ props.FramethreeseventhreeonMouseUp } onMouseDown={ props.FramethreeseventhreeonMouseDown } onKeyDown={ props.FramethreeseventhreeonKeyDown } onChange={ props.FramethreeseventhreeonChange } ondelay={ props.Framethreeseventhreeondelay }>
                            <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['switch']?.animationClass || {}}>
                              <Switch { ...{ ...props, style:false } } variant={'Property 1=Default'} cssClass={"C_fourthreefour_twoofivetwoothree "}  />
    </CSSTransition >
<CSSTransition 
        in={_in}
        appear={true} 
        timeout={100}
        classNames={transaction['deploygitlabeltext']?.animationClass || {}}
    >
    
                <span id="id_fourthreefour_twoofivetwoofive" className={` text deploygitlabeltext ${ props.onClick ? 'cursor' : '' } ${ transaction['deploygitlabeltext']?.type ? transaction['deploygitlabeltext']?.type.toLowerCase() : '' }`} style={{ ...{},  ...props.DeployGITLabelTextStyle , transitionDuration: transaction['deploygitlabeltext']?.duration, transitionTimingFunction: transaction['deploygitlabeltext']?.timingFunction }} onClick={ props.DeployGITLabelTextonClick } onMouseEnter={ props.DeployGITLabelTextonMouseEnter } onMouseOver={ props.DeployGITLabelTextonMouseOver } onKeyPress={ props.DeployGITLabelTextonKeyPress } onDrag={ props.DeployGITLabelTextonDrag } onMouseLeave={ props.DeployGITLabelTextonMouseLeave } onMouseUp={ props.DeployGITLabelTextonMouseUp } onMouseDown={ props.DeployGITLabelTextonMouseDown } onKeyDown={ props.DeployGITLabelTextonKeyDown } onChange={ props.DeployGITLabelTextonChange } ondelay={ props.DeployGITLabelTextondelay }>{props.DeployGITLabelText0 || `Recordarme`}</span>

                            </CSSTransition>
                      </div>

                    </CSSTransition>
                  </div>

                </CSSTransition>
              </div>

            </CSSTransition>
            <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['framethreeeightzero']?.animationClass || {}}>

              <div id="id_fourthreefour_twoofivesixfive" className={` frame framethreeeightzero ${ props.onClick ? 'cursor' : '' } ${ transaction['framethreeeightzero']?.type ? transaction['framethreeeightzero']?.type.toLowerCase() : '' }`} style={ { ...{}, ...props.FramethreeeightzeroStyle , transitionDuration: transaction['framethreeeightzero']?.duration, transitionTimingFunction: transaction['framethreeeightzero']?.timingFunction } } onClick={ props.FramethreeeightzeroonClick } onMouseEnter={ props.FramethreeeightzeroonMouseEnter } onMouseOver={ props.FramethreeeightzeroonMouseOver } onKeyPress={ props.FramethreeeightzeroonKeyPress } onDrag={ props.FramethreeeightzeroonDrag } onMouseLeave={ props.FramethreeeightzeroonMouseLeave } onMouseUp={ props.FramethreeeightzeroonMouseUp } onMouseDown={ props.FramethreeeightzeroonMouseDown } onKeyDown={ props.FramethreeeightzeroonKeyDown } onChange={ props.FramethreeeightzeroonChange } ondelay={ props.Framethreeeightzeroondelay }>
                <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['framethreesevennigth']?.animationClass || {}}>

                  <div id="id_fourthreefour_twoofivesixfour" className={` frame framethreesevennigth ${ props.onClick ? 'cursor' : '' } ${ transaction['framethreesevennigth']?.type ? transaction['framethreesevennigth']?.type.toLowerCase() : '' }`} style={ { ...{}, ...props.FramethreesevennigthStyle , transitionDuration: transaction['framethreesevennigth']?.duration, transitionTimingFunction: transaction['framethreesevennigth']?.timingFunction } } onClick={ props.FramethreesevennigthonClick } onMouseEnter={ props.FramethreesevennigthonMouseEnter } onMouseOver={ props.FramethreesevennigthonMouseOver } onKeyPress={ props.FramethreesevennigthonKeyPress } onDrag={ props.FramethreesevennigthonDrag } onMouseLeave={ props.FramethreesevennigthonMouseLeave } onMouseUp={ props.FramethreesevennigthonMouseUp } onMouseDown={ props.FramethreesevennigthonMouseDown } onKeyDown={ props.FramethreesevennigthonKeyDown } onChange={ props.FramethreesevennigthonChange } ondelay={ props.Framethreesevennigthondelay }>
                    <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['framethreeseveneight']?.animationClass || {}}>

                      <div id="id_fourthreefour_twoofivesixthree" className={` frame framethreeseveneight ${ props.onClick ? 'cursor' : '' } ${ transaction['framethreeseveneight']?.type ? transaction['framethreeseveneight']?.type.toLowerCase() : '' }`} style={ { ...{}, ...props.FramethreeseveneightStyle , transitionDuration: transaction['framethreeseveneight']?.duration, transitionTimingFunction: transaction['framethreeseveneight']?.timingFunction } } onClick={ props.FramethreeseveneightonClick } onMouseEnter={ props.FramethreeseveneightonMouseEnter } onMouseOver={ props.FramethreeseveneightonMouseOver } onKeyPress={ props.FramethreeseveneightonKeyPress } onDrag={ props.FramethreeseveneightonDrag } onMouseLeave={ props.FramethreeseveneightonMouseLeave } onMouseUp={ props.FramethreeseveneightonMouseUp } onMouseDown={ props.FramethreeseveneightonMouseDown } onKeyDown={ props.FramethreeseveneightonKeyDown } onChange={ props.FramethreeseveneightonChange } ondelay={ props.Framethreeseveneightondelay }>
                        <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['buttongreen']?.animationClass || {}}>
                          <ButtonGreen { ...{ ...props, style:false } } variant={'Property 1=Default'} PrincipalButtonText0={ props.PrincipalButtonText0 || "Iniciar Sesión" } PrincipalButtonTextStyle={{"height":"14px"}} cssClass={"C_fourthreeeight_onethreeonezero "}  />
    </CSSTransition >
<CSSTransition 
        in={_in}
        appear={true} 
        timeout={100}
        classNames={transaction['textlinkonesixpx']?.animationClass || {}}
    >
    <TextLinkonesixpx { ...{ ...props, style:false } }  variant={'Property 1=Default'} SubtilteoneText0={ props.SubtilteoneText0 || " ¿Has olvidado tu contraseña?"} cssClass={"C_fourthreefour_twoofivefoursix "}  />
    </CSSTransition >
                    </div>
               
    </CSSTransition >
<CSSTransition 
        in={_in}
        appear={true} 
        timeout={100}
        classNames={transaction['rectangle']?.animationClass || {}}
    >
    <div id="id_fourthreefour_twoofivefourtwoo" className={` rectangle rectangle ${ props.onClick ? 'cursor' : '' } ${ transaction['rectangle']?.type ? transaction['rectangle']?.type.toLowerCase() : '' }`} style={{ ...{},  ...props.RectangleStyle , transitionDuration: transaction['rectangle']?.duration, transitionTimingFunction: transaction['rectangle']?.timingFunction }} onClick={ props.RectangleonClick } onMouseEnter={ props.RectangleonMouseEnter } onMouseOver={ props.RectangleonMouseOver } onKeyPress={ props.RectangleonKeyPress } onDrag={ props.RectangleonDrag } onMouseLeave={ props.RectangleonMouseLeave } onMouseUp={ props.RectangleonMouseUp } onMouseDown={ props.RectangleonMouseDown } onKeyDown={ props.RectangleonKeyDown } onChange={ props.RectangleonChange } ondelay={ props.Rectangleondelay }>
                      </div>
                    </CSSTransition>
                  </div>

                </CSSTransition>
                <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['framethreesevenseven']?.animationClass || {}}>

                  <div id="id_fourthreefour_twoofivesixtwoo" className={` frame framethreesevenseven ${ props.onClick ? 'cursor' : '' } ${ transaction['framethreesevenseven']?.type ? transaction['framethreesevenseven']?.type.toLowerCase() : '' }`} style={ { ...{}, ...props.FramethreesevensevenStyle , transitionDuration: transaction['framethreesevenseven']?.duration, transitionTimingFunction: transaction['framethreesevenseven']?.timingFunction } } onClick={ props.FramethreesevensevenonClick } onMouseEnter={ props.FramethreesevensevenonMouseEnter } onMouseOver={ props.FramethreesevensevenonMouseOver } onKeyPress={ props.FramethreesevensevenonKeyPress } onDrag={ props.FramethreesevensevenonDrag } onMouseLeave={ props.FramethreesevensevenonMouseLeave } onMouseUp={ props.FramethreesevensevenonMouseUp } onMouseDown={ props.FramethreesevensevenonMouseDown } onKeyDown={ props.FramethreesevensevenonKeyDown } onChange={ props.FramethreesevensevenonChange } ondelay={ props.Framethreesevensevenondelay }>
                    <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['textonesixpx']?.animationClass || {}}>

                      <div id="id_fourthreefour_twoofivefivethree" className={` frame textonesixpx ${ props.onClick ? 'cursor' : '' } ${ transaction['textonesixpx']?.type ? transaction['textonesixpx']?.type.toLowerCase() : '' }`} style={ { ...{}, ...props.TextonesixpxStyle , transitionDuration: transaction['textonesixpx']?.duration, transitionTimingFunction: transaction['textonesixpx']?.timingFunction } } onClick={ props.TextonesixpxonClick } onMouseEnter={ props.TextonesixpxonMouseEnter } onMouseOver={ props.TextonesixpxonMouseOver } onKeyPress={ props.TextonesixpxonKeyPress } onDrag={ props.TextonesixpxonDrag } onMouseLeave={ props.TextonesixpxonMouseLeave } onMouseUp={ props.TextonesixpxonMouseUp } onMouseDown={ props.TextonesixpxonMouseDown } onKeyDown={ props.TextonesixpxonKeyDown } onChange={ props.TextonesixpxonChange } ondelay={ props.Textonesixpxondelay }>
                        <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['subtilteonetext']?.animationClass || {}}>

                          <span id="id_fourthreefour_twoofivefivefour"  className={` text subtilteonetext    ${ props.onClick ? 'cursor' : ''}  ${ transaction['subtilteonetext']?.type ? transaction['subtilteonetext']?.type.toLowerCase() : '' }`} style={{ ...{},  ...props.SubtilteoneTextStyle , transitionDuration: transaction['subtilteonetext']?.duration, transitionTimingFunction: transaction['subtilteonetext']?.timingFunction }} onClick={ props.SubtilteoneTextonClick } onMouseEnter={ props.SubtilteoneTextonMouseEnter } onMouseOver={ props.SubtilteoneTextonMouseOver } onKeyPress={ props.SubtilteoneTextonKeyPress } onDrag={ props.SubtilteoneTextonDrag } onMouseLeave={ props.SubtilteoneTextonMouseLeave } onMouseUp={ props.SubtilteoneTextonMouseUp } onMouseDown={ props.SubtilteoneTextonMouseDown } onKeyDown={ props.SubtilteoneTextonKeyDown } onChange={ props.SubtilteoneTextonChange } ondelay={ props.SubtilteoneTextondelay } >{props.SubtilteoneText0 || `¿No tienes cuenta?`}</span>

                        </CSSTransition>
                      </div>

                    </CSSTransition>
                    <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['textlinkonesixpx']?.animationClass || {}}>
                      <TextLinkonesixpx { ...{ ...props, style:false } } variant={'Property 1=Default'} SubtilteoneText0={ props.SubtilteoneText1 || "Regístrate en Spotify" } cssClass={"C_fourthreefour_twoofivefoureight "}  />
    </CSSTransition >
                    </div>
               
    </CSSTransition >
                    </div>
               
    </CSSTransition >
                    </div>
               
    </CSSTransition >
            
            </>
        }
        </div>
    
</CSSTransition>
            </Contexts.LoginLocalContext.Provider>
        
    ) 
}

Login.propTypes = {
    style: PropTypes.any,
Uncodie_logoLogin0: PropTypes.any,
DisplaytwooText0: PropTypes.any,
PrincipalButtonText0: PropTypes.any,
PrincipalButtonText1: PropTypes.any,
Rectangleone1: PropTypes.any,
Rectangleoneundefined: PropTypes.any,
PrincipalButtonText2: PropTypes.any,
Rectangleone2: PropTypes.any,
PrincipalButtonText3: PropTypes.any,
Rectangleone3: PropTypes.any,
Rectangle0: PropTypes.any,
DeployTextInput0: PropTypes.any,
DeployGITLabelText0: PropTypes.any,
SubtilteoneText0: PropTypes.any,
Rectangle1: PropTypes.any,
SubtilteoneText1: PropTypes.any,
LoginonClick: PropTypes.any,
LoginonMouseEnter: PropTypes.any,
LoginonMouseOver: PropTypes.any,
LoginonKeyPress: PropTypes.any,
LoginonDrag: PropTypes.any,
LoginonMouseLeave: PropTypes.any,
LoginonMouseUp: PropTypes.any,
LoginonMouseDown: PropTypes.any,
LoginonKeyDown: PropTypes.any,
LoginonChange: PropTypes.any,
Loginondelay: PropTypes.any,
HeaderlOGINonClick: PropTypes.any,
HeaderlOGINonMouseEnter: PropTypes.any,
HeaderlOGINonMouseOver: PropTypes.any,
HeaderlOGINonKeyPress: PropTypes.any,
HeaderlOGINonDrag: PropTypes.any,
HeaderlOGINonMouseLeave: PropTypes.any,
HeaderlOGINonMouseUp: PropTypes.any,
HeaderlOGINonMouseDown: PropTypes.any,
HeaderlOGINonKeyDown: PropTypes.any,
HeaderlOGINonChange: PropTypes.any,
HeaderlOGINondelay: PropTypes.any,
Uncodie_logoLoginonClick: PropTypes.any,
Uncodie_logoLoginonMouseEnter: PropTypes.any,
Uncodie_logoLoginonMouseOver: PropTypes.any,
Uncodie_logoLoginonKeyPress: PropTypes.any,
Uncodie_logoLoginonDrag: PropTypes.any,
Uncodie_logoLoginonMouseLeave: PropTypes.any,
Uncodie_logoLoginonMouseUp: PropTypes.any,
Uncodie_logoLoginonMouseDown: PropTypes.any,
Uncodie_logoLoginonKeyDown: PropTypes.any,
Uncodie_logoLoginonChange: PropTypes.any,
Uncodie_logoLoginondelay: PropTypes.any,
FramethreeeightthreeonClick: PropTypes.any,
FramethreeeightthreeonMouseEnter: PropTypes.any,
FramethreeeightthreeonMouseOver: PropTypes.any,
FramethreeeightthreeonKeyPress: PropTypes.any,
FramethreeeightthreeonDrag: PropTypes.any,
FramethreeeightthreeonMouseLeave: PropTypes.any,
FramethreeeightthreeonMouseUp: PropTypes.any,
FramethreeeightthreeonMouseDown: PropTypes.any,
FramethreeeightthreeonKeyDown: PropTypes.any,
FramethreeeightthreeonChange: PropTypes.any,
Framethreeeightthreeondelay: PropTypes.any,
FramethreeeighttwooonClick: PropTypes.any,
FramethreeeighttwooonMouseEnter: PropTypes.any,
FramethreeeighttwooonMouseOver: PropTypes.any,
FramethreeeighttwooonKeyPress: PropTypes.any,
FramethreeeighttwooonDrag: PropTypes.any,
FramethreeeighttwooonMouseLeave: PropTypes.any,
FramethreeeighttwooonMouseUp: PropTypes.any,
FramethreeeighttwooonMouseDown: PropTypes.any,
FramethreeeighttwooonKeyDown: PropTypes.any,
FramethreeeighttwooonChange: PropTypes.any,
Framethreeeighttwooondelay: PropTypes.any,
FramethreeeightoneonClick: PropTypes.any,
FramethreeeightoneonMouseEnter: PropTypes.any,
FramethreeeightoneonMouseOver: PropTypes.any,
FramethreeeightoneonKeyPress: PropTypes.any,
FramethreeeightoneonDrag: PropTypes.any,
FramethreeeightoneonMouseLeave: PropTypes.any,
FramethreeeightoneonMouseUp: PropTypes.any,
FramethreeeightoneonMouseDown: PropTypes.any,
FramethreeeightoneonKeyDown: PropTypes.any,
FramethreeeightoneonChange: PropTypes.any,
Framethreeeightoneondelay: PropTypes.any,
FramethreesevenoneonClick: PropTypes.any,
FramethreesevenoneonMouseEnter: PropTypes.any,
FramethreesevenoneonMouseOver: PropTypes.any,
FramethreesevenoneonKeyPress: PropTypes.any,
FramethreesevenoneonDrag: PropTypes.any,
FramethreesevenoneonMouseLeave: PropTypes.any,
FramethreesevenoneonMouseUp: PropTypes.any,
FramethreesevenoneonMouseDown: PropTypes.any,
FramethreesevenoneonKeyDown: PropTypes.any,
FramethreesevenoneonChange: PropTypes.any,
Framethreesevenoneondelay: PropTypes.any,
FramethreefivezeroonClick: PropTypes.any,
FramethreefivezeroonMouseEnter: PropTypes.any,
FramethreefivezeroonMouseOver: PropTypes.any,
FramethreefivezeroonKeyPress: PropTypes.any,
FramethreefivezeroonDrag: PropTypes.any,
FramethreefivezeroonMouseLeave: PropTypes.any,
FramethreefivezeroonMouseUp: PropTypes.any,
FramethreefivezeroonMouseDown: PropTypes.any,
FramethreefivezeroonKeyDown: PropTypes.any,
FramethreefivezeroonChange: PropTypes.any,
Framethreefivezeroondelay: PropTypes.any,
RectangleonClick: PropTypes.any,
RectangleonMouseEnter: PropTypes.any,
RectangleonMouseOver: PropTypes.any,
RectangleonKeyPress: PropTypes.any,
RectangleonDrag: PropTypes.any,
RectangleonMouseLeave: PropTypes.any,
RectangleonMouseUp: PropTypes.any,
RectangleonMouseDown: PropTypes.any,
RectangleonKeyDown: PropTypes.any,
RectangleonChange: PropTypes.any,
Rectangleondelay: PropTypes.any,
FramethreesevensixonClick: PropTypes.any,
FramethreesevensixonMouseEnter: PropTypes.any,
FramethreesevensixonMouseOver: PropTypes.any,
FramethreesevensixonKeyPress: PropTypes.any,
FramethreesevensixonDrag: PropTypes.any,
FramethreesevensixonMouseLeave: PropTypes.any,
FramethreesevensixonMouseUp: PropTypes.any,
FramethreesevensixonMouseDown: PropTypes.any,
FramethreesevensixonKeyDown: PropTypes.any,
FramethreesevensixonChange: PropTypes.any,
Framethreesevensixondelay: PropTypes.any,
FramethreesevenfiveonClick: PropTypes.any,
FramethreesevenfiveonMouseEnter: PropTypes.any,
FramethreesevenfiveonMouseOver: PropTypes.any,
FramethreesevenfiveonKeyPress: PropTypes.any,
FramethreesevenfiveonDrag: PropTypes.any,
FramethreesevenfiveonMouseLeave: PropTypes.any,
FramethreesevenfiveonMouseUp: PropTypes.any,
FramethreesevenfiveonMouseDown: PropTypes.any,
FramethreesevenfiveonKeyDown: PropTypes.any,
FramethreesevenfiveonChange: PropTypes.any,
Framethreesevenfiveondelay: PropTypes.any,
FramethreeseventhreeonClick: PropTypes.any,
FramethreeseventhreeonMouseEnter: PropTypes.any,
FramethreeseventhreeonMouseOver: PropTypes.any,
FramethreeseventhreeonKeyPress: PropTypes.any,
FramethreeseventhreeonDrag: PropTypes.any,
FramethreeseventhreeonMouseLeave: PropTypes.any,
FramethreeseventhreeonMouseUp: PropTypes.any,
FramethreeseventhreeonMouseDown: PropTypes.any,
FramethreeseventhreeonKeyDown: PropTypes.any,
FramethreeseventhreeonChange: PropTypes.any,
Framethreeseventhreeondelay: PropTypes.any,
DeployGITLabelTextonClick: PropTypes.any,
DeployGITLabelTextonMouseEnter: PropTypes.any,
DeployGITLabelTextonMouseOver: PropTypes.any,
DeployGITLabelTextonKeyPress: PropTypes.any,
DeployGITLabelTextonDrag: PropTypes.any,
DeployGITLabelTextonMouseLeave: PropTypes.any,
DeployGITLabelTextonMouseUp: PropTypes.any,
DeployGITLabelTextonMouseDown: PropTypes.any,
DeployGITLabelTextonKeyDown: PropTypes.any,
DeployGITLabelTextonChange: PropTypes.any,
DeployGITLabelTextondelay: PropTypes.any,
FramethreeeightzeroonClick: PropTypes.any,
FramethreeeightzeroonMouseEnter: PropTypes.any,
FramethreeeightzeroonMouseOver: PropTypes.any,
FramethreeeightzeroonKeyPress: PropTypes.any,
FramethreeeightzeroonDrag: PropTypes.any,
FramethreeeightzeroonMouseLeave: PropTypes.any,
FramethreeeightzeroonMouseUp: PropTypes.any,
FramethreeeightzeroonMouseDown: PropTypes.any,
FramethreeeightzeroonKeyDown: PropTypes.any,
FramethreeeightzeroonChange: PropTypes.any,
Framethreeeightzeroondelay: PropTypes.any,
FramethreesevennigthonClick: PropTypes.any,
FramethreesevennigthonMouseEnter: PropTypes.any,
FramethreesevennigthonMouseOver: PropTypes.any,
FramethreesevennigthonKeyPress: PropTypes.any,
FramethreesevennigthonDrag: PropTypes.any,
FramethreesevennigthonMouseLeave: PropTypes.any,
FramethreesevennigthonMouseUp: PropTypes.any,
FramethreesevennigthonMouseDown: PropTypes.any,
FramethreesevennigthonKeyDown: PropTypes.any,
FramethreesevennigthonChange: PropTypes.any,
Framethreesevennigthondelay: PropTypes.any,
FramethreeseveneightonClick: PropTypes.any,
FramethreeseveneightonMouseEnter: PropTypes.any,
FramethreeseveneightonMouseOver: PropTypes.any,
FramethreeseveneightonKeyPress: PropTypes.any,
FramethreeseveneightonDrag: PropTypes.any,
FramethreeseveneightonMouseLeave: PropTypes.any,
FramethreeseveneightonMouseUp: PropTypes.any,
FramethreeseveneightonMouseDown: PropTypes.any,
FramethreeseveneightonKeyDown: PropTypes.any,
FramethreeseveneightonChange: PropTypes.any,
Framethreeseveneightondelay: PropTypes.any,
FramethreesevensevenonClick: PropTypes.any,
FramethreesevensevenonMouseEnter: PropTypes.any,
FramethreesevensevenonMouseOver: PropTypes.any,
FramethreesevensevenonKeyPress: PropTypes.any,
FramethreesevensevenonDrag: PropTypes.any,
FramethreesevensevenonMouseLeave: PropTypes.any,
FramethreesevensevenonMouseUp: PropTypes.any,
FramethreesevensevenonMouseDown: PropTypes.any,
FramethreesevensevenonKeyDown: PropTypes.any,
FramethreesevensevenonChange: PropTypes.any,
Framethreesevensevenondelay: PropTypes.any,
TextonesixpxonClick: PropTypes.any,
TextonesixpxonMouseEnter: PropTypes.any,
TextonesixpxonMouseOver: PropTypes.any,
TextonesixpxonKeyPress: PropTypes.any,
TextonesixpxonDrag: PropTypes.any,
TextonesixpxonMouseLeave: PropTypes.any,
TextonesixpxonMouseUp: PropTypes.any,
TextonesixpxonMouseDown: PropTypes.any,
TextonesixpxonKeyDown: PropTypes.any,
TextonesixpxonChange: PropTypes.any,
Textonesixpxondelay: PropTypes.any,
SubtilteoneTextonClick: PropTypes.any,
SubtilteoneTextonMouseEnter: PropTypes.any,
SubtilteoneTextonMouseOver: PropTypes.any,
SubtilteoneTextonKeyPress: PropTypes.any,
SubtilteoneTextonDrag: PropTypes.any,
SubtilteoneTextonMouseLeave: PropTypes.any,
SubtilteoneTextonMouseUp: PropTypes.any,
SubtilteoneTextonMouseDown: PropTypes.any,
SubtilteoneTextonKeyDown: PropTypes.any,
SubtilteoneTextonChange: PropTypes.any,
SubtilteoneTextondelay: PropTypes.any
}
export default Login;