import React, { Suspense } from 'react';
import PropTypes from 'prop-types';
import * as Contexts  from 'Contexts';
import { OverlaySwapContext } from 'Components/OverlaySwapProvider';
import CorebooksAnimation from 'Components/CorebooksAnimation';
import CorebooksAnimationSingle from 'Components/CorebooksAnimationSingle';

import * as UtilsScripts  from 'utils/utils';
import { Route, useHistory, useParams } from "react-router-dom";
import SingletoneNavigation from 'Class/SingletoneNavigation';

import LoadingComponent from "Components/Loading";


import { Transition } from "react-transition-group";
import { CSSTransition } from "react-transition-group";
import { useAppContext, useSessionContext } from 'context/AppContext';
import './onesixpx.css'





const onesixpx = (props)=>{
    const [state,dispatch] = useAppContext();
    const [, setNameSelectedComponent] = React.useContext(OverlaySwapContext);
    
    const nodeRef = React.useRef(null);
    const history = useHistory();
    let RouterContext = useParams();
    
    
    
    
    
    
    const [globalStateForm, dispatchForm] = React.useState({});
    
    const [ _in, setIn ] = React.useState(false)
    const [ transaction, setTransaction ] = React.useState({}) 
    const [ animation, setAnimation ] = React.useState('')

    
    
    
    
    
    
    React.useEffect(()=>{
        
        
        
    },[]);
    
    
    return (
        <>
  <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['onesixpx']?.animationClass || {}}>

    <div id="id_fivesixnigth_twoooneonenigth" ref={nodeRef} className={` ${ props.onClick ? 'cursor' : '' } onesixpx C_fivesixnigth_twoooneonenigth ${ props.cssClass } ${ transaction['onesixpx']?.type ? transaction['onesixpx']?.type.toLowerCase() : '' }`} style={ { ...{ ...{}, transitionDuration: transaction['onesixpx']?.duration, transitionTimingFunction: transaction['onesixpx']?.timingFunction }, ...props.style }} onClick={ props.OnesixpxonClick } onMouseEnter={ props.OnesixpxonMouseEnter } onMouseOver={ props.OnesixpxonMouseOver } onKeyPress={ props.OnesixpxonKeyPress } onDrag={ props.OnesixpxonDrag } onMouseLeave={ props.OnesixpxonMouseLeave } onMouseUp={ props.OnesixpxonMouseUp } onMouseDown={ props.OnesixpxonMouseDown } onKeyDown={ props.OnesixpxonKeyDown } onChange={ props.OnesixpxonChange } ondelay={ props.Onesixpxondelay }>
      {
      props.children ?
      props.children :
      <>
        <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['mvil']?.animationClass || {}}>

          <span id="id_fivesixnigth_twoooneoneeight"  className={` text mvil    ${ props.onClick ? 'cursor' : ''}  ${ transaction['mvil']?.type ? transaction['mvil']?.type.toLowerCase() : '' }`} style={{ ...{},  ...props.MVILStyle , transitionDuration: transaction['mvil']?.duration, transitionTimingFunction: transaction['mvil']?.timingFunction }} onClick={ props.MVILonClick } onMouseEnter={ props.MVILonMouseEnter } onMouseOver={ props.MVILonMouseOver } onKeyPress={ props.MVILonKeyPress } onDrag={ props.MVILonDrag } onMouseLeave={ props.MVILonMouseLeave } onMouseUp={ props.MVILonMouseUp } onMouseDown={ props.MVILonMouseDown } onKeyDown={ props.MVILonKeyDown } onChange={ props.MVILonChange } ondelay={ props.MVILondelay } >{props.MVIL0 || `MÓVIL`}</span>

        </CSSTransition>

      </>
      }
    </div>

  </CSSTransition>
</>
    ) 
}

onesixpx.propTypes = {
    style: PropTypes.any,
MVIL0: PropTypes.any,
OnesixpxonClick: PropTypes.any,
OnesixpxonMouseEnter: PropTypes.any,
OnesixpxonMouseOver: PropTypes.any,
OnesixpxonKeyPress: PropTypes.any,
OnesixpxonDrag: PropTypes.any,
OnesixpxonMouseLeave: PropTypes.any,
OnesixpxonMouseUp: PropTypes.any,
OnesixpxonMouseDown: PropTypes.any,
OnesixpxonKeyDown: PropTypes.any,
OnesixpxonChange: PropTypes.any,
Onesixpxondelay: PropTypes.any,
MVILonClick: PropTypes.any,
MVILonMouseEnter: PropTypes.any,
MVILonMouseOver: PropTypes.any,
MVILonKeyPress: PropTypes.any,
MVILonDrag: PropTypes.any,
MVILonMouseLeave: PropTypes.any,
MVILonMouseUp: PropTypes.any,
MVILonMouseDown: PropTypes.any,
MVILonKeyDown: PropTypes.any,
MVILonChange: PropTypes.any,
MVILondelay: PropTypes.any
}
export default onesixpx;