import React, { Suspense } from 'react';
import PropTypes from 'prop-types';
import * as Contexts  from 'Contexts';
import { OverlaySwapContext } from 'Components/OverlaySwapProvider';
import CorebooksAnimation from 'Components/CorebooksAnimation';
import CorebooksAnimationSingle from 'Components/CorebooksAnimationSingle';

import * as UtilsScripts  from 'utils/utils';
import { Route, useHistory, useParams } from "react-router-dom";
import SingletoneNavigation from 'Class/SingletoneNavigation';

import LoadingComponent from "Components/Loading";


import { Transition } from "react-transition-group";
import { CSSTransition } from "react-transition-group";
import { useAppContext, useSessionContext } from 'context/AppContext';
import './ButtonFacebook.css'





const ButtonFacebook = (props)=>{
    const [state,dispatch] = useAppContext();
    const [, setNameSelectedComponent] = React.useContext(OverlaySwapContext);
    
    const nodeRefpropertyonedefault = React.useRef(null);
const nodeRefpropertyonevarianttwoo = React.useRef(null);
    const history = useHistory();
    let RouterContext = useParams();
    
    
    
    
    
    
    const [globalStateForm, dispatchForm] = React.useState({});
    const [ variant, setvariant] = React.useState(props.variant || 'Property 1=Default')
    const [ _in, setIn ] = React.useState(false)
    const [ transaction, setTransaction ] = React.useState({}) 
    const [ animation, setAnimation ] = React.useState('')

    
    
    
    
    
    
    React.useEffect(()=>{
        
        
        
    },[]);
    
    
    React.useEffect(()=>{
        
        setIn(false);
        setTimeout(()=>setIn(true))
           
      },[variant]);
    const switchVariant = (value)=>{
        switch (value) {
            case 'Property 1=Default':{
            return (
            <>

  <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['propertyonedefault']?.animationClass || {}}>

    <div id="id_fourthreeeight_onetwoofourfour" ref={nodeRefpropertyonedefault} className={` cursor ${ props.onClick ? 'cursor' : '' } buttonfacebookpropertyonedefault C_fourthreeeight_onetwoofourfour ${ props.cssClass } ${ transaction['propertyonedefault']?.type ? transaction['propertyonedefault']?.type.toLowerCase() : '' }`} style={ { ...{ ...{"minWidth":"250px"}, transitionDuration: transaction['propertyonedefault']?.duration, transitionTimingFunction: transaction['propertyonedefault']?.timingFunction }, ...props.style }} onClick={ props.ButtonFacebookonClick } onMouseEnter={ props.ButtonFacebookonMouseEnter } onMouseOver={ props.ButtonFacebookonMouseOver || function(e){ setTransaction({ }); setvariant('Property 1=Variant2'); }} onKeyPress={ props.ButtonFacebookonKeyPress } onDrag={ props.ButtonFacebookonDrag } onMouseLeave={ props.ButtonFacebookonMouseLeave } onMouseUp={ props.ButtonFacebookonMouseUp } onMouseDown={ props.ButtonFacebookonMouseDown } onKeyDown={ props.ButtonFacebookonKeyDown } onChange={ props.ButtonFacebookonChange } ondelay={ props.ButtonFacebookondelay }>
      {
      props.children ?
      props.children :
      <>
        <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['framethreefourseven']?.animationClass || {}}>

          <div id="id_fourthreeeight_onetwoothreesix" className={` frame buttonfacebookframethreefourseven ${ props.onClick ? 'cursor' : '' } ${ transaction['framethreefourseven']?.type ? transaction['framethreefourseven']?.type.toLowerCase() : '' }`} style={ { ...{}, ...props.FramethreefoursevenStyle , transitionDuration: transaction['framethreefourseven']?.duration, transitionTimingFunction: transaction['framethreefourseven']?.timingFunction } } onClick={ props.FramethreefoursevenonClick } onMouseEnter={ props.FramethreefoursevenonMouseEnter } onMouseOver={ props.FramethreefoursevenonMouseOver } onKeyPress={ props.FramethreefoursevenonKeyPress } onDrag={ props.FramethreefoursevenonDrag } onMouseLeave={ props.FramethreefoursevenonMouseLeave } onMouseUp={ props.FramethreefoursevenonMouseUp } onMouseDown={ props.FramethreefoursevenonMouseDown } onKeyDown={ props.FramethreefoursevenonKeyDown } onChange={ props.FramethreefoursevenonChange } ondelay={ props.Framethreefoursevenondelay }>
            <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['rectangletwoo']?.animationClass || {}}>
              <img id="id_fourthreeeight_onetwoofourthree" className={` rectangle buttonfacebookrectangletwoo ${ props.onClick ? 'cursor' : '' } ${ transaction['rectangletwoo']?.type ? transaction['rectangletwoo']?.type.toLowerCase() : '' }`} style={{ ...{},  ...props.RectangletwooStyle , transitionDuration: transaction['rectangletwoo']?.duration, transitionTimingFunction: transaction['rectangletwoo']?.timingFunction }} onClick={ props.RectangletwooonClick } onMouseEnter={ props.RectangletwooonMouseEnter } onMouseOver={ props.RectangletwooonMouseOver } onKeyPress={ props.RectangletwooonKeyPress } onDrag={ props.RectangletwooonDrag } onMouseLeave={ props.RectangletwooonMouseLeave } onMouseUp={ props.RectangletwooonMouseUp } onMouseDown={ props.RectangletwooonMouseDown } onKeyDown={ props.RectangletwooonKeyDown } onChange={ props.RectangletwooonChange } ondelay={ props.Rectangletwooondelay } src={props.Rectangletwoo0 || "https://cdn.uncodie.com/tACQBrKCV97e0EqPE1XNLJ/9dd00b223813441d341149040a1e4991c3ff7429.png" } />
            </CSSTransition>
            <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['principalbuttontext']?.animationClass || {}}>

              <span id="id_fourthreeeight_onetwoofourtwoo"  className={` text buttonfacebookprincipalbuttontext    ${ props.onClick ? 'cursor' : ''}  ${ transaction['principalbuttontext']?.type ? transaction['principalbuttontext']?.type.toLowerCase() : '' }`} style={{ ...{"height":"14px"},  ...props.PrincipalButtonTextStyle , transitionDuration: transaction['principalbuttontext']?.duration, transitionTimingFunction: transaction['principalbuttontext']?.timingFunction }} onClick={ props.PrincipalButtonTextonClick } onMouseEnter={ props.PrincipalButtonTextonMouseEnter } onMouseOver={ props.PrincipalButtonTextonMouseOver } onKeyPress={ props.PrincipalButtonTextonKeyPress } onDrag={ props.PrincipalButtonTextonDrag } onMouseLeave={ props.PrincipalButtonTextonMouseLeave } onMouseUp={ props.PrincipalButtonTextonMouseUp } onMouseDown={ props.PrincipalButtonTextonMouseDown } onKeyDown={ props.PrincipalButtonTextonKeyDown } onChange={ props.PrincipalButtonTextonChange } ondelay={ props.PrincipalButtonTextondelay } >{props.PrincipalButtonText0 || `Registrarte con Google`}</span>

            </CSSTransition>
          </div>

        </CSSTransition>

      </>
      }
    </div>

  </CSSTransition>

</>
            )
        }
case 'Property 1=Variant2':{
            return (
            <>

  <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['propertyonevarianttwoo']?.animationClass || {}}>

    <div id="id_fourthreeeight_onetwoofoursix" ref={nodeRefpropertyonevarianttwoo} className={` cursor ${ props.onClick ? 'cursor' : '' } buttonfacebookpropertyonevarianttwoo C_fourthreeeight_onetwoofoursix ${ props.cssClass } ${ transaction['propertyonevarianttwoo']?.type ? transaction['propertyonevarianttwoo']?.type.toLowerCase() : '' }`} style={ { ...{ ...{"minWidth":"250px"}, transitionDuration: transaction['propertyonevarianttwoo']?.duration, transitionTimingFunction: transaction['propertyonevarianttwoo']?.timingFunction }, ...props.style }} onClick={ props.ButtonFacebookonClick } onMouseEnter={ props.ButtonFacebookonMouseEnter } onMouseOver={ props.ButtonFacebookonMouseOver } onKeyPress={ props.ButtonFacebookonKeyPress } onDrag={ props.ButtonFacebookonDrag } onMouseLeave={ props.ButtonFacebookonMouseLeave || function(e){ setTransaction({ }); setvariant('Property 1=Default'); }} onMouseUp={ props.ButtonFacebookonMouseUp } onMouseDown={ props.ButtonFacebookonMouseDown } onKeyDown={ props.ButtonFacebookonKeyDown } onChange={ props.ButtonFacebookonChange } ondelay={ props.ButtonFacebookondelay }>
      {
      props.children ?
      props.children :
      <>
        <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['framethreefourseven']?.animationClass || {}}>

          <div id="id_fourthreeeight_onetwoofourseven" className={` frame buttonfacebookframethreefourseven ${ props.onClick ? 'cursor' : '' } ${ transaction['framethreefourseven']?.type ? transaction['framethreefourseven']?.type.toLowerCase() : '' }`} style={ { ...{}, ...props.FramethreefoursevenStyle , transitionDuration: transaction['framethreefourseven']?.duration, transitionTimingFunction: transaction['framethreefourseven']?.timingFunction } } onClick={ props.FramethreefoursevenonClick } onMouseEnter={ props.FramethreefoursevenonMouseEnter } onMouseOver={ props.FramethreefoursevenonMouseOver } onKeyPress={ props.FramethreefoursevenonKeyPress } onDrag={ props.FramethreefoursevenonDrag } onMouseLeave={ props.FramethreefoursevenonMouseLeave } onMouseUp={ props.FramethreefoursevenonMouseUp } onMouseDown={ props.FramethreefoursevenonMouseDown } onKeyDown={ props.FramethreefoursevenonKeyDown } onChange={ props.FramethreefoursevenonChange } ondelay={ props.Framethreefoursevenondelay }>
            <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['rectangletwoo']?.animationClass || {}}>
              <img id="id_fourthreeeight_onetwoofoureight" className={` rectangle buttonfacebookrectangletwoo ${ props.onClick ? 'cursor' : '' } ${ transaction['rectangletwoo']?.type ? transaction['rectangletwoo']?.type.toLowerCase() : '' }`} style={{ ...{},  ...props.RectangletwooStyle , transitionDuration: transaction['rectangletwoo']?.duration, transitionTimingFunction: transaction['rectangletwoo']?.timingFunction }} onClick={ props.RectangletwooonClick } onMouseEnter={ props.RectangletwooonMouseEnter } onMouseOver={ props.RectangletwooonMouseOver } onKeyPress={ props.RectangletwooonKeyPress } onDrag={ props.RectangletwooonDrag } onMouseLeave={ props.RectangletwooonMouseLeave } onMouseUp={ props.RectangletwooonMouseUp } onMouseDown={ props.RectangletwooonMouseDown } onKeyDown={ props.RectangletwooonKeyDown } onChange={ props.RectangletwooonChange } ondelay={ props.Rectangletwooondelay } src={props.Rectangletwoo0 || "https://cdn.uncodie.com/tACQBrKCV97e0EqPE1XNLJ/9dd00b223813441d341149040a1e4991c3ff7429.png" } />
            </CSSTransition>
            <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['principalbuttontext']?.animationClass || {}}>

              <span id="id_fourthreeeight_onetwoofournigth"  className={` text buttonfacebookprincipalbuttontext    ${ props.onClick ? 'cursor' : ''}  ${ transaction['principalbuttontext']?.type ? transaction['principalbuttontext']?.type.toLowerCase() : '' }`} style={{ ...{"height":"14px"},  ...props.PrincipalButtonTextStyle , transitionDuration: transaction['principalbuttontext']?.duration, transitionTimingFunction: transaction['principalbuttontext']?.timingFunction }} onClick={ props.PrincipalButtonTextonClick } onMouseEnter={ props.PrincipalButtonTextonMouseEnter } onMouseOver={ props.PrincipalButtonTextonMouseOver } onKeyPress={ props.PrincipalButtonTextonKeyPress } onDrag={ props.PrincipalButtonTextonDrag } onMouseLeave={ props.PrincipalButtonTextonMouseLeave } onMouseUp={ props.PrincipalButtonTextonMouseUp } onMouseDown={ props.PrincipalButtonTextonMouseDown } onKeyDown={ props.PrincipalButtonTextonKeyDown } onChange={ props.PrincipalButtonTextonChange } ondelay={ props.PrincipalButtonTextondelay } >{props.PrincipalButtonText0 || `Registrarte con Google`}</span>

            </CSSTransition>
          </div>

        </CSSTransition>

      </>
      }
    </div>

  </CSSTransition>

</>
            )
        }
        }
    }

    return (
        <>
            {switchVariant(variant)}
        </>
    )
    
}

ButtonFacebook.propTypes = {
    style: PropTypes.any,
variant: PropTypes.any,
Rectangletwoo0: PropTypes.any,
PrincipalButtonText0: PropTypes.any,
ButtonFacebookonClick: PropTypes.any,
ButtonFacebookonMouseEnter: PropTypes.any,
ButtonFacebookonMouseOver: PropTypes.any,
ButtonFacebookonKeyPress: PropTypes.any,
ButtonFacebookonDrag: PropTypes.any,
ButtonFacebookonMouseLeave: PropTypes.any,
ButtonFacebookonMouseUp: PropTypes.any,
ButtonFacebookonMouseDown: PropTypes.any,
ButtonFacebookonKeyDown: PropTypes.any,
ButtonFacebookonChange: PropTypes.any,
ButtonFacebookondelay: PropTypes.any,
FramethreefoursevenonClick: PropTypes.any,
FramethreefoursevenonMouseEnter: PropTypes.any,
FramethreefoursevenonMouseOver: PropTypes.any,
FramethreefoursevenonKeyPress: PropTypes.any,
FramethreefoursevenonDrag: PropTypes.any,
FramethreefoursevenonMouseLeave: PropTypes.any,
FramethreefoursevenonMouseUp: PropTypes.any,
FramethreefoursevenonMouseDown: PropTypes.any,
FramethreefoursevenonKeyDown: PropTypes.any,
FramethreefoursevenonChange: PropTypes.any,
Framethreefoursevenondelay: PropTypes.any,
RectangletwooonClick: PropTypes.any,
RectangletwooonMouseEnter: PropTypes.any,
RectangletwooonMouseOver: PropTypes.any,
RectangletwooonKeyPress: PropTypes.any,
RectangletwooonDrag: PropTypes.any,
RectangletwooonMouseLeave: PropTypes.any,
RectangletwooonMouseUp: PropTypes.any,
RectangletwooonMouseDown: PropTypes.any,
RectangletwooonKeyDown: PropTypes.any,
RectangletwooonChange: PropTypes.any,
Rectangletwooondelay: PropTypes.any,
PrincipalButtonTextonClick: PropTypes.any,
PrincipalButtonTextonMouseEnter: PropTypes.any,
PrincipalButtonTextonMouseOver: PropTypes.any,
PrincipalButtonTextonKeyPress: PropTypes.any,
PrincipalButtonTextonDrag: PropTypes.any,
PrincipalButtonTextonMouseLeave: PropTypes.any,
PrincipalButtonTextonMouseUp: PropTypes.any,
PrincipalButtonTextonMouseDown: PropTypes.any,
PrincipalButtonTextonKeyDown: PropTypes.any,
PrincipalButtonTextonChange: PropTypes.any,
PrincipalButtonTextondelay: PropTypes.any
}
export default ButtonFacebook;