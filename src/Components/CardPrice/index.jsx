import React, { Suspense } from 'react';
import PropTypes from 'prop-types';
import * as Contexts  from 'Contexts';
import { OverlaySwapContext } from 'Components/OverlaySwapProvider';
import CorebooksAnimation from 'Components/CorebooksAnimation';
import CorebooksAnimationSingle from 'Components/CorebooksAnimationSingle';

import * as UtilsScripts  from 'utils/utils';
import { Route, useHistory, useParams } from "react-router-dom";
import SingletoneNavigation from 'Class/SingletoneNavigation';

import LoadingComponent from "Components/Loading";


import { Transition } from "react-transition-group";
import { CSSTransition } from "react-transition-group";
import StartButtonLarge from 'Components/StartButtonLarge'
import Framethreezerothree from 'Components/Framethreezerothree'
import Displayfive from 'Components/Displayfive'
import { useAppContext, useSessionContext } from 'context/AppContext';
import './CardPrice.css'





const CardPrice = (props)=>{
    const [state,dispatch] = useAppContext();
    const [, setNameSelectedComponent] = React.useContext(OverlaySwapContext);
    
    const nodeRef = React.useRef(null);
    const history = useHistory();
    let RouterContext = useParams();
    
    
    
    
    
    
    const [globalStateForm, dispatchForm] = React.useState({});
    
    const [ _in, setIn ] = React.useState(false)
    const [ transaction, setTransaction ] = React.useState({}) 
    const [ animation, setAnimation ] = React.useState('')

    
    
    
    
    
    
    React.useEffect(()=>{
        
        
        
    },[]);
    
    
    return (
        <>
  <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['cardprice']?.animationClass || {}}>

    <div id="id_threeonefour_onefivenigthnigth" ref={nodeRef} className={` ${ props.onClick ? 'cursor' : '' } cardprice C_threeonefour_onefivenigthnigth ${ props.cssClass } ${ transaction['cardprice']?.type ? transaction['cardprice']?.type.toLowerCase() : '' }`} style={ { ...{ ...{}, transitionDuration: transaction['cardprice']?.duration, transitionTimingFunction: transaction['cardprice']?.timingFunction }, ...props.style }} onClick={ props.CardPriceonClick } onMouseEnter={ props.CardPriceonMouseEnter } onMouseOver={ props.CardPriceonMouseOver } onKeyPress={ props.CardPriceonKeyPress } onDrag={ props.CardPriceonDrag } onMouseLeave={ props.CardPriceonMouseLeave } onMouseUp={ props.CardPriceonMouseUp } onMouseDown={ props.CardPriceonMouseDown } onKeyDown={ props.CardPriceonKeyDown } onChange={ props.CardPriceonChange } ondelay={ props.CardPriceondelay }>
      {
      props.children ?
      props.children :
      <>
        <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['framethreezerotwoo']?.animationClass || {}}>

          <div id="id_threeonefour_onefiveonezero" className={` frame framethreezerotwoo ${ props.onClick ? 'cursor' : '' } ${ transaction['framethreezerotwoo']?.type ? transaction['framethreezerotwoo']?.type.toLowerCase() : '' }`} style={ { ...{}, ...props.FramethreezerotwooStyle , transitionDuration: transaction['framethreezerotwoo']?.duration, transitionTimingFunction: transaction['framethreezerotwoo']?.timingFunction } } onClick={ props.FramethreezerotwooonClick } onMouseEnter={ props.FramethreezerotwooonMouseEnter } onMouseOver={ props.FramethreezerotwooonMouseOver } onKeyPress={ props.FramethreezerotwooonKeyPress } onDrag={ props.FramethreezerotwooonDrag } onMouseLeave={ props.FramethreezerotwooonMouseLeave } onMouseUp={ props.FramethreezerotwooonMouseUp } onMouseDown={ props.FramethreezerotwooonMouseDown } onKeyDown={ props.FramethreezerotwooonKeyDown } onChange={ props.FramethreezerotwooonChange } ondelay={ props.Framethreezerotwooondelay }>
            <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['frametwoonigthfour']?.animationClass || {}}>

              <div id="id_threeonefour_onefournigthsix" className={` frame frametwoonigthfour ${ props.onClick ? 'cursor' : '' } ${ transaction['frametwoonigthfour']?.type ? transaction['frametwoonigthfour']?.type.toLowerCase() : '' }`} style={ { ...{}, ...props.FrametwoonigthfourStyle , transitionDuration: transaction['frametwoonigthfour']?.duration, transitionTimingFunction: transaction['frametwoonigthfour']?.timingFunction } } onClick={ props.FrametwoonigthfouronClick } onMouseEnter={ props.FrametwoonigthfouronMouseEnter } onMouseOver={ props.FrametwoonigthfouronMouseOver } onKeyPress={ props.FrametwoonigthfouronKeyPress } onDrag={ props.FrametwoonigthfouronDrag } onMouseLeave={ props.FrametwoonigthfouronMouseLeave } onMouseUp={ props.FrametwoonigthfouronMouseUp } onMouseDown={ props.FrametwoonigthfouronMouseDown } onKeyDown={ props.FrametwoonigthfouronKeyDown } onChange={ props.FrametwoonigthfouronChange } ondelay={ props.Frametwoonigthfourondelay }>
                <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['twoomesesgratisconsuscripcin']?.animationClass || {}}>

                  <span id="id_threeonefour_onefoureightfive"  className={` text twoomesesgratisconsuscripcin    ${ props.onClick ? 'cursor' : ''}  ${ transaction['twoomesesgratisconsuscripcin']?.type ? transaction['twoomesesgratisconsuscripcin']?.type.toLowerCase() : '' }`} style={{ ...{},  ...props.twoomesesgratisconsuscripcinStyle , transitionDuration: transaction['twoomesesgratisconsuscripcin']?.duration, transitionTimingFunction: transaction['twoomesesgratisconsuscripcin']?.timingFunction }} onClick={ props.TwoomesesgratisconsuscripcinonClick } onMouseEnter={ props.TwoomesesgratisconsuscripcinonMouseEnter } onMouseOver={ props.TwoomesesgratisconsuscripcinonMouseOver } onKeyPress={ props.TwoomesesgratisconsuscripcinonKeyPress } onDrag={ props.TwoomesesgratisconsuscripcinonDrag } onMouseLeave={ props.TwoomesesgratisconsuscripcinonMouseLeave } onMouseUp={ props.TwoomesesgratisconsuscripcinonMouseUp } onMouseDown={ props.TwoomesesgratisconsuscripcinonMouseDown } onKeyDown={ props.TwoomesesgratisconsuscripcinonKeyDown } onChange={ props.TwoomesesgratisconsuscripcinonChange } ondelay={ props.Twoomesesgratisconsuscripcinondelay } >{props.Twoomesesgratisconsuscripcin0 || `2 meses gratis con suscripción`}</span>

                </CSSTransition>
              </div>

            </CSSTransition>
            <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['frametwoonigthfive']?.animationClass || {}}>

              <div id="id_threeonefour_onefournigthseven" className={` frame frametwoonigthfive ${ props.onClick ? 'cursor' : '' } ${ transaction['frametwoonigthfive']?.type ? transaction['frametwoonigthfive']?.type.toLowerCase() : '' }`} style={ { ...{}, ...props.FrametwoonigthfiveStyle , transitionDuration: transaction['frametwoonigthfive']?.duration, transitionTimingFunction: transaction['frametwoonigthfive']?.timingFunction } } onClick={ props.FrametwoonigthfiveonClick } onMouseEnter={ props.FrametwoonigthfiveonMouseEnter } onMouseOver={ props.FrametwoonigthfiveonMouseOver } onKeyPress={ props.FrametwoonigthfiveonKeyPress } onDrag={ props.FrametwoonigthfiveonDrag } onMouseLeave={ props.FrametwoonigthfiveonMouseLeave } onMouseUp={ props.FrametwoonigthfiveonMouseUp } onMouseDown={ props.FrametwoonigthfiveonMouseDown } onKeyDown={ props.FrametwoonigthfiveonKeyDown } onChange={ props.FrametwoonigthfiveonChange } ondelay={ props.Frametwoonigthfiveondelay }>
                <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['unpagonicodisponible']?.animationClass || {}}>

                  <span id="id_threeonefour_onefournigtheight"  className={` text unpagonicodisponible    ${ props.onClick ? 'cursor' : ''}  ${ transaction['unpagonicodisponible']?.type ? transaction['unpagonicodisponible']?.type.toLowerCase() : '' }`} style={{ ...{},  ...props.UnpagonicodisponibleStyle , transitionDuration: transaction['unpagonicodisponible']?.duration, transitionTimingFunction: transaction['unpagonicodisponible']?.timingFunction }} onClick={ props.UnpagonicodisponibleonClick } onMouseEnter={ props.UnpagonicodisponibleonMouseEnter } onMouseOver={ props.UnpagonicodisponibleonMouseOver } onKeyPress={ props.UnpagonicodisponibleonKeyPress } onDrag={ props.UnpagonicodisponibleonDrag } onMouseLeave={ props.UnpagonicodisponibleonMouseLeave } onMouseUp={ props.UnpagonicodisponibleonMouseUp } onMouseDown={ props.UnpagonicodisponibleonMouseDown } onKeyDown={ props.UnpagonicodisponibleonKeyDown } onChange={ props.UnpagonicodisponibleonChange } ondelay={ props.Unpagonicodisponibleondelay } >{props.Unpagonicodisponible0 || `Un pago único disponible`}</span>

                </CSSTransition>
              </div>

            </CSSTransition>
            <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['framethreezeroone']?.animationClass || {}}>

              <div id="id_threeonefour_onefivezeroeight" className={` frame framethreezeroone ${ props.onClick ? 'cursor' : '' } ${ transaction['framethreezeroone']?.type ? transaction['framethreezeroone']?.type.toLowerCase() : '' }`} style={ { ...{}, ...props.FramethreezerooneStyle , transitionDuration: transaction['framethreezeroone']?.duration, transitionTimingFunction: transaction['framethreezeroone']?.timingFunction } } onClick={ props.FramethreezerooneonClick } onMouseEnter={ props.FramethreezerooneonMouseEnter } onMouseOver={ props.FramethreezerooneonMouseOver } onKeyPress={ props.FramethreezerooneonKeyPress } onDrag={ props.FramethreezerooneonDrag } onMouseLeave={ props.FramethreezerooneonMouseLeave } onMouseUp={ props.FramethreezerooneonMouseUp } onMouseDown={ props.FramethreezerooneonMouseDown } onKeyDown={ props.FramethreezerooneonKeyDown } onChange={ props.FramethreezerooneonChange } ondelay={ props.Framethreezerooneondelay }>
                <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['frametwoonigtheight']?.animationClass || {}}>

                  <div id="id_threeonefour_onefivezerofive" className={` frame frametwoonigtheight ${ props.onClick ? 'cursor' : '' } ${ transaction['frametwoonigtheight']?.type ? transaction['frametwoonigtheight']?.type.toLowerCase() : '' }`} style={ { ...{}, ...props.FrametwoonigtheightStyle , transitionDuration: transaction['frametwoonigtheight']?.duration, transitionTimingFunction: transaction['frametwoonigtheight']?.timingFunction } } onClick={ props.FrametwoonigtheightonClick } onMouseEnter={ props.FrametwoonigtheightonMouseEnter } onMouseOver={ props.FrametwoonigtheightonMouseOver } onKeyPress={ props.FrametwoonigtheightonKeyPress } onDrag={ props.FrametwoonigtheightonDrag } onMouseLeave={ props.FrametwoonigtheightonMouseLeave } onMouseUp={ props.FrametwoonigtheightonMouseUp } onMouseDown={ props.FrametwoonigtheightonMouseDown } onKeyDown={ props.FrametwoonigtheightonKeyDown } onChange={ props.FrametwoonigtheightonChange } ondelay={ props.Frametwoonigtheightondelay }>
                    <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['displayfive']?.animationClass || {}}>
                      <Displayfive { ...{ ...props, style:false } } DisplayfiveText0={ props.DisplayfiveText0 || "Individual" } DisplayfiveText0={ props.DisplayfiveText0 || "Individual" } cssClass={"C_sixthreetwoo_onesixsixnigth "}  />
    </CSSTransition >
<CSSTransition 
        in={_in}
        appear={true} 
        timeout={100}
        classNames={transaction['frametwoonigthseven']?.animationClass || {}}
    >
    
                    <div id="id_threeonefour_onefivezerofour" className={` frame frametwoonigthseven ${ props.onClick ? 'cursor' : '' } ${ transaction['frametwoonigthseven']?.type ? transaction['frametwoonigthseven']?.type.toLowerCase() : '' }`} style={ { ...{}, ...props.FrametwoonigthsevenStyle , transitionDuration: transaction['frametwoonigthseven']?.duration, transitionTimingFunction: transaction['frametwoonigthseven']?.timingFunction } } onClick={ props.FrametwoonigthsevenonClick } onMouseEnter={ props.FrametwoonigthsevenonMouseEnter } onMouseOver={ props.FrametwoonigthsevenonMouseOver } onKeyPress={ props.FrametwoonigthsevenonKeyPress } onDrag={ props.FrametwoonigthsevenonDrag } onMouseLeave={ props.FrametwoonigthsevenonMouseLeave } onMouseUp={ props.FrametwoonigthsevenonMouseUp } onMouseDown={ props.FrametwoonigthsevenonMouseDown } onKeyDown={ props.FrametwoonigthsevenonKeyDown } onChange={ props.FrametwoonigthsevenonChange } ondelay={ props.Frametwoonigthsevenondelay }>
                        <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['honetext']?.animationClass || {}}>

                          <span id="id_threeonefour_onefivezerotwoo"  className={` text honetext    ${ props.onClick ? 'cursor' : ''}  ${ transaction['honetext']?.type ? transaction['honetext']?.type.toLowerCase() : '' }`} style={{ ...{},  ...props.HoneTextStyle , transitionDuration: transaction['honetext']?.duration, transitionTimingFunction: transaction['honetext']?.timingFunction }} onClick={ props.HoneTextonClick } onMouseEnter={ props.HoneTextonMouseEnter } onMouseOver={ props.HoneTextonMouseOver } onKeyPress={ props.HoneTextonKeyPress } onDrag={ props.HoneTextonDrag } onMouseLeave={ props.HoneTextonMouseLeave } onMouseUp={ props.HoneTextonMouseUp } onMouseDown={ props.HoneTextonMouseDown } onKeyDown={ props.HoneTextonKeyDown } onChange={ props.HoneTextonChange } ondelay={ props.HoneTextondelay } >{props.HoneText0 || `Después del período de la oferta, $115.00 al mes`}</span>

                        </CSSTransition>
                        <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['honetext']?.animationClass || {}}>

                          <span id="id_threeonefour_onefivezerothree"  className={` text honetext    ${ props.onClick ? 'cursor' : ''}  ${ transaction['honetext']?.type ? transaction['honetext']?.type.toLowerCase() : '' }`} style={{ ...{},  ...props.HoneTextStyle , transitionDuration: transaction['honetext']?.duration, transitionTimingFunction: transaction['honetext']?.timingFunction }} onClick={ props.HoneTextonClick } onMouseEnter={ props.HoneTextonMouseEnter } onMouseOver={ props.HoneTextonMouseOver } onKeyPress={ props.HoneTextonKeyPress } onDrag={ props.HoneTextonDrag } onMouseLeave={ props.HoneTextonMouseLeave } onMouseUp={ props.HoneTextonMouseUp } onMouseDown={ props.HoneTextonMouseDown } onKeyDown={ props.HoneTextonKeyDown } onChange={ props.HoneTextonChange } ondelay={ props.HoneTextondelay } >{props.HoneText1 || `1 cuenta`}</span>

                        </CSSTransition>
                  </div>

                </CSSTransition>
              </div>

            </CSSTransition>
          </div>

        </CSSTransition>
    </div>

  </CSSTransition>
  <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['framethreeoneone']?.animationClass || {}}>

    <div id="id_threeonefour_onefivesevenfour" className={` frame framethreeoneone ${ props.onClick ? 'cursor' : '' } ${ transaction['framethreeoneone']?.type ? transaction['framethreeoneone']?.type.toLowerCase() : '' }`} style={ { ...{}, ...props.FramethreeoneoneStyle , transitionDuration: transaction['framethreeoneone']?.duration, transitionTimingFunction: transaction['framethreeoneone']?.timingFunction } } onClick={ props.FramethreeoneoneonClick } onMouseEnter={ props.FramethreeoneoneonMouseEnter } onMouseOver={ props.FramethreeoneoneonMouseOver } onKeyPress={ props.FramethreeoneoneonKeyPress } onDrag={ props.FramethreeoneoneonDrag } onMouseLeave={ props.FramethreeoneoneonMouseLeave } onMouseUp={ props.FramethreeoneoneonMouseUp } onMouseDown={ props.FramethreeoneoneonMouseDown } onKeyDown={ props.FramethreeoneoneonKeyDown } onChange={ props.FramethreeoneoneonChange } ondelay={ props.Framethreeoneoneondelay }>
      <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['rectangle']?.animationClass || {}}>
        <div id="id_threeonetwoo_eighteightseven" className={` rectangle rectangle ${ props.onClick ? 'cursor' : '' } ${ transaction['rectangle']?.type ? transaction['rectangle']?.type.toLowerCase() : '' }`} style={{ ...{},  ...props.RectangleStyle , transitionDuration: transaction['rectangle']?.duration, transitionTimingFunction: transaction['rectangle']?.timingFunction }} onClick={ props.RectangleonClick } onMouseEnter={ props.RectangleonMouseEnter } onMouseOver={ props.RectangleonMouseOver } onKeyPress={ props.RectangleonKeyPress } onDrag={ props.RectangleonDrag } onMouseLeave={ props.RectangleonMouseLeave } onMouseUp={ props.RectangleonMouseUp } onMouseDown={ props.RectangleonMouseDown } onKeyDown={ props.RectangleonKeyDown } onChange={ props.RectangleonChange } ondelay={ props.Rectangleondelay }></div>
      </CSSTransition>
      <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['framethreezeroeight']?.animationClass || {}}>

        <div id="id_threeonefour_onefivethreethree" className={` frame framethreezeroeight ${ props.onClick ? 'cursor' : '' } ${ transaction['framethreezeroeight']?.type ? transaction['framethreezeroeight']?.type.toLowerCase() : '' }`} style={ { ...{}, ...props.FramethreezeroeightStyle , transitionDuration: transaction['framethreezeroeight']?.duration, transitionTimingFunction: transaction['framethreezeroeight']?.timingFunction } } onClick={ props.FramethreezeroeightonClick } onMouseEnter={ props.FramethreezeroeightonMouseEnter } onMouseOver={ props.FramethreezeroeightonMouseOver } onKeyPress={ props.FramethreezeroeightonKeyPress } onDrag={ props.FramethreezeroeightonDrag } onMouseLeave={ props.FramethreezeroeightonMouseLeave } onMouseUp={ props.FramethreezeroeightonMouseUp } onMouseDown={ props.FramethreezeroeightonMouseDown } onKeyDown={ props.FramethreezeroeightonKeyDown } onChange={ props.FramethreezeroeightonChange } ondelay={ props.Framethreezeroeightondelay }>
          <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['framethreezerofour']?.animationClass || {}}>
            <Framethreezerothree { ...{ ...props, style:false } } Escuchamsicasinanuncios0={ props.Escuchamsicasinanuncios0 || "Escucha música sin anuncios" } cssClass={"C_threeonefour_onefiveoneseven "}  />
    </CSSTransition >
<CSSTransition 
        in={_in}
        appear={true} 
        timeout={100}
        classNames={transaction['framethreezerofive']?.animationClass || {}}
    >
    <Framethreezerothree { ...{ ...props, style:false } }   Escuchamsicasinanuncios0={ props.Escuchamsicasinanuncios1 || " Reproduce tus canciones en cualquier lugar, incluso en offline"} cssClass={"C_threeonefour_onefivetwooone "}  />
    </CSSTransition >
<CSSTransition 
        in={_in}
        appear={true} 
        timeout={100}
        classNames={transaction['framethreezerosix']?.animationClass || {}}
    >
    <Framethreezerothree { ...{ ...props, style:false } }   Escuchamsicasinanuncios0={ props.Escuchamsicasinanuncios2 || " Reproduce contenido on-demand"} cssClass={"C_threeonefour_onefivetwoofive "}  />
    </CSSTransition >
<CSSTransition 
        in={_in}
        appear={true} 
        timeout={100}
        classNames={transaction['framethreezeroseven']?.animationClass || {}}
    >
    <Framethreezerothree { ...{ ...props, style:false } }   Escuchamsicasinanuncios0={ props.Escuchamsicasinanuncios3 || " Elige la opción prepaga o suscríbete"} cssClass={"C_threeonefour_onefivetwoonigth "}  />
    </CSSTransition >
<CSSTransition 
        in={_in}
        appear={true} 
        timeout={100}
        classNames={transaction['framethreezeroeight']?.animationClass || {}}
    >
    <Framethreezerothree { ...{ ...props, style:false } }   Escuchamsicasinanuncios0={ props.Escuchamsicasinanuncios4 || " Elige la opción prepaga o suscríbete"} cssClass={"C_threeonefour_onefivefoursix "}  />
    </CSSTransition >
                    </div>
               
    </CSSTransition >
                    </div>
               
    </CSSTransition >
<CSSTransition 
        in={_in}
        appear={true} 
        timeout={100}
        classNames={transaction['framethreezeronigth']?.animationClass || {}}
    >
    
                    <div id="id_threeonefour_onefivesixnigth" className={` frame framethreezeronigth ${ props.onClick ? 'cursor' : '' } ${ transaction['framethreezeronigth']?.type ? transaction['framethreezeronigth']?.type.toLowerCase() : '' }`} style={ { ...{}, ...props.FramethreezeronigthStyle , transitionDuration: transaction['framethreezeronigth']?.duration, transitionTimingFunction: transaction['framethreezeronigth']?.timingFunction } } onClick={ props.FramethreezeronigthonClick } onMouseEnter={ props.FramethreezeronigthonMouseEnter } onMouseOver={ props.FramethreezeronigthonMouseOver } onKeyPress={ props.FramethreezeronigthonKeyPress } onDrag={ props.FramethreezeronigthonDrag } onMouseLeave={ props.FramethreezeronigthonMouseLeave } onMouseUp={ props.FramethreezeronigthonMouseUp } onMouseDown={ props.FramethreezeronigthonMouseDown } onKeyDown={ props.FramethreezeronigthonKeyDown } onChange={ props.FramethreezeronigthonChange } ondelay={ props.Framethreezeronigthondelay }>
              <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['startbuttonlarge']?.animationClass || {}}>
                <StartButtonLarge { ...{ ...props, style:false } } variant={'Property 1=Default'} PrincipalButtonText0={ props.PrincipalButtonText0 || "EMPEZAR" } cssClass={"C_threeonefour_onefivenigthtwoo "}  />
    </CSSTransition >
<CSSTransition 
        in={_in}
        appear={true} 
        timeout={100}
        classNames={transaction['frametwooseventwoo']?.animationClass || {}}
    >
    
                    <div id="id_threeonefour_onefivefiveseven" className={` frame frametwooseventwoo ${ props.onClick ? 'cursor' : '' } ${ transaction['frametwooseventwoo']?.type ? transaction['frametwooseventwoo']?.type.toLowerCase() : '' }`} style={ { ...{}, ...props.FrametwooseventwooStyle , transitionDuration: transaction['frametwooseventwoo']?.duration, transitionTimingFunction: transaction['frametwooseventwoo']?.timingFunction } } onClick={ props.FrametwooseventwooonClick } onMouseEnter={ props.FrametwooseventwooonMouseEnter } onMouseOver={ props.FrametwooseventwooonMouseOver } onKeyPress={ props.FrametwooseventwooonKeyPress } onDrag={ props.FrametwooseventwooonDrag } onMouseLeave={ props.FrametwooseventwooonMouseLeave } onMouseUp={ props.FrametwooseventwooonMouseUp } onMouseDown={ props.FrametwooseventwooonMouseDown } onKeyDown={ props.FrametwooseventwooonKeyDown } onChange={ props.FrametwooseventwooonChange } ondelay={ props.Frametwooseventwooondelay }>
                  <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['termsandcondition']?.animationClass || {}}>

                    <div id="id_threeonefour_onefivefiveeight" className={` frame termsandcondition ${ props.onClick ? 'cursor' : '' } ${ transaction['termsandcondition']?.type ? transaction['termsandcondition']?.type.toLowerCase() : '' }`} style={ { ...{}, ...props.TermsandconditionStyle , transitionDuration: transaction['termsandcondition']?.duration, transitionTimingFunction: transaction['termsandcondition']?.timingFunction } } onClick={ props.TermsandconditiononClick } onMouseEnter={ props.TermsandconditiononMouseEnter } onMouseOver={ props.TermsandconditiononMouseOver } onKeyPress={ props.TermsandconditiononKeyPress } onDrag={ props.TermsandconditiononDrag } onMouseLeave={ props.TermsandconditiononMouseLeave } onMouseUp={ props.TermsandconditiononMouseUp } onMouseDown={ props.TermsandconditiononMouseDown } onKeyDown={ props.TermsandconditiononKeyDown } onChange={ props.TermsandconditiononChange } ondelay={ props.Termsandconditionondelay }>
                      <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['honetext']?.animationClass || {}}>

                        <span id="id_threeonefour_onefivefivenigth"  className={` text honetext    ${ props.onClick ? 'cursor' : ''}  ${ transaction['honetext']?.type ? transaction['honetext']?.type.toLowerCase() : '' }`} style={{ ...{},  ...props.HoneTextStyle , transitionDuration: transaction['honetext']?.duration, transitionTimingFunction: transaction['honetext']?.timingFunction }} onClick={ props.HoneTextonClick } onMouseEnter={ props.HoneTextonMouseEnter } onMouseOver={ props.HoneTextonMouseOver } onKeyPress={ props.HoneTextonKeyPress } onDrag={ props.HoneTextonDrag } onMouseLeave={ props.HoneTextonMouseLeave } onMouseUp={ props.HoneTextonMouseUp } onMouseDown={ props.HoneTextonMouseDown } onKeyDown={ props.HoneTextonKeyDown } onChange={ props.HoneTextonChange } ondelay={ props.HoneTextondelay } >{props.HoneText2 || `Se aplican Términos y Condiciones.`}</span>

                      </CSSTransition>
                    </div>

                  </CSSTransition>
                  <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['termsandcondition']?.animationClass || {}}>

                    <div id="id_threeonefour_onefivesixzero" className={` frame termsandcondition ${ props.onClick ? 'cursor' : '' } ${ transaction['termsandcondition']?.type ? transaction['termsandcondition']?.type.toLowerCase() : '' }`} style={ { ...{}, ...props.TermsandconditionStyle , transitionDuration: transaction['termsandcondition']?.duration, transitionTimingFunction: transaction['termsandcondition']?.timingFunction } } onClick={ props.TermsandconditiononClick } onMouseEnter={ props.TermsandconditiononMouseEnter } onMouseOver={ props.TermsandconditiononMouseOver } onKeyPress={ props.TermsandconditiononKeyPress } onDrag={ props.TermsandconditiononDrag } onMouseLeave={ props.TermsandconditiononMouseLeave } onMouseUp={ props.TermsandconditiononMouseUp } onMouseDown={ props.TermsandconditiononMouseDown } onKeyDown={ props.TermsandconditiononKeyDown } onChange={ props.TermsandconditiononChange } ondelay={ props.Termsandconditionondelay }>
                      <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['honetext']?.animationClass || {}}>

                        <span id="id_threeonefour_onefivesixone"  className={` text honetext    ${ props.onClick ? 'cursor' : ''}  ${ transaction['honetext']?.type ? transaction['honetext']?.type.toLowerCase() : '' }`} style={{ ...{},  ...props.HoneTextStyle , transitionDuration: transaction['honetext']?.duration, transitionTimingFunction: transaction['honetext']?.timingFunction }} onClick={ props.HoneTextonClick } onMouseEnter={ props.HoneTextonMouseEnter } onMouseOver={ props.HoneTextonMouseOver } onKeyPress={ props.HoneTextonKeyPress } onDrag={ props.HoneTextonDrag } onMouseLeave={ props.HoneTextonMouseLeave } onMouseUp={ props.HoneTextonMouseUp } onMouseDown={ props.HoneTextonMouseDown } onKeyDown={ props.HoneTextonKeyDown } onChange={ props.HoneTextonChange } ondelay={ props.HoneTextondelay } >{props.HoneText3 || `Los 2 meses gratis no están disponibles para los usuarios que ya hayan probado Premium.`}</span>

                      </CSSTransition>
                    </div>

                  </CSSTransition>
        </div>

      </CSSTransition>
    </div>

  </CSSTransition>

</>
}
</div>

</CSSTransition>
</>
    ) 
}

CardPrice.propTypes = {
    style: PropTypes.any,
Twoomesesgratisconsuscripcin0: PropTypes.any,
Unpagonicodisponible0: PropTypes.any,
DisplayfiveText0: PropTypes.any,
HoneText0: PropTypes.any,
HoneText1: PropTypes.any,
Rectangle0: PropTypes.any,
Escuchamsicasinanuncios0: PropTypes.any,
Escuchamsicasinanuncios1: PropTypes.any,
Escuchamsicasinanuncios2: PropTypes.any,
Escuchamsicasinanuncios3: PropTypes.any,
Escuchamsicasinanuncios4: PropTypes.any,
PrincipalButtonText0: PropTypes.any,
HoneText2: PropTypes.any,
HoneText3: PropTypes.any,
CardPriceonClick: PropTypes.any,
CardPriceonMouseEnter: PropTypes.any,
CardPriceonMouseOver: PropTypes.any,
CardPriceonKeyPress: PropTypes.any,
CardPriceonDrag: PropTypes.any,
CardPriceonMouseLeave: PropTypes.any,
CardPriceonMouseUp: PropTypes.any,
CardPriceonMouseDown: PropTypes.any,
CardPriceonKeyDown: PropTypes.any,
CardPriceonChange: PropTypes.any,
CardPriceondelay: PropTypes.any,
FramethreezerotwooonClick: PropTypes.any,
FramethreezerotwooonMouseEnter: PropTypes.any,
FramethreezerotwooonMouseOver: PropTypes.any,
FramethreezerotwooonKeyPress: PropTypes.any,
FramethreezerotwooonDrag: PropTypes.any,
FramethreezerotwooonMouseLeave: PropTypes.any,
FramethreezerotwooonMouseUp: PropTypes.any,
FramethreezerotwooonMouseDown: PropTypes.any,
FramethreezerotwooonKeyDown: PropTypes.any,
FramethreezerotwooonChange: PropTypes.any,
Framethreezerotwooondelay: PropTypes.any,
FrametwoonigthfouronClick: PropTypes.any,
FrametwoonigthfouronMouseEnter: PropTypes.any,
FrametwoonigthfouronMouseOver: PropTypes.any,
FrametwoonigthfouronKeyPress: PropTypes.any,
FrametwoonigthfouronDrag: PropTypes.any,
FrametwoonigthfouronMouseLeave: PropTypes.any,
FrametwoonigthfouronMouseUp: PropTypes.any,
FrametwoonigthfouronMouseDown: PropTypes.any,
FrametwoonigthfouronKeyDown: PropTypes.any,
FrametwoonigthfouronChange: PropTypes.any,
Frametwoonigthfourondelay: PropTypes.any,
TwoomesesgratisconsuscripcinonClick: PropTypes.any,
TwoomesesgratisconsuscripcinonMouseEnter: PropTypes.any,
TwoomesesgratisconsuscripcinonMouseOver: PropTypes.any,
TwoomesesgratisconsuscripcinonKeyPress: PropTypes.any,
TwoomesesgratisconsuscripcinonDrag: PropTypes.any,
TwoomesesgratisconsuscripcinonMouseLeave: PropTypes.any,
TwoomesesgratisconsuscripcinonMouseUp: PropTypes.any,
TwoomesesgratisconsuscripcinonMouseDown: PropTypes.any,
TwoomesesgratisconsuscripcinonKeyDown: PropTypes.any,
TwoomesesgratisconsuscripcinonChange: PropTypes.any,
Twoomesesgratisconsuscripcinondelay: PropTypes.any,
FrametwoonigthfiveonClick: PropTypes.any,
FrametwoonigthfiveonMouseEnter: PropTypes.any,
FrametwoonigthfiveonMouseOver: PropTypes.any,
FrametwoonigthfiveonKeyPress: PropTypes.any,
FrametwoonigthfiveonDrag: PropTypes.any,
FrametwoonigthfiveonMouseLeave: PropTypes.any,
FrametwoonigthfiveonMouseUp: PropTypes.any,
FrametwoonigthfiveonMouseDown: PropTypes.any,
FrametwoonigthfiveonKeyDown: PropTypes.any,
FrametwoonigthfiveonChange: PropTypes.any,
Frametwoonigthfiveondelay: PropTypes.any,
UnpagonicodisponibleonClick: PropTypes.any,
UnpagonicodisponibleonMouseEnter: PropTypes.any,
UnpagonicodisponibleonMouseOver: PropTypes.any,
UnpagonicodisponibleonKeyPress: PropTypes.any,
UnpagonicodisponibleonDrag: PropTypes.any,
UnpagonicodisponibleonMouseLeave: PropTypes.any,
UnpagonicodisponibleonMouseUp: PropTypes.any,
UnpagonicodisponibleonMouseDown: PropTypes.any,
UnpagonicodisponibleonKeyDown: PropTypes.any,
UnpagonicodisponibleonChange: PropTypes.any,
Unpagonicodisponibleondelay: PropTypes.any,
FramethreezerooneonClick: PropTypes.any,
FramethreezerooneonMouseEnter: PropTypes.any,
FramethreezerooneonMouseOver: PropTypes.any,
FramethreezerooneonKeyPress: PropTypes.any,
FramethreezerooneonDrag: PropTypes.any,
FramethreezerooneonMouseLeave: PropTypes.any,
FramethreezerooneonMouseUp: PropTypes.any,
FramethreezerooneonMouseDown: PropTypes.any,
FramethreezerooneonKeyDown: PropTypes.any,
FramethreezerooneonChange: PropTypes.any,
Framethreezerooneondelay: PropTypes.any,
FrametwoonigtheightonClick: PropTypes.any,
FrametwoonigtheightonMouseEnter: PropTypes.any,
FrametwoonigtheightonMouseOver: PropTypes.any,
FrametwoonigtheightonKeyPress: PropTypes.any,
FrametwoonigtheightonDrag: PropTypes.any,
FrametwoonigtheightonMouseLeave: PropTypes.any,
FrametwoonigtheightonMouseUp: PropTypes.any,
FrametwoonigtheightonMouseDown: PropTypes.any,
FrametwoonigtheightonKeyDown: PropTypes.any,
FrametwoonigtheightonChange: PropTypes.any,
Frametwoonigtheightondelay: PropTypes.any,
FrametwoonigthsevenonClick: PropTypes.any,
FrametwoonigthsevenonMouseEnter: PropTypes.any,
FrametwoonigthsevenonMouseOver: PropTypes.any,
FrametwoonigthsevenonKeyPress: PropTypes.any,
FrametwoonigthsevenonDrag: PropTypes.any,
FrametwoonigthsevenonMouseLeave: PropTypes.any,
FrametwoonigthsevenonMouseUp: PropTypes.any,
FrametwoonigthsevenonMouseDown: PropTypes.any,
FrametwoonigthsevenonKeyDown: PropTypes.any,
FrametwoonigthsevenonChange: PropTypes.any,
Frametwoonigthsevenondelay: PropTypes.any,
HoneTextonClick: PropTypes.any,
HoneTextonMouseEnter: PropTypes.any,
HoneTextonMouseOver: PropTypes.any,
HoneTextonKeyPress: PropTypes.any,
HoneTextonDrag: PropTypes.any,
HoneTextonMouseLeave: PropTypes.any,
HoneTextonMouseUp: PropTypes.any,
HoneTextonMouseDown: PropTypes.any,
HoneTextonKeyDown: PropTypes.any,
HoneTextonChange: PropTypes.any,
HoneTextondelay: PropTypes.any,
FramethreeoneoneonClick: PropTypes.any,
FramethreeoneoneonMouseEnter: PropTypes.any,
FramethreeoneoneonMouseOver: PropTypes.any,
FramethreeoneoneonKeyPress: PropTypes.any,
FramethreeoneoneonDrag: PropTypes.any,
FramethreeoneoneonMouseLeave: PropTypes.any,
FramethreeoneoneonMouseUp: PropTypes.any,
FramethreeoneoneonMouseDown: PropTypes.any,
FramethreeoneoneonKeyDown: PropTypes.any,
FramethreeoneoneonChange: PropTypes.any,
Framethreeoneoneondelay: PropTypes.any,
RectangleonClick: PropTypes.any,
RectangleonMouseEnter: PropTypes.any,
RectangleonMouseOver: PropTypes.any,
RectangleonKeyPress: PropTypes.any,
RectangleonDrag: PropTypes.any,
RectangleonMouseLeave: PropTypes.any,
RectangleonMouseUp: PropTypes.any,
RectangleonMouseDown: PropTypes.any,
RectangleonKeyDown: PropTypes.any,
RectangleonChange: PropTypes.any,
Rectangleondelay: PropTypes.any,
FramethreezeroeightonClick: PropTypes.any,
FramethreezeroeightonMouseEnter: PropTypes.any,
FramethreezeroeightonMouseOver: PropTypes.any,
FramethreezeroeightonKeyPress: PropTypes.any,
FramethreezeroeightonDrag: PropTypes.any,
FramethreezeroeightonMouseLeave: PropTypes.any,
FramethreezeroeightonMouseUp: PropTypes.any,
FramethreezeroeightonMouseDown: PropTypes.any,
FramethreezeroeightonKeyDown: PropTypes.any,
FramethreezeroeightonChange: PropTypes.any,
Framethreezeroeightondelay: PropTypes.any,
FramethreezeronigthonClick: PropTypes.any,
FramethreezeronigthonMouseEnter: PropTypes.any,
FramethreezeronigthonMouseOver: PropTypes.any,
FramethreezeronigthonKeyPress: PropTypes.any,
FramethreezeronigthonDrag: PropTypes.any,
FramethreezeronigthonMouseLeave: PropTypes.any,
FramethreezeronigthonMouseUp: PropTypes.any,
FramethreezeronigthonMouseDown: PropTypes.any,
FramethreezeronigthonKeyDown: PropTypes.any,
FramethreezeronigthonChange: PropTypes.any,
Framethreezeronigthondelay: PropTypes.any,
FrametwooseventwooonClick: PropTypes.any,
FrametwooseventwooonMouseEnter: PropTypes.any,
FrametwooseventwooonMouseOver: PropTypes.any,
FrametwooseventwooonKeyPress: PropTypes.any,
FrametwooseventwooonDrag: PropTypes.any,
FrametwooseventwooonMouseLeave: PropTypes.any,
FrametwooseventwooonMouseUp: PropTypes.any,
FrametwooseventwooonMouseDown: PropTypes.any,
FrametwooseventwooonKeyDown: PropTypes.any,
FrametwooseventwooonChange: PropTypes.any,
Frametwooseventwooondelay: PropTypes.any,
TermsandconditiononClick: PropTypes.any,
TermsandconditiononMouseEnter: PropTypes.any,
TermsandconditiononMouseOver: PropTypes.any,
TermsandconditiononKeyPress: PropTypes.any,
TermsandconditiononDrag: PropTypes.any,
TermsandconditiononMouseLeave: PropTypes.any,
TermsandconditiononMouseUp: PropTypes.any,
TermsandconditiononMouseDown: PropTypes.any,
TermsandconditiononKeyDown: PropTypes.any,
TermsandconditiononChange: PropTypes.any,
Termsandconditionondelay: PropTypes.any
}
export default CardPrice;