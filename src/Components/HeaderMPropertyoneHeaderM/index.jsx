import React, { Suspense } from 'react';
import PropTypes from 'prop-types';
import * as Contexts  from 'Contexts';
import { OverlaySwapContext } from 'Components/OverlaySwapProvider';
import CorebooksAnimation from 'Components/CorebooksAnimation';
import CorebooksAnimationSingle from 'Components/CorebooksAnimationSingle';

import * as UtilsScripts  from 'utils/utils';
import { Route, useHistory, useParams } from "react-router-dom";
import SingletoneNavigation from 'Class/SingletoneNavigation';

import LoadingComponent from "Components/Loading";


import { Transition } from "react-transition-group";
import { CSSTransition } from "react-transition-group";
import { useAppContext, useSessionContext } from 'context/AppContext';
import './HeaderMPropertyoneHeaderM.css'





const PropertyoneHeaderM = (props)=>{
    const [state,dispatch] = useAppContext();
    const [, setNameSelectedComponent] = React.useContext(OverlaySwapContext);
    
    const nodeRef = React.useRef(null);
    const history = useHistory();
    let RouterContext = useParams();
    
    
    
    
    
    
    const [globalStateForm, dispatchForm] = React.useState({});
    
    const [ _in, setIn ] = React.useState(false)
    const [ transaction, setTransaction ] = React.useState({}) 
    const [ animation, setAnimation ] = React.useState('')

    
    
    
    
    
    
    React.useEffect(()=>{
        
        
        
    },[]);
    
    
    return (
        <>
  <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['propertyoneheaderm']?.animationClass || {}}>

    <div id="id_twoofiveeight_onesixfourzero" ref={nodeRef} className={` ${ props.onClick ? 'cursor' : '' } propertyoneheaderm C_twoofiveeight_onesixfourzero ${ props.cssClass } ${ transaction['propertyoneheaderm']?.type ? transaction['propertyoneheaderm']?.type.toLowerCase() : '' }`} style={ { ...{ ...{"marginBottom":"-100px"}, transitionDuration: transaction['propertyoneheaderm']?.duration, transitionTimingFunction: transaction['propertyoneheaderm']?.timingFunction }, ...props.style }} onClick={ props.HeaderMonClick } onMouseEnter={ props.HeaderMonMouseEnter } onMouseOver={ props.HeaderMonMouseOver } onKeyPress={ props.HeaderMonKeyPress } onDrag={ props.HeaderMonDrag } onMouseLeave={ props.HeaderMonMouseLeave } onMouseUp={ props.HeaderMonMouseUp } onMouseDown={ props.HeaderMonMouseDown } onKeyDown={ props.HeaderMonKeyDown } onChange={ props.HeaderMonChange } ondelay={ props.HeaderMondelay }>
      {
      props.children ?
      props.children :
      <>
        <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['uncodie_logom']?.animationClass || {}}>
          <img id="id_twoofiveeight_onesixfourone" className={` rectangle uncodie_logom ${ props.onClick ? 'cursor' : '' } ${ transaction['uncodie_logom']?.type ? transaction['uncodie_logom']?.type.toLowerCase() : '' }`} style={{ ...{},  ...props.Uncodie_logoMStyle , transitionDuration: transaction['uncodie_logom']?.duration, transitionTimingFunction: transaction['uncodie_logom']?.timingFunction }} onClick={ props.Uncodie_logoMonClick } onMouseEnter={ props.Uncodie_logoMonMouseEnter } onMouseOver={ props.Uncodie_logoMonMouseOver } onKeyPress={ props.Uncodie_logoMonKeyPress } onDrag={ props.Uncodie_logoMonDrag } onMouseLeave={ props.Uncodie_logoMonMouseLeave } onMouseUp={ props.Uncodie_logoMonMouseUp } onMouseDown={ props.Uncodie_logoMonMouseDown } onKeyDown={ props.Uncodie_logoMonKeyDown } onChange={ props.Uncodie_logoMonChange } ondelay={ props.Uncodie_logoMondelay } src={props.Uncodie_logoM0 || "https://cdn.uncodie.com/tACQBrKCV97e0EqPE1XNLJ/efbb152934f48228779350567e3e051c143ce0f2.png" } />
        </CSSTransition>
        <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['iconmenu']?.animationClass || {}}>
          <img id="id_twoofiveeight_onesixfourtwoo" className={` rectangle iconmenu cursor ${ props.onClick ? 'cursor' : '' } ${ transaction['iconmenu']?.type ? transaction['iconmenu']?.type.toLowerCase() : '' }`} style={{ ...{},  ...props.IconMenuStyle , transitionDuration: transaction['iconmenu']?.duration, transitionTimingFunction: transaction['iconmenu']?.timingFunction }} onClick={ props.IconMenuonClick || function(e){ setTransaction({ }); setvariant('Property1=HeaderExtM'); }} onMouseEnter={ props.IconMenuonMouseEnter } onMouseOver={ props.IconMenuonMouseOver } onKeyPress={ props.IconMenuonKeyPress } onDrag={ props.IconMenuonDrag } onMouseLeave={ props.IconMenuonMouseLeave } onMouseUp={ props.IconMenuonMouseUp } onMouseDown={ props.IconMenuonMouseDown } onKeyDown={ props.IconMenuonKeyDown } onChange={ props.IconMenuonChange } ondelay={ props.IconMenuondelay } src={props.IconMenu0 || "https://cdn.uncodie.com/tACQBrKCV97e0EqPE1XNLJ/d49df48697b10d4caaddceeab09c71d040761bdf.png" } />
        </CSSTransition>

      </>
      }
    </div>

  </CSSTransition>
</>
    ) 
}

PropertyoneHeaderM.propTypes = {
    style: PropTypes.any,
Uncodie_logoM0: PropTypes.any,
IconMenu0: PropTypes.any,
HeaderMonClick: PropTypes.any,
HeaderMonMouseEnter: PropTypes.any,
HeaderMonMouseOver: PropTypes.any,
HeaderMonKeyPress: PropTypes.any,
HeaderMonDrag: PropTypes.any,
HeaderMonMouseLeave: PropTypes.any,
HeaderMonMouseUp: PropTypes.any,
HeaderMonMouseDown: PropTypes.any,
HeaderMonKeyDown: PropTypes.any,
HeaderMonChange: PropTypes.any,
HeaderMondelay: PropTypes.any,
Uncodie_logoMonClick: PropTypes.any,
Uncodie_logoMonMouseEnter: PropTypes.any,
Uncodie_logoMonMouseOver: PropTypes.any,
Uncodie_logoMonKeyPress: PropTypes.any,
Uncodie_logoMonDrag: PropTypes.any,
Uncodie_logoMonMouseLeave: PropTypes.any,
Uncodie_logoMonMouseUp: PropTypes.any,
Uncodie_logoMonMouseDown: PropTypes.any,
Uncodie_logoMonKeyDown: PropTypes.any,
Uncodie_logoMonChange: PropTypes.any,
Uncodie_logoMondelay: PropTypes.any,
IconMenuonClick: PropTypes.any,
IconMenuonMouseEnter: PropTypes.any,
IconMenuonMouseOver: PropTypes.any,
IconMenuonKeyPress: PropTypes.any,
IconMenuonDrag: PropTypes.any,
IconMenuonMouseLeave: PropTypes.any,
IconMenuonMouseUp: PropTypes.any,
IconMenuonMouseDown: PropTypes.any,
IconMenuonKeyDown: PropTypes.any,
IconMenuonChange: PropTypes.any,
IconMenuondelay: PropTypes.any
}
export default PropertyoneHeaderM;