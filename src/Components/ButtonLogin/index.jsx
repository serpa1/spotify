import React, { Suspense } from 'react';
import PropTypes from 'prop-types';
import * as Contexts  from 'Contexts';
import { OverlaySwapContext } from 'Components/OverlaySwapProvider';
import CorebooksAnimation from 'Components/CorebooksAnimation';
import CorebooksAnimationSingle from 'Components/CorebooksAnimationSingle';

import * as UtilsScripts  from 'utils/utils';
import { Route, useHistory, useParams } from "react-router-dom";
import SingletoneNavigation from 'Class/SingletoneNavigation';

import LoadingComponent from "Components/Loading";


import { Transition } from "react-transition-group";
import { CSSTransition } from "react-transition-group";
import { useAppContext, useSessionContext } from 'context/AppContext';
import './ButtonLogin.css'





const ButtonLogin = (props)=>{
    const [state,dispatch] = useAppContext();
    const [, setNameSelectedComponent] = React.useContext(OverlaySwapContext);
    
    const nodeRefpropertyonedefault = React.useRef(null);
const nodeRefpropertyonevarianttwoo = React.useRef(null);
    const history = useHistory();
    let RouterContext = useParams();
    
    
    
    
    
    
    const [globalStateForm, dispatchForm] = React.useState({});
    const [ variant, setvariant] = React.useState(props.variant || 'Property 1=Default')
    const [ _in, setIn ] = React.useState(false)
    const [ transaction, setTransaction ] = React.useState({}) 
    const [ animation, setAnimation ] = React.useState('')

    
    
    
    
    
    
    React.useEffect(()=>{
        
        
        
    },[]);
    
    
    React.useEffect(()=>{
        
        setIn(false);
        setTimeout(()=>setIn(true))
           
      },[variant]);
    const switchVariant = (value)=>{
        switch (value) {
            case 'Property 1=Default':{
            return (
            <>

  <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['propertyonedefault']?.animationClass || {}}>

    <div id="id_fourthreefour_twootwoofournigth" ref={nodeRefpropertyonedefault} className={` cursor ${ props.onClick ? 'cursor' : '' } buttonloginpropertyonedefault C_fourthreefour_twootwoofournigth ${ props.cssClass } ${ transaction['propertyonedefault']?.type ? transaction['propertyonedefault']?.type.toLowerCase() : '' }`} style={ { ...{ ...{"minWidth":"250px"}, transitionDuration: transaction['propertyonedefault']?.duration, transitionTimingFunction: transaction['propertyonedefault']?.timingFunction }, ...props.style }} onClick={ props.ButtonLoginonClick } onMouseEnter={ props.ButtonLoginonMouseEnter } onMouseOver={ props.ButtonLoginonMouseOver || function(e){ setTransaction({ }); setvariant('Property 1=Variant2'); }} onKeyPress={ props.ButtonLoginonKeyPress } onDrag={ props.ButtonLoginonDrag } onMouseLeave={ props.ButtonLoginonMouseLeave } onMouseUp={ props.ButtonLoginonMouseUp } onMouseDown={ props.ButtonLoginonMouseDown } onKeyDown={ props.ButtonLoginonKeyDown } onChange={ props.ButtonLoginonChange } ondelay={ props.ButtonLoginondelay }>
      {
      props.children ?
      props.children :
      <>
        <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['framethreefourseven']?.animationClass || {}}>

          <div id="id_fourthreefour_twootwootwoozero" className={` frame buttonloginframethreefourseven ${ props.onClick ? 'cursor' : '' } ${ transaction['framethreefourseven']?.type ? transaction['framethreefourseven']?.type.toLowerCase() : '' }`} style={ { ...{}, ...props.FramethreefoursevenStyle , transitionDuration: transaction['framethreefourseven']?.duration, transitionTimingFunction: transaction['framethreefourseven']?.timingFunction } } onClick={ props.FramethreefoursevenonClick } onMouseEnter={ props.FramethreefoursevenonMouseEnter } onMouseOver={ props.FramethreefoursevenonMouseOver } onKeyPress={ props.FramethreefoursevenonKeyPress } onDrag={ props.FramethreefoursevenonDrag } onMouseLeave={ props.FramethreefoursevenonMouseLeave } onMouseUp={ props.FramethreefoursevenonMouseUp } onMouseDown={ props.FramethreefoursevenonMouseDown } onKeyDown={ props.FramethreefoursevenonKeyDown } onChange={ props.FramethreefoursevenonChange } ondelay={ props.Framethreefoursevenondelay }>
            <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['rectangleone']?.animationClass || {}}>
              <img id="id_fourthreefour_twootwoofoureight" className={` rectangle buttonloginrectangleone ${ props.onClick ? 'cursor' : '' } ${ transaction['rectangleone']?.type ? transaction['rectangleone']?.type.toLowerCase() : '' }`} style={{ ...{},  ...props.RectangleoneStyle , transitionDuration: transaction['rectangleone']?.duration, transitionTimingFunction: transaction['rectangleone']?.timingFunction }} onClick={ props.RectangleoneonClick } onMouseEnter={ props.RectangleoneonMouseEnter } onMouseOver={ props.RectangleoneonMouseOver } onKeyPress={ props.RectangleoneonKeyPress } onDrag={ props.RectangleoneonDrag } onMouseLeave={ props.RectangleoneonMouseLeave } onMouseUp={ props.RectangleoneonMouseUp } onMouseDown={ props.RectangleoneonMouseDown } onKeyDown={ props.RectangleoneonKeyDown } onChange={ props.RectangleoneonChange } ondelay={ props.Rectangleoneondelay } src={props.Rectangleone0 || "https://cdn.uncodie.com/tACQBrKCV97e0EqPE1XNLJ/70f4d66ab8ab6519bb5d5e66cce5d979f7a680ec.png" } />
            </CSSTransition>
            <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['principalbuttontext']?.animationClass || {}}>

              <span id="id_fourthreefour_twootwootwoosix"  className={` text buttonloginprincipalbuttontext    ${ props.onClick ? 'cursor' : ''}  ${ transaction['principalbuttontext']?.type ? transaction['principalbuttontext']?.type.toLowerCase() : '' }`} style={{ ...{"height":"14px"},  ...props.PrincipalButtonTextStyle , transitionDuration: transaction['principalbuttontext']?.duration, transitionTimingFunction: transaction['principalbuttontext']?.timingFunction }} onClick={ props.PrincipalButtonTextonClick } onMouseEnter={ props.PrincipalButtonTextonMouseEnter } onMouseOver={ props.PrincipalButtonTextonMouseOver } onKeyPress={ props.PrincipalButtonTextonKeyPress } onDrag={ props.PrincipalButtonTextonDrag } onMouseLeave={ props.PrincipalButtonTextonMouseLeave } onMouseUp={ props.PrincipalButtonTextonMouseUp } onMouseDown={ props.PrincipalButtonTextonMouseDown } onKeyDown={ props.PrincipalButtonTextonKeyDown } onChange={ props.PrincipalButtonTextonChange } ondelay={ props.PrincipalButtonTextondelay } >{props.PrincipalButtonText0 || `Continuar Con Número De Teléfono `}</span>

            </CSSTransition>
          </div>

        </CSSTransition>

      </>
      }
    </div>

  </CSSTransition>

</>
            )
        }
case 'Property 1=Variant2':{
            return (
            <>

  <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['propertyonevarianttwoo']?.animationClass || {}}>

    <div id="id_fourthreesix_oneoneeighteight" ref={nodeRefpropertyonevarianttwoo} className={` cursor ${ props.onClick ? 'cursor' : '' } buttonloginpropertyonevarianttwoo C_fourthreesix_oneoneeighteight ${ props.cssClass } ${ transaction['propertyonevarianttwoo']?.type ? transaction['propertyonevarianttwoo']?.type.toLowerCase() : '' }`} style={ { ...{ ...{"minWidth":"250px"}, transitionDuration: transaction['propertyonevarianttwoo']?.duration, transitionTimingFunction: transaction['propertyonevarianttwoo']?.timingFunction }, ...props.style }} onClick={ props.ButtonLoginonClick } onMouseEnter={ props.ButtonLoginonMouseEnter } onMouseOver={ props.ButtonLoginonMouseOver } onKeyPress={ props.ButtonLoginonKeyPress } onDrag={ props.ButtonLoginonDrag } onMouseLeave={ props.ButtonLoginonMouseLeave || function(e){ setTransaction({ }); setvariant('Property 1=Default'); }} onMouseUp={ props.ButtonLoginonMouseUp } onMouseDown={ props.ButtonLoginonMouseDown } onKeyDown={ props.ButtonLoginonKeyDown } onChange={ props.ButtonLoginonChange } ondelay={ props.ButtonLoginondelay }>
      {
      props.children ?
      props.children :
      <>
        <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['framethreefourseven']?.animationClass || {}}>

          <div id="id_fourthreesix_oneoneeightnigth" className={` frame buttonloginframethreefourseven ${ props.onClick ? 'cursor' : '' } ${ transaction['framethreefourseven']?.type ? transaction['framethreefourseven']?.type.toLowerCase() : '' }`} style={ { ...{}, ...props.FramethreefoursevenStyle , transitionDuration: transaction['framethreefourseven']?.duration, transitionTimingFunction: transaction['framethreefourseven']?.timingFunction } } onClick={ props.FramethreefoursevenonClick } onMouseEnter={ props.FramethreefoursevenonMouseEnter } onMouseOver={ props.FramethreefoursevenonMouseOver } onKeyPress={ props.FramethreefoursevenonKeyPress } onDrag={ props.FramethreefoursevenonDrag } onMouseLeave={ props.FramethreefoursevenonMouseLeave } onMouseUp={ props.FramethreefoursevenonMouseUp } onMouseDown={ props.FramethreefoursevenonMouseDown } onKeyDown={ props.FramethreefoursevenonKeyDown } onChange={ props.FramethreefoursevenonChange } ondelay={ props.Framethreefoursevenondelay }>
            <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['rectangleone']?.animationClass || {}}>
              <img id="id_fourthreesix_oneonenigthzero" className={` rectangle buttonloginrectangleone ${ props.onClick ? 'cursor' : '' } ${ transaction['rectangleone']?.type ? transaction['rectangleone']?.type.toLowerCase() : '' }`} style={{ ...{},  ...props.RectangleoneStyle , transitionDuration: transaction['rectangleone']?.duration, transitionTimingFunction: transaction['rectangleone']?.timingFunction }} onClick={ props.RectangleoneonClick } onMouseEnter={ props.RectangleoneonMouseEnter } onMouseOver={ props.RectangleoneonMouseOver } onKeyPress={ props.RectangleoneonKeyPress } onDrag={ props.RectangleoneonDrag } onMouseLeave={ props.RectangleoneonMouseLeave } onMouseUp={ props.RectangleoneonMouseUp } onMouseDown={ props.RectangleoneonMouseDown } onKeyDown={ props.RectangleoneonKeyDown } onChange={ props.RectangleoneonChange } ondelay={ props.Rectangleoneondelay } src={props.Rectangleone0 || "https://cdn.uncodie.com/tACQBrKCV97e0EqPE1XNLJ/70f4d66ab8ab6519bb5d5e66cce5d979f7a680ec.png" } />
            </CSSTransition>
            <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['principalbuttontext']?.animationClass || {}}>

              <span id="id_fourthreesix_oneonenigthone"  className={` text buttonloginprincipalbuttontext    ${ props.onClick ? 'cursor' : ''}  ${ transaction['principalbuttontext']?.type ? transaction['principalbuttontext']?.type.toLowerCase() : '' }`} style={{ ...{"height":"14px"},  ...props.PrincipalButtonTextStyle , transitionDuration: transaction['principalbuttontext']?.duration, transitionTimingFunction: transaction['principalbuttontext']?.timingFunction }} onClick={ props.PrincipalButtonTextonClick } onMouseEnter={ props.PrincipalButtonTextonMouseEnter } onMouseOver={ props.PrincipalButtonTextonMouseOver } onKeyPress={ props.PrincipalButtonTextonKeyPress } onDrag={ props.PrincipalButtonTextonDrag } onMouseLeave={ props.PrincipalButtonTextonMouseLeave } onMouseUp={ props.PrincipalButtonTextonMouseUp } onMouseDown={ props.PrincipalButtonTextonMouseDown } onKeyDown={ props.PrincipalButtonTextonKeyDown } onChange={ props.PrincipalButtonTextonChange } ondelay={ props.PrincipalButtonTextondelay } >{props.PrincipalButtonText0 || `Continuar Con Número De Teléfono `}</span>

            </CSSTransition>
          </div>

        </CSSTransition>

      </>
      }
    </div>

  </CSSTransition>

</>
            )
        }
        }
    }

    return (
        <>
            {switchVariant(variant)}
        </>
    )
    
}

ButtonLogin.propTypes = {
    style: PropTypes.any,
variant: PropTypes.any,
Rectangleone0: PropTypes.any,
PrincipalButtonText0: PropTypes.any,
ButtonLoginonClick: PropTypes.any,
ButtonLoginonMouseEnter: PropTypes.any,
ButtonLoginonMouseOver: PropTypes.any,
ButtonLoginonKeyPress: PropTypes.any,
ButtonLoginonDrag: PropTypes.any,
ButtonLoginonMouseLeave: PropTypes.any,
ButtonLoginonMouseUp: PropTypes.any,
ButtonLoginonMouseDown: PropTypes.any,
ButtonLoginonKeyDown: PropTypes.any,
ButtonLoginonChange: PropTypes.any,
ButtonLoginondelay: PropTypes.any,
FramethreefoursevenonClick: PropTypes.any,
FramethreefoursevenonMouseEnter: PropTypes.any,
FramethreefoursevenonMouseOver: PropTypes.any,
FramethreefoursevenonKeyPress: PropTypes.any,
FramethreefoursevenonDrag: PropTypes.any,
FramethreefoursevenonMouseLeave: PropTypes.any,
FramethreefoursevenonMouseUp: PropTypes.any,
FramethreefoursevenonMouseDown: PropTypes.any,
FramethreefoursevenonKeyDown: PropTypes.any,
FramethreefoursevenonChange: PropTypes.any,
Framethreefoursevenondelay: PropTypes.any,
RectangleoneonClick: PropTypes.any,
RectangleoneonMouseEnter: PropTypes.any,
RectangleoneonMouseOver: PropTypes.any,
RectangleoneonKeyPress: PropTypes.any,
RectangleoneonDrag: PropTypes.any,
RectangleoneonMouseLeave: PropTypes.any,
RectangleoneonMouseUp: PropTypes.any,
RectangleoneonMouseDown: PropTypes.any,
RectangleoneonKeyDown: PropTypes.any,
RectangleoneonChange: PropTypes.any,
Rectangleoneondelay: PropTypes.any,
PrincipalButtonTextonClick: PropTypes.any,
PrincipalButtonTextonMouseEnter: PropTypes.any,
PrincipalButtonTextonMouseOver: PropTypes.any,
PrincipalButtonTextonKeyPress: PropTypes.any,
PrincipalButtonTextonDrag: PropTypes.any,
PrincipalButtonTextonMouseLeave: PropTypes.any,
PrincipalButtonTextonMouseUp: PropTypes.any,
PrincipalButtonTextonMouseDown: PropTypes.any,
PrincipalButtonTextonKeyDown: PropTypes.any,
PrincipalButtonTextonChange: PropTypes.any,
PrincipalButtonTextondelay: PropTypes.any
}
export default ButtonLogin;