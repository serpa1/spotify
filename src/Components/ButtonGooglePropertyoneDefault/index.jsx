import React, { Suspense } from 'react';
import PropTypes from 'prop-types';
import * as Contexts  from 'Contexts';
import { OverlaySwapContext } from 'Components/OverlaySwapProvider';
import CorebooksAnimation from 'Components/CorebooksAnimation';
import CorebooksAnimationSingle from 'Components/CorebooksAnimationSingle';

import * as UtilsScripts  from 'utils/utils';
import { Route, useHistory, useParams } from "react-router-dom";
import SingletoneNavigation from 'Class/SingletoneNavigation';

import LoadingComponent from "Components/Loading";


import { Transition } from "react-transition-group";
import { CSSTransition } from "react-transition-group";
import { useAppContext, useSessionContext } from 'context/AppContext';
import './ButtonGooglePropertyoneDefault.css'





const PropertyoneDefault = (props)=>{
    const [state,dispatch] = useAppContext();
    const [, setNameSelectedComponent] = React.useContext(OverlaySwapContext);
    
    const nodeRef = React.useRef(null);
    const history = useHistory();
    let RouterContext = useParams();
    
    
    
    
    
    
    const [globalStateForm, dispatchForm] = React.useState({});
    
    const [ _in, setIn ] = React.useState(false)
    const [ transaction, setTransaction ] = React.useState({}) 
    const [ animation, setAnimation ] = React.useState('')

    
    
    
    
    
    
    React.useEffect(()=>{
        
        
        
    },[]);
    
    
    return (
        <>
  <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['propertyonedefault']?.animationClass || {}}>

    <div id="id_fourthreefour_onefourfournigth" ref={nodeRef} className={` cursor ${ props.onClick ? 'cursor' : '' } propertyonedefault C_fourthreefour_onefourfournigth ${ props.cssClass } ${ transaction['propertyonedefault']?.type ? transaction['propertyonedefault']?.type.toLowerCase() : '' }`} style={ { ...{ ...{"minWidth":"250px"}, transitionDuration: transaction['propertyonedefault']?.duration, transitionTimingFunction: transaction['propertyonedefault']?.timingFunction }, ...props.style }} onClick={ props.ButtonGoogleonClick } onMouseEnter={ props.ButtonGoogleonMouseEnter } onMouseOver={ props.ButtonGoogleonMouseOver || function(e){ setTransaction({ }); setvariant('Property 1=Variant2'); }} onKeyPress={ props.ButtonGoogleonKeyPress } onDrag={ props.ButtonGoogleonDrag } onMouseLeave={ props.ButtonGoogleonMouseLeave } onMouseUp={ props.ButtonGoogleonMouseUp } onMouseDown={ props.ButtonGoogleonMouseDown } onKeyDown={ props.ButtonGoogleonKeyDown } onChange={ props.ButtonGoogleonChange } ondelay={ props.ButtonGoogleondelay }>
      {
      props.children ?
      props.children :
      <>
        <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['framethreefourseven']?.animationClass || {}}>

          <div id="id_fourthreefour_onefourfoureight" className={` frame framethreefourseven ${ props.onClick ? 'cursor' : '' } ${ transaction['framethreefourseven']?.type ? transaction['framethreefourseven']?.type.toLowerCase() : '' }`} style={ { ...{}, ...props.FramethreefoursevenStyle , transitionDuration: transaction['framethreefourseven']?.duration, transitionTimingFunction: transaction['framethreefourseven']?.timingFunction } } onClick={ props.FramethreefoursevenonClick } onMouseEnter={ props.FramethreefoursevenonMouseEnter } onMouseOver={ props.FramethreefoursevenonMouseOver } onKeyPress={ props.FramethreefoursevenonKeyPress } onDrag={ props.FramethreefoursevenonDrag } onMouseLeave={ props.FramethreefoursevenonMouseLeave } onMouseUp={ props.FramethreefoursevenonMouseUp } onMouseDown={ props.FramethreefoursevenonMouseDown } onKeyDown={ props.FramethreefoursevenonKeyDown } onChange={ props.FramethreefoursevenonChange } ondelay={ props.Framethreefoursevenondelay }>
            <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['frame']?.animationClass || {}}>

              <div id="id_fourthreefour_onefourthreesix" className={` frame frame ${ props.onClick ? 'cursor' : '' } ${ transaction['frame']?.type ? transaction['frame']?.type.toLowerCase() : '' }`} style={ { ...{}, ...props.FrameStyle , transitionDuration: transaction['frame']?.duration, transitionTimingFunction: transaction['frame']?.timingFunction } } onClick={ props.FrameonClick } onMouseEnter={ props.FrameonMouseEnter } onMouseOver={ props.FrameonMouseOver } onKeyPress={ props.FrameonKeyPress } onDrag={ props.FrameonDrag } onMouseLeave={ props.FrameonMouseLeave } onMouseUp={ props.FrameonMouseUp } onMouseDown={ props.FrameonMouseDown } onKeyDown={ props.FrameonKeyDown } onChange={ props.FrameonChange } ondelay={ props.Frameondelay }>
                <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['vector']?.animationClass || {}}>
                  <svg id="id_fourthreefour_onefourthreeseven" className="vector  " style={{}} onClick={ props.VectoronClick } onMouseEnter={ props.VectoronMouseEnter } onMouseOver={ props.VectoronMouseOver } onKeyPress={ props.VectoronKeyPress } onDrag={ props.VectoronDrag } onMouseLeave={ props.VectoronMouseLeave } onMouseUp={ props.VectoronMouseUp } onMouseDown={ props.VectoronMouseDown } onKeyDown={ props.VectoronKeyDown } onChange={ props.VectoronChange } ondelay={ props.Vectorondelay } width="9.60009765625" height="9.3953857421875">
                    <path d="M9.6 2.0454C9.6 1.3363 9.5364 0.6545 9.4182 0L0 0L0 3.8681L5.3818 3.8681C5.15 5.1181 4.4455 6.1772 3.3864 6.8863L3.3864 9.3954L6.6182 9.3954C8.5091 7.6545 9.6 5.0909 9.6 2.0454Z" />
                  </svg>
                </CSSTransition>
                <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['vector']?.animationClass || {}}>
                  <svg id="id_fourthreefour_onefourthreeeight" className="vector  " style={{}} onClick={ props.VectoronClick } onMouseEnter={ props.VectoronMouseEnter } onMouseOver={ props.VectoronMouseOver } onKeyPress={ props.VectoronKeyPress } onDrag={ props.VectoronDrag } onMouseLeave={ props.VectoronMouseLeave } onMouseUp={ props.VectoronMouseUp } onMouseDown={ props.VectoronMouseDown } onKeyDown={ props.VectoronKeyDown } onChange={ props.VectoronChange } ondelay={ props.Vectorondelay } width="15.5546875" height="8.100006103515625">
                    <path d="M8.9364 8.1C11.6364 8.1 13.9 7.2045 15.5545 5.6773L12.3227 3.1682C11.4273 3.7682 10.2818 4.1227 8.9364 4.1227C6.33182 4.1227 4.12727 2.3636 3.34091 0L0 0L0 2.5909C1.64545 5.8591 5.02727 8.1 8.9364 8.1Z" />
                  </svg>
                </CSSTransition>
                <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['vector']?.animationClass || {}}>
                  <svg id="id_fourthreefour_onefourthreenigth" className="vector  " style={{}} onClick={ props.VectoronClick } onMouseEnter={ props.VectoronMouseEnter } onMouseOver={ props.VectoronMouseOver } onKeyPress={ props.VectoronKeyPress } onDrag={ props.VectoronDrag } onMouseLeave={ props.VectoronMouseLeave } onMouseUp={ props.VectoronMouseUp } onMouseDown={ props.VectoronMouseDown } onKeyDown={ props.VectoronKeyDown } onChange={ props.VectoronChange } ondelay={ props.Vectorondelay } width="4.40478515625" height="8.981842041015625">
                    <path d="M4.40455 6.39094C4.20455 5.79094 4.09091 5.15004 4.09091 4.49094C4.09091 3.83184 4.20455 3.19094 4.40455 2.59094L4.40455 0L1.06364 0C0.38636 1.35 0 2.87724 0 4.49094C0 6.10454 0.38636 7.63184 1.06364 8.98184L4.40455 6.39094Z" />
                  </svg>
                </CSSTransition>
                <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['vector']?.animationClass || {}}>
                  <svg id="id_fourthreefour_onefourfourzero" className="vector  " style={{}} onClick={ props.VectoronClick } onMouseEnter={ props.VectoronMouseEnter } onMouseOver={ props.VectoronMouseOver } onKeyPress={ props.VectoronKeyPress } onDrag={ props.VectoronDrag } onMouseLeave={ props.VectoronMouseLeave } onMouseUp={ props.VectoronMouseUp } onMouseDown={ props.VectoronMouseDown } onKeyDown={ props.VectoronKeyDown } onChange={ props.VectoronChange } ondelay={ props.Vectorondelay } width="15.62744140625" height="8.100006103515625">
                    <path d="M8.9364 3.97727C10.4045 3.97727 11.7227 4.48182 12.7591 5.47273L15.6273 2.60455C13.8955 0.99091 11.6318 0 8.9364 0C5.02727 0 1.64545 2.24091 0 5.50909L3.34091 8.1C4.12727 5.73636 6.33182 3.97727 8.9364 3.97727Z" />
                  </svg>
                </CSSTransition>
              </div>

            </CSSTransition>
            <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['principalbuttontext']?.animationClass || {}}>

              <span id="id_fourthreefour_onefourfourseven"  className={` text principalbuttontext    ${ props.onClick ? 'cursor' : ''}  ${ transaction['principalbuttontext']?.type ? transaction['principalbuttontext']?.type.toLowerCase() : '' }`} style={{ ...{"height":"14px"},  ...props.PrincipalButtonTextStyle , transitionDuration: transaction['principalbuttontext']?.duration, transitionTimingFunction: transaction['principalbuttontext']?.timingFunction }} onClick={ props.PrincipalButtonTextonClick } onMouseEnter={ props.PrincipalButtonTextonMouseEnter } onMouseOver={ props.PrincipalButtonTextonMouseOver } onKeyPress={ props.PrincipalButtonTextonKeyPress } onDrag={ props.PrincipalButtonTextonDrag } onMouseLeave={ props.PrincipalButtonTextonMouseLeave } onMouseUp={ props.PrincipalButtonTextonMouseUp } onMouseDown={ props.PrincipalButtonTextonMouseDown } onKeyDown={ props.PrincipalButtonTextonKeyDown } onChange={ props.PrincipalButtonTextonChange } ondelay={ props.PrincipalButtonTextondelay } >{props.PrincipalButtonText0 || `Registrarte con Google`}</span>

            </CSSTransition>
          </div>

        </CSSTransition>

      </>
      }
    </div>

  </CSSTransition>
</>
    ) 
}

PropertyoneDefault.propTypes = {
    style: PropTypes.any,
PrincipalButtonText0: PropTypes.any,
ButtonGoogleonClick: PropTypes.any,
ButtonGoogleonMouseEnter: PropTypes.any,
ButtonGoogleonMouseOver: PropTypes.any,
ButtonGoogleonKeyPress: PropTypes.any,
ButtonGoogleonDrag: PropTypes.any,
ButtonGoogleonMouseLeave: PropTypes.any,
ButtonGoogleonMouseUp: PropTypes.any,
ButtonGoogleonMouseDown: PropTypes.any,
ButtonGoogleonKeyDown: PropTypes.any,
ButtonGoogleonChange: PropTypes.any,
ButtonGoogleondelay: PropTypes.any,
FramethreefoursevenonClick: PropTypes.any,
FramethreefoursevenonMouseEnter: PropTypes.any,
FramethreefoursevenonMouseOver: PropTypes.any,
FramethreefoursevenonKeyPress: PropTypes.any,
FramethreefoursevenonDrag: PropTypes.any,
FramethreefoursevenonMouseLeave: PropTypes.any,
FramethreefoursevenonMouseUp: PropTypes.any,
FramethreefoursevenonMouseDown: PropTypes.any,
FramethreefoursevenonKeyDown: PropTypes.any,
FramethreefoursevenonChange: PropTypes.any,
Framethreefoursevenondelay: PropTypes.any,
FrameonClick: PropTypes.any,
FrameonMouseEnter: PropTypes.any,
FrameonMouseOver: PropTypes.any,
FrameonKeyPress: PropTypes.any,
FrameonDrag: PropTypes.any,
FrameonMouseLeave: PropTypes.any,
FrameonMouseUp: PropTypes.any,
FrameonMouseDown: PropTypes.any,
FrameonKeyDown: PropTypes.any,
FrameonChange: PropTypes.any,
Frameondelay: PropTypes.any,
VectoronClick: PropTypes.any,
VectoronMouseEnter: PropTypes.any,
VectoronMouseOver: PropTypes.any,
VectoronKeyPress: PropTypes.any,
VectoronDrag: PropTypes.any,
VectoronMouseLeave: PropTypes.any,
VectoronMouseUp: PropTypes.any,
VectoronMouseDown: PropTypes.any,
VectoronKeyDown: PropTypes.any,
VectoronChange: PropTypes.any,
Vectorondelay: PropTypes.any,
PrincipalButtonTextonClick: PropTypes.any,
PrincipalButtonTextonMouseEnter: PropTypes.any,
PrincipalButtonTextonMouseOver: PropTypes.any,
PrincipalButtonTextonKeyPress: PropTypes.any,
PrincipalButtonTextonDrag: PropTypes.any,
PrincipalButtonTextonMouseLeave: PropTypes.any,
PrincipalButtonTextonMouseUp: PropTypes.any,
PrincipalButtonTextonMouseDown: PropTypes.any,
PrincipalButtonTextonKeyDown: PropTypes.any,
PrincipalButtonTextonChange: PropTypes.any,
PrincipalButtonTextondelay: PropTypes.any
}
export default PropertyoneDefault;