import React, { Suspense } from 'react';
import PropTypes from 'prop-types';
import * as Contexts  from 'Contexts';
import { OverlaySwapContext } from 'Components/OverlaySwapProvider';
import CorebooksAnimation from 'Components/CorebooksAnimation';
import CorebooksAnimationSingle from 'Components/CorebooksAnimationSingle';

import * as UtilsScripts  from 'utils/utils';
import { Route, useHistory, useParams } from "react-router-dom";
import SingletoneNavigation from 'Class/SingletoneNavigation';

import LoadingComponent from "Components/Loading";


import { Transition } from "react-transition-group";
import { CSSTransition } from "react-transition-group";
import { useAppContext, useSessionContext } from 'context/AppContext';
import './Imageone.css'





const Imageone = (props)=>{
    const [state,dispatch] = useAppContext();
    const [, setNameSelectedComponent] = React.useContext(OverlaySwapContext);
    
    const nodeRef = React.useRef(null);
    const history = useHistory();
    let RouterContext = useParams();
    
    
    
    
    
    
    const [globalStateForm, dispatchForm] = React.useState({});
    
    const [ _in, setIn ] = React.useState(false)
    const [ transaction, setTransaction ] = React.useState({}) 
    const [ animation, setAnimation ] = React.useState('')

    
    
    
    
    
    
    React.useEffect(()=>{
        
        
        
    },[]);
    
    
    return (
        <>
  <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['imageone']?.animationClass || {}}>

    <div id="id_fourthreeeight_onethreethreesix" ref={nodeRef} className={` ${ props.onClick ? 'cursor' : '' } imageone ${ props.cssClass } ${ transaction['imageone']?.type ? transaction['imageone']?.type.toLowerCase() : '' }`} style={ { ...{ ...{}, transitionDuration: transaction['imageone']?.duration, transitionTimingFunction: transaction['imageone']?.timingFunction }, ...props.style }} onClick={ props.ImageoneonClick } onMouseEnter={ props.ImageoneonMouseEnter } onMouseOver={ props.ImageoneonMouseOver } onKeyPress={ props.ImageoneonKeyPress } onDrag={ props.ImageoneonDrag } onMouseLeave={ props.ImageoneonMouseLeave } onMouseUp={ props.ImageoneonMouseUp } onMouseDown={ props.ImageoneonMouseDown } onKeyDown={ props.ImageoneonKeyDown } onChange={ props.ImageoneonChange } ondelay={ props.Imageoneondelay }>
      {
      props.children ?
      props.children :
      <>


      </>
      }
    </div>

  </CSSTransition>
</>
    ) 
}

Imageone.propTypes = {
    style: PropTypes.any,
ImageoneonClick: PropTypes.any,
ImageoneonMouseEnter: PropTypes.any,
ImageoneonMouseOver: PropTypes.any,
ImageoneonKeyPress: PropTypes.any,
ImageoneonDrag: PropTypes.any,
ImageoneonMouseLeave: PropTypes.any,
ImageoneonMouseUp: PropTypes.any,
ImageoneonMouseDown: PropTypes.any,
ImageoneonKeyDown: PropTypes.any,
ImageoneonChange: PropTypes.any,
Imageoneondelay: PropTypes.any
}
export default Imageone;