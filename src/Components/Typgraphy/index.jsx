import React, { Suspense } from 'react';
import PropTypes from 'prop-types';
import * as Contexts  from 'Contexts';
import { OverlaySwapContext } from 'Components/OverlaySwapProvider';
import CorebooksAnimation from 'Components/CorebooksAnimation';
import CorebooksAnimationSingle from 'Components/CorebooksAnimationSingle';

import * as UtilsScripts  from 'utils/utils';
import { Route, useHistory, useParams } from "react-router-dom";
import SingletoneNavigation from 'Class/SingletoneNavigation';

import LoadingComponent from "Components/Loading";


import { Transition } from "react-transition-group";
import { CSSTransition } from "react-transition-group";
import { useAppContext, useSessionContext } from 'context/AppContext';
import './Typgraphy.css'





const Typgraphy = (props)=>{
    const [state,dispatch] = useAppContext();
    const [, setNameSelectedComponent] = React.useContext(OverlaySwapContext);
    
    const nodeRef = React.useRef(null);
    const history = useHistory();
    let RouterContext = useParams();
    
    
    
    
    
    
    const [globalStateForm, dispatchForm] = React.useState({});
    
    const [ _in, setIn ] = React.useState(false)
    const [ transaction, setTransaction ] = React.useState({}) 
    const [ animation, setAnimation ] = React.useState('')

    
    
    
    
    
    
    React.useEffect(()=>{
        
        
        
    },[]);
    
    
    return (
        <>
  <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['typgraphy']?.animationClass || {}}>

    <div id="id_sixzerothree_onefivethreefour" ref={nodeRef} className={` ${ props.onClick ? 'cursor' : '' } typgraphy ${ props.cssClass } ${ transaction['typgraphy']?.type ? transaction['typgraphy']?.type.toLowerCase() : '' }`} style={ { ...{ ...{}, transitionDuration: transaction['typgraphy']?.duration, transitionTimingFunction: transaction['typgraphy']?.timingFunction }, ...props.style }} onClick={ props.TypgraphyonClick } onMouseEnter={ props.TypgraphyonMouseEnter } onMouseOver={ props.TypgraphyonMouseOver } onKeyPress={ props.TypgraphyonKeyPress } onDrag={ props.TypgraphyonDrag } onMouseLeave={ props.TypgraphyonMouseLeave } onMouseUp={ props.TypgraphyonMouseUp } onMouseDown={ props.TypgraphyonMouseDown } onKeyDown={ props.TypgraphyonKeyDown } onChange={ props.TypgraphyonChange } ondelay={ props.Typgraphyondelay }>
      {
      props.children ?
      props.children :
      <>
        <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['title']?.animationClass || {}}>

          <div id="id_sixzerothree_onefivethreefive" className={` group title ${ props.onClick ? 'cursor' : '' } ${ transaction['title']?.type ? transaction['title']?.type.toLowerCase() : '' }`} style={{ ...{},  ...props.TitleStyle , transitionDuration: transaction['title']?.duration, transitionTimingFunction: transaction['title']?.timingFunction }} onClick={ props.TitleonClick } onMouseEnter={ props.TitleonMouseEnter } onMouseOver={ props.TitleonMouseOver } onKeyPress={ props.TitleonKeyPress } onDrag={ props.TitleonDrag } onMouseLeave={ props.TitleonMouseLeave } onMouseUp={ props.TitleonMouseUp } onMouseDown={ props.TitleonMouseDown } onKeyDown={ props.TitleonKeyDown } onChange={ props.TitleonChange } ondelay={ props.Titleondelay }>
            <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['typography']?.animationClass || {}}>

              <span id="id_sixzerothree_onefivethreesix"  className={` text typography    ${ props.onClick ? 'cursor' : ''}  ${ transaction['typography']?.type ? transaction['typography']?.type.toLowerCase() : '' }`} style={{ ...{},  ...props.TypographyStyle , transitionDuration: transaction['typography']?.duration, transitionTimingFunction: transaction['typography']?.timingFunction }} onClick={ props.TypographyonClick } onMouseEnter={ props.TypographyonMouseEnter } onMouseOver={ props.TypographyonMouseOver } onKeyPress={ props.TypographyonKeyPress } onDrag={ props.TypographyonDrag } onMouseLeave={ props.TypographyonMouseLeave } onMouseUp={ props.TypographyonMouseUp } onMouseDown={ props.TypographyonMouseDown } onKeyDown={ props.TypographyonKeyDown } onChange={ props.TypographyonChange } ondelay={ props.Typographyondelay } >{props.Typography0 || `Typography`}</span>

            </CSSTransition>
            <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['rectangle']?.animationClass || {}}>
              <div id="id_sixzerothree_onefivethreeeight" className={` rectangle rectangle ${ props.onClick ? 'cursor' : '' } ${ transaction['rectangle']?.type ? transaction['rectangle']?.type.toLowerCase() : '' }`} style={{ ...{},  ...props.RectangleStyle , transitionDuration: transaction['rectangle']?.duration, transitionTimingFunction: transaction['rectangle']?.timingFunction }} onClick={ props.RectangleonClick } onMouseEnter={ props.RectangleonMouseEnter } onMouseOver={ props.RectangleonMouseOver } onKeyPress={ props.RectangleonKeyPress } onDrag={ props.RectangleonDrag } onMouseLeave={ props.RectangleonMouseLeave } onMouseUp={ props.RectangleonMouseUp } onMouseDown={ props.RectangleonMouseDown } onKeyDown={ props.RectangleonKeyDown } onChange={ props.RectangleonChange } ondelay={ props.Rectangleondelay }></div>
            </CSSTransition>
          </div>

        </CSSTransition>
        <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['framefoursixzerotwoo']?.animationClass || {}}>

          <div id="id_sixtwoozero_onesixsixzero" className={` frame framefoursixzerotwoo ${ props.onClick ? 'cursor' : '' } ${ transaction['framefoursixzerotwoo']?.type ? transaction['framefoursixzerotwoo']?.type.toLowerCase() : '' }`} style={ { ...{}, ...props.FramefoursixzerotwooStyle , transitionDuration: transaction['framefoursixzerotwoo']?.duration, transitionTimingFunction: transaction['framefoursixzerotwoo']?.timingFunction } } onClick={ props.FramefoursixzerotwooonClick } onMouseEnter={ props.FramefoursixzerotwooonMouseEnter } onMouseOver={ props.FramefoursixzerotwooonMouseOver } onKeyPress={ props.FramefoursixzerotwooonKeyPress } onDrag={ props.FramefoursixzerotwooonDrag } onMouseLeave={ props.FramefoursixzerotwooonMouseLeave } onMouseUp={ props.FramefoursixzerotwooonMouseUp } onMouseDown={ props.FramefoursixzerotwooonMouseDown } onKeyDown={ props.FramefoursixzerotwooonKeyDown } onChange={ props.FramefoursixzerotwooonChange } ondelay={ props.Framefoursixzerotwooondelay }>
            <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['groupfoursixzerozero']?.animationClass || {}}>

              <div id="id_sixtwoozero_onesixsixone" className={` group groupfoursixzerozero ${ props.onClick ? 'cursor' : '' } ${ transaction['groupfoursixzerozero']?.type ? transaction['groupfoursixzerozero']?.type.toLowerCase() : '' }`} style={{ ...{},  ...props.GroupfoursixzerozeroStyle , transitionDuration: transaction['groupfoursixzerozero']?.duration, transitionTimingFunction: transaction['groupfoursixzerozero']?.timingFunction }} onClick={ props.GroupfoursixzerozeroonClick } onMouseEnter={ props.GroupfoursixzerozeroonMouseEnter } onMouseOver={ props.GroupfoursixzerozeroonMouseOver } onKeyPress={ props.GroupfoursixzerozeroonKeyPress } onDrag={ props.GroupfoursixzerozeroonDrag } onMouseLeave={ props.GroupfoursixzerozeroonMouseLeave } onMouseUp={ props.GroupfoursixzerozeroonMouseUp } onMouseDown={ props.GroupfoursixzerozeroonMouseDown } onKeyDown={ props.GroupfoursixzerozeroonKeyDown } onChange={ props.GroupfoursixzerozeroonChange } ondelay={ props.Groupfoursixzerozeroondelay }>
                <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['framefoursixzerothree']?.animationClass || {}}>

                  <div id="id_sixtwoozero_onesixsixtwoo" className={` frame framefoursixzerothree ${ props.onClick ? 'cursor' : '' } ${ transaction['framefoursixzerothree']?.type ? transaction['framefoursixzerothree']?.type.toLowerCase() : '' }`} style={ { ...{}, ...props.FramefoursixzerothreeStyle , transitionDuration: transaction['framefoursixzerothree']?.duration, transitionTimingFunction: transaction['framefoursixzerothree']?.timingFunction } } onClick={ props.FramefoursixzerothreeonClick } onMouseEnter={ props.FramefoursixzerothreeonMouseEnter } onMouseOver={ props.FramefoursixzerothreeonMouseOver } onKeyPress={ props.FramefoursixzerothreeonKeyPress } onDrag={ props.FramefoursixzerothreeonDrag } onMouseLeave={ props.FramefoursixzerothreeonMouseLeave } onMouseUp={ props.FramefoursixzerothreeonMouseUp } onMouseDown={ props.FramefoursixzerothreeonMouseDown } onKeyDown={ props.FramefoursixzerothreeonKeyDown } onChange={ props.FramefoursixzerothreeonChange } ondelay={ props.Framefoursixzerothreeondelay }>
                    <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['typographydetails']?.animationClass || {}}>

                      <div id="id_sixtwoozero_onesixsixthree" className={` group typographydetails ${ props.onClick ? 'cursor' : '' } ${ transaction['typographydetails']?.type ? transaction['typographydetails']?.type.toLowerCase() : '' }`} style={{ ...{},  ...props.TypographyDetailsStyle , transitionDuration: transaction['typographydetails']?.duration, transitionTimingFunction: transaction['typographydetails']?.timingFunction }} onClick={ props.TypographyDetailsonClick } onMouseEnter={ props.TypographyDetailsonMouseEnter } onMouseOver={ props.TypographyDetailsonMouseOver } onKeyPress={ props.TypographyDetailsonKeyPress } onDrag={ props.TypographyDetailsonDrag } onMouseLeave={ props.TypographyDetailsonMouseLeave } onMouseUp={ props.TypographyDetailsonMouseUp } onMouseDown={ props.TypographyDetailsonMouseDown } onKeyDown={ props.TypographyDetailsonKeyDown } onChange={ props.TypographyDetailsonChange } ondelay={ props.TypographyDetailsondelay }>
                        <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['framefoursixzerotwoo']?.animationClass || {}}>

                          <div id="id_sixtwoozero_onesixsixfour" className={` frame framefoursixzerotwoo ${ props.onClick ? 'cursor' : '' } ${ transaction['framefoursixzerotwoo']?.type ? transaction['framefoursixzerotwoo']?.type.toLowerCase() : '' }`} style={ { ...{}, ...props.FramefoursixzerotwooStyle , transitionDuration: transaction['framefoursixzerotwoo']?.duration, transitionTimingFunction: transaction['framefoursixzerotwoo']?.timingFunction } } onClick={ props.FramefoursixzerotwooonClick } onMouseEnter={ props.FramefoursixzerotwooonMouseEnter } onMouseOver={ props.FramefoursixzerotwooonMouseOver } onKeyPress={ props.FramefoursixzerotwooonKeyPress } onDrag={ props.FramefoursixzerotwooonDrag } onMouseLeave={ props.FramefoursixzerotwooonMouseLeave } onMouseUp={ props.FramefoursixzerotwooonMouseUp } onMouseDown={ props.FramefoursixzerotwooonMouseDown } onKeyDown={ props.FramefoursixzerotwooonKeyDown } onChange={ props.FramefoursixzerotwooonChange } ondelay={ props.Framefoursixzerotwooondelay }>
                            <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['framefoursixzeroone']?.animationClass || {}}>

                              <div id="id_sixtwoozero_onesixsixfive" className={` frame framefoursixzeroone ${ props.onClick ? 'cursor' : '' } ${ transaction['framefoursixzeroone']?.type ? transaction['framefoursixzeroone']?.type.toLowerCase() : '' }`} style={ { ...{}, ...props.FramefoursixzerooneStyle , transitionDuration: transaction['framefoursixzeroone']?.duration, transitionTimingFunction: transaction['framefoursixzeroone']?.timingFunction } } onClick={ props.FramefoursixzerooneonClick } onMouseEnter={ props.FramefoursixzerooneonMouseEnter } onMouseOver={ props.FramefoursixzerooneonMouseOver } onKeyPress={ props.FramefoursixzerooneonKeyPress } onDrag={ props.FramefoursixzerooneonDrag } onMouseLeave={ props.FramefoursixzerooneonMouseLeave } onMouseUp={ props.FramefoursixzerooneonMouseUp } onMouseDown={ props.FramefoursixzerooneonMouseDown } onKeyDown={ props.FramefoursixzerooneonKeyDown } onChange={ props.FramefoursixzerooneonChange } ondelay={ props.Framefoursixzerooneondelay }>
                                <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['typographyname']?.animationClass || {}}>

                                  <span id="id_sixtwoozero_onesixsixsix"  className={` text typographyname    ${ props.onClick ? 'cursor' : ''}  ${ transaction['typographyname']?.type ? transaction['typographyname']?.type.toLowerCase() : '' }`} style={{ ...{},  ...props.TypographyNameStyle , transitionDuration: transaction['typographyname']?.duration, transitionTimingFunction: transaction['typographyname']?.timingFunction }} onClick={ props.TypographyNameonClick } onMouseEnter={ props.TypographyNameonMouseEnter } onMouseOver={ props.TypographyNameonMouseOver } onKeyPress={ props.TypographyNameonKeyPress } onDrag={ props.TypographyNameonDrag } onMouseLeave={ props.TypographyNameonMouseLeave } onMouseUp={ props.TypographyNameonMouseUp } onMouseDown={ props.TypographyNameonMouseDown } onKeyDown={ props.TypographyNameonKeyDown } onChange={ props.TypographyNameonChange } ondelay={ props.TypographyNameondelay } >{props.TypographyName0 || `Circular Std`}</span>

                                </CSSTransition>
                                <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['framefoursixzeroone']?.animationClass || {}}>

                                  <div id="id_sixtwoozero_onesixsixseven" className={` frame framefoursixzeroone ${ props.onClick ? 'cursor' : '' } ${ transaction['framefoursixzeroone']?.type ? transaction['framefoursixzeroone']?.type.toLowerCase() : '' }`} style={ { ...{}, ...props.FramefoursixzerooneStyle , transitionDuration: transaction['framefoursixzeroone']?.duration, transitionTimingFunction: transaction['framefoursixzeroone']?.timingFunction } } onClick={ props.FramefoursixzerooneonClick } onMouseEnter={ props.FramefoursixzerooneonMouseEnter } onMouseOver={ props.FramefoursixzerooneonMouseOver } onKeyPress={ props.FramefoursixzerooneonKeyPress } onDrag={ props.FramefoursixzerooneonDrag } onMouseLeave={ props.FramefoursixzerooneonMouseLeave } onMouseUp={ props.FramefoursixzerooneonMouseUp } onMouseDown={ props.FramefoursixzerooneonMouseDown } onKeyDown={ props.FramefoursixzerooneonKeyDown } onChange={ props.FramefoursixzerooneonChange } ondelay={ props.Framefoursixzerooneondelay }>
                                    <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['typographyweights']?.animationClass || {}}>

                                      <div id="id_sixtwoozero_onesixsixeight" className={` frame typographyweights ${ props.onClick ? 'cursor' : '' } ${ transaction['typographyweights']?.type ? transaction['typographyweights']?.type.toLowerCase() : '' }`} style={ { ...{}, ...props.TypographyWeightsStyle , transitionDuration: transaction['typographyweights']?.duration, transitionTimingFunction: transaction['typographyweights']?.timingFunction } } onClick={ props.TypographyWeightsonClick } onMouseEnter={ props.TypographyWeightsonMouseEnter } onMouseOver={ props.TypographyWeightsonMouseOver } onKeyPress={ props.TypographyWeightsonKeyPress } onDrag={ props.TypographyWeightsonDrag } onMouseLeave={ props.TypographyWeightsonMouseLeave } onMouseUp={ props.TypographyWeightsonMouseUp } onMouseDown={ props.TypographyWeightsonMouseDown } onKeyDown={ props.TypographyWeightsonKeyDown } onChange={ props.TypographyWeightsonChange } ondelay={ props.TypographyWeightsondelay }>
                                        <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['book']?.animationClass || {}}>

                                          <span id="id_sixtwoozero_onesixsixnigth"  className={` text book    ${ props.onClick ? 'cursor' : ''}  ${ transaction['book']?.type ? transaction['book']?.type.toLowerCase() : '' }`} style={{ ...{},  ...props.BookStyle , transitionDuration: transaction['book']?.duration, transitionTimingFunction: transaction['book']?.timingFunction }} onClick={ props.BookonClick } onMouseEnter={ props.BookonMouseEnter } onMouseOver={ props.BookonMouseOver } onKeyPress={ props.BookonKeyPress } onDrag={ props.BookonDrag } onMouseLeave={ props.BookonMouseLeave } onMouseUp={ props.BookonMouseUp } onMouseDown={ props.BookonMouseDown } onKeyDown={ props.BookonKeyDown } onChange={ props.BookonChange } ondelay={ props.Bookondelay } >{props.Book0 || `Book`}</span>

                                        </CSSTransition>
                                        <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['medium']?.animationClass || {}}>

                                          <span id="id_sixtwoozero_onesixsevennigth"  className={` text medium    ${ props.onClick ? 'cursor' : ''}  ${ transaction['medium']?.type ? transaction['medium']?.type.toLowerCase() : '' }`} style={{ ...{},  ...props.MediumStyle , transitionDuration: transaction['medium']?.duration, transitionTimingFunction: transaction['medium']?.timingFunction }} onClick={ props.MediumonClick } onMouseEnter={ props.MediumonMouseEnter } onMouseOver={ props.MediumonMouseOver } onKeyPress={ props.MediumonKeyPress } onDrag={ props.MediumonDrag } onMouseLeave={ props.MediumonMouseLeave } onMouseUp={ props.MediumonMouseUp } onMouseDown={ props.MediumonMouseDown } onKeyDown={ props.MediumonKeyDown } onChange={ props.MediumonChange } ondelay={ props.Mediumondelay } >{props.Medium0 || `Medium`}</span>

                                        </CSSTransition>
                                        <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['bold']?.animationClass || {}}>

                                          <span id="id_sixtwoozero_onesixeightzero"  className={` text bold    ${ props.onClick ? 'cursor' : ''}  ${ transaction['bold']?.type ? transaction['bold']?.type.toLowerCase() : '' }`} style={{ ...{},  ...props.BoldStyle , transitionDuration: transaction['bold']?.duration, transitionTimingFunction: transaction['bold']?.timingFunction }} onClick={ props.BoldonClick } onMouseEnter={ props.BoldonMouseEnter } onMouseOver={ props.BoldonMouseOver } onKeyPress={ props.BoldonKeyPress } onDrag={ props.BoldonDrag } onMouseLeave={ props.BoldonMouseLeave } onMouseUp={ props.BoldonMouseUp } onMouseDown={ props.BoldonMouseDown } onKeyDown={ props.BoldonKeyDown } onChange={ props.BoldonChange } ondelay={ props.Boldondelay } >{props.Bold0 || `Bold`}</span>

                                        </CSSTransition>
                                        <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['black']?.animationClass || {}}>

                                          <span id="id_sixtwoozero_onesixeightone"  className={` text black    ${ props.onClick ? 'cursor' : ''}  ${ transaction['black']?.type ? transaction['black']?.type.toLowerCase() : '' }`} style={{ ...{},  ...props.BlackStyle , transitionDuration: transaction['black']?.duration, transitionTimingFunction: transaction['black']?.timingFunction }} onClick={ props.BlackonClick } onMouseEnter={ props.BlackonMouseEnter } onMouseOver={ props.BlackonMouseOver } onKeyPress={ props.BlackonKeyPress } onDrag={ props.BlackonDrag } onMouseLeave={ props.BlackonMouseLeave } onMouseUp={ props.BlackonMouseUp } onMouseDown={ props.BlackonMouseDown } onKeyDown={ props.BlackonKeyDown } onChange={ props.BlackonChange } ondelay={ props.Blackondelay } >{props.Black0 || `Black`}</span>

                                        </CSSTransition>
                                      </div>

                                    </CSSTransition>
                                    <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['typographyexample']?.animationClass || {}}>

                                      <span id="id_sixtwoozero_onesixseventwoo"  className={` text typographyexample    ${ props.onClick ? 'cursor' : ''}  ${ transaction['typographyexample']?.type ? transaction['typographyexample']?.type.toLowerCase() : '' }`} style={{ ...{},  ...props.TypographyExampleStyle , transitionDuration: transaction['typographyexample']?.duration, transitionTimingFunction: transaction['typographyexample']?.timingFunction }} onClick={ props.TypographyExampleonClick } onMouseEnter={ props.TypographyExampleonMouseEnter } onMouseOver={ props.TypographyExampleonMouseOver } onKeyPress={ props.TypographyExampleonKeyPress } onDrag={ props.TypographyExampleonDrag } onMouseLeave={ props.TypographyExampleonMouseLeave } onMouseUp={ props.TypographyExampleonMouseUp } onMouseDown={ props.TypographyExampleonMouseDown } onKeyDown={ props.TypographyExampleonKeyDown } onChange={ props.TypographyExampleonChange } ondelay={ props.TypographyExampleondelay } >{props.TypographyExample0 || `“Lorem ipsum dolor sit amet consectetur adipiscing elit at erat consectetur ultricies sapien facilisi euismod duis mauris a sed quam aliquet dui eros sit lacus vitae ut viverra at praesent.”`}</span>

                                    </CSSTransition>
                                  </div>

                                </CSSTransition>
                              </div>

                            </CSSTransition>
                            <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['primarybutton']?.animationClass || {}}>

                              <div id="id_sixtwoozero_onesixseventhree" className={` frame primarybutton ${ props.onClick ? 'cursor' : '' } ${ transaction['primarybutton']?.type ? transaction['primarybutton']?.type.toLowerCase() : '' }`} style={ { ...{}, ...props.PrimaryButtonStyle , transitionDuration: transaction['primarybutton']?.duration, transitionTimingFunction: transaction['primarybutton']?.timingFunction } } onClick={ props.PrimaryButtononClick } onMouseEnter={ props.PrimaryButtononMouseEnter } onMouseOver={ props.PrimaryButtononMouseOver } onKeyPress={ props.PrimaryButtononKeyPress } onDrag={ props.PrimaryButtononDrag } onMouseLeave={ props.PrimaryButtononMouseLeave } onMouseUp={ props.PrimaryButtononMouseUp } onMouseDown={ props.PrimaryButtononMouseDown } onKeyDown={ props.PrimaryButtononKeyDown } onChange={ props.PrimaryButtononChange } ondelay={ props.PrimaryButtonondelay }>
                                <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['masterprimarybutton']?.animationClass || {}}>

                                  <div id="id_sixtwoozero_onesixsevenfour" className={` frame masterprimarybutton ${ props.onClick ? 'cursor' : '' } ${ transaction['masterprimarybutton']?.type ? transaction['masterprimarybutton']?.type.toLowerCase() : '' }`} style={ { ...{}, ...props.MasterPrimaryButtonStyle , transitionDuration: transaction['masterprimarybutton']?.duration, transitionTimingFunction: transaction['masterprimarybutton']?.timingFunction } } onClick={ props.MasterPrimaryButtononClick } onMouseEnter={ props.MasterPrimaryButtononMouseEnter } onMouseOver={ props.MasterPrimaryButtononMouseOver } onKeyPress={ props.MasterPrimaryButtononKeyPress } onDrag={ props.MasterPrimaryButtononDrag } onMouseLeave={ props.MasterPrimaryButtononMouseLeave } onMouseUp={ props.MasterPrimaryButtononMouseUp } onMouseDown={ props.MasterPrimaryButtononMouseDown } onKeyDown={ props.MasterPrimaryButtononKeyDown } onChange={ props.MasterPrimaryButtononChange } ondelay={ props.MasterPrimaryButtonondelay }>
                                    <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['lineroundedsearch']?.animationClass || {}}>

                                      <div id="id_sixtwoozero_onesixsevenfive" className={` instance lineroundedsearch ${ props.onClick ? 'cursor' : '' } ${ transaction['lineroundedsearch']?.type ? transaction['lineroundedsearch']?.type.toLowerCase() : '' }`} style={{ ...{},  ...props.LineRoundedSearchStyle , transitionDuration: transaction['lineroundedsearch']?.duration, transitionTimingFunction: transaction['lineroundedsearch']?.timingFunction }} onClick={ props.LineRoundedSearchonClick } onMouseEnter={ props.LineRoundedSearchonMouseEnter } onMouseOver={ props.LineRoundedSearchonMouseOver } onKeyPress={ props.LineRoundedSearchonKeyPress } onDrag={ props.LineRoundedSearchonDrag } onMouseLeave={ props.LineRoundedSearchonMouseLeave } onMouseUp={ props.LineRoundedSearchonMouseUp } onMouseDown={ props.LineRoundedSearchonMouseDown } onKeyDown={ props.LineRoundedSearchonKeyDown } onChange={ props.LineRoundedSearchonChange } ondelay={ props.LineRoundedSearchondelay }>
                                        <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['search']?.animationClass || {}}>

                                          <div id="id_sixtwoozero_onesixsevenfive_eightonetwoo_seventhreefourfive" className={` group search ${ props.onClick ? 'cursor' : '' } ${ transaction['search']?.type ? transaction['search']?.type.toLowerCase() : '' }`} style={{ ...{},  ...props.SearchStyle , transitionDuration: transaction['search']?.duration, transitionTimingFunction: transaction['search']?.timingFunction }} onClick={ props.SearchonClick } onMouseEnter={ props.SearchonMouseEnter } onMouseOver={ props.SearchonMouseOver } onKeyPress={ props.SearchonKeyPress } onDrag={ props.SearchonDrag } onMouseLeave={ props.SearchonMouseLeave } onMouseUp={ props.SearchonMouseUp } onMouseDown={ props.SearchonMouseDown } onKeyDown={ props.SearchonKeyDown } onChange={ props.SearchonChange } ondelay={ props.Searchondelay }>
                                            <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['vector']?.animationClass || {}}>
                                              <svg id="id_sixtwoozero_onesixsevenfive_eightonetwoo_seventhreefoursix" className="vector  " style={{}} onClick={ props.VectoronClick } onMouseEnter={ props.VectoronMouseEnter } onMouseOver={ props.VectoronMouseOver } onKeyPress={ props.VectoronKeyPress } onDrag={ props.VectoronDrag } onMouseLeave={ props.VectoronMouseLeave } onMouseUp={ props.VectoronMouseUp } onMouseDown={ props.VectoronMouseDown } onKeyDown={ props.VectoronKeyDown } onChange={ props.VectoronChange } ondelay={ props.Vectorondelay } width="17.77783203125" height="17.77777099609375">

                                              </svg>
                                            </CSSTransition>
                                            <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['vector']?.animationClass || {}}>
                                              <svg id="id_sixtwoozero_onesixsevenfive_eightonetwoo_seventhreefourseven" className="vector  " style={{}} onClick={ props.VectoronClick } onMouseEnter={ props.VectoronMouseEnter } onMouseOver={ props.VectoronMouseOver } onKeyPress={ props.VectoronKeyPress } onDrag={ props.VectoronDrag } onMouseLeave={ props.VectoronMouseLeave } onMouseUp={ props.VectoronMouseUp } onMouseDown={ props.VectoronMouseDown } onKeyDown={ props.VectoronKeyDown } onChange={ props.VectoronChange } ondelay={ props.Vectorondelay } width="4.83349609375" height="4.833343505859375">

                                              </svg>
                                            </CSSTransition>
                                          </div>

                                        </CSSTransition>
                                      </div>

                                    </CSSTransition>
                                    <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['buttontext']?.animationClass || {}}>

                                      <span id="id_sixtwoozero_onesixsevensix"  className={` text buttontext    ${ props.onClick ? 'cursor' : ''}  ${ transaction['buttontext']?.type ? transaction['buttontext']?.type.toLowerCase() : '' }`} style={{ ...{},  ...props.ButtonTextStyle , transitionDuration: transaction['buttontext']?.duration, transitionTimingFunction: transaction['buttontext']?.timingFunction }} onClick={ props.ButtonTextonClick } onMouseEnter={ props.ButtonTextonMouseEnter } onMouseOver={ props.ButtonTextonMouseOver } onKeyPress={ props.ButtonTextonKeyPress } onDrag={ props.ButtonTextonDrag } onMouseLeave={ props.ButtonTextonMouseLeave } onMouseUp={ props.ButtonTextonMouseUp } onMouseDown={ props.ButtonTextonMouseDown } onKeyDown={ props.ButtonTextonKeyDown } onChange={ props.ButtonTextonChange } ondelay={ props.ButtonTextondelay } >{props.ButtonText0 || `Download font`}</span>

                                    </CSSTransition>
                                    <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['lineroundedarrowright']?.animationClass || {}}>

                                      <div id="id_sixtwoozero_onesixsevenseven" className={` instance lineroundedarrowright ${ props.onClick ? 'cursor' : '' } ${ transaction['lineroundedarrowright']?.type ? transaction['lineroundedarrowright']?.type.toLowerCase() : '' }`} style={{ ...{},  ...props.LineRoundedArrowRightStyle , transitionDuration: transaction['lineroundedarrowright']?.duration, transitionTimingFunction: transaction['lineroundedarrowright']?.timingFunction }} onClick={ props.LineRoundedArrowRightonClick } onMouseEnter={ props.LineRoundedArrowRightonMouseEnter } onMouseOver={ props.LineRoundedArrowRightonMouseOver } onKeyPress={ props.LineRoundedArrowRightonKeyPress } onDrag={ props.LineRoundedArrowRightonDrag } onMouseLeave={ props.LineRoundedArrowRightonMouseLeave } onMouseUp={ props.LineRoundedArrowRightonMouseUp } onMouseDown={ props.LineRoundedArrowRightonMouseDown } onKeyDown={ props.LineRoundedArrowRightonKeyDown } onChange={ props.LineRoundedArrowRightonChange } ondelay={ props.LineRoundedArrowRightondelay }>
                                        <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['arrowright']?.animationClass || {}}>

                                          <div id="id_sixtwoozero_onesixsevenseven_eightonetwoo_seventhreefourfour" className={` group arrowright ${ props.onClick ? 'cursor' : '' } ${ transaction['arrowright']?.type ? transaction['arrowright']?.type.toLowerCase() : '' }`} style={{ ...{},  ...props.ArrowRightStyle , transitionDuration: transaction['arrowright']?.duration, transitionTimingFunction: transaction['arrowright']?.timingFunction }} onClick={ props.ArrowRightonClick } onMouseEnter={ props.ArrowRightonMouseEnter } onMouseOver={ props.ArrowRightonMouseOver } onKeyPress={ props.ArrowRightonKeyPress } onDrag={ props.ArrowRightonDrag } onMouseLeave={ props.ArrowRightonMouseLeave } onMouseUp={ props.ArrowRightonMouseUp } onMouseDown={ props.ArrowRightonMouseDown } onKeyDown={ props.ArrowRightonKeyDown } onChange={ props.ArrowRightonChange } ondelay={ props.ArrowRightondelay }>
                                            <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['line']?.animationClass || {}}>
                                              <svg id="id_sixtwoozero_onesixsevenseven_eightonetwoo_seventhreefourtwoo" className="line  " style={{}} onClick={ props.LineonClick } onMouseEnter={ props.LineonMouseEnter } onMouseOver={ props.LineonMouseOver } onKeyPress={ props.LineonKeyPress } onDrag={ props.LineonDrag } onMouseLeave={ props.LineonMouseLeave } onMouseUp={ props.LineonMouseUp } onMouseDown={ props.LineonMouseDown } onKeyDown={ props.LineonKeyDown } onChange={ props.LineonChange } ondelay={ props.Lineondelay } width="8.4609375" height="16.921722412109375">

                                              </svg>
                                            </CSSTransition>
                                            <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['line']?.animationClass || {}}>
                                              <svg id="id_sixtwoozero_onesixsevenseven_eightonetwoo_seventhreefourthree" className="line  " style={{}} onClick={ props.LineonClick } onMouseEnter={ props.LineonMouseEnter } onMouseOver={ props.LineonMouseOver } onKeyPress={ props.LineonKeyPress } onDrag={ props.LineonDrag } onMouseLeave={ props.LineonMouseLeave } onMouseUp={ props.LineonMouseUp } onMouseDown={ props.LineonMouseDown } onKeyDown={ props.LineonKeyDown } onChange={ props.LineonChange } ondelay={ props.Lineondelay } width="18" height="0">

                                              </svg>
                                            </CSSTransition>
                                          </div>

                                        </CSSTransition>
                                      </div>

                                    </CSSTransition>
                                  </div>

                                </CSSTransition>
                              </div>

                            </CSSTransition>
                          </div>

                        </CSSTransition>
                      </div>

                    </CSSTransition>
                    <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['typographylargeletters']?.animationClass || {}}>

                      <span id="id_sixtwoozero_onesixseveneight"  className={` text typographylargeletters    ${ props.onClick ? 'cursor' : ''}  ${ transaction['typographylargeletters']?.type ? transaction['typographylargeletters']?.type.toLowerCase() : '' }`} style={{ ...{},  ...props.TypographyLargeLettersStyle , transitionDuration: transaction['typographylargeletters']?.duration, transitionTimingFunction: transaction['typographylargeletters']?.timingFunction }} onClick={ props.TypographyLargeLettersonClick } onMouseEnter={ props.TypographyLargeLettersonMouseEnter } onMouseOver={ props.TypographyLargeLettersonMouseOver } onKeyPress={ props.TypographyLargeLettersonKeyPress } onDrag={ props.TypographyLargeLettersonDrag } onMouseLeave={ props.TypographyLargeLettersonMouseLeave } onMouseUp={ props.TypographyLargeLettersonMouseUp } onMouseDown={ props.TypographyLargeLettersonMouseDown } onKeyDown={ props.TypographyLargeLettersonKeyDown } onChange={ props.TypographyLargeLettersonChange } ondelay={ props.TypographyLargeLettersondelay } >{props.TypographyLargeLetters0 || `Aa`}</span>

                    </CSSTransition>
                  </div>

                </CSSTransition>
              </div>

            </CSSTransition>
          </div>

        </CSSTransition>

      </>
      }
    </div>

  </CSSTransition>
</>
    ) 
}

Typgraphy.propTypes = {
    style: PropTypes.any,
Typography0: PropTypes.any,
Rectangle0: PropTypes.any,
TypographyName0: PropTypes.any,
Book0: PropTypes.any,
Medium0: PropTypes.any,
Bold0: PropTypes.any,
Black0: PropTypes.any,
TypographyExample0: PropTypes.any,
ButtonText0: PropTypes.any,
TypographyLargeLetters0: PropTypes.any,
TypgraphyonClick: PropTypes.any,
TypgraphyonMouseEnter: PropTypes.any,
TypgraphyonMouseOver: PropTypes.any,
TypgraphyonKeyPress: PropTypes.any,
TypgraphyonDrag: PropTypes.any,
TypgraphyonMouseLeave: PropTypes.any,
TypgraphyonMouseUp: PropTypes.any,
TypgraphyonMouseDown: PropTypes.any,
TypgraphyonKeyDown: PropTypes.any,
TypgraphyonChange: PropTypes.any,
Typgraphyondelay: PropTypes.any,
TitleonClick: PropTypes.any,
TitleonMouseEnter: PropTypes.any,
TitleonMouseOver: PropTypes.any,
TitleonKeyPress: PropTypes.any,
TitleonDrag: PropTypes.any,
TitleonMouseLeave: PropTypes.any,
TitleonMouseUp: PropTypes.any,
TitleonMouseDown: PropTypes.any,
TitleonKeyDown: PropTypes.any,
TitleonChange: PropTypes.any,
Titleondelay: PropTypes.any,
TypographyonClick: PropTypes.any,
TypographyonMouseEnter: PropTypes.any,
TypographyonMouseOver: PropTypes.any,
TypographyonKeyPress: PropTypes.any,
TypographyonDrag: PropTypes.any,
TypographyonMouseLeave: PropTypes.any,
TypographyonMouseUp: PropTypes.any,
TypographyonMouseDown: PropTypes.any,
TypographyonKeyDown: PropTypes.any,
TypographyonChange: PropTypes.any,
Typographyondelay: PropTypes.any,
RectangleonClick: PropTypes.any,
RectangleonMouseEnter: PropTypes.any,
RectangleonMouseOver: PropTypes.any,
RectangleonKeyPress: PropTypes.any,
RectangleonDrag: PropTypes.any,
RectangleonMouseLeave: PropTypes.any,
RectangleonMouseUp: PropTypes.any,
RectangleonMouseDown: PropTypes.any,
RectangleonKeyDown: PropTypes.any,
RectangleonChange: PropTypes.any,
Rectangleondelay: PropTypes.any,
FramefoursixzerotwooonClick: PropTypes.any,
FramefoursixzerotwooonMouseEnter: PropTypes.any,
FramefoursixzerotwooonMouseOver: PropTypes.any,
FramefoursixzerotwooonKeyPress: PropTypes.any,
FramefoursixzerotwooonDrag: PropTypes.any,
FramefoursixzerotwooonMouseLeave: PropTypes.any,
FramefoursixzerotwooonMouseUp: PropTypes.any,
FramefoursixzerotwooonMouseDown: PropTypes.any,
FramefoursixzerotwooonKeyDown: PropTypes.any,
FramefoursixzerotwooonChange: PropTypes.any,
Framefoursixzerotwooondelay: PropTypes.any,
GroupfoursixzerozeroonClick: PropTypes.any,
GroupfoursixzerozeroonMouseEnter: PropTypes.any,
GroupfoursixzerozeroonMouseOver: PropTypes.any,
GroupfoursixzerozeroonKeyPress: PropTypes.any,
GroupfoursixzerozeroonDrag: PropTypes.any,
GroupfoursixzerozeroonMouseLeave: PropTypes.any,
GroupfoursixzerozeroonMouseUp: PropTypes.any,
GroupfoursixzerozeroonMouseDown: PropTypes.any,
GroupfoursixzerozeroonKeyDown: PropTypes.any,
GroupfoursixzerozeroonChange: PropTypes.any,
Groupfoursixzerozeroondelay: PropTypes.any,
FramefoursixzerothreeonClick: PropTypes.any,
FramefoursixzerothreeonMouseEnter: PropTypes.any,
FramefoursixzerothreeonMouseOver: PropTypes.any,
FramefoursixzerothreeonKeyPress: PropTypes.any,
FramefoursixzerothreeonDrag: PropTypes.any,
FramefoursixzerothreeonMouseLeave: PropTypes.any,
FramefoursixzerothreeonMouseUp: PropTypes.any,
FramefoursixzerothreeonMouseDown: PropTypes.any,
FramefoursixzerothreeonKeyDown: PropTypes.any,
FramefoursixzerothreeonChange: PropTypes.any,
Framefoursixzerothreeondelay: PropTypes.any,
TypographyDetailsonClick: PropTypes.any,
TypographyDetailsonMouseEnter: PropTypes.any,
TypographyDetailsonMouseOver: PropTypes.any,
TypographyDetailsonKeyPress: PropTypes.any,
TypographyDetailsonDrag: PropTypes.any,
TypographyDetailsonMouseLeave: PropTypes.any,
TypographyDetailsonMouseUp: PropTypes.any,
TypographyDetailsonMouseDown: PropTypes.any,
TypographyDetailsonKeyDown: PropTypes.any,
TypographyDetailsonChange: PropTypes.any,
TypographyDetailsondelay: PropTypes.any,
FramefoursixzerooneonClick: PropTypes.any,
FramefoursixzerooneonMouseEnter: PropTypes.any,
FramefoursixzerooneonMouseOver: PropTypes.any,
FramefoursixzerooneonKeyPress: PropTypes.any,
FramefoursixzerooneonDrag: PropTypes.any,
FramefoursixzerooneonMouseLeave: PropTypes.any,
FramefoursixzerooneonMouseUp: PropTypes.any,
FramefoursixzerooneonMouseDown: PropTypes.any,
FramefoursixzerooneonKeyDown: PropTypes.any,
FramefoursixzerooneonChange: PropTypes.any,
Framefoursixzerooneondelay: PropTypes.any,
TypographyNameonClick: PropTypes.any,
TypographyNameonMouseEnter: PropTypes.any,
TypographyNameonMouseOver: PropTypes.any,
TypographyNameonKeyPress: PropTypes.any,
TypographyNameonDrag: PropTypes.any,
TypographyNameonMouseLeave: PropTypes.any,
TypographyNameonMouseUp: PropTypes.any,
TypographyNameonMouseDown: PropTypes.any,
TypographyNameonKeyDown: PropTypes.any,
TypographyNameonChange: PropTypes.any,
TypographyNameondelay: PropTypes.any,
TypographyWeightsonClick: PropTypes.any,
TypographyWeightsonMouseEnter: PropTypes.any,
TypographyWeightsonMouseOver: PropTypes.any,
TypographyWeightsonKeyPress: PropTypes.any,
TypographyWeightsonDrag: PropTypes.any,
TypographyWeightsonMouseLeave: PropTypes.any,
TypographyWeightsonMouseUp: PropTypes.any,
TypographyWeightsonMouseDown: PropTypes.any,
TypographyWeightsonKeyDown: PropTypes.any,
TypographyWeightsonChange: PropTypes.any,
TypographyWeightsondelay: PropTypes.any,
BookonClick: PropTypes.any,
BookonMouseEnter: PropTypes.any,
BookonMouseOver: PropTypes.any,
BookonKeyPress: PropTypes.any,
BookonDrag: PropTypes.any,
BookonMouseLeave: PropTypes.any,
BookonMouseUp: PropTypes.any,
BookonMouseDown: PropTypes.any,
BookonKeyDown: PropTypes.any,
BookonChange: PropTypes.any,
Bookondelay: PropTypes.any,
MediumonClick: PropTypes.any,
MediumonMouseEnter: PropTypes.any,
MediumonMouseOver: PropTypes.any,
MediumonKeyPress: PropTypes.any,
MediumonDrag: PropTypes.any,
MediumonMouseLeave: PropTypes.any,
MediumonMouseUp: PropTypes.any,
MediumonMouseDown: PropTypes.any,
MediumonKeyDown: PropTypes.any,
MediumonChange: PropTypes.any,
Mediumondelay: PropTypes.any,
BoldonClick: PropTypes.any,
BoldonMouseEnter: PropTypes.any,
BoldonMouseOver: PropTypes.any,
BoldonKeyPress: PropTypes.any,
BoldonDrag: PropTypes.any,
BoldonMouseLeave: PropTypes.any,
BoldonMouseUp: PropTypes.any,
BoldonMouseDown: PropTypes.any,
BoldonKeyDown: PropTypes.any,
BoldonChange: PropTypes.any,
Boldondelay: PropTypes.any,
BlackonClick: PropTypes.any,
BlackonMouseEnter: PropTypes.any,
BlackonMouseOver: PropTypes.any,
BlackonKeyPress: PropTypes.any,
BlackonDrag: PropTypes.any,
BlackonMouseLeave: PropTypes.any,
BlackonMouseUp: PropTypes.any,
BlackonMouseDown: PropTypes.any,
BlackonKeyDown: PropTypes.any,
BlackonChange: PropTypes.any,
Blackondelay: PropTypes.any,
TypographyExampleonClick: PropTypes.any,
TypographyExampleonMouseEnter: PropTypes.any,
TypographyExampleonMouseOver: PropTypes.any,
TypographyExampleonKeyPress: PropTypes.any,
TypographyExampleonDrag: PropTypes.any,
TypographyExampleonMouseLeave: PropTypes.any,
TypographyExampleonMouseUp: PropTypes.any,
TypographyExampleonMouseDown: PropTypes.any,
TypographyExampleonKeyDown: PropTypes.any,
TypographyExampleonChange: PropTypes.any,
TypographyExampleondelay: PropTypes.any,
PrimaryButtononClick: PropTypes.any,
PrimaryButtononMouseEnter: PropTypes.any,
PrimaryButtononMouseOver: PropTypes.any,
PrimaryButtononKeyPress: PropTypes.any,
PrimaryButtononDrag: PropTypes.any,
PrimaryButtononMouseLeave: PropTypes.any,
PrimaryButtononMouseUp: PropTypes.any,
PrimaryButtononMouseDown: PropTypes.any,
PrimaryButtononKeyDown: PropTypes.any,
PrimaryButtononChange: PropTypes.any,
PrimaryButtonondelay: PropTypes.any,
MasterPrimaryButtononClick: PropTypes.any,
MasterPrimaryButtononMouseEnter: PropTypes.any,
MasterPrimaryButtononMouseOver: PropTypes.any,
MasterPrimaryButtononKeyPress: PropTypes.any,
MasterPrimaryButtononDrag: PropTypes.any,
MasterPrimaryButtononMouseLeave: PropTypes.any,
MasterPrimaryButtononMouseUp: PropTypes.any,
MasterPrimaryButtononMouseDown: PropTypes.any,
MasterPrimaryButtononKeyDown: PropTypes.any,
MasterPrimaryButtononChange: PropTypes.any,
MasterPrimaryButtonondelay: PropTypes.any,
LineRoundedSearchonClick: PropTypes.any,
LineRoundedSearchonMouseEnter: PropTypes.any,
LineRoundedSearchonMouseOver: PropTypes.any,
LineRoundedSearchonKeyPress: PropTypes.any,
LineRoundedSearchonDrag: PropTypes.any,
LineRoundedSearchonMouseLeave: PropTypes.any,
LineRoundedSearchonMouseUp: PropTypes.any,
LineRoundedSearchonMouseDown: PropTypes.any,
LineRoundedSearchonKeyDown: PropTypes.any,
LineRoundedSearchonChange: PropTypes.any,
LineRoundedSearchondelay: PropTypes.any,
SearchonClick: PropTypes.any,
SearchonMouseEnter: PropTypes.any,
SearchonMouseOver: PropTypes.any,
SearchonKeyPress: PropTypes.any,
SearchonDrag: PropTypes.any,
SearchonMouseLeave: PropTypes.any,
SearchonMouseUp: PropTypes.any,
SearchonMouseDown: PropTypes.any,
SearchonKeyDown: PropTypes.any,
SearchonChange: PropTypes.any,
Searchondelay: PropTypes.any,
VectoronClick: PropTypes.any,
VectoronMouseEnter: PropTypes.any,
VectoronMouseOver: PropTypes.any,
VectoronKeyPress: PropTypes.any,
VectoronDrag: PropTypes.any,
VectoronMouseLeave: PropTypes.any,
VectoronMouseUp: PropTypes.any,
VectoronMouseDown: PropTypes.any,
VectoronKeyDown: PropTypes.any,
VectoronChange: PropTypes.any,
Vectorondelay: PropTypes.any,
ButtonTextonClick: PropTypes.any,
ButtonTextonMouseEnter: PropTypes.any,
ButtonTextonMouseOver: PropTypes.any,
ButtonTextonKeyPress: PropTypes.any,
ButtonTextonDrag: PropTypes.any,
ButtonTextonMouseLeave: PropTypes.any,
ButtonTextonMouseUp: PropTypes.any,
ButtonTextonMouseDown: PropTypes.any,
ButtonTextonKeyDown: PropTypes.any,
ButtonTextonChange: PropTypes.any,
ButtonTextondelay: PropTypes.any,
LineRoundedArrowRightonClick: PropTypes.any,
LineRoundedArrowRightonMouseEnter: PropTypes.any,
LineRoundedArrowRightonMouseOver: PropTypes.any,
LineRoundedArrowRightonKeyPress: PropTypes.any,
LineRoundedArrowRightonDrag: PropTypes.any,
LineRoundedArrowRightonMouseLeave: PropTypes.any,
LineRoundedArrowRightonMouseUp: PropTypes.any,
LineRoundedArrowRightonMouseDown: PropTypes.any,
LineRoundedArrowRightonKeyDown: PropTypes.any,
LineRoundedArrowRightonChange: PropTypes.any,
LineRoundedArrowRightondelay: PropTypes.any,
ArrowRightonClick: PropTypes.any,
ArrowRightonMouseEnter: PropTypes.any,
ArrowRightonMouseOver: PropTypes.any,
ArrowRightonKeyPress: PropTypes.any,
ArrowRightonDrag: PropTypes.any,
ArrowRightonMouseLeave: PropTypes.any,
ArrowRightonMouseUp: PropTypes.any,
ArrowRightonMouseDown: PropTypes.any,
ArrowRightonKeyDown: PropTypes.any,
ArrowRightonChange: PropTypes.any,
ArrowRightondelay: PropTypes.any,
LineonClick: PropTypes.any,
LineonMouseEnter: PropTypes.any,
LineonMouseOver: PropTypes.any,
LineonKeyPress: PropTypes.any,
LineonDrag: PropTypes.any,
LineonMouseLeave: PropTypes.any,
LineonMouseUp: PropTypes.any,
LineonMouseDown: PropTypes.any,
LineonKeyDown: PropTypes.any,
LineonChange: PropTypes.any,
Lineondelay: PropTypes.any,
TypographyLargeLettersonClick: PropTypes.any,
TypographyLargeLettersonMouseEnter: PropTypes.any,
TypographyLargeLettersonMouseOver: PropTypes.any,
TypographyLargeLettersonKeyPress: PropTypes.any,
TypographyLargeLettersonDrag: PropTypes.any,
TypographyLargeLettersonMouseLeave: PropTypes.any,
TypographyLargeLettersonMouseUp: PropTypes.any,
TypographyLargeLettersonMouseDown: PropTypes.any,
TypographyLargeLettersonKeyDown: PropTypes.any,
TypographyLargeLettersonChange: PropTypes.any,
TypographyLargeLettersondelay: PropTypes.any
}
export default Typgraphy;