import React, { Suspense } from 'react';
import PropTypes from 'prop-types';
import * as Contexts  from 'Contexts';
import { OverlaySwapContext } from 'Components/OverlaySwapProvider';
import CorebooksAnimation from 'Components/CorebooksAnimation';
import CorebooksAnimationSingle from 'Components/CorebooksAnimationSingle';

import * as UtilsScripts  from 'utils/utils';
import { Route, useHistory, useParams } from "react-router-dom";
import SingletoneNavigation from 'Class/SingletoneNavigation';

import LoadingComponent from "Components/Loading";


import { Transition } from "react-transition-group";
import { CSSTransition } from "react-transition-group";
import ButtonGreen from 'Components/ButtonGreen'
import Displaytwoo from 'Components/Displaytwoo'
import Framethreethreetwoo from 'Components/Framethreethreetwoo'
import Framethreeoneseven from 'Components/Framethreeoneseven'
import Framethreetwooone from 'Components/Framethreetwooone'
import Displayone from 'Components/Displayone'
import Displayfive from 'Components/Displayfive'
import { useAppContext, useSessionContext } from 'context/AppContext';
import './Help.css'




export const HelpLocalStateReducer = (state = {}, action) => {

    return {
        ...state,
        [action.payload.key]: action.payload.value,
    };
};
    

const Help = (props)=>{
    const [state,dispatch] = useAppContext();
    const [, setNameSelectedComponent] = React.useContext(OverlaySwapContext);
    
    const nodeRef = React.useRef(null);
    const history = useHistory();
    let RouterContext = useParams();
    
    
    
    
    
    
    const [globalStateForm, dispatchForm] = React.useState({});
    
    const [ _in, setIn ] = React.useState(false)
    const [ transaction, setTransaction ] = React.useState({}) 
    const [ animation, setAnimation ] = React.useState('')

    const [HelpLocalState, dispatchHelp] = React.useReducer(HelpLocalStateReducer, { ...{"View":"row","search":0}, ...(props.state || {} ) });
    
    
    
    
    
    React.useEffect(()=>{
        

        
        
    },[]);
    
    
    return (
        <Contexts.HelpLocalContext.Provider value={[HelpLocalState,dispatchHelp]}>
  <CSSTransition nodeRef={nodeRef} in={true} appear={true} timeout={400} classNames={SingletoneNavigation.getTransitionInstance()?.Help?.cssClass || '' }>

    <div id="id_threeonefour_onenigthzerofive" ref={nodeRef} className={` ${ props.onClick ? 'cursor' : '' } help ${ props.cssClass } ${ transaction['help']?.type ? transaction['help']?.type.toLowerCase() : '' }`} style={ { ...{ ...{"width":"100%"}, transitionDuration: `${((SingletoneNavigation.getTransitionInstance()?.Help?.duration || 0) * 1000).toFixed(0)}ms` }, ...props.style }} onClick={ props.HelponClick } onMouseEnter={ props.HelponMouseEnter } onMouseOver={ props.HelponMouseOver } onKeyPress={ props.HelponKeyPress } onDrag={ props.HelponDrag } onMouseLeave={ props.HelponMouseLeave } onMouseUp={ props.HelponMouseUp } onMouseDown={ props.HelponMouseDown } onKeyDown={ props.HelponKeyDown } onChange={ props.HelponChange } ondelay={ props.Helpondelay }>
      {
      props.children ?
      props.children :
      <>
        <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['dashboardmastercontainer']?.animationClass || {}}>

          <div id="id_threeonefour_onenigthzerosix" className={` frame dashboardmastercontainer ${ props.onClick ? 'cursor' : '' } ${ transaction['dashboardmastercontainer']?.type ? transaction['dashboardmastercontainer']?.type.toLowerCase() : '' }`} style={ { ...{"width":"100%","height":"100%"}, ...props.DashboardMasterContainerStyle , transitionDuration: transaction['dashboardmastercontainer']?.duration, transitionTimingFunction: transaction['dashboardmastercontainer']?.timingFunction } } onClick={ props.DashboardMasterContaineronClick } onMouseEnter={ props.DashboardMasterContaineronMouseEnter } onMouseOver={ props.DashboardMasterContaineronMouseOver } onKeyPress={ props.DashboardMasterContaineronKeyPress } onDrag={ props.DashboardMasterContaineronDrag } onMouseLeave={ props.DashboardMasterContaineronMouseLeave } onMouseUp={ props.DashboardMasterContaineronMouseUp } onMouseDown={ props.DashboardMasterContaineronMouseDown } onKeyDown={ props.DashboardMasterContaineronKeyDown } onChange={ props.DashboardMasterContaineronChange } ondelay={ props.DashboardMasterContainerondelay }>
            <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['framethreeonethree']?.animationClass || {}}>

              <div id="id_threeonefour_onenigthzeroseven" className={` frame framethreeonethree ${ props.onClick ? 'cursor' : '' } ${ transaction['framethreeonethree']?.type ? transaction['framethreeonethree']?.type.toLowerCase() : '' }`} style={ { ...{}, ...props.FramethreeonethreeStyle , transitionDuration: transaction['framethreeonethree']?.duration, transitionTimingFunction: transaction['framethreeonethree']?.timingFunction } } onClick={ props.FramethreeonethreeonClick } onMouseEnter={ props.FramethreeonethreeonMouseEnter } onMouseOver={ props.FramethreeonethreeonMouseOver } onKeyPress={ props.FramethreeonethreeonKeyPress } onDrag={ props.FramethreeonethreeonDrag } onMouseLeave={ props.FramethreeonethreeonMouseLeave } onMouseUp={ props.FramethreeonethreeonMouseUp } onMouseDown={ props.FramethreeonethreeonMouseDown } onKeyDown={ props.FramethreeonethreeonKeyDown } onChange={ props.FramethreeonethreeonChange } ondelay={ props.Framethreeonethreeondelay }>
                <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['projectscardsdashboard']?.animationClass || {}}>

                  <div id="id_threeonefour_onenigthzeroeight" className={` frame projectscardsdashboard ${ props.onClick ? 'cursor' : '' } ${ transaction['projectscardsdashboard']?.type ? transaction['projectscardsdashboard']?.type.toLowerCase() : '' }`} style={ { ...{"flexWrap":"nowrap","overflow":"auto","margin":"auto","width":"100%","height":"100%"}, ...props.ProjectsCardsDashboardStyle , transitionDuration: transaction['projectscardsdashboard']?.duration, transitionTimingFunction: transaction['projectscardsdashboard']?.timingFunction } } onClick={ props.ProjectsCardsDashboardonClick } onMouseEnter={ props.ProjectsCardsDashboardonMouseEnter } onMouseOver={ props.ProjectsCardsDashboardonMouseOver } onKeyPress={ props.ProjectsCardsDashboardonKeyPress } onDrag={ props.ProjectsCardsDashboardonDrag } onMouseLeave={ props.ProjectsCardsDashboardonMouseLeave } onMouseUp={ props.ProjectsCardsDashboardonMouseUp } onMouseDown={ props.ProjectsCardsDashboardonMouseDown } onKeyDown={ props.ProjectsCardsDashboardonKeyDown } onChange={ props.ProjectsCardsDashboardonChange } ondelay={ props.ProjectsCardsDashboardondelay }>
                    <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['frametwooseventhree']?.animationClass || {}}>

                      <div id="id_threeonefour_onenigthzeronigth" className={` frame frametwooseventhree ${ props.onClick ? 'cursor' : '' } ${ transaction['frametwooseventhree']?.type ? transaction['frametwooseventhree']?.type.toLowerCase() : '' }`} style={ { ...{}, ...props.FrametwooseventhreeStyle , transitionDuration: transaction['frametwooseventhree']?.duration, transitionTimingFunction: transaction['frametwooseventhree']?.timingFunction } } onClick={ props.FrametwooseventhreeonClick } onMouseEnter={ props.FrametwooseventhreeonMouseEnter } onMouseOver={ props.FrametwooseventhreeonMouseOver } onKeyPress={ props.FrametwooseventhreeonKeyPress } onDrag={ props.FrametwooseventhreeonDrag } onMouseLeave={ props.FrametwooseventhreeonMouseLeave } onMouseUp={ props.FrametwooseventhreeonMouseUp } onMouseDown={ props.FrametwooseventhreeonMouseDown } onKeyDown={ props.FrametwooseventhreeonKeyDown } onChange={ props.FrametwooseventhreeonChange } ondelay={ props.Frametwooseventhreeondelay }>
                        <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['framethreethreezero']?.animationClass || {}}>

                          <div id="id_threeonefour_foureightnigthnigth" className={` frame framethreethreezero ${ props.onClick ? 'cursor' : '' } ${ transaction['framethreethreezero']?.type ? transaction['framethreethreezero']?.type.toLowerCase() : '' }`} style={ { ...{}, ...props.FramethreethreezeroStyle , transitionDuration: transaction['framethreethreezero']?.duration, transitionTimingFunction: transaction['framethreethreezero']?.timingFunction } } onClick={ props.FramethreethreezeroonClick } onMouseEnter={ props.FramethreethreezeroonMouseEnter } onMouseOver={ props.FramethreethreezeroonMouseOver } onKeyPress={ props.FramethreethreezeroonKeyPress } onDrag={ props.FramethreethreezeroonDrag } onMouseLeave={ props.FramethreethreezeroonMouseLeave } onMouseUp={ props.FramethreethreezeroonMouseUp } onMouseDown={ props.FramethreethreezeroonMouseDown } onKeyDown={ props.FramethreethreezeroonKeyDown } onChange={ props.FramethreethreezeroonChange } ondelay={ props.Framethreethreezeroondelay }>
                            <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['framethreetwoonigth']?.animationClass || {}}>

                              <div id="id_threeonefour_foureightnigtheight" className={` frame framethreetwoonigth ${ props.onClick ? 'cursor' : '' } ${ transaction['framethreetwoonigth']?.type ? transaction['framethreetwoonigth']?.type.toLowerCase() : '' }`} style={ { ...{}, ...props.FramethreetwoonigthStyle , transitionDuration: transaction['framethreetwoonigth']?.duration, transitionTimingFunction: transaction['framethreetwoonigth']?.timingFunction } } onClick={ props.FramethreetwoonigthonClick } onMouseEnter={ props.FramethreetwoonigthonMouseEnter } onMouseOver={ props.FramethreetwoonigthonMouseOver } onKeyPress={ props.FramethreetwoonigthonKeyPress } onDrag={ props.FramethreetwoonigthonDrag } onMouseLeave={ props.FramethreetwoonigthonMouseLeave } onMouseUp={ props.FramethreetwoonigthonMouseUp } onMouseDown={ props.FramethreetwoonigthonMouseDown } onKeyDown={ props.FramethreetwoonigthonKeyDown } onChange={ props.FramethreetwoonigthonChange } ondelay={ props.Framethreetwoonigthondelay }>
                                <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['framethreeoneeight']?.animationClass || {}}>

                                  <div id="id_threeonefour_twootwootwooeight" className={` frame framethreeoneeight ${ props.onClick ? 'cursor' : '' } ${ transaction['framethreeoneeight']?.type ? transaction['framethreeoneeight']?.type.toLowerCase() : '' }`} style={ { ...{}, ...props.FramethreeoneeightStyle , transitionDuration: transaction['framethreeoneeight']?.duration, transitionTimingFunction: transaction['framethreeoneeight']?.timingFunction } } onClick={ props.FramethreeoneeightonClick } onMouseEnter={ props.FramethreeoneeightonMouseEnter } onMouseOver={ props.FramethreeoneeightonMouseOver } onKeyPress={ props.FramethreeoneeightonKeyPress } onDrag={ props.FramethreeoneeightonDrag } onMouseLeave={ props.FramethreeoneeightonMouseLeave } onMouseUp={ props.FramethreeoneeightonMouseUp } onMouseDown={ props.FramethreeoneeightonMouseDown } onKeyDown={ props.FramethreeoneeightonKeyDown } onChange={ props.FramethreeoneeightonChange } ondelay={ props.Framethreeoneeightondelay }>
                                    <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['displayfive']?.animationClass || {}}>
                                      <Displayfive { ...{ ...props, style:false } } DisplayfiveText0={ props.DisplayfiveText0 || "ASISTENCIA DE SPOTIFY" } DisplayfiveText0={ props.DisplayfiveText0 || "ASISTENCIA DE SPOTIFY" } cssClass={"C_sixtwooeight_onesixfourzero "}  />
    </CSSTransition >
<CSSTransition 
        in={_in}
        appear={true} 
        timeout={100}
        classNames={transaction['displayone']?.animationClass || {}}
    >
    <Displayone { ...{ ...props, style:false } }   DisplayoneText0={ props.DisplayoneText0 || " ¿Cómo te podemos ayudar?"} cssClass={"C_sixtwoosix_onesixsixtwoo "}  />
    </CSSTransition >
<CSSTransition 
        in={_in}
        appear={true} 
        timeout={100}
        classNames={transaction['framethreeoneseven']?.animationClass || {}}
    >
    
                    <div id="id_threeonefour_twootwootwooseven" className={` frame framethreeoneseven ${ props.onClick ? 'cursor' : '' } ${ transaction['framethreeoneseven']?.type ? transaction['framethreeoneseven']?.type.toLowerCase() : '' }`} style={ { ...{}, ...props.FramethreeonesevenStyle , transitionDuration: transaction['framethreeoneseven']?.duration, transitionTimingFunction: transaction['framethreeoneseven']?.timingFunction } } onClick={ props.FramethreeonesevenonClick } onMouseEnter={ props.FramethreeonesevenonMouseEnter } onMouseOver={ props.FramethreeonesevenonMouseOver } onKeyPress={ props.FramethreeonesevenonKeyPress } onDrag={ props.FramethreeonesevenonDrag } onMouseLeave={ props.FramethreeonesevenonMouseLeave } onMouseUp={ props.FramethreeonesevenonMouseUp } onMouseDown={ props.FramethreeonesevenonMouseDown } onKeyDown={ props.FramethreeonesevenonKeyDown } onChange={ props.FramethreeonesevenonChange } ondelay={ props.Framethreeonesevenondelay }>
                                        <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['framethreeonefive']?.animationClass || {}}>

                                          <div id="id_threeonefour_twootwoooneseven" className={` frame framethreeonefive ${ props.onClick ? 'cursor' : '' } ${ transaction['framethreeonefive']?.type ? transaction['framethreeonefive']?.type.toLowerCase() : '' }`} style={ { ...{}, ...props.FramethreeonefiveStyle , transitionDuration: transaction['framethreeonefive']?.duration, transitionTimingFunction: transaction['framethreeonefive']?.timingFunction } } onClick={ props.FramethreeonefiveonClick } onMouseEnter={ props.FramethreeonefiveonMouseEnter } onMouseOver={ props.FramethreeonefiveonMouseOver } onKeyPress={ props.FramethreeonefiveonKeyPress } onDrag={ props.FramethreeonefiveonDrag } onMouseLeave={ props.FramethreeonefiveonMouseLeave } onMouseUp={ props.FramethreeonefiveonMouseUp } onMouseDown={ props.FramethreeonefiveonMouseDown } onKeyDown={ props.FramethreeonefiveonKeyDown } onChange={ props.FramethreeonefiveonChange } ondelay={ props.Framethreeonefiveondelay }>
                                            <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['honetext']?.animationClass || {}}>

                                              <span id="id_threeonefour_twootwooonezero"  className={` text honetext    ${ props.onClick ? 'cursor' : ''}  ${ transaction['honetext']?.type ? transaction['honetext']?.type.toLowerCase() : '' }`} style={{ ...{},  ...props.HoneTextStyle , transitionDuration: transaction['honetext']?.duration, transitionTimingFunction: transaction['honetext']?.timingFunction }} onClick={ props.HoneTextonClick } onMouseEnter={ props.HoneTextonMouseEnter } onMouseOver={ props.HoneTextonMouseOver } onKeyPress={ props.HoneTextonKeyPress } onDrag={ props.HoneTextonDrag } onMouseLeave={ props.HoneTextonMouseLeave } onMouseUp={ props.HoneTextonMouseUp } onMouseDown={ props.HoneTextonMouseDown } onKeyDown={ props.HoneTextonKeyDown } onChange={ props.HoneTextonChange } ondelay={ props.HoneTextondelay } >{props.HoneText0 || `Iniciar sesión`}</span>

                                            </CSSTransition>
                                          </div>

                                        </CSSTransition>
                                        <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['displayfive']?.animationClass || {}}>
                                          <Displayfive { ...{ ...props, style:false } } DisplayfiveText0={ props.DisplayfiveText1 || "para obtener ayuda más rápido" } DisplayfiveText0={ props.DisplayfiveText1 || "para obtener ayuda más rápido" } cssClass={"C_sixtwooeight_onesixfourtwoo "}  />
    </CSSTransition >
                    </div>
               
    </CSSTransition >
                    </div>
               
    </CSSTransition >
<CSSTransition 
        in={_in}
        appear={true} 
        timeout={100}
        classNames={transaction['searchbard']?.animationClass || {}}
    >
    
                    <div id="id_threeonefour_twootwoothreefive" className={` frame searchbard ${ props.onClick ? 'cursor' : '' } ${ transaction['searchbard']?.type ? transaction['searchbard']?.type.toLowerCase() : '' }`} style={ { ...{}, ...props.SearchBarDStyle , transitionDuration: transaction['searchbard']?.duration, transitionTimingFunction: transaction['searchbard']?.timingFunction } } onClick={ props.SearchBarDonClick } onMouseEnter={ props.SearchBarDonMouseEnter } onMouseOver={ props.SearchBarDonMouseOver } onKeyPress={ props.SearchBarDonKeyPress } onDrag={ props.SearchBarDonDrag } onMouseLeave={ props.SearchBarDonMouseLeave } onMouseUp={ props.SearchBarDonMouseUp } onMouseDown={ props.SearchBarDonMouseDown } onKeyDown={ props.SearchBarDonKeyDown } onChange={ props.SearchBarDonChange } ondelay={ props.SearchBarDondelay }>
                                            <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['framethreeonenigth']?.animationClass || {}}>

                                              <div id="id_threeonefour_twoosevennigthsix" className={` frame framethreeonenigth ${ props.onClick ? 'cursor' : '' } ${ transaction['framethreeonenigth']?.type ? transaction['framethreeonenigth']?.type.toLowerCase() : '' }`} style={ { ...{}, ...props.FramethreeonenigthStyle , transitionDuration: transaction['framethreeonenigth']?.duration, transitionTimingFunction: transaction['framethreeonenigth']?.timingFunction } } onClick={ props.FramethreeonenigthonClick } onMouseEnter={ props.FramethreeonenigthonMouseEnter } onMouseOver={ props.FramethreeonenigthonMouseOver } onKeyPress={ props.FramethreeonenigthonKeyPress } onDrag={ props.FramethreeonenigthonDrag } onMouseLeave={ props.FramethreeonenigthonMouseLeave } onMouseUp={ props.FramethreeonenigthonMouseUp } onMouseDown={ props.FramethreeonenigthonMouseDown } onKeyDown={ props.FramethreeonenigthonKeyDown } onChange={ props.FramethreeonenigthonChange } ondelay={ props.Framethreeonenigthondelay }>
                                                <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['vector']?.animationClass || {}}>
                                                  <svg id="id_threeonefour_twootwoothreethree" className="vector  " style={{}} onClick={ props.VectoronClick } onMouseEnter={ props.VectoronMouseEnter } onMouseOver={ props.VectoronMouseOver } onKeyPress={ props.VectoronKeyPress } onDrag={ props.VectoronDrag } onMouseLeave={ props.VectoronMouseLeave } onMouseUp={ props.VectoronMouseUp } onMouseDown={ props.VectoronMouseDown } onKeyDown={ props.VectoronKeyDown } onChange={ props.VectoronChange } ondelay={ props.Vectorondelay } width="21.38623046875" height="21.1593017578125">
                                                    <path d="M9.407 0.00100005C4.227 0.00100005 0 4.141 0 9.28C0 14.419 4.226 18.559 9.407 18.559C11.641 18.559 13.697 17.789 15.314 16.501L19.667 20.854C19.7592 20.9495 19.8696 21.0257 19.9916 21.0781C20.1136 21.1305 20.2448 21.1581 20.3776 21.1593C20.5104 21.1604 20.6421 21.1351 20.765 21.0848C20.8878 21.0345 20.9995 20.9603 21.0934 20.8664C21.1873 20.7725 21.2615 20.6609 21.3118 20.538C21.3621 20.4151 21.3874 20.2834 21.3863 20.1506C21.3851 20.0178 21.3575 19.8866 21.3051 19.7646C21.2527 19.6426 21.1765 19.5322 21.081 19.44L16.737 15.096C18.0819 13.4563 18.816 11.4007 18.814 9.28C18.814 4.14 14.588 0 9.407 0L9.407 0.00100005ZM2 9.28C2 5.274 5.302 2 9.407 2C13.512 2 16.814 5.274 16.814 9.28C16.814 13.286 13.512 16.559 9.407 16.559C5.302 16.559 2 13.287 2 9.28Z" />
                                                  </svg>
                                                </CSSTransition>
                                              </div>

                                            </CSSTransition>
                                            <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['searchbartextd']?.animationClass || {}}>

                                              <div id="id_threeonefour_twootwoothreesix" className={` frame searchbartextd ${ props.onClick ? 'cursor' : '' } ${ transaction['searchbartextd']?.type ? transaction['searchbartextd']?.type.toLowerCase() : '' }`} style={ { ...{}, ...props.SearchBarTextDStyle , transitionDuration: transaction['searchbartextd']?.duration, transitionTimingFunction: transaction['searchbartextd']?.timingFunction } } onClick={ props.SearchBarTextDonClick } onMouseEnter={ props.SearchBarTextDonMouseEnter } onMouseOver={ props.SearchBarTextDonMouseOver } onKeyPress={ props.SearchBarTextDonKeyPress } onDrag={ props.SearchBarTextDonDrag } onMouseLeave={ props.SearchBarTextDonMouseLeave } onMouseUp={ props.SearchBarTextDonMouseUp } onMouseDown={ props.SearchBarTextDonMouseDown } onKeyDown={ props.SearchBarTextDonKeyDown } onChange={ props.SearchBarTextDonChange } ondelay={ props.SearchBarTextDondelay }>

                                              </div>

                                            </CSSTransition>
                                  </div>

                                </CSSTransition>
                              </div>

                            </CSSTransition>
                            <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['framethreetwooeight']?.animationClass || {}}>

                              <div id="id_threeonefour_foureightnigthseven" className={` frame framethreetwooeight ${ props.onClick ? 'cursor' : '' } ${ transaction['framethreetwooeight']?.type ? transaction['framethreetwooeight']?.type.toLowerCase() : '' }`} style={ { ...{}, ...props.FramethreetwooeightStyle , transitionDuration: transaction['framethreetwooeight']?.duration, transitionTimingFunction: transaction['framethreetwooeight']?.timingFunction } } onClick={ props.FramethreetwooeightonClick } onMouseEnter={ props.FramethreetwooeightonMouseEnter } onMouseOver={ props.FramethreetwooeightonMouseOver } onKeyPress={ props.FramethreetwooeightonKeyPress } onDrag={ props.FramethreetwooeightonDrag } onMouseLeave={ props.FramethreetwooeightonMouseLeave } onMouseUp={ props.FramethreetwooeightonMouseUp } onMouseDown={ props.FramethreetwooeightonMouseDown } onKeyDown={ props.FramethreetwooeightonKeyDown } onChange={ props.FramethreetwooeightonChange } ondelay={ props.Framethreetwooeightondelay }>
                                <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['framethreetwootwoo']?.animationClass || {}}>
                                  <Framethreetwooone { ...{ ...props, style:false } } cssClass={"C_threeonefour_foureightonenigth "}  />
    </CSSTransition >
<CSSTransition 
        in={_in}
        appear={true} 
        timeout={100}
        classNames={transaction['framethreetwoothree']?.animationClass || {}}
    >
    <Framethreetwooone { ...{ ...props, style:false } }   Ayudaconlospagos0={ props.Ayudaconlospagos1 || " Ayuda con un plan"} Framethreetwooone0={ props.Framethreetwooone1 || "" } IMAGE0={ props.IMAGE1 || "https://cdn.uncodie.com/tACQBrKCV97e0EqPE1XNLJ/27bedb0007f2a85b9b23d257492bef3c07d583c6.png" } cssClass={"C_threeonefour_foureightsevenseven "}  />
    </CSSTransition >
<CSSTransition 
        in={_in}
        appear={true} 
        timeout={100}
        classNames={transaction['framethreetwoofour']?.animationClass || {}}
    >
    <Framethreetwooone { ...{ ...props, style:false } }   Ayudaconlospagos0={ props.Ayudaconlospagos2 || " Ayuda con la aplicación"} Framethreetwooone0={ props.Framethreetwooone2 || "" } IMAGE0={ props.IMAGE2 || "https://cdn.uncodie.com/tACQBrKCV97e0EqPE1XNLJ/2ed1193bfbc03eec682df3a6ae73082397f642f3.png" } cssClass={"C_threeonefour_foureighteightone "}  />
    </CSSTransition >
<CSSTransition 
        in={_in}
        appear={true} 
        timeout={100}
        classNames={transaction['framethreetwoofive']?.animationClass || {}}
    >
    <Framethreetwooone { ...{ ...props, style:false } }   Ayudaconlospagos0={ props.Ayudaconlospagos3 || " Ayuda con un dispositivo"} Framethreetwooone0={ props.Framethreetwooone3 || "" } IMAGE0={ props.IMAGE3 || "https://cdn.uncodie.com/tACQBrKCV97e0EqPE1XNLJ/6968b3ee2db2655671bf700bbf4cd4e649968aac.png" } cssClass={"C_threeonefour_foureighteightfive "}  />
    </CSSTransition >
<CSSTransition 
        in={_in}
        appear={true} 
        timeout={100}
        classNames={transaction['framethreetwoosix']?.animationClass || {}}
    >
    <Framethreetwooone { ...{ ...props, style:false } }   Ayudaconlospagos0={ props.Ayudaconlospagos4 || " Seguridad y privacidad"} Framethreetwooone0={ props.Framethreetwooone4 || "" } IMAGE0={ props.IMAGE4 || "https://cdn.uncodie.com/tACQBrKCV97e0EqPE1XNLJ/57e2e0db35a54d882a326acefe4f02e39f5e689e.png" } cssClass={"C_threeonefour_foureighteightnigth "}  />
    </CSSTransition >
<CSSTransition 
        in={_in}
        appear={true} 
        timeout={100}
        classNames={transaction['framethreetwooseven']?.animationClass || {}}
    >
    <Framethreetwooone { ...{ ...props, style:false } }   Ayudaconlospagos0={ props.Ayudaconlospagos5 || " Ayuda con la cuenta"} Framethreetwooone0={ props.Framethreetwooone5 || "" } IMAGE0={ props.IMAGE5 || "https://cdn.uncodie.com/tACQBrKCV97e0EqPE1XNLJ/235483ca69712d2d04c85ea2c8a312b9a2917124.png" } cssClass={"C_threeonefour_foureightnigththree "}  />
    </CSSTransition >
                    </div>
               
    </CSSTransition >
                    </div>
               
    </CSSTransition >
<CSSTransition 
        in={_in}
        appear={true} 
        timeout={100}
        classNames={transaction['framethreefourone']?.animationClass || {}}
    >
    
                    <div id="id_fourthreethree_oneonethreeseven" className={` frame framethreefourone ${ props.onClick ? 'cursor' : '' } ${ transaction['framethreefourone']?.type ? transaction['framethreefourone']?.type.toLowerCase() : '' }`} style={ { ...{}, ...props.FramethreefouroneStyle , transitionDuration: transaction['framethreefourone']?.duration, transitionTimingFunction: transaction['framethreefourone']?.timingFunction } } onClick={ props.FramethreefouroneonClick } onMouseEnter={ props.FramethreefouroneonMouseEnter } onMouseOver={ props.FramethreefouroneonMouseOver } onKeyPress={ props.FramethreefouroneonKeyPress } onDrag={ props.FramethreefouroneonDrag } onMouseLeave={ props.FramethreefouroneonMouseLeave } onMouseUp={ props.FramethreefouroneonMouseUp } onMouseDown={ props.FramethreefouroneonMouseDown } onKeyDown={ props.FramethreefouroneonKeyDown } onChange={ props.FramethreefouroneonChange } ondelay={ props.Framethreefouroneondelay }>
                                    <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['framethreefourtwoo']?.animationClass || {}}>

                                      <div id="id_fourthreethree_oneonethreeeight" className={` frame framethreefourtwoo ${ props.onClick ? 'cursor' : '' } ${ transaction['framethreefourtwoo']?.type ? transaction['framethreefourtwoo']?.type.toLowerCase() : '' }`} style={ { ...{}, ...props.FramethreefourtwooStyle , transitionDuration: transaction['framethreefourtwoo']?.duration, transitionTimingFunction: transaction['framethreefourtwoo']?.timingFunction } } onClick={ props.FramethreefourtwooonClick } onMouseEnter={ props.FramethreefourtwooonMouseEnter } onMouseOver={ props.FramethreefourtwooonMouseOver } onKeyPress={ props.FramethreefourtwooonKeyPress } onDrag={ props.FramethreefourtwooonDrag } onMouseLeave={ props.FramethreefourtwooonMouseLeave } onMouseUp={ props.FramethreefourtwooonMouseUp } onMouseDown={ props.FramethreefourtwooonMouseDown } onKeyDown={ props.FramethreefourtwooonKeyDown } onChange={ props.FramethreefourtwooonChange } ondelay={ props.Framethreefourtwooondelay }>
                                        <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['framethreefourzero']?.animationClass || {}}>
                                          <Framethreeoneseven { ...{ ...props, style:false } } HoneText0={ props.HoneText0 || "Ayuda rápida" } cssClass={"C_fourthreethree_oneonezerofour "}  />
    </CSSTransition >
<CSSTransition 
        in={_in}
        appear={true} 
        timeout={100}
        classNames={transaction['framethreethreenigth']?.animationClass || {}}
    >
    
                    <div id="id_fourthreethree_oneonezerothree" className={` frame framethreethreenigth ${ props.onClick ? 'cursor' : '' } ${ transaction['framethreethreenigth']?.type ? transaction['framethreethreenigth']?.type.toLowerCase() : '' }`} style={ { ...{}, ...props.FramethreethreenigthStyle , transitionDuration: transaction['framethreethreenigth']?.duration, transitionTimingFunction: transaction['framethreethreenigth']?.timingFunction } } onClick={ props.FramethreethreenigthonClick } onMouseEnter={ props.FramethreethreenigthonMouseEnter } onMouseOver={ props.FramethreethreenigthonMouseOver } onKeyPress={ props.FramethreethreenigthonKeyPress } onDrag={ props.FramethreethreenigthonDrag } onMouseLeave={ props.FramethreethreenigthonMouseLeave } onMouseUp={ props.FramethreethreenigthonMouseUp } onMouseDown={ props.FramethreethreenigthonMouseDown } onKeyDown={ props.FramethreethreenigthonKeyDown } onChange={ props.FramethreethreenigthonChange } ondelay={ props.Framethreethreenigthondelay }>
                                            <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['framethreethreethree']?.animationClass || {}}>
                                              <Framethreethreetwoo { ...{ ...props, style:false } } HoneText0={ props.HoneText0 || "No puedo establecer mi contraseña" } cssClass={"C_fourthreethree_onezeroseventhree "}  />
    </CSSTransition >
<CSSTransition 
        in={_in}
        appear={true} 
        timeout={100}
        classNames={transaction['framethreethreefour']?.animationClass || {}}
    >
    <Framethreethreetwoo { ...{ ...props, style:false } }   HoneText0={ props.HoneText1 || " No recuerdo mi información para iniciar sesión"} cssClass={"C_fourthreethree_onezeroseveneight "}  />
    </CSSTransition >
<CSSTransition 
        in={_in}
        appear={true} 
        timeout={100}
        classNames={transaction['framethreethreefive']?.animationClass || {}}
    >
    <Framethreethreetwoo { ...{ ...props, style:false } }    cssClass={" C_fourthreethree_onezeroeightthree "}  />
    </CSSTransition >
<CSSTransition 
        in={_in}
        appear={true} 
        timeout={100}
        classNames={transaction['framethreethreesix']?.animationClass || {}}
    >
    <Framethreethreetwoo { ...{ ...props, style:false } }   HoneText0={ props.HoneText3 || " Ayuda para iniciar sesión con Facebook"} cssClass={"C_fourthreethree_onezeroeighteight "}  />
    </CSSTransition >
<CSSTransition 
        in={_in}
        appear={true} 
        timeout={100}
        classNames={transaction['framethreethreeseven']?.animationClass || {}}
    >
    <Framethreethreetwoo { ...{ ...props, style:false } }   HoneText0={ props.HoneText4 || " Método de pago"} cssClass={"C_fourthreethree_onezeronigththree "}  />
    </CSSTransition >
<CSSTransition 
        in={_in}
        appear={true} 
        timeout={100}
        classNames={transaction['framethreethreeeight']?.animationClass || {}}
    >
    <Framethreethreetwoo { ...{ ...props, style:false } }   HoneText0={ props.HoneText5 || " Suscríbete o únete a Premium Familiar"} cssClass={"C_fourthreethree_onezeronigtheight "}  />
    </CSSTransition >
                    </div>
               
    </CSSTransition >
                    </div>
               
    </CSSTransition >
                    </div>
               
    </CSSTransition >
<CSSTransition 
        in={_in}
        appear={true} 
        timeout={100}
        classNames={transaction['framethreefoursix']?.animationClass || {}}
    >
    
                    <div id="id_fourthreefour_oneonesixsix" className={` frame framethreefoursix ${ props.onClick ? 'cursor' : '' } ${ transaction['framethreefoursix']?.type ? transaction['framethreefoursix']?.type.toLowerCase() : '' }`} style={ { ...{}, ...props.FramethreefoursixStyle , transitionDuration: transaction['framethreefoursix']?.duration, transitionTimingFunction: transaction['framethreefoursix']?.timingFunction } } onClick={ props.FramethreefoursixonClick } onMouseEnter={ props.FramethreefoursixonMouseEnter } onMouseOver={ props.FramethreefoursixonMouseOver } onKeyPress={ props.FramethreefoursixonKeyPress } onDrag={ props.FramethreefoursixonDrag } onMouseLeave={ props.FramethreefoursixonMouseLeave } onMouseUp={ props.FramethreefoursixonMouseUp } onMouseDown={ props.FramethreefoursixonMouseDown } onKeyDown={ props.FramethreefoursixonKeyDown } onChange={ props.FramethreefoursixonChange } ondelay={ props.Framethreefoursixondelay }>
                                                <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['displaytwoo']?.animationClass || {}}>
                                                  <Displaytwoo { ...{ ...props, style:false } } DisplaytwooText0={ props.DisplaytwooText0 || "Visita nuestra Comunidad" } cssClass={"C_sixtwoosix_onesixnigthnigth "}  />
    </CSSTransition >
<CSSTransition 
        in={_in}
        appear={true} 
        timeout={100}
        classNames={transaction['framethreefourfive']?.animationClass || {}}
    >
    <Framethreethreetwoo { ...{ ...props, style:false } }   HoneText0={ props.HoneText6 || " ¿Tienes preguntas? ¡Nuestra comunidad mundial de expertos te puede ayudar!"} cssClass={"C_fourthreefour_oneonefivefive "}  />
    </CSSTransition >
<CSSTransition 
        in={_in}
        appear={true} 
        timeout={100}
        classNames={transaction['buttongreen']?.animationClass || {}}
    >
    <ButtonGreen { ...{ ...props, style:false } }  variant={'Property 1=Default'} PrincipalButtonText0={ props.PrincipalButtonText0 || " Ir a la Comunidad"} cssClass={"C_fourthreeeight_onethreezerosix "}  />
    </CSSTransition >
                    </div>
               
    </CSSTransition >
                    </div>
               
    </CSSTransition >
                    </div>
               
    </CSSTransition >
                    </div>
               
    </CSSTransition >
                    </div>
               
    </CSSTransition >
            
            </>
        }
        </div>
    
</CSSTransition>
            </Contexts.HelpLocalContext.Provider>
        
    ) 
}

Help.propTypes = {
    style: PropTypes.any,
DisplayfiveText0: PropTypes.any,
DisplayoneText0: PropTypes.any,
HoneText0: PropTypes.any,
DisplayfiveText1: PropTypes.any,
Ayudaconlospagos1: PropTypes.any,
Framethreetwooone1: PropTypes.any,
Framethreetwoooneundefined: PropTypes.any,
IMAGE1: PropTypes.any,
IMAGEundefined: PropTypes.any,
Ayudaconlospagos2: PropTypes.any,
Framethreetwooone2: PropTypes.any,
IMAGE2: PropTypes.any,
Ayudaconlospagos3: PropTypes.any,
Framethreetwooone3: PropTypes.any,
IMAGE3: PropTypes.any,
Ayudaconlospagos4: PropTypes.any,
Framethreetwooone4: PropTypes.any,
IMAGE4: PropTypes.any,
Ayudaconlospagos5: PropTypes.any,
Framethreetwooone5: PropTypes.any,
IMAGE5: PropTypes.any,
HoneText1: PropTypes.any,
HoneText3: PropTypes.any,
HoneText4: PropTypes.any,
HoneText5: PropTypes.any,
DisplaytwooText0: PropTypes.any,
HoneText6: PropTypes.any,
PrincipalButtonText0: PropTypes.any,
HelponClick: PropTypes.any,
HelponMouseEnter: PropTypes.any,
HelponMouseOver: PropTypes.any,
HelponKeyPress: PropTypes.any,
HelponDrag: PropTypes.any,
HelponMouseLeave: PropTypes.any,
HelponMouseUp: PropTypes.any,
HelponMouseDown: PropTypes.any,
HelponKeyDown: PropTypes.any,
HelponChange: PropTypes.any,
Helpondelay: PropTypes.any,
DashboardMasterContaineronClick: PropTypes.any,
DashboardMasterContaineronMouseEnter: PropTypes.any,
DashboardMasterContaineronMouseOver: PropTypes.any,
DashboardMasterContaineronKeyPress: PropTypes.any,
DashboardMasterContaineronDrag: PropTypes.any,
DashboardMasterContaineronMouseLeave: PropTypes.any,
DashboardMasterContaineronMouseUp: PropTypes.any,
DashboardMasterContaineronMouseDown: PropTypes.any,
DashboardMasterContaineronKeyDown: PropTypes.any,
DashboardMasterContaineronChange: PropTypes.any,
DashboardMasterContainerondelay: PropTypes.any,
FramethreeonethreeonClick: PropTypes.any,
FramethreeonethreeonMouseEnter: PropTypes.any,
FramethreeonethreeonMouseOver: PropTypes.any,
FramethreeonethreeonKeyPress: PropTypes.any,
FramethreeonethreeonDrag: PropTypes.any,
FramethreeonethreeonMouseLeave: PropTypes.any,
FramethreeonethreeonMouseUp: PropTypes.any,
FramethreeonethreeonMouseDown: PropTypes.any,
FramethreeonethreeonKeyDown: PropTypes.any,
FramethreeonethreeonChange: PropTypes.any,
Framethreeonethreeondelay: PropTypes.any,
ProjectsCardsDashboardonClick: PropTypes.any,
ProjectsCardsDashboardonMouseEnter: PropTypes.any,
ProjectsCardsDashboardonMouseOver: PropTypes.any,
ProjectsCardsDashboardonKeyPress: PropTypes.any,
ProjectsCardsDashboardonDrag: PropTypes.any,
ProjectsCardsDashboardonMouseLeave: PropTypes.any,
ProjectsCardsDashboardonMouseUp: PropTypes.any,
ProjectsCardsDashboardonMouseDown: PropTypes.any,
ProjectsCardsDashboardonKeyDown: PropTypes.any,
ProjectsCardsDashboardonChange: PropTypes.any,
ProjectsCardsDashboardondelay: PropTypes.any,
FrametwooseventhreeonClick: PropTypes.any,
FrametwooseventhreeonMouseEnter: PropTypes.any,
FrametwooseventhreeonMouseOver: PropTypes.any,
FrametwooseventhreeonKeyPress: PropTypes.any,
FrametwooseventhreeonDrag: PropTypes.any,
FrametwooseventhreeonMouseLeave: PropTypes.any,
FrametwooseventhreeonMouseUp: PropTypes.any,
FrametwooseventhreeonMouseDown: PropTypes.any,
FrametwooseventhreeonKeyDown: PropTypes.any,
FrametwooseventhreeonChange: PropTypes.any,
Frametwooseventhreeondelay: PropTypes.any,
FramethreethreezeroonClick: PropTypes.any,
FramethreethreezeroonMouseEnter: PropTypes.any,
FramethreethreezeroonMouseOver: PropTypes.any,
FramethreethreezeroonKeyPress: PropTypes.any,
FramethreethreezeroonDrag: PropTypes.any,
FramethreethreezeroonMouseLeave: PropTypes.any,
FramethreethreezeroonMouseUp: PropTypes.any,
FramethreethreezeroonMouseDown: PropTypes.any,
FramethreethreezeroonKeyDown: PropTypes.any,
FramethreethreezeroonChange: PropTypes.any,
Framethreethreezeroondelay: PropTypes.any,
FramethreetwoonigthonClick: PropTypes.any,
FramethreetwoonigthonMouseEnter: PropTypes.any,
FramethreetwoonigthonMouseOver: PropTypes.any,
FramethreetwoonigthonKeyPress: PropTypes.any,
FramethreetwoonigthonDrag: PropTypes.any,
FramethreetwoonigthonMouseLeave: PropTypes.any,
FramethreetwoonigthonMouseUp: PropTypes.any,
FramethreetwoonigthonMouseDown: PropTypes.any,
FramethreetwoonigthonKeyDown: PropTypes.any,
FramethreetwoonigthonChange: PropTypes.any,
Framethreetwoonigthondelay: PropTypes.any,
FramethreeoneeightonClick: PropTypes.any,
FramethreeoneeightonMouseEnter: PropTypes.any,
FramethreeoneeightonMouseOver: PropTypes.any,
FramethreeoneeightonKeyPress: PropTypes.any,
FramethreeoneeightonDrag: PropTypes.any,
FramethreeoneeightonMouseLeave: PropTypes.any,
FramethreeoneeightonMouseUp: PropTypes.any,
FramethreeoneeightonMouseDown: PropTypes.any,
FramethreeoneeightonKeyDown: PropTypes.any,
FramethreeoneeightonChange: PropTypes.any,
Framethreeoneeightondelay: PropTypes.any,
FramethreeonesevenonClick: PropTypes.any,
FramethreeonesevenonMouseEnter: PropTypes.any,
FramethreeonesevenonMouseOver: PropTypes.any,
FramethreeonesevenonKeyPress: PropTypes.any,
FramethreeonesevenonDrag: PropTypes.any,
FramethreeonesevenonMouseLeave: PropTypes.any,
FramethreeonesevenonMouseUp: PropTypes.any,
FramethreeonesevenonMouseDown: PropTypes.any,
FramethreeonesevenonKeyDown: PropTypes.any,
FramethreeonesevenonChange: PropTypes.any,
Framethreeonesevenondelay: PropTypes.any,
FramethreeonefiveonClick: PropTypes.any,
FramethreeonefiveonMouseEnter: PropTypes.any,
FramethreeonefiveonMouseOver: PropTypes.any,
FramethreeonefiveonKeyPress: PropTypes.any,
FramethreeonefiveonDrag: PropTypes.any,
FramethreeonefiveonMouseLeave: PropTypes.any,
FramethreeonefiveonMouseUp: PropTypes.any,
FramethreeonefiveonMouseDown: PropTypes.any,
FramethreeonefiveonKeyDown: PropTypes.any,
FramethreeonefiveonChange: PropTypes.any,
Framethreeonefiveondelay: PropTypes.any,
HoneTextonClick: PropTypes.any,
HoneTextonMouseEnter: PropTypes.any,
HoneTextonMouseOver: PropTypes.any,
HoneTextonKeyPress: PropTypes.any,
HoneTextonDrag: PropTypes.any,
HoneTextonMouseLeave: PropTypes.any,
HoneTextonMouseUp: PropTypes.any,
HoneTextonMouseDown: PropTypes.any,
HoneTextonKeyDown: PropTypes.any,
HoneTextonChange: PropTypes.any,
HoneTextondelay: PropTypes.any,
SearchBarDonClick: PropTypes.any,
SearchBarDonMouseEnter: PropTypes.any,
SearchBarDonMouseOver: PropTypes.any,
SearchBarDonKeyPress: PropTypes.any,
SearchBarDonDrag: PropTypes.any,
SearchBarDonMouseLeave: PropTypes.any,
SearchBarDonMouseUp: PropTypes.any,
SearchBarDonMouseDown: PropTypes.any,
SearchBarDonKeyDown: PropTypes.any,
SearchBarDonChange: PropTypes.any,
SearchBarDondelay: PropTypes.any,
FramethreeonenigthonClick: PropTypes.any,
FramethreeonenigthonMouseEnter: PropTypes.any,
FramethreeonenigthonMouseOver: PropTypes.any,
FramethreeonenigthonKeyPress: PropTypes.any,
FramethreeonenigthonDrag: PropTypes.any,
FramethreeonenigthonMouseLeave: PropTypes.any,
FramethreeonenigthonMouseUp: PropTypes.any,
FramethreeonenigthonMouseDown: PropTypes.any,
FramethreeonenigthonKeyDown: PropTypes.any,
FramethreeonenigthonChange: PropTypes.any,
Framethreeonenigthondelay: PropTypes.any,
VectoronClick: PropTypes.any,
VectoronMouseEnter: PropTypes.any,
VectoronMouseOver: PropTypes.any,
VectoronKeyPress: PropTypes.any,
VectoronDrag: PropTypes.any,
VectoronMouseLeave: PropTypes.any,
VectoronMouseUp: PropTypes.any,
VectoronMouseDown: PropTypes.any,
VectoronKeyDown: PropTypes.any,
VectoronChange: PropTypes.any,
Vectorondelay: PropTypes.any,
SearchBarTextDonClick: PropTypes.any,
SearchBarTextDonMouseEnter: PropTypes.any,
SearchBarTextDonMouseOver: PropTypes.any,
SearchBarTextDonKeyPress: PropTypes.any,
SearchBarTextDonDrag: PropTypes.any,
SearchBarTextDonMouseLeave: PropTypes.any,
SearchBarTextDonMouseUp: PropTypes.any,
SearchBarTextDonMouseDown: PropTypes.any,
SearchBarTextDonKeyDown: PropTypes.any,
SearchBarTextDonChange: PropTypes.any,
SearchBarTextDondelay: PropTypes.any,
FramethreetwooeightonClick: PropTypes.any,
FramethreetwooeightonMouseEnter: PropTypes.any,
FramethreetwooeightonMouseOver: PropTypes.any,
FramethreetwooeightonKeyPress: PropTypes.any,
FramethreetwooeightonDrag: PropTypes.any,
FramethreetwooeightonMouseLeave: PropTypes.any,
FramethreetwooeightonMouseUp: PropTypes.any,
FramethreetwooeightonMouseDown: PropTypes.any,
FramethreetwooeightonKeyDown: PropTypes.any,
FramethreetwooeightonChange: PropTypes.any,
Framethreetwooeightondelay: PropTypes.any,
FramethreefouroneonClick: PropTypes.any,
FramethreefouroneonMouseEnter: PropTypes.any,
FramethreefouroneonMouseOver: PropTypes.any,
FramethreefouroneonKeyPress: PropTypes.any,
FramethreefouroneonDrag: PropTypes.any,
FramethreefouroneonMouseLeave: PropTypes.any,
FramethreefouroneonMouseUp: PropTypes.any,
FramethreefouroneonMouseDown: PropTypes.any,
FramethreefouroneonKeyDown: PropTypes.any,
FramethreefouroneonChange: PropTypes.any,
Framethreefouroneondelay: PropTypes.any,
FramethreefourtwooonClick: PropTypes.any,
FramethreefourtwooonMouseEnter: PropTypes.any,
FramethreefourtwooonMouseOver: PropTypes.any,
FramethreefourtwooonKeyPress: PropTypes.any,
FramethreefourtwooonDrag: PropTypes.any,
FramethreefourtwooonMouseLeave: PropTypes.any,
FramethreefourtwooonMouseUp: PropTypes.any,
FramethreefourtwooonMouseDown: PropTypes.any,
FramethreefourtwooonKeyDown: PropTypes.any,
FramethreefourtwooonChange: PropTypes.any,
Framethreefourtwooondelay: PropTypes.any,
FramethreethreenigthonClick: PropTypes.any,
FramethreethreenigthonMouseEnter: PropTypes.any,
FramethreethreenigthonMouseOver: PropTypes.any,
FramethreethreenigthonKeyPress: PropTypes.any,
FramethreethreenigthonDrag: PropTypes.any,
FramethreethreenigthonMouseLeave: PropTypes.any,
FramethreethreenigthonMouseUp: PropTypes.any,
FramethreethreenigthonMouseDown: PropTypes.any,
FramethreethreenigthonKeyDown: PropTypes.any,
FramethreethreenigthonChange: PropTypes.any,
Framethreethreenigthondelay: PropTypes.any,
FramethreefoursixonClick: PropTypes.any,
FramethreefoursixonMouseEnter: PropTypes.any,
FramethreefoursixonMouseOver: PropTypes.any,
FramethreefoursixonKeyPress: PropTypes.any,
FramethreefoursixonDrag: PropTypes.any,
FramethreefoursixonMouseLeave: PropTypes.any,
FramethreefoursixonMouseUp: PropTypes.any,
FramethreefoursixonMouseDown: PropTypes.any,
FramethreefoursixonKeyDown: PropTypes.any,
FramethreefoursixonChange: PropTypes.any,
Framethreefoursixondelay: PropTypes.any
}
export default Help;