import React, { Suspense } from 'react';
import PropTypes from 'prop-types';
import * as Contexts  from 'Contexts';
import { OverlaySwapContext } from 'Components/OverlaySwapProvider';
import CorebooksAnimation from 'Components/CorebooksAnimation';
import CorebooksAnimationSingle from 'Components/CorebooksAnimationSingle';

import * as UtilsScripts  from 'utils/utils';
import { Route, useHistory, useParams } from "react-router-dom";
import SingletoneNavigation from 'Class/SingletoneNavigation';

import LoadingComponent from "Components/Loading";


import { Transition } from "react-transition-group";
import { CSSTransition } from "react-transition-group";
import HFive from 'Components/HFive'
import { useAppContext, useSessionContext } from 'context/AppContext';
import './MenuLibrary.css'





const MenuLibrary = (props)=>{
    const [state,dispatch] = useAppContext();
    const [, setNameSelectedComponent] = React.useContext(OverlaySwapContext);
    
    const nodeRefpropertyonedefault = React.useRef(null);
const nodeRefpropertyonevarianttwoo = React.useRef(null);
    const history = useHistory();
    let RouterContext = useParams();
    
    
    
    
    
    
    const [globalStateForm, dispatchForm] = React.useState({});
    const [ variant, setvariant] = React.useState(props.variant || 'Property 1=Default')
    const [ _in, setIn ] = React.useState(false)
    const [ transaction, setTransaction ] = React.useState({}) 
    const [ animation, setAnimation ] = React.useState('')

    
    
    
    
    
    
    React.useEffect(()=>{
        
        
        
    },[]);
    
    
    React.useEffect(()=>{
        
        setIn(false);
        setTimeout(()=>setIn(true))
           
      },[variant]);
    const switchVariant = (value)=>{
        switch (value) {
            case 'Property 1=Default':{
            return (
            <>

  <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['propertyonedefault']?.animationClass || {}}>

    <div id="id_onesixzero_fourtwoofourfour" ref={nodeRefpropertyonedefault} className={` ${ props.onClick ? 'cursor' : '' } menulibrarypropertyonedefault C_onesixzero_fourtwoofourfour ${ props.cssClass } ${ transaction['propertyonedefault']?.type ? transaction['propertyonedefault']?.type.toLowerCase() : '' }`} style={ { ...{ ...{}, transitionDuration: transaction['propertyonedefault']?.duration, transitionTimingFunction: transaction['propertyonedefault']?.timingFunction }, ...props.style }} onClick={ props.MenuLibraryonClick } onMouseEnter={ props.MenuLibraryonMouseEnter } onMouseOver={ props.MenuLibraryonMouseOver } onKeyPress={ props.MenuLibraryonKeyPress } onDrag={ props.MenuLibraryonDrag } onMouseLeave={ props.MenuLibraryonMouseLeave } onMouseUp={ props.MenuLibraryonMouseUp } onMouseDown={ props.MenuLibraryonMouseDown } onKeyDown={ props.MenuLibraryonKeyDown } onChange={ props.MenuLibraryonChange } ondelay={ props.MenuLibraryondelay }>
      {
      props.children ?
      props.children :
      <>
        <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['menuhomeicon']?.animationClass || {}}>
          <img id="id_onesixzero_fourtwoofourfive" className={` rectangle menulibrarymenuhomeicon ${ props.onClick ? 'cursor' : '' } ${ transaction['menuhomeicon']?.type ? transaction['menuhomeicon']?.type.toLowerCase() : '' }`} style={{ ...{},  ...props.MenuHomeIconStyle , transitionDuration: transaction['menuhomeicon']?.duration, transitionTimingFunction: transaction['menuhomeicon']?.timingFunction }} onClick={ props.MenuHomeIcononClick } onMouseEnter={ props.MenuHomeIcononMouseEnter } onMouseOver={ props.MenuHomeIcononMouseOver } onKeyPress={ props.MenuHomeIcononKeyPress } onDrag={ props.MenuHomeIcononDrag } onMouseLeave={ props.MenuHomeIcononMouseLeave } onMouseUp={ props.MenuHomeIcononMouseUp } onMouseDown={ props.MenuHomeIcononMouseDown } onKeyDown={ props.MenuHomeIcononKeyDown } onChange={ props.MenuHomeIcononChange } ondelay={ props.MenuHomeIconondelay } src={props.MenuHomeIcon0 || "https://cdn.uncodie.com/tACQBrKCV97e0EqPE1XNLJ/e83cf65d7b7ebf95dd515c7960ce9ab5007a8e44.png" } />
        </CSSTransition>
        <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['hfive']?.animationClass || {}}>
          <HFive { ...{ ...props, style:false } } HfiveText0={ props.HfiveText0 || "Tu biblioteca" } HfiveText0={ props.HfiveText0 || "Tu biblioteca" } HfiveTextStyle={{"fontSize":"0.875rem"}} cssClass={"C_onesixzero_fourtwoofoursix "}  />
    </CSSTransition >
            
            </>
        }
        </div>
    
    </CSSTransition >
                    
                </>
                
            )
        }
case 'Property 1=Variant2':{
            return (
            <>

  <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['propertyonevarianttwoo']?.animationClass || {}}>

    <div id="id_onesixzero_fourtwoofourseven" ref={nodeRefpropertyonevarianttwoo} className={` ${ props.onClick ? 'cursor' : '' } menulibrarypropertyonevarianttwoo C_onesixzero_fourtwoofourseven ${ props.cssClass } ${ transaction['propertyonevarianttwoo']?.type ? transaction['propertyonevarianttwoo']?.type.toLowerCase() : '' }`} style={ { ...{ ...{}, transitionDuration: transaction['propertyonevarianttwoo']?.duration, transitionTimingFunction: transaction['propertyonevarianttwoo']?.timingFunction }, ...props.style }} onClick={ props.MenuLibraryonClick } onMouseEnter={ props.MenuLibraryonMouseEnter } onMouseOver={ props.MenuLibraryonMouseOver } onKeyPress={ props.MenuLibraryonKeyPress } onDrag={ props.MenuLibraryonDrag } onMouseLeave={ props.MenuLibraryonMouseLeave } onMouseUp={ props.MenuLibraryonMouseUp } onMouseDown={ props.MenuLibraryonMouseDown } onKeyDown={ props.MenuLibraryonKeyDown } onChange={ props.MenuLibraryonChange } ondelay={ props.MenuLibraryondelay }>
      {
      props.children ?
      props.children :
      <>
        <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['menuhomeicon']?.animationClass || {}}>
          <img id="id_onesixzero_fourtwoofoureight" className={` rectangle menulibrarymenuhomeicon ${ props.onClick ? 'cursor' : '' } ${ transaction['menuhomeicon']?.type ? transaction['menuhomeicon']?.type.toLowerCase() : '' }`} style={{ ...{},  ...props.MenuHomeIconStyle , transitionDuration: transaction['menuhomeicon']?.duration, transitionTimingFunction: transaction['menuhomeicon']?.timingFunction }} onClick={ props.MenuHomeIcononClick } onMouseEnter={ props.MenuHomeIcononMouseEnter } onMouseOver={ props.MenuHomeIcononMouseOver } onKeyPress={ props.MenuHomeIcononKeyPress } onDrag={ props.MenuHomeIcononDrag } onMouseLeave={ props.MenuHomeIcononMouseLeave } onMouseUp={ props.MenuHomeIcononMouseUp } onMouseDown={ props.MenuHomeIcononMouseDown } onKeyDown={ props.MenuHomeIcononKeyDown } onChange={ props.MenuHomeIcononChange } ondelay={ props.MenuHomeIconondelay } src={props.MenuHomeIcon0 || "https://cdn.uncodie.com/tACQBrKCV97e0EqPE1XNLJ/5fb752e354bd2bb6e5ced993d71651c81a72ebc2.png" } />
        </CSSTransition>
        <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['hfive']?.animationClass || {}}>
          <HFive { ...{ ...props, style:false } } HfiveText0={ props.HfiveText0 || "Tu biblioteca" } HfiveTextStyle={{"fontSize":"0.875rem"}} cssClass={"C_onesixzero_fourtwoofournigth "}  />
    </CSSTransition >
            
            </>
        }
        </div>
    
    </CSSTransition >
                    
                </>
                
            )
        }
        }
    }

    return (
        <>
            {switchVariant(variant)}
        </>
    )
    
}

MenuLibrary.propTypes = {
    style: PropTypes.any,
variant: PropTypes.any,
MenuHomeIcon0: PropTypes.any,
HfiveText0: PropTypes.any,
MenuLibraryonClick: PropTypes.any,
MenuLibraryonMouseEnter: PropTypes.any,
MenuLibraryonMouseOver: PropTypes.any,
MenuLibraryonKeyPress: PropTypes.any,
MenuLibraryonDrag: PropTypes.any,
MenuLibraryonMouseLeave: PropTypes.any,
MenuLibraryonMouseUp: PropTypes.any,
MenuLibraryonMouseDown: PropTypes.any,
MenuLibraryonKeyDown: PropTypes.any,
MenuLibraryonChange: PropTypes.any,
MenuLibraryondelay: PropTypes.any,
MenuHomeIcononClick: PropTypes.any,
MenuHomeIcononMouseEnter: PropTypes.any,
MenuHomeIcononMouseOver: PropTypes.any,
MenuHomeIcononKeyPress: PropTypes.any,
MenuHomeIcononDrag: PropTypes.any,
MenuHomeIcononMouseLeave: PropTypes.any,
MenuHomeIcononMouseUp: PropTypes.any,
MenuHomeIcononMouseDown: PropTypes.any,
MenuHomeIcononKeyDown: PropTypes.any,
MenuHomeIcononChange: PropTypes.any,
MenuHomeIconondelay: PropTypes.any
}
export default MenuLibrary;