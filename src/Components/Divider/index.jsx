import React, { Suspense } from 'react';
import PropTypes from 'prop-types';
import * as Contexts  from 'Contexts';
import { OverlaySwapContext } from 'Components/OverlaySwapProvider';
import CorebooksAnimation from 'Components/CorebooksAnimation';
import CorebooksAnimationSingle from 'Components/CorebooksAnimationSingle';

import * as UtilsScripts  from 'utils/utils';
import { Route, useHistory, useParams } from "react-router-dom";
import SingletoneNavigation from 'Class/SingletoneNavigation';

import LoadingComponent from "Components/Loading";


import { Transition } from "react-transition-group";
import { CSSTransition } from "react-transition-group";
import { useAppContext, useSessionContext } from 'context/AppContext';
import './Divider.css'





const Divider = (props)=>{
    const [state,dispatch] = useAppContext();
    const [, setNameSelectedComponent] = React.useContext(OverlaySwapContext);
    
    const nodeRef = React.useRef(null);
    const history = useHistory();
    let RouterContext = useParams();
    
    
    
    
    
    
    const [globalStateForm, dispatchForm] = React.useState({});
    
    const [ _in, setIn ] = React.useState(false)
    const [ transaction, setTransaction ] = React.useState({}) 
    const [ animation, setAnimation ] = React.useState('')

    
    
    
    
    
    
    React.useEffect(()=>{
        
        
        
    },[]);
    
    
    return (
        <>
  <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['divider']?.animationClass || {}}>

    <div id="id_fourthreeeight_sevenonezeroeight" ref={nodeRef} className={` ${ props.onClick ? 'cursor' : '' } divider ${ props.cssClass } ${ transaction['divider']?.type ? transaction['divider']?.type.toLowerCase() : '' }`} style={ { ...{ ...{}, transitionDuration: transaction['divider']?.duration, transitionTimingFunction: transaction['divider']?.timingFunction }, ...props.style }} onClick={ props.DivideronClick } onMouseEnter={ props.DivideronMouseEnter } onMouseOver={ props.DivideronMouseOver } onKeyPress={ props.DivideronKeyPress } onDrag={ props.DivideronDrag } onMouseLeave={ props.DivideronMouseLeave } onMouseUp={ props.DivideronMouseUp } onMouseDown={ props.DivideronMouseDown } onKeyDown={ props.DivideronKeyDown } onChange={ props.DivideronChange } ondelay={ props.Dividerondelay }>
      {
      props.children ?
      props.children :
      <>


      </>
      }
    </div>

  </CSSTransition>
</>
    ) 
}

Divider.propTypes = {
    style: PropTypes.any,
DivideronClick: PropTypes.any,
DivideronMouseEnter: PropTypes.any,
DivideronMouseOver: PropTypes.any,
DivideronKeyPress: PropTypes.any,
DivideronDrag: PropTypes.any,
DivideronMouseLeave: PropTypes.any,
DivideronMouseUp: PropTypes.any,
DivideronMouseDown: PropTypes.any,
DivideronKeyDown: PropTypes.any,
DivideronChange: PropTypes.any,
Dividerondelay: PropTypes.any
}
export default Divider;