import React, { Suspense } from 'react';
import PropTypes from 'prop-types';
import * as Contexts  from 'Contexts';
import { OverlaySwapContext } from 'Components/OverlaySwapProvider';
import CorebooksAnimation from 'Components/CorebooksAnimation';
import CorebooksAnimationSingle from 'Components/CorebooksAnimationSingle';

import * as UtilsScripts  from 'utils/utils';
import { Route, useHistory, useParams } from "react-router-dom";
import SingletoneNavigation from 'Class/SingletoneNavigation';

import LoadingComponent from "Components/Loading";


import { Transition } from "react-transition-group";
import { CSSTransition } from "react-transition-group";
import { useAppContext, useSessionContext } from 'context/AppContext';
import './Frame.css'





const Frame = (props)=>{
    const [state,dispatch] = useAppContext();
    const [, setNameSelectedComponent] = React.useContext(OverlaySwapContext);
    
    const nodeRef = React.useRef(null);
    const history = useHistory();
    let RouterContext = useParams();
    
    
    
    
    
    
    const [globalStateForm, dispatchForm] = React.useState({});
    
    const [ _in, setIn ] = React.useState(false)
    const [ transaction, setTransaction ] = React.useState({}) 
    const [ animation, setAnimation ] = React.useState('')

    
    
    
    
    
    
    React.useEffect(()=>{
        
        
        
    },[]);
    
    
    return (
        <>
  <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['frame']?.animationClass || {}}>

    <div id="id_fivetwoofour_onesixnigthseven" ref={nodeRef} className={` ${ props.onClick ? 'cursor' : '' } frame ${ props.cssClass } ${ transaction['frame']?.type ? transaction['frame']?.type.toLowerCase() : '' }`} style={ { ...{ ...{}, transitionDuration: transaction['frame']?.duration, transitionTimingFunction: transaction['frame']?.timingFunction }, ...props.style }} onClick={ props.FrameonClick } onMouseEnter={ props.FrameonMouseEnter } onMouseOver={ props.FrameonMouseOver } onKeyPress={ props.FrameonKeyPress } onDrag={ props.FrameonDrag } onMouseLeave={ props.FrameonMouseLeave } onMouseUp={ props.FrameonMouseUp } onMouseDown={ props.FrameonMouseDown } onKeyDown={ props.FrameonKeyDown } onChange={ props.FrameonChange } ondelay={ props.Frameondelay }>
      {
      props.children ?
      props.children :
      <>
        <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['vector']?.animationClass || {}}>
          <svg id="id_fivetwoofour_onesixnigtheight" className="vector  " style={{}} onClick={ props.VectoronClick } onMouseEnter={ props.VectoronMouseEnter } onMouseOver={ props.VectoronMouseOver } onKeyPress={ props.VectoronKeyPress } onDrag={ props.VectoronDrag } onMouseLeave={ props.VectoronMouseLeave } onMouseUp={ props.VectoronMouseUp } onMouseDown={ props.VectoronMouseDown } onKeyDown={ props.VectoronKeyDown } onChange={ props.VectoronChange } ondelay={ props.Vectorondelay } width="22" height="20">
            <path d="M0 2C0 1.46957 0.210714 0.960859 0.585786 0.585786C0.960859 0.210714 1.46957 4.44089e-16 2 0L7.155 0C7.6816 1.33777e-05 8.19891 0.138638 8.65496 0.401943C9.111 0.665247 9.4897 1.04395 9.753 1.5L10.619 3L20 3C20.5304 3 21.0391 3.21071 21.4142 3.58579C21.7893 3.96086 22 4.46957 22 5L22 18C22 18.5304 21.7893 19.0391 21.4142 19.4142C21.0391 19.7893 20.5304 20 20 20L2 20C1.46957 20 0.960859 19.7893 0.585786 19.4142C0.210714 19.0391 2.22045e-16 18.5304 0 18L0 2ZM7.155 2L2 2L2 18L20 18L20 5L9.464 5L8.021 2.5C7.93323 2.34798 7.807 2.22175 7.65499 2.13398C7.50297 2.04621 7.33053 2 7.155 2Z" />
          </svg>
        </CSSTransition>

      </>
      }
    </div>

  </CSSTransition>
</>
    ) 
}

Frame.propTypes = {
    style: PropTypes.any,
FrameonClick: PropTypes.any,
FrameonMouseEnter: PropTypes.any,
FrameonMouseOver: PropTypes.any,
FrameonKeyPress: PropTypes.any,
FrameonDrag: PropTypes.any,
FrameonMouseLeave: PropTypes.any,
FrameonMouseUp: PropTypes.any,
FrameonMouseDown: PropTypes.any,
FrameonKeyDown: PropTypes.any,
FrameonChange: PropTypes.any,
Frameondelay: PropTypes.any,
VectoronClick: PropTypes.any,
VectoronMouseEnter: PropTypes.any,
VectoronMouseOver: PropTypes.any,
VectoronKeyPress: PropTypes.any,
VectoronDrag: PropTypes.any,
VectoronMouseLeave: PropTypes.any,
VectoronMouseUp: PropTypes.any,
VectoronMouseDown: PropTypes.any,
VectoronKeyDown: PropTypes.any,
VectoronChange: PropTypes.any,
Vectorondelay: PropTypes.any
}
export default Frame;