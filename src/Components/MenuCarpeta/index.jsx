import React, { Suspense } from 'react';
import PropTypes from 'prop-types';
import * as Contexts  from 'Contexts';
import { OverlaySwapContext } from 'Components/OverlaySwapProvider';
import CorebooksAnimation from 'Components/CorebooksAnimation';
import CorebooksAnimationSingle from 'Components/CorebooksAnimationSingle';

import * as UtilsScripts  from 'utils/utils';
import { Route, useHistory, useParams } from "react-router-dom";
import SingletoneNavigation from 'Class/SingletoneNavigation';

import LoadingComponent from "Components/Loading";


import { Transition } from "react-transition-group";
import { CSSTransition } from "react-transition-group";
import { useAppContext, useSessionContext } from 'context/AppContext';
import './MenuCarpeta.css'





const MenuCarpeta = (props)=>{
    const [state,dispatch] = useAppContext();
    const [, setNameSelectedComponent] = React.useContext(OverlaySwapContext);
    
    const nodeRefpropertyonedefault = React.useRef(null);
const nodeRefpropertyonevarianttwoo = React.useRef(null);
    const history = useHistory();
    let RouterContext = useParams();
    
    
    
    
    
    
    const [globalStateForm, dispatchForm] = React.useState({});
    const [ variant, setvariant] = React.useState(props.variant || 'Property 1=Default')
    const [ _in, setIn ] = React.useState(false)
    const [ transaction, setTransaction ] = React.useState({}) 
    const [ animation, setAnimation ] = React.useState('')

    
    
    
    
    
    
    React.useEffect(()=>{
        
        
        
    },[]);
    
    
    React.useEffect(()=>{
        
        setIn(false);
        setTimeout(()=>setIn(true))
           
      },[variant]);
    const switchVariant = (value)=>{
        switch (value) {
            case 'Property 1=Default':{
            return (
            <>

  <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['propertyonedefault']?.animationClass || {}}>

    <div id="id_fourthreeeight_sixthreeonetwoo" ref={nodeRefpropertyonedefault} className={` cursor ${ props.onClick ? 'cursor' : '' } menucarpetapropertyonedefault C_fourthreeeight_sixthreeonetwoo ${ props.cssClass } ${ transaction['propertyonedefault']?.type ? transaction['propertyonedefault']?.type.toLowerCase() : '' }`} style={ { ...{ ...{}, transitionDuration: transaction['propertyonedefault']?.duration, transitionTimingFunction: transaction['propertyonedefault']?.timingFunction }, ...props.style }} onClick={ props.MenuCarpetaonClick } onMouseEnter={ props.MenuCarpetaonMouseEnter } onMouseOver={ props.MenuCarpetaonMouseOver || function(e){ setTransaction({ }); setvariant('Property 1=Variant2'); }} onKeyPress={ props.MenuCarpetaonKeyPress } onDrag={ props.MenuCarpetaonDrag } onMouseLeave={ props.MenuCarpetaonMouseLeave } onMouseUp={ props.MenuCarpetaonMouseUp } onMouseDown={ props.MenuCarpetaonMouseDown } onKeyDown={ props.MenuCarpetaonKeyDown } onChange={ props.MenuCarpetaonChange } ondelay={ props.MenuCarpetaondelay }>
      {
      props.children ?
      props.children :
      <>
        <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['frame']?.animationClass || {}}>

          <div id="id_fourthreeeight_sixtwooeightnigth" className={` frame menucarpetaframe ${ props.onClick ? 'cursor' : '' } ${ transaction['frame']?.type ? transaction['frame']?.type.toLowerCase() : '' }`} style={ { ...{}, ...props.FrameStyle , transitionDuration: transaction['frame']?.duration, transitionTimingFunction: transaction['frame']?.timingFunction } } onClick={ props.FrameonClick } onMouseEnter={ props.FrameonMouseEnter } onMouseOver={ props.FrameonMouseOver } onKeyPress={ props.FrameonKeyPress } onDrag={ props.FrameonDrag } onMouseLeave={ props.FrameonMouseLeave } onMouseUp={ props.FrameonMouseUp } onMouseDown={ props.FrameonMouseDown } onKeyDown={ props.FrameonKeyDown } onChange={ props.FrameonChange } ondelay={ props.Frameondelay }>
            <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['rectangleonetwoo']?.animationClass || {}}>
              <img id="id_fourthreeeight_sixtwoonigthzero" className={` rectangle menucarpetarectangleonetwoo ${ props.onClick ? 'cursor' : '' } ${ transaction['rectangleonetwoo']?.type ? transaction['rectangleonetwoo']?.type.toLowerCase() : '' }`} style={{ ...{},  ...props.RectangleonetwooStyle , transitionDuration: transaction['rectangleonetwoo']?.duration, transitionTimingFunction: transaction['rectangleonetwoo']?.timingFunction }} onClick={ props.RectangleonetwooonClick } onMouseEnter={ props.RectangleonetwooonMouseEnter } onMouseOver={ props.RectangleonetwooonMouseOver } onKeyPress={ props.RectangleonetwooonKeyPress } onDrag={ props.RectangleonetwooonDrag } onMouseLeave={ props.RectangleonetwooonMouseLeave } onMouseUp={ props.RectangleonetwooonMouseUp } onMouseDown={ props.RectangleonetwooonMouseDown } onKeyDown={ props.RectangleonetwooonKeyDown } onChange={ props.RectangleonetwooonChange } ondelay={ props.Rectangleonetwooondelay } src={props.Rectangleonetwoo0 || "https://cdn.uncodie.com/tACQBrKCV97e0EqPE1XNLJ/164f576aa590c9eac96c7ca63f933b36b27b68ec.png" } />
            </CSSTransition>
          </div>

        </CSSTransition>
        <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['framethreenigtheight']?.animationClass || {}}>

          <div id="id_fourthreeeight_sixtwoonigthone" className={` frame menucarpetaframethreenigtheight ${ props.onClick ? 'cursor' : '' } ${ transaction['framethreenigtheight']?.type ? transaction['framethreenigtheight']?.type.toLowerCase() : '' }`} style={ { ...{}, ...props.FramethreenigtheightStyle , transitionDuration: transaction['framethreenigtheight']?.duration, transitionTimingFunction: transaction['framethreenigtheight']?.timingFunction } } onClick={ props.FramethreenigtheightonClick } onMouseEnter={ props.FramethreenigtheightonMouseEnter } onMouseOver={ props.FramethreenigtheightonMouseOver } onKeyPress={ props.FramethreenigtheightonKeyPress } onDrag={ props.FramethreenigtheightonDrag } onMouseLeave={ props.FramethreenigtheightonMouseLeave } onMouseUp={ props.FramethreenigtheightonMouseUp } onMouseDown={ props.FramethreenigtheightonMouseDown } onKeyDown={ props.FramethreenigtheightonKeyDown } onChange={ props.FramethreenigtheightonChange } ondelay={ props.Framethreenigtheightondelay }>
            <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['framethreenigthseven']?.animationClass || {}}>

              <div id="id_fourthreeeight_sixtwoonigthtwoo" className={` frame menucarpetaframethreenigthseven ${ props.onClick ? 'cursor' : '' } ${ transaction['framethreenigthseven']?.type ? transaction['framethreenigthseven']?.type.toLowerCase() : '' }`} style={ { ...{}, ...props.FramethreenigthsevenStyle , transitionDuration: transaction['framethreenigthseven']?.duration, transitionTimingFunction: transaction['framethreenigthseven']?.timingFunction } } onClick={ props.FramethreenigthsevenonClick } onMouseEnter={ props.FramethreenigthsevenonMouseEnter } onMouseOver={ props.FramethreenigthsevenonMouseOver } onKeyPress={ props.FramethreenigthsevenonKeyPress } onDrag={ props.FramethreenigthsevenonDrag } onMouseLeave={ props.FramethreenigthsevenonMouseLeave } onMouseUp={ props.FramethreenigthsevenonMouseUp } onMouseDown={ props.FramethreenigthsevenonMouseDown } onKeyDown={ props.FramethreenigthsevenonKeyDown } onChange={ props.FramethreenigthsevenonChange } ondelay={ props.Framethreenigthsevenondelay }>
                <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['framethreenigthfive']?.animationClass || {}}>

                  <div id="id_fourthreeeight_sixtwoonigththree" className={` frame menucarpetaframethreenigthfive ${ props.onClick ? 'cursor' : '' } ${ transaction['framethreenigthfive']?.type ? transaction['framethreenigthfive']?.type.toLowerCase() : '' }`} style={ { ...{}, ...props.FramethreenigthfiveStyle , transitionDuration: transaction['framethreenigthfive']?.duration, transitionTimingFunction: transaction['framethreenigthfive']?.timingFunction } } onClick={ props.FramethreenigthfiveonClick } onMouseEnter={ props.FramethreenigthfiveonMouseEnter } onMouseOver={ props.FramethreenigthfiveonMouseOver } onKeyPress={ props.FramethreenigthfiveonKeyPress } onDrag={ props.FramethreenigthfiveonDrag } onMouseLeave={ props.FramethreenigthfiveonMouseLeave } onMouseUp={ props.FramethreenigthfiveonMouseUp } onMouseDown={ props.FramethreenigthfiveonMouseDown } onKeyDown={ props.FramethreenigthfiveonKeyDown } onChange={ props.FramethreenigthfiveonChange } ondelay={ props.Framethreenigthfiveondelay }>
                    <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['miplaylistnone']?.animationClass || {}}>

                      <span id="id_fourthreeeight_sixtwoonigthfour"  className={` text menucarpetamiplaylistnone    ${ props.onClick ? 'cursor' : ''}  ${ transaction['miplaylistnone']?.type ? transaction['miplaylistnone']?.type.toLowerCase() : '' }`} style={{ ...{},  ...props.MiplaylistnoneStyle , transitionDuration: transaction['miplaylistnone']?.duration, transitionTimingFunction: transaction['miplaylistnone']?.timingFunction }} onClick={ props.MiplaylistnoneonClick } onMouseEnter={ props.MiplaylistnoneonMouseEnter } onMouseOver={ props.MiplaylistnoneonMouseOver } onKeyPress={ props.MiplaylistnoneonKeyPress } onDrag={ props.MiplaylistnoneonDrag } onMouseLeave={ props.MiplaylistnoneonMouseLeave } onMouseUp={ props.MiplaylistnoneonMouseUp } onMouseDown={ props.MiplaylistnoneonMouseDown } onKeyDown={ props.MiplaylistnoneonKeyDown } onChange={ props.MiplaylistnoneonChange } ondelay={ props.Miplaylistnoneondelay } >{props.Miplaylistnone0 || `Mi playlist n.° 1`}</span>

                    </CSSTransition>
                  </div>

                </CSSTransition>
                <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['framethreenigthsix']?.animationClass || {}}>

                  <div id="id_fourthreeeight_sixtwoonigthfive" className={` frame menucarpetaframethreenigthsix ${ props.onClick ? 'cursor' : '' } ${ transaction['framethreenigthsix']?.type ? transaction['framethreenigthsix']?.type.toLowerCase() : '' }`} style={ { ...{}, ...props.FramethreenigthsixStyle , transitionDuration: transaction['framethreenigthsix']?.duration, transitionTimingFunction: transaction['framethreenigthsix']?.timingFunction } } onClick={ props.FramethreenigthsixonClick } onMouseEnter={ props.FramethreenigthsixonMouseEnter } onMouseOver={ props.FramethreenigthsixonMouseOver } onKeyPress={ props.FramethreenigthsixonKeyPress } onDrag={ props.FramethreenigthsixonDrag } onMouseLeave={ props.FramethreenigthsixonMouseLeave } onMouseUp={ props.FramethreenigthsixonMouseUp } onMouseDown={ props.FramethreenigthsixonMouseDown } onKeyDown={ props.FramethreenigthsixonKeyDown } onChange={ props.FramethreenigthsixonChange } ondelay={ props.Framethreenigthsixondelay }>
                    <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['playlistalejandrabarragn']?.animationClass || {}}>

                      <span id="id_fourthreeeight_sixtwoonigthsix"  className={` text menucarpetaplaylistalejandrabarragn    ${ props.onClick ? 'cursor' : ''}  ${ transaction['playlistalejandrabarragn']?.type ? transaction['playlistalejandrabarragn']?.type.toLowerCase() : '' }`} style={{ ...{},  ...props.PlaylistAlejandraBarragnStyle , transitionDuration: transaction['playlistalejandrabarragn']?.duration, transitionTimingFunction: transaction['playlistalejandrabarragn']?.timingFunction }} onClick={ props.PlaylistAlejandraBarragnonClick } onMouseEnter={ props.PlaylistAlejandraBarragnonMouseEnter } onMouseOver={ props.PlaylistAlejandraBarragnonMouseOver } onKeyPress={ props.PlaylistAlejandraBarragnonKeyPress } onDrag={ props.PlaylistAlejandraBarragnonDrag } onMouseLeave={ props.PlaylistAlejandraBarragnonMouseLeave } onMouseUp={ props.PlaylistAlejandraBarragnonMouseUp } onMouseDown={ props.PlaylistAlejandraBarragnonMouseDown } onKeyDown={ props.PlaylistAlejandraBarragnonKeyDown } onChange={ props.PlaylistAlejandraBarragnonChange } ondelay={ props.PlaylistAlejandraBarragnondelay } >{props.PlaylistAlejandraBarragn0 || `Playlist • Alejandra Barragán`}</span>

                    </CSSTransition>
                  </div>

                </CSSTransition>
              </div>

            </CSSTransition>
          </div>

        </CSSTransition>

      </>
      }
    </div>

  </CSSTransition>

</>
            )
        }
case 'Property 1=Variant2':{
            return (
            <>

  <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['propertyonevarianttwoo']?.animationClass || {}}>

    <div id="id_fourthreeeight_sixthreeonefour" ref={nodeRefpropertyonevarianttwoo} className={` cursor ${ props.onClick ? 'cursor' : '' } menucarpetapropertyonevarianttwoo C_fourthreeeight_sixthreeonefour ${ props.cssClass } ${ transaction['propertyonevarianttwoo']?.type ? transaction['propertyonevarianttwoo']?.type.toLowerCase() : '' }`} style={ { ...{ ...{}, transitionDuration: transaction['propertyonevarianttwoo']?.duration, transitionTimingFunction: transaction['propertyonevarianttwoo']?.timingFunction }, ...props.style }} onClick={ props.MenuCarpetaonClick } onMouseEnter={ props.MenuCarpetaonMouseEnter } onMouseOver={ props.MenuCarpetaonMouseOver } onKeyPress={ props.MenuCarpetaonKeyPress } onDrag={ props.MenuCarpetaonDrag } onMouseLeave={ props.MenuCarpetaonMouseLeave || function(e){ setTransaction({ }); setvariant('Property 1=Default'); }} onMouseUp={ props.MenuCarpetaonMouseUp } onMouseDown={ props.MenuCarpetaonMouseDown } onKeyDown={ props.MenuCarpetaonKeyDown } onChange={ props.MenuCarpetaonChange } ondelay={ props.MenuCarpetaondelay }>
      {
      props.children ?
      props.children :
      <>
        <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['frame']?.animationClass || {}}>

          <div id="id_fourthreeeight_sixthreeonefive" className={` frame menucarpetaframe ${ props.onClick ? 'cursor' : '' } ${ transaction['frame']?.type ? transaction['frame']?.type.toLowerCase() : '' }`} style={ { ...{}, ...props.FrameStyle , transitionDuration: transaction['frame']?.duration, transitionTimingFunction: transaction['frame']?.timingFunction } } onClick={ props.FrameonClick } onMouseEnter={ props.FrameonMouseEnter } onMouseOver={ props.FrameonMouseOver } onKeyPress={ props.FrameonKeyPress } onDrag={ props.FrameonDrag } onMouseLeave={ props.FrameonMouseLeave } onMouseUp={ props.FrameonMouseUp } onMouseDown={ props.FrameonMouseDown } onKeyDown={ props.FrameonKeyDown } onChange={ props.FrameonChange } ondelay={ props.Frameondelay }>
            <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['rectangleonetwoo']?.animationClass || {}}>
              <img id="id_fourthreeeight_sixthreeonesix" className={` rectangle menucarpetarectangleonetwoo ${ props.onClick ? 'cursor' : '' } ${ transaction['rectangleonetwoo']?.type ? transaction['rectangleonetwoo']?.type.toLowerCase() : '' }`} style={{ ...{},  ...props.RectangleonetwooStyle , transitionDuration: transaction['rectangleonetwoo']?.duration, transitionTimingFunction: transaction['rectangleonetwoo']?.timingFunction }} onClick={ props.RectangleonetwooonClick } onMouseEnter={ props.RectangleonetwooonMouseEnter } onMouseOver={ props.RectangleonetwooonMouseOver } onKeyPress={ props.RectangleonetwooonKeyPress } onDrag={ props.RectangleonetwooonDrag } onMouseLeave={ props.RectangleonetwooonMouseLeave } onMouseUp={ props.RectangleonetwooonMouseUp } onMouseDown={ props.RectangleonetwooonMouseDown } onKeyDown={ props.RectangleonetwooonKeyDown } onChange={ props.RectangleonetwooonChange } ondelay={ props.Rectangleonetwooondelay } src={props.Rectangleonetwoo0 || "https://cdn.uncodie.com/tACQBrKCV97e0EqPE1XNLJ/164f576aa590c9eac96c7ca63f933b36b27b68ec.png" } />
            </CSSTransition>
          </div>

        </CSSTransition>
        <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['framethreenigtheight']?.animationClass || {}}>

          <div id="id_fourthreeeight_sixthreeoneseven" className={` frame menucarpetaframethreenigtheight ${ props.onClick ? 'cursor' : '' } ${ transaction['framethreenigtheight']?.type ? transaction['framethreenigtheight']?.type.toLowerCase() : '' }`} style={ { ...{}, ...props.FramethreenigtheightStyle , transitionDuration: transaction['framethreenigtheight']?.duration, transitionTimingFunction: transaction['framethreenigtheight']?.timingFunction } } onClick={ props.FramethreenigtheightonClick } onMouseEnter={ props.FramethreenigtheightonMouseEnter } onMouseOver={ props.FramethreenigtheightonMouseOver } onKeyPress={ props.FramethreenigtheightonKeyPress } onDrag={ props.FramethreenigtheightonDrag } onMouseLeave={ props.FramethreenigtheightonMouseLeave } onMouseUp={ props.FramethreenigtheightonMouseUp } onMouseDown={ props.FramethreenigtheightonMouseDown } onKeyDown={ props.FramethreenigtheightonKeyDown } onChange={ props.FramethreenigtheightonChange } ondelay={ props.Framethreenigtheightondelay }>
            <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['framethreenigthseven']?.animationClass || {}}>

              <div id="id_fourthreeeight_sixthreeoneeight" className={` frame menucarpetaframethreenigthseven ${ props.onClick ? 'cursor' : '' } ${ transaction['framethreenigthseven']?.type ? transaction['framethreenigthseven']?.type.toLowerCase() : '' }`} style={ { ...{}, ...props.FramethreenigthsevenStyle , transitionDuration: transaction['framethreenigthseven']?.duration, transitionTimingFunction: transaction['framethreenigthseven']?.timingFunction } } onClick={ props.FramethreenigthsevenonClick } onMouseEnter={ props.FramethreenigthsevenonMouseEnter } onMouseOver={ props.FramethreenigthsevenonMouseOver } onKeyPress={ props.FramethreenigthsevenonKeyPress } onDrag={ props.FramethreenigthsevenonDrag } onMouseLeave={ props.FramethreenigthsevenonMouseLeave } onMouseUp={ props.FramethreenigthsevenonMouseUp } onMouseDown={ props.FramethreenigthsevenonMouseDown } onKeyDown={ props.FramethreenigthsevenonKeyDown } onChange={ props.FramethreenigthsevenonChange } ondelay={ props.Framethreenigthsevenondelay }>
                <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['framethreenigthfive']?.animationClass || {}}>

                  <div id="id_fourthreeeight_sixthreeonenigth" className={` frame menucarpetaframethreenigthfive ${ props.onClick ? 'cursor' : '' } ${ transaction['framethreenigthfive']?.type ? transaction['framethreenigthfive']?.type.toLowerCase() : '' }`} style={ { ...{}, ...props.FramethreenigthfiveStyle , transitionDuration: transaction['framethreenigthfive']?.duration, transitionTimingFunction: transaction['framethreenigthfive']?.timingFunction } } onClick={ props.FramethreenigthfiveonClick } onMouseEnter={ props.FramethreenigthfiveonMouseEnter } onMouseOver={ props.FramethreenigthfiveonMouseOver } onKeyPress={ props.FramethreenigthfiveonKeyPress } onDrag={ props.FramethreenigthfiveonDrag } onMouseLeave={ props.FramethreenigthfiveonMouseLeave } onMouseUp={ props.FramethreenigthfiveonMouseUp } onMouseDown={ props.FramethreenigthfiveonMouseDown } onKeyDown={ props.FramethreenigthfiveonKeyDown } onChange={ props.FramethreenigthfiveonChange } ondelay={ props.Framethreenigthfiveondelay }>
                    <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['miplaylistnone']?.animationClass || {}}>

                      <span id="id_fourthreeeight_sixthreetwoozero"  className={` text menucarpetamiplaylistnone    ${ props.onClick ? 'cursor' : ''}  ${ transaction['miplaylistnone']?.type ? transaction['miplaylistnone']?.type.toLowerCase() : '' }`} style={{ ...{},  ...props.MiplaylistnoneStyle , transitionDuration: transaction['miplaylistnone']?.duration, transitionTimingFunction: transaction['miplaylistnone']?.timingFunction }} onClick={ props.MiplaylistnoneonClick } onMouseEnter={ props.MiplaylistnoneonMouseEnter } onMouseOver={ props.MiplaylistnoneonMouseOver } onKeyPress={ props.MiplaylistnoneonKeyPress } onDrag={ props.MiplaylistnoneonDrag } onMouseLeave={ props.MiplaylistnoneonMouseLeave } onMouseUp={ props.MiplaylistnoneonMouseUp } onMouseDown={ props.MiplaylistnoneonMouseDown } onKeyDown={ props.MiplaylistnoneonKeyDown } onChange={ props.MiplaylistnoneonChange } ondelay={ props.Miplaylistnoneondelay } >{props.Miplaylistnone0 || `Mi playlist n.° 1`}</span>

                    </CSSTransition>
                  </div>

                </CSSTransition>
                <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['framethreenigthsix']?.animationClass || {}}>

                  <div id="id_fourthreeeight_sixthreetwooone" className={` frame menucarpetaframethreenigthsix ${ props.onClick ? 'cursor' : '' } ${ transaction['framethreenigthsix']?.type ? transaction['framethreenigthsix']?.type.toLowerCase() : '' }`} style={ { ...{}, ...props.FramethreenigthsixStyle , transitionDuration: transaction['framethreenigthsix']?.duration, transitionTimingFunction: transaction['framethreenigthsix']?.timingFunction } } onClick={ props.FramethreenigthsixonClick } onMouseEnter={ props.FramethreenigthsixonMouseEnter } onMouseOver={ props.FramethreenigthsixonMouseOver } onKeyPress={ props.FramethreenigthsixonKeyPress } onDrag={ props.FramethreenigthsixonDrag } onMouseLeave={ props.FramethreenigthsixonMouseLeave } onMouseUp={ props.FramethreenigthsixonMouseUp } onMouseDown={ props.FramethreenigthsixonMouseDown } onKeyDown={ props.FramethreenigthsixonKeyDown } onChange={ props.FramethreenigthsixonChange } ondelay={ props.Framethreenigthsixondelay }>
                    <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['playlistalejandrabarragn']?.animationClass || {}}>

                      <span id="id_fourthreeeight_sixthreetwootwoo"  className={` text menucarpetaplaylistalejandrabarragn    ${ props.onClick ? 'cursor' : ''}  ${ transaction['playlistalejandrabarragn']?.type ? transaction['playlistalejandrabarragn']?.type.toLowerCase() : '' }`} style={{ ...{},  ...props.PlaylistAlejandraBarragnStyle , transitionDuration: transaction['playlistalejandrabarragn']?.duration, transitionTimingFunction: transaction['playlistalejandrabarragn']?.timingFunction }} onClick={ props.PlaylistAlejandraBarragnonClick } onMouseEnter={ props.PlaylistAlejandraBarragnonMouseEnter } onMouseOver={ props.PlaylistAlejandraBarragnonMouseOver } onKeyPress={ props.PlaylistAlejandraBarragnonKeyPress } onDrag={ props.PlaylistAlejandraBarragnonDrag } onMouseLeave={ props.PlaylistAlejandraBarragnonMouseLeave } onMouseUp={ props.PlaylistAlejandraBarragnonMouseUp } onMouseDown={ props.PlaylistAlejandraBarragnonMouseDown } onKeyDown={ props.PlaylistAlejandraBarragnonKeyDown } onChange={ props.PlaylistAlejandraBarragnonChange } ondelay={ props.PlaylistAlejandraBarragnondelay } >{props.PlaylistAlejandraBarragn0 || `Playlist • Alejandra Barragán`}</span>

                    </CSSTransition>
                  </div>

                </CSSTransition>
              </div>

            </CSSTransition>
          </div>

        </CSSTransition>

      </>
      }
    </div>

  </CSSTransition>

</>
            )
        }
        }
    }

    return (
        <>
            {switchVariant(variant)}
        </>
    )
    
}

MenuCarpeta.propTypes = {
    style: PropTypes.any,
variant: PropTypes.any,
Rectangleonetwoo0: PropTypes.any,
Miplaylistnone0: PropTypes.any,
PlaylistAlejandraBarragn0: PropTypes.any,
MenuCarpetaonClick: PropTypes.any,
MenuCarpetaonMouseEnter: PropTypes.any,
MenuCarpetaonMouseOver: PropTypes.any,
MenuCarpetaonKeyPress: PropTypes.any,
MenuCarpetaonDrag: PropTypes.any,
MenuCarpetaonMouseLeave: PropTypes.any,
MenuCarpetaonMouseUp: PropTypes.any,
MenuCarpetaonMouseDown: PropTypes.any,
MenuCarpetaonKeyDown: PropTypes.any,
MenuCarpetaonChange: PropTypes.any,
MenuCarpetaondelay: PropTypes.any,
FrameonClick: PropTypes.any,
FrameonMouseEnter: PropTypes.any,
FrameonMouseOver: PropTypes.any,
FrameonKeyPress: PropTypes.any,
FrameonDrag: PropTypes.any,
FrameonMouseLeave: PropTypes.any,
FrameonMouseUp: PropTypes.any,
FrameonMouseDown: PropTypes.any,
FrameonKeyDown: PropTypes.any,
FrameonChange: PropTypes.any,
Frameondelay: PropTypes.any,
RectangleonetwooonClick: PropTypes.any,
RectangleonetwooonMouseEnter: PropTypes.any,
RectangleonetwooonMouseOver: PropTypes.any,
RectangleonetwooonKeyPress: PropTypes.any,
RectangleonetwooonDrag: PropTypes.any,
RectangleonetwooonMouseLeave: PropTypes.any,
RectangleonetwooonMouseUp: PropTypes.any,
RectangleonetwooonMouseDown: PropTypes.any,
RectangleonetwooonKeyDown: PropTypes.any,
RectangleonetwooonChange: PropTypes.any,
Rectangleonetwooondelay: PropTypes.any,
FramethreenigtheightonClick: PropTypes.any,
FramethreenigtheightonMouseEnter: PropTypes.any,
FramethreenigtheightonMouseOver: PropTypes.any,
FramethreenigtheightonKeyPress: PropTypes.any,
FramethreenigtheightonDrag: PropTypes.any,
FramethreenigtheightonMouseLeave: PropTypes.any,
FramethreenigtheightonMouseUp: PropTypes.any,
FramethreenigtheightonMouseDown: PropTypes.any,
FramethreenigtheightonKeyDown: PropTypes.any,
FramethreenigtheightonChange: PropTypes.any,
Framethreenigtheightondelay: PropTypes.any,
FramethreenigthsevenonClick: PropTypes.any,
FramethreenigthsevenonMouseEnter: PropTypes.any,
FramethreenigthsevenonMouseOver: PropTypes.any,
FramethreenigthsevenonKeyPress: PropTypes.any,
FramethreenigthsevenonDrag: PropTypes.any,
FramethreenigthsevenonMouseLeave: PropTypes.any,
FramethreenigthsevenonMouseUp: PropTypes.any,
FramethreenigthsevenonMouseDown: PropTypes.any,
FramethreenigthsevenonKeyDown: PropTypes.any,
FramethreenigthsevenonChange: PropTypes.any,
Framethreenigthsevenondelay: PropTypes.any,
FramethreenigthfiveonClick: PropTypes.any,
FramethreenigthfiveonMouseEnter: PropTypes.any,
FramethreenigthfiveonMouseOver: PropTypes.any,
FramethreenigthfiveonKeyPress: PropTypes.any,
FramethreenigthfiveonDrag: PropTypes.any,
FramethreenigthfiveonMouseLeave: PropTypes.any,
FramethreenigthfiveonMouseUp: PropTypes.any,
FramethreenigthfiveonMouseDown: PropTypes.any,
FramethreenigthfiveonKeyDown: PropTypes.any,
FramethreenigthfiveonChange: PropTypes.any,
Framethreenigthfiveondelay: PropTypes.any,
MiplaylistnoneonClick: PropTypes.any,
MiplaylistnoneonMouseEnter: PropTypes.any,
MiplaylistnoneonMouseOver: PropTypes.any,
MiplaylistnoneonKeyPress: PropTypes.any,
MiplaylistnoneonDrag: PropTypes.any,
MiplaylistnoneonMouseLeave: PropTypes.any,
MiplaylistnoneonMouseUp: PropTypes.any,
MiplaylistnoneonMouseDown: PropTypes.any,
MiplaylistnoneonKeyDown: PropTypes.any,
MiplaylistnoneonChange: PropTypes.any,
Miplaylistnoneondelay: PropTypes.any,
FramethreenigthsixonClick: PropTypes.any,
FramethreenigthsixonMouseEnter: PropTypes.any,
FramethreenigthsixonMouseOver: PropTypes.any,
FramethreenigthsixonKeyPress: PropTypes.any,
FramethreenigthsixonDrag: PropTypes.any,
FramethreenigthsixonMouseLeave: PropTypes.any,
FramethreenigthsixonMouseUp: PropTypes.any,
FramethreenigthsixonMouseDown: PropTypes.any,
FramethreenigthsixonKeyDown: PropTypes.any,
FramethreenigthsixonChange: PropTypes.any,
Framethreenigthsixondelay: PropTypes.any,
PlaylistAlejandraBarragnonClick: PropTypes.any,
PlaylistAlejandraBarragnonMouseEnter: PropTypes.any,
PlaylistAlejandraBarragnonMouseOver: PropTypes.any,
PlaylistAlejandraBarragnonKeyPress: PropTypes.any,
PlaylistAlejandraBarragnonDrag: PropTypes.any,
PlaylistAlejandraBarragnonMouseLeave: PropTypes.any,
PlaylistAlejandraBarragnonMouseUp: PropTypes.any,
PlaylistAlejandraBarragnonMouseDown: PropTypes.any,
PlaylistAlejandraBarragnonKeyDown: PropTypes.any,
PlaylistAlejandraBarragnonChange: PropTypes.any,
PlaylistAlejandraBarragnondelay: PropTypes.any
}
export default MenuCarpeta;