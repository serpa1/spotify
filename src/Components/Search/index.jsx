import React, { Suspense } from 'react';
import PropTypes from 'prop-types';
import * as Contexts  from 'Contexts';
import { OverlaySwapContext } from 'Components/OverlaySwapProvider';
import CorebooksAnimation from 'Components/CorebooksAnimation';
import CorebooksAnimationSingle from 'Components/CorebooksAnimationSingle';

import * as UtilsScripts  from 'utils/utils';
import { Route, useHistory, useParams } from "react-router-dom";
import SingletoneNavigation from 'Class/SingletoneNavigation';

import LoadingComponent from "Components/Loading";


import { Transition } from "react-transition-group";
import { CSSTransition } from "react-transition-group";
import { useAppContext, useSessionContext } from 'context/AppContext';
import './Search.css'





const Search = (props)=>{
    const [state,dispatch] = useAppContext();
    const [, setNameSelectedComponent] = React.useContext(OverlaySwapContext);
    
    const nodeRef = React.useRef(null);
    const history = useHistory();
    let RouterContext = useParams();
    
    
    
    
    
    
    const [globalStateForm, dispatchForm] = React.useState({});
    
    const [ _in, setIn ] = React.useState(false)
    const [ transaction, setTransaction ] = React.useState({}) 
    const [ animation, setAnimation ] = React.useState('')

    
    
    
    
    
    
    React.useEffect(()=>{
        
        
        
    },[]);
    
    
    return (
        <>
  <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['search']?.animationClass || {}}>

    <div id="id_fourthreeeight_fourtwoozerotwoo" ref={nodeRef} className={` ${ props.onClick ? 'cursor' : '' } search ${ props.cssClass } ${ transaction['search']?.type ? transaction['search']?.type.toLowerCase() : '' }`} style={ { ...{ ...{}, transitionDuration: transaction['search']?.duration, transitionTimingFunction: transaction['search']?.timingFunction }, ...props.style }} onClick={ props.SearchonClick } onMouseEnter={ props.SearchonMouseEnter } onMouseOver={ props.SearchonMouseOver } onKeyPress={ props.SearchonKeyPress } onDrag={ props.SearchonDrag } onMouseLeave={ props.SearchonMouseLeave } onMouseUp={ props.SearchonMouseUp } onMouseDown={ props.SearchonMouseDown } onKeyDown={ props.SearchonKeyDown } onChange={ props.SearchonChange } ondelay={ props.Searchondelay }>
      {
      props.children ?
      props.children :
      <>
        <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['vector']?.animationClass || {}}>
          <svg id="id_fourthreeeight_fourtwoozerothree" className="vector  " style={{}} onClick={ props.VectoronClick } onMouseEnter={ props.VectoronMouseEnter } onMouseOver={ props.VectoronMouseOver } onKeyPress={ props.VectoronKeyPress } onDrag={ props.VectoronDrag } onMouseLeave={ props.VectoronMouseLeave } onMouseUp={ props.VectoronMouseUp } onMouseDown={ props.VectoronMouseDown } onKeyDown={ props.VectoronKeyDown } onChange={ props.VectoronChange } ondelay={ props.Vectorondelay } width="16" height="16">
            <path d="M6.95934 1.5641C6.24852 1.5641 5.54466 1.7056 4.88794 1.98052C4.23123 2.25545 3.63453 2.65841 3.1319 3.16641C2.62927 3.6744 2.23057 4.27748 1.95855 4.94121C1.68653 5.60493 1.54652 6.31631 1.54652 7.03473C1.54652 7.75314 1.68653 8.46452 1.95855 9.12824C2.23057 9.79197 2.62927 10.395 3.1319 10.903C3.63453 11.411 4.23123 11.814 4.88794 12.0889C5.54466 12.3639 6.24852 12.5054 6.95934 12.5054C8.39491 12.5054 9.77168 11.929 10.7868 10.903C11.8019 9.8771 12.3722 8.48563 12.3722 7.03473C12.3722 5.58382 11.8019 4.19235 10.7868 3.16641C9.77168 2.14047 8.39491 1.5641 6.95934 1.5641ZM7.90445e-08 7.03473C-0.000166698 5.91625 0.263581 4.81387 0.769358 3.81905C1.27514 2.82422 2.00836 1.96565 2.90818 1.31456C3.808 0.663482 4.84848 0.238667 5.94328 0.0753698C7.03807 -0.0879271 8.15561 0.0150031 9.20317 0.375619C10.2507 0.736236 11.1981 1.34414 11.9666 2.14889C12.7352 2.95363 13.3028 3.93202 13.6223 5.00286C13.9418 6.07369 14.0041 7.2061 13.8039 8.30612C13.6037 9.40613 13.1468 10.442 12.4711 11.3279L15.7539 14.6467C15.8299 14.7183 15.8908 14.8045 15.9331 14.9004C15.9753 14.9963 15.998 15.0998 15.9999 15.2047C16.0017 15.3096 15.9826 15.4139 15.9437 15.5112C15.9048 15.6085 15.8469 15.6969 15.7735 15.7711C15.7001 15.8453 15.6126 15.9038 15.5163 15.9431C15.4201 15.9824 15.3169 16.0017 15.2131 15.9999C15.1093 15.998 15.0069 15.9751 14.912 15.9323C14.8172 15.8896 14.7318 15.828 14.661 15.7513L11.3968 12.4533C10.3794 13.3044 9.14326 13.8454 7.83291 14.0129C6.52257 14.1805 5.19211 13.9678 3.99695 13.3995C2.8018 12.8313 1.79131 11.9311 1.08355 10.804C0.375782 9.67696 -3.59709e-05 8.36962 7.90445e-08 7.03473Z" />
          </svg>
        </CSSTransition>

      </>
      }
    </div>

  </CSSTransition>
</>
    ) 
}

Search.propTypes = {
    style: PropTypes.any,
SearchonClick: PropTypes.any,
SearchonMouseEnter: PropTypes.any,
SearchonMouseOver: PropTypes.any,
SearchonKeyPress: PropTypes.any,
SearchonDrag: PropTypes.any,
SearchonMouseLeave: PropTypes.any,
SearchonMouseUp: PropTypes.any,
SearchonMouseDown: PropTypes.any,
SearchonKeyDown: PropTypes.any,
SearchonChange: PropTypes.any,
Searchondelay: PropTypes.any,
VectoronClick: PropTypes.any,
VectoronMouseEnter: PropTypes.any,
VectoronMouseOver: PropTypes.any,
VectoronKeyPress: PropTypes.any,
VectoronDrag: PropTypes.any,
VectoronMouseLeave: PropTypes.any,
VectoronMouseUp: PropTypes.any,
VectoronMouseDown: PropTypes.any,
VectoronKeyDown: PropTypes.any,
VectoronChange: PropTypes.any,
Vectorondelay: PropTypes.any
}
export default Search;