import React, { Suspense } from 'react';
import PropTypes from 'prop-types';
import * as Contexts  from 'Contexts';
import { OverlaySwapContext } from 'Components/OverlaySwapProvider';
import CorebooksAnimation from 'Components/CorebooksAnimation';
import CorebooksAnimationSingle from 'Components/CorebooksAnimationSingle';

import * as UtilsScripts  from 'utils/utils';
import { Route, useHistory, useParams } from "react-router-dom";
import SingletoneNavigation from 'Class/SingletoneNavigation';

import LoadingComponent from "Components/Loading";


import { Transition } from "react-transition-group";
import { CSSTransition } from "react-transition-group";
import { useAppContext, useSessionContext } from 'context/AppContext';
import './TextLinkPropertyoneDefault.css'





const PropertyoneDefault = (props)=>{
    const [state,dispatch] = useAppContext();
    const [, setNameSelectedComponent] = React.useContext(OverlaySwapContext);
    
    const nodeRef = React.useRef(null);
    const history = useHistory();
    let RouterContext = useParams();
    
    
    
    
    
    
    const [globalStateForm, dispatchForm] = React.useState({});
    
    const [ _in, setIn ] = React.useState(false)
    const [ transaction, setTransaction ] = React.useState({}) 
    const [ animation, setAnimation ] = React.useState('')

    
    
    
    
    
    
    React.useEffect(()=>{
        
        
        
    },[]);
    
    
    return (
        <>
  <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['propertyonedefault']?.animationClass || {}}>

    <div id="id_three_oneeightfiveone" ref={nodeRef} className={` cursor ${ props.onClick ? 'cursor' : '' } propertyonedefault C_three_oneeightfiveone ${ props.cssClass } ${ transaction['propertyonedefault']?.type ? transaction['propertyonedefault']?.type.toLowerCase() : '' }`} style={ { ...{ ...{}, transitionDuration: transaction['propertyonedefault']?.duration, transitionTimingFunction: transaction['propertyonedefault']?.timingFunction }, ...props.style }} onClick={ props.TextLinkonClick } onMouseEnter={ props.TextLinkonMouseEnter } onMouseOver={ props.TextLinkonMouseOver || function(e){ setTransaction({ }); setvariant('Property 1=Variant2'); }} onKeyPress={ props.TextLinkonKeyPress } onDrag={ props.TextLinkonDrag } onMouseLeave={ props.TextLinkonMouseLeave } onMouseUp={ props.TextLinkonMouseUp } onMouseDown={ props.TextLinkonMouseDown } onKeyDown={ props.TextLinkonKeyDown } onChange={ props.TextLinkonChange } ondelay={ props.TextLinkondelay }>
      {
      props.children ?
      props.children :
      <>
        <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['textlinktxt']?.animationClass || {}}>

          <span id="id_one_nigthzerotwoo"  className={` text textlinktxt    ${ props.onClick ? 'cursor' : ''}  ${ transaction['textlinktxt']?.type ? transaction['textlinktxt']?.type.toLowerCase() : '' }`} style={{ ...{},  ...props.TextLinkTxtStyle , transitionDuration: transaction['textlinktxt']?.duration, transitionTimingFunction: transaction['textlinktxt']?.timingFunction }} onClick={ props.TextLinkTxtonClick } onMouseEnter={ props.TextLinkTxtonMouseEnter } onMouseOver={ props.TextLinkTxtonMouseOver } onKeyPress={ props.TextLinkTxtonKeyPress } onDrag={ props.TextLinkTxtonDrag } onMouseLeave={ props.TextLinkTxtonMouseLeave } onMouseUp={ props.TextLinkTxtonMouseUp } onMouseDown={ props.TextLinkTxtonMouseDown } onKeyDown={ props.TextLinkTxtonKeyDown } onChange={ props.TextLinkTxtonChange } ondelay={ props.TextLinkTxtondelay } >{props.TextLinkTxt0 || `Acerca de`}</span>

        </CSSTransition>

      </>
      }
    </div>

  </CSSTransition>
</>
    ) 
}

PropertyoneDefault.propTypes = {
    style: PropTypes.any,
TextLinkTxt0: PropTypes.any,
TextLinkonClick: PropTypes.any,
TextLinkonMouseEnter: PropTypes.any,
TextLinkonMouseOver: PropTypes.any,
TextLinkonKeyPress: PropTypes.any,
TextLinkonDrag: PropTypes.any,
TextLinkonMouseLeave: PropTypes.any,
TextLinkonMouseUp: PropTypes.any,
TextLinkonMouseDown: PropTypes.any,
TextLinkonKeyDown: PropTypes.any,
TextLinkonChange: PropTypes.any,
TextLinkondelay: PropTypes.any,
TextLinkTxtonClick: PropTypes.any,
TextLinkTxtonMouseEnter: PropTypes.any,
TextLinkTxtonMouseOver: PropTypes.any,
TextLinkTxtonKeyPress: PropTypes.any,
TextLinkTxtonDrag: PropTypes.any,
TextLinkTxtonMouseLeave: PropTypes.any,
TextLinkTxtonMouseUp: PropTypes.any,
TextLinkTxtonMouseDown: PropTypes.any,
TextLinkTxtonKeyDown: PropTypes.any,
TextLinkTxtonChange: PropTypes.any,
TextLinkTxtondelay: PropTypes.any
}
export default PropertyoneDefault;