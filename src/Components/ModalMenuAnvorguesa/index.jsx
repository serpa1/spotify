import React, { Suspense } from 'react';
import PropTypes from 'prop-types';
import * as Contexts  from 'Contexts';
import { OverlaySwapContext } from 'Components/OverlaySwapProvider';
import CorebooksAnimation from 'Components/CorebooksAnimation';
import CorebooksAnimationSingle from 'Components/CorebooksAnimationSingle';

import * as UtilsScripts  from 'utils/utils';
import { Route, useHistory, useParams } from "react-router-dom";
import SingletoneNavigation from 'Class/SingletoneNavigation';

import LoadingComponent from "Components/Loading";


import { Transition } from "react-transition-group";
import { CSSTransition } from "react-transition-group";
import MenuAnvorguesaBibliotecaCarpeta from 'Components/MenuAnvorguesaBibliotecaCarpeta'
import MenuAnvorguesaBibliotecaMusic from 'Components/MenuAnvorguesaBibliotecaMusic'
import { useAppContext, useSessionContext } from 'context/AppContext';
import './ModalMenuAnvorguesa.css'





const ModalMenuAnvorguesa = (props)=>{
    const [state,dispatch] = useAppContext();
    const [, setNameSelectedComponent] = React.useContext(OverlaySwapContext);
    
    const nodeRef = React.useRef(null);
    const history = useHistory();
    let RouterContext = useParams();
    
    
    
    
    
    
    const [globalStateForm, dispatchForm] = React.useState({});
    
    const [ _in, setIn ] = React.useState(false)
    const [ transaction, setTransaction ] = React.useState({}) 
    const [ animation, setAnimation ] = React.useState('')

    
    
    
    
    
    
    React.useEffect(()=>{
        
        
        
    },[]);
    
    
    return (
        <>
  <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['modalmenuanvorguesa']?.animationClass || {}}>

    <div id="id_fourthreeeight_oneeightfoureight" ref={nodeRef} className={` ${ props.onClick ? 'cursor' : '' } modalmenuanvorguesa ${ props.cssClass } ${ transaction['modalmenuanvorguesa']?.type ? transaction['modalmenuanvorguesa']?.type.toLowerCase() : '' }`} style={ { ...{ ...{}, transitionDuration: transaction['modalmenuanvorguesa']?.duration, transitionTimingFunction: transaction['modalmenuanvorguesa']?.timingFunction }, ...props.style }} onClick={ props.ModalMenuAnvorguesaonClick } onMouseEnter={ props.ModalMenuAnvorguesaonMouseEnter } onMouseOver={ props.ModalMenuAnvorguesaonMouseOver } onKeyPress={ props.ModalMenuAnvorguesaonKeyPress } onDrag={ props.ModalMenuAnvorguesaonDrag } onMouseLeave={ props.ModalMenuAnvorguesaonMouseLeave } onMouseUp={ props.ModalMenuAnvorguesaonMouseUp } onMouseDown={ props.ModalMenuAnvorguesaonMouseDown } onKeyDown={ props.ModalMenuAnvorguesaonKeyDown } onChange={ props.ModalMenuAnvorguesaonChange } ondelay={ props.ModalMenuAnvorguesaondelay }>
      {
      props.children ?
      props.children :
      <>
        <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['menuanvorguesabibliotecamusic']?.animationClass || {}}>
          <MenuAnvorguesaBibliotecaMusic { ...{ ...props, style:false } } variant={'Property 1=Default'} cssClass={"C_fourthreeeight_oneeightfournigth cursor "}  />
    </CSSTransition >
<CSSTransition 
        in={_in}
        appear={true} 
        timeout={100}
        classNames={transaction['menuanvorguesabibliotecacarpeta']?.animationClass || {}}
    >
    <MenuAnvorguesaBibliotecaCarpeta { ...{ ...props, style:false } }  variant={'Property 1=Default'}  cssClass={" C_fourthreeeight_oneeightfivezero "}  />
    </CSSTransition >
            
            </>
        }
        </div>
    
    </CSSTransition >
            </>
        
    ) 
}

ModalMenuAnvorguesa.propTypes = {
    style: PropTypes.any,
ModalMenuAnvorguesaonClick: PropTypes.any,
ModalMenuAnvorguesaonMouseEnter: PropTypes.any,
ModalMenuAnvorguesaonMouseOver: PropTypes.any,
ModalMenuAnvorguesaonKeyPress: PropTypes.any,
ModalMenuAnvorguesaonDrag: PropTypes.any,
ModalMenuAnvorguesaonMouseLeave: PropTypes.any,
ModalMenuAnvorguesaonMouseUp: PropTypes.any,
ModalMenuAnvorguesaonMouseDown: PropTypes.any,
ModalMenuAnvorguesaonKeyDown: PropTypes.any,
ModalMenuAnvorguesaonChange: PropTypes.any,
ModalMenuAnvorguesaondelay: PropTypes.any
}
export default ModalMenuAnvorguesa;