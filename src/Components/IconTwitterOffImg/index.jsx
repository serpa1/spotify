import React, { Suspense } from 'react';
import PropTypes from 'prop-types';
import * as Contexts  from 'Contexts';
import { OverlaySwapContext } from 'Components/OverlaySwapProvider';
import CorebooksAnimation from 'Components/CorebooksAnimation';
import CorebooksAnimationSingle from 'Components/CorebooksAnimationSingle';

import * as UtilsScripts  from 'utils/utils';
import { Route, useHistory, useParams } from "react-router-dom";
import SingletoneNavigation from 'Class/SingletoneNavigation';

import LoadingComponent from "Components/Loading";


import { Transition } from "react-transition-group";
import { CSSTransition } from "react-transition-group";
import { useAppContext, useSessionContext } from 'context/AppContext';
import './IconTwitterOffImg.css'





const IconTwitterOffImg = (props)=>{
    const [state,dispatch] = useAppContext();
    const [, setNameSelectedComponent] = React.useContext(OverlaySwapContext);
    
    const nodeRefpropertyonedefault = React.useRef(null);
const nodeRefpropertyonevarianttwoo = React.useRef(null);
    const history = useHistory();
    let RouterContext = useParams();
    
    
    
    
    
    
    const [globalStateForm, dispatchForm] = React.useState({});
    const [ variant, setvariant] = React.useState(props.variant || 'Property 1=Default')
    const [ _in, setIn ] = React.useState(false)
    const [ transaction, setTransaction ] = React.useState({}) 
    const [ animation, setAnimation ] = React.useState('')

    
    
    
    
    
    
    React.useEffect(()=>{
        
        
        
    },[]);
    
    
    React.useEffect(()=>{
        
        setIn(false);
        setTimeout(()=>setIn(true))
           
      },[variant]);
    const switchVariant = (value)=>{
        switch (value) {
            case 'Property 1=Default':{
            return (
            <>

  <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['propertyonedefault']?.animationClass || {}}>

    <div id="id_onesix_onenigththree" ref={nodeRefpropertyonedefault} className={` cursor ${ props.onClick ? 'cursor' : '' } icontwitteroffimgpropertyonedefault C_onesix_onenigththree ${ props.cssClass } ${ transaction['propertyonedefault']?.type ? transaction['propertyonedefault']?.type.toLowerCase() : '' }`} style={ { ...{ ...{}, transitionDuration: transaction['propertyonedefault']?.duration, transitionTimingFunction: transaction['propertyonedefault']?.timingFunction }, ...props.style }} onClick={ props.IconTwitterOffImgonClick } onMouseEnter={ props.IconTwitterOffImgonMouseEnter } onMouseOver={ props.IconTwitterOffImgonMouseOver || function(e){ setTransaction({ }); setvariant('Property 1=Variant2'); }} onKeyPress={ props.IconTwitterOffImgonKeyPress } onDrag={ props.IconTwitterOffImgonDrag } onMouseLeave={ props.IconTwitterOffImgonMouseLeave } onMouseUp={ props.IconTwitterOffImgonMouseUp } onMouseDown={ props.IconTwitterOffImgonMouseDown } onKeyDown={ props.IconTwitterOffImgonKeyDown } onChange={ props.IconTwitterOffImgonChange } ondelay={ props.IconTwitterOffImgondelay }>
      {
      props.children ?
      props.children :
      <>
        <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['icontwitteroffimg']?.animationClass || {}}>
          <img id="id_onesix_oneeightfour" className={` rectangle icontwitteroffimgicontwitteroffimg ${ props.onClick ? 'cursor' : '' } ${ transaction['icontwitteroffimg']?.type ? transaction['icontwitteroffimg']?.type.toLowerCase() : '' }`} style={{ ...{},  ...props.IconTwitterOffImgStyle , transitionDuration: transaction['icontwitteroffimg']?.duration, transitionTimingFunction: transaction['icontwitteroffimg']?.timingFunction }} onClick={ props.IconTwitterOffImgonClick } onMouseEnter={ props.IconTwitterOffImgonMouseEnter } onMouseOver={ props.IconTwitterOffImgonMouseOver } onKeyPress={ props.IconTwitterOffImgonKeyPress } onDrag={ props.IconTwitterOffImgonDrag } onMouseLeave={ props.IconTwitterOffImgonMouseLeave } onMouseUp={ props.IconTwitterOffImgonMouseUp } onMouseDown={ props.IconTwitterOffImgonMouseDown } onKeyDown={ props.IconTwitterOffImgonKeyDown } onChange={ props.IconTwitterOffImgonChange } ondelay={ props.IconTwitterOffImgondelay } src={props.IconTwitterOffImg0 || "https://cdn.uncodie.com/tACQBrKCV97e0EqPE1XNLJ/6ecaabf4da8cb119aca1992cbeccb0934205d4c7.png" } />
        </CSSTransition>

      </>
      }
    </div>

  </CSSTransition>

</>
            )
        }
case 'Property 1=Variant2':{
            return (
            <>

  <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['propertyonevarianttwoo']?.animationClass || {}}>

    <div id="id_onesix_onenigthfive" ref={nodeRefpropertyonevarianttwoo} className={` cursor ${ props.onClick ? 'cursor' : '' } icontwitteroffimgpropertyonevarianttwoo C_onesix_onenigthfive ${ props.cssClass } ${ transaction['propertyonevarianttwoo']?.type ? transaction['propertyonevarianttwoo']?.type.toLowerCase() : '' }`} style={ { ...{ ...{}, transitionDuration: transaction['propertyonevarianttwoo']?.duration, transitionTimingFunction: transaction['propertyonevarianttwoo']?.timingFunction }, ...props.style }} onClick={ props.IconTwitterOffImgonClick } onMouseEnter={ props.IconTwitterOffImgonMouseEnter } onMouseOver={ props.IconTwitterOffImgonMouseOver } onKeyPress={ props.IconTwitterOffImgonKeyPress } onDrag={ props.IconTwitterOffImgonDrag } onMouseLeave={ props.IconTwitterOffImgonMouseLeave || function(e){ setTransaction({ }); setvariant('Property 1=Default'); }} onMouseUp={ props.IconTwitterOffImgonMouseUp } onMouseDown={ props.IconTwitterOffImgonMouseDown } onKeyDown={ props.IconTwitterOffImgonKeyDown } onChange={ props.IconTwitterOffImgonChange } ondelay={ props.IconTwitterOffImgondelay }>
      {
      props.children ?
      props.children :
      <>
        <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['icontwitteroffimg']?.animationClass || {}}>
          <img id="id_onesix_onenigthsix" className={` rectangle icontwitteroffimgicontwitteroffimg ${ props.onClick ? 'cursor' : '' } ${ transaction['icontwitteroffimg']?.type ? transaction['icontwitteroffimg']?.type.toLowerCase() : '' }`} style={{ ...{},  ...props.IconTwitterOffImgStyle , transitionDuration: transaction['icontwitteroffimg']?.duration, transitionTimingFunction: transaction['icontwitteroffimg']?.timingFunction }} onClick={ props.IconTwitterOffImgonClick } onMouseEnter={ props.IconTwitterOffImgonMouseEnter } onMouseOver={ props.IconTwitterOffImgonMouseOver } onKeyPress={ props.IconTwitterOffImgonKeyPress } onDrag={ props.IconTwitterOffImgonDrag } onMouseLeave={ props.IconTwitterOffImgonMouseLeave } onMouseUp={ props.IconTwitterOffImgonMouseUp } onMouseDown={ props.IconTwitterOffImgonMouseDown } onKeyDown={ props.IconTwitterOffImgonKeyDown } onChange={ props.IconTwitterOffImgonChange } ondelay={ props.IconTwitterOffImgondelay } src={props.IconTwitterOffImg0 || "https://cdn.uncodie.com/tACQBrKCV97e0EqPE1XNLJ/2356c26847211c13e32ecc4b7d84e7cd16148cde.png" } />
        </CSSTransition>

      </>
      }
    </div>

  </CSSTransition>

</>
            )
        }
        }
    }

    return (
        <>
            {switchVariant(variant)}
        </>
    )
    
}

IconTwitterOffImg.propTypes = {
    style: PropTypes.any,
variant: PropTypes.any,
IconTwitterOffImg0: PropTypes.any,
IconTwitterOffImgonClick: PropTypes.any,
IconTwitterOffImgonMouseEnter: PropTypes.any,
IconTwitterOffImgonMouseOver: PropTypes.any,
IconTwitterOffImgonKeyPress: PropTypes.any,
IconTwitterOffImgonDrag: PropTypes.any,
IconTwitterOffImgonMouseLeave: PropTypes.any,
IconTwitterOffImgonMouseUp: PropTypes.any,
IconTwitterOffImgonMouseDown: PropTypes.any,
IconTwitterOffImgonKeyDown: PropTypes.any,
IconTwitterOffImgonChange: PropTypes.any,
IconTwitterOffImgondelay: PropTypes.any
}
export default IconTwitterOffImg;