import React, { Suspense } from 'react';
import PropTypes from 'prop-types';
import * as Contexts  from 'Contexts';
import { OverlaySwapContext } from 'Components/OverlaySwapProvider';
import CorebooksAnimation from 'Components/CorebooksAnimation';
import CorebooksAnimationSingle from 'Components/CorebooksAnimationSingle';

import * as UtilsScripts  from 'utils/utils';
import { Route, useHistory, useParams } from "react-router-dom";
import SingletoneNavigation from 'Class/SingletoneNavigation';

import LoadingComponent from "Components/Loading";


import { Transition } from "react-transition-group";
import { CSSTransition } from "react-transition-group";
import LanguageButton from 'Components/LanguageButton'
import Paragraphone from 'Components/Paragraphone'
import SubtitleTwoo from 'Components/SubtitleTwoo'
import IconInstagramOffImg from 'Components/IconInstagramOffImg'
import IconTwitterOffImg from 'Components/IconTwitterOffImg'
import TextLink from 'Components/TextLink'
import Subtitleone from 'Components/Subtitleone'
import { useAppContext, useSessionContext } from 'context/AppContext';
import './FooterMobile.css'





const FooterMobile = (props)=>{
    const [state,dispatch] = useAppContext();
    const [, setNameSelectedComponent] = React.useContext(OverlaySwapContext);
    
    const nodeRef = React.useRef(null);
    const history = useHistory();
    let RouterContext = useParams();
    
    
    
    
    
    
    const [globalStateForm, dispatchForm] = React.useState({});
    
    const [ _in, setIn ] = React.useState(false)
    const [ transaction, setTransaction ] = React.useState({}) 
    const [ animation, setAnimation ] = React.useState('')

    
    
    
    
    
    
    React.useEffect(()=>{
        
        
        
    },[]);
    
    
    return (
        <>
  <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['footermobile']?.animationClass || {}}>

    <div id="id_threethreefive_oneonesixnigth" ref={nodeRef} className={` ${ props.onClick ? 'cursor' : '' } footermobile show-xs show-s show-m hide-l hide-xl C_threethreefive_oneonesixnigth ${ props.cssClass } ${ transaction['footermobile']?.type ? transaction['footermobile']?.type.toLowerCase() : '' }`} style={ { ...{ ...{"width":"100%"}, transitionDuration: transaction['footermobile']?.duration, transitionTimingFunction: transaction['footermobile']?.timingFunction }, ...props.style }} onClick={ props.FooterMobileonClick } onMouseEnter={ props.FooterMobileonMouseEnter } onMouseOver={ props.FooterMobileonMouseOver } onKeyPress={ props.FooterMobileonKeyPress } onDrag={ props.FooterMobileonDrag } onMouseLeave={ props.FooterMobileonMouseLeave } onMouseUp={ props.FooterMobileonMouseUp } onMouseDown={ props.FooterMobileonMouseDown } onKeyDown={ props.FooterMobileonKeyDown } onChange={ props.FooterMobileonChange } ondelay={ props.FooterMobileondelay }>
      {
      props.children ?
      props.children :
      <>
        <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['footerdatamobile']?.animationClass || {}}>

          <div id="id_twoofivethree_onetwooseventwoo" className={` frame footerdatamobile ${ props.onClick ? 'cursor' : '' } ${ transaction['footerdatamobile']?.type ? transaction['footerdatamobile']?.type.toLowerCase() : '' }`} style={ { ...{}, ...props.FooterDataMobileStyle , transitionDuration: transaction['footerdatamobile']?.duration, transitionTimingFunction: transaction['footerdatamobile']?.timingFunction } } onClick={ props.FooterDataMobileonClick } onMouseEnter={ props.FooterDataMobileonMouseEnter } onMouseOver={ props.FooterDataMobileonMouseOver } onKeyPress={ props.FooterDataMobileonKeyPress } onDrag={ props.FooterDataMobileonDrag } onMouseLeave={ props.FooterDataMobileonMouseLeave } onMouseUp={ props.FooterDataMobileonMouseUp } onMouseDown={ props.FooterDataMobileonMouseDown } onKeyDown={ props.FooterDataMobileonKeyDown } onChange={ props.FooterDataMobileonChange } ondelay={ props.FooterDataMobileondelay }>
            <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['footerdatamobilecompany']?.animationClass || {}}>

              <div id="id_twoofivethree_onetwooseventhree" className={` frame footerdatamobilecompany ${ props.onClick ? 'cursor' : '' } ${ transaction['footerdatamobilecompany']?.type ? transaction['footerdatamobilecompany']?.type.toLowerCase() : '' }`} style={ { ...{}, ...props.FooterDataMobileCompanyStyle , transitionDuration: transaction['footerdatamobilecompany']?.duration, transitionTimingFunction: transaction['footerdatamobilecompany']?.timingFunction } } onClick={ props.FooterDataMobileCompanyonClick } onMouseEnter={ props.FooterDataMobileCompanyonMouseEnter } onMouseOver={ props.FooterDataMobileCompanyonMouseOver } onKeyPress={ props.FooterDataMobileCompanyonKeyPress } onDrag={ props.FooterDataMobileCompanyonDrag } onMouseLeave={ props.FooterDataMobileCompanyonMouseLeave } onMouseUp={ props.FooterDataMobileCompanyonMouseUp } onMouseDown={ props.FooterDataMobileCompanyonMouseDown } onKeyDown={ props.FooterDataMobileCompanyonKeyDown } onChange={ props.FooterDataMobileCompanyonChange } ondelay={ props.FooterDataMobileCompanyondelay }>
                <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['subtitleone']?.animationClass || {}}>
                  <Subtitleone { ...{ ...props, style:false } } SubtilteoneText0={ props.SubtilteoneText0 || "Compañía" } cssClass={"C_twoofivethree_onetwoosevenfour "}  />
    </CSSTransition >
<CSSTransition 
        in={_in}
        appear={true} 
        timeout={100}
        classNames={transaction['footerlinkscompany']?.animationClass || {}}
    >
    
                    <div id="id_twoofivethree_onetwoosevenfive" className={` frame footerlinkscompany ${ props.onClick ? 'cursor' : '' } ${ transaction['footerlinkscompany']?.type ? transaction['footerlinkscompany']?.type.toLowerCase() : '' }`} style={ { ...{}, ...props.FooterLinksCompanyStyle , transitionDuration: transaction['footerlinkscompany']?.duration, transitionTimingFunction: transaction['footerlinkscompany']?.timingFunction } } onClick={ props.FooterLinksCompanyonClick } onMouseEnter={ props.FooterLinksCompanyonMouseEnter } onMouseOver={ props.FooterLinksCompanyonMouseOver } onKeyPress={ props.FooterLinksCompanyonKeyPress } onDrag={ props.FooterLinksCompanyonDrag } onMouseLeave={ props.FooterLinksCompanyonMouseLeave } onMouseUp={ props.FooterLinksCompanyonMouseUp } onMouseDown={ props.FooterLinksCompanyonMouseDown } onKeyDown={ props.FooterLinksCompanyonKeyDown } onChange={ props.FooterLinksCompanyonChange } ondelay={ props.FooterLinksCompanyondelay }>
                    <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['textlink']?.animationClass || {}}>
                      <TextLink { ...{ ...props, style:false } } variant={'Property 1=Default'} TextLinkTxt0={ props.TextLinkTxt0 || "Acerca de" } cssClass={"C_twoofivethree_onetwoosevensix cursor "}  />
    </CSSTransition >
<CSSTransition 
        in={_in}
        appear={true} 
        timeout={100}
        classNames={transaction['textlink']?.animationClass || {}}
    >
    <TextLink { ...{ ...props, style:false } }  variant={'Property 1=Default'} TextLinkTxt0={ props.TextLinkTxt1 || " Empleo"} cssClass={"C_twoofivethree_onetwoosevenseven cursor "}  />
    </CSSTransition >
<CSSTransition 
        in={_in}
        appear={true} 
        timeout={100}
        classNames={transaction['textlink']?.animationClass || {}}
    >
    <TextLink { ...{ ...props, style:false } }  variant={'Property 1=Default'} TextLinkTxt0={ props.TextLinkTxt2 || " For the Record"} cssClass={"C_twoofivethree_onetwooseveneight cursor "}  />
    </CSSTransition >
                    </div>
               
    </CSSTransition >
                    </div>
               
    </CSSTransition >
<CSSTransition 
        in={_in}
        appear={true} 
        timeout={100}
        classNames={transaction['footerdatamobilecommunity']?.animationClass || {}}
    >
    
                    <div id="id_twoofivethree_onetwoosevennigth" className={` frame footerdatamobilecommunity ${ props.onClick ? 'cursor' : '' } ${ transaction['footerdatamobilecommunity']?.type ? transaction['footerdatamobilecommunity']?.type.toLowerCase() : '' }`} style={ { ...{}, ...props.FooterDataMobileCommunityStyle , transitionDuration: transaction['footerdatamobilecommunity']?.duration, transitionTimingFunction: transaction['footerdatamobilecommunity']?.timingFunction } } onClick={ props.FooterDataMobileCommunityonClick } onMouseEnter={ props.FooterDataMobileCommunityonMouseEnter } onMouseOver={ props.FooterDataMobileCommunityonMouseOver } onKeyPress={ props.FooterDataMobileCommunityonKeyPress } onDrag={ props.FooterDataMobileCommunityonDrag } onMouseLeave={ props.FooterDataMobileCommunityonMouseLeave } onMouseUp={ props.FooterDataMobileCommunityonMouseUp } onMouseDown={ props.FooterDataMobileCommunityonMouseDown } onKeyDown={ props.FooterDataMobileCommunityonKeyDown } onChange={ props.FooterDataMobileCommunityonChange } ondelay={ props.FooterDataMobileCommunityondelay }>
                        <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['subtitleone']?.animationClass || {}}>
                          <Subtitleone { ...{ ...props, style:false } } SubtilteoneText0={ props.SubtilteoneText1 || "Comunidades" } cssClass={"C_twoofivethree_onetwooeightzero "}  />
    </CSSTransition >
<CSSTransition 
        in={_in}
        appear={true} 
        timeout={100}
        classNames={transaction['footerlinkscompany']?.animationClass || {}}
    >
    
                    <div id="id_twoofivethree_onetwooeightone" className={` frame footerlinkscompany ${ props.onClick ? 'cursor' : '' } ${ transaction['footerlinkscompany']?.type ? transaction['footerlinkscompany']?.type.toLowerCase() : '' }`} style={ { ...{}, ...props.FooterLinksCompanyStyle , transitionDuration: transaction['footerlinkscompany']?.duration, transitionTimingFunction: transaction['footerlinkscompany']?.timingFunction } } onClick={ props.FooterLinksCompanyonClick } onMouseEnter={ props.FooterLinksCompanyonMouseEnter } onMouseOver={ props.FooterLinksCompanyonMouseOver } onKeyPress={ props.FooterLinksCompanyonKeyPress } onDrag={ props.FooterLinksCompanyonDrag } onMouseLeave={ props.FooterLinksCompanyonMouseLeave } onMouseUp={ props.FooterLinksCompanyonMouseUp } onMouseDown={ props.FooterLinksCompanyonMouseDown } onKeyDown={ props.FooterLinksCompanyonKeyDown } onChange={ props.FooterLinksCompanyonChange } ondelay={ props.FooterLinksCompanyondelay }>
                            <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['textlink']?.animationClass || {}}>
                              <TextLink { ...{ ...props, style:false } } variant={'Property 1=Default'} TextLinkTxt0={ props.TextLinkTxt3 || "Para artistas" } cssClass={"C_twoofivethree_onetwooeighttwoo cursor "}  />
    </CSSTransition >
<CSSTransition 
        in={_in}
        appear={true} 
        timeout={100}
        classNames={transaction['textlink']?.animationClass || {}}
    >
    <TextLink { ...{ ...props, style:false } }  variant={'Property 1=Default'} TextLinkTxt0={ props.TextLinkTxt4 || " Desarrolladores"} cssClass={"C_twoofivethree_onetwooeightthree cursor "}  />
    </CSSTransition >
<CSSTransition 
        in={_in}
        appear={true} 
        timeout={100}
        classNames={transaction['textlink']?.animationClass || {}}
    >
    <TextLink { ...{ ...props, style:false } }  variant={'Property 1=Default'} TextLinkTxt0={ props.TextLinkTxt5 || " Publicidad"} cssClass={"C_twoofivethree_onetwooeightfour cursor "}  />
    </CSSTransition >
<CSSTransition 
        in={_in}
        appear={true} 
        timeout={100}
        classNames={transaction['textlink']?.animationClass || {}}
    >
    <TextLink { ...{ ...props, style:false } }  variant={'Property 1=Default'} TextLinkTxt0={ props.TextLinkTxt6 || " Inversionistas"} cssClass={"C_twoofivethree_onetwooeightfive cursor "}  />
    </CSSTransition >
<CSSTransition 
        in={_in}
        appear={true} 
        timeout={100}
        classNames={transaction['textlink']?.animationClass || {}}
    >
    <TextLink { ...{ ...props, style:false } }  variant={'Property 1=Default'} TextLinkTxt0={ props.TextLinkTxt7 || " Proveedores"} cssClass={"C_twoofivethree_onetwooeightsix cursor "}  />
    </CSSTransition >
<CSSTransition 
        in={_in}
        appear={true} 
        timeout={100}
        classNames={transaction['textlink']?.animationClass || {}}
    >
    <TextLink { ...{ ...props, style:false } }  variant={'Property 1=Default'} TextLinkTxt0={ props.TextLinkTxt8 || " Spotify for Work"} cssClass={"C_twoofivethree_onetwooeightseven cursor "}  />
    </CSSTransition >
                    </div>
               
    </CSSTransition >
                    </div>
               
    </CSSTransition >
<CSSTransition 
        in={_in}
        appear={true} 
        timeout={100}
        classNames={transaction['footerdatamobileutility']?.animationClass || {}}
    >
    
                    <div id="id_twoofivethree_onetwooeighteight" className={` frame footerdatamobileutility ${ props.onClick ? 'cursor' : '' } ${ transaction['footerdatamobileutility']?.type ? transaction['footerdatamobileutility']?.type.toLowerCase() : '' }`} style={ { ...{}, ...props.FooterDataMobileUtilityStyle , transitionDuration: transaction['footerdatamobileutility']?.duration, transitionTimingFunction: transaction['footerdatamobileutility']?.timingFunction } } onClick={ props.FooterDataMobileUtilityonClick } onMouseEnter={ props.FooterDataMobileUtilityonMouseEnter } onMouseOver={ props.FooterDataMobileUtilityonMouseOver } onKeyPress={ props.FooterDataMobileUtilityonKeyPress } onDrag={ props.FooterDataMobileUtilityonDrag } onMouseLeave={ props.FooterDataMobileUtilityonMouseLeave } onMouseUp={ props.FooterDataMobileUtilityonMouseUp } onMouseDown={ props.FooterDataMobileUtilityonMouseDown } onKeyDown={ props.FooterDataMobileUtilityonKeyDown } onChange={ props.FooterDataMobileUtilityonChange } ondelay={ props.FooterDataMobileUtilityondelay }>
                                <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['subtitleone']?.animationClass || {}}>
                                  <Subtitleone { ...{ ...props, style:false } } SubtilteoneText0={ props.SubtilteoneText2 || "Enlaces útiles" } cssClass={"C_twoofivethree_onetwooeightnigth "}  />
    </CSSTransition >
<CSSTransition 
        in={_in}
        appear={true} 
        timeout={100}
        classNames={transaction['footerlinksutility']?.animationClass || {}}
    >
    
                    <div id="id_twoofivethree_onetwoonigthzero" className={` frame footerlinksutility ${ props.onClick ? 'cursor' : '' } ${ transaction['footerlinksutility']?.type ? transaction['footerlinksutility']?.type.toLowerCase() : '' }`} style={ { ...{}, ...props.FooterLinksUtilityStyle , transitionDuration: transaction['footerlinksutility']?.duration, transitionTimingFunction: transaction['footerlinksutility']?.timingFunction } } onClick={ props.FooterLinksUtilityonClick } onMouseEnter={ props.FooterLinksUtilityonMouseEnter } onMouseOver={ props.FooterLinksUtilityonMouseOver } onKeyPress={ props.FooterLinksUtilityonKeyPress } onDrag={ props.FooterLinksUtilityonDrag } onMouseLeave={ props.FooterLinksUtilityonMouseLeave } onMouseUp={ props.FooterLinksUtilityonMouseUp } onMouseDown={ props.FooterLinksUtilityonMouseDown } onKeyDown={ props.FooterLinksUtilityonKeyDown } onChange={ props.FooterLinksUtilityonChange } ondelay={ props.FooterLinksUtilityondelay }>
                                    <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['textlink']?.animationClass || {}}>
                                      <TextLink { ...{ ...props, style:false } } variant={'Property 1=Default'} TextLinkTxt0={ props.TextLinkTxt9 || "Ayuda" } cssClass={"C_twoofivethree_onetwoonigthone cursor "}  />
    </CSSTransition >
<CSSTransition 
        in={_in}
        appear={true} 
        timeout={100}
        classNames={transaction['textlink']?.animationClass || {}}
    >
    <TextLink { ...{ ...props, style:false } }  variant={'Property 1=Default'} TextLinkTxt0={ props.TextLinkTxt10 || " App móvil gratis"} cssClass={"C_twoofivethree_onetwoonigthtwoo cursor "}  />
    </CSSTransition >
                    </div>
               
    </CSSTransition >
                    </div>
               
    </CSSTransition >
                    </div>
               
    </CSSTransition >
<CSSTransition 
        in={_in}
        appear={true} 
        timeout={100}
        classNames={transaction['footerdatamobilesocialmedia']?.animationClass || {}}
    >
    
                    <div id="id_twoofivethree_onetwoonigththree" className={` frame footerdatamobilesocialmedia ${ props.onClick ? 'cursor' : '' } ${ transaction['footerdatamobilesocialmedia']?.type ? transaction['footerdatamobilesocialmedia']?.type.toLowerCase() : '' }`} style={ { ...{}, ...props.FooterDataMobileSocialMediaStyle , transitionDuration: transaction['footerdatamobilesocialmedia']?.duration, transitionTimingFunction: transaction['footerdatamobilesocialmedia']?.timingFunction } } onClick={ props.FooterDataMobileSocialMediaonClick } onMouseEnter={ props.FooterDataMobileSocialMediaonMouseEnter } onMouseOver={ props.FooterDataMobileSocialMediaonMouseOver } onKeyPress={ props.FooterDataMobileSocialMediaonKeyPress } onDrag={ props.FooterDataMobileSocialMediaonDrag } onMouseLeave={ props.FooterDataMobileSocialMediaonMouseLeave } onMouseUp={ props.FooterDataMobileSocialMediaonMouseUp } onMouseDown={ props.FooterDataMobileSocialMediaonMouseDown } onKeyDown={ props.FooterDataMobileSocialMediaonKeyDown } onChange={ props.FooterDataMobileSocialMediaonChange } ondelay={ props.FooterDataMobileSocialMediaondelay }>
                                        <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['footerdatamobilesocialmediacont']?.animationClass || {}}>

                                          <div id="id_twoofivethree_onetwoonigthfour" className={` frame footerdatamobilesocialmediacont ${ props.onClick ? 'cursor' : '' } ${ transaction['footerdatamobilesocialmediacont']?.type ? transaction['footerdatamobilesocialmediacont']?.type.toLowerCase() : '' }`} style={ { ...{}, ...props.FooterDataMobileSocialMediaContStyle , transitionDuration: transaction['footerdatamobilesocialmediacont']?.duration, transitionTimingFunction: transaction['footerdatamobilesocialmediacont']?.timingFunction } } onClick={ props.FooterDataMobileSocialMediaContonClick } onMouseEnter={ props.FooterDataMobileSocialMediaContonMouseEnter } onMouseOver={ props.FooterDataMobileSocialMediaContonMouseOver } onKeyPress={ props.FooterDataMobileSocialMediaContonKeyPress } onDrag={ props.FooterDataMobileSocialMediaContonDrag } onMouseLeave={ props.FooterDataMobileSocialMediaContonMouseLeave } onMouseUp={ props.FooterDataMobileSocialMediaContonMouseUp } onMouseDown={ props.FooterDataMobileSocialMediaContonMouseDown } onKeyDown={ props.FooterDataMobileSocialMediaContonKeyDown } onChange={ props.FooterDataMobileSocialMediaContonChange } ondelay={ props.FooterDataMobileSocialMediaContondelay }>
                                            <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['icontwitteroffimg']?.animationClass || {}}>
                                              <IconTwitterOffImg { ...{ ...props, style:false } } variant={'Property 1=Default'} cssClass={"C_twoofivethree_onetwoonigthfive cursor "}  />
    </CSSTransition >
<CSSTransition 
        in={_in}
        appear={true} 
        timeout={100}
        classNames={transaction['iconinstagramoffimg']?.animationClass || {}}
    >
    <IconInstagramOffImg { ...{ ...props, style:false } }  variant={'Property 1=Default'}  cssClass={" C_twoofivethree_onetwoonigthsix cursor "}  />
    </CSSTransition >
<CSSTransition 
        in={_in}
        appear={true} 
        timeout={100}
        classNames={transaction['icontwitteroffimg']?.animationClass || {}}
    >
    <IconTwitterOffImg { ...{ ...props, style:false } }  variant={'Property 1=Default'}  cssClass={" C_twoofivethree_onetwoonigthseven cursor "}  />
    </CSSTransition >
                    </div>
               
    </CSSTransition >
                    </div>
               
    </CSSTransition >
<CSSTransition 
        in={_in}
        appear={true} 
        timeout={100}
        classNames={transaction['menulegacymobilebutton']?.animationClass || {}}
    >
    
                    <div id="id_twoofivethree_onetwoonigtheight" className={` frame menulegacymobilebutton ${ props.onClick ? 'cursor' : '' } ${ transaction['menulegacymobilebutton']?.type ? transaction['menulegacymobilebutton']?.type.toLowerCase() : '' }`} style={ { ...{}, ...props.MenuLegacyMobileButtonStyle , transitionDuration: transaction['menulegacymobilebutton']?.duration, transitionTimingFunction: transaction['menulegacymobilebutton']?.timingFunction } } onClick={ props.MenuLegacyMobileButtononClick } onMouseEnter={ props.MenuLegacyMobileButtononMouseEnter } onMouseOver={ props.MenuLegacyMobileButtononMouseOver } onKeyPress={ props.MenuLegacyMobileButtononKeyPress } onDrag={ props.MenuLegacyMobileButtononDrag } onMouseLeave={ props.MenuLegacyMobileButtononMouseLeave } onMouseUp={ props.MenuLegacyMobileButtononMouseUp } onMouseDown={ props.MenuLegacyMobileButtononMouseDown } onKeyDown={ props.MenuLegacyMobileButtononKeyDown } onChange={ props.MenuLegacyMobileButtononChange } ondelay={ props.MenuLegacyMobileButtonondelay }>
                                                <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['menulegacymobile']?.animationClass || {}}>

                                                  <div id="id_twoofivethree_onetwoonigthnigth" className={` frame menulegacymobile ${ props.onClick ? 'cursor' : '' } ${ transaction['menulegacymobile']?.type ? transaction['menulegacymobile']?.type.toLowerCase() : '' }`} style={ { ...{}, ...props.MenuLegacyMobileStyle , transitionDuration: transaction['menulegacymobile']?.duration, transitionTimingFunction: transaction['menulegacymobile']?.timingFunction } } onClick={ props.MenuLegacyMobileonClick } onMouseEnter={ props.MenuLegacyMobileonMouseEnter } onMouseOver={ props.MenuLegacyMobileonMouseOver } onKeyPress={ props.MenuLegacyMobileonKeyPress } onDrag={ props.MenuLegacyMobileonDrag } onMouseLeave={ props.MenuLegacyMobileonMouseLeave } onMouseUp={ props.MenuLegacyMobileonMouseUp } onMouseDown={ props.MenuLegacyMobileonMouseDown } onKeyDown={ props.MenuLegacyMobileonKeyDown } onChange={ props.MenuLegacyMobileonChange } ondelay={ props.MenuLegacyMobileondelay }>
                                                    <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['menulegacymobileone']?.animationClass || {}}>

                                                      <div id="id_twoofivethree_onethreezerozero" className={` frame menulegacymobileone ${ props.onClick ? 'cursor' : '' } ${ transaction['menulegacymobileone']?.type ? transaction['menulegacymobileone']?.type.toLowerCase() : '' }`} style={ { ...{}, ...props.MenuLegacyMobileOneStyle , transitionDuration: transaction['menulegacymobileone']?.duration, transitionTimingFunction: transaction['menulegacymobileone']?.timingFunction } } onClick={ props.MenuLegacyMobileOneonClick } onMouseEnter={ props.MenuLegacyMobileOneonMouseEnter } onMouseOver={ props.MenuLegacyMobileOneonMouseOver } onKeyPress={ props.MenuLegacyMobileOneonKeyPress } onDrag={ props.MenuLegacyMobileOneonDrag } onMouseLeave={ props.MenuLegacyMobileOneonMouseLeave } onMouseUp={ props.MenuLegacyMobileOneonMouseUp } onMouseDown={ props.MenuLegacyMobileOneonMouseDown } onKeyDown={ props.MenuLegacyMobileOneonKeyDown } onChange={ props.MenuLegacyMobileOneonChange } ondelay={ props.MenuLegacyMobileOneondelay }>
                                                        <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['subtitletwoo']?.animationClass || {}}>
                                                          <SubtitleTwoo { ...{ ...props, style:false } } SubtitletwooText0={ props.SubtitletwooText0 || "Legal" } cssClass={"C_twoofivethree_onethreezeroone "}  />
    </CSSTransition >
<CSSTransition 
        in={_in}
        appear={true} 
        timeout={100}
        classNames={transaction['subtitletwoo']?.animationClass || {}}
    >
    <SubtitleTwoo { ...{ ...props, style:false } }   SubtitletwooText0={ props.SubtitletwooText1 || " Centro de privacidad"} cssClass={"C_twoofivethree_onethreezerotwoo "}  />
    </CSSTransition >
<CSSTransition 
        in={_in}
        appear={true} 
        timeout={100}
        classNames={transaction['subtitletwoo']?.animationClass || {}}
    >
    <SubtitleTwoo { ...{ ...props, style:false } }   SubtitletwooText0={ props.SubtitletwooText2 || " Políticas de Privacidad"} cssClass={"C_twoofivethree_onethreezerothree "}  />
    </CSSTransition >
                    </div>
               
    </CSSTransition >
<CSSTransition 
        in={_in}
        appear={true} 
        timeout={100}
        classNames={transaction['menulegacymobiletwo']?.animationClass || {}}
    >
    
                    <div id="id_twoofivethree_onethreezerofour" className={` frame menulegacymobiletwo ${ props.onClick ? 'cursor' : '' } ${ transaction['menulegacymobiletwo']?.type ? transaction['menulegacymobiletwo']?.type.toLowerCase() : '' }`} style={ { ...{}, ...props.MenuLegacyMobileTwoStyle , transitionDuration: transaction['menulegacymobiletwo']?.duration, transitionTimingFunction: transaction['menulegacymobiletwo']?.timingFunction } } onClick={ props.MenuLegacyMobileTwoonClick } onMouseEnter={ props.MenuLegacyMobileTwoonMouseEnter } onMouseOver={ props.MenuLegacyMobileTwoonMouseOver } onKeyPress={ props.MenuLegacyMobileTwoonKeyPress } onDrag={ props.MenuLegacyMobileTwoonDrag } onMouseLeave={ props.MenuLegacyMobileTwoonMouseLeave } onMouseUp={ props.MenuLegacyMobileTwoonMouseUp } onMouseDown={ props.MenuLegacyMobileTwoonMouseDown } onKeyDown={ props.MenuLegacyMobileTwoonKeyDown } onChange={ props.MenuLegacyMobileTwoonChange } ondelay={ props.MenuLegacyMobileTwoondelay }>
                                                            <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['subtitletwoo']?.animationClass || {}}>
                                                              <SubtitleTwoo { ...{ ...props, style:false } } SubtitletwooText0={ props.SubtitletwooText3 || "Políticas de Privacidad" } cssClass={"C_twoofivethree_onethreezerofive "}  />
    </CSSTransition >
<CSSTransition 
        in={_in}
        appear={true} 
        timeout={100}
        classNames={transaction['subtitletwoo']?.animationClass || {}}
    >
    <SubtitleTwoo { ...{ ...props, style:false } }   SubtitletwooText0={ props.SubtitletwooText4 || " Sobre los anuncios"} cssClass={"C_twoofivethree_onethreezerosix "}  />
    </CSSTransition >
<CSSTransition 
        in={_in}
        appear={true} 
        timeout={100}
        classNames={transaction['subtitletwoo']?.animationClass || {}}
    >
    <SubtitleTwoo { ...{ ...props, style:false } }   SubtitletwooText0={ props.SubtitletwooText5 || " Cookies"} cssClass={"C_twoofivethree_onethreezeroseven "}  />
    </CSSTransition >
                    </div>
               
    </CSSTransition >
<CSSTransition 
        in={_in}
        appear={true} 
        timeout={100}
        classNames={transaction['subtitletwoo']?.animationClass || {}}
    >
    <SubtitleTwoo { ...{ ...props, style:false } }   SubtitletwooText0={ props.SubtitletwooText6 || " Cookies"} cssClass={"C_twoofivethree_onethreezeroeight "}  />
    </CSSTransition >
<CSSTransition 
        in={_in}
        appear={true} 
        timeout={100}
        classNames={transaction['paragraphone']?.animationClass || {}}
    >
    <Paragraphone { ...{ ...props, style:false } }   ParagraphoneText0={ props.ParagraphoneText0 || " © 2023 Spotify AB"} cssClass={"C_twoofivethree_onethreezeronigth "}  />
    </CSSTransition >
                    </div>
               
    </CSSTransition >
<CSSTransition 
        in={_in}
        appear={true} 
        timeout={100}
        classNames={transaction['languagebutton']?.animationClass || {}}
    >
    <LanguageButton { ...{ ...props, style:false } }  variant={'Property 1=Default'} LanguageButtonText0={ props.LanguageButtonText0 || " Español de Latinoamérica"} cssClass={"C_twoofivethree_onethreefiveseven "}  />
    </CSSTransition >
                    </div>
               
    </CSSTransition >
            
            </>
        }
        </div>
    
    </CSSTransition >
            </>
        
    ) 
}

FooterMobile.propTypes = {
    style: PropTypes.any,
SubtilteoneText0: PropTypes.any,
TextLinkTxt0: PropTypes.any,
TextLinkTxt1: PropTypes.any,
TextLinkTxt2: PropTypes.any,
SubtilteoneText1: PropTypes.any,
TextLinkTxt3: PropTypes.any,
TextLinkTxt4: PropTypes.any,
TextLinkTxt5: PropTypes.any,
TextLinkTxt6: PropTypes.any,
TextLinkTxt7: PropTypes.any,
TextLinkTxt8: PropTypes.any,
SubtilteoneText2: PropTypes.any,
TextLinkTxt9: PropTypes.any,
TextLinkTxt10: PropTypes.any,
SubtitletwooText0: PropTypes.any,
SubtitletwooText1: PropTypes.any,
SubtitletwooText2: PropTypes.any,
SubtitletwooText3: PropTypes.any,
SubtitletwooText4: PropTypes.any,
SubtitletwooText5: PropTypes.any,
SubtitletwooText6: PropTypes.any,
ParagraphoneText0: PropTypes.any,
LanguageButtonText0: PropTypes.any,
FooterMobileonClick: PropTypes.any,
FooterMobileonMouseEnter: PropTypes.any,
FooterMobileonMouseOver: PropTypes.any,
FooterMobileonKeyPress: PropTypes.any,
FooterMobileonDrag: PropTypes.any,
FooterMobileonMouseLeave: PropTypes.any,
FooterMobileonMouseUp: PropTypes.any,
FooterMobileonMouseDown: PropTypes.any,
FooterMobileonKeyDown: PropTypes.any,
FooterMobileonChange: PropTypes.any,
FooterMobileondelay: PropTypes.any,
FooterDataMobileonClick: PropTypes.any,
FooterDataMobileonMouseEnter: PropTypes.any,
FooterDataMobileonMouseOver: PropTypes.any,
FooterDataMobileonKeyPress: PropTypes.any,
FooterDataMobileonDrag: PropTypes.any,
FooterDataMobileonMouseLeave: PropTypes.any,
FooterDataMobileonMouseUp: PropTypes.any,
FooterDataMobileonMouseDown: PropTypes.any,
FooterDataMobileonKeyDown: PropTypes.any,
FooterDataMobileonChange: PropTypes.any,
FooterDataMobileondelay: PropTypes.any,
FooterDataMobileCompanyonClick: PropTypes.any,
FooterDataMobileCompanyonMouseEnter: PropTypes.any,
FooterDataMobileCompanyonMouseOver: PropTypes.any,
FooterDataMobileCompanyonKeyPress: PropTypes.any,
FooterDataMobileCompanyonDrag: PropTypes.any,
FooterDataMobileCompanyonMouseLeave: PropTypes.any,
FooterDataMobileCompanyonMouseUp: PropTypes.any,
FooterDataMobileCompanyonMouseDown: PropTypes.any,
FooterDataMobileCompanyonKeyDown: PropTypes.any,
FooterDataMobileCompanyonChange: PropTypes.any,
FooterDataMobileCompanyondelay: PropTypes.any,
FooterLinksCompanyonClick: PropTypes.any,
FooterLinksCompanyonMouseEnter: PropTypes.any,
FooterLinksCompanyonMouseOver: PropTypes.any,
FooterLinksCompanyonKeyPress: PropTypes.any,
FooterLinksCompanyonDrag: PropTypes.any,
FooterLinksCompanyonMouseLeave: PropTypes.any,
FooterLinksCompanyonMouseUp: PropTypes.any,
FooterLinksCompanyonMouseDown: PropTypes.any,
FooterLinksCompanyonKeyDown: PropTypes.any,
FooterLinksCompanyonChange: PropTypes.any,
FooterLinksCompanyondelay: PropTypes.any,
FooterDataMobileCommunityonClick: PropTypes.any,
FooterDataMobileCommunityonMouseEnter: PropTypes.any,
FooterDataMobileCommunityonMouseOver: PropTypes.any,
FooterDataMobileCommunityonKeyPress: PropTypes.any,
FooterDataMobileCommunityonDrag: PropTypes.any,
FooterDataMobileCommunityonMouseLeave: PropTypes.any,
FooterDataMobileCommunityonMouseUp: PropTypes.any,
FooterDataMobileCommunityonMouseDown: PropTypes.any,
FooterDataMobileCommunityonKeyDown: PropTypes.any,
FooterDataMobileCommunityonChange: PropTypes.any,
FooterDataMobileCommunityondelay: PropTypes.any,
FooterDataMobileUtilityonClick: PropTypes.any,
FooterDataMobileUtilityonMouseEnter: PropTypes.any,
FooterDataMobileUtilityonMouseOver: PropTypes.any,
FooterDataMobileUtilityonKeyPress: PropTypes.any,
FooterDataMobileUtilityonDrag: PropTypes.any,
FooterDataMobileUtilityonMouseLeave: PropTypes.any,
FooterDataMobileUtilityonMouseUp: PropTypes.any,
FooterDataMobileUtilityonMouseDown: PropTypes.any,
FooterDataMobileUtilityonKeyDown: PropTypes.any,
FooterDataMobileUtilityonChange: PropTypes.any,
FooterDataMobileUtilityondelay: PropTypes.any,
FooterLinksUtilityonClick: PropTypes.any,
FooterLinksUtilityonMouseEnter: PropTypes.any,
FooterLinksUtilityonMouseOver: PropTypes.any,
FooterLinksUtilityonKeyPress: PropTypes.any,
FooterLinksUtilityonDrag: PropTypes.any,
FooterLinksUtilityonMouseLeave: PropTypes.any,
FooterLinksUtilityonMouseUp: PropTypes.any,
FooterLinksUtilityonMouseDown: PropTypes.any,
FooterLinksUtilityonKeyDown: PropTypes.any,
FooterLinksUtilityonChange: PropTypes.any,
FooterLinksUtilityondelay: PropTypes.any,
FooterDataMobileSocialMediaonClick: PropTypes.any,
FooterDataMobileSocialMediaonMouseEnter: PropTypes.any,
FooterDataMobileSocialMediaonMouseOver: PropTypes.any,
FooterDataMobileSocialMediaonKeyPress: PropTypes.any,
FooterDataMobileSocialMediaonDrag: PropTypes.any,
FooterDataMobileSocialMediaonMouseLeave: PropTypes.any,
FooterDataMobileSocialMediaonMouseUp: PropTypes.any,
FooterDataMobileSocialMediaonMouseDown: PropTypes.any,
FooterDataMobileSocialMediaonKeyDown: PropTypes.any,
FooterDataMobileSocialMediaonChange: PropTypes.any,
FooterDataMobileSocialMediaondelay: PropTypes.any,
FooterDataMobileSocialMediaContonClick: PropTypes.any,
FooterDataMobileSocialMediaContonMouseEnter: PropTypes.any,
FooterDataMobileSocialMediaContonMouseOver: PropTypes.any,
FooterDataMobileSocialMediaContonKeyPress: PropTypes.any,
FooterDataMobileSocialMediaContonDrag: PropTypes.any,
FooterDataMobileSocialMediaContonMouseLeave: PropTypes.any,
FooterDataMobileSocialMediaContonMouseUp: PropTypes.any,
FooterDataMobileSocialMediaContonMouseDown: PropTypes.any,
FooterDataMobileSocialMediaContonKeyDown: PropTypes.any,
FooterDataMobileSocialMediaContonChange: PropTypes.any,
FooterDataMobileSocialMediaContondelay: PropTypes.any,
MenuLegacyMobileButtononClick: PropTypes.any,
MenuLegacyMobileButtononMouseEnter: PropTypes.any,
MenuLegacyMobileButtononMouseOver: PropTypes.any,
MenuLegacyMobileButtononKeyPress: PropTypes.any,
MenuLegacyMobileButtononDrag: PropTypes.any,
MenuLegacyMobileButtononMouseLeave: PropTypes.any,
MenuLegacyMobileButtononMouseUp: PropTypes.any,
MenuLegacyMobileButtononMouseDown: PropTypes.any,
MenuLegacyMobileButtononKeyDown: PropTypes.any,
MenuLegacyMobileButtononChange: PropTypes.any,
MenuLegacyMobileButtonondelay: PropTypes.any,
MenuLegacyMobileonClick: PropTypes.any,
MenuLegacyMobileonMouseEnter: PropTypes.any,
MenuLegacyMobileonMouseOver: PropTypes.any,
MenuLegacyMobileonKeyPress: PropTypes.any,
MenuLegacyMobileonDrag: PropTypes.any,
MenuLegacyMobileonMouseLeave: PropTypes.any,
MenuLegacyMobileonMouseUp: PropTypes.any,
MenuLegacyMobileonMouseDown: PropTypes.any,
MenuLegacyMobileonKeyDown: PropTypes.any,
MenuLegacyMobileonChange: PropTypes.any,
MenuLegacyMobileondelay: PropTypes.any,
MenuLegacyMobileOneonClick: PropTypes.any,
MenuLegacyMobileOneonMouseEnter: PropTypes.any,
MenuLegacyMobileOneonMouseOver: PropTypes.any,
MenuLegacyMobileOneonKeyPress: PropTypes.any,
MenuLegacyMobileOneonDrag: PropTypes.any,
MenuLegacyMobileOneonMouseLeave: PropTypes.any,
MenuLegacyMobileOneonMouseUp: PropTypes.any,
MenuLegacyMobileOneonMouseDown: PropTypes.any,
MenuLegacyMobileOneonKeyDown: PropTypes.any,
MenuLegacyMobileOneonChange: PropTypes.any,
MenuLegacyMobileOneondelay: PropTypes.any,
MenuLegacyMobileTwoonClick: PropTypes.any,
MenuLegacyMobileTwoonMouseEnter: PropTypes.any,
MenuLegacyMobileTwoonMouseOver: PropTypes.any,
MenuLegacyMobileTwoonKeyPress: PropTypes.any,
MenuLegacyMobileTwoonDrag: PropTypes.any,
MenuLegacyMobileTwoonMouseLeave: PropTypes.any,
MenuLegacyMobileTwoonMouseUp: PropTypes.any,
MenuLegacyMobileTwoonMouseDown: PropTypes.any,
MenuLegacyMobileTwoonKeyDown: PropTypes.any,
MenuLegacyMobileTwoonChange: PropTypes.any,
MenuLegacyMobileTwoondelay: PropTypes.any
}
export default FooterMobile;