import React, { Suspense } from 'react';
import PropTypes from 'prop-types';
import * as Contexts  from 'Contexts';
import { OverlaySwapContext } from 'Components/OverlaySwapProvider';
import CorebooksAnimation from 'Components/CorebooksAnimation';
import CorebooksAnimationSingle from 'Components/CorebooksAnimationSingle';

import * as UtilsScripts  from 'utils/utils';
import { Route, useHistory, useParams } from "react-router-dom";
import SingletoneNavigation from 'Class/SingletoneNavigation';

import LoadingComponent from "Components/Loading";


import { Transition } from "react-transition-group";
import { CSSTransition } from "react-transition-group";
import HFive from 'Components/HFive'
import { useAppContext, useSessionContext } from 'context/AppContext';
import './MenuLibraryPropertyoneDefault.css'





const PropertyoneDefault = (props)=>{
    const [state,dispatch] = useAppContext();
    const [, setNameSelectedComponent] = React.useContext(OverlaySwapContext);
    
    const nodeRef = React.useRef(null);
    const history = useHistory();
    let RouterContext = useParams();
    
    
    
    
    
    
    const [globalStateForm, dispatchForm] = React.useState({});
    
    const [ _in, setIn ] = React.useState(false)
    const [ transaction, setTransaction ] = React.useState({}) 
    const [ animation, setAnimation ] = React.useState('')

    
    
    
    
    
    
    React.useEffect(()=>{
        
        
        
    },[]);
    
    
    return (
        <>
  <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['propertyonedefault']?.animationClass || {}}>

    <div id="id_onesixzero_fourtwoofourfour" ref={nodeRef} className={` ${ props.onClick ? 'cursor' : '' } propertyonedefault C_onesixzero_fourtwoofourfour ${ props.cssClass } ${ transaction['propertyonedefault']?.type ? transaction['propertyonedefault']?.type.toLowerCase() : '' }`} style={ { ...{ ...{}, transitionDuration: transaction['propertyonedefault']?.duration, transitionTimingFunction: transaction['propertyonedefault']?.timingFunction }, ...props.style }} onClick={ props.MenuLibraryonClick } onMouseEnter={ props.MenuLibraryonMouseEnter } onMouseOver={ props.MenuLibraryonMouseOver } onKeyPress={ props.MenuLibraryonKeyPress } onDrag={ props.MenuLibraryonDrag } onMouseLeave={ props.MenuLibraryonMouseLeave } onMouseUp={ props.MenuLibraryonMouseUp } onMouseDown={ props.MenuLibraryonMouseDown } onKeyDown={ props.MenuLibraryonKeyDown } onChange={ props.MenuLibraryonChange } ondelay={ props.MenuLibraryondelay }>
      {
      props.children ?
      props.children :
      <>
        <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['menuhomeicon']?.animationClass || {}}>
          <img id="id_onesixzero_fourtwoofourfive" className={` rectangle menuhomeicon ${ props.onClick ? 'cursor' : '' } ${ transaction['menuhomeicon']?.type ? transaction['menuhomeicon']?.type.toLowerCase() : '' }`} style={{ ...{},  ...props.MenuHomeIconStyle , transitionDuration: transaction['menuhomeicon']?.duration, transitionTimingFunction: transaction['menuhomeicon']?.timingFunction }} onClick={ props.MenuHomeIcononClick } onMouseEnter={ props.MenuHomeIcononMouseEnter } onMouseOver={ props.MenuHomeIcononMouseOver } onKeyPress={ props.MenuHomeIcononKeyPress } onDrag={ props.MenuHomeIcononDrag } onMouseLeave={ props.MenuHomeIcononMouseLeave } onMouseUp={ props.MenuHomeIcononMouseUp } onMouseDown={ props.MenuHomeIcononMouseDown } onKeyDown={ props.MenuHomeIcononKeyDown } onChange={ props.MenuHomeIcononChange } ondelay={ props.MenuHomeIconondelay } src={props.MenuHomeIcon0 || "https://cdn.uncodie.com/tACQBrKCV97e0EqPE1XNLJ/e83cf65d7b7ebf95dd515c7960ce9ab5007a8e44.png" } />
        </CSSTransition>
        <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['hfive']?.animationClass || {}}>
          <HFive { ...{ ...props, style:false } } HfiveText0={ props.HfiveText0 || "Tu biblioteca" } HfiveText0={ props.HfiveText0 || "Tu biblioteca" } HfiveTextStyle={{"fontSize":"0.875rem"}} cssClass={"C_onesixzero_fourtwoofoursix "}  />
    </CSSTransition >
            
            </>
        }
        </div>
    
    </CSSTransition >
            </>
        
    ) 
}

PropertyoneDefault.propTypes = {
    style: PropTypes.any,
MenuHomeIcon0: PropTypes.any,
HfiveText0: PropTypes.any,
MenuLibraryonClick: PropTypes.any,
MenuLibraryonMouseEnter: PropTypes.any,
MenuLibraryonMouseOver: PropTypes.any,
MenuLibraryonKeyPress: PropTypes.any,
MenuLibraryonDrag: PropTypes.any,
MenuLibraryonMouseLeave: PropTypes.any,
MenuLibraryonMouseUp: PropTypes.any,
MenuLibraryonMouseDown: PropTypes.any,
MenuLibraryonKeyDown: PropTypes.any,
MenuLibraryonChange: PropTypes.any,
MenuLibraryondelay: PropTypes.any,
MenuHomeIcononClick: PropTypes.any,
MenuHomeIcononMouseEnter: PropTypes.any,
MenuHomeIcononMouseOver: PropTypes.any,
MenuHomeIcononKeyPress: PropTypes.any,
MenuHomeIcononDrag: PropTypes.any,
MenuHomeIcononMouseLeave: PropTypes.any,
MenuHomeIcononMouseUp: PropTypes.any,
MenuHomeIcononMouseDown: PropTypes.any,
MenuHomeIcononKeyDown: PropTypes.any,
MenuHomeIcononChange: PropTypes.any,
MenuHomeIconondelay: PropTypes.any
}
export default PropertyoneDefault;