import React, { Suspense } from 'react';
import PropTypes from 'prop-types';
import * as Contexts  from 'Contexts';
import { OverlaySwapContext } from 'Components/OverlaySwapProvider';
import CorebooksAnimation from 'Components/CorebooksAnimation';
import CorebooksAnimationSingle from 'Components/CorebooksAnimationSingle';

import * as UtilsScripts  from 'utils/utils';
import { Route, useHistory, useParams } from "react-router-dom";
import SingletoneNavigation from 'Class/SingletoneNavigation';

import LoadingComponent from "Components/Loading";


import { Transition } from "react-transition-group";
import { CSSTransition } from "react-transition-group";
import { useAppContext, useSessionContext } from 'context/AppContext';
import './ColorCard.css'





const ColorCard = (props)=>{
    const [state,dispatch] = useAppContext();
    const [, setNameSelectedComponent] = React.useContext(OverlaySwapContext);
    
    const nodeRef = React.useRef(null);
    const history = useHistory();
    let RouterContext = useParams();
    
    
    
    
    
    
    const [globalStateForm, dispatchForm] = React.useState({});
    
    const [ _in, setIn ] = React.useState(false)
    const [ transaction, setTransaction ] = React.useState({}) 
    const [ animation, setAnimation ] = React.useState('')

    
    
    
    
    
    
    React.useEffect(()=>{
        
        
        
    },[]);
    
    
    return (
        <>
  <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['colorcard']?.animationClass || {}}>

    <div id="id_fourthreeeight_sevenonethreeeight" ref={nodeRef} className={` ${ props.onClick ? 'cursor' : '' } colorcard C_fourthreeeight_sevenonethreeeight ${ props.cssClass } ${ transaction['colorcard']?.type ? transaction['colorcard']?.type.toLowerCase() : '' }`} style={ { ...{ ...{}, transitionDuration: transaction['colorcard']?.duration, transitionTimingFunction: transaction['colorcard']?.timingFunction }, ...props.style }} onClick={ props.ColorCardonClick } onMouseEnter={ props.ColorCardonMouseEnter } onMouseOver={ props.ColorCardonMouseOver } onKeyPress={ props.ColorCardonKeyPress } onDrag={ props.ColorCardonDrag } onMouseLeave={ props.ColorCardonMouseLeave } onMouseUp={ props.ColorCardonMouseUp } onMouseDown={ props.ColorCardonMouseDown } onKeyDown={ props.ColorCardonKeyDown } onChange={ props.ColorCardonChange } ondelay={ props.ColorCardondelay }>
      {
      props.children ?
      props.children :
      <>
        <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['colordata']?.animationClass || {}}>

          <div id="id_fourthreeeight_sevenonefourzero" className={` frame colordata ${ props.onClick ? 'cursor' : '' } ${ transaction['colordata']?.type ? transaction['colordata']?.type.toLowerCase() : '' }`} style={ { ...{}, ...props.ColorDataStyle , transitionDuration: transaction['colordata']?.duration, transitionTimingFunction: transaction['colordata']?.timingFunction } } onClick={ props.ColorDataonClick } onMouseEnter={ props.ColorDataonMouseEnter } onMouseOver={ props.ColorDataonMouseOver } onKeyPress={ props.ColorDataonKeyPress } onDrag={ props.ColorDataonDrag } onMouseLeave={ props.ColorDataonMouseLeave } onMouseUp={ props.ColorDataonMouseUp } onMouseDown={ props.ColorDataonMouseDown } onKeyDown={ props.ColorDataonKeyDown } onChange={ props.ColorDataonChange } ondelay={ props.ColorDataondelay }>
            <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['colordatatitle']?.animationClass || {}}>

              <span id="id_fourthreeeight_sevenonefourone"  className={` text colordatatitle    ${ props.onClick ? 'cursor' : ''}  ${ transaction['colordatatitle']?.type ? transaction['colordatatitle']?.type.toLowerCase() : '' }`} style={{ ...{},  ...props.ColorDataTitleStyle , transitionDuration: transaction['colordatatitle']?.duration, transitionTimingFunction: transaction['colordatatitle']?.timingFunction }} onClick={ props.ColorDataTitleonClick } onMouseEnter={ props.ColorDataTitleonMouseEnter } onMouseOver={ props.ColorDataTitleonMouseOver } onKeyPress={ props.ColorDataTitleonKeyPress } onDrag={ props.ColorDataTitleonDrag } onMouseLeave={ props.ColorDataTitleonMouseLeave } onMouseUp={ props.ColorDataTitleonMouseUp } onMouseDown={ props.ColorDataTitleonMouseDown } onKeyDown={ props.ColorDataTitleonKeyDown } onChange={ props.ColorDataTitleonChange } ondelay={ props.ColorDataTitleondelay } >{props.ColorDataTitle0 || `Base Background`}</span>

            </CSSTransition>
            <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['colorhexcont']?.animationClass || {}}>

              <div id="id_fourthreeeight_sevenonefourtwoo" className={` frame colorhexcont ${ props.onClick ? 'cursor' : '' } ${ transaction['colorhexcont']?.type ? transaction['colorhexcont']?.type.toLowerCase() : '' }`} style={ { ...{}, ...props.ColorHexContStyle , transitionDuration: transaction['colorhexcont']?.duration, transitionTimingFunction: transaction['colorhexcont']?.timingFunction } } onClick={ props.ColorHexContonClick } onMouseEnter={ props.ColorHexContonMouseEnter } onMouseOver={ props.ColorHexContonMouseOver } onKeyPress={ props.ColorHexContonKeyPress } onDrag={ props.ColorHexContonDrag } onMouseLeave={ props.ColorHexContonMouseLeave } onMouseUp={ props.ColorHexContonMouseUp } onMouseDown={ props.ColorHexContonMouseDown } onKeyDown={ props.ColorHexContonKeyDown } onChange={ props.ColorHexContonChange } ondelay={ props.ColorHexContondelay }>
                <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['colorhashtag']?.animationClass || {}}>

                  <span id="id_fourthreeeight_sevenonefourthree"  className={` text colorhashtag    ${ props.onClick ? 'cursor' : ''}  ${ transaction['colorhashtag']?.type ? transaction['colorhashtag']?.type.toLowerCase() : '' }`} style={{ ...{},  ...props.ColorHashtagStyle , transitionDuration: transaction['colorhashtag']?.duration, transitionTimingFunction: transaction['colorhashtag']?.timingFunction }} onClick={ props.ColorHashtagonClick } onMouseEnter={ props.ColorHashtagonMouseEnter } onMouseOver={ props.ColorHashtagonMouseOver } onKeyPress={ props.ColorHashtagonKeyPress } onDrag={ props.ColorHashtagonDrag } onMouseLeave={ props.ColorHashtagonMouseLeave } onMouseUp={ props.ColorHashtagonMouseUp } onMouseDown={ props.ColorHashtagonMouseDown } onKeyDown={ props.ColorHashtagonKeyDown } onChange={ props.ColorHashtagonChange } ondelay={ props.ColorHashtagondelay } >{props.ColorHashtag0 || `#`}</span>

                </CSSTransition>
                <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['colorhextext']?.animationClass || {}}>

                  <span id="id_fourthreeeight_sevenonefourfour"  className={` text colorhextext    ${ props.onClick ? 'cursor' : ''}  ${ transaction['colorhextext']?.type ? transaction['colorhextext']?.type.toLowerCase() : '' }`} style={{ ...{},  ...props.ColorHexTextStyle , transitionDuration: transaction['colorhextext']?.duration, transitionTimingFunction: transaction['colorhextext']?.timingFunction }} onClick={ props.ColorHexTextonClick } onMouseEnter={ props.ColorHexTextonMouseEnter } onMouseOver={ props.ColorHexTextonMouseOver } onKeyPress={ props.ColorHexTextonKeyPress } onDrag={ props.ColorHexTextonDrag } onMouseLeave={ props.ColorHexTextonMouseLeave } onMouseUp={ props.ColorHexTextonMouseUp } onMouseDown={ props.ColorHexTextonMouseDown } onKeyDown={ props.ColorHexTextonKeyDown } onChange={ props.ColorHexTextonChange } ondelay={ props.ColorHexTextondelay } >{props.ColorHexText0 || `000000`}</span>

                </CSSTransition>
              </div>

            </CSSTransition>
          </div>

        </CSSTransition>
        <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['colorcardblock']?.animationClass || {}}>
          <svg id="id_fourthreeeight_sevenonefourfive" className="colorcardblock  " style={{}} onClick={ props.ColorCardBlockonClick } onMouseEnter={ props.ColorCardBlockonMouseEnter } onMouseOver={ props.ColorCardBlockonMouseOver } onKeyPress={ props.ColorCardBlockonKeyPress } onDrag={ props.ColorCardBlockonDrag } onMouseLeave={ props.ColorCardBlockonMouseLeave } onMouseUp={ props.ColorCardBlockonMouseUp } onMouseDown={ props.ColorCardBlockonMouseDown } onKeyDown={ props.ColorCardBlockonKeyDown } onChange={ props.ColorCardBlockonChange } ondelay={ props.ColorCardBlockondelay } width="280" height="210.837890625">
            <path d="M0 24C0 10.7452 10.7452 0 24 0L256 0C269.255 0 280 10.7452 280 24L280 210.838L0 210.838L0 24Z" />
          </svg>
        </CSSTransition>

      </>
      }
    </div>

  </CSSTransition>
</>
    ) 
}

ColorCard.propTypes = {
    style: PropTypes.any,
ColorDataTitle0: PropTypes.any,
ColorHashtag0: PropTypes.any,
ColorHexText0: PropTypes.any,
ColorCardonClick: PropTypes.any,
ColorCardonMouseEnter: PropTypes.any,
ColorCardonMouseOver: PropTypes.any,
ColorCardonKeyPress: PropTypes.any,
ColorCardonDrag: PropTypes.any,
ColorCardonMouseLeave: PropTypes.any,
ColorCardonMouseUp: PropTypes.any,
ColorCardonMouseDown: PropTypes.any,
ColorCardonKeyDown: PropTypes.any,
ColorCardonChange: PropTypes.any,
ColorCardondelay: PropTypes.any,
ColorDataonClick: PropTypes.any,
ColorDataonMouseEnter: PropTypes.any,
ColorDataonMouseOver: PropTypes.any,
ColorDataonKeyPress: PropTypes.any,
ColorDataonDrag: PropTypes.any,
ColorDataonMouseLeave: PropTypes.any,
ColorDataonMouseUp: PropTypes.any,
ColorDataonMouseDown: PropTypes.any,
ColorDataonKeyDown: PropTypes.any,
ColorDataonChange: PropTypes.any,
ColorDataondelay: PropTypes.any,
ColorDataTitleonClick: PropTypes.any,
ColorDataTitleonMouseEnter: PropTypes.any,
ColorDataTitleonMouseOver: PropTypes.any,
ColorDataTitleonKeyPress: PropTypes.any,
ColorDataTitleonDrag: PropTypes.any,
ColorDataTitleonMouseLeave: PropTypes.any,
ColorDataTitleonMouseUp: PropTypes.any,
ColorDataTitleonMouseDown: PropTypes.any,
ColorDataTitleonKeyDown: PropTypes.any,
ColorDataTitleonChange: PropTypes.any,
ColorDataTitleondelay: PropTypes.any,
ColorHexContonClick: PropTypes.any,
ColorHexContonMouseEnter: PropTypes.any,
ColorHexContonMouseOver: PropTypes.any,
ColorHexContonKeyPress: PropTypes.any,
ColorHexContonDrag: PropTypes.any,
ColorHexContonMouseLeave: PropTypes.any,
ColorHexContonMouseUp: PropTypes.any,
ColorHexContonMouseDown: PropTypes.any,
ColorHexContonKeyDown: PropTypes.any,
ColorHexContonChange: PropTypes.any,
ColorHexContondelay: PropTypes.any,
ColorHashtagonClick: PropTypes.any,
ColorHashtagonMouseEnter: PropTypes.any,
ColorHashtagonMouseOver: PropTypes.any,
ColorHashtagonKeyPress: PropTypes.any,
ColorHashtagonDrag: PropTypes.any,
ColorHashtagonMouseLeave: PropTypes.any,
ColorHashtagonMouseUp: PropTypes.any,
ColorHashtagonMouseDown: PropTypes.any,
ColorHashtagonKeyDown: PropTypes.any,
ColorHashtagonChange: PropTypes.any,
ColorHashtagondelay: PropTypes.any,
ColorHexTextonClick: PropTypes.any,
ColorHexTextonMouseEnter: PropTypes.any,
ColorHexTextonMouseOver: PropTypes.any,
ColorHexTextonKeyPress: PropTypes.any,
ColorHexTextonDrag: PropTypes.any,
ColorHexTextonMouseLeave: PropTypes.any,
ColorHexTextonMouseUp: PropTypes.any,
ColorHexTextonMouseDown: PropTypes.any,
ColorHexTextonKeyDown: PropTypes.any,
ColorHexTextonChange: PropTypes.any,
ColorHexTextondelay: PropTypes.any,
ColorCardBlockonClick: PropTypes.any,
ColorCardBlockonMouseEnter: PropTypes.any,
ColorCardBlockonMouseOver: PropTypes.any,
ColorCardBlockonKeyPress: PropTypes.any,
ColorCardBlockonDrag: PropTypes.any,
ColorCardBlockonMouseLeave: PropTypes.any,
ColorCardBlockonMouseUp: PropTypes.any,
ColorCardBlockonMouseDown: PropTypes.any,
ColorCardBlockonKeyDown: PropTypes.any,
ColorCardBlockonChange: PropTypes.any,
ColorCardBlockondelay: PropTypes.any
}
export default ColorCard;