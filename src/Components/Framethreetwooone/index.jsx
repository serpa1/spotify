import React, { Suspense } from 'react';
import PropTypes from 'prop-types';
import * as Contexts  from 'Contexts';
import { OverlaySwapContext } from 'Components/OverlaySwapProvider';
import CorebooksAnimation from 'Components/CorebooksAnimation';
import CorebooksAnimationSingle from 'Components/CorebooksAnimationSingle';

import * as UtilsScripts  from 'utils/utils';
import { Route, useHistory, useParams } from "react-router-dom";
import SingletoneNavigation from 'Class/SingletoneNavigation';

import LoadingComponent from "Components/Loading";


import { Transition } from "react-transition-group";
import { CSSTransition } from "react-transition-group";
import { useAppContext, useSessionContext } from 'context/AppContext';
import './Framethreetwooone.css'





const Framethreetwooone = (props)=>{
    const [state,dispatch] = useAppContext();
    const [, setNameSelectedComponent] = React.useContext(OverlaySwapContext);
    
    const nodeRef = React.useRef(null);
    const history = useHistory();
    let RouterContext = useParams();
    
    
    
    
    
    
    const [globalStateForm, dispatchForm] = React.useState({});
    
    const [ _in, setIn ] = React.useState(false)
    const [ transaction, setTransaction ] = React.useState({}) 
    const [ animation, setAnimation ] = React.useState('')

    
    
    
    
    
    
    React.useEffect(()=>{
        
        
        
    },[]);
    
    
    return (
        <>
  <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['framethreetwooone']?.animationClass || {}}>

    <div id="id_threeonefour_foureightzerozero" ref={nodeRef} className={` ${ props.onClick ? 'cursor' : '' } framethreetwooone C_threeonefour_foureightzerozero ${ props.cssClass } ${ transaction['framethreetwooone']?.type ? transaction['framethreetwooone']?.type.toLowerCase() : '' }`} style={ { ...{ ...{}, transitionDuration: transaction['framethreetwooone']?.duration, transitionTimingFunction: transaction['framethreetwooone']?.timingFunction }, ...props.style }} onClick={ props.FramethreetwoooneonClick } onMouseEnter={ props.FramethreetwoooneonMouseEnter } onMouseOver={ props.FramethreetwoooneonMouseOver } onKeyPress={ props.FramethreetwoooneonKeyPress } onDrag={ props.FramethreetwoooneonDrag } onMouseLeave={ props.FramethreetwoooneonMouseLeave } onMouseUp={ props.FramethreetwoooneonMouseUp } onMouseDown={ props.FramethreetwoooneonMouseDown } onKeyDown={ props.FramethreetwoooneonKeyDown } onChange={ props.FramethreetwoooneonChange } ondelay={ props.Framethreetwoooneondelay }>
      {
      props.children ?
      props.children :
      <>
        <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['framethreetwoozero']?.animationClass || {}}>

          <div id="id_threeonefour_threesevenfivefour" className={` frame framethreetwoozero ${ props.onClick ? 'cursor' : '' } ${ transaction['framethreetwoozero']?.type ? transaction['framethreetwoozero']?.type.toLowerCase() : '' }`} style={ { ...{}, ...props.FramethreetwoozeroStyle , transitionDuration: transaction['framethreetwoozero']?.duration, transitionTimingFunction: transaction['framethreetwoozero']?.timingFunction } } onClick={ props.FramethreetwoozeroonClick } onMouseEnter={ props.FramethreetwoozeroonMouseEnter } onMouseOver={ props.FramethreetwoozeroonMouseOver } onKeyPress={ props.FramethreetwoozeroonKeyPress } onDrag={ props.FramethreetwoozeroonDrag } onMouseLeave={ props.FramethreetwoozeroonMouseLeave } onMouseUp={ props.FramethreetwoozeroonMouseUp } onMouseDown={ props.FramethreetwoozeroonMouseDown } onKeyDown={ props.FramethreetwoozeroonKeyDown } onChange={ props.FramethreetwoozeroonChange } ondelay={ props.Framethreetwoozeroondelay }>
            <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['ayudaconlospagos']?.animationClass || {}}>

              <span id="id_threeonefour_threesixzeronigth"  className={` text ayudaconlospagos    ${ props.onClick ? 'cursor' : ''}  ${ transaction['ayudaconlospagos']?.type ? transaction['ayudaconlospagos']?.type.toLowerCase() : '' }`} style={{ ...{},  ...props.AyudaconlospagosStyle , transitionDuration: transaction['ayudaconlospagos']?.duration, transitionTimingFunction: transaction['ayudaconlospagos']?.timingFunction }} onClick={ props.AyudaconlospagosonClick } onMouseEnter={ props.AyudaconlospagosonMouseEnter } onMouseOver={ props.AyudaconlospagosonMouseOver } onKeyPress={ props.AyudaconlospagosonKeyPress } onDrag={ props.AyudaconlospagosonDrag } onMouseLeave={ props.AyudaconlospagosonMouseLeave } onMouseUp={ props.AyudaconlospagosonMouseUp } onMouseDown={ props.AyudaconlospagosonMouseDown } onKeyDown={ props.AyudaconlospagosonKeyDown } onChange={ props.AyudaconlospagosonChange } ondelay={ props.Ayudaconlospagosondelay } >{props.Ayudaconlospagos0 || `Ayuda con los pagos`}</span>

            </CSSTransition>
            <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['image']?.animationClass || {}}>
              <img id="id_threeonefour_threesixzeroeight" className={` rectangle image ${ props.onClick ? 'cursor' : '' } ${ transaction['image']?.type ? transaction['image']?.type.toLowerCase() : '' }`} style={{ ...{},  ...props.IMAGEStyle , transitionDuration: transaction['image']?.duration, transitionTimingFunction: transaction['image']?.timingFunction }} onClick={ props.IMAGEonClick } onMouseEnter={ props.IMAGEonMouseEnter } onMouseOver={ props.IMAGEonMouseOver } onKeyPress={ props.IMAGEonKeyPress } onDrag={ props.IMAGEonDrag } onMouseLeave={ props.IMAGEonMouseLeave } onMouseUp={ props.IMAGEonMouseUp } onMouseDown={ props.IMAGEonMouseDown } onKeyDown={ props.IMAGEonKeyDown } onChange={ props.IMAGEonChange } ondelay={ props.IMAGEondelay } src={props.IMAGE0 || "https://cdn.uncodie.com/tACQBrKCV97e0EqPE1XNLJ/538f23b6d773c5ae33b611a8e521636cbf128491.png" } />
            </CSSTransition>
          </div>

        </CSSTransition>

      </>
      }
    </div>

  </CSSTransition>
</>
    ) 
}

Framethreetwooone.propTypes = {
    style: PropTypes.any,
Ayudaconlospagos0: PropTypes.any,
IMAGE0: PropTypes.any,
FramethreetwoooneonClick: PropTypes.any,
FramethreetwoooneonMouseEnter: PropTypes.any,
FramethreetwoooneonMouseOver: PropTypes.any,
FramethreetwoooneonKeyPress: PropTypes.any,
FramethreetwoooneonDrag: PropTypes.any,
FramethreetwoooneonMouseLeave: PropTypes.any,
FramethreetwoooneonMouseUp: PropTypes.any,
FramethreetwoooneonMouseDown: PropTypes.any,
FramethreetwoooneonKeyDown: PropTypes.any,
FramethreetwoooneonChange: PropTypes.any,
Framethreetwoooneondelay: PropTypes.any,
FramethreetwoozeroonClick: PropTypes.any,
FramethreetwoozeroonMouseEnter: PropTypes.any,
FramethreetwoozeroonMouseOver: PropTypes.any,
FramethreetwoozeroonKeyPress: PropTypes.any,
FramethreetwoozeroonDrag: PropTypes.any,
FramethreetwoozeroonMouseLeave: PropTypes.any,
FramethreetwoozeroonMouseUp: PropTypes.any,
FramethreetwoozeroonMouseDown: PropTypes.any,
FramethreetwoozeroonKeyDown: PropTypes.any,
FramethreetwoozeroonChange: PropTypes.any,
Framethreetwoozeroondelay: PropTypes.any,
AyudaconlospagosonClick: PropTypes.any,
AyudaconlospagosonMouseEnter: PropTypes.any,
AyudaconlospagosonMouseOver: PropTypes.any,
AyudaconlospagosonKeyPress: PropTypes.any,
AyudaconlospagosonDrag: PropTypes.any,
AyudaconlospagosonMouseLeave: PropTypes.any,
AyudaconlospagosonMouseUp: PropTypes.any,
AyudaconlospagosonMouseDown: PropTypes.any,
AyudaconlospagosonKeyDown: PropTypes.any,
AyudaconlospagosonChange: PropTypes.any,
Ayudaconlospagosondelay: PropTypes.any,
IMAGEonClick: PropTypes.any,
IMAGEonMouseEnter: PropTypes.any,
IMAGEonMouseOver: PropTypes.any,
IMAGEonKeyPress: PropTypes.any,
IMAGEonDrag: PropTypes.any,
IMAGEonMouseLeave: PropTypes.any,
IMAGEonMouseUp: PropTypes.any,
IMAGEonMouseDown: PropTypes.any,
IMAGEonKeyDown: PropTypes.any,
IMAGEonChange: PropTypes.any,
IMAGEondelay: PropTypes.any
}
export default Framethreetwooone;