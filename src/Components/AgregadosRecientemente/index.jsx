import React, { Suspense } from 'react';
import PropTypes from 'prop-types';
import * as Contexts  from 'Contexts';
import { OverlaySwapContext } from 'Components/OverlaySwapProvider';
import CorebooksAnimation from 'Components/CorebooksAnimation';
import CorebooksAnimationSingle from 'Components/CorebooksAnimationSingle';

import * as UtilsScripts  from 'utils/utils';
import { Route, useHistory, useParams } from "react-router-dom";
import SingletoneNavigation from 'Class/SingletoneNavigation';

import LoadingComponent from "Components/Loading";


import { Transition } from "react-transition-group";
import { CSSTransition } from "react-transition-group";
import { useAppContext, useSessionContext } from 'context/AppContext';
import './Agregadosrecientemente.css'





const Agregadosrecientemente = (props)=>{
    const [state,dispatch] = useAppContext();
    const [, setNameSelectedComponent] = React.useContext(OverlaySwapContext);
    
    const nodeRef = React.useRef(null);
    const history = useHistory();
    let RouterContext = useParams();
    
    
    
    
    
    
    const [globalStateForm, dispatchForm] = React.useState({});
    
    const [ _in, setIn ] = React.useState(false)
    const [ transaction, setTransaction ] = React.useState({}) 
    const [ animation, setAnimation ] = React.useState('')

    
    
    
    
    
    
    React.useEffect(()=>{
        
        
        
    },[]);
    
    
    return (
        <>
  <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['agregadosrecientemente']?.animationClass || {}}>

    <div id="id_fourthreeeight_fiveonethreeone" ref={nodeRef} className={` ${ props.onClick ? 'cursor' : '' } agregadosrecientemente ${ props.cssClass } ${ transaction['agregadosrecientemente']?.type ? transaction['agregadosrecientemente']?.type.toLowerCase() : '' }`} style={ { ...{ ...{}, transitionDuration: transaction['agregadosrecientemente']?.duration, transitionTimingFunction: transaction['agregadosrecientemente']?.timingFunction }, ...props.style }} onClick={ props.AgregadosrecientementeonClick } onMouseEnter={ props.AgregadosrecientementeonMouseEnter } onMouseOver={ props.AgregadosrecientementeonMouseOver } onKeyPress={ props.AgregadosrecientementeonKeyPress } onDrag={ props.AgregadosrecientementeonDrag } onMouseLeave={ props.AgregadosrecientementeonMouseLeave } onMouseUp={ props.AgregadosrecientementeonMouseUp } onMouseDown={ props.AgregadosrecientementeonMouseDown } onKeyDown={ props.AgregadosrecientementeonKeyDown } onChange={ props.AgregadosrecientementeonChange } ondelay={ props.Agregadosrecientementeondelay }>
      {
      props.children ?
      props.children :
      <>


      </>
      }
    </div>

  </CSSTransition>
</>
    ) 
}

Agregadosrecientemente.propTypes = {
    style: PropTypes.any,
AgregadosrecientementeonClick: PropTypes.any,
AgregadosrecientementeonMouseEnter: PropTypes.any,
AgregadosrecientementeonMouseOver: PropTypes.any,
AgregadosrecientementeonKeyPress: PropTypes.any,
AgregadosrecientementeonDrag: PropTypes.any,
AgregadosrecientementeonMouseLeave: PropTypes.any,
AgregadosrecientementeonMouseUp: PropTypes.any,
AgregadosrecientementeonMouseDown: PropTypes.any,
AgregadosrecientementeonKeyDown: PropTypes.any,
AgregadosrecientementeonChange: PropTypes.any,
Agregadosrecientementeondelay: PropTypes.any
}
export default Agregadosrecientemente;