import React, { Suspense } from 'react';
import PropTypes from 'prop-types';
import * as Contexts  from 'Contexts';
import { OverlaySwapContext } from 'Components/OverlaySwapProvider';
import CorebooksAnimation from 'Components/CorebooksAnimation';
import CorebooksAnimationSingle from 'Components/CorebooksAnimationSingle';

import * as UtilsScripts  from 'utils/utils';
import { Route, useHistory, useParams } from "react-router-dom";
import SingletoneNavigation from 'Class/SingletoneNavigation';

import LoadingComponent from "Components/Loading";


import { Transition } from "react-transition-group";
import { CSSTransition } from "react-transition-group";
import { useAppContext, useSessionContext } from 'context/AppContext';
import './Framethreefourzero.css'





const Framethreefourzero = (props)=>{
    const [state,dispatch] = useAppContext();
    const [, setNameSelectedComponent] = React.useContext(OverlaySwapContext);
    
    const nodeRef = React.useRef(null);
    const history = useHistory();
    let RouterContext = useParams();
    
    
    
    
    
    
    const [globalStateForm, dispatchForm] = React.useState({});
    
    const [ _in, setIn ] = React.useState(false)
    const [ transaction, setTransaction ] = React.useState({}) 
    const [ animation, setAnimation ] = React.useState('')

    
    
    
    
    
    
    React.useEffect(()=>{
        
        
        
    },[]);
    
    
    return (
        <>
  <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['framethreefourzero']?.animationClass || {}}>

    <div id="id_fourthreefour_oneonefourseven" ref={nodeRef} className={` ${ props.onClick ? 'cursor' : '' } framethreefourzero C_fourthreefour_oneonefourseven ${ props.cssClass } ${ transaction['framethreefourzero']?.type ? transaction['framethreefourzero']?.type.toLowerCase() : '' }`} style={ { ...{ ...{}, transitionDuration: transaction['framethreefourzero']?.duration, transitionTimingFunction: transaction['framethreefourzero']?.timingFunction }, ...props.style }} onClick={ props.FramethreefourzeroonClick } onMouseEnter={ props.FramethreefourzeroonMouseEnter } onMouseOver={ props.FramethreefourzeroonMouseOver } onKeyPress={ props.FramethreefourzeroonKeyPress } onDrag={ props.FramethreefourzeroonDrag } onMouseLeave={ props.FramethreefourzeroonMouseLeave } onMouseUp={ props.FramethreefourzeroonMouseUp } onMouseDown={ props.FramethreefourzeroonMouseDown } onKeyDown={ props.FramethreefourzeroonKeyDown } onChange={ props.FramethreefourzeroonChange } ondelay={ props.Framethreefourzeroondelay }>
      {
      props.children ?
      props.children :
      <>
        <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['honetext']?.animationClass || {}}>

          <span id="id_fourthreefour_oneonefoursix"  className={` text honetext    ${ props.onClick ? 'cursor' : ''}  ${ transaction['honetext']?.type ? transaction['honetext']?.type.toLowerCase() : '' }`} style={{ ...{},  ...props.HoneTextStyle , transitionDuration: transaction['honetext']?.duration, transitionTimingFunction: transaction['honetext']?.timingFunction }} onClick={ props.HoneTextonClick } onMouseEnter={ props.HoneTextonMouseEnter } onMouseOver={ props.HoneTextonMouseOver } onKeyPress={ props.HoneTextonKeyPress } onDrag={ props.HoneTextonDrag } onMouseLeave={ props.HoneTextonMouseLeave } onMouseUp={ props.HoneTextonMouseUp } onMouseDown={ props.HoneTextonMouseDown } onKeyDown={ props.HoneTextonKeyDown } onChange={ props.HoneTextonChange } ondelay={ props.HoneTextondelay } >{props.HoneText0 || `Visita nuestra Comunidad`}</span>

        </CSSTransition>

      </>
      }
    </div>

  </CSSTransition>
</>
    ) 
}

Framethreefourzero.propTypes = {
    style: PropTypes.any,
HoneText0: PropTypes.any,
FramethreefourzeroonClick: PropTypes.any,
FramethreefourzeroonMouseEnter: PropTypes.any,
FramethreefourzeroonMouseOver: PropTypes.any,
FramethreefourzeroonKeyPress: PropTypes.any,
FramethreefourzeroonDrag: PropTypes.any,
FramethreefourzeroonMouseLeave: PropTypes.any,
FramethreefourzeroonMouseUp: PropTypes.any,
FramethreefourzeroonMouseDown: PropTypes.any,
FramethreefourzeroonKeyDown: PropTypes.any,
FramethreefourzeroonChange: PropTypes.any,
Framethreefourzeroondelay: PropTypes.any,
HoneTextonClick: PropTypes.any,
HoneTextonMouseEnter: PropTypes.any,
HoneTextonMouseOver: PropTypes.any,
HoneTextonKeyPress: PropTypes.any,
HoneTextonDrag: PropTypes.any,
HoneTextonMouseLeave: PropTypes.any,
HoneTextonMouseUp: PropTypes.any,
HoneTextonMouseDown: PropTypes.any,
HoneTextonKeyDown: PropTypes.any,
HoneTextonChange: PropTypes.any,
HoneTextondelay: PropTypes.any
}
export default Framethreefourzero;