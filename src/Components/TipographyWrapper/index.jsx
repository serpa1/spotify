import React, { Suspense } from 'react';
import PropTypes from 'prop-types';
import * as Contexts  from 'Contexts';
import { OverlaySwapContext } from 'Components/OverlaySwapProvider';
import CorebooksAnimation from 'Components/CorebooksAnimation';
import CorebooksAnimationSingle from 'Components/CorebooksAnimationSingle';

import * as UtilsScripts  from 'utils/utils';
import { Route, useHistory, useParams } from "react-router-dom";
import SingletoneNavigation from 'Class/SingletoneNavigation';

import LoadingComponent from "Components/Loading";


import { Transition } from "react-transition-group";
import { CSSTransition } from "react-transition-group";
import Displayfive from 'Components/Displayfive'
import Displayfour from 'Components/Displayfour'
import Displaythree from 'Components/Displaythree'
import Displaytwoo from 'Components/Displaytwoo'
import Displayone from 'Components/Displayone'
import { useAppContext, useSessionContext } from 'context/AppContext';
import './TipographyWrapper.css'





const TipographyWrapper = (props)=>{
    const [state,dispatch] = useAppContext();
    const [, setNameSelectedComponent] = React.useContext(OverlaySwapContext);
    
    const nodeRef = React.useRef(null);
    const history = useHistory();
    let RouterContext = useParams();
    
    
    
    
    
    
    const [globalStateForm, dispatchForm] = React.useState({});
    
    const [ _in, setIn ] = React.useState(false)
    const [ transaction, setTransaction ] = React.useState({}) 
    const [ animation, setAnimation ] = React.useState('')

    
    
    
    
    
    
    React.useEffect(()=>{
        
        
        
    },[]);
    
    
    return (
        <>
  <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['tipographywrapper']?.animationClass || {}}>

    <div id="id_sixtwoozero_onesevenonenigth" ref={nodeRef} className={` ${ props.onClick ? 'cursor' : '' } tipographywrapper ${ props.cssClass } ${ transaction['tipographywrapper']?.type ? transaction['tipographywrapper']?.type.toLowerCase() : '' }`} style={ { ...{ ...{}, transitionDuration: transaction['tipographywrapper']?.duration, transitionTimingFunction: transaction['tipographywrapper']?.timingFunction }, ...props.style }} onClick={ props.TipographyWrapperonClick } onMouseEnter={ props.TipographyWrapperonMouseEnter } onMouseOver={ props.TipographyWrapperonMouseOver } onKeyPress={ props.TipographyWrapperonKeyPress } onDrag={ props.TipographyWrapperonDrag } onMouseLeave={ props.TipographyWrapperonMouseLeave } onMouseUp={ props.TipographyWrapperonMouseUp } onMouseDown={ props.TipographyWrapperonMouseDown } onKeyDown={ props.TipographyWrapperonKeyDown } onChange={ props.TipographyWrapperonChange } ondelay={ props.TipographyWrapperondelay }>
      {
      props.children ?
      props.children :
      <>
        <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['specialheadings']?.animationClass || {}}>

          <div id="id_sixtwoozero_oneseventwoozero" className={` frame specialheadings ${ props.onClick ? 'cursor' : '' } ${ transaction['specialheadings']?.type ? transaction['specialheadings']?.type.toLowerCase() : '' }`} style={ { ...{}, ...props.SpecialHeadingsStyle , transitionDuration: transaction['specialheadings']?.duration, transitionTimingFunction: transaction['specialheadings']?.timingFunction } } onClick={ props.SpecialHeadingsonClick } onMouseEnter={ props.SpecialHeadingsonMouseEnter } onMouseOver={ props.SpecialHeadingsonMouseOver } onKeyPress={ props.SpecialHeadingsonKeyPress } onDrag={ props.SpecialHeadingsonDrag } onMouseLeave={ props.SpecialHeadingsonMouseLeave } onMouseUp={ props.SpecialHeadingsonMouseUp } onMouseDown={ props.SpecialHeadingsonMouseDown } onKeyDown={ props.SpecialHeadingsonKeyDown } onChange={ props.SpecialHeadingsonChange } ondelay={ props.SpecialHeadingsondelay }>
            <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['subheadings']?.animationClass || {}}>

              <span id="id_sixtwoozero_oneeightthreeone"  className={` text subheadings    ${ props.onClick ? 'cursor' : ''}  ${ transaction['subheadings']?.type ? transaction['subheadings']?.type.toLowerCase() : '' }`} style={{ ...{},  ...props.SubHeadingsStyle , transitionDuration: transaction['subheadings']?.duration, transitionTimingFunction: transaction['subheadings']?.timingFunction }} onClick={ props.SubHeadingsonClick } onMouseEnter={ props.SubHeadingsonMouseEnter } onMouseOver={ props.SubHeadingsonMouseOver } onKeyPress={ props.SubHeadingsonKeyPress } onDrag={ props.SubHeadingsonDrag } onMouseLeave={ props.SubHeadingsonMouseLeave } onMouseUp={ props.SubHeadingsonMouseUp } onMouseDown={ props.SubHeadingsonMouseDown } onKeyDown={ props.SubHeadingsonKeyDown } onChange={ props.SubHeadingsonChange } ondelay={ props.SubHeadingsondelay } >{props.SubHeadings0 || `Special headings`}</span>

            </CSSTransition>
            <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['divider']?.animationClass || {}}>

              <img id="id_sixtwoozero_oneeightthreezero" className={` line divider ${ props.onClick ? 'cursor' : '' } ${ transaction['divider']?.type ? transaction['divider']?.type.toLowerCase() : '' }`} style={{ ...{},  ...props.DividerStyle , transitionDuration: transaction['divider']?.duration, transitionTimingFunction: transaction['divider']?.timingFunction }} onClick={ props.DivideronClick } onMouseEnter={ props.DivideronMouseEnter } onMouseOver={ props.DivideronMouseOver } onKeyPress={ props.DivideronKeyPress } onDrag={ props.DivideronDrag } onMouseLeave={ props.DivideronMouseLeave } onMouseUp={ props.DivideronMouseUp } onMouseDown={ props.DivideronMouseDown } onKeyDown={ props.DivideronKeyDown } onChange={ props.DivideronChange } ondelay={ props.Dividerondelay } src={props.Divider0 || "" } />

            </CSSTransition>
            <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['displayonecont']?.animationClass || {}}>

              <div id="id_sixtwoosix_onesixfourthree" className={` frame displayonecont ${ props.onClick ? 'cursor' : '' } ${ transaction['displayonecont']?.type ? transaction['displayonecont']?.type.toLowerCase() : '' }`} style={ { ...{}, ...props.DisplayoneContStyle , transitionDuration: transaction['displayonecont']?.duration, transitionTimingFunction: transaction['displayonecont']?.timingFunction } } onClick={ props.DisplayoneContonClick } onMouseEnter={ props.DisplayoneContonMouseEnter } onMouseOver={ props.DisplayoneContonMouseOver } onKeyPress={ props.DisplayoneContonKeyPress } onDrag={ props.DisplayoneContonDrag } onMouseLeave={ props.DisplayoneContonMouseLeave } onMouseUp={ props.DisplayoneContonMouseUp } onMouseDown={ props.DisplayoneContonMouseDown } onKeyDown={ props.DisplayoneContonKeyDown } onChange={ props.DisplayoneContonChange } ondelay={ props.DisplayoneContondelay }>
                <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['displayonedata']?.animationClass || {}}>

                  <div id="id_sixtwoosix_onesixfourfour" className={` frame displayonedata ${ props.onClick ? 'cursor' : '' } ${ transaction['displayonedata']?.type ? transaction['displayonedata']?.type.toLowerCase() : '' }`} style={ { ...{}, ...props.DisplayoneDataStyle , transitionDuration: transaction['displayonedata']?.duration, transitionTimingFunction: transaction['displayonedata']?.timingFunction } } onClick={ props.DisplayoneDataonClick } onMouseEnter={ props.DisplayoneDataonMouseEnter } onMouseOver={ props.DisplayoneDataonMouseOver } onKeyPress={ props.DisplayoneDataonKeyPress } onDrag={ props.DisplayoneDataonDrag } onMouseLeave={ props.DisplayoneDataonMouseLeave } onMouseUp={ props.DisplayoneDataonMouseUp } onMouseDown={ props.DisplayoneDataonMouseDown } onKeyDown={ props.DisplayoneDataonKeyDown } onChange={ props.DisplayoneDataonChange } ondelay={ props.DisplayoneDataondelay }>
                    <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['displayonedatatitle']?.animationClass || {}}>

                      <div id="id_sixtwoosix_onesixfourfive" className={` frame displayonedatatitle ${ props.onClick ? 'cursor' : '' } ${ transaction['displayonedatatitle']?.type ? transaction['displayonedatatitle']?.type.toLowerCase() : '' }`} style={ { ...{}, ...props.DisplayoneDataTitleStyle , transitionDuration: transaction['displayonedatatitle']?.duration, transitionTimingFunction: transaction['displayonedatatitle']?.timingFunction } } onClick={ props.DisplayoneDataTitleonClick } onMouseEnter={ props.DisplayoneDataTitleonMouseEnter } onMouseOver={ props.DisplayoneDataTitleonMouseOver } onKeyPress={ props.DisplayoneDataTitleonKeyPress } onDrag={ props.DisplayoneDataTitleonDrag } onMouseLeave={ props.DisplayoneDataTitleonMouseLeave } onMouseUp={ props.DisplayoneDataTitleonMouseUp } onMouseDown={ props.DisplayoneDataTitleonMouseDown } onKeyDown={ props.DisplayoneDataTitleonKeyDown } onChange={ props.DisplayoneDataTitleonChange } ondelay={ props.DisplayoneDataTitleondelay }>
                        <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['displayonedatatitletext']?.animationClass || {}}>

                          <span id="id_sixtwoosix_onesixfoursix"  className={` text displayonedatatitletext    ${ props.onClick ? 'cursor' : ''}  ${ transaction['displayonedatatitletext']?.type ? transaction['displayonedatatitletext']?.type.toLowerCase() : '' }`} style={{ ...{},  ...props.DisplayoneDataTitleTextStyle , transitionDuration: transaction['displayonedatatitletext']?.duration, transitionTimingFunction: transaction['displayonedatatitletext']?.timingFunction }} onClick={ props.DisplayoneDataTitleTextonClick } onMouseEnter={ props.DisplayoneDataTitleTextonMouseEnter } onMouseOver={ props.DisplayoneDataTitleTextonMouseOver } onKeyPress={ props.DisplayoneDataTitleTextonKeyPress } onDrag={ props.DisplayoneDataTitleTextonDrag } onMouseLeave={ props.DisplayoneDataTitleTextonMouseLeave } onMouseUp={ props.DisplayoneDataTitleTextonMouseUp } onMouseDown={ props.DisplayoneDataTitleTextonMouseDown } onKeyDown={ props.DisplayoneDataTitleTextonKeyDown } onChange={ props.DisplayoneDataTitleTextonChange } ondelay={ props.DisplayoneDataTitleTextondelay } >{props.DisplayoneDataTitleText0 || `Display 1`}</span>

                        </CSSTransition>
                      </div>

                    </CSSTransition>
                    <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['displayonetipographyfamilysize']?.animationClass || {}}>

                      <div id="id_sixtwoosix_onesixfourseven" className={` frame displayonetipographyfamilysize ${ props.onClick ? 'cursor' : '' } ${ transaction['displayonetipographyfamilysize']?.type ? transaction['displayonetipographyfamilysize']?.type.toLowerCase() : '' }`} style={ { ...{}, ...props.DisplayoneTipographyFamilySizeStyle , transitionDuration: transaction['displayonetipographyfamilysize']?.duration, transitionTimingFunction: transaction['displayonetipographyfamilysize']?.timingFunction } } onClick={ props.DisplayoneTipographyFamilySizeonClick } onMouseEnter={ props.DisplayoneTipographyFamilySizeonMouseEnter } onMouseOver={ props.DisplayoneTipographyFamilySizeonMouseOver } onKeyPress={ props.DisplayoneTipographyFamilySizeonKeyPress } onDrag={ props.DisplayoneTipographyFamilySizeonDrag } onMouseLeave={ props.DisplayoneTipographyFamilySizeonMouseLeave } onMouseUp={ props.DisplayoneTipographyFamilySizeonMouseUp } onMouseDown={ props.DisplayoneTipographyFamilySizeonMouseDown } onKeyDown={ props.DisplayoneTipographyFamilySizeonKeyDown } onChange={ props.DisplayoneTipographyFamilySizeonChange } ondelay={ props.DisplayoneTipographyFamilySizeondelay }>
                        <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['displayonetipography']?.animationClass || {}}>

                          <div id="id_sixtwoosix_onesixfoureight" className={` frame displayonetipography ${ props.onClick ? 'cursor' : '' } ${ transaction['displayonetipography']?.type ? transaction['displayonetipography']?.type.toLowerCase() : '' }`} style={ { ...{}, ...props.DisplayoneTipographyStyle , transitionDuration: transaction['displayonetipography']?.duration, transitionTimingFunction: transaction['displayonetipography']?.timingFunction } } onClick={ props.DisplayoneTipographyonClick } onMouseEnter={ props.DisplayoneTipographyonMouseEnter } onMouseOver={ props.DisplayoneTipographyonMouseOver } onKeyPress={ props.DisplayoneTipographyonKeyPress } onDrag={ props.DisplayoneTipographyonDrag } onMouseLeave={ props.DisplayoneTipographyonMouseLeave } onMouseUp={ props.DisplayoneTipographyonMouseUp } onMouseDown={ props.DisplayoneTipographyonMouseDown } onKeyDown={ props.DisplayoneTipographyonKeyDown } onChange={ props.DisplayoneTipographyonChange } ondelay={ props.DisplayoneTipographyondelay }>
                            <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['displayonetipographyname']?.animationClass || {}}>

                              <span id="id_sixtwoosix_onesixfournigth"  className={` text displayonetipographyname    ${ props.onClick ? 'cursor' : ''}  ${ transaction['displayonetipographyname']?.type ? transaction['displayonetipographyname']?.type.toLowerCase() : '' }`} style={{ ...{},  ...props.DisplayoneTipographyNameStyle , transitionDuration: transaction['displayonetipographyname']?.duration, transitionTimingFunction: transaction['displayonetipographyname']?.timingFunction }} onClick={ props.DisplayoneTipographyNameonClick } onMouseEnter={ props.DisplayoneTipographyNameonMouseEnter } onMouseOver={ props.DisplayoneTipographyNameonMouseOver } onKeyPress={ props.DisplayoneTipographyNameonKeyPress } onDrag={ props.DisplayoneTipographyNameonDrag } onMouseLeave={ props.DisplayoneTipographyNameonMouseLeave } onMouseUp={ props.DisplayoneTipographyNameonMouseUp } onMouseDown={ props.DisplayoneTipographyNameonMouseDown } onKeyDown={ props.DisplayoneTipographyNameonKeyDown } onChange={ props.DisplayoneTipographyNameonChange } ondelay={ props.DisplayoneTipographyNameondelay } >{props.DisplayoneTipographyName0 || `Circular Std`}</span>

                            </CSSTransition>
                          </div>

                        </CSSTransition>
                        <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['displayonetipographysize']?.animationClass || {}}>

                          <div id="id_sixtwoosix_onesixfiveone" className={` frame displayonetipographysize ${ props.onClick ? 'cursor' : '' } ${ transaction['displayonetipographysize']?.type ? transaction['displayonetipographysize']?.type.toLowerCase() : '' }`} style={ { ...{}, ...props.DisplayoneTipographySizeStyle , transitionDuration: transaction['displayonetipographysize']?.duration, transitionTimingFunction: transaction['displayonetipographysize']?.timingFunction } } onClick={ props.DisplayoneTipographySizeonClick } onMouseEnter={ props.DisplayoneTipographySizeonMouseEnter } onMouseOver={ props.DisplayoneTipographySizeonMouseOver } onKeyPress={ props.DisplayoneTipographySizeonKeyPress } onDrag={ props.DisplayoneTipographySizeonDrag } onMouseLeave={ props.DisplayoneTipographySizeonMouseLeave } onMouseUp={ props.DisplayoneTipographySizeonMouseUp } onMouseDown={ props.DisplayoneTipographySizeonMouseDown } onKeyDown={ props.DisplayoneTipographySizeonKeyDown } onChange={ props.DisplayoneTipographySizeonChange } ondelay={ props.DisplayoneTipographySizeondelay }>
                            <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['displayonetipographysizetext']?.animationClass || {}}>

                              <span id="id_sixtwoosix_onesixfivetwoo"  className={` text displayonetipographysizetext    ${ props.onClick ? 'cursor' : ''}  ${ transaction['displayonetipographysizetext']?.type ? transaction['displayonetipographysizetext']?.type.toLowerCase() : '' }`} style={{ ...{},  ...props.DisplayoneTipographySizeTextStyle , transitionDuration: transaction['displayonetipographysizetext']?.duration, transitionTimingFunction: transaction['displayonetipographysizetext']?.timingFunction }} onClick={ props.DisplayoneTipographySizeTextonClick } onMouseEnter={ props.DisplayoneTipographySizeTextonMouseEnter } onMouseOver={ props.DisplayoneTipographySizeTextonMouseOver } onKeyPress={ props.DisplayoneTipographySizeTextonKeyPress } onDrag={ props.DisplayoneTipographySizeTextonDrag } onMouseLeave={ props.DisplayoneTipographySizeTextonMouseLeave } onMouseUp={ props.DisplayoneTipographySizeTextonMouseUp } onMouseDown={ props.DisplayoneTipographySizeTextonMouseDown } onKeyDown={ props.DisplayoneTipographySizeTextonKeyDown } onChange={ props.DisplayoneTipographySizeTextonChange } ondelay={ props.DisplayoneTipographySizeTextondelay } >{props.DisplayoneTipographySizeText0 || `72px`}</span>

                            </CSSTransition>
                          </div>

                        </CSSTransition>
                      </div>

                    </CSSTransition>
                  </div>

                </CSSTransition>
                <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['displayone']?.animationClass || {}}>
                  <Displayone { ...{ ...props, style:false } } cssClass={"C_sixtwoosix_onesixfivenigth "}  />
    </CSSTransition >
                    </div>
               
    </CSSTransition >
<CSSTransition 
        in={_in}
        appear={true} 
        timeout={100}
        classNames={transaction['displaytwoocont']?.animationClass || {}}
    >
    
                    <div id="id_sixtwootwoo_onesixtwooeight" className={` frame displaytwoocont ${ props.onClick ? 'cursor' : '' } ${ transaction['displaytwoocont']?.type ? transaction['displaytwoocont']?.type.toLowerCase() : '' }`} style={ { ...{}, ...props.DisplaytwooContStyle , transitionDuration: transaction['displaytwoocont']?.duration, transitionTimingFunction: transaction['displaytwoocont']?.timingFunction } } onClick={ props.DisplaytwooContonClick } onMouseEnter={ props.DisplaytwooContonMouseEnter } onMouseOver={ props.DisplaytwooContonMouseOver } onKeyPress={ props.DisplaytwooContonKeyPress } onDrag={ props.DisplaytwooContonDrag } onMouseLeave={ props.DisplaytwooContonMouseLeave } onMouseUp={ props.DisplaytwooContonMouseUp } onMouseDown={ props.DisplaytwooContonMouseDown } onKeyDown={ props.DisplaytwooContonKeyDown } onChange={ props.DisplaytwooContonChange } ondelay={ props.DisplaytwooContondelay }>
                    <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['displaytwoodata']?.animationClass || {}}>

                      <div id="id_sixtwoozero_oneseventwoofour" className={` frame displaytwoodata ${ props.onClick ? 'cursor' : '' } ${ transaction['displaytwoodata']?.type ? transaction['displaytwoodata']?.type.toLowerCase() : '' }`} style={ { ...{}, ...props.DisplaytwooDataStyle , transitionDuration: transaction['displaytwoodata']?.duration, transitionTimingFunction: transaction['displaytwoodata']?.timingFunction } } onClick={ props.DisplaytwooDataonClick } onMouseEnter={ props.DisplaytwooDataonMouseEnter } onMouseOver={ props.DisplaytwooDataonMouseOver } onKeyPress={ props.DisplaytwooDataonKeyPress } onDrag={ props.DisplaytwooDataonDrag } onMouseLeave={ props.DisplaytwooDataonMouseLeave } onMouseUp={ props.DisplaytwooDataonMouseUp } onMouseDown={ props.DisplaytwooDataonMouseDown } onKeyDown={ props.DisplaytwooDataonKeyDown } onChange={ props.DisplaytwooDataonChange } ondelay={ props.DisplaytwooDataondelay }>
                        <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['displaytwoodatatitle']?.animationClass || {}}>

                          <div id="id_sixtwootwoo_onesixtwoonigth" className={` frame displaytwoodatatitle ${ props.onClick ? 'cursor' : '' } ${ transaction['displaytwoodatatitle']?.type ? transaction['displaytwoodatatitle']?.type.toLowerCase() : '' }`} style={ { ...{}, ...props.DisplaytwooDataTitleStyle , transitionDuration: transaction['displaytwoodatatitle']?.duration, transitionTimingFunction: transaction['displaytwoodatatitle']?.timingFunction } } onClick={ props.DisplaytwooDataTitleonClick } onMouseEnter={ props.DisplaytwooDataTitleonMouseEnter } onMouseOver={ props.DisplaytwooDataTitleonMouseOver } onKeyPress={ props.DisplaytwooDataTitleonKeyPress } onDrag={ props.DisplaytwooDataTitleonDrag } onMouseLeave={ props.DisplaytwooDataTitleonMouseLeave } onMouseUp={ props.DisplaytwooDataTitleonMouseUp } onMouseDown={ props.DisplaytwooDataTitleonMouseDown } onKeyDown={ props.DisplaytwooDataTitleonKeyDown } onChange={ props.DisplaytwooDataTitleonChange } ondelay={ props.DisplaytwooDataTitleondelay }>
                            <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['displaytwoodatatitletext']?.animationClass || {}}>

                              <span id="id_sixtwoozero_oneseventwoofive"  className={` text displaytwoodatatitletext    ${ props.onClick ? 'cursor' : ''}  ${ transaction['displaytwoodatatitletext']?.type ? transaction['displaytwoodatatitletext']?.type.toLowerCase() : '' }`} style={{ ...{},  ...props.DisplaytwooDataTitleTextStyle , transitionDuration: transaction['displaytwoodatatitletext']?.duration, transitionTimingFunction: transaction['displaytwoodatatitletext']?.timingFunction }} onClick={ props.DisplaytwooDataTitleTextonClick } onMouseEnter={ props.DisplaytwooDataTitleTextonMouseEnter } onMouseOver={ props.DisplaytwooDataTitleTextonMouseOver } onKeyPress={ props.DisplaytwooDataTitleTextonKeyPress } onDrag={ props.DisplaytwooDataTitleTextonDrag } onMouseLeave={ props.DisplaytwooDataTitleTextonMouseLeave } onMouseUp={ props.DisplaytwooDataTitleTextonMouseUp } onMouseDown={ props.DisplaytwooDataTitleTextonMouseDown } onKeyDown={ props.DisplaytwooDataTitleTextonKeyDown } onChange={ props.DisplaytwooDataTitleTextonChange } ondelay={ props.DisplaytwooDataTitleTextondelay } >{props.DisplaytwooDataTitleText0 || `Display 2`}</span>

                            </CSSTransition>
                          </div>

                        </CSSTransition>
                        <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['displaytwootipographyfamilysize']?.animationClass || {}}>

                          <div id="id_sixtwoozero_oneseventwoosix" className={` frame displaytwootipographyfamilysize ${ props.onClick ? 'cursor' : '' } ${ transaction['displaytwootipographyfamilysize']?.type ? transaction['displaytwootipographyfamilysize']?.type.toLowerCase() : '' }`} style={ { ...{}, ...props.DisplaytwooTipographyFamilySizeStyle , transitionDuration: transaction['displaytwootipographyfamilysize']?.duration, transitionTimingFunction: transaction['displaytwootipographyfamilysize']?.timingFunction } } onClick={ props.DisplaytwooTipographyFamilySizeonClick } onMouseEnter={ props.DisplaytwooTipographyFamilySizeonMouseEnter } onMouseOver={ props.DisplaytwooTipographyFamilySizeonMouseOver } onKeyPress={ props.DisplaytwooTipographyFamilySizeonKeyPress } onDrag={ props.DisplaytwooTipographyFamilySizeonDrag } onMouseLeave={ props.DisplaytwooTipographyFamilySizeonMouseLeave } onMouseUp={ props.DisplaytwooTipographyFamilySizeonMouseUp } onMouseDown={ props.DisplaytwooTipographyFamilySizeonMouseDown } onKeyDown={ props.DisplaytwooTipographyFamilySizeonKeyDown } onChange={ props.DisplaytwooTipographyFamilySizeonChange } ondelay={ props.DisplaytwooTipographyFamilySizeondelay }>
                            <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['displaytwootipography']?.animationClass || {}}>

                              <div id="id_sixtwoozero_oneseventwooseven" className={` frame displaytwootipography ${ props.onClick ? 'cursor' : '' } ${ transaction['displaytwootipography']?.type ? transaction['displaytwootipography']?.type.toLowerCase() : '' }`} style={ { ...{}, ...props.DisplaytwooTipographyStyle , transitionDuration: transaction['displaytwootipography']?.duration, transitionTimingFunction: transaction['displaytwootipography']?.timingFunction } } onClick={ props.DisplaytwooTipographyonClick } onMouseEnter={ props.DisplaytwooTipographyonMouseEnter } onMouseOver={ props.DisplaytwooTipographyonMouseOver } onKeyPress={ props.DisplaytwooTipographyonKeyPress } onDrag={ props.DisplaytwooTipographyonDrag } onMouseLeave={ props.DisplaytwooTipographyonMouseLeave } onMouseUp={ props.DisplaytwooTipographyonMouseUp } onMouseDown={ props.DisplaytwooTipographyonMouseDown } onKeyDown={ props.DisplaytwooTipographyonKeyDown } onChange={ props.DisplaytwooTipographyonChange } ondelay={ props.DisplaytwooTipographyondelay }>
                                <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['displaytwootipographyname']?.animationClass || {}}>

                                  <span id="id_sixtwoozero_oneseventwooeight"  className={` text displaytwootipographyname    ${ props.onClick ? 'cursor' : ''}  ${ transaction['displaytwootipographyname']?.type ? transaction['displaytwootipographyname']?.type.toLowerCase() : '' }`} style={{ ...{},  ...props.DisplaytwooTipographyNameStyle , transitionDuration: transaction['displaytwootipographyname']?.duration, transitionTimingFunction: transaction['displaytwootipographyname']?.timingFunction }} onClick={ props.DisplaytwooTipographyNameonClick } onMouseEnter={ props.DisplaytwooTipographyNameonMouseEnter } onMouseOver={ props.DisplaytwooTipographyNameonMouseOver } onKeyPress={ props.DisplaytwooTipographyNameonKeyPress } onDrag={ props.DisplaytwooTipographyNameonDrag } onMouseLeave={ props.DisplaytwooTipographyNameonMouseLeave } onMouseUp={ props.DisplaytwooTipographyNameonMouseUp } onMouseDown={ props.DisplaytwooTipographyNameonMouseDown } onKeyDown={ props.DisplaytwooTipographyNameonKeyDown } onChange={ props.DisplaytwooTipographyNameonChange } ondelay={ props.DisplaytwooTipographyNameondelay } >{props.DisplaytwooTipographyName0 || `Circular Std`}</span>

                                </CSSTransition>
                              </div>

                            </CSSTransition>
                            <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['displaytwootipographysize']?.animationClass || {}}>

                              <div id="id_sixtwoozero_oneseventhreezero" className={` frame displaytwootipographysize ${ props.onClick ? 'cursor' : '' } ${ transaction['displaytwootipographysize']?.type ? transaction['displaytwootipographysize']?.type.toLowerCase() : '' }`} style={ { ...{}, ...props.DisplaytwooTipographySizeStyle , transitionDuration: transaction['displaytwootipographysize']?.duration, transitionTimingFunction: transaction['displaytwootipographysize']?.timingFunction } } onClick={ props.DisplaytwooTipographySizeonClick } onMouseEnter={ props.DisplaytwooTipographySizeonMouseEnter } onMouseOver={ props.DisplaytwooTipographySizeonMouseOver } onKeyPress={ props.DisplaytwooTipographySizeonKeyPress } onDrag={ props.DisplaytwooTipographySizeonDrag } onMouseLeave={ props.DisplaytwooTipographySizeonMouseLeave } onMouseUp={ props.DisplaytwooTipographySizeonMouseUp } onMouseDown={ props.DisplaytwooTipographySizeonMouseDown } onKeyDown={ props.DisplaytwooTipographySizeonKeyDown } onChange={ props.DisplaytwooTipographySizeonChange } ondelay={ props.DisplaytwooTipographySizeondelay }>
                                <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['displaytwootipographysizetext']?.animationClass || {}}>

                                  <span id="id_sixtwoozero_oneseventhreeone"  className={` text displaytwootipographysizetext    ${ props.onClick ? 'cursor' : ''}  ${ transaction['displaytwootipographysizetext']?.type ? transaction['displaytwootipographysizetext']?.type.toLowerCase() : '' }`} style={{ ...{},  ...props.DisplaytwooTipographySizeTextStyle , transitionDuration: transaction['displaytwootipographysizetext']?.duration, transitionTimingFunction: transaction['displaytwootipographysizetext']?.timingFunction }} onClick={ props.DisplaytwooTipographySizeTextonClick } onMouseEnter={ props.DisplaytwooTipographySizeTextonMouseEnter } onMouseOver={ props.DisplaytwooTipographySizeTextonMouseOver } onKeyPress={ props.DisplaytwooTipographySizeTextonKeyPress } onDrag={ props.DisplaytwooTipographySizeTextonDrag } onMouseLeave={ props.DisplaytwooTipographySizeTextonMouseLeave } onMouseUp={ props.DisplaytwooTipographySizeTextonMouseUp } onMouseDown={ props.DisplaytwooTipographySizeTextonMouseDown } onKeyDown={ props.DisplaytwooTipographySizeTextonKeyDown } onChange={ props.DisplaytwooTipographySizeTextonChange } ondelay={ props.DisplaytwooTipographySizeTextondelay } >{props.DisplaytwooTipographySizeText0 || `45px`}</span>

                                </CSSTransition>
                              </div>

                            </CSSTransition>
                          </div>

                        </CSSTransition>
                      </div>

                    </CSSTransition>
                    <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['displaytwoo']?.animationClass || {}}>
                      <Displaytwoo { ...{ ...props, style:false } } cssClass={"C_sixtwoosix_onesixsixsix "}  />
    </CSSTransition >
                    </div>
               
    </CSSTransition >
<CSSTransition 
        in={_in}
        appear={true} 
        timeout={100}
        classNames={transaction['displaythreecont']?.animationClass || {}}
    >
    
                    <div id="id_sixtwoosix_onesixseventhree" className={` frame displaythreecont ${ props.onClick ? 'cursor' : '' } ${ transaction['displaythreecont']?.type ? transaction['displaythreecont']?.type.toLowerCase() : '' }`} style={ { ...{}, ...props.DisplaythreeContStyle , transitionDuration: transaction['displaythreecont']?.duration, transitionTimingFunction: transaction['displaythreecont']?.timingFunction } } onClick={ props.DisplaythreeContonClick } onMouseEnter={ props.DisplaythreeContonMouseEnter } onMouseOver={ props.DisplaythreeContonMouseOver } onKeyPress={ props.DisplaythreeContonKeyPress } onDrag={ props.DisplaythreeContonDrag } onMouseLeave={ props.DisplaythreeContonMouseLeave } onMouseUp={ props.DisplaythreeContonMouseUp } onMouseDown={ props.DisplaythreeContonMouseDown } onKeyDown={ props.DisplaythreeContonKeyDown } onChange={ props.DisplaythreeContonChange } ondelay={ props.DisplaythreeContondelay }>
                        <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['displaythreedata']?.animationClass || {}}>

                          <div id="id_sixtwoosix_onesixsevenfour" className={` frame displaythreedata ${ props.onClick ? 'cursor' : '' } ${ transaction['displaythreedata']?.type ? transaction['displaythreedata']?.type.toLowerCase() : '' }`} style={ { ...{}, ...props.DisplaythreeDataStyle , transitionDuration: transaction['displaythreedata']?.duration, transitionTimingFunction: transaction['displaythreedata']?.timingFunction } } onClick={ props.DisplaythreeDataonClick } onMouseEnter={ props.DisplaythreeDataonMouseEnter } onMouseOver={ props.DisplaythreeDataonMouseOver } onKeyPress={ props.DisplaythreeDataonKeyPress } onDrag={ props.DisplaythreeDataonDrag } onMouseLeave={ props.DisplaythreeDataonMouseLeave } onMouseUp={ props.DisplaythreeDataonMouseUp } onMouseDown={ props.DisplaythreeDataonMouseDown } onKeyDown={ props.DisplaythreeDataonKeyDown } onChange={ props.DisplaythreeDataonChange } ondelay={ props.DisplaythreeDataondelay }>
                            <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['displaythreedatatitle']?.animationClass || {}}>

                              <div id="id_sixtwoosix_onesixsevenfive" className={` frame displaythreedatatitle ${ props.onClick ? 'cursor' : '' } ${ transaction['displaythreedatatitle']?.type ? transaction['displaythreedatatitle']?.type.toLowerCase() : '' }`} style={ { ...{}, ...props.DisplaythreeDataTitleStyle , transitionDuration: transaction['displaythreedatatitle']?.duration, transitionTimingFunction: transaction['displaythreedatatitle']?.timingFunction } } onClick={ props.DisplaythreeDataTitleonClick } onMouseEnter={ props.DisplaythreeDataTitleonMouseEnter } onMouseOver={ props.DisplaythreeDataTitleonMouseOver } onKeyPress={ props.DisplaythreeDataTitleonKeyPress } onDrag={ props.DisplaythreeDataTitleonDrag } onMouseLeave={ props.DisplaythreeDataTitleonMouseLeave } onMouseUp={ props.DisplaythreeDataTitleonMouseUp } onMouseDown={ props.DisplaythreeDataTitleonMouseDown } onKeyDown={ props.DisplaythreeDataTitleonKeyDown } onChange={ props.DisplaythreeDataTitleonChange } ondelay={ props.DisplaythreeDataTitleondelay }>
                                <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['displaythreedatatitletext']?.animationClass || {}}>

                                  <span id="id_sixtwoosix_onesixsevensix"  className={` text displaythreedatatitletext    ${ props.onClick ? 'cursor' : ''}  ${ transaction['displaythreedatatitletext']?.type ? transaction['displaythreedatatitletext']?.type.toLowerCase() : '' }`} style={{ ...{},  ...props.DisplaythreeDataTitleTextStyle , transitionDuration: transaction['displaythreedatatitletext']?.duration, transitionTimingFunction: transaction['displaythreedatatitletext']?.timingFunction }} onClick={ props.DisplaythreeDataTitleTextonClick } onMouseEnter={ props.DisplaythreeDataTitleTextonMouseEnter } onMouseOver={ props.DisplaythreeDataTitleTextonMouseOver } onKeyPress={ props.DisplaythreeDataTitleTextonKeyPress } onDrag={ props.DisplaythreeDataTitleTextonDrag } onMouseLeave={ props.DisplaythreeDataTitleTextonMouseLeave } onMouseUp={ props.DisplaythreeDataTitleTextonMouseUp } onMouseDown={ props.DisplaythreeDataTitleTextonMouseDown } onKeyDown={ props.DisplaythreeDataTitleTextonKeyDown } onChange={ props.DisplaythreeDataTitleTextonChange } ondelay={ props.DisplaythreeDataTitleTextondelay } >{props.DisplaythreeDataTitleText0 || `Display 3`}</span>

                                </CSSTransition>
                              </div>

                            </CSSTransition>
                            <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['displaythreetipographyfamilysize']?.animationClass || {}}>

                              <div id="id_sixtwoosix_onesixsevenseven" className={` frame displaythreetipographyfamilysize ${ props.onClick ? 'cursor' : '' } ${ transaction['displaythreetipographyfamilysize']?.type ? transaction['displaythreetipographyfamilysize']?.type.toLowerCase() : '' }`} style={ { ...{}, ...props.DisplaythreeTipographyFamilySizeStyle , transitionDuration: transaction['displaythreetipographyfamilysize']?.duration, transitionTimingFunction: transaction['displaythreetipographyfamilysize']?.timingFunction } } onClick={ props.DisplaythreeTipographyFamilySizeonClick } onMouseEnter={ props.DisplaythreeTipographyFamilySizeonMouseEnter } onMouseOver={ props.DisplaythreeTipographyFamilySizeonMouseOver } onKeyPress={ props.DisplaythreeTipographyFamilySizeonKeyPress } onDrag={ props.DisplaythreeTipographyFamilySizeonDrag } onMouseLeave={ props.DisplaythreeTipographyFamilySizeonMouseLeave } onMouseUp={ props.DisplaythreeTipographyFamilySizeonMouseUp } onMouseDown={ props.DisplaythreeTipographyFamilySizeonMouseDown } onKeyDown={ props.DisplaythreeTipographyFamilySizeonKeyDown } onChange={ props.DisplaythreeTipographyFamilySizeonChange } ondelay={ props.DisplaythreeTipographyFamilySizeondelay }>
                                <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['displaythreetipography']?.animationClass || {}}>

                                  <div id="id_sixtwoosix_onesixseveneight" className={` frame displaythreetipography ${ props.onClick ? 'cursor' : '' } ${ transaction['displaythreetipography']?.type ? transaction['displaythreetipography']?.type.toLowerCase() : '' }`} style={ { ...{}, ...props.DisplaythreeTipographyStyle , transitionDuration: transaction['displaythreetipography']?.duration, transitionTimingFunction: transaction['displaythreetipography']?.timingFunction } } onClick={ props.DisplaythreeTipographyonClick } onMouseEnter={ props.DisplaythreeTipographyonMouseEnter } onMouseOver={ props.DisplaythreeTipographyonMouseOver } onKeyPress={ props.DisplaythreeTipographyonKeyPress } onDrag={ props.DisplaythreeTipographyonDrag } onMouseLeave={ props.DisplaythreeTipographyonMouseLeave } onMouseUp={ props.DisplaythreeTipographyonMouseUp } onMouseDown={ props.DisplaythreeTipographyonMouseDown } onKeyDown={ props.DisplaythreeTipographyonKeyDown } onChange={ props.DisplaythreeTipographyonChange } ondelay={ props.DisplaythreeTipographyondelay }>
                                    <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['displaythreetipographyname']?.animationClass || {}}>

                                      <span id="id_sixtwoosix_onesixsevennigth"  className={` text displaythreetipographyname    ${ props.onClick ? 'cursor' : ''}  ${ transaction['displaythreetipographyname']?.type ? transaction['displaythreetipographyname']?.type.toLowerCase() : '' }`} style={{ ...{},  ...props.DisplaythreeTipographyNameStyle , transitionDuration: transaction['displaythreetipographyname']?.duration, transitionTimingFunction: transaction['displaythreetipographyname']?.timingFunction }} onClick={ props.DisplaythreeTipographyNameonClick } onMouseEnter={ props.DisplaythreeTipographyNameonMouseEnter } onMouseOver={ props.DisplaythreeTipographyNameonMouseOver } onKeyPress={ props.DisplaythreeTipographyNameonKeyPress } onDrag={ props.DisplaythreeTipographyNameonDrag } onMouseLeave={ props.DisplaythreeTipographyNameonMouseLeave } onMouseUp={ props.DisplaythreeTipographyNameonMouseUp } onMouseDown={ props.DisplaythreeTipographyNameonMouseDown } onKeyDown={ props.DisplaythreeTipographyNameonKeyDown } onChange={ props.DisplaythreeTipographyNameonChange } ondelay={ props.DisplaythreeTipographyNameondelay } >{props.DisplaythreeTipographyName0 || `Circular Std`}</span>

                                    </CSSTransition>
                                  </div>

                                </CSSTransition>
                                <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['displaythreetipographysize']?.animationClass || {}}>

                                  <div id="id_sixtwoosix_onesixeightone" className={` frame displaythreetipographysize ${ props.onClick ? 'cursor' : '' } ${ transaction['displaythreetipographysize']?.type ? transaction['displaythreetipographysize']?.type.toLowerCase() : '' }`} style={ { ...{}, ...props.DisplaythreeTipographySizeStyle , transitionDuration: transaction['displaythreetipographysize']?.duration, transitionTimingFunction: transaction['displaythreetipographysize']?.timingFunction } } onClick={ props.DisplaythreeTipographySizeonClick } onMouseEnter={ props.DisplaythreeTipographySizeonMouseEnter } onMouseOver={ props.DisplaythreeTipographySizeonMouseOver } onKeyPress={ props.DisplaythreeTipographySizeonKeyPress } onDrag={ props.DisplaythreeTipographySizeonDrag } onMouseLeave={ props.DisplaythreeTipographySizeonMouseLeave } onMouseUp={ props.DisplaythreeTipographySizeonMouseUp } onMouseDown={ props.DisplaythreeTipographySizeonMouseDown } onKeyDown={ props.DisplaythreeTipographySizeonKeyDown } onChange={ props.DisplaythreeTipographySizeonChange } ondelay={ props.DisplaythreeTipographySizeondelay }>
                                    <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['displaythreetipographysizetext']?.animationClass || {}}>

                                      <span id="id_sixtwoosix_onesixeighttwoo"  className={` text displaythreetipographysizetext    ${ props.onClick ? 'cursor' : ''}  ${ transaction['displaythreetipographysizetext']?.type ? transaction['displaythreetipographysizetext']?.type.toLowerCase() : '' }`} style={{ ...{},  ...props.DisplaythreeTipographySizeTextStyle , transitionDuration: transaction['displaythreetipographysizetext']?.duration, transitionTimingFunction: transaction['displaythreetipographysizetext']?.timingFunction }} onClick={ props.DisplaythreeTipographySizeTextonClick } onMouseEnter={ props.DisplaythreeTipographySizeTextonMouseEnter } onMouseOver={ props.DisplaythreeTipographySizeTextonMouseOver } onKeyPress={ props.DisplaythreeTipographySizeTextonKeyPress } onDrag={ props.DisplaythreeTipographySizeTextonDrag } onMouseLeave={ props.DisplaythreeTipographySizeTextonMouseLeave } onMouseUp={ props.DisplaythreeTipographySizeTextonMouseUp } onMouseDown={ props.DisplaythreeTipographySizeTextonMouseDown } onKeyDown={ props.DisplaythreeTipographySizeTextonKeyDown } onChange={ props.DisplaythreeTipographySizeTextonChange } ondelay={ props.DisplaythreeTipographySizeTextondelay } >{props.DisplaythreeTipographySizeText0 || `40px`}</span>

                                    </CSSTransition>
                                  </div>

                                </CSSTransition>
                              </div>

                            </CSSTransition>
                          </div>

                        </CSSTransition>
                        <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['displaythree']?.animationClass || {}}>
                          <Displaythree { ...{ ...props, style:false } } cssClass={"C_sixtwoosix_onesevenoneone "}  />
    </CSSTransition >
                    </div>
               
    </CSSTransition >
<CSSTransition 
        in={_in}
        appear={true} 
        timeout={100}
        classNames={transaction['displayfourcont']?.animationClass || {}}
    >
    
                    <div id="id_sixtwoosix_oneseventwoozero" className={` frame displayfourcont ${ props.onClick ? 'cursor' : '' } ${ transaction['displayfourcont']?.type ? transaction['displayfourcont']?.type.toLowerCase() : '' }`} style={ { ...{}, ...props.DisplayfourContStyle , transitionDuration: transaction['displayfourcont']?.duration, transitionTimingFunction: transaction['displayfourcont']?.timingFunction } } onClick={ props.DisplayfourContonClick } onMouseEnter={ props.DisplayfourContonMouseEnter } onMouseOver={ props.DisplayfourContonMouseOver } onKeyPress={ props.DisplayfourContonKeyPress } onDrag={ props.DisplayfourContonDrag } onMouseLeave={ props.DisplayfourContonMouseLeave } onMouseUp={ props.DisplayfourContonMouseUp } onMouseDown={ props.DisplayfourContonMouseDown } onKeyDown={ props.DisplayfourContonKeyDown } onChange={ props.DisplayfourContonChange } ondelay={ props.DisplayfourContondelay }>
                            <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['displayfourdata']?.animationClass || {}}>

                              <div id="id_sixtwoosix_oneseventwooone" className={` frame displayfourdata ${ props.onClick ? 'cursor' : '' } ${ transaction['displayfourdata']?.type ? transaction['displayfourdata']?.type.toLowerCase() : '' }`} style={ { ...{}, ...props.DisplayfourDataStyle , transitionDuration: transaction['displayfourdata']?.duration, transitionTimingFunction: transaction['displayfourdata']?.timingFunction } } onClick={ props.DisplayfourDataonClick } onMouseEnter={ props.DisplayfourDataonMouseEnter } onMouseOver={ props.DisplayfourDataonMouseOver } onKeyPress={ props.DisplayfourDataonKeyPress } onDrag={ props.DisplayfourDataonDrag } onMouseLeave={ props.DisplayfourDataonMouseLeave } onMouseUp={ props.DisplayfourDataonMouseUp } onMouseDown={ props.DisplayfourDataonMouseDown } onKeyDown={ props.DisplayfourDataonKeyDown } onChange={ props.DisplayfourDataonChange } ondelay={ props.DisplayfourDataondelay }>
                                <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['displayfourdatatitle']?.animationClass || {}}>

                                  <div id="id_sixtwoosix_oneseventwootwoo" className={` frame displayfourdatatitle ${ props.onClick ? 'cursor' : '' } ${ transaction['displayfourdatatitle']?.type ? transaction['displayfourdatatitle']?.type.toLowerCase() : '' }`} style={ { ...{}, ...props.DisplayfourDataTitleStyle , transitionDuration: transaction['displayfourdatatitle']?.duration, transitionTimingFunction: transaction['displayfourdatatitle']?.timingFunction } } onClick={ props.DisplayfourDataTitleonClick } onMouseEnter={ props.DisplayfourDataTitleonMouseEnter } onMouseOver={ props.DisplayfourDataTitleonMouseOver } onKeyPress={ props.DisplayfourDataTitleonKeyPress } onDrag={ props.DisplayfourDataTitleonDrag } onMouseLeave={ props.DisplayfourDataTitleonMouseLeave } onMouseUp={ props.DisplayfourDataTitleonMouseUp } onMouseDown={ props.DisplayfourDataTitleonMouseDown } onKeyDown={ props.DisplayfourDataTitleonKeyDown } onChange={ props.DisplayfourDataTitleonChange } ondelay={ props.DisplayfourDataTitleondelay }>
                                    <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['displayfourdatatitletext']?.animationClass || {}}>

                                      <span id="id_sixtwoosix_oneseventwoothree"  className={` text displayfourdatatitletext    ${ props.onClick ? 'cursor' : ''}  ${ transaction['displayfourdatatitletext']?.type ? transaction['displayfourdatatitletext']?.type.toLowerCase() : '' }`} style={{ ...{},  ...props.DisplayfourDataTitleTextStyle , transitionDuration: transaction['displayfourdatatitletext']?.duration, transitionTimingFunction: transaction['displayfourdatatitletext']?.timingFunction }} onClick={ props.DisplayfourDataTitleTextonClick } onMouseEnter={ props.DisplayfourDataTitleTextonMouseEnter } onMouseOver={ props.DisplayfourDataTitleTextonMouseOver } onKeyPress={ props.DisplayfourDataTitleTextonKeyPress } onDrag={ props.DisplayfourDataTitleTextonDrag } onMouseLeave={ props.DisplayfourDataTitleTextonMouseLeave } onMouseUp={ props.DisplayfourDataTitleTextonMouseUp } onMouseDown={ props.DisplayfourDataTitleTextonMouseDown } onKeyDown={ props.DisplayfourDataTitleTextonKeyDown } onChange={ props.DisplayfourDataTitleTextonChange } ondelay={ props.DisplayfourDataTitleTextondelay } >{props.DisplayfourDataTitleText0 || `Display 4`}</span>

                                    </CSSTransition>
                                  </div>

                                </CSSTransition>
                                <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['displayfourtipographyfamilysize']?.animationClass || {}}>

                                  <div id="id_sixtwoosix_oneseventwoofour" className={` frame displayfourtipographyfamilysize ${ props.onClick ? 'cursor' : '' } ${ transaction['displayfourtipographyfamilysize']?.type ? transaction['displayfourtipographyfamilysize']?.type.toLowerCase() : '' }`} style={ { ...{}, ...props.DisplayfourTipographyFamilySizeStyle , transitionDuration: transaction['displayfourtipographyfamilysize']?.duration, transitionTimingFunction: transaction['displayfourtipographyfamilysize']?.timingFunction } } onClick={ props.DisplayfourTipographyFamilySizeonClick } onMouseEnter={ props.DisplayfourTipographyFamilySizeonMouseEnter } onMouseOver={ props.DisplayfourTipographyFamilySizeonMouseOver } onKeyPress={ props.DisplayfourTipographyFamilySizeonKeyPress } onDrag={ props.DisplayfourTipographyFamilySizeonDrag } onMouseLeave={ props.DisplayfourTipographyFamilySizeonMouseLeave } onMouseUp={ props.DisplayfourTipographyFamilySizeonMouseUp } onMouseDown={ props.DisplayfourTipographyFamilySizeonMouseDown } onKeyDown={ props.DisplayfourTipographyFamilySizeonKeyDown } onChange={ props.DisplayfourTipographyFamilySizeonChange } ondelay={ props.DisplayfourTipographyFamilySizeondelay }>
                                    <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['displayfourtipography']?.animationClass || {}}>

                                      <div id="id_sixtwoosix_oneseventwoofive" className={` frame displayfourtipography ${ props.onClick ? 'cursor' : '' } ${ transaction['displayfourtipography']?.type ? transaction['displayfourtipography']?.type.toLowerCase() : '' }`} style={ { ...{}, ...props.DisplayfourTipographyStyle , transitionDuration: transaction['displayfourtipography']?.duration, transitionTimingFunction: transaction['displayfourtipography']?.timingFunction } } onClick={ props.DisplayfourTipographyonClick } onMouseEnter={ props.DisplayfourTipographyonMouseEnter } onMouseOver={ props.DisplayfourTipographyonMouseOver } onKeyPress={ props.DisplayfourTipographyonKeyPress } onDrag={ props.DisplayfourTipographyonDrag } onMouseLeave={ props.DisplayfourTipographyonMouseLeave } onMouseUp={ props.DisplayfourTipographyonMouseUp } onMouseDown={ props.DisplayfourTipographyonMouseDown } onKeyDown={ props.DisplayfourTipographyonKeyDown } onChange={ props.DisplayfourTipographyonChange } ondelay={ props.DisplayfourTipographyondelay }>
                                        <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['displayfourtipographyname']?.animationClass || {}}>

                                          <span id="id_sixtwoosix_oneseventwoosix"  className={` text displayfourtipographyname    ${ props.onClick ? 'cursor' : ''}  ${ transaction['displayfourtipographyname']?.type ? transaction['displayfourtipographyname']?.type.toLowerCase() : '' }`} style={{ ...{},  ...props.DisplayfourTipographyNameStyle , transitionDuration: transaction['displayfourtipographyname']?.duration, transitionTimingFunction: transaction['displayfourtipographyname']?.timingFunction }} onClick={ props.DisplayfourTipographyNameonClick } onMouseEnter={ props.DisplayfourTipographyNameonMouseEnter } onMouseOver={ props.DisplayfourTipographyNameonMouseOver } onKeyPress={ props.DisplayfourTipographyNameonKeyPress } onDrag={ props.DisplayfourTipographyNameonDrag } onMouseLeave={ props.DisplayfourTipographyNameonMouseLeave } onMouseUp={ props.DisplayfourTipographyNameonMouseUp } onMouseDown={ props.DisplayfourTipographyNameonMouseDown } onKeyDown={ props.DisplayfourTipographyNameonKeyDown } onChange={ props.DisplayfourTipographyNameonChange } ondelay={ props.DisplayfourTipographyNameondelay } >{props.DisplayfourTipographyName0 || `Circular Std`}</span>

                                        </CSSTransition>
                                      </div>

                                    </CSSTransition>
                                    <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['displayfourtipographysize']?.animationClass || {}}>

                                      <div id="id_sixtwoosix_oneseventwooeight" className={` frame displayfourtipographysize ${ props.onClick ? 'cursor' : '' } ${ transaction['displayfourtipographysize']?.type ? transaction['displayfourtipographysize']?.type.toLowerCase() : '' }`} style={ { ...{}, ...props.DisplayfourTipographySizeStyle , transitionDuration: transaction['displayfourtipographysize']?.duration, transitionTimingFunction: transaction['displayfourtipographysize']?.timingFunction } } onClick={ props.DisplayfourTipographySizeonClick } onMouseEnter={ props.DisplayfourTipographySizeonMouseEnter } onMouseOver={ props.DisplayfourTipographySizeonMouseOver } onKeyPress={ props.DisplayfourTipographySizeonKeyPress } onDrag={ props.DisplayfourTipographySizeonDrag } onMouseLeave={ props.DisplayfourTipographySizeonMouseLeave } onMouseUp={ props.DisplayfourTipographySizeonMouseUp } onMouseDown={ props.DisplayfourTipographySizeonMouseDown } onKeyDown={ props.DisplayfourTipographySizeonKeyDown } onChange={ props.DisplayfourTipographySizeonChange } ondelay={ props.DisplayfourTipographySizeondelay }>
                                        <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['displayfourtipographysizetext']?.animationClass || {}}>

                                          <span id="id_sixtwoosix_oneseventwoonigth"  className={` text displayfourtipographysizetext    ${ props.onClick ? 'cursor' : ''}  ${ transaction['displayfourtipographysizetext']?.type ? transaction['displayfourtipographysizetext']?.type.toLowerCase() : '' }`} style={{ ...{},  ...props.DisplayfourTipographySizeTextStyle , transitionDuration: transaction['displayfourtipographysizetext']?.duration, transitionTimingFunction: transaction['displayfourtipographysizetext']?.timingFunction }} onClick={ props.DisplayfourTipographySizeTextonClick } onMouseEnter={ props.DisplayfourTipographySizeTextonMouseEnter } onMouseOver={ props.DisplayfourTipographySizeTextonMouseOver } onKeyPress={ props.DisplayfourTipographySizeTextonKeyPress } onDrag={ props.DisplayfourTipographySizeTextonDrag } onMouseLeave={ props.DisplayfourTipographySizeTextonMouseLeave } onMouseUp={ props.DisplayfourTipographySizeTextonMouseUp } onMouseDown={ props.DisplayfourTipographySizeTextonMouseDown } onKeyDown={ props.DisplayfourTipographySizeTextonKeyDown } onChange={ props.DisplayfourTipographySizeTextonChange } ondelay={ props.DisplayfourTipographySizeTextondelay } >{props.DisplayfourTipographySizeText0 || `30px`}</span>

                                        </CSSTransition>
                                      </div>

                                    </CSSTransition>
                                  </div>

                                </CSSTransition>
                              </div>

                            </CSSTransition>
                            <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['displayfour']?.animationClass || {}}>
                              <Displayfour { ...{ ...props, style:false } } cssClass={"C_sixtwoosix_oneseventhreefour "}  />
    </CSSTransition >
                    </div>
               
    </CSSTransition >
<CSSTransition 
        in={_in}
        appear={true} 
        timeout={100}
        classNames={transaction['displayfivecont']?.animationClass || {}}
    >
    
                    <div id="id_sixtwoosix_onesevenfourfive" className={` frame displayfivecont ${ props.onClick ? 'cursor' : '' } ${ transaction['displayfivecont']?.type ? transaction['displayfivecont']?.type.toLowerCase() : '' }`} style={ { ...{}, ...props.DisplayfiveContStyle , transitionDuration: transaction['displayfivecont']?.duration, transitionTimingFunction: transaction['displayfivecont']?.timingFunction } } onClick={ props.DisplayfiveContonClick } onMouseEnter={ props.DisplayfiveContonMouseEnter } onMouseOver={ props.DisplayfiveContonMouseOver } onKeyPress={ props.DisplayfiveContonKeyPress } onDrag={ props.DisplayfiveContonDrag } onMouseLeave={ props.DisplayfiveContonMouseLeave } onMouseUp={ props.DisplayfiveContonMouseUp } onMouseDown={ props.DisplayfiveContonMouseDown } onKeyDown={ props.DisplayfiveContonKeyDown } onChange={ props.DisplayfiveContonChange } ondelay={ props.DisplayfiveContondelay }>
                                <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['displayfivedata']?.animationClass || {}}>

                                  <div id="id_sixtwoosix_onesevenfoursix" className={` frame displayfivedata ${ props.onClick ? 'cursor' : '' } ${ transaction['displayfivedata']?.type ? transaction['displayfivedata']?.type.toLowerCase() : '' }`} style={ { ...{}, ...props.DisplayfiveDataStyle , transitionDuration: transaction['displayfivedata']?.duration, transitionTimingFunction: transaction['displayfivedata']?.timingFunction } } onClick={ props.DisplayfiveDataonClick } onMouseEnter={ props.DisplayfiveDataonMouseEnter } onMouseOver={ props.DisplayfiveDataonMouseOver } onKeyPress={ props.DisplayfiveDataonKeyPress } onDrag={ props.DisplayfiveDataonDrag } onMouseLeave={ props.DisplayfiveDataonMouseLeave } onMouseUp={ props.DisplayfiveDataonMouseUp } onMouseDown={ props.DisplayfiveDataonMouseDown } onKeyDown={ props.DisplayfiveDataonKeyDown } onChange={ props.DisplayfiveDataonChange } ondelay={ props.DisplayfiveDataondelay }>
                                    <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['displayfivedatatitle']?.animationClass || {}}>

                                      <div id="id_sixtwoosix_onesevenfourseven" className={` frame displayfivedatatitle ${ props.onClick ? 'cursor' : '' } ${ transaction['displayfivedatatitle']?.type ? transaction['displayfivedatatitle']?.type.toLowerCase() : '' }`} style={ { ...{}, ...props.DisplayfiveDataTitleStyle , transitionDuration: transaction['displayfivedatatitle']?.duration, transitionTimingFunction: transaction['displayfivedatatitle']?.timingFunction } } onClick={ props.DisplayfiveDataTitleonClick } onMouseEnter={ props.DisplayfiveDataTitleonMouseEnter } onMouseOver={ props.DisplayfiveDataTitleonMouseOver } onKeyPress={ props.DisplayfiveDataTitleonKeyPress } onDrag={ props.DisplayfiveDataTitleonDrag } onMouseLeave={ props.DisplayfiveDataTitleonMouseLeave } onMouseUp={ props.DisplayfiveDataTitleonMouseUp } onMouseDown={ props.DisplayfiveDataTitleonMouseDown } onKeyDown={ props.DisplayfiveDataTitleonKeyDown } onChange={ props.DisplayfiveDataTitleonChange } ondelay={ props.DisplayfiveDataTitleondelay }>
                                        <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['displayfivedatatitletext']?.animationClass || {}}>

                                          <span id="id_sixtwoosix_onesevenfoureight"  className={` text displayfivedatatitletext    ${ props.onClick ? 'cursor' : ''}  ${ transaction['displayfivedatatitletext']?.type ? transaction['displayfivedatatitletext']?.type.toLowerCase() : '' }`} style={{ ...{},  ...props.DisplayfiveDataTitleTextStyle , transitionDuration: transaction['displayfivedatatitletext']?.duration, transitionTimingFunction: transaction['displayfivedatatitletext']?.timingFunction }} onClick={ props.DisplayfiveDataTitleTextonClick } onMouseEnter={ props.DisplayfiveDataTitleTextonMouseEnter } onMouseOver={ props.DisplayfiveDataTitleTextonMouseOver } onKeyPress={ props.DisplayfiveDataTitleTextonKeyPress } onDrag={ props.DisplayfiveDataTitleTextonDrag } onMouseLeave={ props.DisplayfiveDataTitleTextonMouseLeave } onMouseUp={ props.DisplayfiveDataTitleTextonMouseUp } onMouseDown={ props.DisplayfiveDataTitleTextonMouseDown } onKeyDown={ props.DisplayfiveDataTitleTextonKeyDown } onChange={ props.DisplayfiveDataTitleTextonChange } ondelay={ props.DisplayfiveDataTitleTextondelay } >{props.DisplayfiveDataTitleText0 || `Display 5`}</span>

                                        </CSSTransition>
                                      </div>

                                    </CSSTransition>
                                    <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['displayfivetipographyfamilysize']?.animationClass || {}}>

                                      <div id="id_sixtwoosix_onesevenfournigth" className={` frame displayfivetipographyfamilysize ${ props.onClick ? 'cursor' : '' } ${ transaction['displayfivetipographyfamilysize']?.type ? transaction['displayfivetipographyfamilysize']?.type.toLowerCase() : '' }`} style={ { ...{}, ...props.DisplayfiveTipographyFamilySizeStyle , transitionDuration: transaction['displayfivetipographyfamilysize']?.duration, transitionTimingFunction: transaction['displayfivetipographyfamilysize']?.timingFunction } } onClick={ props.DisplayfiveTipographyFamilySizeonClick } onMouseEnter={ props.DisplayfiveTipographyFamilySizeonMouseEnter } onMouseOver={ props.DisplayfiveTipographyFamilySizeonMouseOver } onKeyPress={ props.DisplayfiveTipographyFamilySizeonKeyPress } onDrag={ props.DisplayfiveTipographyFamilySizeonDrag } onMouseLeave={ props.DisplayfiveTipographyFamilySizeonMouseLeave } onMouseUp={ props.DisplayfiveTipographyFamilySizeonMouseUp } onMouseDown={ props.DisplayfiveTipographyFamilySizeonMouseDown } onKeyDown={ props.DisplayfiveTipographyFamilySizeonKeyDown } onChange={ props.DisplayfiveTipographyFamilySizeonChange } ondelay={ props.DisplayfiveTipographyFamilySizeondelay }>
                                        <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['displayfivetipography']?.animationClass || {}}>

                                          <div id="id_sixtwoosix_onesevenfivezero" className={` frame displayfivetipography ${ props.onClick ? 'cursor' : '' } ${ transaction['displayfivetipography']?.type ? transaction['displayfivetipography']?.type.toLowerCase() : '' }`} style={ { ...{}, ...props.DisplayfiveTipographyStyle , transitionDuration: transaction['displayfivetipography']?.duration, transitionTimingFunction: transaction['displayfivetipography']?.timingFunction } } onClick={ props.DisplayfiveTipographyonClick } onMouseEnter={ props.DisplayfiveTipographyonMouseEnter } onMouseOver={ props.DisplayfiveTipographyonMouseOver } onKeyPress={ props.DisplayfiveTipographyonKeyPress } onDrag={ props.DisplayfiveTipographyonDrag } onMouseLeave={ props.DisplayfiveTipographyonMouseLeave } onMouseUp={ props.DisplayfiveTipographyonMouseUp } onMouseDown={ props.DisplayfiveTipographyonMouseDown } onKeyDown={ props.DisplayfiveTipographyonKeyDown } onChange={ props.DisplayfiveTipographyonChange } ondelay={ props.DisplayfiveTipographyondelay }>
                                            <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['displayfivetipographyname']?.animationClass || {}}>

                                              <span id="id_sixtwoosix_onesevenfiveone"  className={` text displayfivetipographyname    ${ props.onClick ? 'cursor' : ''}  ${ transaction['displayfivetipographyname']?.type ? transaction['displayfivetipographyname']?.type.toLowerCase() : '' }`} style={{ ...{},  ...props.DisplayfiveTipographyNameStyle , transitionDuration: transaction['displayfivetipographyname']?.duration, transitionTimingFunction: transaction['displayfivetipographyname']?.timingFunction }} onClick={ props.DisplayfiveTipographyNameonClick } onMouseEnter={ props.DisplayfiveTipographyNameonMouseEnter } onMouseOver={ props.DisplayfiveTipographyNameonMouseOver } onKeyPress={ props.DisplayfiveTipographyNameonKeyPress } onDrag={ props.DisplayfiveTipographyNameonDrag } onMouseLeave={ props.DisplayfiveTipographyNameonMouseLeave } onMouseUp={ props.DisplayfiveTipographyNameonMouseUp } onMouseDown={ props.DisplayfiveTipographyNameonMouseDown } onKeyDown={ props.DisplayfiveTipographyNameonKeyDown } onChange={ props.DisplayfiveTipographyNameonChange } ondelay={ props.DisplayfiveTipographyNameondelay } >{props.DisplayfiveTipographyName0 || `Circular Std`}</span>

                                            </CSSTransition>
                                          </div>

                                        </CSSTransition>
                                        <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['displayfivetipographysize']?.animationClass || {}}>

                                          <div id="id_sixtwoosix_onesevenfivethree" className={` frame displayfivetipographysize ${ props.onClick ? 'cursor' : '' } ${ transaction['displayfivetipographysize']?.type ? transaction['displayfivetipographysize']?.type.toLowerCase() : '' }`} style={ { ...{}, ...props.DisplayfiveTipographySizeStyle , transitionDuration: transaction['displayfivetipographysize']?.duration, transitionTimingFunction: transaction['displayfivetipographysize']?.timingFunction } } onClick={ props.DisplayfiveTipographySizeonClick } onMouseEnter={ props.DisplayfiveTipographySizeonMouseEnter } onMouseOver={ props.DisplayfiveTipographySizeonMouseOver } onKeyPress={ props.DisplayfiveTipographySizeonKeyPress } onDrag={ props.DisplayfiveTipographySizeonDrag } onMouseLeave={ props.DisplayfiveTipographySizeonMouseLeave } onMouseUp={ props.DisplayfiveTipographySizeonMouseUp } onMouseDown={ props.DisplayfiveTipographySizeonMouseDown } onKeyDown={ props.DisplayfiveTipographySizeonKeyDown } onChange={ props.DisplayfiveTipographySizeonChange } ondelay={ props.DisplayfiveTipographySizeondelay }>
                                            <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['displayfivetipographysizetext']?.animationClass || {}}>

                                              <span id="id_sixtwoosix_onesevenfivefour"  className={` text displayfivetipographysizetext    ${ props.onClick ? 'cursor' : ''}  ${ transaction['displayfivetipographysizetext']?.type ? transaction['displayfivetipographysizetext']?.type.toLowerCase() : '' }`} style={{ ...{},  ...props.DisplayfiveTipographySizeTextStyle , transitionDuration: transaction['displayfivetipographysizetext']?.duration, transitionTimingFunction: transaction['displayfivetipographysizetext']?.timingFunction }} onClick={ props.DisplayfiveTipographySizeTextonClick } onMouseEnter={ props.DisplayfiveTipographySizeTextonMouseEnter } onMouseOver={ props.DisplayfiveTipographySizeTextonMouseOver } onKeyPress={ props.DisplayfiveTipographySizeTextonKeyPress } onDrag={ props.DisplayfiveTipographySizeTextonDrag } onMouseLeave={ props.DisplayfiveTipographySizeTextonMouseLeave } onMouseUp={ props.DisplayfiveTipographySizeTextonMouseUp } onMouseDown={ props.DisplayfiveTipographySizeTextonMouseDown } onKeyDown={ props.DisplayfiveTipographySizeTextonKeyDown } onChange={ props.DisplayfiveTipographySizeTextonChange } ondelay={ props.DisplayfiveTipographySizeTextondelay } >{props.DisplayfiveTipographySizeText0 || `24px`}</span>

                                            </CSSTransition>
                                          </div>

                                        </CSSTransition>
                                      </div>

                                    </CSSTransition>
                                  </div>

                                </CSSTransition>
                                <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['displayfive']?.animationClass || {}}>
                                  <Displayfive { ...{ ...props, style:false } } cssClass={"C_sixtwoosix_onesevenfivenigth "}  />
    </CSSTransition >
                    </div>
               
    </CSSTransition >
<CSSTransition 
        in={_in}
        appear={true} 
        timeout={100}
        classNames={transaction['displayfivecont']?.animationClass || {}}
    >
    
                    <div id="id_sixthreetwoo_onesixfoursix" className={` frame displayfivecont ${ props.onClick ? 'cursor' : '' } ${ transaction['displayfivecont']?.type ? transaction['displayfivecont']?.type.toLowerCase() : '' }`} style={ { ...{}, ...props.DisplayfiveContStyle , transitionDuration: transaction['displayfivecont']?.duration, transitionTimingFunction: transaction['displayfivecont']?.timingFunction } } onClick={ props.DisplayfiveContonClick } onMouseEnter={ props.DisplayfiveContonMouseEnter } onMouseOver={ props.DisplayfiveContonMouseOver } onKeyPress={ props.DisplayfiveContonKeyPress } onDrag={ props.DisplayfiveContonDrag } onMouseLeave={ props.DisplayfiveContonMouseLeave } onMouseUp={ props.DisplayfiveContonMouseUp } onMouseDown={ props.DisplayfiveContonMouseDown } onKeyDown={ props.DisplayfiveContonKeyDown } onChange={ props.DisplayfiveContonChange } ondelay={ props.DisplayfiveContondelay }>
                                    <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['displayfivedata']?.animationClass || {}}>

                                      <div id="id_sixthreetwoo_onesixfourseven" className={` frame displayfivedata ${ props.onClick ? 'cursor' : '' } ${ transaction['displayfivedata']?.type ? transaction['displayfivedata']?.type.toLowerCase() : '' }`} style={ { ...{}, ...props.DisplayfiveDataStyle , transitionDuration: transaction['displayfivedata']?.duration, transitionTimingFunction: transaction['displayfivedata']?.timingFunction } } onClick={ props.DisplayfiveDataonClick } onMouseEnter={ props.DisplayfiveDataonMouseEnter } onMouseOver={ props.DisplayfiveDataonMouseOver } onKeyPress={ props.DisplayfiveDataonKeyPress } onDrag={ props.DisplayfiveDataonDrag } onMouseLeave={ props.DisplayfiveDataonMouseLeave } onMouseUp={ props.DisplayfiveDataonMouseUp } onMouseDown={ props.DisplayfiveDataonMouseDown } onKeyDown={ props.DisplayfiveDataonKeyDown } onChange={ props.DisplayfiveDataonChange } ondelay={ props.DisplayfiveDataondelay }>
                                        <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['displayfivedatatitle']?.animationClass || {}}>

                                          <div id="id_sixthreetwoo_onesixfoureight" className={` frame displayfivedatatitle ${ props.onClick ? 'cursor' : '' } ${ transaction['displayfivedatatitle']?.type ? transaction['displayfivedatatitle']?.type.toLowerCase() : '' }`} style={ { ...{}, ...props.DisplayfiveDataTitleStyle , transitionDuration: transaction['displayfivedatatitle']?.duration, transitionTimingFunction: transaction['displayfivedatatitle']?.timingFunction } } onClick={ props.DisplayfiveDataTitleonClick } onMouseEnter={ props.DisplayfiveDataTitleonMouseEnter } onMouseOver={ props.DisplayfiveDataTitleonMouseOver } onKeyPress={ props.DisplayfiveDataTitleonKeyPress } onDrag={ props.DisplayfiveDataTitleonDrag } onMouseLeave={ props.DisplayfiveDataTitleonMouseLeave } onMouseUp={ props.DisplayfiveDataTitleonMouseUp } onMouseDown={ props.DisplayfiveDataTitleonMouseDown } onKeyDown={ props.DisplayfiveDataTitleonKeyDown } onChange={ props.DisplayfiveDataTitleonChange } ondelay={ props.DisplayfiveDataTitleondelay }>
                                            <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['displayfivedatatitletext']?.animationClass || {}}>

                                              <span id="id_sixthreetwoo_onesixfournigth"  className={` text displayfivedatatitletext    ${ props.onClick ? 'cursor' : ''}  ${ transaction['displayfivedatatitletext']?.type ? transaction['displayfivedatatitletext']?.type.toLowerCase() : '' }`} style={{ ...{},  ...props.DisplayfiveDataTitleTextStyle , transitionDuration: transaction['displayfivedatatitletext']?.duration, transitionTimingFunction: transaction['displayfivedatatitletext']?.timingFunction }} onClick={ props.DisplayfiveDataTitleTextonClick } onMouseEnter={ props.DisplayfiveDataTitleTextonMouseEnter } onMouseOver={ props.DisplayfiveDataTitleTextonMouseOver } onKeyPress={ props.DisplayfiveDataTitleTextonKeyPress } onDrag={ props.DisplayfiveDataTitleTextonDrag } onMouseLeave={ props.DisplayfiveDataTitleTextonMouseLeave } onMouseUp={ props.DisplayfiveDataTitleTextonMouseUp } onMouseDown={ props.DisplayfiveDataTitleTextonMouseDown } onKeyDown={ props.DisplayfiveDataTitleTextonKeyDown } onChange={ props.DisplayfiveDataTitleTextonChange } ondelay={ props.DisplayfiveDataTitleTextondelay } >{props.DisplayfiveDataTitleText1 || `Display 5`}</span>

                                            </CSSTransition>
                                          </div>

                                        </CSSTransition>
                                        <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['displayfivetipographyfamilysize']?.animationClass || {}}>

                                          <div id="id_sixthreetwoo_onesixfivezero" className={` frame displayfivetipographyfamilysize ${ props.onClick ? 'cursor' : '' } ${ transaction['displayfivetipographyfamilysize']?.type ? transaction['displayfivetipographyfamilysize']?.type.toLowerCase() : '' }`} style={ { ...{}, ...props.DisplayfiveTipographyFamilySizeStyle , transitionDuration: transaction['displayfivetipographyfamilysize']?.duration, transitionTimingFunction: transaction['displayfivetipographyfamilysize']?.timingFunction } } onClick={ props.DisplayfiveTipographyFamilySizeonClick } onMouseEnter={ props.DisplayfiveTipographyFamilySizeonMouseEnter } onMouseOver={ props.DisplayfiveTipographyFamilySizeonMouseOver } onKeyPress={ props.DisplayfiveTipographyFamilySizeonKeyPress } onDrag={ props.DisplayfiveTipographyFamilySizeonDrag } onMouseLeave={ props.DisplayfiveTipographyFamilySizeonMouseLeave } onMouseUp={ props.DisplayfiveTipographyFamilySizeonMouseUp } onMouseDown={ props.DisplayfiveTipographyFamilySizeonMouseDown } onKeyDown={ props.DisplayfiveTipographyFamilySizeonKeyDown } onChange={ props.DisplayfiveTipographyFamilySizeonChange } ondelay={ props.DisplayfiveTipographyFamilySizeondelay }>
                                            <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['displayfivetipography']?.animationClass || {}}>

                                              <div id="id_sixthreetwoo_onesixfiveone" className={` frame displayfivetipography ${ props.onClick ? 'cursor' : '' } ${ transaction['displayfivetipography']?.type ? transaction['displayfivetipography']?.type.toLowerCase() : '' }`} style={ { ...{}, ...props.DisplayfiveTipographyStyle , transitionDuration: transaction['displayfivetipography']?.duration, transitionTimingFunction: transaction['displayfivetipography']?.timingFunction } } onClick={ props.DisplayfiveTipographyonClick } onMouseEnter={ props.DisplayfiveTipographyonMouseEnter } onMouseOver={ props.DisplayfiveTipographyonMouseOver } onKeyPress={ props.DisplayfiveTipographyonKeyPress } onDrag={ props.DisplayfiveTipographyonDrag } onMouseLeave={ props.DisplayfiveTipographyonMouseLeave } onMouseUp={ props.DisplayfiveTipographyonMouseUp } onMouseDown={ props.DisplayfiveTipographyonMouseDown } onKeyDown={ props.DisplayfiveTipographyonKeyDown } onChange={ props.DisplayfiveTipographyonChange } ondelay={ props.DisplayfiveTipographyondelay }>
                                                <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['displayfivetipographyname']?.animationClass || {}}>

                                                  <span id="id_sixthreetwoo_onesixfivetwoo"  className={` text displayfivetipographyname    ${ props.onClick ? 'cursor' : ''}  ${ transaction['displayfivetipographyname']?.type ? transaction['displayfivetipographyname']?.type.toLowerCase() : '' }`} style={{ ...{},  ...props.DisplayfiveTipographyNameStyle , transitionDuration: transaction['displayfivetipographyname']?.duration, transitionTimingFunction: transaction['displayfivetipographyname']?.timingFunction }} onClick={ props.DisplayfiveTipographyNameonClick } onMouseEnter={ props.DisplayfiveTipographyNameonMouseEnter } onMouseOver={ props.DisplayfiveTipographyNameonMouseOver } onKeyPress={ props.DisplayfiveTipographyNameonKeyPress } onDrag={ props.DisplayfiveTipographyNameonDrag } onMouseLeave={ props.DisplayfiveTipographyNameonMouseLeave } onMouseUp={ props.DisplayfiveTipographyNameonMouseUp } onMouseDown={ props.DisplayfiveTipographyNameonMouseDown } onKeyDown={ props.DisplayfiveTipographyNameonKeyDown } onChange={ props.DisplayfiveTipographyNameonChange } ondelay={ props.DisplayfiveTipographyNameondelay } >{props.DisplayfiveTipographyName1 || `Circular Std`}</span>

                                                </CSSTransition>
                                              </div>

                                            </CSSTransition>
                                            <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['displayfivetipographysize']?.animationClass || {}}>

                                              <div id="id_sixthreetwoo_onesixfivethree" className={` frame displayfivetipographysize ${ props.onClick ? 'cursor' : '' } ${ transaction['displayfivetipographysize']?.type ? transaction['displayfivetipographysize']?.type.toLowerCase() : '' }`} style={ { ...{}, ...props.DisplayfiveTipographySizeStyle , transitionDuration: transaction['displayfivetipographysize']?.duration, transitionTimingFunction: transaction['displayfivetipographysize']?.timingFunction } } onClick={ props.DisplayfiveTipographySizeonClick } onMouseEnter={ props.DisplayfiveTipographySizeonMouseEnter } onMouseOver={ props.DisplayfiveTipographySizeonMouseOver } onKeyPress={ props.DisplayfiveTipographySizeonKeyPress } onDrag={ props.DisplayfiveTipographySizeonDrag } onMouseLeave={ props.DisplayfiveTipographySizeonMouseLeave } onMouseUp={ props.DisplayfiveTipographySizeonMouseUp } onMouseDown={ props.DisplayfiveTipographySizeonMouseDown } onKeyDown={ props.DisplayfiveTipographySizeonKeyDown } onChange={ props.DisplayfiveTipographySizeonChange } ondelay={ props.DisplayfiveTipographySizeondelay }>
                                                <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['displayfivetipographysizetext']?.animationClass || {}}>

                                                  <span id="id_sixthreetwoo_onesixfivefour"  className={` text displayfivetipographysizetext    ${ props.onClick ? 'cursor' : ''}  ${ transaction['displayfivetipographysizetext']?.type ? transaction['displayfivetipographysizetext']?.type.toLowerCase() : '' }`} style={{ ...{},  ...props.DisplayfiveTipographySizeTextStyle , transitionDuration: transaction['displayfivetipographysizetext']?.duration, transitionTimingFunction: transaction['displayfivetipographysizetext']?.timingFunction }} onClick={ props.DisplayfiveTipographySizeTextonClick } onMouseEnter={ props.DisplayfiveTipographySizeTextonMouseEnter } onMouseOver={ props.DisplayfiveTipographySizeTextonMouseOver } onKeyPress={ props.DisplayfiveTipographySizeTextonKeyPress } onDrag={ props.DisplayfiveTipographySizeTextonDrag } onMouseLeave={ props.DisplayfiveTipographySizeTextonMouseLeave } onMouseUp={ props.DisplayfiveTipographySizeTextonMouseUp } onMouseDown={ props.DisplayfiveTipographySizeTextonMouseDown } onKeyDown={ props.DisplayfiveTipographySizeTextonKeyDown } onChange={ props.DisplayfiveTipographySizeTextonChange } ondelay={ props.DisplayfiveTipographySizeTextondelay } >{props.DisplayfiveTipographySizeText1 || `24px`}</span>

                                                </CSSTransition>
                                              </div>

                                            </CSSTransition>
                                          </div>

                                        </CSSTransition>
                                      </div>

                                    </CSSTransition>
                                    <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['displayfive']?.animationClass || {}}>
                                      <Displayfive { ...{ ...props, style:false } } DisplayfiveText0={ props.DisplayfiveText1 || "Display 5" } cssClass={"C_sixthreetwoo_onesixfivefive "}  />
    </CSSTransition >
                    </div>
               
    </CSSTransition >
                    </div>
               
    </CSSTransition >
            
            </>
        }
        </div>
    
    </CSSTransition >
            </>
        
    ) 
}

TipographyWrapper.propTypes = {
    style: PropTypes.any,
SubHeadings0: PropTypes.any,
Divider0: PropTypes.any,
DisplayoneDataTitleText0: PropTypes.any,
DisplayoneTipographyName0: PropTypes.any,
DisplayoneTipographySizeText0: PropTypes.any,
DisplaytwooDataTitleText0: PropTypes.any,
DisplaytwooTipographyName0: PropTypes.any,
DisplaytwooTipographySizeText0: PropTypes.any,
DisplaythreeDataTitleText0: PropTypes.any,
DisplaythreeTipographyName0: PropTypes.any,
DisplaythreeTipographySizeText0: PropTypes.any,
DisplayfourDataTitleText0: PropTypes.any,
DisplayfourTipographyName0: PropTypes.any,
DisplayfourTipographySizeText0: PropTypes.any,
DisplayfiveDataTitleText0: PropTypes.any,
DisplayfiveTipographyName0: PropTypes.any,
DisplayfiveTipographySizeText0: PropTypes.any,
DisplayfiveDataTitleText1: PropTypes.any,
DisplayfiveTipographyName1: PropTypes.any,
DisplayfiveTipographySizeText1: PropTypes.any,
DisplayfiveText1: PropTypes.any,
TipographyWrapperonClick: PropTypes.any,
TipographyWrapperonMouseEnter: PropTypes.any,
TipographyWrapperonMouseOver: PropTypes.any,
TipographyWrapperonKeyPress: PropTypes.any,
TipographyWrapperonDrag: PropTypes.any,
TipographyWrapperonMouseLeave: PropTypes.any,
TipographyWrapperonMouseUp: PropTypes.any,
TipographyWrapperonMouseDown: PropTypes.any,
TipographyWrapperonKeyDown: PropTypes.any,
TipographyWrapperonChange: PropTypes.any,
TipographyWrapperondelay: PropTypes.any,
SpecialHeadingsonClick: PropTypes.any,
SpecialHeadingsonMouseEnter: PropTypes.any,
SpecialHeadingsonMouseOver: PropTypes.any,
SpecialHeadingsonKeyPress: PropTypes.any,
SpecialHeadingsonDrag: PropTypes.any,
SpecialHeadingsonMouseLeave: PropTypes.any,
SpecialHeadingsonMouseUp: PropTypes.any,
SpecialHeadingsonMouseDown: PropTypes.any,
SpecialHeadingsonKeyDown: PropTypes.any,
SpecialHeadingsonChange: PropTypes.any,
SpecialHeadingsondelay: PropTypes.any,
SubHeadingsonClick: PropTypes.any,
SubHeadingsonMouseEnter: PropTypes.any,
SubHeadingsonMouseOver: PropTypes.any,
SubHeadingsonKeyPress: PropTypes.any,
SubHeadingsonDrag: PropTypes.any,
SubHeadingsonMouseLeave: PropTypes.any,
SubHeadingsonMouseUp: PropTypes.any,
SubHeadingsonMouseDown: PropTypes.any,
SubHeadingsonKeyDown: PropTypes.any,
SubHeadingsonChange: PropTypes.any,
SubHeadingsondelay: PropTypes.any,
DivideronClick: PropTypes.any,
DivideronMouseEnter: PropTypes.any,
DivideronMouseOver: PropTypes.any,
DivideronKeyPress: PropTypes.any,
DivideronDrag: PropTypes.any,
DivideronMouseLeave: PropTypes.any,
DivideronMouseUp: PropTypes.any,
DivideronMouseDown: PropTypes.any,
DivideronKeyDown: PropTypes.any,
DivideronChange: PropTypes.any,
Dividerondelay: PropTypes.any,
DisplayoneContonClick: PropTypes.any,
DisplayoneContonMouseEnter: PropTypes.any,
DisplayoneContonMouseOver: PropTypes.any,
DisplayoneContonKeyPress: PropTypes.any,
DisplayoneContonDrag: PropTypes.any,
DisplayoneContonMouseLeave: PropTypes.any,
DisplayoneContonMouseUp: PropTypes.any,
DisplayoneContonMouseDown: PropTypes.any,
DisplayoneContonKeyDown: PropTypes.any,
DisplayoneContonChange: PropTypes.any,
DisplayoneContondelay: PropTypes.any,
DisplayoneDataonClick: PropTypes.any,
DisplayoneDataonMouseEnter: PropTypes.any,
DisplayoneDataonMouseOver: PropTypes.any,
DisplayoneDataonKeyPress: PropTypes.any,
DisplayoneDataonDrag: PropTypes.any,
DisplayoneDataonMouseLeave: PropTypes.any,
DisplayoneDataonMouseUp: PropTypes.any,
DisplayoneDataonMouseDown: PropTypes.any,
DisplayoneDataonKeyDown: PropTypes.any,
DisplayoneDataonChange: PropTypes.any,
DisplayoneDataondelay: PropTypes.any,
DisplayoneDataTitleonClick: PropTypes.any,
DisplayoneDataTitleonMouseEnter: PropTypes.any,
DisplayoneDataTitleonMouseOver: PropTypes.any,
DisplayoneDataTitleonKeyPress: PropTypes.any,
DisplayoneDataTitleonDrag: PropTypes.any,
DisplayoneDataTitleonMouseLeave: PropTypes.any,
DisplayoneDataTitleonMouseUp: PropTypes.any,
DisplayoneDataTitleonMouseDown: PropTypes.any,
DisplayoneDataTitleonKeyDown: PropTypes.any,
DisplayoneDataTitleonChange: PropTypes.any,
DisplayoneDataTitleondelay: PropTypes.any,
DisplayoneDataTitleTextonClick: PropTypes.any,
DisplayoneDataTitleTextonMouseEnter: PropTypes.any,
DisplayoneDataTitleTextonMouseOver: PropTypes.any,
DisplayoneDataTitleTextonKeyPress: PropTypes.any,
DisplayoneDataTitleTextonDrag: PropTypes.any,
DisplayoneDataTitleTextonMouseLeave: PropTypes.any,
DisplayoneDataTitleTextonMouseUp: PropTypes.any,
DisplayoneDataTitleTextonMouseDown: PropTypes.any,
DisplayoneDataTitleTextonKeyDown: PropTypes.any,
DisplayoneDataTitleTextonChange: PropTypes.any,
DisplayoneDataTitleTextondelay: PropTypes.any,
DisplayoneTipographyFamilySizeonClick: PropTypes.any,
DisplayoneTipographyFamilySizeonMouseEnter: PropTypes.any,
DisplayoneTipographyFamilySizeonMouseOver: PropTypes.any,
DisplayoneTipographyFamilySizeonKeyPress: PropTypes.any,
DisplayoneTipographyFamilySizeonDrag: PropTypes.any,
DisplayoneTipographyFamilySizeonMouseLeave: PropTypes.any,
DisplayoneTipographyFamilySizeonMouseUp: PropTypes.any,
DisplayoneTipographyFamilySizeonMouseDown: PropTypes.any,
DisplayoneTipographyFamilySizeonKeyDown: PropTypes.any,
DisplayoneTipographyFamilySizeonChange: PropTypes.any,
DisplayoneTipographyFamilySizeondelay: PropTypes.any,
DisplayoneTipographyonClick: PropTypes.any,
DisplayoneTipographyonMouseEnter: PropTypes.any,
DisplayoneTipographyonMouseOver: PropTypes.any,
DisplayoneTipographyonKeyPress: PropTypes.any,
DisplayoneTipographyonDrag: PropTypes.any,
DisplayoneTipographyonMouseLeave: PropTypes.any,
DisplayoneTipographyonMouseUp: PropTypes.any,
DisplayoneTipographyonMouseDown: PropTypes.any,
DisplayoneTipographyonKeyDown: PropTypes.any,
DisplayoneTipographyonChange: PropTypes.any,
DisplayoneTipographyondelay: PropTypes.any,
DisplayoneTipographyNameonClick: PropTypes.any,
DisplayoneTipographyNameonMouseEnter: PropTypes.any,
DisplayoneTipographyNameonMouseOver: PropTypes.any,
DisplayoneTipographyNameonKeyPress: PropTypes.any,
DisplayoneTipographyNameonDrag: PropTypes.any,
DisplayoneTipographyNameonMouseLeave: PropTypes.any,
DisplayoneTipographyNameonMouseUp: PropTypes.any,
DisplayoneTipographyNameonMouseDown: PropTypes.any,
DisplayoneTipographyNameonKeyDown: PropTypes.any,
DisplayoneTipographyNameonChange: PropTypes.any,
DisplayoneTipographyNameondelay: PropTypes.any,
DisplayoneTipographySizeonClick: PropTypes.any,
DisplayoneTipographySizeonMouseEnter: PropTypes.any,
DisplayoneTipographySizeonMouseOver: PropTypes.any,
DisplayoneTipographySizeonKeyPress: PropTypes.any,
DisplayoneTipographySizeonDrag: PropTypes.any,
DisplayoneTipographySizeonMouseLeave: PropTypes.any,
DisplayoneTipographySizeonMouseUp: PropTypes.any,
DisplayoneTipographySizeonMouseDown: PropTypes.any,
DisplayoneTipographySizeonKeyDown: PropTypes.any,
DisplayoneTipographySizeonChange: PropTypes.any,
DisplayoneTipographySizeondelay: PropTypes.any,
DisplayoneTipographySizeTextonClick: PropTypes.any,
DisplayoneTipographySizeTextonMouseEnter: PropTypes.any,
DisplayoneTipographySizeTextonMouseOver: PropTypes.any,
DisplayoneTipographySizeTextonKeyPress: PropTypes.any,
DisplayoneTipographySizeTextonDrag: PropTypes.any,
DisplayoneTipographySizeTextonMouseLeave: PropTypes.any,
DisplayoneTipographySizeTextonMouseUp: PropTypes.any,
DisplayoneTipographySizeTextonMouseDown: PropTypes.any,
DisplayoneTipographySizeTextonKeyDown: PropTypes.any,
DisplayoneTipographySizeTextonChange: PropTypes.any,
DisplayoneTipographySizeTextondelay: PropTypes.any,
DisplaytwooContonClick: PropTypes.any,
DisplaytwooContonMouseEnter: PropTypes.any,
DisplaytwooContonMouseOver: PropTypes.any,
DisplaytwooContonKeyPress: PropTypes.any,
DisplaytwooContonDrag: PropTypes.any,
DisplaytwooContonMouseLeave: PropTypes.any,
DisplaytwooContonMouseUp: PropTypes.any,
DisplaytwooContonMouseDown: PropTypes.any,
DisplaytwooContonKeyDown: PropTypes.any,
DisplaytwooContonChange: PropTypes.any,
DisplaytwooContondelay: PropTypes.any,
DisplaytwooDataonClick: PropTypes.any,
DisplaytwooDataonMouseEnter: PropTypes.any,
DisplaytwooDataonMouseOver: PropTypes.any,
DisplaytwooDataonKeyPress: PropTypes.any,
DisplaytwooDataonDrag: PropTypes.any,
DisplaytwooDataonMouseLeave: PropTypes.any,
DisplaytwooDataonMouseUp: PropTypes.any,
DisplaytwooDataonMouseDown: PropTypes.any,
DisplaytwooDataonKeyDown: PropTypes.any,
DisplaytwooDataonChange: PropTypes.any,
DisplaytwooDataondelay: PropTypes.any,
DisplaytwooDataTitleonClick: PropTypes.any,
DisplaytwooDataTitleonMouseEnter: PropTypes.any,
DisplaytwooDataTitleonMouseOver: PropTypes.any,
DisplaytwooDataTitleonKeyPress: PropTypes.any,
DisplaytwooDataTitleonDrag: PropTypes.any,
DisplaytwooDataTitleonMouseLeave: PropTypes.any,
DisplaytwooDataTitleonMouseUp: PropTypes.any,
DisplaytwooDataTitleonMouseDown: PropTypes.any,
DisplaytwooDataTitleonKeyDown: PropTypes.any,
DisplaytwooDataTitleonChange: PropTypes.any,
DisplaytwooDataTitleondelay: PropTypes.any,
DisplaytwooDataTitleTextonClick: PropTypes.any,
DisplaytwooDataTitleTextonMouseEnter: PropTypes.any,
DisplaytwooDataTitleTextonMouseOver: PropTypes.any,
DisplaytwooDataTitleTextonKeyPress: PropTypes.any,
DisplaytwooDataTitleTextonDrag: PropTypes.any,
DisplaytwooDataTitleTextonMouseLeave: PropTypes.any,
DisplaytwooDataTitleTextonMouseUp: PropTypes.any,
DisplaytwooDataTitleTextonMouseDown: PropTypes.any,
DisplaytwooDataTitleTextonKeyDown: PropTypes.any,
DisplaytwooDataTitleTextonChange: PropTypes.any,
DisplaytwooDataTitleTextondelay: PropTypes.any,
DisplaytwooTipographyFamilySizeonClick: PropTypes.any,
DisplaytwooTipographyFamilySizeonMouseEnter: PropTypes.any,
DisplaytwooTipographyFamilySizeonMouseOver: PropTypes.any,
DisplaytwooTipographyFamilySizeonKeyPress: PropTypes.any,
DisplaytwooTipographyFamilySizeonDrag: PropTypes.any,
DisplaytwooTipographyFamilySizeonMouseLeave: PropTypes.any,
DisplaytwooTipographyFamilySizeonMouseUp: PropTypes.any,
DisplaytwooTipographyFamilySizeonMouseDown: PropTypes.any,
DisplaytwooTipographyFamilySizeonKeyDown: PropTypes.any,
DisplaytwooTipographyFamilySizeonChange: PropTypes.any,
DisplaytwooTipographyFamilySizeondelay: PropTypes.any,
DisplaytwooTipographyonClick: PropTypes.any,
DisplaytwooTipographyonMouseEnter: PropTypes.any,
DisplaytwooTipographyonMouseOver: PropTypes.any,
DisplaytwooTipographyonKeyPress: PropTypes.any,
DisplaytwooTipographyonDrag: PropTypes.any,
DisplaytwooTipographyonMouseLeave: PropTypes.any,
DisplaytwooTipographyonMouseUp: PropTypes.any,
DisplaytwooTipographyonMouseDown: PropTypes.any,
DisplaytwooTipographyonKeyDown: PropTypes.any,
DisplaytwooTipographyonChange: PropTypes.any,
DisplaytwooTipographyondelay: PropTypes.any,
DisplaytwooTipographyNameonClick: PropTypes.any,
DisplaytwooTipographyNameonMouseEnter: PropTypes.any,
DisplaytwooTipographyNameonMouseOver: PropTypes.any,
DisplaytwooTipographyNameonKeyPress: PropTypes.any,
DisplaytwooTipographyNameonDrag: PropTypes.any,
DisplaytwooTipographyNameonMouseLeave: PropTypes.any,
DisplaytwooTipographyNameonMouseUp: PropTypes.any,
DisplaytwooTipographyNameonMouseDown: PropTypes.any,
DisplaytwooTipographyNameonKeyDown: PropTypes.any,
DisplaytwooTipographyNameonChange: PropTypes.any,
DisplaytwooTipographyNameondelay: PropTypes.any,
DisplaytwooTipographySizeonClick: PropTypes.any,
DisplaytwooTipographySizeonMouseEnter: PropTypes.any,
DisplaytwooTipographySizeonMouseOver: PropTypes.any,
DisplaytwooTipographySizeonKeyPress: PropTypes.any,
DisplaytwooTipographySizeonDrag: PropTypes.any,
DisplaytwooTipographySizeonMouseLeave: PropTypes.any,
DisplaytwooTipographySizeonMouseUp: PropTypes.any,
DisplaytwooTipographySizeonMouseDown: PropTypes.any,
DisplaytwooTipographySizeonKeyDown: PropTypes.any,
DisplaytwooTipographySizeonChange: PropTypes.any,
DisplaytwooTipographySizeondelay: PropTypes.any,
DisplaytwooTipographySizeTextonClick: PropTypes.any,
DisplaytwooTipographySizeTextonMouseEnter: PropTypes.any,
DisplaytwooTipographySizeTextonMouseOver: PropTypes.any,
DisplaytwooTipographySizeTextonKeyPress: PropTypes.any,
DisplaytwooTipographySizeTextonDrag: PropTypes.any,
DisplaytwooTipographySizeTextonMouseLeave: PropTypes.any,
DisplaytwooTipographySizeTextonMouseUp: PropTypes.any,
DisplaytwooTipographySizeTextonMouseDown: PropTypes.any,
DisplaytwooTipographySizeTextonKeyDown: PropTypes.any,
DisplaytwooTipographySizeTextonChange: PropTypes.any,
DisplaytwooTipographySizeTextondelay: PropTypes.any,
DisplaythreeContonClick: PropTypes.any,
DisplaythreeContonMouseEnter: PropTypes.any,
DisplaythreeContonMouseOver: PropTypes.any,
DisplaythreeContonKeyPress: PropTypes.any,
DisplaythreeContonDrag: PropTypes.any,
DisplaythreeContonMouseLeave: PropTypes.any,
DisplaythreeContonMouseUp: PropTypes.any,
DisplaythreeContonMouseDown: PropTypes.any,
DisplaythreeContonKeyDown: PropTypes.any,
DisplaythreeContonChange: PropTypes.any,
DisplaythreeContondelay: PropTypes.any,
DisplaythreeDataonClick: PropTypes.any,
DisplaythreeDataonMouseEnter: PropTypes.any,
DisplaythreeDataonMouseOver: PropTypes.any,
DisplaythreeDataonKeyPress: PropTypes.any,
DisplaythreeDataonDrag: PropTypes.any,
DisplaythreeDataonMouseLeave: PropTypes.any,
DisplaythreeDataonMouseUp: PropTypes.any,
DisplaythreeDataonMouseDown: PropTypes.any,
DisplaythreeDataonKeyDown: PropTypes.any,
DisplaythreeDataonChange: PropTypes.any,
DisplaythreeDataondelay: PropTypes.any,
DisplaythreeDataTitleonClick: PropTypes.any,
DisplaythreeDataTitleonMouseEnter: PropTypes.any,
DisplaythreeDataTitleonMouseOver: PropTypes.any,
DisplaythreeDataTitleonKeyPress: PropTypes.any,
DisplaythreeDataTitleonDrag: PropTypes.any,
DisplaythreeDataTitleonMouseLeave: PropTypes.any,
DisplaythreeDataTitleonMouseUp: PropTypes.any,
DisplaythreeDataTitleonMouseDown: PropTypes.any,
DisplaythreeDataTitleonKeyDown: PropTypes.any,
DisplaythreeDataTitleonChange: PropTypes.any,
DisplaythreeDataTitleondelay: PropTypes.any,
DisplaythreeDataTitleTextonClick: PropTypes.any,
DisplaythreeDataTitleTextonMouseEnter: PropTypes.any,
DisplaythreeDataTitleTextonMouseOver: PropTypes.any,
DisplaythreeDataTitleTextonKeyPress: PropTypes.any,
DisplaythreeDataTitleTextonDrag: PropTypes.any,
DisplaythreeDataTitleTextonMouseLeave: PropTypes.any,
DisplaythreeDataTitleTextonMouseUp: PropTypes.any,
DisplaythreeDataTitleTextonMouseDown: PropTypes.any,
DisplaythreeDataTitleTextonKeyDown: PropTypes.any,
DisplaythreeDataTitleTextonChange: PropTypes.any,
DisplaythreeDataTitleTextondelay: PropTypes.any,
DisplaythreeTipographyFamilySizeonClick: PropTypes.any,
DisplaythreeTipographyFamilySizeonMouseEnter: PropTypes.any,
DisplaythreeTipographyFamilySizeonMouseOver: PropTypes.any,
DisplaythreeTipographyFamilySizeonKeyPress: PropTypes.any,
DisplaythreeTipographyFamilySizeonDrag: PropTypes.any,
DisplaythreeTipographyFamilySizeonMouseLeave: PropTypes.any,
DisplaythreeTipographyFamilySizeonMouseUp: PropTypes.any,
DisplaythreeTipographyFamilySizeonMouseDown: PropTypes.any,
DisplaythreeTipographyFamilySizeonKeyDown: PropTypes.any,
DisplaythreeTipographyFamilySizeonChange: PropTypes.any,
DisplaythreeTipographyFamilySizeondelay: PropTypes.any,
DisplaythreeTipographyonClick: PropTypes.any,
DisplaythreeTipographyonMouseEnter: PropTypes.any,
DisplaythreeTipographyonMouseOver: PropTypes.any,
DisplaythreeTipographyonKeyPress: PropTypes.any,
DisplaythreeTipographyonDrag: PropTypes.any,
DisplaythreeTipographyonMouseLeave: PropTypes.any,
DisplaythreeTipographyonMouseUp: PropTypes.any,
DisplaythreeTipographyonMouseDown: PropTypes.any,
DisplaythreeTipographyonKeyDown: PropTypes.any,
DisplaythreeTipographyonChange: PropTypes.any,
DisplaythreeTipographyondelay: PropTypes.any,
DisplaythreeTipographyNameonClick: PropTypes.any,
DisplaythreeTipographyNameonMouseEnter: PropTypes.any,
DisplaythreeTipographyNameonMouseOver: PropTypes.any,
DisplaythreeTipographyNameonKeyPress: PropTypes.any,
DisplaythreeTipographyNameonDrag: PropTypes.any,
DisplaythreeTipographyNameonMouseLeave: PropTypes.any,
DisplaythreeTipographyNameonMouseUp: PropTypes.any,
DisplaythreeTipographyNameonMouseDown: PropTypes.any,
DisplaythreeTipographyNameonKeyDown: PropTypes.any,
DisplaythreeTipographyNameonChange: PropTypes.any,
DisplaythreeTipographyNameondelay: PropTypes.any,
DisplaythreeTipographySizeonClick: PropTypes.any,
DisplaythreeTipographySizeonMouseEnter: PropTypes.any,
DisplaythreeTipographySizeonMouseOver: PropTypes.any,
DisplaythreeTipographySizeonKeyPress: PropTypes.any,
DisplaythreeTipographySizeonDrag: PropTypes.any,
DisplaythreeTipographySizeonMouseLeave: PropTypes.any,
DisplaythreeTipographySizeonMouseUp: PropTypes.any,
DisplaythreeTipographySizeonMouseDown: PropTypes.any,
DisplaythreeTipographySizeonKeyDown: PropTypes.any,
DisplaythreeTipographySizeonChange: PropTypes.any,
DisplaythreeTipographySizeondelay: PropTypes.any,
DisplaythreeTipographySizeTextonClick: PropTypes.any,
DisplaythreeTipographySizeTextonMouseEnter: PropTypes.any,
DisplaythreeTipographySizeTextonMouseOver: PropTypes.any,
DisplaythreeTipographySizeTextonKeyPress: PropTypes.any,
DisplaythreeTipographySizeTextonDrag: PropTypes.any,
DisplaythreeTipographySizeTextonMouseLeave: PropTypes.any,
DisplaythreeTipographySizeTextonMouseUp: PropTypes.any,
DisplaythreeTipographySizeTextonMouseDown: PropTypes.any,
DisplaythreeTipographySizeTextonKeyDown: PropTypes.any,
DisplaythreeTipographySizeTextonChange: PropTypes.any,
DisplaythreeTipographySizeTextondelay: PropTypes.any,
DisplayfourContonClick: PropTypes.any,
DisplayfourContonMouseEnter: PropTypes.any,
DisplayfourContonMouseOver: PropTypes.any,
DisplayfourContonKeyPress: PropTypes.any,
DisplayfourContonDrag: PropTypes.any,
DisplayfourContonMouseLeave: PropTypes.any,
DisplayfourContonMouseUp: PropTypes.any,
DisplayfourContonMouseDown: PropTypes.any,
DisplayfourContonKeyDown: PropTypes.any,
DisplayfourContonChange: PropTypes.any,
DisplayfourContondelay: PropTypes.any,
DisplayfourDataonClick: PropTypes.any,
DisplayfourDataonMouseEnter: PropTypes.any,
DisplayfourDataonMouseOver: PropTypes.any,
DisplayfourDataonKeyPress: PropTypes.any,
DisplayfourDataonDrag: PropTypes.any,
DisplayfourDataonMouseLeave: PropTypes.any,
DisplayfourDataonMouseUp: PropTypes.any,
DisplayfourDataonMouseDown: PropTypes.any,
DisplayfourDataonKeyDown: PropTypes.any,
DisplayfourDataonChange: PropTypes.any,
DisplayfourDataondelay: PropTypes.any,
DisplayfourDataTitleonClick: PropTypes.any,
DisplayfourDataTitleonMouseEnter: PropTypes.any,
DisplayfourDataTitleonMouseOver: PropTypes.any,
DisplayfourDataTitleonKeyPress: PropTypes.any,
DisplayfourDataTitleonDrag: PropTypes.any,
DisplayfourDataTitleonMouseLeave: PropTypes.any,
DisplayfourDataTitleonMouseUp: PropTypes.any,
DisplayfourDataTitleonMouseDown: PropTypes.any,
DisplayfourDataTitleonKeyDown: PropTypes.any,
DisplayfourDataTitleonChange: PropTypes.any,
DisplayfourDataTitleondelay: PropTypes.any,
DisplayfourDataTitleTextonClick: PropTypes.any,
DisplayfourDataTitleTextonMouseEnter: PropTypes.any,
DisplayfourDataTitleTextonMouseOver: PropTypes.any,
DisplayfourDataTitleTextonKeyPress: PropTypes.any,
DisplayfourDataTitleTextonDrag: PropTypes.any,
DisplayfourDataTitleTextonMouseLeave: PropTypes.any,
DisplayfourDataTitleTextonMouseUp: PropTypes.any,
DisplayfourDataTitleTextonMouseDown: PropTypes.any,
DisplayfourDataTitleTextonKeyDown: PropTypes.any,
DisplayfourDataTitleTextonChange: PropTypes.any,
DisplayfourDataTitleTextondelay: PropTypes.any,
DisplayfourTipographyFamilySizeonClick: PropTypes.any,
DisplayfourTipographyFamilySizeonMouseEnter: PropTypes.any,
DisplayfourTipographyFamilySizeonMouseOver: PropTypes.any,
DisplayfourTipographyFamilySizeonKeyPress: PropTypes.any,
DisplayfourTipographyFamilySizeonDrag: PropTypes.any,
DisplayfourTipographyFamilySizeonMouseLeave: PropTypes.any,
DisplayfourTipographyFamilySizeonMouseUp: PropTypes.any,
DisplayfourTipographyFamilySizeonMouseDown: PropTypes.any,
DisplayfourTipographyFamilySizeonKeyDown: PropTypes.any,
DisplayfourTipographyFamilySizeonChange: PropTypes.any,
DisplayfourTipographyFamilySizeondelay: PropTypes.any,
DisplayfourTipographyonClick: PropTypes.any,
DisplayfourTipographyonMouseEnter: PropTypes.any,
DisplayfourTipographyonMouseOver: PropTypes.any,
DisplayfourTipographyonKeyPress: PropTypes.any,
DisplayfourTipographyonDrag: PropTypes.any,
DisplayfourTipographyonMouseLeave: PropTypes.any,
DisplayfourTipographyonMouseUp: PropTypes.any,
DisplayfourTipographyonMouseDown: PropTypes.any,
DisplayfourTipographyonKeyDown: PropTypes.any,
DisplayfourTipographyonChange: PropTypes.any,
DisplayfourTipographyondelay: PropTypes.any,
DisplayfourTipographyNameonClick: PropTypes.any,
DisplayfourTipographyNameonMouseEnter: PropTypes.any,
DisplayfourTipographyNameonMouseOver: PropTypes.any,
DisplayfourTipographyNameonKeyPress: PropTypes.any,
DisplayfourTipographyNameonDrag: PropTypes.any,
DisplayfourTipographyNameonMouseLeave: PropTypes.any,
DisplayfourTipographyNameonMouseUp: PropTypes.any,
DisplayfourTipographyNameonMouseDown: PropTypes.any,
DisplayfourTipographyNameonKeyDown: PropTypes.any,
DisplayfourTipographyNameonChange: PropTypes.any,
DisplayfourTipographyNameondelay: PropTypes.any,
DisplayfourTipographySizeonClick: PropTypes.any,
DisplayfourTipographySizeonMouseEnter: PropTypes.any,
DisplayfourTipographySizeonMouseOver: PropTypes.any,
DisplayfourTipographySizeonKeyPress: PropTypes.any,
DisplayfourTipographySizeonDrag: PropTypes.any,
DisplayfourTipographySizeonMouseLeave: PropTypes.any,
DisplayfourTipographySizeonMouseUp: PropTypes.any,
DisplayfourTipographySizeonMouseDown: PropTypes.any,
DisplayfourTipographySizeonKeyDown: PropTypes.any,
DisplayfourTipographySizeonChange: PropTypes.any,
DisplayfourTipographySizeondelay: PropTypes.any,
DisplayfourTipographySizeTextonClick: PropTypes.any,
DisplayfourTipographySizeTextonMouseEnter: PropTypes.any,
DisplayfourTipographySizeTextonMouseOver: PropTypes.any,
DisplayfourTipographySizeTextonKeyPress: PropTypes.any,
DisplayfourTipographySizeTextonDrag: PropTypes.any,
DisplayfourTipographySizeTextonMouseLeave: PropTypes.any,
DisplayfourTipographySizeTextonMouseUp: PropTypes.any,
DisplayfourTipographySizeTextonMouseDown: PropTypes.any,
DisplayfourTipographySizeTextonKeyDown: PropTypes.any,
DisplayfourTipographySizeTextonChange: PropTypes.any,
DisplayfourTipographySizeTextondelay: PropTypes.any,
DisplayfiveContonClick: PropTypes.any,
DisplayfiveContonMouseEnter: PropTypes.any,
DisplayfiveContonMouseOver: PropTypes.any,
DisplayfiveContonKeyPress: PropTypes.any,
DisplayfiveContonDrag: PropTypes.any,
DisplayfiveContonMouseLeave: PropTypes.any,
DisplayfiveContonMouseUp: PropTypes.any,
DisplayfiveContonMouseDown: PropTypes.any,
DisplayfiveContonKeyDown: PropTypes.any,
DisplayfiveContonChange: PropTypes.any,
DisplayfiveContondelay: PropTypes.any,
DisplayfiveDataonClick: PropTypes.any,
DisplayfiveDataonMouseEnter: PropTypes.any,
DisplayfiveDataonMouseOver: PropTypes.any,
DisplayfiveDataonKeyPress: PropTypes.any,
DisplayfiveDataonDrag: PropTypes.any,
DisplayfiveDataonMouseLeave: PropTypes.any,
DisplayfiveDataonMouseUp: PropTypes.any,
DisplayfiveDataonMouseDown: PropTypes.any,
DisplayfiveDataonKeyDown: PropTypes.any,
DisplayfiveDataonChange: PropTypes.any,
DisplayfiveDataondelay: PropTypes.any,
DisplayfiveDataTitleonClick: PropTypes.any,
DisplayfiveDataTitleonMouseEnter: PropTypes.any,
DisplayfiveDataTitleonMouseOver: PropTypes.any,
DisplayfiveDataTitleonKeyPress: PropTypes.any,
DisplayfiveDataTitleonDrag: PropTypes.any,
DisplayfiveDataTitleonMouseLeave: PropTypes.any,
DisplayfiveDataTitleonMouseUp: PropTypes.any,
DisplayfiveDataTitleonMouseDown: PropTypes.any,
DisplayfiveDataTitleonKeyDown: PropTypes.any,
DisplayfiveDataTitleonChange: PropTypes.any,
DisplayfiveDataTitleondelay: PropTypes.any,
DisplayfiveDataTitleTextonClick: PropTypes.any,
DisplayfiveDataTitleTextonMouseEnter: PropTypes.any,
DisplayfiveDataTitleTextonMouseOver: PropTypes.any,
DisplayfiveDataTitleTextonKeyPress: PropTypes.any,
DisplayfiveDataTitleTextonDrag: PropTypes.any,
DisplayfiveDataTitleTextonMouseLeave: PropTypes.any,
DisplayfiveDataTitleTextonMouseUp: PropTypes.any,
DisplayfiveDataTitleTextonMouseDown: PropTypes.any,
DisplayfiveDataTitleTextonKeyDown: PropTypes.any,
DisplayfiveDataTitleTextonChange: PropTypes.any,
DisplayfiveDataTitleTextondelay: PropTypes.any,
DisplayfiveTipographyFamilySizeonClick: PropTypes.any,
DisplayfiveTipographyFamilySizeonMouseEnter: PropTypes.any,
DisplayfiveTipographyFamilySizeonMouseOver: PropTypes.any,
DisplayfiveTipographyFamilySizeonKeyPress: PropTypes.any,
DisplayfiveTipographyFamilySizeonDrag: PropTypes.any,
DisplayfiveTipographyFamilySizeonMouseLeave: PropTypes.any,
DisplayfiveTipographyFamilySizeonMouseUp: PropTypes.any,
DisplayfiveTipographyFamilySizeonMouseDown: PropTypes.any,
DisplayfiveTipographyFamilySizeonKeyDown: PropTypes.any,
DisplayfiveTipographyFamilySizeonChange: PropTypes.any,
DisplayfiveTipographyFamilySizeondelay: PropTypes.any,
DisplayfiveTipographyonClick: PropTypes.any,
DisplayfiveTipographyonMouseEnter: PropTypes.any,
DisplayfiveTipographyonMouseOver: PropTypes.any,
DisplayfiveTipographyonKeyPress: PropTypes.any,
DisplayfiveTipographyonDrag: PropTypes.any,
DisplayfiveTipographyonMouseLeave: PropTypes.any,
DisplayfiveTipographyonMouseUp: PropTypes.any,
DisplayfiveTipographyonMouseDown: PropTypes.any,
DisplayfiveTipographyonKeyDown: PropTypes.any,
DisplayfiveTipographyonChange: PropTypes.any,
DisplayfiveTipographyondelay: PropTypes.any,
DisplayfiveTipographyNameonClick: PropTypes.any,
DisplayfiveTipographyNameonMouseEnter: PropTypes.any,
DisplayfiveTipographyNameonMouseOver: PropTypes.any,
DisplayfiveTipographyNameonKeyPress: PropTypes.any,
DisplayfiveTipographyNameonDrag: PropTypes.any,
DisplayfiveTipographyNameonMouseLeave: PropTypes.any,
DisplayfiveTipographyNameonMouseUp: PropTypes.any,
DisplayfiveTipographyNameonMouseDown: PropTypes.any,
DisplayfiveTipographyNameonKeyDown: PropTypes.any,
DisplayfiveTipographyNameonChange: PropTypes.any,
DisplayfiveTipographyNameondelay: PropTypes.any,
DisplayfiveTipographySizeonClick: PropTypes.any,
DisplayfiveTipographySizeonMouseEnter: PropTypes.any,
DisplayfiveTipographySizeonMouseOver: PropTypes.any,
DisplayfiveTipographySizeonKeyPress: PropTypes.any,
DisplayfiveTipographySizeonDrag: PropTypes.any,
DisplayfiveTipographySizeonMouseLeave: PropTypes.any,
DisplayfiveTipographySizeonMouseUp: PropTypes.any,
DisplayfiveTipographySizeonMouseDown: PropTypes.any,
DisplayfiveTipographySizeonKeyDown: PropTypes.any,
DisplayfiveTipographySizeonChange: PropTypes.any,
DisplayfiveTipographySizeondelay: PropTypes.any,
DisplayfiveTipographySizeTextonClick: PropTypes.any,
DisplayfiveTipographySizeTextonMouseEnter: PropTypes.any,
DisplayfiveTipographySizeTextonMouseOver: PropTypes.any,
DisplayfiveTipographySizeTextonKeyPress: PropTypes.any,
DisplayfiveTipographySizeTextonDrag: PropTypes.any,
DisplayfiveTipographySizeTextonMouseLeave: PropTypes.any,
DisplayfiveTipographySizeTextonMouseUp: PropTypes.any,
DisplayfiveTipographySizeTextonMouseDown: PropTypes.any,
DisplayfiveTipographySizeTextonKeyDown: PropTypes.any,
DisplayfiveTipographySizeTextonChange: PropTypes.any,
DisplayfiveTipographySizeTextondelay: PropTypes.any
}
export default TipographyWrapper;