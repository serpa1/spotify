import React, { Suspense } from 'react';
import PropTypes from 'prop-types';
import * as Contexts  from 'Contexts';
import { OverlaySwapContext } from 'Components/OverlaySwapProvider';
import CorebooksAnimation from 'Components/CorebooksAnimation';
import CorebooksAnimationSingle from 'Components/CorebooksAnimationSingle';

import * as UtilsScripts  from 'utils/utils';
import { Route, useHistory, useParams } from "react-router-dom";
import SingletoneNavigation from 'Class/SingletoneNavigation';

import LoadingComponent from "Components/Loading";


import { Transition } from "react-transition-group";
import { CSSTransition } from "react-transition-group";
import { useAppContext, useSessionContext } from 'context/AppContext';
import './Rectanglethree.css'





const Rectanglethree = (props)=>{
    const [state,dispatch] = useAppContext();
    const [, setNameSelectedComponent] = React.useContext(OverlaySwapContext);
    
    const nodeRefpropertyonedefault = React.useRef(null);
const nodeRefpropertyonevarianttwoo = React.useRef(null);
    const history = useHistory();
    let RouterContext = useParams();
    
    
    
    
    
    
    const [globalStateForm, dispatchForm] = React.useState({});
    const [ variant, setvariant] = React.useState(props.variant || 'Property 1=Default')
    const [ _in, setIn ] = React.useState(false)
    const [ transaction, setTransaction ] = React.useState({}) 
    const [ animation, setAnimation ] = React.useState('')

    
    
    
    
    
    
    React.useEffect(()=>{
        
        
        
    },[]);
    
    
    React.useEffect(()=>{
        
        setIn(false);
        setTimeout(()=>setIn(true))
           
      },[variant]);
    const switchVariant = (value)=>{
        switch (value) {
            case 'Property 1=Default':{
            return (
            <>

  <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['propertyonedefault']?.animationClass || {}}>

    <div id="id_fourthreeeight_onethreefourone" ref={nodeRefpropertyonedefault} className={` cursor ${ props.onClick ? 'cursor' : '' } rectanglethreepropertyonedefault C_fourthreeeight_onethreefourone ${ props.cssClass } ${ transaction['propertyonedefault']?.type ? transaction['propertyonedefault']?.type.toLowerCase() : '' }`} style={ { ...{ ...{}, transitionDuration: transaction['propertyonedefault']?.duration, transitionTimingFunction: transaction['propertyonedefault']?.timingFunction }, ...props.style }} onClick={ props.RectanglethreeonClick || function(e){ setTransaction({ }); setvariant('Property 1=Variant2'); }} onMouseEnter={ props.RectanglethreeonMouseEnter } onMouseOver={ props.RectanglethreeonMouseOver } onKeyPress={ props.RectanglethreeonKeyPress } onDrag={ props.RectanglethreeonDrag } onMouseLeave={ props.RectanglethreeonMouseLeave } onMouseUp={ props.RectanglethreeonMouseUp } onMouseDown={ props.RectanglethreeonMouseDown } onKeyDown={ props.RectanglethreeonKeyDown } onChange={ props.RectanglethreeonChange } ondelay={ props.Rectanglethreeondelay }>
      {
      props.children ?
      props.children :
      <>
        <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['rectanglethree']?.animationClass || {}}>
          <img id="id_fourthreeeight_onethreethreenigth" className={` rectangle rectanglethreerectanglethree ${ props.onClick ? 'cursor' : '' } ${ transaction['rectanglethree']?.type ? transaction['rectanglethree']?.type.toLowerCase() : '' }`} style={{ ...{},  ...props.RectanglethreeStyle , transitionDuration: transaction['rectanglethree']?.duration, transitionTimingFunction: transaction['rectanglethree']?.timingFunction }} onClick={ props.RectanglethreeonClick } onMouseEnter={ props.RectanglethreeonMouseEnter } onMouseOver={ props.RectanglethreeonMouseOver } onKeyPress={ props.RectanglethreeonKeyPress } onDrag={ props.RectanglethreeonDrag } onMouseLeave={ props.RectanglethreeonMouseLeave } onMouseUp={ props.RectanglethreeonMouseUp } onMouseDown={ props.RectanglethreeonMouseDown } onKeyDown={ props.RectanglethreeonKeyDown } onChange={ props.RectanglethreeonChange } ondelay={ props.Rectanglethreeondelay } src={props.Rectanglethree0 || "https://cdn.uncodie.com/tACQBrKCV97e0EqPE1XNLJ/d68dca10b4c92cdb394ecc9a3429c6412ce5b402.png" } />
        </CSSTransition>

      </>
      }
    </div>

  </CSSTransition>

</>
            )
        }
case 'Property 1=Variant2':{
            return (
            <>

  <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['propertyonevarianttwoo']?.animationClass || {}}>

    <div id="id_fourthreeeight_onethreefoureight" ref={nodeRefpropertyonevarianttwoo} className={` cursor ${ props.onClick ? 'cursor' : '' } rectanglethreepropertyonevarianttwoo C_fourthreeeight_onethreefoureight ${ props.cssClass } ${ transaction['propertyonevarianttwoo']?.type ? transaction['propertyonevarianttwoo']?.type.toLowerCase() : '' }`} style={ { ...{ ...{}, transitionDuration: transaction['propertyonevarianttwoo']?.duration, transitionTimingFunction: transaction['propertyonevarianttwoo']?.timingFunction }, ...props.style }} onClick={ props.RectanglethreeonClick || function(e){ setTransaction({ }); setvariant('Property 1=Default'); }} onMouseEnter={ props.RectanglethreeonMouseEnter } onMouseOver={ props.RectanglethreeonMouseOver } onKeyPress={ props.RectanglethreeonKeyPress } onDrag={ props.RectanglethreeonDrag } onMouseLeave={ props.RectanglethreeonMouseLeave } onMouseUp={ props.RectanglethreeonMouseUp } onMouseDown={ props.RectanglethreeonMouseDown } onKeyDown={ props.RectanglethreeonKeyDown } onChange={ props.RectanglethreeonChange } ondelay={ props.Rectanglethreeondelay }>
      {
      props.children ?
      props.children :
      <>
        <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['rectanglethree']?.animationClass || {}}>
          <img id="id_fourthreeeight_onethreefournigth" className={` rectangle rectanglethreerectanglethree ${ props.onClick ? 'cursor' : '' } ${ transaction['rectanglethree']?.type ? transaction['rectanglethree']?.type.toLowerCase() : '' }`} style={{ ...{},  ...props.RectanglethreeStyle , transitionDuration: transaction['rectanglethree']?.duration, transitionTimingFunction: transaction['rectanglethree']?.timingFunction }} onClick={ props.RectanglethreeonClick } onMouseEnter={ props.RectanglethreeonMouseEnter } onMouseOver={ props.RectanglethreeonMouseOver } onKeyPress={ props.RectanglethreeonKeyPress } onDrag={ props.RectanglethreeonDrag } onMouseLeave={ props.RectanglethreeonMouseLeave } onMouseUp={ props.RectanglethreeonMouseUp } onMouseDown={ props.RectanglethreeonMouseDown } onKeyDown={ props.RectanglethreeonKeyDown } onChange={ props.RectanglethreeonChange } ondelay={ props.Rectanglethreeondelay } src={props.Rectanglethree0 || "https://cdn.uncodie.com/tACQBrKCV97e0EqPE1XNLJ/5f3a866e5bc4045bd28d5c4b6433095970519e3b.png" } />
        </CSSTransition>

      </>
      }
    </div>

  </CSSTransition>

</>
            )
        }
        }
    }

    return (
        <>
            {switchVariant(variant)}
        </>
    )
    
}

Rectanglethree.propTypes = {
    style: PropTypes.any,
variant: PropTypes.any,
Rectanglethree0: PropTypes.any,
RectanglethreeonClick: PropTypes.any,
RectanglethreeonMouseEnter: PropTypes.any,
RectanglethreeonMouseOver: PropTypes.any,
RectanglethreeonKeyPress: PropTypes.any,
RectanglethreeonDrag: PropTypes.any,
RectanglethreeonMouseLeave: PropTypes.any,
RectanglethreeonMouseUp: PropTypes.any,
RectanglethreeonMouseDown: PropTypes.any,
RectanglethreeonKeyDown: PropTypes.any,
RectanglethreeonChange: PropTypes.any,
Rectanglethreeondelay: PropTypes.any
}
export default Rectanglethree;