import React, { Suspense } from 'react';
import PropTypes from 'prop-types';
import * as Contexts  from 'Contexts';
import { OverlaySwapContext } from 'Components/OverlaySwapProvider';
import CorebooksAnimation from 'Components/CorebooksAnimation';
import CorebooksAnimationSingle from 'Components/CorebooksAnimationSingle';

import * as UtilsScripts  from 'utils/utils';
import { Route, useHistory, useParams } from "react-router-dom";
import SingletoneNavigation from 'Class/SingletoneNavigation';

import LoadingComponent from "Components/Loading";


import { Transition } from "react-transition-group";
import { CSSTransition } from "react-transition-group";
import { useAppContext, useSessionContext } from 'context/AppContext';
import './HFive.css'





const HFive = (props)=>{
    const [state,dispatch] = useAppContext();
    const [, setNameSelectedComponent] = React.useContext(OverlaySwapContext);
    
    const nodeRef = React.useRef(null);
    const history = useHistory();
    let RouterContext = useParams();
    
    
    
    
    
    
    const [globalStateForm, dispatchForm] = React.useState({});
    
    const [ _in, setIn ] = React.useState(false)
    const [ transaction, setTransaction ] = React.useState({}) 
    const [ animation, setAnimation ] = React.useState('')

    
    
    
    
    
    
    React.useEffect(()=>{
        
        
        
    },[]);
    
    
    return (
        <>
  <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['hfive']?.animationClass || {}}>

    <div id="id_three_oneeightfournigth" ref={nodeRef} className={` ${ props.onClick ? 'cursor' : '' } hfive C_three_oneeightfournigth ${ props.cssClass } ${ transaction['hfive']?.type ? transaction['hfive']?.type.toLowerCase() : '' }`} style={ { ...{ ...{}, transitionDuration: transaction['hfive']?.duration, transitionTimingFunction: transaction['hfive']?.timingFunction }, ...props.style }} onClick={ props.HFiveonClick } onMouseEnter={ props.HFiveonMouseEnter } onMouseOver={ props.HFiveonMouseOver } onKeyPress={ props.HFiveonKeyPress } onDrag={ props.HFiveonDrag } onMouseLeave={ props.HFiveonMouseLeave } onMouseUp={ props.HFiveonMouseUp } onMouseDown={ props.HFiveonMouseDown } onKeyDown={ props.HFiveonKeyDown } onChange={ props.HFiveonChange } ondelay={ props.HFiveondelay }>
      {
      props.children ?
      props.children :
      <>
        <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['hfivetext']?.animationClass || {}}>

          <span id="id_one_onesevennigtheight"  className={` text hfivetext    ${ props.onClick ? 'cursor' : ''}  ${ transaction['hfivetext']?.type ? transaction['hfivetext']?.type.toLowerCase() : '' }`} style={{ ...{"fontSize":"0.875rem"},  ...props.HfiveTextStyle , transitionDuration: transaction['hfivetext']?.duration, transitionTimingFunction: transaction['hfivetext']?.timingFunction }} onClick={ props.HfiveTextonClick } onMouseEnter={ props.HfiveTextonMouseEnter } onMouseOver={ props.HfiveTextonMouseOver } onKeyPress={ props.HfiveTextonKeyPress } onDrag={ props.HfiveTextonDrag } onMouseLeave={ props.HfiveTextonMouseLeave } onMouseUp={ props.HfiveTextonMouseUp } onMouseDown={ props.HfiveTextonMouseDown } onKeyDown={ props.HfiveTextonKeyDown } onChange={ props.HfiveTextonChange } ondelay={ props.HfiveTextondelay } >{props.HfiveText0 || `Te mantendremos al tanto de los nuevos episodios`}</span>

        </CSSTransition>

      </>
      }
    </div>

  </CSSTransition>
</>
    ) 
}

HFive.propTypes = {
    style: PropTypes.any,
HfiveText0: PropTypes.any,
HFiveonClick: PropTypes.any,
HFiveonMouseEnter: PropTypes.any,
HFiveonMouseOver: PropTypes.any,
HFiveonKeyPress: PropTypes.any,
HFiveonDrag: PropTypes.any,
HFiveonMouseLeave: PropTypes.any,
HFiveonMouseUp: PropTypes.any,
HFiveonMouseDown: PropTypes.any,
HFiveonKeyDown: PropTypes.any,
HFiveonChange: PropTypes.any,
HFiveondelay: PropTypes.any,
HfiveTextonClick: PropTypes.any,
HfiveTextonMouseEnter: PropTypes.any,
HfiveTextonMouseOver: PropTypes.any,
HfiveTextonKeyPress: PropTypes.any,
HfiveTextonDrag: PropTypes.any,
HfiveTextonMouseLeave: PropTypes.any,
HfiveTextonMouseUp: PropTypes.any,
HfiveTextonMouseDown: PropTypes.any,
HfiveTextonKeyDown: PropTypes.any,
HfiveTextonChange: PropTypes.any,
HfiveTextondelay: PropTypes.any
}
export default HFive;