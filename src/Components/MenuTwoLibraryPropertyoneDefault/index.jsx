import React, { Suspense } from 'react';
import PropTypes from 'prop-types';
import * as Contexts  from 'Contexts';
import { OverlaySwapContext } from 'Components/OverlaySwapProvider';
import CorebooksAnimation from 'Components/CorebooksAnimation';
import CorebooksAnimationSingle from 'Components/CorebooksAnimationSingle';

import * as UtilsScripts  from 'utils/utils';
import { Route, useHistory, useParams } from "react-router-dom";
import SingletoneNavigation from 'Class/SingletoneNavigation';

import LoadingComponent from "Components/Loading";


import { Transition } from "react-transition-group";
import { CSSTransition } from "react-transition-group";
import HFive from 'Components/HFive'
import { useAppContext, useSessionContext } from 'context/AppContext';
import './MenuTwoLibraryPropertyoneDefault.css'





const PropertyoneDefault = (props)=>{
    const [state,dispatch] = useAppContext();
    const [, setNameSelectedComponent] = React.useContext(OverlaySwapContext);
    
    const nodeRef = React.useRef(null);
    const history = useHistory();
    let RouterContext = useParams();
    
    
    
    
    
    
    const [globalStateForm, dispatchForm] = React.useState({});
    
    const [ _in, setIn ] = React.useState(false)
    const [ transaction, setTransaction ] = React.useState({}) 
    const [ animation, setAnimation ] = React.useState('')

    
    
    
    
    
    
    React.useEffect(()=>{
        
        
        
    },[]);
    
    
    return (
        <>
  <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['propertyonedefault']?.animationClass || {}}>

    <div id="id_fourthreeeight_sixzerofourseven" ref={nodeRef} className={` cursor ${ props.onClick ? 'cursor' : '' } propertyonedefault C_fourthreeeight_sixzerofourseven ${ props.cssClass } ${ transaction['propertyonedefault']?.type ? transaction['propertyonedefault']?.type.toLowerCase() : '' }`} style={ { ...{ ...{}, transitionDuration: transaction['propertyonedefault']?.duration, transitionTimingFunction: transaction['propertyonedefault']?.timingFunction }, ...props.style }} onClick={ props.MenuTwoLibraryonClick } onMouseEnter={ props.MenuTwoLibraryonMouseEnter } onMouseOver={ props.MenuTwoLibraryonMouseOver || function(e){ setTransaction({ }); setvariant('Property 1=Variant2'); }} onKeyPress={ props.MenuTwoLibraryonKeyPress } onDrag={ props.MenuTwoLibraryonDrag } onMouseLeave={ props.MenuTwoLibraryonMouseLeave } onMouseUp={ props.MenuTwoLibraryonMouseUp } onMouseDown={ props.MenuTwoLibraryonMouseDown } onKeyDown={ props.MenuTwoLibraryonKeyDown } onChange={ props.MenuTwoLibraryonChange } ondelay={ props.MenuTwoLibraryondelay }>
      {
      props.children ?
      props.children :
      <>
        <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['menuhomeicon']?.animationClass || {}}>
          <img id="id_fourthreeeight_sixzerofourfour" className={` rectangle menuhomeicon ${ props.onClick ? 'cursor' : '' } ${ transaction['menuhomeicon']?.type ? transaction['menuhomeicon']?.type.toLowerCase() : '' }`} style={{ ...{},  ...props.MenuHomeIconStyle , transitionDuration: transaction['menuhomeicon']?.duration, transitionTimingFunction: transaction['menuhomeicon']?.timingFunction }} onClick={ props.MenuHomeIcononClick } onMouseEnter={ props.MenuHomeIcononMouseEnter } onMouseOver={ props.MenuHomeIcononMouseOver } onKeyPress={ props.MenuHomeIcononKeyPress } onDrag={ props.MenuHomeIcononDrag } onMouseLeave={ props.MenuHomeIcononMouseLeave } onMouseUp={ props.MenuHomeIcononMouseUp } onMouseDown={ props.MenuHomeIcononMouseDown } onKeyDown={ props.MenuHomeIcononKeyDown } onChange={ props.MenuHomeIcononChange } ondelay={ props.MenuHomeIconondelay } src={props.MenuHomeIcon0 || "https://cdn.uncodie.com/tACQBrKCV97e0EqPE1XNLJ/bfd90d37ac8d24b6af1abee4331ff3589619cd93.png" } />
        </CSSTransition>
        <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['hfive']?.animationClass || {}}>
          <HFive { ...{ ...props, style:false } } HfiveText0={ props.HfiveText0 || "Tu biblioteca" } HfiveText0={ props.HfiveText0 || "Tu biblioteca" } HfiveTextStyle={{"fontSize":"0.875rem"}} cssClass={"C_fourthreeeight_sixzerofourfive "}  />
    </CSSTransition >
            
            </>
        }
        </div>
    
    </CSSTransition >
            </>
        
    ) 
}

PropertyoneDefault.propTypes = {
    style: PropTypes.any,
MenuHomeIcon0: PropTypes.any,
HfiveText0: PropTypes.any,
MenuTwoLibraryonClick: PropTypes.any,
MenuTwoLibraryonMouseEnter: PropTypes.any,
MenuTwoLibraryonMouseOver: PropTypes.any,
MenuTwoLibraryonKeyPress: PropTypes.any,
MenuTwoLibraryonDrag: PropTypes.any,
MenuTwoLibraryonMouseLeave: PropTypes.any,
MenuTwoLibraryonMouseUp: PropTypes.any,
MenuTwoLibraryonMouseDown: PropTypes.any,
MenuTwoLibraryonKeyDown: PropTypes.any,
MenuTwoLibraryonChange: PropTypes.any,
MenuTwoLibraryondelay: PropTypes.any,
MenuHomeIcononClick: PropTypes.any,
MenuHomeIcononMouseEnter: PropTypes.any,
MenuHomeIcononMouseOver: PropTypes.any,
MenuHomeIcononKeyPress: PropTypes.any,
MenuHomeIcononDrag: PropTypes.any,
MenuHomeIcononMouseLeave: PropTypes.any,
MenuHomeIcononMouseUp: PropTypes.any,
MenuHomeIcononMouseDown: PropTypes.any,
MenuHomeIcononKeyDown: PropTypes.any,
MenuHomeIcononChange: PropTypes.any,
MenuHomeIconondelay: PropTypes.any
}
export default PropertyoneDefault;