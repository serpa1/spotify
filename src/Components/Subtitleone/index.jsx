import React, { Suspense } from 'react';
import PropTypes from 'prop-types';
import * as Contexts  from 'Contexts';
import { OverlaySwapContext } from 'Components/OverlaySwapProvider';
import CorebooksAnimation from 'Components/CorebooksAnimation';
import CorebooksAnimationSingle from 'Components/CorebooksAnimationSingle';

import * as UtilsScripts  from 'utils/utils';
import { Route, useHistory, useParams } from "react-router-dom";
import SingletoneNavigation from 'Class/SingletoneNavigation';

import LoadingComponent from "Components/Loading";


import { Transition } from "react-transition-group";
import { CSSTransition } from "react-transition-group";
import { useAppContext, useSessionContext } from 'context/AppContext';
import './Subtitleone.css'





const Subtitleone = (props)=>{
    const [state,dispatch] = useAppContext();
    const [, setNameSelectedComponent] = React.useContext(OverlaySwapContext);
    
    const nodeRef = React.useRef(null);
    const history = useHistory();
    let RouterContext = useParams();
    
    
    
    
    
    
    const [globalStateForm, dispatchForm] = React.useState({});
    
    const [ _in, setIn ] = React.useState(false)
    const [ transaction, setTransaction ] = React.useState({}) 
    const [ animation, setAnimation ] = React.useState('')

    
    
    
    
    
    
    React.useEffect(()=>{
        
        
        
    },[]);
    
    
    return (
        <>
  <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['subtitleone']?.animationClass || {}}>

    <div id="id_three_oneeightfivezero" ref={nodeRef} className={` ${ props.onClick ? 'cursor' : '' } subtitleone C_three_oneeightfivezero ${ props.cssClass } ${ transaction['subtitleone']?.type ? transaction['subtitleone']?.type.toLowerCase() : '' }`} style={ { ...{ ...{}, transitionDuration: transaction['subtitleone']?.duration, transitionTimingFunction: transaction['subtitleone']?.timingFunction }, ...props.style }} onClick={ props.SubtitleoneonClick } onMouseEnter={ props.SubtitleoneonMouseEnter } onMouseOver={ props.SubtitleoneonMouseOver } onKeyPress={ props.SubtitleoneonKeyPress } onDrag={ props.SubtitleoneonDrag } onMouseLeave={ props.SubtitleoneonMouseLeave } onMouseUp={ props.SubtitleoneonMouseUp } onMouseDown={ props.SubtitleoneonMouseDown } onKeyDown={ props.SubtitleoneonKeyDown } onChange={ props.SubtitleoneonChange } ondelay={ props.Subtitleoneondelay }>
      {
      props.children ?
      props.children :
      <>
        <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['subtilteonetext']?.animationClass || {}}>

          <span id="id_one_nigthzeroone"  className={` text subtilteonetext    ${ props.onClick ? 'cursor' : ''}  ${ transaction['subtilteonetext']?.type ? transaction['subtilteonetext']?.type.toLowerCase() : '' }`} style={{ ...{},  ...props.SubtilteoneTextStyle , transitionDuration: transaction['subtilteonetext']?.duration, transitionTimingFunction: transaction['subtilteonetext']?.timingFunction }} onClick={ props.SubtilteoneTextonClick } onMouseEnter={ props.SubtilteoneTextonMouseEnter } onMouseOver={ props.SubtilteoneTextonMouseOver } onKeyPress={ props.SubtilteoneTextonKeyPress } onDrag={ props.SubtilteoneTextonDrag } onMouseLeave={ props.SubtilteoneTextonMouseLeave } onMouseUp={ props.SubtilteoneTextonMouseUp } onMouseDown={ props.SubtilteoneTextonMouseDown } onKeyDown={ props.SubtilteoneTextonKeyDown } onChange={ props.SubtilteoneTextonChange } ondelay={ props.SubtilteoneTextondelay } >{props.SubtilteoneText0 || `Busquemos algunos podcast para seguir`}</span>

        </CSSTransition>

      </>
      }
    </div>

  </CSSTransition>
</>
    ) 
}

Subtitleone.propTypes = {
    style: PropTypes.any,
SubtilteoneText0: PropTypes.any,
SubtitleoneonClick: PropTypes.any,
SubtitleoneonMouseEnter: PropTypes.any,
SubtitleoneonMouseOver: PropTypes.any,
SubtitleoneonKeyPress: PropTypes.any,
SubtitleoneonDrag: PropTypes.any,
SubtitleoneonMouseLeave: PropTypes.any,
SubtitleoneonMouseUp: PropTypes.any,
SubtitleoneonMouseDown: PropTypes.any,
SubtitleoneonKeyDown: PropTypes.any,
SubtitleoneonChange: PropTypes.any,
Subtitleoneondelay: PropTypes.any,
SubtilteoneTextonClick: PropTypes.any,
SubtilteoneTextonMouseEnter: PropTypes.any,
SubtilteoneTextonMouseOver: PropTypes.any,
SubtilteoneTextonKeyPress: PropTypes.any,
SubtilteoneTextonDrag: PropTypes.any,
SubtilteoneTextonMouseLeave: PropTypes.any,
SubtilteoneTextonMouseUp: PropTypes.any,
SubtilteoneTextonMouseDown: PropTypes.any,
SubtilteoneTextonKeyDown: PropTypes.any,
SubtilteoneTextonChange: PropTypes.any,
SubtilteoneTextondelay: PropTypes.any
}
export default Subtitleone;