import React, { Suspense } from 'react';
import PropTypes from 'prop-types';
import * as Contexts  from 'Contexts';
import { OverlaySwapContext } from 'Components/OverlaySwapProvider';
import CorebooksAnimation from 'Components/CorebooksAnimation';
import CorebooksAnimationSingle from 'Components/CorebooksAnimationSingle';

import * as UtilsScripts  from 'utils/utils';
import { Route, useHistory, useParams } from "react-router-dom";
import SingletoneNavigation from 'Class/SingletoneNavigation';

import LoadingComponent from "Components/Loading";


import { Transition } from "react-transition-group";
import { CSSTransition } from "react-transition-group";
import { useAppContext, useSessionContext } from 'context/AppContext';
import './SecundarTwoButtonPropertyoneVarianttwoo.css'





const PropertyoneVarianttwoo = (props)=>{
    const [state,dispatch] = useAppContext();
    const [, setNameSelectedComponent] = React.useContext(OverlaySwapContext);
    
    const nodeRef = React.useRef(null);
    const history = useHistory();
    let RouterContext = useParams();
    
    
    
    
    
    
    const [globalStateForm, dispatchForm] = React.useState({});
    
    const [ _in, setIn ] = React.useState(false)
    const [ transaction, setTransaction ] = React.useState({}) 
    const [ animation, setAnimation ] = React.useState('')

    
    
    
    
    
    
    React.useEffect(()=>{
        
        
        
    },[]);
    
    
    return (
        <>
  <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['propertyonevarianttwoo']?.animationClass || {}}>

    <div id="id_threeonetwoo_oneonetwoofour" ref={nodeRef} className={` ${ props.onClick ? 'cursor' : '' } propertyonevarianttwoo C_threeonetwoo_oneonetwoofour ${ props.cssClass } ${ transaction['propertyonevarianttwoo']?.type ? transaction['propertyonevarianttwoo']?.type.toLowerCase() : '' }`} style={ { ...{ ...{"minWidth":"250px"}, transitionDuration: transaction['propertyonevarianttwoo']?.duration, transitionTimingFunction: transaction['propertyonevarianttwoo']?.timingFunction }, ...props.style }} onClick={ props.SecundarTwoButtononClick } onMouseEnter={ props.SecundarTwoButtononMouseEnter } onMouseOver={ props.SecundarTwoButtononMouseOver } onKeyPress={ props.SecundarTwoButtononKeyPress } onDrag={ props.SecundarTwoButtononDrag } onMouseLeave={ props.SecundarTwoButtononMouseLeave } onMouseUp={ props.SecundarTwoButtononMouseUp } onMouseDown={ props.SecundarTwoButtononMouseDown } onKeyDown={ props.SecundarTwoButtononKeyDown } onChange={ props.SecundarTwoButtononChange } ondelay={ props.SecundarTwoButtonondelay }>
      {
      props.children ?
      props.children :
      <>
        <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['principalbuttontext']?.animationClass || {}}>

          <span id="id_threeonetwoo_oneonetwoofive"  className={` text principalbuttontext    ${ props.onClick ? 'cursor' : ''}  ${ transaction['principalbuttontext']?.type ? transaction['principalbuttontext']?.type.toLowerCase() : '' }`} style={{ ...{"height":"14px"},  ...props.PrincipalButtonTextStyle , transitionDuration: transaction['principalbuttontext']?.duration, transitionTimingFunction: transaction['principalbuttontext']?.timingFunction }} onClick={ props.PrincipalButtonTextonClick } onMouseEnter={ props.PrincipalButtonTextonMouseEnter } onMouseOver={ props.PrincipalButtonTextonMouseOver } onKeyPress={ props.PrincipalButtonTextonKeyPress } onDrag={ props.PrincipalButtonTextonDrag } onMouseLeave={ props.PrincipalButtonTextonMouseLeave } onMouseUp={ props.PrincipalButtonTextonMouseUp } onMouseDown={ props.PrincipalButtonTextonMouseDown } onKeyDown={ props.PrincipalButtonTextonKeyDown } onChange={ props.PrincipalButtonTextonChange } ondelay={ props.PrincipalButtonTextondelay } >{props.PrincipalButtonText0 || `VER PLANES`}</span>

        </CSSTransition>

      </>
      }
    </div>

  </CSSTransition>
</>
    ) 
}

PropertyoneVarianttwoo.propTypes = {
    style: PropTypes.any,
PrincipalButtonText0: PropTypes.any,
SecundarTwoButtononClick: PropTypes.any,
SecundarTwoButtononMouseEnter: PropTypes.any,
SecundarTwoButtononMouseOver: PropTypes.any,
SecundarTwoButtononKeyPress: PropTypes.any,
SecundarTwoButtononDrag: PropTypes.any,
SecundarTwoButtononMouseLeave: PropTypes.any,
SecundarTwoButtononMouseUp: PropTypes.any,
SecundarTwoButtononMouseDown: PropTypes.any,
SecundarTwoButtononKeyDown: PropTypes.any,
SecundarTwoButtononChange: PropTypes.any,
SecundarTwoButtonondelay: PropTypes.any,
PrincipalButtonTextonClick: PropTypes.any,
PrincipalButtonTextonMouseEnter: PropTypes.any,
PrincipalButtonTextonMouseOver: PropTypes.any,
PrincipalButtonTextonKeyPress: PropTypes.any,
PrincipalButtonTextonDrag: PropTypes.any,
PrincipalButtonTextonMouseLeave: PropTypes.any,
PrincipalButtonTextonMouseUp: PropTypes.any,
PrincipalButtonTextonMouseDown: PropTypes.any,
PrincipalButtonTextonKeyDown: PropTypes.any,
PrincipalButtonTextonChange: PropTypes.any,
PrincipalButtonTextondelay: PropTypes.any
}
export default PropertyoneVarianttwoo;