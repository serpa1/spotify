import React, { Suspense } from 'react';
import PropTypes from 'prop-types';
import * as Contexts  from 'Contexts';
import { OverlaySwapContext } from 'Components/OverlaySwapProvider';
import CorebooksAnimation from 'Components/CorebooksAnimation';
import CorebooksAnimationSingle from 'Components/CorebooksAnimationSingle';

import * as UtilsScripts  from 'utils/utils';
import { Route, useHistory, useParams } from "react-router-dom";
import SingletoneNavigation from 'Class/SingletoneNavigation';

import LoadingComponent from "Components/Loading";


import { Transition } from "react-transition-group";
import { CSSTransition } from "react-transition-group";
import { useAppContext, useSessionContext } from 'context/AppContext';
import './PlayIconAnimationPropertyoneDefault.css'





const PropertyoneDefault = (props)=>{
    const [state,dispatch] = useAppContext();
    const [, setNameSelectedComponent] = React.useContext(OverlaySwapContext);
    
    const nodeRef = React.useRef(null);
    const history = useHistory();
    let RouterContext = useParams();
    
    
    
    
    
    
    const [globalStateForm, dispatchForm] = React.useState({});
    
    const [ _in, setIn ] = React.useState(false)
    const [ transaction, setTransaction ] = React.useState({}) 
    const [ animation, setAnimation ] = React.useState('')

    
    
    
    
    
    
    React.useEffect(()=>{
        
        
        
    },[]);
    
    
    return (
        <>
  <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['propertyonedefault']?.animationClass || {}}>

    <div id="id_twooone_threezerotwoo" ref={nodeRef} className={` cursor ${ props.onClick ? 'cursor' : '' } propertyonedefault C_twooone_threezerotwoo ${ props.cssClass } ${ transaction['propertyonedefault']?.type ? transaction['propertyonedefault']?.type.toLowerCase() : '' }`} style={ { ...{ ...{}, transitionDuration: transaction['propertyonedefault']?.duration, transitionTimingFunction: transaction['propertyonedefault']?.timingFunction }, ...props.style }} onClick={ props.PlayIconAnimationonClick } onMouseEnter={ props.PlayIconAnimationonMouseEnter } onMouseOver={ props.PlayIconAnimationonMouseOver || function(e){ setTransaction({ }); setvariant('Property 1=Variant2'); }} onKeyPress={ props.PlayIconAnimationonKeyPress } onDrag={ props.PlayIconAnimationonDrag } onMouseLeave={ props.PlayIconAnimationonMouseLeave } onMouseUp={ props.PlayIconAnimationonMouseUp } onMouseDown={ props.PlayIconAnimationonMouseDown } onKeyDown={ props.PlayIconAnimationonKeyDown } onChange={ props.PlayIconAnimationonChange } ondelay={ props.PlayIconAnimationondelay }>
      {
      props.children ?
      props.children :
      <>
        <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['frametwoosixseven']?.animationClass || {}}>

          <div id="id_sixnigth_eightsevenone" className={` frame frametwoosixseven ${ props.onClick ? 'cursor' : '' } ${ transaction['frametwoosixseven']?.type ? transaction['frametwoosixseven']?.type.toLowerCase() : '' }`} style={ { ...{}, ...props.FrametwoosixsevenStyle , transitionDuration: transaction['frametwoosixseven']?.duration, transitionTimingFunction: transaction['frametwoosixseven']?.timingFunction } } onClick={ props.FrametwoosixsevenonClick } onMouseEnter={ props.FrametwoosixsevenonMouseEnter } onMouseOver={ props.FrametwoosixsevenonMouseOver } onKeyPress={ props.FrametwoosixsevenonKeyPress } onDrag={ props.FrametwoosixsevenonDrag } onMouseLeave={ props.FrametwoosixsevenonMouseLeave } onMouseUp={ props.FrametwoosixsevenonMouseUp } onMouseDown={ props.FrametwoosixsevenonMouseDown } onKeyDown={ props.FrametwoosixsevenonKeyDown } onChange={ props.FrametwoosixsevenonChange } ondelay={ props.Frametwoosixsevenondelay }>
            <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['playiconvectoranimation']?.animationClass || {}}>
              <svg id="id_twooone_twoonigthnigth" className="playiconvectoranimation  " style={{}} onClick={ props.PlayIconVectorAnimationonClick } onMouseEnter={ props.PlayIconVectorAnimationonMouseEnter } onMouseOver={ props.PlayIconVectorAnimationonMouseOver } onKeyPress={ props.PlayIconVectorAnimationonKeyPress } onDrag={ props.PlayIconVectorAnimationonDrag } onMouseLeave={ props.PlayIconVectorAnimationonMouseLeave } onMouseUp={ props.PlayIconVectorAnimationonMouseUp } onMouseDown={ props.PlayIconVectorAnimationonMouseDown } onKeyDown={ props.PlayIconVectorAnimationonKeyDown } onChange={ props.PlayIconVectorAnimationonChange } ondelay={ props.PlayIconVectorAnimationondelay } width="14.889892578125" height="16.97998046875">
                <path d="M1.05003 0.0938067L14.5404 7.88384C14.6467 7.94533 14.735 8.0337 14.7963 8.1401C14.8577 8.24649 14.89 8.36717 14.89 8.49C14.89 8.61283 14.8577 8.73351 14.7963 8.8399C14.735 8.9463 14.6467 9.03468 14.5404 9.09616L1.05003 16.8862C0.943627 16.9476 0.822933 16.98 0.700072 16.98C0.577212 16.98 0.456513 16.9477 0.350103 16.8862C0.243693 16.8248 0.155321 16.7365 0.0938663 16.6301C0.0324114 16.5236 3.82206e-05 16.4029 0 16.28L0 0.699965C3.82206e-05 0.577076 0.0324114 0.456361 0.0938663 0.34995C0.155321 0.243539 0.243693 0.15518 0.350103 0.0937523C0.456513 0.0323242 0.577212 -9.56044e-06 0.700072 2.12045e-09C0.822933 9.56468e-06 0.943627 0.032362 1.05003 0.0938067Z" />
              </svg>
            </CSSTransition>
          </div>

        </CSSTransition>

      </>
      }
    </div>

  </CSSTransition>
</>
    ) 
}

PropertyoneDefault.propTypes = {
    style: PropTypes.any,
PlayIconAnimationonClick: PropTypes.any,
PlayIconAnimationonMouseEnter: PropTypes.any,
PlayIconAnimationonMouseOver: PropTypes.any,
PlayIconAnimationonKeyPress: PropTypes.any,
PlayIconAnimationonDrag: PropTypes.any,
PlayIconAnimationonMouseLeave: PropTypes.any,
PlayIconAnimationonMouseUp: PropTypes.any,
PlayIconAnimationonMouseDown: PropTypes.any,
PlayIconAnimationonKeyDown: PropTypes.any,
PlayIconAnimationonChange: PropTypes.any,
PlayIconAnimationondelay: PropTypes.any,
FrametwoosixsevenonClick: PropTypes.any,
FrametwoosixsevenonMouseEnter: PropTypes.any,
FrametwoosixsevenonMouseOver: PropTypes.any,
FrametwoosixsevenonKeyPress: PropTypes.any,
FrametwoosixsevenonDrag: PropTypes.any,
FrametwoosixsevenonMouseLeave: PropTypes.any,
FrametwoosixsevenonMouseUp: PropTypes.any,
FrametwoosixsevenonMouseDown: PropTypes.any,
FrametwoosixsevenonKeyDown: PropTypes.any,
FrametwoosixsevenonChange: PropTypes.any,
Frametwoosixsevenondelay: PropTypes.any,
PlayIconVectorAnimationonClick: PropTypes.any,
PlayIconVectorAnimationonMouseEnter: PropTypes.any,
PlayIconVectorAnimationonMouseOver: PropTypes.any,
PlayIconVectorAnimationonKeyPress: PropTypes.any,
PlayIconVectorAnimationonDrag: PropTypes.any,
PlayIconVectorAnimationonMouseLeave: PropTypes.any,
PlayIconVectorAnimationonMouseUp: PropTypes.any,
PlayIconVectorAnimationonMouseDown: PropTypes.any,
PlayIconVectorAnimationonKeyDown: PropTypes.any,
PlayIconVectorAnimationonChange: PropTypes.any,
PlayIconVectorAnimationondelay: PropTypes.any
}
export default PropertyoneDefault;