import React, { Suspense } from 'react';
import PropTypes from 'prop-types';
import * as Contexts  from 'Contexts';
import { OverlaySwapContext } from 'Components/OverlaySwapProvider';
import CorebooksAnimation from 'Components/CorebooksAnimation';
import CorebooksAnimationSingle from 'Components/CorebooksAnimationSingle';

import * as UtilsScripts  from 'utils/utils';
import { Route, useHistory, useParams } from "react-router-dom";
import SingletoneNavigation from 'Class/SingletoneNavigation';

import LoadingComponent from "Components/Loading";


import { Transition } from "react-transition-group";
import { CSSTransition } from "react-transition-group";
import { useAppContext, useSessionContext } from 'context/AppContext';
import './SubtitleTwoo.css'





const SubtitleTwoo = (props)=>{
    const [state,dispatch] = useAppContext();
    const [, setNameSelectedComponent] = React.useContext(OverlaySwapContext);
    
    const nodeRef = React.useRef(null);
    const history = useHistory();
    let RouterContext = useParams();
    
    
    
    
    
    
    const [globalStateForm, dispatchForm] = React.useState({});
    
    const [ _in, setIn ] = React.useState(false)
    const [ transaction, setTransaction ] = React.useState({}) 
    const [ animation, setAnimation ] = React.useState('')

    
    
    
    
    
    
    React.useEffect(()=>{
        
        
        
    },[]);
    
    
    return (
        <>
  <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['subtitletwoo']?.animationClass || {}}>

    <div id="id_three_oneeightfivethree" ref={nodeRef} className={` ${ props.onClick ? 'cursor' : '' } subtitletwoo C_three_oneeightfivethree ${ props.cssClass } ${ transaction['subtitletwoo']?.type ? transaction['subtitletwoo']?.type.toLowerCase() : '' }`} style={ { ...{ ...{}, transitionDuration: transaction['subtitletwoo']?.duration, transitionTimingFunction: transaction['subtitletwoo']?.timingFunction }, ...props.style }} onClick={ props.SubtitleTwooonClick } onMouseEnter={ props.SubtitleTwooonMouseEnter } onMouseOver={ props.SubtitleTwooonMouseOver } onKeyPress={ props.SubtitleTwooonKeyPress } onDrag={ props.SubtitleTwooonDrag } onMouseLeave={ props.SubtitleTwooonMouseLeave } onMouseUp={ props.SubtitleTwooonMouseUp } onMouseDown={ props.SubtitleTwooonMouseDown } onKeyDown={ props.SubtitleTwooonKeyDown } onChange={ props.SubtitleTwooonChange } ondelay={ props.SubtitleTwooondelay }>
      {
      props.children ?
      props.children :
      <>
        <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['subtitletwootext']?.animationClass || {}}>

          <span id="id_one_eightnigthsix"  className={` text subtitletwootext    ${ props.onClick ? 'cursor' : ''}  ${ transaction['subtitletwootext']?.type ? transaction['subtitletwootext']?.type.toLowerCase() : '' }`} style={{ ...{},  ...props.SubtitletwooTextStyle , transitionDuration: transaction['subtitletwootext']?.duration, transitionTimingFunction: transaction['subtitletwootext']?.timingFunction }} onClick={ props.SubtitletwooTextonClick } onMouseEnter={ props.SubtitletwooTextonMouseEnter } onMouseOver={ props.SubtitletwooTextonMouseOver } onKeyPress={ props.SubtitletwooTextonKeyPress } onDrag={ props.SubtitletwooTextonDrag } onMouseLeave={ props.SubtitletwooTextonMouseLeave } onMouseUp={ props.SubtitletwooTextonMouseUp } onMouseDown={ props.SubtitletwooTextonMouseDown } onKeyDown={ props.SubtitletwooTextonKeyDown } onChange={ props.SubtitletwooTextonChange } ondelay={ props.SubtitletwooTextondelay } >{props.SubtitletwooText0 || `Legal`}</span>

        </CSSTransition>

      </>
      }
    </div>

  </CSSTransition>
</>
    ) 
}

SubtitleTwoo.propTypes = {
    style: PropTypes.any,
SubtitletwooText0: PropTypes.any,
SubtitleTwooonClick: PropTypes.any,
SubtitleTwooonMouseEnter: PropTypes.any,
SubtitleTwooonMouseOver: PropTypes.any,
SubtitleTwooonKeyPress: PropTypes.any,
SubtitleTwooonDrag: PropTypes.any,
SubtitleTwooonMouseLeave: PropTypes.any,
SubtitleTwooonMouseUp: PropTypes.any,
SubtitleTwooonMouseDown: PropTypes.any,
SubtitleTwooonKeyDown: PropTypes.any,
SubtitleTwooonChange: PropTypes.any,
SubtitleTwooondelay: PropTypes.any,
SubtitletwooTextonClick: PropTypes.any,
SubtitletwooTextonMouseEnter: PropTypes.any,
SubtitletwooTextonMouseOver: PropTypes.any,
SubtitletwooTextonKeyPress: PropTypes.any,
SubtitletwooTextonDrag: PropTypes.any,
SubtitletwooTextonMouseLeave: PropTypes.any,
SubtitletwooTextonMouseUp: PropTypes.any,
SubtitletwooTextonMouseDown: PropTypes.any,
SubtitletwooTextonKeyDown: PropTypes.any,
SubtitletwooTextonChange: PropTypes.any,
SubtitletwooTextondelay: PropTypes.any
}
export default SubtitleTwoo;