import React, { Suspense } from 'react';
import PropTypes from 'prop-types';
import * as Contexts  from 'Contexts';
import { OverlaySwapContext } from 'Components/OverlaySwapProvider';
import CorebooksAnimation from 'Components/CorebooksAnimation';
import CorebooksAnimationSingle from 'Components/CorebooksAnimationSingle';

import * as UtilsScripts  from 'utils/utils';
import { Route, useHistory, useParams } from "react-router-dom";
import SingletoneNavigation from 'Class/SingletoneNavigation';

import LoadingComponent from "Components/Loading";


import { Transition } from "react-transition-group";
import { CSSTransition } from "react-transition-group";
import Subtitleone from 'Components/Subtitleone'
import { useAppContext, useSessionContext } from 'context/AppContext';
import './HeaderMPropertyoneHeaderExtM.css'





const PropertyoneHeaderExtM = (props)=>{
    const [state,dispatch] = useAppContext();
    const [, setNameSelectedComponent] = React.useContext(OverlaySwapContext);
    
    const nodeRef = React.useRef(null);
    const history = useHistory();
    let RouterContext = useParams();
    
    
    
    
    
    
    const [globalStateForm, dispatchForm] = React.useState({});
    
    const [ _in, setIn ] = React.useState(false)
    const [ transaction, setTransaction ] = React.useState({}) 
    const [ animation, setAnimation ] = React.useState('')

    
    
    
    
    
    
    React.useEffect(()=>{
        
        
        
    },[]);
    
    
    return (
        <>
  <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['propertyoneheaderextm']?.animationClass || {}}>

    <div id="id_twoofiveeight_onesixfourthree" ref={nodeRef} className={` ${ props.onClick ? 'cursor' : '' } propertyoneheaderextm show-xs show-s show-m hide-l hide-xl C_twoofiveeight_onesixfourthree ${ props.cssClass } ${ transaction['propertyoneheaderextm']?.type ? transaction['propertyoneheaderextm']?.type.toLowerCase() : '' }`} style={ { ...{ ...{}, transitionDuration: transaction['propertyoneheaderextm']?.duration, transitionTimingFunction: transaction['propertyoneheaderextm']?.timingFunction }, ...props.style }} onClick={ props.HeaderMonClick } onMouseEnter={ props.HeaderMonMouseEnter } onMouseOver={ props.HeaderMonMouseOver } onKeyPress={ props.HeaderMonKeyPress } onDrag={ props.HeaderMonDrag } onMouseLeave={ props.HeaderMonMouseLeave } onMouseUp={ props.HeaderMonMouseUp } onMouseDown={ props.HeaderMonMouseDown } onKeyDown={ props.HeaderMonKeyDown } onChange={ props.HeaderMonChange } ondelay={ props.HeaderMondelay }>
      {
      props.children ?
      props.children :
      <>
        <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['headercont']?.animationClass || {}}>

          <div id="id_twoofiveeight_onesixfourfour" className={` frame headercont cursor ${ props.onClick ? 'cursor' : '' } ${ transaction['headercont']?.type ? transaction['headercont']?.type.toLowerCase() : '' }`} style={ { ...{}, ...props.HeaderContStyle , transitionDuration: transaction['headercont']?.duration, transitionTimingFunction: transaction['headercont']?.timingFunction } } onClick={ props.HeaderContonClick || function(e){ setTransaction({ }); setvariant('Property1=HeaderM'); }} onMouseEnter={ props.HeaderContonMouseEnter } onMouseOver={ props.HeaderContonMouseOver } onKeyPress={ props.HeaderContonKeyPress } onDrag={ props.HeaderContonDrag } onMouseLeave={ props.HeaderContonMouseLeave } onMouseUp={ props.HeaderContonMouseUp } onMouseDown={ props.HeaderContonMouseDown } onKeyDown={ props.HeaderContonKeyDown } onChange={ props.HeaderContonChange } ondelay={ props.HeaderContondelay }>
            <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['uncodie_logom']?.animationClass || {}}>
              <img id="id_twoofiveeight_onesixfourfive" className={` rectangle uncodie_logom ${ props.onClick ? 'cursor' : '' } ${ transaction['uncodie_logom']?.type ? transaction['uncodie_logom']?.type.toLowerCase() : '' }`} style={{ ...{},  ...props.Uncodie_logoMStyle , transitionDuration: transaction['uncodie_logom']?.duration, transitionTimingFunction: transaction['uncodie_logom']?.timingFunction }} onClick={ props.Uncodie_logoMonClick } onMouseEnter={ props.Uncodie_logoMonMouseEnter } onMouseOver={ props.Uncodie_logoMonMouseOver } onKeyPress={ props.Uncodie_logoMonKeyPress } onDrag={ props.Uncodie_logoMonDrag } onMouseLeave={ props.Uncodie_logoMonMouseLeave } onMouseUp={ props.Uncodie_logoMonMouseUp } onMouseDown={ props.Uncodie_logoMonMouseDown } onKeyDown={ props.Uncodie_logoMonKeyDown } onChange={ props.Uncodie_logoMonChange } ondelay={ props.Uncodie_logoMondelay } src={props.Uncodie_logoM0 || "https://cdn.uncodie.com/tACQBrKCV97e0EqPE1XNLJ/efbb152934f48228779350567e3e051c143ce0f2.png" } />
            </CSSTransition>
            <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['icon']?.animationClass || {}}>
              <img id="id_twoofiveeight_onesixfoursix" className={` rectangle icon cursor ${ props.onClick ? 'cursor' : '' } ${ transaction['icon']?.type ? transaction['icon']?.type.toLowerCase() : '' }`} style={{ ...{},  ...props.IconStyle , transitionDuration: transaction['icon']?.duration, transitionTimingFunction: transaction['icon']?.timingFunction }} onClick={ props.IcononClick || function(e){ setTransaction({ }); setvariant('Property1=HeaderM'); }} onMouseEnter={ props.IcononMouseEnter } onMouseOver={ props.IcononMouseOver } onKeyPress={ props.IcononKeyPress } onDrag={ props.IcononDrag } onMouseLeave={ props.IcononMouseLeave } onMouseUp={ props.IcononMouseUp } onMouseDown={ props.IcononMouseDown } onKeyDown={ props.IcononKeyDown } onChange={ props.IcononChange } ondelay={ props.Iconondelay } src={props.Icon0 || "https://cdn.uncodie.com/tACQBrKCV97e0EqPE1XNLJ/421fe09abe5716dce1eeaef9fd78c31f99756fd9.png" } />
            </CSSTransition>
          </div>

        </CSSTransition>
        <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['subtitleone']?.animationClass || {}}>
          <Subtitleone { ...{ ...props, style:false } } SubtilteoneText0={ props.SubtilteoneText0 || "Iniciar Sesión" } cssClass={"C_twoofiveeight_onesevenonethree "}  />
    </CSSTransition >
<CSSTransition 
        in={_in}
        appear={true} 
        timeout={100}
        classNames={transaction['subtitleone']?.animationClass || {}}
    >
    <Subtitleone { ...{ ...props, style:false } }   SubtilteoneText0={ props.SubtilteoneText1 || " Registrate"} cssClass={"C_twoofiveeight_onesevenonefour "}  />
    </CSSTransition >
<CSSTransition 
        in={_in}
        appear={true} 
        timeout={100}
        classNames={transaction['menudivmobile']?.animationClass || {}}
    >
    
                    <div id="id_twoofiveeight_onesevenonefive" className={` frame menudivmobile ${ props.onClick ? 'cursor' : '' } ${ transaction['menudivmobile']?.type ? transaction['menudivmobile']?.type.toLowerCase() : '' }`} style={ { ...{}, ...props.MenuDivMobileStyle , transitionDuration: transaction['menudivmobile']?.duration, transitionTimingFunction: transaction['menudivmobile']?.timingFunction } } onClick={ props.MenuDivMobileonClick } onMouseEnter={ props.MenuDivMobileonMouseEnter } onMouseOver={ props.MenuDivMobileonMouseOver } onKeyPress={ props.MenuDivMobileonKeyPress } onDrag={ props.MenuDivMobileonDrag } onMouseLeave={ props.MenuDivMobileonMouseLeave } onMouseUp={ props.MenuDivMobileonMouseUp } onMouseDown={ props.MenuDivMobileonMouseDown } onKeyDown={ props.MenuDivMobileonKeyDown } onChange={ props.MenuDivMobileonChange } ondelay={ props.MenuDivMobileondelay }>

    </div>

  </CSSTransition>
  <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['menuoptionmobile']?.animationClass || {}}>

    <div id="id_twoofiveeight_oneseventwoonigth" className={` frame menuoptionmobile ${ props.onClick ? 'cursor' : '' } ${ transaction['menuoptionmobile']?.type ? transaction['menuoptionmobile']?.type.toLowerCase() : '' }`} style={ { ...{}, ...props.MenuOptionMobileStyle , transitionDuration: transaction['menuoptionmobile']?.duration, transitionTimingFunction: transaction['menuoptionmobile']?.timingFunction } } onClick={ props.MenuOptionMobileonClick } onMouseEnter={ props.MenuOptionMobileonMouseEnter } onMouseOver={ props.MenuOptionMobileonMouseOver } onKeyPress={ props.MenuOptionMobileonKeyPress } onDrag={ props.MenuOptionMobileonDrag } onMouseLeave={ props.MenuOptionMobileonMouseLeave } onMouseUp={ props.MenuOptionMobileonMouseUp } onMouseDown={ props.MenuOptionMobileonMouseDown } onKeyDown={ props.MenuOptionMobileonKeyDown } onChange={ props.MenuOptionMobileonChange } ondelay={ props.MenuOptionMobileondelay }>
      <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['subtitleone']?.animationClass || {}}>
        <Subtitleone { ...{ ...props, style:false } } SubtilteoneText0={ props.SubtilteoneText2 || "Premium" } cssClass={"C_twoofiveeight_oneseventhreezero "}  />
    </CSSTransition >
<CSSTransition 
        in={_in}
        appear={true} 
        timeout={100}
        classNames={transaction['subtitleone']?.animationClass || {}}
    >
    <Subtitleone { ...{ ...props, style:false } }   SubtilteoneText0={ props.SubtilteoneText3 || " Ayuda"} cssClass={"C_twoofiveeight_oneseventhreeone "}  />
    </CSSTransition >
<CSSTransition 
        in={_in}
        appear={true} 
        timeout={100}
        classNames={transaction['subtitleone']?.animationClass || {}}
    >
    <Subtitleone { ...{ ...props, style:false } }   SubtilteoneText0={ props.SubtilteoneText4 || " Descargar"} cssClass={"C_twoofiveeight_oneseventhreetwoo "}  />
    </CSSTransition >
<CSSTransition 
        in={_in}
        appear={true} 
        timeout={100}
        classNames={transaction['subtitleone']?.animationClass || {}}
    >
    <Subtitleone { ...{ ...props, style:false } }   SubtilteoneText0={ props.SubtilteoneText5 || " Privacidad"} cssClass={"C_twoofiveeight_oneseventhreethree "}  />
    </CSSTransition >
<CSSTransition 
        in={_in}
        appear={true} 
        timeout={100}
        classNames={transaction['subtitleone']?.animationClass || {}}
    >
    <Subtitleone { ...{ ...props, style:false } }   SubtilteoneText0={ props.SubtilteoneText6 || " Términos y Condiciones"} cssClass={"C_twoofiveeight_oneseventhreefour "}  />
    </CSSTransition >
                    </div>
               
    </CSSTransition >
            
            </>
        }
        </div>
    
    </CSSTransition >
            </>
        
    ) 
}

PropertyoneHeaderExtM.propTypes = {
    style: PropTypes.any,
Uncodie_logoM0: PropTypes.any,
Icon0: PropTypes.any,
SubtilteoneText0: PropTypes.any,
SubtilteoneText1: PropTypes.any,
SubtilteoneText2: PropTypes.any,
SubtilteoneText3: PropTypes.any,
SubtilteoneText4: PropTypes.any,
SubtilteoneText5: PropTypes.any,
SubtilteoneText6: PropTypes.any,
HeaderMonClick: PropTypes.any,
HeaderMonMouseEnter: PropTypes.any,
HeaderMonMouseOver: PropTypes.any,
HeaderMonKeyPress: PropTypes.any,
HeaderMonDrag: PropTypes.any,
HeaderMonMouseLeave: PropTypes.any,
HeaderMonMouseUp: PropTypes.any,
HeaderMonMouseDown: PropTypes.any,
HeaderMonKeyDown: PropTypes.any,
HeaderMonChange: PropTypes.any,
HeaderMondelay: PropTypes.any,
HeaderContonClick: PropTypes.any,
HeaderContonMouseEnter: PropTypes.any,
HeaderContonMouseOver: PropTypes.any,
HeaderContonKeyPress: PropTypes.any,
HeaderContonDrag: PropTypes.any,
HeaderContonMouseLeave: PropTypes.any,
HeaderContonMouseUp: PropTypes.any,
HeaderContonMouseDown: PropTypes.any,
HeaderContonKeyDown: PropTypes.any,
HeaderContonChange: PropTypes.any,
HeaderContondelay: PropTypes.any,
Uncodie_logoMonClick: PropTypes.any,
Uncodie_logoMonMouseEnter: PropTypes.any,
Uncodie_logoMonMouseOver: PropTypes.any,
Uncodie_logoMonKeyPress: PropTypes.any,
Uncodie_logoMonDrag: PropTypes.any,
Uncodie_logoMonMouseLeave: PropTypes.any,
Uncodie_logoMonMouseUp: PropTypes.any,
Uncodie_logoMonMouseDown: PropTypes.any,
Uncodie_logoMonKeyDown: PropTypes.any,
Uncodie_logoMonChange: PropTypes.any,
Uncodie_logoMondelay: PropTypes.any,
IcononClick: PropTypes.any,
IcononMouseEnter: PropTypes.any,
IcononMouseOver: PropTypes.any,
IcononKeyPress: PropTypes.any,
IcononDrag: PropTypes.any,
IcononMouseLeave: PropTypes.any,
IcononMouseUp: PropTypes.any,
IcononMouseDown: PropTypes.any,
IcononKeyDown: PropTypes.any,
IcononChange: PropTypes.any,
Iconondelay: PropTypes.any,
MenuDivMobileonClick: PropTypes.any,
MenuDivMobileonMouseEnter: PropTypes.any,
MenuDivMobileonMouseOver: PropTypes.any,
MenuDivMobileonKeyPress: PropTypes.any,
MenuDivMobileonDrag: PropTypes.any,
MenuDivMobileonMouseLeave: PropTypes.any,
MenuDivMobileonMouseUp: PropTypes.any,
MenuDivMobileonMouseDown: PropTypes.any,
MenuDivMobileonKeyDown: PropTypes.any,
MenuDivMobileonChange: PropTypes.any,
MenuDivMobileondelay: PropTypes.any,
MenuOptionMobileonClick: PropTypes.any,
MenuOptionMobileonMouseEnter: PropTypes.any,
MenuOptionMobileonMouseOver: PropTypes.any,
MenuOptionMobileonKeyPress: PropTypes.any,
MenuOptionMobileonDrag: PropTypes.any,
MenuOptionMobileonMouseLeave: PropTypes.any,
MenuOptionMobileonMouseUp: PropTypes.any,
MenuOptionMobileonMouseDown: PropTypes.any,
MenuOptionMobileonKeyDown: PropTypes.any,
MenuOptionMobileonChange: PropTypes.any,
MenuOptionMobileondelay: PropTypes.any
}
export default PropertyoneHeaderExtM;