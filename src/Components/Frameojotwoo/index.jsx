import React, { Suspense } from 'react';
import PropTypes from 'prop-types';
import * as Contexts  from 'Contexts';
import { OverlaySwapContext } from 'Components/OverlaySwapProvider';
import CorebooksAnimation from 'Components/CorebooksAnimation';
import CorebooksAnimationSingle from 'Components/CorebooksAnimationSingle';

import * as UtilsScripts  from 'utils/utils';
import { Route, useHistory, useParams } from "react-router-dom";
import SingletoneNavigation from 'Class/SingletoneNavigation';

import LoadingComponent from "Components/Loading";


import { Transition } from "react-transition-group";
import { CSSTransition } from "react-transition-group";
import { useAppContext, useSessionContext } from 'context/AppContext';
import './Frameojotwoo.css'





const Frameojotwoo = (props)=>{
    const [state,dispatch] = useAppContext();
    const [, setNameSelectedComponent] = React.useContext(OverlaySwapContext);
    
    const nodeRef = React.useRef(null);
    const history = useHistory();
    let RouterContext = useParams();
    
    
    
    
    
    
    const [globalStateForm, dispatchForm] = React.useState({});
    
    const [ _in, setIn ] = React.useState(false)
    const [ transaction, setTransaction ] = React.useState({}) 
    const [ animation, setAnimation ] = React.useState('')

    
    
    
    
    
    
    React.useEffect(()=>{
        
        
        
    },[]);
    
    
    return (
        <>
  <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['frameojotwoo']?.animationClass || {}}>

    <div id="id_fourthreefour_twoothreefoureight" ref={nodeRef} className={` ${ props.onClick ? 'cursor' : '' } frameojotwoo C_fourthreefour_twoothreefoureight ${ props.cssClass } ${ transaction['frameojotwoo']?.type ? transaction['frameojotwoo']?.type.toLowerCase() : '' }`} style={ { ...{ ...{}, transitionDuration: transaction['frameojotwoo']?.duration, transitionTimingFunction: transaction['frameojotwoo']?.timingFunction }, ...props.style }} onClick={ props.FrameojotwooonClick } onMouseEnter={ props.FrameojotwooonMouseEnter } onMouseOver={ props.FrameojotwooonMouseOver } onKeyPress={ props.FrameojotwooonKeyPress } onDrag={ props.FrameojotwooonDrag } onMouseLeave={ props.FrameojotwooonMouseLeave } onMouseUp={ props.FrameojotwooonMouseUp } onMouseDown={ props.FrameojotwooonMouseDown } onKeyDown={ props.FrameojotwooonKeyDown } onChange={ props.FrameojotwooonChange } ondelay={ props.Frameojotwooondelay }>
      {
      props.children ?
      props.children :
      <>
        <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['vector']?.animationClass || {}}>
          <svg id="id_fourthreefour_twoothreefoursix" className="vector  " style={{}} onClick={ props.VectoronClick } onMouseEnter={ props.VectoronMouseEnter } onMouseOver={ props.VectoronMouseOver } onKeyPress={ props.VectoronKeyPress } onDrag={ props.VectoronDrag } onMouseLeave={ props.VectoronMouseLeave } onMouseUp={ props.VectoronMouseUp } onMouseDown={ props.VectoronMouseDown } onKeyDown={ props.VectoronKeyDown } onChange={ props.VectoronChange } ondelay={ props.Vectorondelay } width="23.75" height="21.02459716796875">
            <path d="M22.082 1.71929C22.1775 1.62704 22.2537 1.5167 22.3061 1.39469C22.3585 1.27269 22.3861 1.14147 22.3873 1.00869C22.3884 0.87591 22.3631 0.744231 22.3128 0.621334C22.2625 0.498438 22.1883 0.386786 22.0944 0.292893C22.0005 0.199001 21.8888 0.124747 21.766 0.0744666C21.6431 0.0241857 21.5114 -0.00111606 21.3786 3.77567e-05C21.2458 0.00119157 21.1146 0.0287777 20.9926 0.0811867C20.8706 0.133596 20.7602 0.209778 20.668 0.305288L17.025 3.94829C15.496 3.25829 13.795 2.89529 11.875 2.89529C8.546 2.89529 5.871 3.98629 3.617 5.98429C2.721 6.77829 1.317 8.33729 0.236 10.4373L0 10.8953L0.236 11.3533C1.318 13.4533 2.721 15.0123 3.617 15.8063C3.895 16.0523 4.179 16.2853 4.47 16.5033L1.668 19.3053C1.57249 19.3975 1.49631 19.5079 1.4439 19.6299C1.39149 19.7519 1.3639 19.8831 1.36275 20.0159C1.3616 20.1487 1.3869 20.2803 1.43718 20.4032C1.48746 20.5261 1.56171 20.6378 1.6556 20.7317C1.7495 20.8256 1.86115 20.8998 1.98405 20.9501C2.10694 21.0004 2.23862 21.0257 2.3714 21.0245C2.50418 21.0234 2.6354 20.9958 2.7574 20.9434C2.87941 20.891 2.98975 20.8148 3.082 20.7193L6.208 17.5933L6.211 17.5953L7.714 16.0923L7.71 16.0913L9.44 14.3613L9.444 14.3623L11.011 12.7953L11.007 12.7953L15.687 8.11429L15.688 8.11829L17.283 6.52329L17.281 6.52029L17.391 6.41129L17.393 6.41329L18.837 4.96929L18.834 4.96729L22.082 1.71929ZM14.759 6.21529L9.189 11.7853C8.80644 11.4078 8.50314 10.9577 8.2969 10.4615C8.09066 9.96519 7.98564 9.43271 7.988 8.89529C7.988 6.66529 9.749 4.89529 11.874 4.89529C13.011 4.89529 14.044 5.40129 14.758 6.21429L14.759 6.21529ZM7.775 13.1993L5.902 15.0723C5.56947 14.8354 5.25001 14.5807 4.945 14.3093C4.271 13.7133 3.175 12.5163 2.262 10.8953C3.175 9.27529 4.272 8.07729 4.945 7.48129C5.464 7.02129 6.006 6.61829 6.579 6.27729C6.18922 7.09496 5.98729 7.98947 5.988 8.89529C5.988 10.5763 6.671 12.1053 7.775 13.1993ZM19.343 7.99929L20.758 6.58429C21.8572 7.7294 22.7854 9.0271 23.514 10.4373L23.75 10.8953L23.514 11.3533C22.432 13.4533 21.029 15.0123 20.133 15.8063C17.879 17.8033 15.203 18.8953 11.875 18.8953C10.8382 18.9005 9.80447 18.7837 8.795 18.5473L10.521 16.8213C10.956 16.8713 11.407 16.8953 11.875 16.8953C14.708 16.8953 16.912 15.9883 18.806 14.3093C19.48 13.7133 20.576 12.5163 21.489 10.8953C20.8968 9.84454 20.1759 8.87171 19.343 7.99929Z" />
          </svg>
        </CSSTransition>
        <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['vector']?.animationClass || {}}>
          <svg id="id_fourthreefour_twoothreefourseven" className="vector  " style={{}} onClick={ props.VectoronClick } onMouseEnter={ props.VectoronMouseEnter } onMouseOver={ props.VectoronMouseOver } onKeyPress={ props.VectoronKeyPress } onDrag={ props.VectoronDrag } onMouseLeave={ props.VectoronMouseLeave } onMouseUp={ props.VectoronMouseUp } onMouseDown={ props.VectoronMouseDown } onKeyDown={ props.VectoronKeyDown } onChange={ props.VectoronChange } ondelay={ props.Vectorondelay } width="5.185546875" height="5.02703857421875">
            <path d="M5.18542 0C4.86083 2.64303 2.7174 4.75457 0 5.02703L5.18542 0Z" />
          </svg>
        </CSSTransition>

      </>
      }
    </div>

  </CSSTransition>
</>
    ) 
}

Frameojotwoo.propTypes = {
    style: PropTypes.any,
FrameojotwooonClick: PropTypes.any,
FrameojotwooonMouseEnter: PropTypes.any,
FrameojotwooonMouseOver: PropTypes.any,
FrameojotwooonKeyPress: PropTypes.any,
FrameojotwooonDrag: PropTypes.any,
FrameojotwooonMouseLeave: PropTypes.any,
FrameojotwooonMouseUp: PropTypes.any,
FrameojotwooonMouseDown: PropTypes.any,
FrameojotwooonKeyDown: PropTypes.any,
FrameojotwooonChange: PropTypes.any,
Frameojotwooondelay: PropTypes.any,
VectoronClick: PropTypes.any,
VectoronMouseEnter: PropTypes.any,
VectoronMouseOver: PropTypes.any,
VectoronKeyPress: PropTypes.any,
VectoronDrag: PropTypes.any,
VectoronMouseLeave: PropTypes.any,
VectoronMouseUp: PropTypes.any,
VectoronMouseDown: PropTypes.any,
VectoronKeyDown: PropTypes.any,
VectoronChange: PropTypes.any,
Vectorondelay: PropTypes.any
}
export default Frameojotwoo;