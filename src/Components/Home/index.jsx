import React, { Suspense } from 'react';
import PropTypes from 'prop-types';
import * as Contexts  from 'Contexts';
import { OverlaySwapContext } from 'Components/OverlaySwapProvider';
import CorebooksAnimation from 'Components/CorebooksAnimation';
import CorebooksAnimationSingle from 'Components/CorebooksAnimationSingle';

import * as UtilsScripts  from 'utils/utils';
import { Route, useHistory, useParams } from "react-router-dom";
import SingletoneNavigation from 'Class/SingletoneNavigation';

import LoadingComponent from "Components/Loading";


import { Transition } from "react-transition-group";
import { CSSTransition } from "react-transition-group";
import Paragraphone from 'Components/Paragraphone'
import Paragraphtwoo from 'Components/Paragraphtwoo'
import PlayIconAnimation from 'Components/PlayIconAnimation'
import { useAppContext, useSessionContext } from 'context/AppContext';
import './Home.css'




export const HomeLocalStateReducer = (state = {}, action) => {

    return {
        ...state,
        [action.payload.key]: action.payload.value,
    };
};
    

const Home = (props)=>{
    const [state,dispatch] = useAppContext();
    const [, setNameSelectedComponent] = React.useContext(OverlaySwapContext);
    
    const nodeRef = React.useRef(null);
    const history = useHistory();
    let RouterContext = useParams();
    
    
    
    
    
    
    const [globalStateForm, dispatchForm] = React.useState({});
    
    const [ _in, setIn ] = React.useState(false)
    const [ transaction, setTransaction ] = React.useState({}) 
    const [ animation, setAnimation ] = React.useState('')

    const [HomeLocalState, dispatchHome] = React.useReducer(HomeLocalStateReducer, { ...{"View":"row","search":0}, ...(props.state || {} ) });
    
    
    
    
    
    React.useEffect(()=>{
        document.title = "Home"

        
        
    },[]);
    
    
    return (
        <Contexts.HomeLocalContext.Provider value={[HomeLocalState,dispatchHome]}>
  <CSSTransition nodeRef={nodeRef} in={true} appear={true} timeout={400} classNames={SingletoneNavigation.getTransitionInstance()?.Home?.cssClass || '' }>

    <div id="id_onefoureight_onezerotwooeight" ref={nodeRef} className={` ${ props.onClick ? 'cursor' : '' } home ${ props.cssClass } ${ transaction['home']?.type ? transaction['home']?.type.toLowerCase() : '' }`} style={ { ...{ ...{"width":"100%"}, transitionDuration: `${((SingletoneNavigation.getTransitionInstance()?.Home?.duration || 0) * 1000).toFixed(0)}ms` }, ...props.style }} onClick={ props.HomeonClick } onMouseEnter={ props.HomeonMouseEnter } onMouseOver={ props.HomeonMouseOver } onKeyPress={ props.HomeonKeyPress } onDrag={ props.HomeonDrag } onMouseLeave={ props.HomeonMouseLeave } onMouseUp={ props.HomeonMouseUp } onMouseDown={ props.HomeonMouseDown } onKeyDown={ props.HomeonKeyDown } onChange={ props.HomeonChange } ondelay={ props.Homeondelay }>
      {
      props.children ?
      props.children :
      <>
        <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['dashboardmastercontainer']?.animationClass || {}}>

          <div id="id_onefoureight_onezerotwoonigth" className={` frame dashboardmastercontainer ${ props.onClick ? 'cursor' : '' } ${ transaction['dashboardmastercontainer']?.type ? transaction['dashboardmastercontainer']?.type.toLowerCase() : '' }`} style={ { ...{"width":"100%","height":"100%"}, ...props.DashboardMasterContainerStyle , transitionDuration: transaction['dashboardmastercontainer']?.duration, transitionTimingFunction: transaction['dashboardmastercontainer']?.timingFunction } } onClick={ props.DashboardMasterContaineronClick } onMouseEnter={ props.DashboardMasterContaineronMouseEnter } onMouseOver={ props.DashboardMasterContaineronMouseOver } onKeyPress={ props.DashboardMasterContaineronKeyPress } onDrag={ props.DashboardMasterContaineronDrag } onMouseLeave={ props.DashboardMasterContaineronMouseLeave } onMouseUp={ props.DashboardMasterContaineronMouseUp } onMouseDown={ props.DashboardMasterContaineronMouseDown } onKeyDown={ props.DashboardMasterContaineronKeyDown } onChange={ props.DashboardMasterContaineronChange } ondelay={ props.DashboardMasterContainerondelay }>
            <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['projectscardsdashboard']?.animationClass || {}}>

              <div id="id_onefoureight_onezerofourone" className={` frame projectscardsdashboard ${ props.onClick ? 'cursor' : '' } ${ transaction['projectscardsdashboard']?.type ? transaction['projectscardsdashboard']?.type.toLowerCase() : '' }`} style={ { ...{"flexWrap":"nowrap","overflow":"auto","margin":"auto","width":"100%","height":"100%"}, ...props.ProjectsCardsDashboardStyle , transitionDuration: transaction['projectscardsdashboard']?.duration, transitionTimingFunction: transaction['projectscardsdashboard']?.timingFunction } } onClick={ props.ProjectsCardsDashboardonClick } onMouseEnter={ props.ProjectsCardsDashboardonMouseEnter } onMouseOver={ props.ProjectsCardsDashboardonMouseOver } onKeyPress={ props.ProjectsCardsDashboardonKeyPress } onDrag={ props.ProjectsCardsDashboardonDrag } onMouseLeave={ props.ProjectsCardsDashboardonMouseLeave } onMouseUp={ props.ProjectsCardsDashboardonMouseUp } onMouseDown={ props.ProjectsCardsDashboardonMouseDown } onKeyDown={ props.ProjectsCardsDashboardonKeyDown } onChange={ props.ProjectsCardsDashboardonChange } ondelay={ props.ProjectsCardsDashboardondelay }>
                <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['hone']?.animationClass || {}}>

                  <div id="id_onefoureight_onezerofourtwoo" className={` instance hone ${ props.onClick ? 'cursor' : '' } ${ transaction['hone']?.type ? transaction['hone']?.type.toLowerCase() : '' }`} style={{ ...{},  ...props.HoneStyle , transitionDuration: transaction['hone']?.duration, transitionTimingFunction: transaction['hone']?.timingFunction }} onClick={ props.HoneonClick } onMouseEnter={ props.HoneonMouseEnter } onMouseOver={ props.HoneonMouseOver } onKeyPress={ props.HoneonKeyPress } onDrag={ props.HoneonDrag } onMouseLeave={ props.HoneonMouseLeave } onMouseUp={ props.HoneonMouseUp } onMouseDown={ props.HoneonMouseDown } onKeyDown={ props.HoneonKeyDown } onChange={ props.HoneonChange } ondelay={ props.Honeondelay }>
                    <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['honetext']?.animationClass || {}}>

                      <span id="id_onefoureight_onezerofourtwoo_one_eightnigthseven"  className={` text honetext    ${ props.onClick ? 'cursor' : ''}  ${ transaction['honetext']?.type ? transaction['honetext']?.type.toLowerCase() : '' }`} style={{ ...{},  ...props.HoneTextStyle , transitionDuration: transaction['honetext']?.duration, transitionTimingFunction: transaction['honetext']?.timingFunction }} onClick={ props.HoneTextonClick } onMouseEnter={ props.HoneTextonMouseEnter } onMouseOver={ props.HoneTextonMouseOver } onKeyPress={ props.HoneTextonKeyPress } onDrag={ props.HoneTextonDrag } onMouseLeave={ props.HoneTextonMouseLeave } onMouseUp={ props.HoneTextonMouseUp } onMouseDown={ props.HoneTextonMouseDown } onKeyDown={ props.HoneTextonKeyDown } onChange={ props.HoneTextonChange } ondelay={ props.HoneTextondelay } >{props.HoneText0 || `Your top mixes`}</span>

                    </CSSTransition>
                  </div>

                </CSSTransition>
                <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['cardview']?.animationClass || {}}>

                  <div id="id_onefoureight_onezerofourthree" className={` frame cardview ${ props.onClick ? 'cursor' : '' } ${ transaction['cardview']?.type ? transaction['cardview']?.type.toLowerCase() : '' }`} style={ { ...{"width":"100%","padding":"20px"}, ...props.CardViewStyle , transitionDuration: transaction['cardview']?.duration, transitionTimingFunction: transaction['cardview']?.timingFunction } } onClick={ props.CardViewonClick } onMouseEnter={ props.CardViewonMouseEnter } onMouseOver={ props.CardViewonMouseOver } onKeyPress={ props.CardViewonKeyPress } onDrag={ props.CardViewonDrag } onMouseLeave={ props.CardViewonMouseLeave } onMouseUp={ props.CardViewonMouseUp } onMouseDown={ props.CardViewonMouseDown } onKeyDown={ props.CardViewonKeyDown } onChange={ props.CardViewonChange } ondelay={ props.CardViewondelay }>
                    <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['cardsrow']?.animationClass || {}}>

                      <div id="id_onefoureight_onezerofourfour" className={` frame cardsrow ${ props.onClick ? 'cursor' : '' } ${ transaction['cardsrow']?.type ? transaction['cardsrow']?.type.toLowerCase() : '' }`} style={ { ...{"width":"100%","maxWidth":"320px"}, ...props.CardsRowStyle , transitionDuration: transaction['cardsrow']?.duration, transitionTimingFunction: transaction['cardsrow']?.timingFunction } } onClick={ props.CardsRowonClick } onMouseEnter={ props.CardsRowonMouseEnter } onMouseOver={ props.CardsRowonMouseOver } onKeyPress={ props.CardsRowonKeyPress } onDrag={ props.CardsRowonDrag } onMouseLeave={ props.CardsRowonMouseLeave } onMouseUp={ props.CardsRowonMouseUp } onMouseDown={ props.CardsRowonMouseDown } onKeyDown={ props.CardsRowonKeyDown } onChange={ props.CardsRowonChange } ondelay={ props.CardsRowondelay }>
                        <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['cardplaylistimgcont']?.animationClass || {}}>

                          <div id="id_onefoureight_onezerofourfive" className={` frame cardplaylistimgcont ${ props.onClick ? 'cursor' : '' } ${ transaction['cardplaylistimgcont']?.type ? transaction['cardplaylistimgcont']?.type.toLowerCase() : '' }`} style={ { ...{"width":"189px"}, ...props.CardPlaylistImgContStyle , transitionDuration: transaction['cardplaylistimgcont']?.duration, transitionTimingFunction: transaction['cardplaylistimgcont']?.timingFunction } } onClick={ props.CardPlaylistImgContonClick } onMouseEnter={ props.CardPlaylistImgContonMouseEnter } onMouseOver={ props.CardPlaylistImgContonMouseOver } onKeyPress={ props.CardPlaylistImgContonKeyPress } onDrag={ props.CardPlaylistImgContonDrag } onMouseLeave={ props.CardPlaylistImgContonMouseLeave } onMouseUp={ props.CardPlaylistImgContonMouseUp } onMouseDown={ props.CardPlaylistImgContonMouseDown } onKeyDown={ props.CardPlaylistImgContonKeyDown } onChange={ props.CardPlaylistImgContonChange } ondelay={ props.CardPlaylistImgContondelay }>
                            <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['cardplaylistimg']?.animationClass || {}}>

                              <div id="id_onefoureight_onezerofoursix" className={` frame cardplaylistimg ${ props.onClick ? 'cursor' : '' } ${ transaction['cardplaylistimg']?.type ? transaction['cardplaylistimg']?.type.toLowerCase() : '' }`} style={ { ...{}, ...props.CardPlaylistImgStyle , transitionDuration: transaction['cardplaylistimg']?.duration, transitionTimingFunction: transaction['cardplaylistimg']?.timingFunction } } onClick={ props.CardPlaylistImgonClick } onMouseEnter={ props.CardPlaylistImgonMouseEnter } onMouseOver={ props.CardPlaylistImgonMouseOver } onKeyPress={ props.CardPlaylistImgonKeyPress } onDrag={ props.CardPlaylistImgonDrag } onMouseLeave={ props.CardPlaylistImgonMouseLeave } onMouseUp={ props.CardPlaylistImgonMouseUp } onMouseDown={ props.CardPlaylistImgonMouseDown } onKeyDown={ props.CardPlaylistImgonKeyDown } onChange={ props.CardPlaylistImgonChange } ondelay={ props.CardPlaylistImgondelay }>
                                <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['imageplayon']?.animationClass || {}}>

                                  <div id="id_onefoureight_onezerofourseven" className={` frame imageplayon ${ props.onClick ? 'cursor' : '' } ${ transaction['imageplayon']?.type ? transaction['imageplayon']?.type.toLowerCase() : '' }`} style={ { ...{}, ...props.ImagePlayOnStyle , transitionDuration: transaction['imageplayon']?.duration, transitionTimingFunction: transaction['imageplayon']?.timingFunction } } onClick={ props.ImagePlayOnonClick } onMouseEnter={ props.ImagePlayOnonMouseEnter } onMouseOver={ props.ImagePlayOnonMouseOver } onKeyPress={ props.ImagePlayOnonKeyPress } onDrag={ props.ImagePlayOnonDrag } onMouseLeave={ props.ImagePlayOnonMouseLeave } onMouseUp={ props.ImagePlayOnonMouseUp } onMouseDown={ props.ImagePlayOnonMouseDown } onKeyDown={ props.ImagePlayOnonKeyDown } onChange={ props.ImagePlayOnonChange } ondelay={ props.ImagePlayOnondelay }>
                                    <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['playiconanimation']?.animationClass || {}}>
                                      <PlayIconAnimation { ...{ ...props, style:false } } variant={'Property 1=Default'} cssClass={"C_onefoureight_onezerofoureight cursor "}  />
    </CSSTransition >
                    </div>
               
    </CSSTransition >
                    </div>
               
    </CSSTransition >
<CSSTransition 
        in={_in}
        appear={true} 
        timeout={100}
        classNames={transaction['cardplaylistdata']?.animationClass || {}}
    >
    
                    <div id="id_onefoureight_onezerofournigth" className={` frame cardplaylistdata ${ props.onClick ? 'cursor' : '' } ${ transaction['cardplaylistdata']?.type ? transaction['cardplaylistdata']?.type.toLowerCase() : '' }`} style={ { ...{}, ...props.CardPlaylistDataStyle , transitionDuration: transaction['cardplaylistdata']?.duration, transitionTimingFunction: transaction['cardplaylistdata']?.timingFunction } } onClick={ props.CardPlaylistDataonClick } onMouseEnter={ props.CardPlaylistDataonMouseEnter } onMouseOver={ props.CardPlaylistDataonMouseOver } onKeyPress={ props.CardPlaylistDataonKeyPress } onDrag={ props.CardPlaylistDataonDrag } onMouseLeave={ props.CardPlaylistDataonMouseLeave } onMouseUp={ props.CardPlaylistDataonMouseUp } onMouseDown={ props.CardPlaylistDataonMouseDown } onKeyDown={ props.CardPlaylistDataonKeyDown } onChange={ props.CardPlaylistDataonChange } ondelay={ props.CardPlaylistDataondelay }>
                                        <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['paragraphtwoo']?.animationClass || {}}>
                                          <Paragraphtwoo { ...{ ...props, style:false } } ParagraphtwooText0={ props.ParagraphtwooText0 || "Peacefull Piano" } cssClass={"C_onefoureight_onezerofivezero "}  />
    </CSSTransition >
<CSSTransition 
        in={_in}
        appear={true} 
        timeout={100}
        classNames={transaction['paragraphone']?.animationClass || {}}
    >
    <Paragraphone { ...{ ...props, style:false } }   ParagraphoneText0={ props.ParagraphoneText0 || " Peaceful piano to help\nyou slow down, breathe"} cssClass={"C_onefoureight_onezerofiveone "}  />
    </CSSTransition >
                    </div>
               
    </CSSTransition >
                    </div>
               
    </CSSTransition >
                    </div>
               
    </CSSTransition >
                    </div>
               
    </CSSTransition >
<CSSTransition 
        in={_in}
        appear={true} 
        timeout={100}
        classNames={transaction['cardview']?.animationClass || {}}
    >
    
                    <div id="id_onefivefour_threetwoonigthfour" className={` frame cardview ${ props.onClick ? 'cursor' : '' } ${ transaction['cardview']?.type ? transaction['cardview']?.type.toLowerCase() : '' }`} style={ { ...{"width":"100%","padding":"20px"}, ...props.CardViewStyle , transitionDuration: transaction['cardview']?.duration, transitionTimingFunction: transaction['cardview']?.timingFunction } } onClick={ props.CardViewonClick } onMouseEnter={ props.CardViewonMouseEnter } onMouseOver={ props.CardViewonMouseOver } onKeyPress={ props.CardViewonKeyPress } onDrag={ props.CardViewonDrag } onMouseLeave={ props.CardViewonMouseLeave } onMouseUp={ props.CardViewonMouseUp } onMouseDown={ props.CardViewonMouseDown } onKeyDown={ props.CardViewonKeyDown } onChange={ props.CardViewonChange } ondelay={ props.CardViewondelay }>
                                            <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['cardsrow']?.animationClass || {}}>

                                              <div id="id_onefivefour_threetwoonigthfive" className={` frame cardsrow ${ props.onClick ? 'cursor' : '' } ${ transaction['cardsrow']?.type ? transaction['cardsrow']?.type.toLowerCase() : '' }`} style={ { ...{"width":"100%","maxWidth":"320px"}, ...props.CardsRowStyle , transitionDuration: transaction['cardsrow']?.duration, transitionTimingFunction: transaction['cardsrow']?.timingFunction } } onClick={ props.CardsRowonClick } onMouseEnter={ props.CardsRowonMouseEnter } onMouseOver={ props.CardsRowonMouseOver } onKeyPress={ props.CardsRowonKeyPress } onDrag={ props.CardsRowonDrag } onMouseLeave={ props.CardsRowonMouseLeave } onMouseUp={ props.CardsRowonMouseUp } onMouseDown={ props.CardsRowonMouseDown } onKeyDown={ props.CardsRowonKeyDown } onChange={ props.CardsRowonChange } ondelay={ props.CardsRowondelay }>
                                                <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['cardplaylistimgcont']?.animationClass || {}}>

                                                  <div id="id_onefivefour_threetwoonigthsix" className={` frame cardplaylistimgcont ${ props.onClick ? 'cursor' : '' } ${ transaction['cardplaylistimgcont']?.type ? transaction['cardplaylistimgcont']?.type.toLowerCase() : '' }`} style={ { ...{"width":"189px"}, ...props.CardPlaylistImgContStyle , transitionDuration: transaction['cardplaylistimgcont']?.duration, transitionTimingFunction: transaction['cardplaylistimgcont']?.timingFunction } } onClick={ props.CardPlaylistImgContonClick } onMouseEnter={ props.CardPlaylistImgContonMouseEnter } onMouseOver={ props.CardPlaylistImgContonMouseOver } onKeyPress={ props.CardPlaylistImgContonKeyPress } onDrag={ props.CardPlaylistImgContonDrag } onMouseLeave={ props.CardPlaylistImgContonMouseLeave } onMouseUp={ props.CardPlaylistImgContonMouseUp } onMouseDown={ props.CardPlaylistImgContonMouseDown } onKeyDown={ props.CardPlaylistImgContonKeyDown } onChange={ props.CardPlaylistImgContonChange } ondelay={ props.CardPlaylistImgContondelay }>
                                                    <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['cardplaylistimg']?.animationClass || {}}>

                                                      <div id="id_onefivefour_threetwoonigthseven" className={` frame cardplaylistimg ${ props.onClick ? 'cursor' : '' } ${ transaction['cardplaylistimg']?.type ? transaction['cardplaylistimg']?.type.toLowerCase() : '' }`} style={ { ...{}, ...props.CardPlaylistImgStyle , transitionDuration: transaction['cardplaylistimg']?.duration, transitionTimingFunction: transaction['cardplaylistimg']?.timingFunction } } onClick={ props.CardPlaylistImgonClick } onMouseEnter={ props.CardPlaylistImgonMouseEnter } onMouseOver={ props.CardPlaylistImgonMouseOver } onKeyPress={ props.CardPlaylistImgonKeyPress } onDrag={ props.CardPlaylistImgonDrag } onMouseLeave={ props.CardPlaylistImgonMouseLeave } onMouseUp={ props.CardPlaylistImgonMouseUp } onMouseDown={ props.CardPlaylistImgonMouseDown } onKeyDown={ props.CardPlaylistImgonKeyDown } onChange={ props.CardPlaylistImgonChange } ondelay={ props.CardPlaylistImgondelay }>
                                                        <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['imageplayon']?.animationClass || {}}>

                                                          <div id="id_onefivefour_threetwoonigtheight" className={` frame imageplayon ${ props.onClick ? 'cursor' : '' } ${ transaction['imageplayon']?.type ? transaction['imageplayon']?.type.toLowerCase() : '' }`} style={ { ...{}, ...props.ImagePlayOnStyle , transitionDuration: transaction['imageplayon']?.duration, transitionTimingFunction: transaction['imageplayon']?.timingFunction } } onClick={ props.ImagePlayOnonClick } onMouseEnter={ props.ImagePlayOnonMouseEnter } onMouseOver={ props.ImagePlayOnonMouseOver } onKeyPress={ props.ImagePlayOnonKeyPress } onDrag={ props.ImagePlayOnonDrag } onMouseLeave={ props.ImagePlayOnonMouseLeave } onMouseUp={ props.ImagePlayOnonMouseUp } onMouseDown={ props.ImagePlayOnonMouseDown } onKeyDown={ props.ImagePlayOnonKeyDown } onChange={ props.ImagePlayOnonChange } ondelay={ props.ImagePlayOnondelay }>
                                                            <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['playiconanimation']?.animationClass || {}}>
                                                              <PlayIconAnimation { ...{ ...props, style:false } } variant={'Property 1=Default'} cssClass={"C_onefivefour_threetwoonigthnigth cursor "}  />
    </CSSTransition >
                    </div>
               
    </CSSTransition >
                    </div>
               
    </CSSTransition >
<CSSTransition 
        in={_in}
        appear={true} 
        timeout={100}
        classNames={transaction['cardplaylistdata']?.animationClass || {}}
    >
    
                    <div id="id_onefivefour_threethreezerozero" className={` frame cardplaylistdata ${ props.onClick ? 'cursor' : '' } ${ transaction['cardplaylistdata']?.type ? transaction['cardplaylistdata']?.type.toLowerCase() : '' }`} style={ { ...{}, ...props.CardPlaylistDataStyle , transitionDuration: transaction['cardplaylistdata']?.duration, transitionTimingFunction: transaction['cardplaylistdata']?.timingFunction } } onClick={ props.CardPlaylistDataonClick } onMouseEnter={ props.CardPlaylistDataonMouseEnter } onMouseOver={ props.CardPlaylistDataonMouseOver } onKeyPress={ props.CardPlaylistDataonKeyPress } onDrag={ props.CardPlaylistDataonDrag } onMouseLeave={ props.CardPlaylistDataonMouseLeave } onMouseUp={ props.CardPlaylistDataonMouseUp } onMouseDown={ props.CardPlaylistDataonMouseDown } onKeyDown={ props.CardPlaylistDataonKeyDown } onChange={ props.CardPlaylistDataonChange } ondelay={ props.CardPlaylistDataondelay }>
                                                                <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['paragraphtwoo']?.animationClass || {}}>
                                                                  <Paragraphtwoo { ...{ ...props, style:false } } ParagraphtwooText0={ props.ParagraphtwooText1 || "Peacefull Piano" } cssClass={"C_onefivefour_threethreezeroone "}  />
    </CSSTransition >
<CSSTransition 
        in={_in}
        appear={true} 
        timeout={100}
        classNames={transaction['paragraphone']?.animationClass || {}}
    >
    <Paragraphone { ...{ ...props, style:false } }   ParagraphoneText0={ props.ParagraphoneText1 || " Peaceful piano to help\nyou slow down, breathe"} cssClass={"C_onefivefour_threethreezerotwoo "}  />
    </CSSTransition >
                    </div>
               
    </CSSTransition >
                    </div>
               
    </CSSTransition >
                    </div>
               
    </CSSTransition >
                    </div>
               
    </CSSTransition >
                    </div>
               
    </CSSTransition >
                    </div>
               
    </CSSTransition >
            
            </>
        }
        </div>
    
</CSSTransition>
            </Contexts.HomeLocalContext.Provider>
        
    ) 
}

Home.propTypes = {
    style: PropTypes.any,
HoneText0: PropTypes.any,
ParagraphtwooText0: PropTypes.any,
ParagraphoneText0: PropTypes.any,
ParagraphtwooText1: PropTypes.any,
ParagraphoneText1: PropTypes.any,
HomeonClick: PropTypes.any,
HomeonMouseEnter: PropTypes.any,
HomeonMouseOver: PropTypes.any,
HomeonKeyPress: PropTypes.any,
HomeonDrag: PropTypes.any,
HomeonMouseLeave: PropTypes.any,
HomeonMouseUp: PropTypes.any,
HomeonMouseDown: PropTypes.any,
HomeonKeyDown: PropTypes.any,
HomeonChange: PropTypes.any,
Homeondelay: PropTypes.any,
DashboardMasterContaineronClick: PropTypes.any,
DashboardMasterContaineronMouseEnter: PropTypes.any,
DashboardMasterContaineronMouseOver: PropTypes.any,
DashboardMasterContaineronKeyPress: PropTypes.any,
DashboardMasterContaineronDrag: PropTypes.any,
DashboardMasterContaineronMouseLeave: PropTypes.any,
DashboardMasterContaineronMouseUp: PropTypes.any,
DashboardMasterContaineronMouseDown: PropTypes.any,
DashboardMasterContaineronKeyDown: PropTypes.any,
DashboardMasterContaineronChange: PropTypes.any,
DashboardMasterContainerondelay: PropTypes.any,
ProjectsCardsDashboardonClick: PropTypes.any,
ProjectsCardsDashboardonMouseEnter: PropTypes.any,
ProjectsCardsDashboardonMouseOver: PropTypes.any,
ProjectsCardsDashboardonKeyPress: PropTypes.any,
ProjectsCardsDashboardonDrag: PropTypes.any,
ProjectsCardsDashboardonMouseLeave: PropTypes.any,
ProjectsCardsDashboardonMouseUp: PropTypes.any,
ProjectsCardsDashboardonMouseDown: PropTypes.any,
ProjectsCardsDashboardonKeyDown: PropTypes.any,
ProjectsCardsDashboardonChange: PropTypes.any,
ProjectsCardsDashboardondelay: PropTypes.any,
HoneonClick: PropTypes.any,
HoneonMouseEnter: PropTypes.any,
HoneonMouseOver: PropTypes.any,
HoneonKeyPress: PropTypes.any,
HoneonDrag: PropTypes.any,
HoneonMouseLeave: PropTypes.any,
HoneonMouseUp: PropTypes.any,
HoneonMouseDown: PropTypes.any,
HoneonKeyDown: PropTypes.any,
HoneonChange: PropTypes.any,
Honeondelay: PropTypes.any,
HoneTextonClick: PropTypes.any,
HoneTextonMouseEnter: PropTypes.any,
HoneTextonMouseOver: PropTypes.any,
HoneTextonKeyPress: PropTypes.any,
HoneTextonDrag: PropTypes.any,
HoneTextonMouseLeave: PropTypes.any,
HoneTextonMouseUp: PropTypes.any,
HoneTextonMouseDown: PropTypes.any,
HoneTextonKeyDown: PropTypes.any,
HoneTextonChange: PropTypes.any,
HoneTextondelay: PropTypes.any,
CardViewonClick: PropTypes.any,
CardViewonMouseEnter: PropTypes.any,
CardViewonMouseOver: PropTypes.any,
CardViewonKeyPress: PropTypes.any,
CardViewonDrag: PropTypes.any,
CardViewonMouseLeave: PropTypes.any,
CardViewonMouseUp: PropTypes.any,
CardViewonMouseDown: PropTypes.any,
CardViewonKeyDown: PropTypes.any,
CardViewonChange: PropTypes.any,
CardViewondelay: PropTypes.any,
CardsRowonClick: PropTypes.any,
CardsRowonMouseEnter: PropTypes.any,
CardsRowonMouseOver: PropTypes.any,
CardsRowonKeyPress: PropTypes.any,
CardsRowonDrag: PropTypes.any,
CardsRowonMouseLeave: PropTypes.any,
CardsRowonMouseUp: PropTypes.any,
CardsRowonMouseDown: PropTypes.any,
CardsRowonKeyDown: PropTypes.any,
CardsRowonChange: PropTypes.any,
CardsRowondelay: PropTypes.any,
CardPlaylistImgContonClick: PropTypes.any,
CardPlaylistImgContonMouseEnter: PropTypes.any,
CardPlaylistImgContonMouseOver: PropTypes.any,
CardPlaylistImgContonKeyPress: PropTypes.any,
CardPlaylistImgContonDrag: PropTypes.any,
CardPlaylistImgContonMouseLeave: PropTypes.any,
CardPlaylistImgContonMouseUp: PropTypes.any,
CardPlaylistImgContonMouseDown: PropTypes.any,
CardPlaylistImgContonKeyDown: PropTypes.any,
CardPlaylistImgContonChange: PropTypes.any,
CardPlaylistImgContondelay: PropTypes.any,
CardPlaylistImgonClick: PropTypes.any,
CardPlaylistImgonMouseEnter: PropTypes.any,
CardPlaylistImgonMouseOver: PropTypes.any,
CardPlaylistImgonKeyPress: PropTypes.any,
CardPlaylistImgonDrag: PropTypes.any,
CardPlaylistImgonMouseLeave: PropTypes.any,
CardPlaylistImgonMouseUp: PropTypes.any,
CardPlaylistImgonMouseDown: PropTypes.any,
CardPlaylistImgonKeyDown: PropTypes.any,
CardPlaylistImgonChange: PropTypes.any,
CardPlaylistImgondelay: PropTypes.any,
ImagePlayOnonClick: PropTypes.any,
ImagePlayOnonMouseEnter: PropTypes.any,
ImagePlayOnonMouseOver: PropTypes.any,
ImagePlayOnonKeyPress: PropTypes.any,
ImagePlayOnonDrag: PropTypes.any,
ImagePlayOnonMouseLeave: PropTypes.any,
ImagePlayOnonMouseUp: PropTypes.any,
ImagePlayOnonMouseDown: PropTypes.any,
ImagePlayOnonKeyDown: PropTypes.any,
ImagePlayOnonChange: PropTypes.any,
ImagePlayOnondelay: PropTypes.any,
CardPlaylistDataonClick: PropTypes.any,
CardPlaylistDataonMouseEnter: PropTypes.any,
CardPlaylistDataonMouseOver: PropTypes.any,
CardPlaylistDataonKeyPress: PropTypes.any,
CardPlaylistDataonDrag: PropTypes.any,
CardPlaylistDataonMouseLeave: PropTypes.any,
CardPlaylistDataonMouseUp: PropTypes.any,
CardPlaylistDataonMouseDown: PropTypes.any,
CardPlaylistDataonKeyDown: PropTypes.any,
CardPlaylistDataonChange: PropTypes.any,
CardPlaylistDataondelay: PropTypes.any
}
export default Home;