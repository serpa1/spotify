import React, { Suspense } from 'react';
import PropTypes from 'prop-types';
import * as Contexts  from 'Contexts';
import { OverlaySwapContext } from 'Components/OverlaySwapProvider';
import CorebooksAnimation from 'Components/CorebooksAnimation';
import CorebooksAnimationSingle from 'Components/CorebooksAnimationSingle';

import * as UtilsScripts  from 'utils/utils';
import { Route, useHistory, useParams } from "react-router-dom";
import SingletoneNavigation from 'Class/SingletoneNavigation';

import LoadingComponent from "Components/Loading";


import { Transition } from "react-transition-group";
import { CSSTransition } from "react-transition-group";
import { useAppContext, useSessionContext } from 'context/AppContext';
import './Frametwoo.css'





const Frametwoo = (props)=>{
    const [state,dispatch] = useAppContext();
    const [, setNameSelectedComponent] = React.useContext(OverlaySwapContext);
    
    const nodeRef = React.useRef(null);
    const history = useHistory();
    let RouterContext = useParams();
    
    
    
    
    
    
    const [globalStateForm, dispatchForm] = React.useState({});
    
    const [ _in, setIn ] = React.useState(false)
    const [ transaction, setTransaction ] = React.useState({}) 
    const [ animation, setAnimation ] = React.useState('')

    
    
    
    
    
    
    React.useEffect(()=>{
        
        
        
    },[]);
    
    
    return (
        <>
  <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['frametwoo']?.animationClass || {}}>

    <div id="id_fourthreeeight_oneeightfoursix" ref={nodeRef} className={` ${ props.onClick ? 'cursor' : '' } frametwoo ${ props.cssClass } ${ transaction['frametwoo']?.type ? transaction['frametwoo']?.type.toLowerCase() : '' }`} style={ { ...{ ...{}, transitionDuration: transaction['frametwoo']?.duration, transitionTimingFunction: transaction['frametwoo']?.timingFunction }, ...props.style }} onClick={ props.FrametwooonClick } onMouseEnter={ props.FrametwooonMouseEnter } onMouseOver={ props.FrametwooonMouseOver } onKeyPress={ props.FrametwooonKeyPress } onDrag={ props.FrametwooonDrag } onMouseLeave={ props.FrametwooonMouseLeave } onMouseUp={ props.FrametwooonMouseUp } onMouseDown={ props.FrametwooonMouseDown } onKeyDown={ props.FrametwooonKeyDown } onChange={ props.FrametwooonChange } ondelay={ props.Frametwooondelay }>
      {
      props.children ?
      props.children :
      <>
        <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['vector']?.animationClass || {}}>
          <svg id="id_fourthreeeight_oneeightfourseven" className="vector  " style={{}} onClick={ props.VectoronClick } onMouseEnter={ props.VectoronMouseEnter } onMouseOver={ props.VectoronMouseOver } onKeyPress={ props.VectoronKeyPress } onDrag={ props.VectoronDrag } onMouseLeave={ props.VectoronMouseLeave } onMouseUp={ props.VectoronMouseUp } onMouseDown={ props.VectoronMouseDown } onKeyDown={ props.VectoronKeyDown } onChange={ props.VectoronChange } ondelay={ props.Vectorondelay } width="16" height="15">
            <path d="M1.75 0C1.28587 4.44089e-16 0.840752 0.184374 0.512563 0.512563C0.184374 0.840752 3.46945e-16 1.28587 0 1.75L2.77556e-16 13.25C2.77556e-16 14.216 0.784 15 1.75 15L14.25 15C14.7141 15 15.1592 14.8156 15.4874 14.4874C15.8156 14.1592 16 13.7141 16 13.25L16 3.75C16 3.28587 15.8156 2.84075 15.4874 2.51256C15.1592 2.18437 14.7141 2 14.25 2L7.82 2L7.17 0.875C7.01645 0.609046 6.79562 0.388181 6.52969 0.234592C6.26376 0.0810018 5.9621 9.5552e-05 5.655 0L1.75 0ZM1.5 1.75C1.5 1.6837 1.52634 1.62011 1.57322 1.57322C1.62011 1.52634 1.6837 1.5 1.75 1.5L5.655 1.5C5.6988 1.50009 5.74181 1.51168 5.77971 1.53362C5.81762 1.55556 5.8491 1.58707 5.871 1.625L6.954 3.5L14.25 3.5C14.3163 3.5 14.3799 3.52634 14.4268 3.57322C14.4737 3.62011 14.5 3.6837 14.5 3.75L14.5 13.25C14.5 13.3163 14.4737 13.3799 14.4268 13.4268C14.3799 13.4737 14.3163 13.5 14.25 13.5L1.75 13.5C1.6837 13.5 1.62011 13.4737 1.57322 13.4268C1.52634 13.3799 1.5 13.3163 1.5 13.25L1.5 1.75Z" />
          </svg>
        </CSSTransition>

      </>
      }
    </div>

  </CSSTransition>
</>
    ) 
}

Frametwoo.propTypes = {
    style: PropTypes.any,
FrametwooonClick: PropTypes.any,
FrametwooonMouseEnter: PropTypes.any,
FrametwooonMouseOver: PropTypes.any,
FrametwooonKeyPress: PropTypes.any,
FrametwooonDrag: PropTypes.any,
FrametwooonMouseLeave: PropTypes.any,
FrametwooonMouseUp: PropTypes.any,
FrametwooonMouseDown: PropTypes.any,
FrametwooonKeyDown: PropTypes.any,
FrametwooonChange: PropTypes.any,
Frametwooondelay: PropTypes.any,
VectoronClick: PropTypes.any,
VectoronMouseEnter: PropTypes.any,
VectoronMouseOver: PropTypes.any,
VectoronKeyPress: PropTypes.any,
VectoronDrag: PropTypes.any,
VectoronMouseLeave: PropTypes.any,
VectoronMouseUp: PropTypes.any,
VectoronMouseDown: PropTypes.any,
VectoronKeyDown: PropTypes.any,
VectoronChange: PropTypes.any,
Vectorondelay: PropTypes.any
}
export default Frametwoo;