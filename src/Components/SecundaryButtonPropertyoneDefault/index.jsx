import React, { Suspense } from 'react';
import PropTypes from 'prop-types';
import * as Contexts  from 'Contexts';
import { OverlaySwapContext } from 'Components/OverlaySwapProvider';
import CorebooksAnimation from 'Components/CorebooksAnimation';
import CorebooksAnimationSingle from 'Components/CorebooksAnimationSingle';

import * as UtilsScripts  from 'utils/utils';
import { Route, useHistory, useParams } from "react-router-dom";
import SingletoneNavigation from 'Class/SingletoneNavigation';

import LoadingComponent from "Components/Loading";


import { Transition } from "react-transition-group";
import { CSSTransition } from "react-transition-group";
import { useAppContext, useSessionContext } from 'context/AppContext';
import './SecundaryButtonPropertyoneDefault.css'





const PropertyoneDefault = (props)=>{
    const [state,dispatch] = useAppContext();
    const [, setNameSelectedComponent] = React.useContext(OverlaySwapContext);
    
    const nodeRef = React.useRef(null);
    const history = useHistory();
    let RouterContext = useParams();
    
    
    
    
    
    
    const [globalStateForm, dispatchForm] = React.useState({});
    
    const [ _in, setIn ] = React.useState(false)
    const [ transaction, setTransaction ] = React.useState({}) 
    const [ animation, setAnimation ] = React.useState('')

    
    
    
    
    
    
    React.useEffect(()=>{
        
        
        
    },[]);
    
    
    return (
        <>
  <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['propertyonedefault']?.animationClass || {}}>

    <div id="id_onesixtwoo_fournigthfivefour" ref={nodeRef} className={` cursor ${ props.onClick ? 'cursor' : '' } propertyonedefault C_onesixtwoo_fournigthfivefour ${ props.cssClass } ${ transaction['propertyonedefault']?.type ? transaction['propertyonedefault']?.type.toLowerCase() : '' }`} style={ { ...{ ...{}, transitionDuration: transaction['propertyonedefault']?.duration, transitionTimingFunction: transaction['propertyonedefault']?.timingFunction }, ...props.style }} onClick={ props.SecundaryButtononClick } onMouseEnter={ props.SecundaryButtononMouseEnter } onMouseOver={ props.SecundaryButtononMouseOver || function(e){ setTransaction({ }); setvariant('Property 1=Variant2'); }} onKeyPress={ props.SecundaryButtononKeyPress } onDrag={ props.SecundaryButtononDrag } onMouseLeave={ props.SecundaryButtononMouseLeave } onMouseUp={ props.SecundaryButtononMouseUp } onMouseDown={ props.SecundaryButtononMouseDown } onKeyDown={ props.SecundaryButtononKeyDown } onChange={ props.SecundaryButtononChange } ondelay={ props.SecundaryButtonondelay }>
      {
      props.children ?
      props.children :
      <>
        <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['secundarybuttontext']?.animationClass || {}}>

          <span id="id_onesixtwoo_fournigthfivethree"  className={` text secundarybuttontext    ${ props.onClick ? 'cursor' : ''}  ${ transaction['secundarybuttontext']?.type ? transaction['secundarybuttontext']?.type.toLowerCase() : '' }`} style={{ ...{},  ...props.SecundaryButtonTextStyle , transitionDuration: transaction['secundarybuttontext']?.duration, transitionTimingFunction: transaction['secundarybuttontext']?.timingFunction }} onClick={ props.SecundaryButtonTextonClick } onMouseEnter={ props.SecundaryButtonTextonMouseEnter } onMouseOver={ props.SecundaryButtonTextonMouseOver } onKeyPress={ props.SecundaryButtonTextonKeyPress } onDrag={ props.SecundaryButtonTextonDrag } onMouseLeave={ props.SecundaryButtonTextonMouseLeave } onMouseUp={ props.SecundaryButtonTextonMouseUp } onMouseDown={ props.SecundaryButtonTextonMouseDown } onKeyDown={ props.SecundaryButtonTextonKeyDown } onChange={ props.SecundaryButtonTextonChange } ondelay={ props.SecundaryButtonTextondelay } >{props.SecundaryButtonText0 || `Crear playlist`}</span>

        </CSSTransition>

      </>
      }
    </div>

  </CSSTransition>
</>
    ) 
}

PropertyoneDefault.propTypes = {
    style: PropTypes.any,
SecundaryButtonText0: PropTypes.any,
SecundaryButtononClick: PropTypes.any,
SecundaryButtononMouseEnter: PropTypes.any,
SecundaryButtononMouseOver: PropTypes.any,
SecundaryButtononKeyPress: PropTypes.any,
SecundaryButtononDrag: PropTypes.any,
SecundaryButtononMouseLeave: PropTypes.any,
SecundaryButtononMouseUp: PropTypes.any,
SecundaryButtononMouseDown: PropTypes.any,
SecundaryButtononKeyDown: PropTypes.any,
SecundaryButtononChange: PropTypes.any,
SecundaryButtonondelay: PropTypes.any,
SecundaryButtonTextonClick: PropTypes.any,
SecundaryButtonTextonMouseEnter: PropTypes.any,
SecundaryButtonTextonMouseOver: PropTypes.any,
SecundaryButtonTextonKeyPress: PropTypes.any,
SecundaryButtonTextonDrag: PropTypes.any,
SecundaryButtonTextonMouseLeave: PropTypes.any,
SecundaryButtonTextonMouseUp: PropTypes.any,
SecundaryButtonTextonMouseDown: PropTypes.any,
SecundaryButtonTextonKeyDown: PropTypes.any,
SecundaryButtonTextonChange: PropTypes.any,
SecundaryButtonTextondelay: PropTypes.any
}
export default PropertyoneDefault;