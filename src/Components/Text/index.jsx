import React, { Suspense } from 'react';
import PropTypes from 'prop-types';
import * as Contexts  from 'Contexts';
import { OverlaySwapContext } from 'Components/OverlaySwapProvider';
import CorebooksAnimation from 'Components/CorebooksAnimation';
import CorebooksAnimationSingle from 'Components/CorebooksAnimationSingle';

import * as UtilsScripts  from 'utils/utils';
import { Route, useHistory, useParams } from "react-router-dom";
import SingletoneNavigation from 'Class/SingletoneNavigation';

import LoadingComponent from "Components/Loading";


import { Transition } from "react-transition-group";
import { CSSTransition } from "react-transition-group";
import TextLink from 'Components/TextLink'
import Paragraphtwoo from 'Components/Paragraphtwoo'
import Paragraphone from 'Components/Paragraphone'
import Subtitleone from 'Components/Subtitleone'
import HFive from 'Components/HFive'
import Hfour from 'Components/Hfour'
import Hthree from 'Components/Hthree'
import Htwoo from 'Components/Htwoo'
import { useAppContext, useSessionContext } from 'context/AppContext';
import './Text.css'





const Text = (props)=>{
    const [state,dispatch] = useAppContext();
    const [, setNameSelectedComponent] = React.useContext(OverlaySwapContext);
    
    const nodeRef = React.useRef(null);
    const history = useHistory();
    let RouterContext = useParams();
    
    
    
    
    
    
    const [globalStateForm, dispatchForm] = React.useState({});
    
    const [ _in, setIn ] = React.useState(false)
    const [ transaction, setTransaction ] = React.useState({}) 
    const [ animation, setAnimation ] = React.useState('')

    
    
    
    
    
    
    React.useEffect(()=>{
        
        
        
    },[]);
    
    
    return (
        <>
  <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['text']?.animationClass || {}}>

    <div id="id_one_eightnigthfive" ref={nodeRef} className={` ${ props.onClick ? 'cursor' : '' } text ${ props.cssClass } ${ transaction['text']?.type ? transaction['text']?.type.toLowerCase() : '' }`} style={ { ...{ ...{}, transitionDuration: transaction['text']?.duration, transitionTimingFunction: transaction['text']?.timingFunction }, ...props.style }} onClick={ props.TextonClick } onMouseEnter={ props.TextonMouseEnter } onMouseOver={ props.TextonMouseOver } onKeyPress={ props.TextonKeyPress } onDrag={ props.TextonDrag } onMouseLeave={ props.TextonMouseLeave } onMouseUp={ props.TextonMouseUp } onMouseDown={ props.TextonMouseDown } onKeyDown={ props.TextonKeyDown } onChange={ props.TextonChange } ondelay={ props.Textondelay }>
      {
      props.children ?
      props.children :
      <>
        <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['hone']?.animationClass || {}}>

          <div id="id_twootwoo_onefourtwoo" className={` instance hone ${ props.onClick ? 'cursor' : '' } ${ transaction['hone']?.type ? transaction['hone']?.type.toLowerCase() : '' }`} style={{ ...{},  ...props.HoneStyle , transitionDuration: transaction['hone']?.duration, transitionTimingFunction: transaction['hone']?.timingFunction }} onClick={ props.HoneonClick } onMouseEnter={ props.HoneonMouseEnter } onMouseOver={ props.HoneonMouseOver } onKeyPress={ props.HoneonKeyPress } onDrag={ props.HoneonDrag } onMouseLeave={ props.HoneonMouseLeave } onMouseUp={ props.HoneonMouseUp } onMouseDown={ props.HoneonMouseDown } onKeyDown={ props.HoneonKeyDown } onChange={ props.HoneonChange } ondelay={ props.Honeondelay }>
            <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['honetext']?.animationClass || {}}>

              <span id="id_twootwoo_onefourtwoo_one_eightnigthseven"  className={` text honetext    ${ props.onClick ? 'cursor' : ''}  ${ transaction['honetext']?.type ? transaction['honetext']?.type.toLowerCase() : '' }`} style={{ ...{},  ...props.HoneTextStyle , transitionDuration: transaction['honetext']?.duration, transitionTimingFunction: transaction['honetext']?.timingFunction }} onClick={ props.HoneTextonClick } onMouseEnter={ props.HoneTextonMouseEnter } onMouseOver={ props.HoneTextonMouseOver } onKeyPress={ props.HoneTextonKeyPress } onDrag={ props.HoneTextonDrag } onMouseLeave={ props.HoneTextonMouseLeave } onMouseUp={ props.HoneTextonMouseUp } onMouseDown={ props.HoneTextonMouseDown } onKeyDown={ props.HoneTextonKeyDown } onChange={ props.HoneTextonChange } ondelay={ props.HoneTextondelay } >{props.HoneText0 || `Your top mixes`}</span>

            </CSSTransition>
          </div>

        </CSSTransition>
        <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['htwoo']?.animationClass || {}}>
          <Htwoo { ...{ ...props, style:false } } cssClass={"C_three_oneeightfoursix "}  />
    </CSSTransition >
<CSSTransition 
        in={_in}
        appear={true} 
        timeout={100}
        classNames={transaction['hthree']?.animationClass || {}}
    >
    <Hthree { ...{ ...props, style:false } }    cssClass={" C_three_oneeightfourseven "}  />
    </CSSTransition >
<CSSTransition 
        in={_in}
        appear={true} 
        timeout={100}
        classNames={transaction['hfour']?.animationClass || {}}
    >
    <Hfour { ...{ ...props, style:false } }    cssClass={" C_three_oneeightfoureight "}  />
    </CSSTransition >
<CSSTransition 
        in={_in}
        appear={true} 
        timeout={100}
        classNames={transaction['hfive']?.animationClass || {}}
    >
    <HFive { ...{ ...props, style:false } }    cssClass={" C_three_oneeightfournigth "}  />
    </CSSTransition >
<CSSTransition 
        in={_in}
        appear={true} 
        timeout={100}
        classNames={transaction['subtitleone']?.animationClass || {}}
    >
    <Subtitleone { ...{ ...props, style:false } }    cssClass={" C_three_oneeightfivezero "}  />
    </CSSTransition >
<CSSTransition 
        in={_in}
        appear={true} 
        timeout={100}
        classNames={transaction['paragraphone']?.animationClass || {}}
    >
    <Paragraphone { ...{ ...props, style:false } }    cssClass={" C_three_oneeightfivetwoo "}  />
    </CSSTransition >
<CSSTransition 
        in={_in}
        appear={true} 
        timeout={100}
        classNames={transaction['paragraphtwoo']?.animationClass || {}}
    >
    <Paragraphtwoo { ...{ ...props, style:false } }    cssClass={" C_oneseven_twoosixfour "}  />
    </CSSTransition >
<CSSTransition 
        in={_in}
        appear={true} 
        timeout={100}
        classNames={transaction['textlink']?.animationClass || {}}
    >
    <TextLink { ...{ ...props, style:false } }  variant={'Property 1=Default'} TextLinkTxt0={ props.TextLinkTxt0 || " Acerca de"} cssClass={"C_twootwoo_onetwooone "}  />
    </CSSTransition >
            
            </>
        }
        </div>
    
    </CSSTransition >
            </>
        
    ) 
}

Text.propTypes = {
    style: PropTypes.any,
HoneText0: PropTypes.any,
TextLinkTxt0: PropTypes.any,
TextonClick: PropTypes.any,
TextonMouseEnter: PropTypes.any,
TextonMouseOver: PropTypes.any,
TextonKeyPress: PropTypes.any,
TextonDrag: PropTypes.any,
TextonMouseLeave: PropTypes.any,
TextonMouseUp: PropTypes.any,
TextonMouseDown: PropTypes.any,
TextonKeyDown: PropTypes.any,
TextonChange: PropTypes.any,
Textondelay: PropTypes.any,
HoneonClick: PropTypes.any,
HoneonMouseEnter: PropTypes.any,
HoneonMouseOver: PropTypes.any,
HoneonKeyPress: PropTypes.any,
HoneonDrag: PropTypes.any,
HoneonMouseLeave: PropTypes.any,
HoneonMouseUp: PropTypes.any,
HoneonMouseDown: PropTypes.any,
HoneonKeyDown: PropTypes.any,
HoneonChange: PropTypes.any,
Honeondelay: PropTypes.any,
HoneTextonClick: PropTypes.any,
HoneTextonMouseEnter: PropTypes.any,
HoneTextonMouseOver: PropTypes.any,
HoneTextonKeyPress: PropTypes.any,
HoneTextonDrag: PropTypes.any,
HoneTextonMouseLeave: PropTypes.any,
HoneTextonMouseUp: PropTypes.any,
HoneTextonMouseDown: PropTypes.any,
HoneTextonKeyDown: PropTypes.any,
HoneTextonChange: PropTypes.any,
HoneTextondelay: PropTypes.any
}
export default Text;