import React, { Suspense } from 'react';
import PropTypes from 'prop-types';
import * as Contexts  from 'Contexts';
import { OverlaySwapContext } from 'Components/OverlaySwapProvider';
import CorebooksAnimation from 'Components/CorebooksAnimation';
import CorebooksAnimationSingle from 'Components/CorebooksAnimationSingle';

import * as UtilsScripts  from 'utils/utils';
import { Route, useHistory, useParams } from "react-router-dom";
import SingletoneNavigation from 'Class/SingletoneNavigation';

import LoadingComponent from "Components/Loading";


import { Transition } from "react-transition-group";
import { CSSTransition } from "react-transition-group";
import { useAppContext, useSessionContext } from 'context/AppContext';
import './AgregadosRecientementePropertyoneVariantthree.css'





const PropertyoneVariantthree = (props)=>{
    const [state,dispatch] = useAppContext();
    const [, setNameSelectedComponent] = React.useContext(OverlaySwapContext);
    
    const nodeRef = React.useRef(null);
    const history = useHistory();
    let RouterContext = useParams();
    
    
    
    
    
    
    const [globalStateForm, dispatchForm] = React.useState({});
    
    const [ _in, setIn ] = React.useState(false)
    const [ transaction, setTransaction ] = React.useState({}) 
    const [ animation, setAnimation ] = React.useState('')

    
    
    
    
    
    
    React.useEffect(()=>{
        
        
        
    },[]);
    
    
    return (
        <>
  <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['propertyonevariantthree']?.animationClass || {}}>

    <div id="id_fourthreeeight_fourtwoofivefour" ref={nodeRef} className={` ${ props.onClick ? 'cursor' : '' } propertyonevariantthree C_fourthreeeight_fourtwoofivefour ${ props.cssClass } ${ transaction['propertyonevariantthree']?.type ? transaction['propertyonevariantthree']?.type.toLowerCase() : '' }`} style={ { ...{ ...{}, transitionDuration: transaction['propertyonevariantthree']?.duration, transitionTimingFunction: transaction['propertyonevariantthree']?.timingFunction }, ...props.style }} onClick={ props.AgregadosRecientementeonClick } onMouseEnter={ props.AgregadosRecientementeonMouseEnter } onMouseOver={ props.AgregadosRecientementeonMouseOver } onKeyPress={ props.AgregadosRecientementeonKeyPress } onDrag={ props.AgregadosRecientementeonDrag } onMouseLeave={ props.AgregadosRecientementeonMouseLeave } onMouseUp={ props.AgregadosRecientementeonMouseUp } onMouseDown={ props.AgregadosRecientementeonMouseDown } onKeyDown={ props.AgregadosRecientementeonKeyDown } onChange={ props.AgregadosRecientementeonChange } ondelay={ props.AgregadosRecientementeondelay }>
      {
      props.children ?
      props.children :
      <>
        <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['agregadosrecientemente']?.animationClass || {}}>

          <span id="id_fourthreeeight_fourtwoofivefive"  className={` text agregadosrecientemente    ${ props.onClick ? 'cursor' : ''}  ${ transaction['agregadosrecientemente']?.type ? transaction['agregadosrecientemente']?.type.toLowerCase() : '' }`} style={{ ...{},  ...props.AgregadosrecientementeStyle , transitionDuration: transaction['agregadosrecientemente']?.duration, transitionTimingFunction: transaction['agregadosrecientemente']?.timingFunction }} onClick={ props.AgregadosrecientementeonClick } onMouseEnter={ props.AgregadosrecientementeonMouseEnter } onMouseOver={ props.AgregadosrecientementeonMouseOver } onKeyPress={ props.AgregadosrecientementeonKeyPress } onDrag={ props.AgregadosrecientementeonDrag } onMouseLeave={ props.AgregadosrecientementeonMouseLeave } onMouseUp={ props.AgregadosrecientementeonMouseUp } onMouseDown={ props.AgregadosrecientementeonMouseDown } onKeyDown={ props.AgregadosrecientementeonKeyDown } onChange={ props.AgregadosrecientementeonChange } ondelay={ props.Agregadosrecientementeondelay } >{props.Agregadosrecientemente0 || `Agregados recientemente`}</span>

        </CSSTransition>
        <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['rectangleonezero']?.animationClass || {}}>
          <img id="id_fourthreeeight_fourtwoofivesix" className={` rectangle rectangleonezero ${ props.onClick ? 'cursor' : '' } ${ transaction['rectangleonezero']?.type ? transaction['rectangleonezero']?.type.toLowerCase() : '' }`} style={{ ...{},  ...props.RectangleonezeroStyle , transitionDuration: transaction['rectangleonezero']?.duration, transitionTimingFunction: transaction['rectangleonezero']?.timingFunction }} onClick={ props.RectangleonezeroonClick } onMouseEnter={ props.RectangleonezeroonMouseEnter } onMouseOver={ props.RectangleonezeroonMouseOver } onKeyPress={ props.RectangleonezeroonKeyPress } onDrag={ props.RectangleonezeroonDrag } onMouseLeave={ props.RectangleonezeroonMouseLeave } onMouseUp={ props.RectangleonezeroonMouseUp } onMouseDown={ props.RectangleonezeroonMouseDown } onKeyDown={ props.RectangleonezeroonKeyDown } onChange={ props.RectangleonezeroonChange } ondelay={ props.Rectangleonezeroondelay } src={props.Rectangleonezero0 || "https://cdn.uncodie.com/tACQBrKCV97e0EqPE1XNLJ/44841fd43ab07855e7aa0aa775800c4be4ff0f83.png" } />
        </CSSTransition>

      </>
      }
    </div>

  </CSSTransition>
</>
    ) 
}

PropertyoneVariantthree.propTypes = {
    style: PropTypes.any,
Agregadosrecientemente0: PropTypes.any,
Rectangleonezero0: PropTypes.any,
AgregadosRecientementeonClick: PropTypes.any,
AgregadosRecientementeonMouseEnter: PropTypes.any,
AgregadosRecientementeonMouseOver: PropTypes.any,
AgregadosRecientementeonKeyPress: PropTypes.any,
AgregadosRecientementeonDrag: PropTypes.any,
AgregadosRecientementeonMouseLeave: PropTypes.any,
AgregadosRecientementeonMouseUp: PropTypes.any,
AgregadosRecientementeonMouseDown: PropTypes.any,
AgregadosRecientementeonKeyDown: PropTypes.any,
AgregadosRecientementeonChange: PropTypes.any,
AgregadosRecientementeondelay: PropTypes.any,
AgregadosrecientementeonClick: PropTypes.any,
AgregadosrecientementeonMouseEnter: PropTypes.any,
AgregadosrecientementeonMouseOver: PropTypes.any,
AgregadosrecientementeonKeyPress: PropTypes.any,
AgregadosrecientementeonDrag: PropTypes.any,
AgregadosrecientementeonMouseLeave: PropTypes.any,
AgregadosrecientementeonMouseUp: PropTypes.any,
AgregadosrecientementeonMouseDown: PropTypes.any,
AgregadosrecientementeonKeyDown: PropTypes.any,
AgregadosrecientementeonChange: PropTypes.any,
Agregadosrecientementeondelay: PropTypes.any,
RectangleonezeroonClick: PropTypes.any,
RectangleonezeroonMouseEnter: PropTypes.any,
RectangleonezeroonMouseOver: PropTypes.any,
RectangleonezeroonKeyPress: PropTypes.any,
RectangleonezeroonDrag: PropTypes.any,
RectangleonezeroonMouseLeave: PropTypes.any,
RectangleonezeroonMouseUp: PropTypes.any,
RectangleonezeroonMouseDown: PropTypes.any,
RectangleonezeroonKeyDown: PropTypes.any,
RectangleonezeroonChange: PropTypes.any,
Rectangleonezeroondelay: PropTypes.any
}
export default PropertyoneVariantthree;