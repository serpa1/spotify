import React, { Suspense } from 'react';
import PropTypes from 'prop-types';
import * as Contexts  from 'Contexts';
import { OverlaySwapContext } from 'Components/OverlaySwapProvider';
import CorebooksAnimation from 'Components/CorebooksAnimation';
import CorebooksAnimationSingle from 'Components/CorebooksAnimationSingle';

import * as UtilsScripts  from 'utils/utils';
import { Route, useHistory, useParams } from "react-router-dom";
import SingletoneNavigation from 'Class/SingletoneNavigation';

import LoadingComponent from "Components/Loading";


import { Transition } from "react-transition-group";
import { CSSTransition } from "react-transition-group";
import SelectRectangle from 'Components/SelectRectangle'
import { useAppContext, useSessionContext } from 'context/AppContext';
import './Groupone.css'





const Groupone = (props)=>{
    const [state,dispatch] = useAppContext();
    const [, setNameSelectedComponent] = React.useContext(OverlaySwapContext);
    
    const nodeRef = React.useRef(null);
    const history = useHistory();
    let RouterContext = useParams();
    
    
    
    
    
    
    const [globalStateForm, dispatchForm] = React.useState({});
    
    const [ _in, setIn ] = React.useState(false)
    const [ transaction, setTransaction ] = React.useState({}) 
    const [ animation, setAnimation ] = React.useState('')

    
    
    
    
    
    
    React.useEffect(()=>{
        
        
        
    },[]);
    
    
    return (
        <>
  <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['groupone']?.animationClass || {}}>

    <div id="id_fourthreeeight_onefoursixnigth" ref={nodeRef} className={` ${ props.onClick ? 'cursor' : '' } groupone ${ props.cssClass } ${ transaction['groupone']?.type ? transaction['groupone']?.type.toLowerCase() : '' }`} style={ { ...{ ...{}, transitionDuration: transaction['groupone']?.duration, transitionTimingFunction: transaction['groupone']?.timingFunction }, ...props.style }} onClick={ props.GrouponeonClick } onMouseEnter={ props.GrouponeonMouseEnter } onMouseOver={ props.GrouponeonMouseOver } onKeyPress={ props.GrouponeonKeyPress } onDrag={ props.GrouponeonDrag } onMouseLeave={ props.GrouponeonMouseLeave } onMouseUp={ props.GrouponeonMouseUp } onMouseDown={ props.GrouponeonMouseDown } onKeyDown={ props.GrouponeonKeyDown } onChange={ props.GrouponeonChange } ondelay={ props.Grouponeondelay }>
      {
      props.children ?
      props.children :
      <>
        <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['selectrectangle']?.animationClass || {}}>
          <SelectRectangle { ...{ ...props, style:false } } cssClass={"C_fourthreeeight_onefoursixseven "}  />
    </CSSTransition >
<CSSTransition 
        in={_in}
        appear={true} 
        timeout={100}
        classNames={transaction['vector']?.animationClass || {}}
    >
    <svg  id="id_fourthreeeight_onefoursixfive" className="vector  " style={{}} onClick={ props.VectoronClick } onMouseEnter={ props.VectoronMouseEnter } onMouseOver={ props.VectoronMouseOver } onKeyPress={ props.VectoronKeyPress } onDrag={ props.VectoronDrag } onMouseLeave={ props.VectoronMouseLeave } onMouseUp={ props.VectoronMouseUp } onMouseDown={ props.VectoronMouseDown } onKeyDown={ props.VectoronKeyDown } onChange={ props.VectoronChange } ondelay={ props.Vectorondelay } width="14" height="14">
            <path d="M12.4444 0L1.55556 0C0.692222 0 0 0.7 0 1.55556L0 12.4444C0 13.3 0.692222 14 1.55556 14L12.4444 14C13.3078 14 14 13.3 14 12.4444L14 1.55556C14 0.7 13.3078 0 12.4444 0ZM5.44444 10.8889L1.55556 7L2.65222 5.90333L5.44444 8.68778L11.3478 2.78444L12.4444 3.88889L5.44444 10.8889Z" />
            </svg>
        </CSSTransition>

      </>
      }
    </div>

  </CSSTransition>
</>
    ) 
}

Groupone.propTypes = {
    style: PropTypes.any,
GrouponeonClick: PropTypes.any,
GrouponeonMouseEnter: PropTypes.any,
GrouponeonMouseOver: PropTypes.any,
GrouponeonKeyPress: PropTypes.any,
GrouponeonDrag: PropTypes.any,
GrouponeonMouseLeave: PropTypes.any,
GrouponeonMouseUp: PropTypes.any,
GrouponeonMouseDown: PropTypes.any,
GrouponeonKeyDown: PropTypes.any,
GrouponeonChange: PropTypes.any,
Grouponeondelay: PropTypes.any,
VectoronClick: PropTypes.any,
VectoronMouseEnter: PropTypes.any,
VectoronMouseOver: PropTypes.any,
VectoronKeyPress: PropTypes.any,
VectoronDrag: PropTypes.any,
VectoronMouseLeave: PropTypes.any,
VectoronMouseUp: PropTypes.any,
VectoronMouseDown: PropTypes.any,
VectoronKeyDown: PropTypes.any,
VectoronChange: PropTypes.any,
Vectorondelay: PropTypes.any
}
export default Groupone;