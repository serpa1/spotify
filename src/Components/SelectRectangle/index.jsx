import React, { Suspense } from 'react';
import PropTypes from 'prop-types';
import * as Contexts  from 'Contexts';
import { OverlaySwapContext } from 'Components/OverlaySwapProvider';
import CorebooksAnimation from 'Components/CorebooksAnimation';
import CorebooksAnimationSingle from 'Components/CorebooksAnimationSingle';

import * as UtilsScripts  from 'utils/utils';
import { Route, useHistory, useParams } from "react-router-dom";
import SingletoneNavigation from 'Class/SingletoneNavigation';

import LoadingComponent from "Components/Loading";


import { Transition } from "react-transition-group";
import { CSSTransition } from "react-transition-group";
import { useAppContext, useSessionContext } from 'context/AppContext';
import './SelectRectangle.css'





const SelectRectangle = (props)=>{
    const [state,dispatch] = useAppContext();
    const [, setNameSelectedComponent] = React.useContext(OverlaySwapContext);
    
    const nodeRef = React.useRef(null);
    const history = useHistory();
    let RouterContext = useParams();
    
    
    
    
    
    
    const [globalStateForm, dispatchForm] = React.useState({});
    
    const [ _in, setIn ] = React.useState(false)
    const [ transaction, setTransaction ] = React.useState({}) 
    const [ animation, setAnimation ] = React.useState('')

    
    
    
    
    
    
    React.useEffect(()=>{
        
        
        
    },[]);
    
    
    return (
        <>
  <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['selectrectangle']?.animationClass || {}}>

    <div id="id_fourthreefour_oneeightonezero" ref={nodeRef} className={` ${ props.onClick ? 'cursor' : '' } selectrectangle C_fourthreefour_oneeightonezero ${ props.cssClass } ${ transaction['selectrectangle']?.type ? transaction['selectrectangle']?.type.toLowerCase() : '' }`} style={ { ...{ ...{}, transitionDuration: transaction['selectrectangle']?.duration, transitionTimingFunction: transaction['selectrectangle']?.timingFunction }, ...props.style }} onClick={ props.SelectRectangleonClick } onMouseEnter={ props.SelectRectangleonMouseEnter } onMouseOver={ props.SelectRectangleonMouseOver } onKeyPress={ props.SelectRectangleonKeyPress } onDrag={ props.SelectRectangleonDrag } onMouseLeave={ props.SelectRectangleonMouseLeave } onMouseUp={ props.SelectRectangleonMouseUp } onMouseDown={ props.SelectRectangleonMouseDown } onKeyDown={ props.SelectRectangleonKeyDown } onChange={ props.SelectRectangleonChange } ondelay={ props.SelectRectangleondelay }>
      {
      props.children ?
      props.children :
      <>
        <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['selectrectanglevector']?.animationClass || {}}>
          <div id="id_fourthreefour_oneeightzeroeight" className={` rectangle selectrectanglevector ${ props.onClick ? 'cursor' : '' } ${ transaction['selectrectanglevector']?.type ? transaction['selectrectanglevector']?.type.toLowerCase() : '' }`} style={{ ...{},  ...props.SelectRectangleVectorStyle , transitionDuration: transaction['selectrectanglevector']?.duration, transitionTimingFunction: transaction['selectrectanglevector']?.timingFunction }} onClick={ props.SelectRectangleVectoronClick } onMouseEnter={ props.SelectRectangleVectoronMouseEnter } onMouseOver={ props.SelectRectangleVectoronMouseOver } onKeyPress={ props.SelectRectangleVectoronKeyPress } onDrag={ props.SelectRectangleVectoronDrag } onMouseLeave={ props.SelectRectangleVectoronMouseLeave } onMouseUp={ props.SelectRectangleVectoronMouseUp } onMouseDown={ props.SelectRectangleVectoronMouseDown } onKeyDown={ props.SelectRectangleVectoronKeyDown } onChange={ props.SelectRectangleVectoronChange } ondelay={ props.SelectRectangleVectorondelay }></div>
        </CSSTransition>

      </>
      }
    </div>

  </CSSTransition>
</>
    ) 
}

SelectRectangle.propTypes = {
    style: PropTypes.any,
SelectRectangleVector0: PropTypes.any,
SelectRectangleonClick: PropTypes.any,
SelectRectangleonMouseEnter: PropTypes.any,
SelectRectangleonMouseOver: PropTypes.any,
SelectRectangleonKeyPress: PropTypes.any,
SelectRectangleonDrag: PropTypes.any,
SelectRectangleonMouseLeave: PropTypes.any,
SelectRectangleonMouseUp: PropTypes.any,
SelectRectangleonMouseDown: PropTypes.any,
SelectRectangleonKeyDown: PropTypes.any,
SelectRectangleonChange: PropTypes.any,
SelectRectangleondelay: PropTypes.any,
SelectRectangleVectoronClick: PropTypes.any,
SelectRectangleVectoronMouseEnter: PropTypes.any,
SelectRectangleVectoronMouseOver: PropTypes.any,
SelectRectangleVectoronKeyPress: PropTypes.any,
SelectRectangleVectoronDrag: PropTypes.any,
SelectRectangleVectoronMouseLeave: PropTypes.any,
SelectRectangleVectoronMouseUp: PropTypes.any,
SelectRectangleVectoronMouseDown: PropTypes.any,
SelectRectangleVectoronKeyDown: PropTypes.any,
SelectRectangleVectoronChange: PropTypes.any,
SelectRectangleVectorondelay: PropTypes.any
}
export default SelectRectangle;