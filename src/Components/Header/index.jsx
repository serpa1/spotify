import React, { Suspense } from 'react';
import PropTypes from 'prop-types';
import * as Contexts  from 'Contexts';
import { OverlaySwapContext } from 'Components/OverlaySwapProvider';
import CorebooksAnimation from 'Components/CorebooksAnimation';
import CorebooksAnimationSingle from 'Components/CorebooksAnimationSingle';

import * as UtilsScripts  from 'utils/utils';
import { Route, useHistory, useParams } from "react-router-dom";
import SingletoneNavigation from 'Class/SingletoneNavigation';

import LoadingComponent from "Components/Loading";


import { Transition } from "react-transition-group";
import { CSSTransition } from "react-transition-group";
import PrincipalButton from 'Components/PrincipalButton'
import TextLink from 'Components/TextLink'
import { useAppContext, useSessionContext } from 'context/AppContext';
import './Header.css'





const Header = (props)=>{
    const [state,dispatch] = useAppContext();
    const [, setNameSelectedComponent] = React.useContext(OverlaySwapContext);
    
    const nodeRef = React.useRef(null);
    const history = useHistory();
    let RouterContext = useParams();
    
    
    
    
    
    
    const [globalStateForm, dispatchForm] = React.useState({});
    
    const [ _in, setIn ] = React.useState(false)
    const [ transaction, setTransaction ] = React.useState({}) 
    const [ animation, setAnimation ] = React.useState('')

    
    
    
    
    
    
    React.useEffect(()=>{
        
        
        
    },[]);
    
    
    return (
        <>
  <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['header']?.animationClass || {}}>

    <div id="id_twoofourfour_nigthfoureight" ref={nodeRef} className={` ${ props.onClick ? 'cursor' : '' } header C_twoofourfour_nigthfoureight ${ props.cssClass } ${ transaction['header']?.type ? transaction['header']?.type.toLowerCase() : '' }`} style={ { ...{ ...{"width":"100%"}, transitionDuration: transaction['header']?.duration, transitionTimingFunction: transaction['header']?.timingFunction }, ...props.style }} onClick={ props.HeaderonClick } onMouseEnter={ props.HeaderonMouseEnter } onMouseOver={ props.HeaderonMouseOver } onKeyPress={ props.HeaderonKeyPress } onDrag={ props.HeaderonDrag } onMouseLeave={ props.HeaderonMouseLeave } onMouseUp={ props.HeaderonMouseUp } onMouseDown={ props.HeaderonMouseDown } onKeyDown={ props.HeaderonKeyDown } onChange={ props.HeaderonChange } ondelay={ props.Headerondelay }>
      {
      props.children ?
      props.children :
      <>
        <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['headercont']?.animationClass || {}}>

          <div id="id_twoooneeight_onefoursixtwoo" className={` frame headercont ${ props.onClick ? 'cursor' : '' } ${ transaction['headercont']?.type ? transaction['headercont']?.type.toLowerCase() : '' }`} style={ { ...{"width":"100%"}, ...props.HeaderContStyle , transitionDuration: transaction['headercont']?.duration, transitionTimingFunction: transaction['headercont']?.timingFunction } } onClick={ props.HeaderContonClick } onMouseEnter={ props.HeaderContonMouseEnter } onMouseOver={ props.HeaderContonMouseOver } onKeyPress={ props.HeaderContonKeyPress } onDrag={ props.HeaderContonDrag } onMouseLeave={ props.HeaderContonMouseLeave } onMouseUp={ props.HeaderContonMouseUp } onMouseDown={ props.HeaderContonMouseDown } onKeyDown={ props.HeaderContonKeyDown } onChange={ props.HeaderContonChange } ondelay={ props.HeaderContondelay }>
            <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['headerdata']?.animationClass || {}}>

              <div id="id_twoooneeight_onefoursixthree" className={` frame headerdata ${ props.onClick ? 'cursor' : '' } ${ transaction['headerdata']?.type ? transaction['headerdata']?.type.toLowerCase() : '' }`} style={ { ...{"width":"100%"}, ...props.HeaderDataStyle , transitionDuration: transaction['headerdata']?.duration, transitionTimingFunction: transaction['headerdata']?.timingFunction } } onClick={ props.HeaderDataonClick } onMouseEnter={ props.HeaderDataonMouseEnter } onMouseOver={ props.HeaderDataonMouseOver } onKeyPress={ props.HeaderDataonKeyPress } onDrag={ props.HeaderDataonDrag } onMouseLeave={ props.HeaderDataonMouseLeave } onMouseUp={ props.HeaderDataonMouseUp } onMouseDown={ props.HeaderDataonMouseDown } onKeyDown={ props.HeaderDataonKeyDown } onChange={ props.HeaderDataonChange } ondelay={ props.HeaderDataondelay }>
                <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['headerchevroncont']?.animationClass || {}}>

                  <div id="id_twoooneeight_onefoursixfour" className={` frame headerchevroncont ${ props.onClick ? 'cursor' : '' } ${ transaction['headerchevroncont']?.type ? transaction['headerchevroncont']?.type.toLowerCase() : '' }`} style={ { ...{}, ...props.HeaderChevronContStyle , transitionDuration: transaction['headerchevroncont']?.duration, transitionTimingFunction: transaction['headerchevroncont']?.timingFunction } } onClick={ props.HeaderChevronContonClick } onMouseEnter={ props.HeaderChevronContonMouseEnter } onMouseOver={ props.HeaderChevronContonMouseOver } onKeyPress={ props.HeaderChevronContonKeyPress } onDrag={ props.HeaderChevronContonDrag } onMouseLeave={ props.HeaderChevronContonMouseLeave } onMouseUp={ props.HeaderChevronContonMouseUp } onMouseDown={ props.HeaderChevronContonMouseDown } onKeyDown={ props.HeaderChevronContonKeyDown } onChange={ props.HeaderChevronContonChange } ondelay={ props.HeaderChevronContondelay }>
                    <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['iconleftoffimg']?.animationClass || {}}>
                      <img id="id_twoooneeight_onefoursixfive" className={` rectangle iconleftoffimg ${ props.onClick ? 'cursor' : '' } ${ transaction['iconleftoffimg']?.type ? transaction['iconleftoffimg']?.type.toLowerCase() : '' }`} style={{ ...{},  ...props.IconLeftOffImgStyle , transitionDuration: transaction['iconleftoffimg']?.duration, transitionTimingFunction: transaction['iconleftoffimg']?.timingFunction }} onClick={ props.IconLeftOffImgonClick } onMouseEnter={ props.IconLeftOffImgonMouseEnter } onMouseOver={ props.IconLeftOffImgonMouseOver } onKeyPress={ props.IconLeftOffImgonKeyPress } onDrag={ props.IconLeftOffImgonDrag } onMouseLeave={ props.IconLeftOffImgonMouseLeave } onMouseUp={ props.IconLeftOffImgonMouseUp } onMouseDown={ props.IconLeftOffImgonMouseDown } onKeyDown={ props.IconLeftOffImgonKeyDown } onChange={ props.IconLeftOffImgonChange } ondelay={ props.IconLeftOffImgondelay } src={props.IconLeftOffImg0 || "https://cdn.uncodie.com/tACQBrKCV97e0EqPE1XNLJ/cec3b16681c6d1265aafb2abdb4c4c103947cddd.png" } />
                    </CSSTransition>
                    <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['iconlrigthoffimg']?.animationClass || {}}>
                      <img id="id_twoooneeight_onefoursixsix" className={` rectangle iconlrigthoffimg ${ props.onClick ? 'cursor' : '' } ${ transaction['iconlrigthoffimg']?.type ? transaction['iconlrigthoffimg']?.type.toLowerCase() : '' }`} style={{ ...{},  ...props.IconLRigthOffImgStyle , transitionDuration: transaction['iconlrigthoffimg']?.duration, transitionTimingFunction: transaction['iconlrigthoffimg']?.timingFunction }} onClick={ props.IconLRigthOffImgonClick } onMouseEnter={ props.IconLRigthOffImgonMouseEnter } onMouseOver={ props.IconLRigthOffImgonMouseOver } onKeyPress={ props.IconLRigthOffImgonKeyPress } onDrag={ props.IconLRigthOffImgonDrag } onMouseLeave={ props.IconLRigthOffImgonMouseLeave } onMouseUp={ props.IconLRigthOffImgonMouseUp } onMouseDown={ props.IconLRigthOffImgonMouseDown } onKeyDown={ props.IconLRigthOffImgonKeyDown } onChange={ props.IconLRigthOffImgonChange } ondelay={ props.IconLRigthOffImgondelay } src={props.IconLRigthOffImg0 || "https://cdn.uncodie.com/tACQBrKCV97e0EqPE1XNLJ/7cef8d788abd2463bca0186d06a1a90f235d07a5.png" } />
                    </CSSTransition>
                  </div>

                </CSSTransition>
                <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['headerlinksbuttoncont']?.animationClass || {}}>

                  <div id="id_twoooneeight_onefoursixseven" className={` frame headerlinksbuttoncont ${ props.onClick ? 'cursor' : '' } ${ transaction['headerlinksbuttoncont']?.type ? transaction['headerlinksbuttoncont']?.type.toLowerCase() : '' }`} style={ { ...{}, ...props.HeaderLinksButtonContStyle , transitionDuration: transaction['headerlinksbuttoncont']?.duration, transitionTimingFunction: transaction['headerlinksbuttoncont']?.timingFunction } } onClick={ props.HeaderLinksButtonContonClick } onMouseEnter={ props.HeaderLinksButtonContonMouseEnter } onMouseOver={ props.HeaderLinksButtonContonMouseOver } onKeyPress={ props.HeaderLinksButtonContonKeyPress } onDrag={ props.HeaderLinksButtonContonDrag } onMouseLeave={ props.HeaderLinksButtonContonMouseLeave } onMouseUp={ props.HeaderLinksButtonContonMouseUp } onMouseDown={ props.HeaderLinksButtonContonMouseDown } onKeyDown={ props.HeaderLinksButtonContonKeyDown } onChange={ props.HeaderLinksButtonContonChange } ondelay={ props.HeaderLinksButtonContondelay }>
                    <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['headertextlinks']?.animationClass || {}}>

                      <div id="id_twoooneeight_onefoursixeight" className={` frame headertextlinks ${ props.onClick ? 'cursor' : '' } ${ transaction['headertextlinks']?.type ? transaction['headertextlinks']?.type.toLowerCase() : '' }`} style={ { ...{}, ...props.HeaderTextLinksStyle , transitionDuration: transaction['headertextlinks']?.duration, transitionTimingFunction: transaction['headertextlinks']?.timingFunction } } onClick={ props.HeaderTextLinksonClick } onMouseEnter={ props.HeaderTextLinksonMouseEnter } onMouseOver={ props.HeaderTextLinksonMouseOver } onKeyPress={ props.HeaderTextLinksonKeyPress } onDrag={ props.HeaderTextLinksonDrag } onMouseLeave={ props.HeaderTextLinksonMouseLeave } onMouseUp={ props.HeaderTextLinksonMouseUp } onMouseDown={ props.HeaderTextLinksonMouseDown } onKeyDown={ props.HeaderTextLinksonKeyDown } onChange={ props.HeaderTextLinksonChange } ondelay={ props.HeaderTextLinksondelay }>
                        <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['headertextlinkscont']?.animationClass || {}}>

                          <div id="id_twoooneeight_onefoursevenzero" className={` frame headertextlinkscont ${ props.onClick ? 'cursor' : '' } ${ transaction['headertextlinkscont']?.type ? transaction['headertextlinkscont']?.type.toLowerCase() : '' }`} style={ { ...{"borderRight":"1px solid #FFFFFF"}, ...props.HeaderTextLinksContStyle , transitionDuration: transaction['headertextlinkscont']?.duration, transitionTimingFunction: transaction['headertextlinkscont']?.timingFunction } } onClick={ props.HeaderTextLinksContonClick } onMouseEnter={ props.HeaderTextLinksContonMouseEnter } onMouseOver={ props.HeaderTextLinksContonMouseOver } onKeyPress={ props.HeaderTextLinksContonKeyPress } onDrag={ props.HeaderTextLinksContonDrag } onMouseLeave={ props.HeaderTextLinksContonMouseLeave } onMouseUp={ props.HeaderTextLinksContonMouseUp } onMouseDown={ props.HeaderTextLinksContonMouseDown } onKeyDown={ props.HeaderTextLinksContonKeyDown } onChange={ props.HeaderTextLinksContonChange } ondelay={ props.HeaderTextLinksContondelay }>
                            <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['textlink']?.animationClass || {}}>
                              <TextLink { ...{ ...props, style:false } } variant={'Property 1=Default'} TextLinkTxt0={ props.TextLinkTxt0 || "Premium" } cssClass={"C_twoooneeight_onefoursevenone "}  />
    </CSSTransition >
<CSSTransition 
        in={_in}
        appear={true} 
        timeout={100}
        classNames={transaction['textlink']?.animationClass || {}}
    >
    <TextLink { ...{ ...props, style:false } }  variant={'Property 1=Default'} TextLinkTxt0={ props.TextLinkTxt1 || " Ayuda"} cssClass={"C_twoooneeight_onefourseventwoo "}  />
    </CSSTransition >
<CSSTransition 
        in={_in}
        appear={true} 
        timeout={100}
        classNames={transaction['textlink']?.animationClass || {}}
    >
    <TextLink { ...{ ...props, style:false } }  variant={'Property 1=Default'} TextLinkTxt0={ props.TextLinkTxt2 || " Descargar"} cssClass={"C_twoooneeight_onefourseventhree "}  />
    </CSSTransition >
                    </div>
               
    </CSSTransition >
                    </div>
               
    </CSSTransition >
<CSSTransition 
        in={_in}
        appear={true} 
        timeout={100}
        classNames={transaction['headertextlinkbutton']?.animationClass || {}}
    >
    
                    <div id="id_twoooneeight_onefoursevenfour" className={` frame headertextlinkbutton ${ props.onClick ? 'cursor' : '' } ${ transaction['headertextlinkbutton']?.type ? transaction['headertextlinkbutton']?.type.toLowerCase() : '' }`} style={ { ...{}, ...props.HeaderTextLinkButtonStyle , transitionDuration: transaction['headertextlinkbutton']?.duration, transitionTimingFunction: transaction['headertextlinkbutton']?.timingFunction } } onClick={ props.HeaderTextLinkButtononClick } onMouseEnter={ props.HeaderTextLinkButtononMouseEnter } onMouseOver={ props.HeaderTextLinkButtononMouseOver } onKeyPress={ props.HeaderTextLinkButtononKeyPress } onDrag={ props.HeaderTextLinkButtononDrag } onMouseLeave={ props.HeaderTextLinkButtononMouseLeave } onMouseUp={ props.HeaderTextLinkButtononMouseUp } onMouseDown={ props.HeaderTextLinkButtononMouseDown } onKeyDown={ props.HeaderTextLinkButtononKeyDown } onChange={ props.HeaderTextLinkButtononChange } ondelay={ props.HeaderTextLinkButtonondelay }>
                                <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['headertextlinkbuttoncont']?.animationClass || {}}>

                                  <div id="id_twoooneeight_onefoursevenfive" className={` frame headertextlinkbuttoncont ${ props.onClick ? 'cursor' : '' } ${ transaction['headertextlinkbuttoncont']?.type ? transaction['headertextlinkbuttoncont']?.type.toLowerCase() : '' }`} style={ { ...{}, ...props.HeaderTextLinkButtonContStyle , transitionDuration: transaction['headertextlinkbuttoncont']?.duration, transitionTimingFunction: transaction['headertextlinkbuttoncont']?.timingFunction } } onClick={ props.HeaderTextLinkButtonContonClick } onMouseEnter={ props.HeaderTextLinkButtonContonMouseEnter } onMouseOver={ props.HeaderTextLinkButtonContonMouseOver } onKeyPress={ props.HeaderTextLinkButtonContonKeyPress } onDrag={ props.HeaderTextLinkButtonContonDrag } onMouseLeave={ props.HeaderTextLinkButtonContonMouseLeave } onMouseUp={ props.HeaderTextLinkButtonContonMouseUp } onMouseDown={ props.HeaderTextLinkButtonContonMouseDown } onKeyDown={ props.HeaderTextLinkButtonContonKeyDown } onChange={ props.HeaderTextLinkButtonContonChange } ondelay={ props.HeaderTextLinkButtonContondelay }>
                                    <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['textlink']?.animationClass || {}}>
                                      <TextLink { ...{ ...props, style:false } } variant={'Property 1=Default'} TextLinkTxt0={ props.TextLinkTxt3 || "Regístrate" } cssClass={"C_twoooneeight_onefoursevensix cursor "}  />
    </CSSTransition >
<CSSTransition 
        in={_in}
        appear={true} 
        timeout={100}
        classNames={transaction['principalbutton']?.animationClass || {}}
    >
    <PrincipalButton { ...{ ...props, style:false } }  variant={'Property 1=Default'} PrincipalButtonText0={ props.PrincipalButtonText0 || " Iniciar Sesión"} cssClass={"C_twoooneeight_onefoursevenseven "}  />
    </CSSTransition >
                    </div>
               
    </CSSTransition >
                    </div>
               
    </CSSTransition >
                    </div>
               
    </CSSTransition >
                    </div>
               
    </CSSTransition >
                    </div>
               
    </CSSTransition >
            
            </>
        }
        </div>
    
    </CSSTransition >
            </>
        
    ) 
}

Header.propTypes = {
    style: PropTypes.any,
IconLeftOffImg0: PropTypes.any,
IconLRigthOffImg0: PropTypes.any,
TextLinkTxt0: PropTypes.any,
TextLinkTxt1: PropTypes.any,
TextLinkTxt2: PropTypes.any,
TextLinkTxt3: PropTypes.any,
PrincipalButtonText0: PropTypes.any,
HeaderonClick: PropTypes.any,
HeaderonMouseEnter: PropTypes.any,
HeaderonMouseOver: PropTypes.any,
HeaderonKeyPress: PropTypes.any,
HeaderonDrag: PropTypes.any,
HeaderonMouseLeave: PropTypes.any,
HeaderonMouseUp: PropTypes.any,
HeaderonMouseDown: PropTypes.any,
HeaderonKeyDown: PropTypes.any,
HeaderonChange: PropTypes.any,
Headerondelay: PropTypes.any,
HeaderContonClick: PropTypes.any,
HeaderContonMouseEnter: PropTypes.any,
HeaderContonMouseOver: PropTypes.any,
HeaderContonKeyPress: PropTypes.any,
HeaderContonDrag: PropTypes.any,
HeaderContonMouseLeave: PropTypes.any,
HeaderContonMouseUp: PropTypes.any,
HeaderContonMouseDown: PropTypes.any,
HeaderContonKeyDown: PropTypes.any,
HeaderContonChange: PropTypes.any,
HeaderContondelay: PropTypes.any,
HeaderDataonClick: PropTypes.any,
HeaderDataonMouseEnter: PropTypes.any,
HeaderDataonMouseOver: PropTypes.any,
HeaderDataonKeyPress: PropTypes.any,
HeaderDataonDrag: PropTypes.any,
HeaderDataonMouseLeave: PropTypes.any,
HeaderDataonMouseUp: PropTypes.any,
HeaderDataonMouseDown: PropTypes.any,
HeaderDataonKeyDown: PropTypes.any,
HeaderDataonChange: PropTypes.any,
HeaderDataondelay: PropTypes.any,
HeaderChevronContonClick: PropTypes.any,
HeaderChevronContonMouseEnter: PropTypes.any,
HeaderChevronContonMouseOver: PropTypes.any,
HeaderChevronContonKeyPress: PropTypes.any,
HeaderChevronContonDrag: PropTypes.any,
HeaderChevronContonMouseLeave: PropTypes.any,
HeaderChevronContonMouseUp: PropTypes.any,
HeaderChevronContonMouseDown: PropTypes.any,
HeaderChevronContonKeyDown: PropTypes.any,
HeaderChevronContonChange: PropTypes.any,
HeaderChevronContondelay: PropTypes.any,
IconLeftOffImgonClick: PropTypes.any,
IconLeftOffImgonMouseEnter: PropTypes.any,
IconLeftOffImgonMouseOver: PropTypes.any,
IconLeftOffImgonKeyPress: PropTypes.any,
IconLeftOffImgonDrag: PropTypes.any,
IconLeftOffImgonMouseLeave: PropTypes.any,
IconLeftOffImgonMouseUp: PropTypes.any,
IconLeftOffImgonMouseDown: PropTypes.any,
IconLeftOffImgonKeyDown: PropTypes.any,
IconLeftOffImgonChange: PropTypes.any,
IconLeftOffImgondelay: PropTypes.any,
IconLRigthOffImgonClick: PropTypes.any,
IconLRigthOffImgonMouseEnter: PropTypes.any,
IconLRigthOffImgonMouseOver: PropTypes.any,
IconLRigthOffImgonKeyPress: PropTypes.any,
IconLRigthOffImgonDrag: PropTypes.any,
IconLRigthOffImgonMouseLeave: PropTypes.any,
IconLRigthOffImgonMouseUp: PropTypes.any,
IconLRigthOffImgonMouseDown: PropTypes.any,
IconLRigthOffImgonKeyDown: PropTypes.any,
IconLRigthOffImgonChange: PropTypes.any,
IconLRigthOffImgondelay: PropTypes.any,
HeaderLinksButtonContonClick: PropTypes.any,
HeaderLinksButtonContonMouseEnter: PropTypes.any,
HeaderLinksButtonContonMouseOver: PropTypes.any,
HeaderLinksButtonContonKeyPress: PropTypes.any,
HeaderLinksButtonContonDrag: PropTypes.any,
HeaderLinksButtonContonMouseLeave: PropTypes.any,
HeaderLinksButtonContonMouseUp: PropTypes.any,
HeaderLinksButtonContonMouseDown: PropTypes.any,
HeaderLinksButtonContonKeyDown: PropTypes.any,
HeaderLinksButtonContonChange: PropTypes.any,
HeaderLinksButtonContondelay: PropTypes.any,
HeaderTextLinksonClick: PropTypes.any,
HeaderTextLinksonMouseEnter: PropTypes.any,
HeaderTextLinksonMouseOver: PropTypes.any,
HeaderTextLinksonKeyPress: PropTypes.any,
HeaderTextLinksonDrag: PropTypes.any,
HeaderTextLinksonMouseLeave: PropTypes.any,
HeaderTextLinksonMouseUp: PropTypes.any,
HeaderTextLinksonMouseDown: PropTypes.any,
HeaderTextLinksonKeyDown: PropTypes.any,
HeaderTextLinksonChange: PropTypes.any,
HeaderTextLinksondelay: PropTypes.any,
HeaderTextLinksContonClick: PropTypes.any,
HeaderTextLinksContonMouseEnter: PropTypes.any,
HeaderTextLinksContonMouseOver: PropTypes.any,
HeaderTextLinksContonKeyPress: PropTypes.any,
HeaderTextLinksContonDrag: PropTypes.any,
HeaderTextLinksContonMouseLeave: PropTypes.any,
HeaderTextLinksContonMouseUp: PropTypes.any,
HeaderTextLinksContonMouseDown: PropTypes.any,
HeaderTextLinksContonKeyDown: PropTypes.any,
HeaderTextLinksContonChange: PropTypes.any,
HeaderTextLinksContondelay: PropTypes.any,
HeaderTextLinkButtononClick: PropTypes.any,
HeaderTextLinkButtononMouseEnter: PropTypes.any,
HeaderTextLinkButtononMouseOver: PropTypes.any,
HeaderTextLinkButtononKeyPress: PropTypes.any,
HeaderTextLinkButtononDrag: PropTypes.any,
HeaderTextLinkButtononMouseLeave: PropTypes.any,
HeaderTextLinkButtononMouseUp: PropTypes.any,
HeaderTextLinkButtononMouseDown: PropTypes.any,
HeaderTextLinkButtononKeyDown: PropTypes.any,
HeaderTextLinkButtononChange: PropTypes.any,
HeaderTextLinkButtonondelay: PropTypes.any,
HeaderTextLinkButtonContonClick: PropTypes.any,
HeaderTextLinkButtonContonMouseEnter: PropTypes.any,
HeaderTextLinkButtonContonMouseOver: PropTypes.any,
HeaderTextLinkButtonContonKeyPress: PropTypes.any,
HeaderTextLinkButtonContonDrag: PropTypes.any,
HeaderTextLinkButtonContonMouseLeave: PropTypes.any,
HeaderTextLinkButtonContonMouseUp: PropTypes.any,
HeaderTextLinkButtonContonMouseDown: PropTypes.any,
HeaderTextLinkButtonContonKeyDown: PropTypes.any,
HeaderTextLinkButtonContonChange: PropTypes.any,
HeaderTextLinkButtonContondelay: PropTypes.any
}
export default Header;