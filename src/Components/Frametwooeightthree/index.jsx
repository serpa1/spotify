import React, { Suspense } from 'react';
import PropTypes from 'prop-types';
import * as Contexts  from 'Contexts';
import { OverlaySwapContext } from 'Components/OverlaySwapProvider';
import CorebooksAnimation from 'Components/CorebooksAnimation';
import CorebooksAnimationSingle from 'Components/CorebooksAnimationSingle';

import * as UtilsScripts  from 'utils/utils';
import { Route, useHistory, useParams } from "react-router-dom";
import SingletoneNavigation from 'Class/SingletoneNavigation';

import LoadingComponent from "Components/Loading";


import { Transition } from "react-transition-group";
import { CSSTransition } from "react-transition-group";
import { useAppContext, useSessionContext } from 'context/AppContext';
import './Frametwooeightthree.css'





const Frametwooeightthree = (props)=>{
    const [state,dispatch] = useAppContext();
    const [, setNameSelectedComponent] = React.useContext(OverlaySwapContext);
    
    const nodeRef = React.useRef(null);
    const history = useHistory();
    let RouterContext = useParams();
    
    
    
    
    
    
    const [globalStateForm, dispatchForm] = React.useState({});
    
    const [ _in, setIn ] = React.useState(false)
    const [ transaction, setTransaction ] = React.useState({}) 
    const [ animation, setAnimation ] = React.useState('')

    
    
    
    
    
    
    React.useEffect(()=>{
        
        
        
    },[]);
    
    
    return (
        <>
  <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['frametwooeightthree']?.animationClass || {}}>

    <div id="id_threeonefour_onetwooeightseven" ref={nodeRef} className={` ${ props.onClick ? 'cursor' : '' } frametwooeightthree C_threeonefour_onetwooeightseven ${ props.cssClass } ${ transaction['frametwooeightthree']?.type ? transaction['frametwooeightthree']?.type.toLowerCase() : '' }`} style={ { ...{ ...{}, transitionDuration: transaction['frametwooeightthree']?.duration, transitionTimingFunction: transaction['frametwooeightthree']?.timingFunction }, ...props.style }} onClick={ props.FrametwooeightthreeonClick } onMouseEnter={ props.FrametwooeightthreeonMouseEnter } onMouseOver={ props.FrametwooeightthreeonMouseOver } onKeyPress={ props.FrametwooeightthreeonKeyPress } onDrag={ props.FrametwooeightthreeonDrag } onMouseLeave={ props.FrametwooeightthreeonMouseLeave } onMouseUp={ props.FrametwooeightthreeonMouseUp } onMouseDown={ props.FrametwooeightthreeonMouseDown } onKeyDown={ props.FrametwooeightthreeonKeyDown } onChange={ props.FrametwooeightthreeonChange } ondelay={ props.Frametwooeightthreeondelay }>
      {
      props.children ?
      props.children :
      <>
        <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['frametwoosevennigth']?.animationClass || {}}>

          <div id="id_threeonefour_onetwooeightone" className={` frame frametwoosevennigth ${ props.onClick ? 'cursor' : '' } ${ transaction['frametwoosevennigth']?.type ? transaction['frametwoosevennigth']?.type.toLowerCase() : '' }`} style={ { ...{}, ...props.FrametwoosevennigthStyle , transitionDuration: transaction['frametwoosevennigth']?.duration, transitionTimingFunction: transaction['frametwoosevennigth']?.timingFunction } } onClick={ props.FrametwoosevennigthonClick } onMouseEnter={ props.FrametwoosevennigthonMouseEnter } onMouseOver={ props.FrametwoosevennigthonMouseOver } onKeyPress={ props.FrametwoosevennigthonKeyPress } onDrag={ props.FrametwoosevennigthonDrag } onMouseLeave={ props.FrametwoosevennigthonMouseLeave } onMouseUp={ props.FrametwoosevennigthonMouseUp } onMouseDown={ props.FrametwoosevennigthonMouseDown } onKeyDown={ props.FrametwoosevennigthonKeyDown } onChange={ props.FrametwoosevennigthonChange } ondelay={ props.Frametwoosevennigthondelay }>
            <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['image']?.animationClass || {}}>
              <img id="id_threeonefour_onetwooeighttwoo" className={` rectangle image ${ props.onClick ? 'cursor' : '' } ${ transaction['image']?.type ? transaction['image']?.type.toLowerCase() : '' }`} style={{ ...{},  ...props.IMAGEStyle , transitionDuration: transaction['image']?.duration, transitionTimingFunction: transaction['image']?.timingFunction }} onClick={ props.IMAGEonClick } onMouseEnter={ props.IMAGEonMouseEnter } onMouseOver={ props.IMAGEonMouseOver } onKeyPress={ props.IMAGEonKeyPress } onDrag={ props.IMAGEonDrag } onMouseLeave={ props.IMAGEonMouseLeave } onMouseUp={ props.IMAGEonMouseUp } onMouseDown={ props.IMAGEonMouseDown } onKeyDown={ props.IMAGEonKeyDown } onChange={ props.IMAGEonChange } ondelay={ props.IMAGEondelay } src={props.IMAGE0 || "https://cdn.uncodie.com/tACQBrKCV97e0EqPE1XNLJ/0d19209fee347534f28da3bfebec1ea2b9a3f645.png" } />
            </CSSTransition>
            <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['frametwooseveneight']?.animationClass || {}}>

              <div id="id_threeonefour_onetwooeightthree" className={` frame frametwooseveneight ${ props.onClick ? 'cursor' : '' } ${ transaction['frametwooseveneight']?.type ? transaction['frametwooseveneight']?.type.toLowerCase() : '' }`} style={ { ...{"lineHeight":"1.3em"}, ...props.FrametwooseveneightStyle , transitionDuration: transaction['frametwooseveneight']?.duration, transitionTimingFunction: transaction['frametwooseveneight']?.timingFunction } } onClick={ props.FrametwooseveneightonClick } onMouseEnter={ props.FrametwooseveneightonMouseEnter } onMouseOver={ props.FrametwooseveneightonMouseOver } onKeyPress={ props.FrametwooseveneightonKeyPress } onDrag={ props.FrametwooseveneightonDrag } onMouseLeave={ props.FrametwooseveneightonMouseLeave } onMouseUp={ props.FrametwooseveneightonMouseUp } onMouseDown={ props.FrametwooseveneightonMouseDown } onKeyDown={ props.FrametwooseveneightonKeyDown } onChange={ props.FrametwooseveneightonChange } ondelay={ props.Frametwooseveneightondelay }>
                <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['reproducelacancinquequieras']?.animationClass || {}}>

                  <span id="id_threeonefour_onetwooeightfour"  className={` text reproducelacancinquequieras    ${ props.onClick ? 'cursor' : ''}  ${ transaction['reproducelacancinquequieras']?.type ? transaction['reproducelacancinquequieras']?.type.toLowerCase() : '' }`} style={{ ...{},  ...props.ReproducelacancinquequierasStyle , transitionDuration: transaction['reproducelacancinquequieras']?.duration, transitionTimingFunction: transaction['reproducelacancinquequieras']?.timingFunction }} onClick={ props.ReproducelacancinquequierasonClick } onMouseEnter={ props.ReproducelacancinquequierasonMouseEnter } onMouseOver={ props.ReproducelacancinquequierasonMouseOver } onKeyPress={ props.ReproducelacancinquequierasonKeyPress } onDrag={ props.ReproducelacancinquequierasonDrag } onMouseLeave={ props.ReproducelacancinquequierasonMouseLeave } onMouseUp={ props.ReproducelacancinquequierasonMouseUp } onMouseDown={ props.ReproducelacancinquequierasonMouseDown } onKeyDown={ props.ReproducelacancinquequierasonKeyDown } onChange={ props.ReproducelacancinquequierasonChange } ondelay={ props.Reproducelacancinquequierasondelay } >{props.Reproducelacancinquequieras0 || `Reproduce la canción que quieras.`}</span>

                </CSSTransition>
              </div>

            </CSSTransition>
            <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['frametwoosevennigth']?.animationClass || {}}>

              <div id="id_threeonefour_onetwooeightfive" className={` frame frametwoosevennigth ${ props.onClick ? 'cursor' : '' } ${ transaction['frametwoosevennigth']?.type ? transaction['frametwoosevennigth']?.type.toLowerCase() : '' }`} style={ { ...{}, ...props.FrametwoosevennigthStyle , transitionDuration: transaction['frametwoosevennigth']?.duration, transitionTimingFunction: transaction['frametwoosevennigth']?.timingFunction } } onClick={ props.FrametwoosevennigthonClick } onMouseEnter={ props.FrametwoosevennigthonMouseEnter } onMouseOver={ props.FrametwoosevennigthonMouseOver } onKeyPress={ props.FrametwoosevennigthonKeyPress } onDrag={ props.FrametwoosevennigthonDrag } onMouseLeave={ props.FrametwoosevennigthonMouseLeave } onMouseUp={ props.FrametwoosevennigthonMouseUp } onMouseDown={ props.FrametwoosevennigthonMouseDown } onKeyDown={ props.FrametwoosevennigthonKeyDown } onChange={ props.FrametwoosevennigthonChange } ondelay={ props.Frametwoosevennigthondelay }>
                <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['disfrutademsicasininterrupciones']?.animationClass || {}}>

                  <span id="id_threeonefour_onetwooeightsix"  className={` text disfrutademsicasininterrupciones    ${ props.onClick ? 'cursor' : ''}  ${ transaction['disfrutademsicasininterrupciones']?.type ? transaction['disfrutademsicasininterrupciones']?.type.toLowerCase() : '' }`} style={{ ...{},  ...props.DisfrutademsicasininterrupcionesStyle , transitionDuration: transaction['disfrutademsicasininterrupciones']?.duration, transitionTimingFunction: transaction['disfrutademsicasininterrupciones']?.timingFunction }} onClick={ props.DisfrutademsicasininterrupcionesonClick } onMouseEnter={ props.DisfrutademsicasininterrupcionesonMouseEnter } onMouseOver={ props.DisfrutademsicasininterrupcionesonMouseOver } onKeyPress={ props.DisfrutademsicasininterrupcionesonKeyPress } onDrag={ props.DisfrutademsicasininterrupcionesonDrag } onMouseLeave={ props.DisfrutademsicasininterrupcionesonMouseLeave } onMouseUp={ props.DisfrutademsicasininterrupcionesonMouseUp } onMouseDown={ props.DisfrutademsicasininterrupcionesonMouseDown } onKeyDown={ props.DisfrutademsicasininterrupcionesonKeyDown } onChange={ props.DisfrutademsicasininterrupcionesonChange } ondelay={ props.Disfrutademsicasininterrupcionesondelay } >{props.Disfrutademsicasininterrupciones0 || `Disfruta de música sin interrupciones.`}</span>

                </CSSTransition>
              </div>

            </CSSTransition>
          </div>

        </CSSTransition>

      </>
      }
    </div>

  </CSSTransition>
</>
    ) 
}

Frametwooeightthree.propTypes = {
    style: PropTypes.any,
IMAGE0: PropTypes.any,
Reproducelacancinquequieras0: PropTypes.any,
Disfrutademsicasininterrupciones0: PropTypes.any,
FrametwooeightthreeonClick: PropTypes.any,
FrametwooeightthreeonMouseEnter: PropTypes.any,
FrametwooeightthreeonMouseOver: PropTypes.any,
FrametwooeightthreeonKeyPress: PropTypes.any,
FrametwooeightthreeonDrag: PropTypes.any,
FrametwooeightthreeonMouseLeave: PropTypes.any,
FrametwooeightthreeonMouseUp: PropTypes.any,
FrametwooeightthreeonMouseDown: PropTypes.any,
FrametwooeightthreeonKeyDown: PropTypes.any,
FrametwooeightthreeonChange: PropTypes.any,
Frametwooeightthreeondelay: PropTypes.any,
FrametwoosevennigthonClick: PropTypes.any,
FrametwoosevennigthonMouseEnter: PropTypes.any,
FrametwoosevennigthonMouseOver: PropTypes.any,
FrametwoosevennigthonKeyPress: PropTypes.any,
FrametwoosevennigthonDrag: PropTypes.any,
FrametwoosevennigthonMouseLeave: PropTypes.any,
FrametwoosevennigthonMouseUp: PropTypes.any,
FrametwoosevennigthonMouseDown: PropTypes.any,
FrametwoosevennigthonKeyDown: PropTypes.any,
FrametwoosevennigthonChange: PropTypes.any,
Frametwoosevennigthondelay: PropTypes.any,
IMAGEonClick: PropTypes.any,
IMAGEonMouseEnter: PropTypes.any,
IMAGEonMouseOver: PropTypes.any,
IMAGEonKeyPress: PropTypes.any,
IMAGEonDrag: PropTypes.any,
IMAGEonMouseLeave: PropTypes.any,
IMAGEonMouseUp: PropTypes.any,
IMAGEonMouseDown: PropTypes.any,
IMAGEonKeyDown: PropTypes.any,
IMAGEonChange: PropTypes.any,
IMAGEondelay: PropTypes.any,
FrametwooseveneightonClick: PropTypes.any,
FrametwooseveneightonMouseEnter: PropTypes.any,
FrametwooseveneightonMouseOver: PropTypes.any,
FrametwooseveneightonKeyPress: PropTypes.any,
FrametwooseveneightonDrag: PropTypes.any,
FrametwooseveneightonMouseLeave: PropTypes.any,
FrametwooseveneightonMouseUp: PropTypes.any,
FrametwooseveneightonMouseDown: PropTypes.any,
FrametwooseveneightonKeyDown: PropTypes.any,
FrametwooseveneightonChange: PropTypes.any,
Frametwooseveneightondelay: PropTypes.any,
ReproducelacancinquequierasonClick: PropTypes.any,
ReproducelacancinquequierasonMouseEnter: PropTypes.any,
ReproducelacancinquequierasonMouseOver: PropTypes.any,
ReproducelacancinquequierasonKeyPress: PropTypes.any,
ReproducelacancinquequierasonDrag: PropTypes.any,
ReproducelacancinquequierasonMouseLeave: PropTypes.any,
ReproducelacancinquequierasonMouseUp: PropTypes.any,
ReproducelacancinquequierasonMouseDown: PropTypes.any,
ReproducelacancinquequierasonKeyDown: PropTypes.any,
ReproducelacancinquequierasonChange: PropTypes.any,
Reproducelacancinquequierasondelay: PropTypes.any,
DisfrutademsicasininterrupcionesonClick: PropTypes.any,
DisfrutademsicasininterrupcionesonMouseEnter: PropTypes.any,
DisfrutademsicasininterrupcionesonMouseOver: PropTypes.any,
DisfrutademsicasininterrupcionesonKeyPress: PropTypes.any,
DisfrutademsicasininterrupcionesonDrag: PropTypes.any,
DisfrutademsicasininterrupcionesonMouseLeave: PropTypes.any,
DisfrutademsicasininterrupcionesonMouseUp: PropTypes.any,
DisfrutademsicasininterrupcionesonMouseDown: PropTypes.any,
DisfrutademsicasininterrupcionesonKeyDown: PropTypes.any,
DisfrutademsicasininterrupcionesonChange: PropTypes.any,
Disfrutademsicasininterrupcionesondelay: PropTypes.any
}
export default Frametwooeightthree;