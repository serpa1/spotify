import React, { Suspense } from 'react';
import PropTypes from 'prop-types';
import * as Contexts  from 'Contexts';
import { OverlaySwapContext } from 'Components/OverlaySwapProvider';
import CorebooksAnimation from 'Components/CorebooksAnimation';
import CorebooksAnimationSingle from 'Components/CorebooksAnimationSingle';

import * as UtilsScripts  from 'utils/utils';
import { Route, useHistory, useParams } from "react-router-dom";
import SingletoneNavigation from 'Class/SingletoneNavigation';

import LoadingComponent from "Components/Loading";


import { Transition } from "react-transition-group";
import { CSSTransition } from "react-transition-group";
import { useAppContext, useSessionContext } from 'context/AppContext';
import './Displaytwoo.css'





const Displaytwoo = (props)=>{
    const [state,dispatch] = useAppContext();
    const [, setNameSelectedComponent] = React.useContext(OverlaySwapContext);
    
    const nodeRef = React.useRef(null);
    const history = useHistory();
    let RouterContext = useParams();
    
    
    
    
    
    
    const [globalStateForm, dispatchForm] = React.useState({});
    
    const [ _in, setIn ] = React.useState(false)
    const [ transaction, setTransaction ] = React.useState({}) 
    const [ animation, setAnimation ] = React.useState('')

    
    
    
    
    
    
    React.useEffect(()=>{
        
        
        
    },[]);
    
    
    return (
        <>
  <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['displaytwoo']?.animationClass || {}}>

    <div id="id_sixtwoosix_onesixsixsix" ref={nodeRef} className={` ${ props.onClick ? 'cursor' : '' } displaytwoo C_sixtwoosix_onesixsixsix ${ props.cssClass } ${ transaction['displaytwoo']?.type ? transaction['displaytwoo']?.type.toLowerCase() : '' }`} style={ { ...{ ...{}, transitionDuration: transaction['displaytwoo']?.duration, transitionTimingFunction: transaction['displaytwoo']?.timingFunction }, ...props.style }} onClick={ props.DisplaytwooonClick } onMouseEnter={ props.DisplaytwooonMouseEnter } onMouseOver={ props.DisplaytwooonMouseOver } onKeyPress={ props.DisplaytwooonKeyPress } onDrag={ props.DisplaytwooonDrag } onMouseLeave={ props.DisplaytwooonMouseLeave } onMouseUp={ props.DisplaytwooonMouseUp } onMouseDown={ props.DisplaytwooonMouseDown } onKeyDown={ props.DisplaytwooonKeyDown } onChange={ props.DisplaytwooonChange } ondelay={ props.Displaytwooondelay }>
      {
      props.children ?
      props.children :
      <>
        <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['displaytwootext']?.animationClass || {}}>

          <span id="id_sixtwoosix_onesixsixfive"  className={` text displaytwootext    ${ props.onClick ? 'cursor' : ''}  ${ transaction['displaytwootext']?.type ? transaction['displaytwootext']?.type.toLowerCase() : '' }`} style={{ ...{},  ...props.DisplaytwooTextStyle , transitionDuration: transaction['displaytwootext']?.duration, transitionTimingFunction: transaction['displaytwootext']?.timingFunction }} onClick={ props.DisplaytwooTextonClick } onMouseEnter={ props.DisplaytwooTextonMouseEnter } onMouseOver={ props.DisplaytwooTextonMouseOver } onKeyPress={ props.DisplaytwooTextonKeyPress } onDrag={ props.DisplaytwooTextonDrag } onMouseLeave={ props.DisplaytwooTextonMouseLeave } onMouseUp={ props.DisplaytwooTextonMouseUp } onMouseDown={ props.DisplaytwooTextonMouseDown } onKeyDown={ props.DisplaytwooTextonKeyDown } onChange={ props.DisplaytwooTextonChange } ondelay={ props.DisplaytwooTextondelay } >{props.DisplaytwooText0 || `Display 2`}</span>

        </CSSTransition>

      </>
      }
    </div>

  </CSSTransition>
</>
    ) 
}

Displaytwoo.propTypes = {
    style: PropTypes.any,
DisplaytwooText0: PropTypes.any,
DisplaytwooonClick: PropTypes.any,
DisplaytwooonMouseEnter: PropTypes.any,
DisplaytwooonMouseOver: PropTypes.any,
DisplaytwooonKeyPress: PropTypes.any,
DisplaytwooonDrag: PropTypes.any,
DisplaytwooonMouseLeave: PropTypes.any,
DisplaytwooonMouseUp: PropTypes.any,
DisplaytwooonMouseDown: PropTypes.any,
DisplaytwooonKeyDown: PropTypes.any,
DisplaytwooonChange: PropTypes.any,
Displaytwooondelay: PropTypes.any,
DisplaytwooTextonClick: PropTypes.any,
DisplaytwooTextonMouseEnter: PropTypes.any,
DisplaytwooTextonMouseOver: PropTypes.any,
DisplaytwooTextonKeyPress: PropTypes.any,
DisplaytwooTextonDrag: PropTypes.any,
DisplaytwooTextonMouseLeave: PropTypes.any,
DisplaytwooTextonMouseUp: PropTypes.any,
DisplaytwooTextonMouseDown: PropTypes.any,
DisplaytwooTextonKeyDown: PropTypes.any,
DisplaytwooTextonChange: PropTypes.any,
DisplaytwooTextondelay: PropTypes.any
}
export default Displaytwoo;