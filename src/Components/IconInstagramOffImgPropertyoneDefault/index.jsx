import React, { Suspense } from 'react';
import PropTypes from 'prop-types';
import * as Contexts  from 'Contexts';
import { OverlaySwapContext } from 'Components/OverlaySwapProvider';
import CorebooksAnimation from 'Components/CorebooksAnimation';
import CorebooksAnimationSingle from 'Components/CorebooksAnimationSingle';

import * as UtilsScripts  from 'utils/utils';
import { Route, useHistory, useParams } from "react-router-dom";
import SingletoneNavigation from 'Class/SingletoneNavigation';

import LoadingComponent from "Components/Loading";


import { Transition } from "react-transition-group";
import { CSSTransition } from "react-transition-group";
import { useAppContext, useSessionContext } from 'context/AppContext';
import './IconInstagramOffImgPropertyoneDefault.css'





const PropertyoneDefault = (props)=>{
    const [state,dispatch] = useAppContext();
    const [, setNameSelectedComponent] = React.useContext(OverlaySwapContext);
    
    const nodeRef = React.useRef(null);
    const history = useHistory();
    let RouterContext = useParams();
    
    
    
    
    
    
    const [globalStateForm, dispatchForm] = React.useState({});
    
    const [ _in, setIn ] = React.useState(false)
    const [ transaction, setTransaction ] = React.useState({}) 
    const [ animation, setAnimation ] = React.useState('')

    
    
    
    
    
    
    React.useEffect(()=>{
        
        
        
    },[]);
    
    
    return (
        <>
  <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['propertyonedefault']?.animationClass || {}}>

    <div id="id_onesix_oneeightfive" ref={nodeRef} className={` cursor ${ props.onClick ? 'cursor' : '' } propertyonedefault C_onesix_oneeightfive ${ props.cssClass } ${ transaction['propertyonedefault']?.type ? transaction['propertyonedefault']?.type.toLowerCase() : '' }`} style={ { ...{ ...{}, transitionDuration: transaction['propertyonedefault']?.duration, transitionTimingFunction: transaction['propertyonedefault']?.timingFunction }, ...props.style }} onClick={ props.IconInstagramOffImgonClick } onMouseEnter={ props.IconInstagramOffImgonMouseEnter } onMouseOver={ props.IconInstagramOffImgonMouseOver || function(e){ setTransaction({ }); setvariant('Property 1=Variant2'); }} onKeyPress={ props.IconInstagramOffImgonKeyPress } onDrag={ props.IconInstagramOffImgonDrag } onMouseLeave={ props.IconInstagramOffImgonMouseLeave } onMouseUp={ props.IconInstagramOffImgonMouseUp } onMouseDown={ props.IconInstagramOffImgonMouseDown } onKeyDown={ props.IconInstagramOffImgonKeyDown } onChange={ props.IconInstagramOffImgonChange } ondelay={ props.IconInstagramOffImgondelay }>
      {
      props.children ?
      props.children :
      <>
        <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['iconinstagramoffimg']?.animationClass || {}}>
          <img id="id_onesix_oneeighttwoo" className={` rectangle iconinstagramoffimg ${ props.onClick ? 'cursor' : '' } ${ transaction['iconinstagramoffimg']?.type ? transaction['iconinstagramoffimg']?.type.toLowerCase() : '' }`} style={{ ...{},  ...props.IconInstagramOffImgStyle , transitionDuration: transaction['iconinstagramoffimg']?.duration, transitionTimingFunction: transaction['iconinstagramoffimg']?.timingFunction }} onClick={ props.IconInstagramOffImgonClick } onMouseEnter={ props.IconInstagramOffImgonMouseEnter } onMouseOver={ props.IconInstagramOffImgonMouseOver } onKeyPress={ props.IconInstagramOffImgonKeyPress } onDrag={ props.IconInstagramOffImgonDrag } onMouseLeave={ props.IconInstagramOffImgonMouseLeave } onMouseUp={ props.IconInstagramOffImgonMouseUp } onMouseDown={ props.IconInstagramOffImgonMouseDown } onKeyDown={ props.IconInstagramOffImgonKeyDown } onChange={ props.IconInstagramOffImgonChange } ondelay={ props.IconInstagramOffImgondelay } src={props.IconInstagramOffImg0 || "https://cdn.uncodie.com/tACQBrKCV97e0EqPE1XNLJ/aa5e38cc3adc8a07d8ca20ddf83e756f5be57d6f.png" } />
        </CSSTransition>

      </>
      }
    </div>

  </CSSTransition>
</>
    ) 
}

PropertyoneDefault.propTypes = {
    style: PropTypes.any,
IconInstagramOffImg0: PropTypes.any,
IconInstagramOffImgonClick: PropTypes.any,
IconInstagramOffImgonMouseEnter: PropTypes.any,
IconInstagramOffImgonMouseOver: PropTypes.any,
IconInstagramOffImgonKeyPress: PropTypes.any,
IconInstagramOffImgonDrag: PropTypes.any,
IconInstagramOffImgonMouseLeave: PropTypes.any,
IconInstagramOffImgonMouseUp: PropTypes.any,
IconInstagramOffImgonMouseDown: PropTypes.any,
IconInstagramOffImgonKeyDown: PropTypes.any,
IconInstagramOffImgonChange: PropTypes.any,
IconInstagramOffImgondelay: PropTypes.any
}
export default PropertyoneDefault;