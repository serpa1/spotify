import React, { Suspense } from 'react';
import PropTypes from 'prop-types';
import * as Contexts  from 'Contexts';
import { OverlaySwapContext } from 'Components/OverlaySwapProvider';
import CorebooksAnimation from 'Components/CorebooksAnimation';
import CorebooksAnimationSingle from 'Components/CorebooksAnimationSingle';

import * as UtilsScripts  from 'utils/utils';
import { Route, useHistory, useParams } from "react-router-dom";
import SingletoneNavigation from 'Class/SingletoneNavigation';

import LoadingComponent from "Components/Loading";


import { Transition } from "react-transition-group";
import { CSSTransition } from "react-transition-group";
import { useAppContext, useSessionContext } from 'context/AppContext';
import './SwitchPropertyoneDefault.css'





const PropertyoneDefault = (props)=>{
    const [state,dispatch] = useAppContext();
    const [, setNameSelectedComponent] = React.useContext(OverlaySwapContext);
    
    const nodeRef = React.useRef(null);
    const history = useHistory();
    let RouterContext = useParams();
    
    
    
    
    
    
    const [globalStateForm, dispatchForm] = React.useState({});
    
    const [ _in, setIn ] = React.useState(false)
    const [ transaction, setTransaction ] = React.useState({}) 
    const [ animation, setAnimation ] = React.useState('')

    
    
    
    
    
    
    React.useEffect(()=>{
        
        
        
    },[]);
    
    
    return (
        <>
  <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['propertyonedefault']?.animationClass || {}}>

    <div id="id_fourthreefour_twoofiveonenigth" ref={nodeRef} className={` ${ props.onClick ? 'cursor' : '' } propertyonedefault C_fourthreefour_twoofiveonenigth ${ props.cssClass } ${ transaction['propertyonedefault']?.type ? transaction['propertyonedefault']?.type.toLowerCase() : '' }`} style={ { ...{ ...{"minHeight":"40px"}, transitionDuration: transaction['propertyonedefault']?.duration, transitionTimingFunction: transaction['propertyonedefault']?.timingFunction }, ...props.style }} onClick={ props.SwitchonClick } onMouseEnter={ props.SwitchonMouseEnter } onMouseOver={ props.SwitchonMouseOver } onKeyPress={ props.SwitchonKeyPress } onDrag={ props.SwitchonDrag } onMouseLeave={ props.SwitchonMouseLeave } onMouseUp={ props.SwitchonMouseUp } onMouseDown={ props.SwitchonMouseDown } onKeyDown={ props.SwitchonKeyDown } onChange={ props.SwitchonChange } ondelay={ props.Switchondelay }>
      {
      props.children ?
      props.children :
      <>
        <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['landingiconprimarybuttom']?.animationClass || {}}>

          <div id="id_fourthreefour_twoofiveonethree" className={` frame landingiconprimarybuttom ${ props.onClick ? 'cursor' : '' } ${ transaction['landingiconprimarybuttom']?.type ? transaction['landingiconprimarybuttom']?.type.toLowerCase() : '' }`} style={ { ...{}, ...props.LandingIconPrimaryButtomStyle , transitionDuration: transaction['landingiconprimarybuttom']?.duration, transitionTimingFunction: transaction['landingiconprimarybuttom']?.timingFunction } } onClick={ props.LandingIconPrimaryButtomonClick } onMouseEnter={ props.LandingIconPrimaryButtomonMouseEnter } onMouseOver={ props.LandingIconPrimaryButtomonMouseOver } onKeyPress={ props.LandingIconPrimaryButtomonKeyPress } onDrag={ props.LandingIconPrimaryButtomonDrag } onMouseLeave={ props.LandingIconPrimaryButtomonMouseLeave } onMouseUp={ props.LandingIconPrimaryButtomonMouseUp } onMouseDown={ props.LandingIconPrimaryButtomonMouseDown } onKeyDown={ props.LandingIconPrimaryButtomonKeyDown } onChange={ props.LandingIconPrimaryButtomonChange } ondelay={ props.LandingIconPrimaryButtomondelay }>

          </div>

        </CSSTransition>

      </>
      }
    </div>

  </CSSTransition>
</>
    ) 
}

PropertyoneDefault.propTypes = {
    style: PropTypes.any,
SwitchonClick: PropTypes.any,
SwitchonMouseEnter: PropTypes.any,
SwitchonMouseOver: PropTypes.any,
SwitchonKeyPress: PropTypes.any,
SwitchonDrag: PropTypes.any,
SwitchonMouseLeave: PropTypes.any,
SwitchonMouseUp: PropTypes.any,
SwitchonMouseDown: PropTypes.any,
SwitchonKeyDown: PropTypes.any,
SwitchonChange: PropTypes.any,
Switchondelay: PropTypes.any,
LandingIconPrimaryButtomonClick: PropTypes.any,
LandingIconPrimaryButtomonMouseEnter: PropTypes.any,
LandingIconPrimaryButtomonMouseOver: PropTypes.any,
LandingIconPrimaryButtomonKeyPress: PropTypes.any,
LandingIconPrimaryButtomonDrag: PropTypes.any,
LandingIconPrimaryButtomonMouseLeave: PropTypes.any,
LandingIconPrimaryButtomonMouseUp: PropTypes.any,
LandingIconPrimaryButtomonMouseDown: PropTypes.any,
LandingIconPrimaryButtomonKeyDown: PropTypes.any,
LandingIconPrimaryButtomonChange: PropTypes.any,
LandingIconPrimaryButtomondelay: PropTypes.any
}
export default PropertyoneDefault;