import React, { Suspense } from 'react';
import PropTypes from 'prop-types';
import * as Contexts  from 'Contexts';
import { OverlaySwapContext } from 'Components/OverlaySwapProvider';
import CorebooksAnimation from 'Components/CorebooksAnimation';
import CorebooksAnimationSingle from 'Components/CorebooksAnimationSingle';

import * as UtilsScripts  from 'utils/utils';
import { Route, useHistory, useParams } from "react-router-dom";
import SingletoneNavigation from 'Class/SingletoneNavigation';

import LoadingComponent from "Components/Loading";


import { Transition } from "react-transition-group";
import { CSSTransition } from "react-transition-group";
import ButtonGreen from 'Components/ButtonGreen'
import TextLinkGreen from 'Components/TextLinkGreen'
import SelectCreate from 'Components/SelectCreate'
import SelectLoginSex from 'Components/SelectLoginSex'
import InputDate from 'Components/InputDate'
import InputCreate from 'Components/InputCreate'
import InputCreateText from 'Components/InputCreateText'
import ButtonGoogle from 'Components/ButtonGoogle'
import ButtonFacebook from 'Components/ButtonFacebook'
import Displayfive from 'Components/Displayfive'
import { useAppContext, useSessionContext } from 'context/AppContext';
import './CrearCuenta.css'




export const CrearCuentaLocalStateReducer = (state = {}, action) => {

    return {
        ...state,
        [action.payload.key]: action.payload.value,
    };
};
    

const CrearCuenta = (props)=>{
    const [state,dispatch] = useAppContext();
    const [, setNameSelectedComponent] = React.useContext(OverlaySwapContext);
    
    const nodeRef = React.useRef(null);
    const history = useHistory();
    let RouterContext = useParams();
    
    
    
    const DeploymentContainerContext = React.useContext(Contexts.DeploymentContainer)
    
    
    const [globalStateForm, dispatchForm] = React.useState({});
    
    const [ _in, setIn ] = React.useState(false)
    const [ transaction, setTransaction ] = React.useState({}) 
    const [ animation, setAnimation ] = React.useState('')

    const [CrearCuentaLocalState, dispatchCrearCuenta] = React.useReducer(CrearCuentaLocalStateReducer, { ...{"View":"row","search":0}, ...(props.state || {} ) });
    
    
    
    
    
    React.useEffect(()=>{
        

        
        
    },[]);
    
    
    return (
        <Contexts.CrearCuentaLocalContext.Provider value={[CrearCuentaLocalState,dispatchCrearCuenta]}>
  <CSSTransition nodeRef={nodeRef} in={true} appear={true} timeout={400} classNames={SingletoneNavigation.getTransitionInstance()?.CrearCuenta?.cssClass || '' }>

    <div id="id_fourthreefour_onethreetwoonigth" ref={nodeRef} className={` ${ props.onClick ? 'cursor' : '' } crearcuenta ${ props.cssClass } ${ transaction['crearcuenta']?.type ? transaction['crearcuenta']?.type.toLowerCase() : '' }`} style={ { ...{ ...{"width":"100%"}, transitionDuration: `${((SingletoneNavigation.getTransitionInstance()?.CrearCuenta?.duration || 0) * 1000).toFixed(0)}ms` }, ...props.style }} onClick={ props.CrearCuentaonClick } onMouseEnter={ props.CrearCuentaonMouseEnter } onMouseOver={ props.CrearCuentaonMouseOver } onKeyPress={ props.CrearCuentaonKeyPress } onDrag={ props.CrearCuentaonDrag } onMouseLeave={ props.CrearCuentaonMouseLeave } onMouseUp={ props.CrearCuentaonMouseUp } onMouseDown={ props.CrearCuentaonMouseDown } onKeyDown={ props.CrearCuentaonKeyDown } onChange={ props.CrearCuentaonChange } ondelay={ props.CrearCuentaondelay }>
      {
      props.children ?
      props.children :
      <>
        <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['framethreesevenone']?.animationClass || {}}>

          <div id="id_fourthreefour_oneeightsevenseven" className={` frame framethreesevenone ${ props.onClick ? 'cursor' : '' } ${ transaction['framethreesevenone']?.type ? transaction['framethreesevenone']?.type.toLowerCase() : '' }`} style={ { ...{}, ...props.FramethreesevenoneStyle , transitionDuration: transaction['framethreesevenone']?.duration, transitionTimingFunction: transaction['framethreesevenone']?.timingFunction } } onClick={ props.FramethreesevenoneonClick } onMouseEnter={ props.FramethreesevenoneonMouseEnter } onMouseOver={ props.FramethreesevenoneonMouseOver } onKeyPress={ props.FramethreesevenoneonKeyPress } onDrag={ props.FramethreesevenoneonDrag } onMouseLeave={ props.FramethreesevenoneonMouseLeave } onMouseUp={ props.FramethreesevenoneonMouseUp } onMouseDown={ props.FramethreesevenoneonMouseDown } onKeyDown={ props.FramethreesevenoneonKeyDown } onChange={ props.FramethreesevenoneonChange } ondelay={ props.Framethreesevenoneondelay }>
            <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['framethreesixnigth']?.animationClass || {}}>

              <div id="id_fourthreefour_oneeightsevenfive" className={` frame framethreesixnigth ${ props.onClick ? 'cursor' : '' } ${ transaction['framethreesixnigth']?.type ? transaction['framethreesixnigth']?.type.toLowerCase() : '' }`} style={ { ...{}, ...props.FramethreesixnigthStyle , transitionDuration: transaction['framethreesixnigth']?.duration, transitionTimingFunction: transaction['framethreesixnigth']?.timingFunction } } onClick={ props.FramethreesixnigthonClick } onMouseEnter={ props.FramethreesixnigthonMouseEnter } onMouseOver={ props.FramethreesixnigthonMouseOver } onKeyPress={ props.FramethreesixnigthonKeyPress } onDrag={ props.FramethreesixnigthonDrag } onMouseLeave={ props.FramethreesixnigthonMouseLeave } onMouseUp={ props.FramethreesixnigthonMouseUp } onMouseDown={ props.FramethreesixnigthonMouseDown } onKeyDown={ props.FramethreesixnigthonKeyDown } onChange={ props.FramethreesixnigthonChange } ondelay={ props.Framethreesixnigthondelay }>
                <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['framethreesixeight']?.animationClass || {}}>

                  <div id="id_fourthreefour_oneeightsevenfour" className={` frame framethreesixeight ${ props.onClick ? 'cursor' : '' } ${ transaction['framethreesixeight']?.type ? transaction['framethreesixeight']?.type.toLowerCase() : '' }`} style={ { ...{}, ...props.FramethreesixeightStyle , transitionDuration: transaction['framethreesixeight']?.duration, transitionTimingFunction: transaction['framethreesixeight']?.timingFunction } } onClick={ props.FramethreesixeightonClick } onMouseEnter={ props.FramethreesixeightonMouseEnter } onMouseOver={ props.FramethreesixeightonMouseOver } onKeyPress={ props.FramethreesixeightonKeyPress } onDrag={ props.FramethreesixeightonDrag } onMouseLeave={ props.FramethreesixeightonMouseLeave } onMouseUp={ props.FramethreesixeightonMouseUp } onMouseDown={ props.FramethreesixeightonMouseDown } onKeyDown={ props.FramethreesixeightonKeyDown } onChange={ props.FramethreesixeightonChange } ondelay={ props.Framethreesixeightondelay }>
                    <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['frame']?.animationClass || {}}>

                      <div id="id_fourthreefour_onefouronenigth" className={` frame frame ${ props.onClick ? 'cursor' : '' } ${ transaction['frame']?.type ? transaction['frame']?.type.toLowerCase() : '' }`} style={ { ...{}, ...props.FrameStyle , transitionDuration: transaction['frame']?.duration, transitionTimingFunction: transaction['frame']?.timingFunction } } onClick={ props.FrameonClick } onMouseEnter={ props.FrameonMouseEnter } onMouseOver={ props.FrameonMouseOver } onKeyPress={ props.FrameonKeyPress } onDrag={ props.FrameonDrag } onMouseLeave={ props.FrameonMouseLeave } onMouseUp={ props.FrameonMouseUp } onMouseDown={ props.FrameonMouseDown } onKeyDown={ props.FrameonKeyDown } onChange={ props.FrameonChange } ondelay={ props.Frameondelay }>
                        <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['vector']?.animationClass || {}}>
                          <svg id="id_fourthreefour_onefourtwoozero" className="vector  " style={{}} onClick={ props.VectoronClick } onMouseEnter={ props.VectoronMouseEnter } onMouseOver={ props.VectoronMouseOver } onKeyPress={ props.VectoronKeyPress } onDrag={ props.VectoronDrag } onMouseLeave={ props.VectoronMouseLeave } onMouseUp={ props.VectoronMouseUp } onMouseDown={ props.VectoronMouseDown } onKeyDown={ props.VectoronKeyDown } onChange={ props.VectoronChange } ondelay={ props.Vectorondelay } width="129.998046875" height="40">
                            <path d="M31.0267 17.7317C24.7433 13.9033 14.3767 13.55 8.37667 15.4183C8.03004 15.5253 7.65913 15.5248 7.31281 15.4168C6.96649 15.3088 6.66102 15.0984 6.43667 14.8133C6.2077 14.5257 6.06708 14.1778 6.03188 13.8119C5.99668 13.446 6.06839 13.0777 6.23833 12.7517C6.465 12.315 6.855 11.985 7.31833 11.8383C14.205 9.69333 25.6517 10.1083 32.8883 14.5133C33.1474 14.6746 33.3638 14.8961 33.519 15.1589C33.6742 15.4217 33.7638 15.7181 33.78 16.0228C33.7962 16.3276 33.7386 16.6318 33.6121 16.9096C33.4856 17.1873 33.2939 17.4305 33.0533 17.6183C32.8626 17.765 32.6445 17.8719 32.4117 17.9329C32.179 17.9939 31.9365 18.0076 31.6983 17.9733C31.4617 17.94 31.2317 17.8567 31.0267 17.7317ZM30.82 23.4017C30.7174 23.5744 30.5817 23.7252 30.4207 23.8453C30.2597 23.9654 30.0765 24.0525 29.8817 24.1017C29.6873 24.1494 29.4853 24.1575 29.2877 24.1254C29.0901 24.0933 28.901 24.0218 28.7317 23.915C23.4917 20.61 15.5033 19.6533 9.30333 21.585C9.01416 21.6743 8.70469 21.6739 8.41577 21.5837C8.12685 21.4936 7.87206 21.3179 7.685 21.08C7.49517 20.8401 7.37875 20.5504 7.34977 20.2458C7.32079 19.9412 7.38049 19.6348 7.52167 19.3633C7.70913 19.0001 8.03266 18.7257 8.42167 18.6C15.5017 16.395 24.305 17.4633 30.3217 21.2583C30.6667 21.481 30.9117 21.829 31.005 22.2289C31.0982 22.6288 31.0309 23.0493 30.82 23.4017ZM28.4367 28.8467C28.3552 28.9855 28.2469 29.1068 28.1181 29.2035C27.9894 29.3002 27.8427 29.3704 27.6867 29.41C27.531 29.4489 27.3691 29.4558 27.2106 29.4303C27.0522 29.4048 26.9006 29.3474 26.765 29.2617C22.185 26.3917 16.425 25.7433 9.63833 27.335C9.40223 27.3899 9.15502 27.3718 8.92943 27.2831C8.70384 27.1944 8.51049 27.0393 8.375 26.8383C8.28291 26.705 8.21779 26.555 8.18333 26.3967C8.11013 26.0759 8.16515 25.7391 8.33667 25.4583C8.42084 25.3202 8.53145 25.2001 8.66216 25.1049C8.79286 25.0096 8.94109 24.9411 9.09833 24.9033C16.5233 23.1617 22.8933 23.9117 28.0333 27.1333C28.3086 27.3118 28.5041 27.5898 28.579 27.9092C28.6539 28.2285 28.604 28.5645 28.4367 28.8467ZM19.4933 0C8.72833 0 0 8.955 0 20C0 31.0467 8.72667 40 19.495 40C30.2617 40 38.9917 31.0467 38.9917 20C38.9917 8.955 30.2617 0 19.4933 0ZM53.0367 18.4633C49.6717 17.64 49.0717 17.0633 49.0717 15.8467C49.0717 14.7 50.1267 13.9283 51.6917 13.9283C53.2083 13.9283 54.715 14.515 56.2917 15.7217C56.3152 15.7395 56.342 15.7524 56.3707 15.7595C56.3993 15.7667 56.4291 15.768 56.4582 15.7633C56.4873 15.7586 56.5152 15.7481 56.5401 15.7323C56.5651 15.7166 56.5866 15.696 56.6033 15.6717L58.2483 13.295C58.2809 13.2476 58.2947 13.1898 58.287 13.1328C58.2793 13.0758 58.2506 13.0237 58.2067 12.9867C56.3283 11.44 54.2133 10.69 51.74 10.69C48.1067 10.69 45.5683 12.9267 45.5683 16.1283C45.5683 19.5617 47.76 20.775 51.5433 21.715C54.7633 22.4767 55.3067 23.115 55.3067 24.255C55.3067 25.5167 54.2083 26.3033 52.44 26.3033C50.4733 26.3033 48.8717 25.6233 47.08 24.03C47.058 24.0104 47.0323 23.9956 47.0044 23.9863C46.9764 23.977 46.9469 23.9735 46.9176 23.976C46.8883 23.9785 46.8598 23.9869 46.8338 24.0008C46.8079 24.0147 46.785 24.0337 46.7667 24.0567L44.9233 26.3067C44.8862 26.3517 44.8677 26.4094 44.8717 26.4676C44.8758 26.5259 44.902 26.5805 44.945 26.62C47.0317 28.5317 49.595 29.54 52.3633 29.54C56.28 29.54 58.8117 27.345 58.8117 23.9467C58.8117 21.075 57.14 19.4867 53.0367 18.4633ZM70.515 22.3133C70.515 24.7367 69.0583 26.43 66.9733 26.43C64.915 26.43 63.3583 24.6617 63.3583 22.3133C63.3583 19.9633 64.9133 18.1967 66.975 18.1967C69.025 18.1967 70.515 19.9267 70.515 22.3133ZM67.6717 15.0567C65.975 15.0567 64.5817 15.7433 63.4333 17.15L63.4333 15.5667C63.4336 15.5077 63.4109 15.4509 63.37 15.4083C63.3498 15.3876 63.3257 15.371 63.2991 15.3595C63.2726 15.3481 63.244 15.342 63.215 15.3417L60.1983 15.3417C60.1694 15.3419 60.1408 15.348 60.1141 15.3594C60.0875 15.3709 60.0635 15.3875 60.0433 15.4083C60.0014 15.451 59.978 15.5085 59.9783 15.5683L59.9783 33.145C59.9783 33.2717 60.0783 33.3733 60.2 33.3733L63.215 33.3733C63.2441 33.3727 63.2727 33.3664 63.2993 33.3547C63.3259 33.343 63.35 33.3261 63.37 33.305C63.4113 33.2621 63.4341 33.2046 63.4333 33.145L63.4333 27.5967C64.5833 28.92 65.975 29.5633 67.6717 29.5633C70.8267 29.5633 74.0183 27.0733 74.0183 22.3133C74.0183 17.55 70.8267 15.0567 67.6717 15.0567ZM82.205 26.455C80.0433 26.455 78.415 24.675 78.415 22.3117C78.415 19.9417 79.9867 18.22 82.155 18.22C84.33 18.22 85.9683 20.0017 85.9683 22.365C85.9683 24.735 84.3867 26.455 82.205 26.455ZM82.205 15.0583C78.1417 15.0583 74.96 18.2683 74.96 22.3633C74.96 26.4167 78.12 29.5933 82.155 29.5933C86.23 29.5933 89.4217 26.3933 89.4217 22.3117C89.4217 18.245 86.2533 15.0583 82.205 15.0583ZM98.0967 15.3417L94.78 15.3417L94.78 11.8617C94.7807 11.8175 94.7684 11.7741 94.7447 11.7367C94.721 11.6994 94.6869 11.6699 94.6467 11.6517C94.6192 11.6402 94.5897 11.6346 94.56 11.635L91.545 11.635C91.4866 11.6359 91.4309 11.6599 91.39 11.7017C91.3491 11.7442 91.3264 11.801 91.3267 11.86L91.3267 15.3383L89.8767 15.3383C89.8324 15.3386 89.7893 15.3522 89.753 15.3775C89.7166 15.4028 89.6889 15.4386 89.6733 15.48C89.6623 15.5076 89.6566 15.537 89.6567 15.5667L89.6567 18.225C89.6567 18.3483 89.7567 18.45 89.8767 18.45L91.3267 18.45L91.3267 25.3267C91.3267 28.105 92.675 29.515 95.3333 29.515C96.4133 29.515 97.31 29.285 98.1567 28.7917C98.1907 28.7718 98.219 28.7433 98.2386 28.7091C98.2582 28.6749 98.2684 28.6361 98.2683 28.5967L98.2683 26.0667C98.2682 26.0285 98.2586 25.9909 98.2403 25.9575C98.2219 25.924 98.1955 25.8956 98.1633 25.875C98.1315 25.8547 98.0949 25.8432 98.0572 25.8414C98.0195 25.8397 97.9819 25.8478 97.9483 25.865C97.3667 26.165 96.8067 26.3033 96.18 26.3033C95.2133 26.3033 94.78 25.8517 94.78 24.8433L94.78 18.45L98.0967 18.45C98.1259 18.45 98.1549 18.4442 98.1818 18.4327C98.2087 18.4212 98.2331 18.4044 98.2533 18.3833C98.2953 18.3407 98.3186 18.2832 98.3183 18.2233L98.3183 15.565C98.3185 15.5052 98.2951 15.4478 98.2533 15.405C98.233 15.384 98.2086 15.3673 98.1817 15.3559C98.1548 15.3444 98.1259 15.3385 98.0967 15.3383L98.0967 15.3417ZM109.653 15.3533L109.653 14.9283C109.653 13.67 110.122 13.1083 111.177 13.1083C111.803 13.1083 112.31 13.2367 112.875 13.4317C112.908 13.4423 112.943 13.4448 112.977 13.439C113.012 13.4332 113.044 13.4192 113.072 13.3983C113.101 13.3774 113.124 13.3498 113.141 13.318C113.157 13.2861 113.165 13.2508 113.165 13.215L113.165 10.6083C113.165 10.5599 113.15 10.5126 113.122 10.4733C113.094 10.434 113.054 10.4047 113.008 10.39C112.198 10.1372 111.354 10.0135 110.505 10.0233C107.722 10.0233 106.248 11.6317 106.248 14.67L106.248 15.325L104.798 15.325C104.769 15.3254 104.741 15.3314 104.714 15.3429C104.688 15.3543 104.664 15.3709 104.643 15.3917C104.602 15.4346 104.579 15.4921 104.58 15.5517L104.58 18.2233C104.58 18.3483 104.678 18.45 104.8 18.45L106.25 18.45L106.25 29.0567C106.25 29.1817 106.347 29.2817 106.468 29.2817L109.483 29.2817C109.603 29.2817 109.702 29.1817 109.702 29.0567L109.702 18.45L112.518 18.45L116.828 29.0533C116.338 30.1683 115.857 30.39 115.2 30.39C114.668 30.39 114.11 30.2267 113.537 29.9067C113.51 29.8921 113.481 29.8831 113.451 29.8802C113.421 29.8774 113.39 29.8807 113.362 29.89C113.333 29.9002 113.307 29.9161 113.284 29.9367C113.262 29.9573 113.244 29.9823 113.232 30.01L112.212 32.31C112.188 32.3615 112.185 32.4199 112.202 32.4738C112.219 32.5277 112.256 32.5733 112.305 32.6017C113.281 33.1712 114.395 33.4639 115.525 33.4483C117.748 33.4483 118.98 32.385 120.065 29.5233L125.293 15.6617C125.306 15.6273 125.311 15.5904 125.307 15.5539C125.303 15.5174 125.29 15.4824 125.27 15.4517C125.251 15.4215 125.224 15.3966 125.193 15.3791C125.161 15.3617 125.126 15.3522 125.09 15.3517L121.952 15.3517C121.905 15.3516 121.859 15.3664 121.822 15.394C121.784 15.4216 121.756 15.4604 121.742 15.505L118.528 24.9233L115.01 15.5C114.995 15.4574 114.967 15.4204 114.93 15.3942C114.893 15.368 114.849 15.3537 114.803 15.3533L109.653 15.3533ZM102.953 15.34L99.9383 15.34C99.9093 15.3406 99.8806 15.3469 99.854 15.3586C99.8274 15.3704 99.8034 15.3873 99.7833 15.4083C99.742 15.4513 99.7193 15.5087 99.72 15.5683L99.72 29.0583C99.72 29.1833 99.82 29.2833 99.94 29.2833L102.955 29.2833C103.075 29.2833 103.173 29.1833 103.173 29.0583L103.173 15.5667C103.174 15.5071 103.151 15.4496 103.11 15.4067C103.09 15.3858 103.065 15.3691 103.038 15.3577C103.011 15.3462 102.983 15.3402 102.953 15.34ZM101.46 9.19833C101.174 9.20059 100.891 9.25966 100.628 9.37211C100.365 9.48455 100.128 9.64813 99.9283 9.85333C99.5233 10.27 99.2967 10.83 99.3 11.415C99.2977 11.9981 99.5231 12.559 99.9283 12.9783C100.128 13.1831 100.367 13.3463 100.63 13.4587C100.893 13.5711 101.176 13.6305 101.462 13.6333C102.038 13.63 102.59 13.395 102.995 12.9783C103.4 12.5617 103.627 12 103.623 11.415C103.623 10.19 102.655 9.19833 101.463 9.19833L101.46 9.19833ZM127.99 16.6817L127.438 16.6817L127.438 17.405L127.99 17.405C128.265 17.405 128.43 17.2667 128.43 17.045C128.43 16.8083 128.265 16.6833 127.99 16.6833L127.99 16.6817ZM128.348 17.7133L128.948 18.575L128.442 18.575L127.903 17.785L127.438 17.785L127.438 18.575L127.015 18.575L127.015 16.2917L128.007 16.2917C128.523 16.2917 128.865 16.5633 128.865 17.0183C128.872 17.1759 128.824 17.331 128.73 17.4576C128.636 17.5841 128.501 17.6744 128.348 17.7133ZM127.878 15.53C126.792 15.53 125.968 16.4167 125.968 17.5017C125.968 18.585 126.785 19.46 127.867 19.46C128.953 19.46 129.778 18.5733 129.778 17.4883C129.778 16.405 128.958 15.5317 127.878 15.5317L127.878 15.53ZM127.867 19.6767C127.585 19.6764 127.307 19.6196 127.048 19.5096C126.79 19.3997 126.555 19.2389 126.36 19.0367C125.961 18.627 125.74 18.0755 125.747 17.5033C125.747 16.3083 126.685 15.3133 127.878 15.3133C128.159 15.3138 128.437 15.3707 128.696 15.4807C128.954 15.5906 129.188 15.7513 129.383 15.9533C129.582 16.1562 129.738 16.3962 129.844 16.6596C129.949 16.923 130.002 17.2046 129.998 17.4883C129.998 18.6833 129.06 19.6783 127.867 19.6783L127.867 19.6767Z" />
                          </svg>
                        </CSSTransition>
                      </div>

                    </CSSTransition>
                    <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['displayfive']?.animationClass || {}}>
                      <Displayfive { ...{ ...props, style:false } } DisplayfiveText0={ props.DisplayfiveText0 || "Regístrate gratis para escuchar" } DisplayfiveText0={ props.DisplayfiveText0 || "Regístrate gratis para escuchar" } cssClass={"C_sixtwoosix_onesevensixfour "}  />
    </CSSTransition >
                    </div>
               
    </CSSTransition >
<CSSTransition 
        in={_in}
        appear={true} 
        timeout={100}
        classNames={transaction['framethreefivezero']?.animationClass || {}}
    >
    
                    <div id="id_fourthreefour_onefoursixsix" className={` frame framethreefivezero ${ props.onClick ? 'cursor' : '' } ${ transaction['framethreefivezero']?.type ? transaction['framethreefivezero']?.type.toLowerCase() : '' }`} style={ { ...{}, ...props.FramethreefivezeroStyle , transitionDuration: transaction['framethreefivezero']?.duration, transitionTimingFunction: transaction['framethreefivezero']?.timingFunction } } onClick={ props.FramethreefivezeroonClick } onMouseEnter={ props.FramethreefivezeroonMouseEnter } onMouseOver={ props.FramethreefivezeroonMouseOver } onKeyPress={ props.FramethreefivezeroonKeyPress } onDrag={ props.FramethreefivezeroonDrag } onMouseLeave={ props.FramethreefivezeroonMouseLeave } onMouseUp={ props.FramethreefivezeroonMouseUp } onMouseDown={ props.FramethreefivezeroonMouseDown } onKeyDown={ props.FramethreefivezeroonKeyDown } onChange={ props.FramethreefivezeroonChange } ondelay={ props.Framethreefivezeroondelay }>
                        <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['framethreefoureight']?.animationClass || {}}>

                          <div id="id_fourthreefour_onefourfiveeight" className={` frame framethreefoureight ${ props.onClick ? 'cursor' : '' } ${ transaction['framethreefoureight']?.type ? transaction['framethreefoureight']?.type.toLowerCase() : '' }`} style={ { ...{}, ...props.FramethreefoureightStyle , transitionDuration: transaction['framethreefoureight']?.duration, transitionTimingFunction: transaction['framethreefoureight']?.timingFunction } } onClick={ props.FramethreefoureightonClick } onMouseEnter={ props.FramethreefoureightonMouseEnter } onMouseOver={ props.FramethreefoureightonMouseOver } onKeyPress={ props.FramethreefoureightonKeyPress } onDrag={ props.FramethreefoureightonDrag } onMouseLeave={ props.FramethreefoureightonMouseLeave } onMouseUp={ props.FramethreefoureightonMouseUp } onMouseDown={ props.FramethreefoureightonMouseDown } onKeyDown={ props.FramethreefoureightonKeyDown } onChange={ props.FramethreefoureightonChange } ondelay={ props.Framethreefoureightondelay }>
                            <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['buttonfacebook']?.animationClass || {}}>
                              <ButtonFacebook { ...{ ...props, style:false } } variant={'Property 1=Default'} cssClass={"C_fourthreeeight_onetwoofivefour "}  />
    </CSSTransition >
<CSSTransition 
        in={_in}
        appear={true} 
        timeout={100}
        classNames={transaction['buttongoogle']?.animationClass || {}}
    >
    <ButtonGoogle { ...{ ...props, style:false } }  variant={'Property 1=Default'}  cssClass={" C_fourthreeeight_onetwoofiveeight "}  />
    </CSSTransition >
                    </div>
               
    </CSSTransition >
<CSSTransition 
        in={_in}
        appear={true} 
        timeout={100}
        classNames={transaction['framethreefournigth']?.animationClass || {}}
    >
    
                    <div id="id_fourthreefour_onefoursixfive" className={` frame framethreefournigth ${ props.onClick ? 'cursor' : '' } ${ transaction['framethreefournigth']?.type ? transaction['framethreefournigth']?.type.toLowerCase() : '' }`} style={ { ...{}, ...props.FramethreefournigthStyle , transitionDuration: transaction['framethreefournigth']?.duration, transitionTimingFunction: transaction['framethreefournigth']?.timingFunction } } onClick={ props.FramethreefournigthonClick } onMouseEnter={ props.FramethreefournigthonMouseEnter } onMouseOver={ props.FramethreefournigthonMouseOver } onKeyPress={ props.FramethreefournigthonKeyPress } onDrag={ props.FramethreefournigthonDrag } onMouseLeave={ props.FramethreefournigthonMouseLeave } onMouseUp={ props.FramethreefournigthonMouseUp } onMouseDown={ props.FramethreefournigthonMouseDown } onKeyDown={ props.FramethreefournigthonKeyDown } onChange={ props.FramethreefournigthonChange } ondelay={ props.Framethreefournigthondelay }>
                                <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['lineleft']?.animationClass || {}}>

                                  <img id="id_fourthreefour_onefourfivenigth" className={` line lineleft ${ props.onClick ? 'cursor' : '' } ${ transaction['lineleft']?.type ? transaction['lineleft']?.type.toLowerCase() : '' }`} style={{ ...{},  ...props.LineLeftStyle , transitionDuration: transaction['lineleft']?.duration, transitionTimingFunction: transaction['lineleft']?.timingFunction }} onClick={ props.LineLeftonClick } onMouseEnter={ props.LineLeftonMouseEnter } onMouseOver={ props.LineLeftonMouseOver } onKeyPress={ props.LineLeftonKeyPress } onDrag={ props.LineLeftonDrag } onMouseLeave={ props.LineLeftonMouseLeave } onMouseUp={ props.LineLeftonMouseUp } onMouseDown={ props.LineLeftonMouseDown } onKeyDown={ props.LineLeftonKeyDown } onChange={ props.LineLeftonChange } ondelay={ props.LineLeftondelay } src={props.LineLeft0 || "" } />

                                </CSSTransition>
                                <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['hone']?.animationClass || {}}>

                                  <div id="id_fourthreefour_onefoursixthree" className={` instance hone ${ props.onClick ? 'cursor' : '' } ${ transaction['hone']?.type ? transaction['hone']?.type.toLowerCase() : '' }`} style={{ ...{},  ...props.HoneStyle , transitionDuration: transaction['hone']?.duration, transitionTimingFunction: transaction['hone']?.timingFunction }} onClick={ props.HoneonClick } onMouseEnter={ props.HoneonMouseEnter } onMouseOver={ props.HoneonMouseOver } onKeyPress={ props.HoneonKeyPress } onDrag={ props.HoneonDrag } onMouseLeave={ props.HoneonMouseLeave } onMouseUp={ props.HoneonMouseUp } onMouseDown={ props.HoneonMouseDown } onKeyDown={ props.HoneonKeyDown } onChange={ props.HoneonChange } ondelay={ props.Honeondelay }>
                                    <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['honetext']?.animationClass || {}}>

                                      <span id="id_fourthreefour_onefoursixthree_one_eightnigthseven"  className={` text honetext    ${ props.onClick ? 'cursor' : ''}  ${ transaction['honetext']?.type ? transaction['honetext']?.type.toLowerCase() : '' }`} style={{ ...{},  ...props.HoneTextStyle , transitionDuration: transaction['honetext']?.duration, transitionTimingFunction: transaction['honetext']?.timingFunction }} onClick={ props.HoneTextonClick } onMouseEnter={ props.HoneTextonMouseEnter } onMouseOver={ props.HoneTextonMouseOver } onKeyPress={ props.HoneTextonKeyPress } onDrag={ props.HoneTextonDrag } onMouseLeave={ props.HoneTextonMouseLeave } onMouseUp={ props.HoneTextonMouseUp } onMouseDown={ props.HoneTextonMouseDown } onKeyDown={ props.HoneTextonKeyDown } onChange={ props.HoneTextonChange } ondelay={ props.HoneTextondelay } >{props.HoneText0 || `O`}</span>

                                    </CSSTransition>
                                  </div>

                                </CSSTransition>
                                <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['linerigth']?.animationClass || {}}>

                                  <img id="id_fourthreefour_onefoursixzero" className={` line linerigth ${ props.onClick ? 'cursor' : '' } ${ transaction['linerigth']?.type ? transaction['linerigth']?.type.toLowerCase() : '' }`} style={{ ...{},  ...props.LineRigthStyle , transitionDuration: transaction['linerigth']?.duration, transitionTimingFunction: transaction['linerigth']?.timingFunction }} onClick={ props.LineRigthonClick } onMouseEnter={ props.LineRigthonMouseEnter } onMouseOver={ props.LineRigthonMouseOver } onKeyPress={ props.LineRigthonKeyPress } onDrag={ props.LineRigthonDrag } onMouseLeave={ props.LineRigthonMouseLeave } onMouseUp={ props.LineRigthonMouseUp } onMouseDown={ props.LineRigthonMouseDown } onKeyDown={ props.LineRigthonKeyDown } onChange={ props.LineRigthonChange } ondelay={ props.LineRigthondelay } src={props.LineRigth0 || "" } />

                                </CSSTransition>
                          </div>

                        </CSSTransition>
                  </div>

                </CSSTransition>
              </div>

            </CSSTransition>
            <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['framethreesevenzero']?.animationClass || {}}>

              <div id="id_fourthreefour_oneeightsevensix" className={` frame framethreesevenzero ${ props.onClick ? 'cursor' : '' } ${ transaction['framethreesevenzero']?.type ? transaction['framethreesevenzero']?.type.toLowerCase() : '' }`} style={ { ...{}, ...props.FramethreesevenzeroStyle , transitionDuration: transaction['framethreesevenzero']?.duration, transitionTimingFunction: transaction['framethreesevenzero']?.timingFunction } } onClick={ props.FramethreesevenzeroonClick } onMouseEnter={ props.FramethreesevenzeroonMouseEnter } onMouseOver={ props.FramethreesevenzeroonMouseOver } onKeyPress={ props.FramethreesevenzeroonKeyPress } onDrag={ props.FramethreesevenzeroonDrag } onMouseLeave={ props.FramethreesevenzeroonMouseLeave } onMouseUp={ props.FramethreesevenzeroonMouseUp } onMouseDown={ props.FramethreesevenzeroonMouseDown } onKeyDown={ props.FramethreesevenzeroonKeyDown } onChange={ props.FramethreesevenzeroonChange } ondelay={ props.Framethreesevenzeroondelay }>
                <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['framethreefivefive']?.animationClass || {}}>
                  <InputCreateText { ...{ ...props, style:false } } cssClass={"C_fourthreefour_onesevenfourone "}  />
    </CSSTransition >
<CSSTransition 
        in={_in}
        appear={true} 
        timeout={100}
        classNames={transaction['inputcreate']?.animationClass || {}}
    >
    <InputCreate { ...{ ...props, style:false } }   DeployTextInput0={ props.DeployTextInput0 || " Crea una contraseña"} DeployGITLabelText0={ props.DeployGITLabelText0 || "Crea una contraseña" } cssClass={"C_fourthreefour_onesixnigthone "}  />
    </CSSTransition >
<CSSTransition 
        in={_in}
        appear={true} 
        timeout={100}
        classNames={transaction['framethreefivesix']?.animationClass || {}}
    >
    <InputCreateText { ...{ ...props, style:false } }   DeployTextInput0={ props.DeployTextInput1 || " Introduce un nombre de perfil"} HfiveText0={ props.HfiveText1 || "Esto aparece en tu perfil" } DeployGITLabelText0={ props.DeployGITLabelText1 || "¿Cómo quieres que te llamemos?" } HfiveText0={ props.HfiveText1 || "Esto aparece en tu perfil" } HfiveTextStyle={{"fontSize":"0.875rem"}} cssClass={"C_fourthreefour_onesevenfivethree "}  />
    </CSSTransition >
<CSSTransition 
        in={_in}
        appear={true} 
        timeout={100}
        classNames={transaction['datadatecont']?.animationClass || {}}
    >
    
                    <div id="id_fourthreefour_oneseventhreenigth" className={` frame datadatecont ${ props.onClick ? 'cursor' : '' } ${ transaction['datadatecont']?.type ? transaction['datadatecont']?.type.toLowerCase() : '' }`} style={ { ...{}, ...props.DataDateContStyle , transitionDuration: transaction['datadatecont']?.duration, transitionTimingFunction: transaction['datadatecont']?.timingFunction } } onClick={ props.DataDateContonClick } onMouseEnter={ props.DataDateContonMouseEnter } onMouseOver={ props.DataDateContonMouseOver } onKeyPress={ props.DataDateContonKeyPress } onDrag={ props.DataDateContonDrag } onMouseLeave={ props.DataDateContonMouseLeave } onMouseUp={ props.DataDateContonMouseUp } onMouseDown={ props.DataDateContonMouseDown } onKeyDown={ props.DataDateContonKeyDown } onChange={ props.DataDateContonChange } ondelay={ props.DataDateContondelay }>
                    <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['deploygitlabel']?.animationClass || {}}>

                      <div id="id_fourthreefour_onesixsixseven" className={` frame deploygitlabel ${ props.onClick ? 'cursor' : '' } ${ transaction['deploygitlabel']?.type ? transaction['deploygitlabel']?.type.toLowerCase() : '' }`} style={ { ...{}, ...props.DeployGITLabelStyle , transitionDuration: transaction['deploygitlabel']?.duration, transitionTimingFunction: transaction['deploygitlabel']?.timingFunction } } onClick={ props.DeployGITLabelonClick } onMouseEnter={ props.DeployGITLabelonMouseEnter } onMouseOver={ props.DeployGITLabelonMouseOver } onKeyPress={ props.DeployGITLabelonKeyPress } onDrag={ props.DeployGITLabelonDrag } onMouseLeave={ props.DeployGITLabelonMouseLeave } onMouseUp={ props.DeployGITLabelonMouseUp } onMouseDown={ props.DeployGITLabelonMouseDown } onKeyDown={ props.DeployGITLabelonKeyDown } onChange={ props.DeployGITLabelonChange } ondelay={ props.DeployGITLabelondelay }>
                        <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['deploygitlabeltext']?.animationClass || {}}>

                          <span id="id_fourthreefour_onesixsixeight"  className={` text deploygitlabeltext    ${ props.onClick ? 'cursor' : ''}  ${ transaction['deploygitlabeltext']?.type ? transaction['deploygitlabeltext']?.type.toLowerCase() : '' }`} style={{ ...{},  ...props.DeployGITLabelTextStyle , transitionDuration: transaction['deploygitlabeltext']?.duration, transitionTimingFunction: transaction['deploygitlabeltext']?.timingFunction }} onClick={ props.DeployGITLabelTextonClick } onMouseEnter={ props.DeployGITLabelTextonMouseEnter } onMouseOver={ props.DeployGITLabelTextonMouseOver } onKeyPress={ props.DeployGITLabelTextonKeyPress } onDrag={ props.DeployGITLabelTextonDrag } onMouseLeave={ props.DeployGITLabelTextonMouseLeave } onMouseUp={ props.DeployGITLabelTextonMouseUp } onMouseDown={ props.DeployGITLabelTextonMouseDown } onKeyDown={ props.DeployGITLabelTextonKeyDown } onChange={ props.DeployGITLabelTextonChange } ondelay={ props.DeployGITLabelTextondelay } >{props.DeployGITLabelText0 || `¿Cuál es tu fecha de nacimiento?`}</span>

                        </CSSTransition>
                      </div>

                    </CSSTransition>
                    <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['framethreefivethree']?.animationClass || {}}>

                      <div id="id_fourthreefour_oneseventhreeeight" className={` frame framethreefivethree ${ props.onClick ? 'cursor' : '' } ${ transaction['framethreefivethree']?.type ? transaction['framethreefivethree']?.type.toLowerCase() : '' }`} style={ { ...{}, ...props.FramethreefivethreeStyle , transitionDuration: transaction['framethreefivethree']?.duration, transitionTimingFunction: transaction['framethreefivethree']?.timingFunction } } onClick={ props.FramethreefivethreeonClick } onMouseEnter={ props.FramethreefivethreeonMouseEnter } onMouseOver={ props.FramethreefivethreeonMouseOver } onKeyPress={ props.FramethreefivethreeonKeyPress } onDrag={ props.FramethreefivethreeonDrag } onMouseLeave={ props.FramethreefivethreeonMouseLeave } onMouseUp={ props.FramethreefivethreeonMouseUp } onMouseDown={ props.FramethreefivethreeonMouseDown } onKeyDown={ props.FramethreefivethreeonKeyDown } onChange={ props.FramethreefivethreeonChange } ondelay={ props.Framethreefivethreeondelay }>
                        <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['inputdate']?.animationClass || {}}>
                          <InputDate { ...{ ...props, style:false } } cssClass={"C_fourthreefour_oneseventwootwoo "}  />
    </CSSTransition >
<CSSTransition 
        in={_in}
        appear={true} 
        timeout={100}
        classNames={transaction['inputdate']?.animationClass || {}}
    >
    <InputDate { ...{ ...props, style:false } }   HfiveText0={ props.HfiveText1 || " Mes"} DeployTextInput0={ props.DeployTextInput1 || "Mes" } HfiveTextStyle={{"fontSize":"0.875rem"}} cssClass={"C_fourthreefour_onesevenonefour "}  />
    </CSSTransition >
<CSSTransition 
        in={_in}
        appear={true} 
        timeout={100}
        classNames={transaction['inputdate']?.animationClass || {}}
    >
    <InputDate { ...{ ...props, style:false } }   HfiveText0={ props.HfiveText2 || " Año"} DeployTextInput0={ props.DeployTextInput2 || "AAAA" } HfiveTextStyle={{"fontSize":"0.875rem"}} cssClass={"C_fourthreefour_oneseventhreezero "}  />
    </CSSTransition >
                    </div>
               
    </CSSTransition >
                    </div>
               
    </CSSTransition >
<CSSTransition 
        in={_in}
        appear={true} 
        timeout={100}
        classNames={transaction['selectscreatecont']?.animationClass || {}}
    >
    
                    <div id="id_fourthreefour_oneeightzeroseven" className={` frame selectscreatecont ${ props.onClick ? 'cursor' : '' } ${ transaction['selectscreatecont']?.type ? transaction['selectscreatecont']?.type.toLowerCase() : '' }`} style={ { ...{}, ...props.SelectsCreateContStyle , transitionDuration: transaction['selectscreatecont']?.duration, transitionTimingFunction: transaction['selectscreatecont']?.timingFunction } } onClick={ props.SelectsCreateContonClick } onMouseEnter={ props.SelectsCreateContonMouseEnter } onMouseOver={ props.SelectsCreateContonMouseOver } onKeyPress={ props.SelectsCreateContonKeyPress } onDrag={ props.SelectsCreateContonDrag } onMouseLeave={ props.SelectsCreateContonMouseLeave } onMouseUp={ props.SelectsCreateContonMouseUp } onMouseDown={ props.SelectsCreateContonMouseDown } onKeyDown={ props.SelectsCreateContonKeyDown } onChange={ props.SelectsCreateContonChange } ondelay={ props.SelectsCreateContondelay }>
                            <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['deploygitlabel']?.animationClass || {}}>

                              <div id="id_fourthreefour_onesevensixfive" className={` frame deploygitlabel ${ props.onClick ? 'cursor' : '' } ${ transaction['deploygitlabel']?.type ? transaction['deploygitlabel']?.type.toLowerCase() : '' }`} style={ { ...{}, ...props.DeployGITLabelStyle , transitionDuration: transaction['deploygitlabel']?.duration, transitionTimingFunction: transaction['deploygitlabel']?.timingFunction } } onClick={ props.DeployGITLabelonClick } onMouseEnter={ props.DeployGITLabelonMouseEnter } onMouseOver={ props.DeployGITLabelonMouseOver } onKeyPress={ props.DeployGITLabelonKeyPress } onDrag={ props.DeployGITLabelonDrag } onMouseLeave={ props.DeployGITLabelonMouseLeave } onMouseUp={ props.DeployGITLabelonMouseUp } onMouseDown={ props.DeployGITLabelonMouseDown } onKeyDown={ props.DeployGITLabelonKeyDown } onChange={ props.DeployGITLabelonChange } ondelay={ props.DeployGITLabelondelay }>
                                <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['deploygitlabeltext']?.animationClass || {}}>

                                  <span id="id_fourthreefour_onesevensixsix"  className={` text deploygitlabeltext    ${ props.onClick ? 'cursor' : ''}  ${ transaction['deploygitlabeltext']?.type ? transaction['deploygitlabeltext']?.type.toLowerCase() : '' }`} style={{ ...{},  ...props.DeployGITLabelTextStyle , transitionDuration: transaction['deploygitlabeltext']?.duration, transitionTimingFunction: transaction['deploygitlabeltext']?.timingFunction }} onClick={ props.DeployGITLabelTextonClick } onMouseEnter={ props.DeployGITLabelTextonMouseEnter } onMouseOver={ props.DeployGITLabelTextonMouseOver } onKeyPress={ props.DeployGITLabelTextonKeyPress } onDrag={ props.DeployGITLabelTextonDrag } onMouseLeave={ props.DeployGITLabelTextonMouseLeave } onMouseUp={ props.DeployGITLabelTextonMouseUp } onMouseDown={ props.DeployGITLabelTextonMouseDown } onKeyDown={ props.DeployGITLabelTextonKeyDown } onChange={ props.DeployGITLabelTextonChange } ondelay={ props.DeployGITLabelTextondelay } >{props.DeployGITLabelText1 || `¿Cuál es tu sexo?`}</span>

                                </CSSTransition>
                              </div>

                            </CSSTransition>
                            <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['framethreefivenigth']?.animationClass || {}}>

                              <div id="id_fourthreefour_oneeightzerosix" className={` frame framethreefivenigth ${ props.onClick ? 'cursor' : '' } ${ transaction['framethreefivenigth']?.type ? transaction['framethreefivenigth']?.type.toLowerCase() : '' }`} style={ { ...{}, ...props.FramethreefivenigthStyle , transitionDuration: transaction['framethreefivenigth']?.duration, transitionTimingFunction: transaction['framethreefivenigth']?.timingFunction } } onClick={ props.FramethreefivenigthonClick } onMouseEnter={ props.FramethreefivenigthonMouseEnter } onMouseOver={ props.FramethreefivenigthonMouseOver } onKeyPress={ props.FramethreefivenigthonKeyPress } onDrag={ props.FramethreefivenigthonDrag } onMouseLeave={ props.FramethreefivenigthonMouseLeave } onMouseUp={ props.FramethreefivenigthonMouseUp } onMouseDown={ props.FramethreefivenigthonMouseDown } onKeyDown={ props.FramethreefivenigthonKeyDown } onChange={ props.FramethreefivenigthonChange } ondelay={ props.Framethreefivenigthondelay }>
                                <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['framethreefiveeight']?.animationClass || {}}>

                                  <div id="id_fourthreefour_oneeightzerofive" className={` frame framethreefiveeight ${ props.onClick ? 'cursor' : '' } ${ transaction['framethreefiveeight']?.type ? transaction['framethreefiveeight']?.type.toLowerCase() : '' }`} style={ { ...{}, ...props.FramethreefiveeightStyle , transitionDuration: transaction['framethreefiveeight']?.duration, transitionTimingFunction: transaction['framethreefiveeight']?.timingFunction } } onClick={ props.FramethreefiveeightonClick } onMouseEnter={ props.FramethreefiveeightonMouseEnter } onMouseOver={ props.FramethreefiveeightonMouseOver } onKeyPress={ props.FramethreefiveeightonKeyPress } onDrag={ props.FramethreefiveeightonDrag } onMouseLeave={ props.FramethreefiveeightonMouseLeave } onMouseUp={ props.FramethreefiveeightonMouseUp } onMouseDown={ props.FramethreefiveeightonMouseDown } onKeyDown={ props.FramethreefiveeightonKeyDown } onChange={ props.FramethreefiveeightonChange } ondelay={ props.Framethreefiveeightondelay }>
                                    <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['selectloginsex']?.animationClass || {}}>
                                      <SelectLoginSex { ...{ ...props, style:false } } cssClass={"C_fourthreefour_oneseveneightzero "}  />
    </CSSTransition >
<CSSTransition 
        in={_in}
        appear={true} 
        timeout={100}
        classNames={transaction['selectloginsex']?.animationClass || {}}
    >
    <SelectLoginSex { ...{ ...props, style:false } }   Hombre0={ props.Hombre1 || " Mujer"} cssClass={"C_fourthreefour_oneseveneightfive "}  />
    </CSSTransition >
<CSSTransition 
        in={_in}
        appear={true} 
        timeout={100}
        classNames={transaction['selectloginsex']?.animationClass || {}}
    >
    <SelectLoginSex { ...{ ...props, style:false } }   Hombre0={ props.Hombre2 || " No binario"} cssClass={"C_fourthreefour_onesevennigthzero "}  />
    </CSSTransition >
<CSSTransition 
        in={_in}
        appear={true} 
        timeout={100}
        classNames={transaction['selectloginsex']?.animationClass || {}}
    >
    <SelectLoginSex { ...{ ...props, style:false } }   Hombre0={ props.Hombre3 || " Otro"} cssClass={"C_fourthreefour_onesevennigthfive "}  />
    </CSSTransition >
                    </div>
               
    </CSSTransition >
<CSSTransition 
        in={_in}
        appear={true} 
        timeout={100}
        classNames={transaction['selectloginsex']?.animationClass || {}}
    >
    <SelectLoginSex { ...{ ...props, style:false } }   Hombre0={ props.Hombre4 || " Prefiero no aclararlo"} cssClass={"C_fourthreefour_oneeightzerozero "}  />
    </CSSTransition >
                    </div>
               
    </CSSTransition >
                    </div>
               
    </CSSTransition >
<CSSTransition 
        in={_in}
        appear={true} 
        timeout={100}
        classNames={transaction['selectpreferencias']?.animationClass || {}}
    >
    
                    <div id="id_fourthreefour_oneeightthreefour" className={` frame selectpreferencias ${ props.onClick ? 'cursor' : '' } ${ transaction['selectpreferencias']?.type ? transaction['selectpreferencias']?.type.toLowerCase() : '' }`} style={ { ...{}, ...props.SelectPreferenciasStyle , transitionDuration: transaction['selectpreferencias']?.duration, transitionTimingFunction: transaction['selectpreferencias']?.timingFunction } } onClick={ props.SelectPreferenciasonClick } onMouseEnter={ props.SelectPreferenciasonMouseEnter } onMouseOver={ props.SelectPreferenciasonMouseOver } onKeyPress={ props.SelectPreferenciasonKeyPress } onDrag={ props.SelectPreferenciasonDrag } onMouseLeave={ props.SelectPreferenciasonMouseLeave } onMouseUp={ props.SelectPreferenciasonMouseUp } onMouseDown={ props.SelectPreferenciasonMouseDown } onKeyDown={ props.SelectPreferenciasonKeyDown } onChange={ props.SelectPreferenciasonChange } ondelay={ props.SelectPreferenciasondelay }>
                                        <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['selectcreate']?.animationClass || {}}>
                                          <SelectCreate { ...{ ...props, style:false } } cssClass={"C_fourthreefour_oneeighttwoofour "}  />
    </CSSTransition >
<CSSTransition 
        in={_in}
        appear={true} 
        timeout={100}
        classNames={transaction['selectcreate']?.animationClass || {}}
    >
    <SelectCreate { ...{ ...props, style:false } }   PrefieronorecibirpublicidaddeSpotify0={ props.PrefieronorecibirpublicidaddeSpotify1 || " Compartir mis datos de registro con los proveedores de contenido de Spotify para fines de marketing"} cssClass={"C_fourthreefour_oneeighttwoonigth "}  />
    </CSSTransition >
                    </div>
               
    </CSSTransition >
<CSSTransition 
        in={_in}
        appear={true} 
        timeout={100}
        classNames={transaction['framethreesixseven']?.animationClass || {}}
    >
    
                    <div id="id_fourthreefour_oneeightseventhree" className={` frame framethreesixseven ${ props.onClick ? 'cursor' : '' } ${ transaction['framethreesixseven']?.type ? transaction['framethreesixseven']?.type.toLowerCase() : '' }`} style={ { ...{}, ...props.FramethreesixsevenStyle , transitionDuration: transaction['framethreesixseven']?.duration, transitionTimingFunction: transaction['framethreesixseven']?.timingFunction } } onClick={ props.FramethreesixsevenonClick } onMouseEnter={ props.FramethreesixsevenonMouseEnter } onMouseOver={ props.FramethreesixsevenonMouseOver } onKeyPress={ props.FramethreesixsevenonKeyPress } onDrag={ props.FramethreesixsevenonDrag } onMouseLeave={ props.FramethreesixsevenonMouseLeave } onMouseUp={ props.FramethreesixsevenonMouseUp } onMouseDown={ props.FramethreesixsevenonMouseDown } onKeyDown={ props.FramethreesixsevenonKeyDown } onChange={ props.FramethreesixsevenonChange } ondelay={ props.Framethreesixsevenondelay }>
                                            <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['framethreesixfour']?.animationClass || {}}>

                                              <div id="id_fourthreefour_oneeightsixfive" className={` frame framethreesixfour ${ props.onClick ? 'cursor' : '' } ${ transaction['framethreesixfour']?.type ? transaction['framethreesixfour']?.type.toLowerCase() : '' }`} style={ { ...{}, ...props.FramethreesixfourStyle , transitionDuration: transaction['framethreesixfour']?.duration, transitionTimingFunction: transaction['framethreesixfour']?.timingFunction } } onClick={ props.FramethreesixfouronClick } onMouseEnter={ props.FramethreesixfouronMouseEnter } onMouseOver={ props.FramethreesixfouronMouseOver } onKeyPress={ props.FramethreesixfouronKeyPress } onDrag={ props.FramethreesixfouronDrag } onMouseLeave={ props.FramethreesixfouronMouseLeave } onMouseUp={ props.FramethreesixfouronMouseUp } onMouseDown={ props.FramethreesixfouronMouseDown } onKeyDown={ props.FramethreesixfouronKeyDown } onChange={ props.FramethreesixfouronChange } ondelay={ props.Framethreesixfourondelay }>
                                                <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['framethreesixzero']?.animationClass || {}}>

                                                  <div id="id_fourthreefour_oneeightfournigth" className={` frame framethreesixzero ${ props.onClick ? 'cursor' : '' } ${ transaction['framethreesixzero']?.type ? transaction['framethreesixzero']?.type.toLowerCase() : '' }`} style={ { ...{}, ...props.FramethreesixzeroStyle , transitionDuration: transaction['framethreesixzero']?.duration, transitionTimingFunction: transaction['framethreesixzero']?.timingFunction } } onClick={ props.FramethreesixzeroonClick } onMouseEnter={ props.FramethreesixzeroonMouseEnter } onMouseOver={ props.FramethreesixzeroonMouseOver } onKeyPress={ props.FramethreesixzeroonKeyPress } onDrag={ props.FramethreesixzeroonDrag } onMouseLeave={ props.FramethreesixzeroonMouseLeave } onMouseUp={ props.FramethreesixzeroonMouseUp } onMouseDown={ props.FramethreesixzeroonMouseDown } onKeyDown={ props.FramethreesixzeroonKeyDown } onChange={ props.FramethreesixzeroonChange } ondelay={ props.Framethreesixzeroondelay }>
                                                    <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['frame']?.animationClass || {}}>

                                                      <div id="id_fourthreefour_oneeightfourzero" className={` frame frame ${ props.onClick ? 'cursor' : '' } ${ transaction['frame']?.type ? transaction['frame']?.type.toLowerCase() : '' }`} style={ { ...{}, ...props.FrameStyle , transitionDuration: transaction['frame']?.duration, transitionTimingFunction: transaction['frame']?.timingFunction } } onClick={ props.FrameonClick } onMouseEnter={ props.FrameonMouseEnter } onMouseOver={ props.FrameonMouseOver } onKeyPress={ props.FrameonKeyPress } onDrag={ props.FrameonDrag } onMouseLeave={ props.FrameonMouseLeave } onMouseUp={ props.FrameonMouseUp } onMouseDown={ props.FrameonMouseDown } onKeyDown={ props.FrameonKeyDown } onChange={ props.FrameonChange } ondelay={ props.Frameondelay }>
                                                        <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['alregistrarteaceptaslos']?.animationClass || {}}>

                                                          <span id="id_fourthreefour_oneeightthreefive"  className={` text alregistrarteaceptaslos    ${ props.onClick ? 'cursor' : ''}  ${ transaction['alregistrarteaceptaslos']?.type ? transaction['alregistrarteaceptaslos']?.type.toLowerCase() : '' }`} style={{ ...{},  ...props.AlregistrarteaceptaslosStyle , transitionDuration: transaction['alregistrarteaceptaslos']?.duration, transitionTimingFunction: transaction['alregistrarteaceptaslos']?.timingFunction }} onClick={ props.AlregistrarteaceptaslosonClick } onMouseEnter={ props.AlregistrarteaceptaslosonMouseEnter } onMouseOver={ props.AlregistrarteaceptaslosonMouseOver } onKeyPress={ props.AlregistrarteaceptaslosonKeyPress } onDrag={ props.AlregistrarteaceptaslosonDrag } onMouseLeave={ props.AlregistrarteaceptaslosonMouseLeave } onMouseUp={ props.AlregistrarteaceptaslosonMouseUp } onMouseDown={ props.AlregistrarteaceptaslosonMouseDown } onKeyDown={ props.AlregistrarteaceptaslosonKeyDown } onChange={ props.AlregistrarteaceptaslosonChange } ondelay={ props.Alregistrarteaceptaslosondelay } >{props.Alregistrarteaceptaslos0 || `Al registrarte, aceptas los`}</span>

                                                        </CSSTransition>
                                                      </div>

                                                    </CSSTransition>
                                                    <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['textlinkgreen']?.animationClass || {}}>
                                                      <TextLinkGreen { ...{ ...props, style:false } } variant={'Property 1=Default'} TrminosyCondicionesdeUso0={ props.TrminosyCondicionesdeUso0 || "Términos y Condiciones de Uso" } cssClass={"C_fourthreeeight_onethreetwoothree "}  />
    </CSSTransition >
<CSSTransition 
        in={_in}
        appear={true} 
        timeout={100}
        classNames={transaction['frame']?.animationClass || {}}
    >
    
                    <div id="id_fourthreefour_oneeightfourseven" className={` frame frame ${ props.onClick ? 'cursor' : '' } ${ transaction['frame']?.type ? transaction['frame']?.type.toLowerCase() : '' }`} style={ { ...{}, ...props.FrameStyle , transitionDuration: transaction['frame']?.duration, transitionTimingFunction: transaction['frame']?.timingFunction } } onClick={ props.FrameonClick } onMouseEnter={ props.FrameonMouseEnter } onMouseOver={ props.FrameonMouseOver } onKeyPress={ props.FrameonKeyPress } onDrag={ props.FrameonDrag } onMouseLeave={ props.FrameonMouseLeave } onMouseUp={ props.FrameonMouseUp } onMouseDown={ props.FrameonMouseDown } onKeyDown={ props.FrameonKeyDown } onChange={ props.FrameonChange } ondelay={ props.Frameondelay }>
                                                        <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['despotify']?.animationClass || {}}>

                                                          <span id="id_fourthreefour_oneeightfoureight"  className={` text despotify    ${ props.onClick ? 'cursor' : ''}  ${ transaction['despotify']?.type ? transaction['despotify']?.type.toLowerCase() : '' }`} style={{ ...{},  ...props.DeSpotifyStyle , transitionDuration: transaction['despotify']?.duration, transitionTimingFunction: transaction['despotify']?.timingFunction }} onClick={ props.DeSpotifyonClick } onMouseEnter={ props.DeSpotifyonMouseEnter } onMouseOver={ props.DeSpotifyonMouseOver } onKeyPress={ props.DeSpotifyonKeyPress } onDrag={ props.DeSpotifyonDrag } onMouseLeave={ props.DeSpotifyonMouseLeave } onMouseUp={ props.DeSpotifyonMouseUp } onMouseDown={ props.DeSpotifyonMouseDown } onKeyDown={ props.DeSpotifyonKeyDown } onChange={ props.DeSpotifyonChange } ondelay={ props.DeSpotifyondelay } >{props.DeSpotify0 || `de Spotify`}</span>

                                                        </CSSTransition>
                                                  </div>

                                                </CSSTransition>
                                              </div>

                                            </CSSTransition>
                                            <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['framethreesixthree']?.animationClass || {}}>

                                              <div id="id_fourthreefour_oneeightsixfour" className={` frame framethreesixthree ${ props.onClick ? 'cursor' : '' } ${ transaction['framethreesixthree']?.type ? transaction['framethreesixthree']?.type.toLowerCase() : '' }`} style={ { ...{}, ...props.FramethreesixthreeStyle , transitionDuration: transaction['framethreesixthree']?.duration, transitionTimingFunction: transaction['framethreesixthree']?.timingFunction } } onClick={ props.FramethreesixthreeonClick } onMouseEnter={ props.FramethreesixthreeonMouseEnter } onMouseOver={ props.FramethreesixthreeonMouseOver } onKeyPress={ props.FramethreesixthreeonKeyPress } onDrag={ props.FramethreesixthreeonDrag } onMouseLeave={ props.FramethreesixthreeonMouseLeave } onMouseUp={ props.FramethreesixthreeonMouseUp } onMouseDown={ props.FramethreesixthreeonMouseDown } onKeyDown={ props.FramethreesixthreeonKeyDown } onChange={ props.FramethreesixthreeonChange } ondelay={ props.Framethreesixthreeondelay }>
                                                <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['frame']?.animationClass || {}}>

                                                  <div id="id_fourthreefour_oneeightfiveone" className={` frame frame ${ props.onClick ? 'cursor' : '' } ${ transaction['frame']?.type ? transaction['frame']?.type.toLowerCase() : '' }`} style={ { ...{}, ...props.FrameStyle , transitionDuration: transaction['frame']?.duration, transitionTimingFunction: transaction['frame']?.timingFunction } } onClick={ props.FrameonClick } onMouseEnter={ props.FrameonMouseEnter } onMouseOver={ props.FrameonMouseOver } onKeyPress={ props.FrameonKeyPress } onDrag={ props.FrameonDrag } onMouseLeave={ props.FrameonMouseLeave } onMouseUp={ props.FrameonMouseUp } onMouseDown={ props.FrameonMouseDown } onKeyDown={ props.FrameonKeyDown } onChange={ props.FrameonChange } ondelay={ props.Frameondelay }>
                                                    <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['paraobtenermsinformacinsobrecmospotifyrecopilausacomparteyprotegetus']?.animationClass || {}}>

                                                      <span id="id_fourthreefour_oneeightfivetwoo"  className={` text paraobtenermsinformacinsobrecmospotifyrecopilausacomparteyprotegetus    ${ props.onClick ? 'cursor' : ''}  ${ transaction['paraobtenermsinformacinsobrecmospotifyrecopilausacomparteyprotegetus']?.type ? transaction['paraobtenermsinformacinsobrecmospotifyrecopilausacomparteyprotegetus']?.type.toLowerCase() : '' }`} style={{ ...{},  ...props.ParaobtenermsinformacinsobrecmoSpotifyrecopilausacomparteyprotegetusStyle , transitionDuration: transaction['paraobtenermsinformacinsobrecmospotifyrecopilausacomparteyprotegetus']?.duration, transitionTimingFunction: transaction['paraobtenermsinformacinsobrecmospotifyrecopilausacomparteyprotegetus']?.timingFunction }} onClick={ props.ParaobtenermsinformacinsobrecmoSpotifyrecopilausacomparteyprotegetusonClick } onMouseEnter={ props.ParaobtenermsinformacinsobrecmoSpotifyrecopilausacomparteyprotegetusonMouseEnter } onMouseOver={ props.ParaobtenermsinformacinsobrecmoSpotifyrecopilausacomparteyprotegetusonMouseOver } onKeyPress={ props.ParaobtenermsinformacinsobrecmoSpotifyrecopilausacomparteyprotegetusonKeyPress } onDrag={ props.ParaobtenermsinformacinsobrecmoSpotifyrecopilausacomparteyprotegetusonDrag } onMouseLeave={ props.ParaobtenermsinformacinsobrecmoSpotifyrecopilausacomparteyprotegetusonMouseLeave } onMouseUp={ props.ParaobtenermsinformacinsobrecmoSpotifyrecopilausacomparteyprotegetusonMouseUp } onMouseDown={ props.ParaobtenermsinformacinsobrecmoSpotifyrecopilausacomparteyprotegetusonMouseDown } onKeyDown={ props.ParaobtenermsinformacinsobrecmoSpotifyrecopilausacomparteyprotegetusonKeyDown } onChange={ props.ParaobtenermsinformacinsobrecmoSpotifyrecopilausacomparteyprotegetusonChange } ondelay={ props.ParaobtenermsinformacinsobrecmoSpotifyrecopilausacomparteyprotegetusondelay } >{props.ParaobtenermsinformacinsobrecmoSpotifyrecopilausacomparteyprotegetus0 || `Para obtener más información sobre cómo Spotify recopila, usa, comparte y protege tus`}</span>

                                                    </CSSTransition>
                                                  </div>

                                                </CSSTransition>
                                                <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['framethreesixtwoo']?.animationClass || {}}>

                                                  <div id="id_fourthreefour_oneeightfiveseven" className={` frame framethreesixtwoo ${ props.onClick ? 'cursor' : '' } ${ transaction['framethreesixtwoo']?.type ? transaction['framethreesixtwoo']?.type.toLowerCase() : '' }`} style={ { ...{}, ...props.FramethreesixtwooStyle , transitionDuration: transaction['framethreesixtwoo']?.duration, transitionTimingFunction: transaction['framethreesixtwoo']?.timingFunction } } onClick={ props.FramethreesixtwooonClick } onMouseEnter={ props.FramethreesixtwooonMouseEnter } onMouseOver={ props.FramethreesixtwooonMouseOver } onKeyPress={ props.FramethreesixtwooonKeyPress } onDrag={ props.FramethreesixtwooonDrag } onMouseLeave={ props.FramethreesixtwooonMouseLeave } onMouseUp={ props.FramethreesixtwooonMouseUp } onMouseDown={ props.FramethreesixtwooonMouseDown } onKeyDown={ props.FramethreesixtwooonKeyDown } onChange={ props.FramethreesixtwooonChange } ondelay={ props.Framethreesixtwooondelay }>
                                                    <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['frame']?.animationClass || {}}>

                                                      <div id="id_fourthreefour_oneeightfiveeight" className={` frame frame ${ props.onClick ? 'cursor' : '' } ${ transaction['frame']?.type ? transaction['frame']?.type.toLowerCase() : '' }`} style={ { ...{}, ...props.FrameStyle , transitionDuration: transaction['frame']?.duration, transitionTimingFunction: transaction['frame']?.timingFunction } } onClick={ props.FrameonClick } onMouseEnter={ props.FrameonMouseEnter } onMouseOver={ props.FrameonMouseOver } onKeyPress={ props.FrameonKeyPress } onDrag={ props.FrameonDrag } onMouseLeave={ props.FrameonMouseLeave } onMouseUp={ props.FrameonMouseUp } onMouseDown={ props.FrameonMouseDown } onKeyDown={ props.FrameonKeyDown } onChange={ props.FrameonChange } ondelay={ props.Frameondelay }>
                                                        <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['datospersonalesleela']?.animationClass || {}}>

                                                          <span id="id_fourthreefour_oneeightfivenigth"  className={` text datospersonalesleela    ${ props.onClick ? 'cursor' : ''}  ${ transaction['datospersonalesleela']?.type ? transaction['datospersonalesleela']?.type.toLowerCase() : '' }`} style={{ ...{},  ...props.DatospersonalesleelaStyle , transitionDuration: transaction['datospersonalesleela']?.duration, transitionTimingFunction: transaction['datospersonalesleela']?.timingFunction }} onClick={ props.DatospersonalesleelaonClick } onMouseEnter={ props.DatospersonalesleelaonMouseEnter } onMouseOver={ props.DatospersonalesleelaonMouseOver } onKeyPress={ props.DatospersonalesleelaonKeyPress } onDrag={ props.DatospersonalesleelaonDrag } onMouseLeave={ props.DatospersonalesleelaonMouseLeave } onMouseUp={ props.DatospersonalesleelaonMouseUp } onMouseDown={ props.DatospersonalesleelaonMouseDown } onKeyDown={ props.DatospersonalesleelaonKeyDown } onChange={ props.DatospersonalesleelaonChange } ondelay={ props.Datospersonalesleelaondelay } >{props.Datospersonalesleela0 || `datos personales, lee la`}</span>

                                                        </CSSTransition>
                                                      </div>

                                                    </CSSTransition>
                                                    <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['textlinkgreen']?.animationClass || {}}>
                                                      <TextLinkGreen { ...{ ...props, style:false } } variant={'Property 1=Default'} TrminosyCondicionesdeUso0={ props.TrminosyCondicionesdeUso1 || "Política de Privacidad de Spotify" } cssClass={"C_fourthreeeight_onethreetwoonigth "}  />
    </CSSTransition >
                    </div>
               
    </CSSTransition >
                    </div>
               
    </CSSTransition >
<CSSTransition 
        in={_in}
        appear={true} 
        timeout={100}
        classNames={transaction['buttongreen']?.animationClass || {}}
    >
    <ButtonGreen { ...{ ...props, style:false } }  variant={'Property 1=Default'} PrincipalButtonText0={ props.PrincipalButtonText0 || " Registrate"} PrincipalButtonTextStyle={{"height":"14px"}} cssClass={"C_fourthreeeight_onethreezeroeight "}  />
    </CSSTransition >
                    </div>
               
    </CSSTransition >
<CSSTransition 
        in={_in}
        appear={true} 
        timeout={100}
        classNames={transaction['framethreesixsix']?.animationClass || {}}
    >
    
                    <div id="id_fourthreefour_oneeightseventwoo" className={` frame framethreesixsix ${ props.onClick ? 'cursor' : '' } ${ transaction['framethreesixsix']?.type ? transaction['framethreesixsix']?.type.toLowerCase() : '' }`} style={ { ...{}, ...props.FramethreesixsixStyle , transitionDuration: transaction['framethreesixsix']?.duration, transitionTimingFunction: transaction['framethreesixsix']?.timingFunction } } onClick={ props.FramethreesixsixonClick } onMouseEnter={ props.FramethreesixsixonMouseEnter } onMouseOver={ props.FramethreesixsixonMouseOver } onKeyPress={ props.FramethreesixsixonKeyPress } onDrag={ props.FramethreesixsixonDrag } onMouseLeave={ props.FramethreesixsixonMouseLeave } onMouseUp={ props.FramethreesixsixonMouseUp } onMouseDown={ props.FramethreesixsixonMouseDown } onKeyDown={ props.FramethreesixsixonKeyDown } onChange={ props.FramethreesixsixonChange } ondelay={ props.Framethreesixsixondelay }>
                                                        <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['framethreefiveseven']?.animationClass || {}}>

                                                          <div id="id_fourthreefour_oneeightsixeight" className={` frame framethreefiveseven ${ props.onClick ? 'cursor' : '' } ${ transaction['framethreefiveseven']?.type ? transaction['framethreefiveseven']?.type.toLowerCase() : '' }`} style={ { ...{}, ...props.FramethreefivesevenStyle , transitionDuration: transaction['framethreefiveseven']?.duration, transitionTimingFunction: transaction['framethreefiveseven']?.timingFunction } } onClick={ props.FramethreefivesevenonClick } onMouseEnter={ props.FramethreefivesevenonMouseEnter } onMouseOver={ props.FramethreefivesevenonMouseOver } onKeyPress={ props.FramethreefivesevenonKeyPress } onDrag={ props.FramethreefivesevenonDrag } onMouseLeave={ props.FramethreefivesevenonMouseLeave } onMouseUp={ props.FramethreefivesevenonMouseUp } onMouseDown={ props.FramethreefivesevenonMouseDown } onKeyDown={ props.FramethreefivesevenonKeyDown } onChange={ props.FramethreefivesevenonChange } ondelay={ props.Framethreefivesevenondelay }>
                                                            <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['yatienescuenta']?.animationClass || {}}>

                                                              <span id="id_fourthreefour_oneeightsixnigth"  className={` text yatienescuenta    ${ props.onClick ? 'cursor' : ''}  ${ transaction['yatienescuenta']?.type ? transaction['yatienescuenta']?.type.toLowerCase() : '' }`} style={{ ...{},  ...props.YatienescuentaStyle , transitionDuration: transaction['yatienescuenta']?.duration, transitionTimingFunction: transaction['yatienescuenta']?.timingFunction }} onClick={ props.YatienescuentaonClick } onMouseEnter={ props.YatienescuentaonMouseEnter } onMouseOver={ props.YatienescuentaonMouseOver } onKeyPress={ props.YatienescuentaonKeyPress } onDrag={ props.YatienescuentaonDrag } onMouseLeave={ props.YatienescuentaonMouseLeave } onMouseUp={ props.YatienescuentaonMouseUp } onMouseDown={ props.YatienescuentaonMouseDown } onKeyDown={ props.YatienescuentaonKeyDown } onChange={ props.YatienescuentaonChange } ondelay={ props.Yatienescuentaondelay } >{props.Yatienescuenta0 || `¿Ya tienes cuenta?`}</span>

                                                            </CSSTransition>
                                                          </div>

                                                        </CSSTransition>
                                                        <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['framethreesixfive']?.animationClass || {}}>

                                                          <div id="id_fourthreefour_oneeightsevenzero" className={` frame framethreesixfive ${ props.onClick ? 'cursor' : '' } ${ transaction['framethreesixfive']?.type ? transaction['framethreesixfive']?.type.toLowerCase() : '' }`} style={ { ...{}, ...props.FramethreesixfiveStyle , transitionDuration: transaction['framethreesixfive']?.duration, transitionTimingFunction: transaction['framethreesixfive']?.timingFunction } } onClick={ props.FramethreesixfiveonClick } onMouseEnter={ props.FramethreesixfiveonMouseEnter } onMouseOver={ props.FramethreesixfiveonMouseOver } onKeyPress={ props.FramethreesixfiveonKeyPress } onDrag={ props.FramethreesixfiveonDrag } onMouseLeave={ props.FramethreesixfiveonMouseLeave } onMouseUp={ props.FramethreesixfiveonMouseUp } onMouseDown={ props.FramethreesixfiveonMouseDown } onKeyDown={ props.FramethreesixfiveonKeyDown } onChange={ props.FramethreesixfiveonChange } ondelay={ props.Framethreesixfiveondelay }>
                                                            <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['iniciasesin']?.animationClass || {}}>

                                                              <span id="id_fourthreefour_oneeightsevenone"  className={` text iniciasesin    ${ props.onClick ? 'cursor' : ''}  ${ transaction['iniciasesin']?.type ? transaction['iniciasesin']?.type.toLowerCase() : '' }`} style={{ ...{},  ...props.IniciasesinStyle , transitionDuration: transaction['iniciasesin']?.duration, transitionTimingFunction: transaction['iniciasesin']?.timingFunction }} onClick={ props.IniciasesinonClick } onMouseEnter={ props.IniciasesinonMouseEnter } onMouseOver={ props.IniciasesinonMouseOver } onKeyPress={ props.IniciasesinonKeyPress } onDrag={ props.IniciasesinonDrag } onMouseLeave={ props.IniciasesinonMouseLeave } onMouseUp={ props.IniciasesinonMouseUp } onMouseDown={ props.IniciasesinonMouseDown } onKeyDown={ props.IniciasesinonKeyDown } onChange={ props.IniciasesinonChange } ondelay={ props.Iniciasesinondelay } >{props.Iniciasesin0 || `Inicia sesión`}</span>

                                                            </CSSTransition>
                                                          </div>

                                                        </CSSTransition>
                                                  </div>

                                                </CSSTransition>
                                              </div>

                                            </CSSTransition>
                                  </div>

                                </CSSTransition>
                              </div>

                            </CSSTransition>

      </>
      }
    </div>

  </CSSTransition>
</Contexts.CrearCuentaLocalContext.Provider>
    ) 
}

CrearCuenta.propTypes = {
    style: PropTypes.any,
DisplayfiveText0: PropTypes.any,
LineLeft0: PropTypes.any,
HoneText0: PropTypes.any,
LineRigth0: PropTypes.any,
DeployTextInput0: PropTypes.any,
DeployGITLabelText0: PropTypes.any,
DeployTextInput1: PropTypes.any,
HfiveText1: PropTypes.any,
DeployGITLabelText1: PropTypes.any,
HfiveText2: PropTypes.any,
DeployTextInput2: PropTypes.any,
Hombre1: PropTypes.any,
Hombre2: PropTypes.any,
Hombre3: PropTypes.any,
Hombre4: PropTypes.any,
PrefieronorecibirpublicidaddeSpotify1: PropTypes.any,
Alregistrarteaceptaslos0: PropTypes.any,
TrminosyCondicionesdeUso0: PropTypes.any,
DeSpotify0: PropTypes.any,
ParaobtenermsinformacinsobrecmoSpotifyrecopilausacomparteyprotegetus0: PropTypes.any,
Datospersonalesleela0: PropTypes.any,
TrminosyCondicionesdeUso1: PropTypes.any,
PrincipalButtonText0: PropTypes.any,
Yatienescuenta0: PropTypes.any,
Iniciasesin0: PropTypes.any,
CrearCuentaonClick: PropTypes.any,
CrearCuentaonMouseEnter: PropTypes.any,
CrearCuentaonMouseOver: PropTypes.any,
CrearCuentaonKeyPress: PropTypes.any,
CrearCuentaonDrag: PropTypes.any,
CrearCuentaonMouseLeave: PropTypes.any,
CrearCuentaonMouseUp: PropTypes.any,
CrearCuentaonMouseDown: PropTypes.any,
CrearCuentaonKeyDown: PropTypes.any,
CrearCuentaonChange: PropTypes.any,
CrearCuentaondelay: PropTypes.any,
FramethreesevenoneonClick: PropTypes.any,
FramethreesevenoneonMouseEnter: PropTypes.any,
FramethreesevenoneonMouseOver: PropTypes.any,
FramethreesevenoneonKeyPress: PropTypes.any,
FramethreesevenoneonDrag: PropTypes.any,
FramethreesevenoneonMouseLeave: PropTypes.any,
FramethreesevenoneonMouseUp: PropTypes.any,
FramethreesevenoneonMouseDown: PropTypes.any,
FramethreesevenoneonKeyDown: PropTypes.any,
FramethreesevenoneonChange: PropTypes.any,
Framethreesevenoneondelay: PropTypes.any,
FramethreesixnigthonClick: PropTypes.any,
FramethreesixnigthonMouseEnter: PropTypes.any,
FramethreesixnigthonMouseOver: PropTypes.any,
FramethreesixnigthonKeyPress: PropTypes.any,
FramethreesixnigthonDrag: PropTypes.any,
FramethreesixnigthonMouseLeave: PropTypes.any,
FramethreesixnigthonMouseUp: PropTypes.any,
FramethreesixnigthonMouseDown: PropTypes.any,
FramethreesixnigthonKeyDown: PropTypes.any,
FramethreesixnigthonChange: PropTypes.any,
Framethreesixnigthondelay: PropTypes.any,
FramethreesixeightonClick: PropTypes.any,
FramethreesixeightonMouseEnter: PropTypes.any,
FramethreesixeightonMouseOver: PropTypes.any,
FramethreesixeightonKeyPress: PropTypes.any,
FramethreesixeightonDrag: PropTypes.any,
FramethreesixeightonMouseLeave: PropTypes.any,
FramethreesixeightonMouseUp: PropTypes.any,
FramethreesixeightonMouseDown: PropTypes.any,
FramethreesixeightonKeyDown: PropTypes.any,
FramethreesixeightonChange: PropTypes.any,
Framethreesixeightondelay: PropTypes.any,
FrameonClick: PropTypes.any,
FrameonMouseEnter: PropTypes.any,
FrameonMouseOver: PropTypes.any,
FrameonKeyPress: PropTypes.any,
FrameonDrag: PropTypes.any,
FrameonMouseLeave: PropTypes.any,
FrameonMouseUp: PropTypes.any,
FrameonMouseDown: PropTypes.any,
FrameonKeyDown: PropTypes.any,
FrameonChange: PropTypes.any,
Frameondelay: PropTypes.any,
VectoronClick: PropTypes.any,
VectoronMouseEnter: PropTypes.any,
VectoronMouseOver: PropTypes.any,
VectoronKeyPress: PropTypes.any,
VectoronDrag: PropTypes.any,
VectoronMouseLeave: PropTypes.any,
VectoronMouseUp: PropTypes.any,
VectoronMouseDown: PropTypes.any,
VectoronKeyDown: PropTypes.any,
VectoronChange: PropTypes.any,
Vectorondelay: PropTypes.any,
FramethreefivezeroonClick: PropTypes.any,
FramethreefivezeroonMouseEnter: PropTypes.any,
FramethreefivezeroonMouseOver: PropTypes.any,
FramethreefivezeroonKeyPress: PropTypes.any,
FramethreefivezeroonDrag: PropTypes.any,
FramethreefivezeroonMouseLeave: PropTypes.any,
FramethreefivezeroonMouseUp: PropTypes.any,
FramethreefivezeroonMouseDown: PropTypes.any,
FramethreefivezeroonKeyDown: PropTypes.any,
FramethreefivezeroonChange: PropTypes.any,
Framethreefivezeroondelay: PropTypes.any,
FramethreefoureightonClick: PropTypes.any,
FramethreefoureightonMouseEnter: PropTypes.any,
FramethreefoureightonMouseOver: PropTypes.any,
FramethreefoureightonKeyPress: PropTypes.any,
FramethreefoureightonDrag: PropTypes.any,
FramethreefoureightonMouseLeave: PropTypes.any,
FramethreefoureightonMouseUp: PropTypes.any,
FramethreefoureightonMouseDown: PropTypes.any,
FramethreefoureightonKeyDown: PropTypes.any,
FramethreefoureightonChange: PropTypes.any,
Framethreefoureightondelay: PropTypes.any,
FramethreefournigthonClick: PropTypes.any,
FramethreefournigthonMouseEnter: PropTypes.any,
FramethreefournigthonMouseOver: PropTypes.any,
FramethreefournigthonKeyPress: PropTypes.any,
FramethreefournigthonDrag: PropTypes.any,
FramethreefournigthonMouseLeave: PropTypes.any,
FramethreefournigthonMouseUp: PropTypes.any,
FramethreefournigthonMouseDown: PropTypes.any,
FramethreefournigthonKeyDown: PropTypes.any,
FramethreefournigthonChange: PropTypes.any,
Framethreefournigthondelay: PropTypes.any,
LineLeftonClick: PropTypes.any,
LineLeftonMouseEnter: PropTypes.any,
LineLeftonMouseOver: PropTypes.any,
LineLeftonKeyPress: PropTypes.any,
LineLeftonDrag: PropTypes.any,
LineLeftonMouseLeave: PropTypes.any,
LineLeftonMouseUp: PropTypes.any,
LineLeftonMouseDown: PropTypes.any,
LineLeftonKeyDown: PropTypes.any,
LineLeftonChange: PropTypes.any,
LineLeftondelay: PropTypes.any,
HoneonClick: PropTypes.any,
HoneonMouseEnter: PropTypes.any,
HoneonMouseOver: PropTypes.any,
HoneonKeyPress: PropTypes.any,
HoneonDrag: PropTypes.any,
HoneonMouseLeave: PropTypes.any,
HoneonMouseUp: PropTypes.any,
HoneonMouseDown: PropTypes.any,
HoneonKeyDown: PropTypes.any,
HoneonChange: PropTypes.any,
Honeondelay: PropTypes.any,
HoneTextonClick: PropTypes.any,
HoneTextonMouseEnter: PropTypes.any,
HoneTextonMouseOver: PropTypes.any,
HoneTextonKeyPress: PropTypes.any,
HoneTextonDrag: PropTypes.any,
HoneTextonMouseLeave: PropTypes.any,
HoneTextonMouseUp: PropTypes.any,
HoneTextonMouseDown: PropTypes.any,
HoneTextonKeyDown: PropTypes.any,
HoneTextonChange: PropTypes.any,
HoneTextondelay: PropTypes.any,
LineRigthonClick: PropTypes.any,
LineRigthonMouseEnter: PropTypes.any,
LineRigthonMouseOver: PropTypes.any,
LineRigthonKeyPress: PropTypes.any,
LineRigthonDrag: PropTypes.any,
LineRigthonMouseLeave: PropTypes.any,
LineRigthonMouseUp: PropTypes.any,
LineRigthonMouseDown: PropTypes.any,
LineRigthonKeyDown: PropTypes.any,
LineRigthonChange: PropTypes.any,
LineRigthondelay: PropTypes.any,
FramethreesevenzeroonClick: PropTypes.any,
FramethreesevenzeroonMouseEnter: PropTypes.any,
FramethreesevenzeroonMouseOver: PropTypes.any,
FramethreesevenzeroonKeyPress: PropTypes.any,
FramethreesevenzeroonDrag: PropTypes.any,
FramethreesevenzeroonMouseLeave: PropTypes.any,
FramethreesevenzeroonMouseUp: PropTypes.any,
FramethreesevenzeroonMouseDown: PropTypes.any,
FramethreesevenzeroonKeyDown: PropTypes.any,
FramethreesevenzeroonChange: PropTypes.any,
Framethreesevenzeroondelay: PropTypes.any,
DataDateContonClick: PropTypes.any,
DataDateContonMouseEnter: PropTypes.any,
DataDateContonMouseOver: PropTypes.any,
DataDateContonKeyPress: PropTypes.any,
DataDateContonDrag: PropTypes.any,
DataDateContonMouseLeave: PropTypes.any,
DataDateContonMouseUp: PropTypes.any,
DataDateContonMouseDown: PropTypes.any,
DataDateContonKeyDown: PropTypes.any,
DataDateContonChange: PropTypes.any,
DataDateContondelay: PropTypes.any,
DeployGITLabelonClick: PropTypes.any,
DeployGITLabelonMouseEnter: PropTypes.any,
DeployGITLabelonMouseOver: PropTypes.any,
DeployGITLabelonKeyPress: PropTypes.any,
DeployGITLabelonDrag: PropTypes.any,
DeployGITLabelonMouseLeave: PropTypes.any,
DeployGITLabelonMouseUp: PropTypes.any,
DeployGITLabelonMouseDown: PropTypes.any,
DeployGITLabelonKeyDown: PropTypes.any,
DeployGITLabelonChange: PropTypes.any,
DeployGITLabelondelay: PropTypes.any,
DeployGITLabelTextonClick: PropTypes.any,
DeployGITLabelTextonMouseEnter: PropTypes.any,
DeployGITLabelTextonMouseOver: PropTypes.any,
DeployGITLabelTextonKeyPress: PropTypes.any,
DeployGITLabelTextonDrag: PropTypes.any,
DeployGITLabelTextonMouseLeave: PropTypes.any,
DeployGITLabelTextonMouseUp: PropTypes.any,
DeployGITLabelTextonMouseDown: PropTypes.any,
DeployGITLabelTextonKeyDown: PropTypes.any,
DeployGITLabelTextonChange: PropTypes.any,
DeployGITLabelTextondelay: PropTypes.any,
FramethreefivethreeonClick: PropTypes.any,
FramethreefivethreeonMouseEnter: PropTypes.any,
FramethreefivethreeonMouseOver: PropTypes.any,
FramethreefivethreeonKeyPress: PropTypes.any,
FramethreefivethreeonDrag: PropTypes.any,
FramethreefivethreeonMouseLeave: PropTypes.any,
FramethreefivethreeonMouseUp: PropTypes.any,
FramethreefivethreeonMouseDown: PropTypes.any,
FramethreefivethreeonKeyDown: PropTypes.any,
FramethreefivethreeonChange: PropTypes.any,
Framethreefivethreeondelay: PropTypes.any,
SelectsCreateContonClick: PropTypes.any,
SelectsCreateContonMouseEnter: PropTypes.any,
SelectsCreateContonMouseOver: PropTypes.any,
SelectsCreateContonKeyPress: PropTypes.any,
SelectsCreateContonDrag: PropTypes.any,
SelectsCreateContonMouseLeave: PropTypes.any,
SelectsCreateContonMouseUp: PropTypes.any,
SelectsCreateContonMouseDown: PropTypes.any,
SelectsCreateContonKeyDown: PropTypes.any,
SelectsCreateContonChange: PropTypes.any,
SelectsCreateContondelay: PropTypes.any,
FramethreefivenigthonClick: PropTypes.any,
FramethreefivenigthonMouseEnter: PropTypes.any,
FramethreefivenigthonMouseOver: PropTypes.any,
FramethreefivenigthonKeyPress: PropTypes.any,
FramethreefivenigthonDrag: PropTypes.any,
FramethreefivenigthonMouseLeave: PropTypes.any,
FramethreefivenigthonMouseUp: PropTypes.any,
FramethreefivenigthonMouseDown: PropTypes.any,
FramethreefivenigthonKeyDown: PropTypes.any,
FramethreefivenigthonChange: PropTypes.any,
Framethreefivenigthondelay: PropTypes.any,
FramethreefiveeightonClick: PropTypes.any,
FramethreefiveeightonMouseEnter: PropTypes.any,
FramethreefiveeightonMouseOver: PropTypes.any,
FramethreefiveeightonKeyPress: PropTypes.any,
FramethreefiveeightonDrag: PropTypes.any,
FramethreefiveeightonMouseLeave: PropTypes.any,
FramethreefiveeightonMouseUp: PropTypes.any,
FramethreefiveeightonMouseDown: PropTypes.any,
FramethreefiveeightonKeyDown: PropTypes.any,
FramethreefiveeightonChange: PropTypes.any,
Framethreefiveeightondelay: PropTypes.any,
SelectPreferenciasonClick: PropTypes.any,
SelectPreferenciasonMouseEnter: PropTypes.any,
SelectPreferenciasonMouseOver: PropTypes.any,
SelectPreferenciasonKeyPress: PropTypes.any,
SelectPreferenciasonDrag: PropTypes.any,
SelectPreferenciasonMouseLeave: PropTypes.any,
SelectPreferenciasonMouseUp: PropTypes.any,
SelectPreferenciasonMouseDown: PropTypes.any,
SelectPreferenciasonKeyDown: PropTypes.any,
SelectPreferenciasonChange: PropTypes.any,
SelectPreferenciasondelay: PropTypes.any,
FramethreesixsevenonClick: PropTypes.any,
FramethreesixsevenonMouseEnter: PropTypes.any,
FramethreesixsevenonMouseOver: PropTypes.any,
FramethreesixsevenonKeyPress: PropTypes.any,
FramethreesixsevenonDrag: PropTypes.any,
FramethreesixsevenonMouseLeave: PropTypes.any,
FramethreesixsevenonMouseUp: PropTypes.any,
FramethreesixsevenonMouseDown: PropTypes.any,
FramethreesixsevenonKeyDown: PropTypes.any,
FramethreesixsevenonChange: PropTypes.any,
Framethreesixsevenondelay: PropTypes.any,
FramethreesixfouronClick: PropTypes.any,
FramethreesixfouronMouseEnter: PropTypes.any,
FramethreesixfouronMouseOver: PropTypes.any,
FramethreesixfouronKeyPress: PropTypes.any,
FramethreesixfouronDrag: PropTypes.any,
FramethreesixfouronMouseLeave: PropTypes.any,
FramethreesixfouronMouseUp: PropTypes.any,
FramethreesixfouronMouseDown: PropTypes.any,
FramethreesixfouronKeyDown: PropTypes.any,
FramethreesixfouronChange: PropTypes.any,
Framethreesixfourondelay: PropTypes.any,
FramethreesixzeroonClick: PropTypes.any,
FramethreesixzeroonMouseEnter: PropTypes.any,
FramethreesixzeroonMouseOver: PropTypes.any,
FramethreesixzeroonKeyPress: PropTypes.any,
FramethreesixzeroonDrag: PropTypes.any,
FramethreesixzeroonMouseLeave: PropTypes.any,
FramethreesixzeroonMouseUp: PropTypes.any,
FramethreesixzeroonMouseDown: PropTypes.any,
FramethreesixzeroonKeyDown: PropTypes.any,
FramethreesixzeroonChange: PropTypes.any,
Framethreesixzeroondelay: PropTypes.any,
AlregistrarteaceptaslosonClick: PropTypes.any,
AlregistrarteaceptaslosonMouseEnter: PropTypes.any,
AlregistrarteaceptaslosonMouseOver: PropTypes.any,
AlregistrarteaceptaslosonKeyPress: PropTypes.any,
AlregistrarteaceptaslosonDrag: PropTypes.any,
AlregistrarteaceptaslosonMouseLeave: PropTypes.any,
AlregistrarteaceptaslosonMouseUp: PropTypes.any,
AlregistrarteaceptaslosonMouseDown: PropTypes.any,
AlregistrarteaceptaslosonKeyDown: PropTypes.any,
AlregistrarteaceptaslosonChange: PropTypes.any,
Alregistrarteaceptaslosondelay: PropTypes.any,
DeSpotifyonClick: PropTypes.any,
DeSpotifyonMouseEnter: PropTypes.any,
DeSpotifyonMouseOver: PropTypes.any,
DeSpotifyonKeyPress: PropTypes.any,
DeSpotifyonDrag: PropTypes.any,
DeSpotifyonMouseLeave: PropTypes.any,
DeSpotifyonMouseUp: PropTypes.any,
DeSpotifyonMouseDown: PropTypes.any,
DeSpotifyonKeyDown: PropTypes.any,
DeSpotifyonChange: PropTypes.any,
DeSpotifyondelay: PropTypes.any,
FramethreesixthreeonClick: PropTypes.any,
FramethreesixthreeonMouseEnter: PropTypes.any,
FramethreesixthreeonMouseOver: PropTypes.any,
FramethreesixthreeonKeyPress: PropTypes.any,
FramethreesixthreeonDrag: PropTypes.any,
FramethreesixthreeonMouseLeave: PropTypes.any,
FramethreesixthreeonMouseUp: PropTypes.any,
FramethreesixthreeonMouseDown: PropTypes.any,
FramethreesixthreeonKeyDown: PropTypes.any,
FramethreesixthreeonChange: PropTypes.any,
Framethreesixthreeondelay: PropTypes.any,
ParaobtenermsinformacinsobrecmoSpotifyrecopilausacomparteyprotegetusonClick: PropTypes.any,
ParaobtenermsinformacinsobrecmoSpotifyrecopilausacomparteyprotegetusonMouseEnter: PropTypes.any,
ParaobtenermsinformacinsobrecmoSpotifyrecopilausacomparteyprotegetusonMouseOver: PropTypes.any,
ParaobtenermsinformacinsobrecmoSpotifyrecopilausacomparteyprotegetusonKeyPress: PropTypes.any,
ParaobtenermsinformacinsobrecmoSpotifyrecopilausacomparteyprotegetusonDrag: PropTypes.any,
ParaobtenermsinformacinsobrecmoSpotifyrecopilausacomparteyprotegetusonMouseLeave: PropTypes.any,
ParaobtenermsinformacinsobrecmoSpotifyrecopilausacomparteyprotegetusonMouseUp: PropTypes.any,
ParaobtenermsinformacinsobrecmoSpotifyrecopilausacomparteyprotegetusonMouseDown: PropTypes.any,
ParaobtenermsinformacinsobrecmoSpotifyrecopilausacomparteyprotegetusonKeyDown: PropTypes.any,
ParaobtenermsinformacinsobrecmoSpotifyrecopilausacomparteyprotegetusonChange: PropTypes.any,
ParaobtenermsinformacinsobrecmoSpotifyrecopilausacomparteyprotegetusondelay: PropTypes.any,
FramethreesixtwooonClick: PropTypes.any,
FramethreesixtwooonMouseEnter: PropTypes.any,
FramethreesixtwooonMouseOver: PropTypes.any,
FramethreesixtwooonKeyPress: PropTypes.any,
FramethreesixtwooonDrag: PropTypes.any,
FramethreesixtwooonMouseLeave: PropTypes.any,
FramethreesixtwooonMouseUp: PropTypes.any,
FramethreesixtwooonMouseDown: PropTypes.any,
FramethreesixtwooonKeyDown: PropTypes.any,
FramethreesixtwooonChange: PropTypes.any,
Framethreesixtwooondelay: PropTypes.any,
DatospersonalesleelaonClick: PropTypes.any,
DatospersonalesleelaonMouseEnter: PropTypes.any,
DatospersonalesleelaonMouseOver: PropTypes.any,
DatospersonalesleelaonKeyPress: PropTypes.any,
DatospersonalesleelaonDrag: PropTypes.any,
DatospersonalesleelaonMouseLeave: PropTypes.any,
DatospersonalesleelaonMouseUp: PropTypes.any,
DatospersonalesleelaonMouseDown: PropTypes.any,
DatospersonalesleelaonKeyDown: PropTypes.any,
DatospersonalesleelaonChange: PropTypes.any,
Datospersonalesleelaondelay: PropTypes.any,
FramethreesixsixonClick: PropTypes.any,
FramethreesixsixonMouseEnter: PropTypes.any,
FramethreesixsixonMouseOver: PropTypes.any,
FramethreesixsixonKeyPress: PropTypes.any,
FramethreesixsixonDrag: PropTypes.any,
FramethreesixsixonMouseLeave: PropTypes.any,
FramethreesixsixonMouseUp: PropTypes.any,
FramethreesixsixonMouseDown: PropTypes.any,
FramethreesixsixonKeyDown: PropTypes.any,
FramethreesixsixonChange: PropTypes.any,
Framethreesixsixondelay: PropTypes.any,
FramethreefivesevenonClick: PropTypes.any,
FramethreefivesevenonMouseEnter: PropTypes.any,
FramethreefivesevenonMouseOver: PropTypes.any,
FramethreefivesevenonKeyPress: PropTypes.any,
FramethreefivesevenonDrag: PropTypes.any,
FramethreefivesevenonMouseLeave: PropTypes.any,
FramethreefivesevenonMouseUp: PropTypes.any,
FramethreefivesevenonMouseDown: PropTypes.any,
FramethreefivesevenonKeyDown: PropTypes.any,
FramethreefivesevenonChange: PropTypes.any,
Framethreefivesevenondelay: PropTypes.any,
YatienescuentaonClick: PropTypes.any,
YatienescuentaonMouseEnter: PropTypes.any,
YatienescuentaonMouseOver: PropTypes.any,
YatienescuentaonKeyPress: PropTypes.any,
YatienescuentaonDrag: PropTypes.any,
YatienescuentaonMouseLeave: PropTypes.any,
YatienescuentaonMouseUp: PropTypes.any,
YatienescuentaonMouseDown: PropTypes.any,
YatienescuentaonKeyDown: PropTypes.any,
YatienescuentaonChange: PropTypes.any,
Yatienescuentaondelay: PropTypes.any,
FramethreesixfiveonClick: PropTypes.any,
FramethreesixfiveonMouseEnter: PropTypes.any,
FramethreesixfiveonMouseOver: PropTypes.any,
FramethreesixfiveonKeyPress: PropTypes.any,
FramethreesixfiveonDrag: PropTypes.any,
FramethreesixfiveonMouseLeave: PropTypes.any,
FramethreesixfiveonMouseUp: PropTypes.any,
FramethreesixfiveonMouseDown: PropTypes.any,
FramethreesixfiveonKeyDown: PropTypes.any,
FramethreesixfiveonChange: PropTypes.any,
Framethreesixfiveondelay: PropTypes.any,
IniciasesinonClick: PropTypes.any,
IniciasesinonMouseEnter: PropTypes.any,
IniciasesinonMouseOver: PropTypes.any,
IniciasesinonKeyPress: PropTypes.any,
IniciasesinonDrag: PropTypes.any,
IniciasesinonMouseLeave: PropTypes.any,
IniciasesinonMouseUp: PropTypes.any,
IniciasesinonMouseDown: PropTypes.any,
IniciasesinonKeyDown: PropTypes.any,
IniciasesinonChange: PropTypes.any,
Iniciasesinondelay: PropTypes.any
}
export default CrearCuenta;