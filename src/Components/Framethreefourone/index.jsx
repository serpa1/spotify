import React, { Suspense } from 'react';
import PropTypes from 'prop-types';
import * as Contexts  from 'Contexts';
import { OverlaySwapContext } from 'Components/OverlaySwapProvider';
import CorebooksAnimation from 'Components/CorebooksAnimation';
import CorebooksAnimationSingle from 'Components/CorebooksAnimationSingle';

import * as UtilsScripts  from 'utils/utils';
import { Route, useHistory, useParams } from "react-router-dom";
import SingletoneNavigation from 'Class/SingletoneNavigation';

import LoadingComponent from "Components/Loading";


import { Transition } from "react-transition-group";
import { CSSTransition } from "react-transition-group";
import PrincipalButton from 'Components/PrincipalButton'
import { useAppContext, useSessionContext } from 'context/AppContext';
import './Framethreefourone.css'





const Framethreefourone = (props)=>{
    const [state,dispatch] = useAppContext();
    const [, setNameSelectedComponent] = React.useContext(OverlaySwapContext);
    
    const nodeRef = React.useRef(null);
    const history = useHistory();
    let RouterContext = useParams();
    
    
    
    
    
    
    const [globalStateForm, dispatchForm] = React.useState({});
    
    const [ _in, setIn ] = React.useState(false)
    const [ transaction, setTransaction ] = React.useState({}) 
    const [ animation, setAnimation ] = React.useState('')

    
    
    
    
    
    
    React.useEffect(()=>{
        
        
        
    },[]);
    
    
    return (
        <>
  <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['framethreefourone']?.animationClass || {}}>

    <div id="id_fourthreeeight_onesevensixsix" ref={nodeRef} className={` ${ props.onClick ? 'cursor' : '' } framethreefourone ${ props.cssClass } ${ transaction['framethreefourone']?.type ? transaction['framethreefourone']?.type.toLowerCase() : '' }`} style={ { ...{ ...{}, transitionDuration: transaction['framethreefourone']?.duration, transitionTimingFunction: transaction['framethreefourone']?.timingFunction }, ...props.style }} onClick={ props.FramethreefouroneonClick } onMouseEnter={ props.FramethreefouroneonMouseEnter } onMouseOver={ props.FramethreefouroneonMouseOver } onKeyPress={ props.FramethreefouroneonKeyPress } onDrag={ props.FramethreefouroneonDrag } onMouseLeave={ props.FramethreefouroneonMouseLeave } onMouseUp={ props.FramethreefouroneonMouseUp } onMouseDown={ props.FramethreefouroneonMouseDown } onKeyDown={ props.FramethreefouroneonKeyDown } onChange={ props.FramethreefouroneonChange } ondelay={ props.Framethreefouroneondelay }>
      {
      props.children ?
      props.children :
      <>
        <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['framethreeonefive']?.animationClass || {}}>

          <div id="id_fourthreeeight_onesevensixseven" className={` frame framethreeonefive ${ props.onClick ? 'cursor' : '' } ${ transaction['framethreeonefive']?.type ? transaction['framethreeonefive']?.type.toLowerCase() : '' }`} style={ { ...{}, ...props.FramethreeonefiveStyle , transitionDuration: transaction['framethreeonefive']?.duration, transitionTimingFunction: transaction['framethreeonefive']?.timingFunction } } onClick={ props.FramethreeonefiveonClick } onMouseEnter={ props.FramethreeonefiveonMouseEnter } onMouseOver={ props.FramethreeonefiveonMouseOver } onKeyPress={ props.FramethreeonefiveonKeyPress } onDrag={ props.FramethreeonefiveonDrag } onMouseLeave={ props.FramethreeonefiveonMouseLeave } onMouseUp={ props.FramethreeonefiveonMouseUp } onMouseDown={ props.FramethreeonefiveonMouseDown } onKeyDown={ props.FramethreeonefiveonKeyDown } onChange={ props.FramethreeonefiveonChange } ondelay={ props.Framethreeonefiveondelay }>
            <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['muestradespotify']?.animationClass || {}}>

              <span id="id_fourthreeeight_onesevensixeight"  className={` text muestradespotify    ${ props.onClick ? 'cursor' : ''}  ${ transaction['muestradespotify']?.type ? transaction['muestradespotify']?.type.toLowerCase() : '' }`} style={{ ...{},  ...props.MuestradeSpotifyStyle , transitionDuration: transaction['muestradespotify']?.duration, transitionTimingFunction: transaction['muestradespotify']?.timingFunction }} onClick={ props.MuestradeSpotifyonClick } onMouseEnter={ props.MuestradeSpotifyonMouseEnter } onMouseOver={ props.MuestradeSpotifyonMouseOver } onKeyPress={ props.MuestradeSpotifyonKeyPress } onDrag={ props.MuestradeSpotifyonDrag } onMouseLeave={ props.MuestradeSpotifyonMouseLeave } onMouseUp={ props.MuestradeSpotifyonMouseUp } onMouseDown={ props.MuestradeSpotifyonMouseDown } onKeyDown={ props.MuestradeSpotifyonKeyDown } onChange={ props.MuestradeSpotifyonChange } ondelay={ props.MuestradeSpotifyondelay } >{props.MuestradeSpotify0 || `Muestra de Spotify`}</span>

            </CSSTransition>
            <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['regstrateparaaccederacancionesypodcastsilimitadosconalgunosanunciosnonecesitastarjetadecrdito']?.animationClass || {}}>

              <span id="id_fourthreeeight_onesevensixnigth"  className={` text regstrateparaaccederacancionesypodcastsilimitadosconalgunosanunciosnonecesitastarjetadecrdito    ${ props.onClick ? 'cursor' : ''}  ${ transaction['regstrateparaaccederacancionesypodcastsilimitadosconalgunosanunciosnonecesitastarjetadecrdito']?.type ? transaction['regstrateparaaccederacancionesypodcastsilimitadosconalgunosanunciosnonecesitastarjetadecrdito']?.type.toLowerCase() : '' }`} style={{ ...{},  ...props.RegstrateparaaccederacancionesypodcastsilimitadosconalgunosanunciosNonecesitastarjetadecrditoStyle , transitionDuration: transaction['regstrateparaaccederacancionesypodcastsilimitadosconalgunosanunciosnonecesitastarjetadecrdito']?.duration, transitionTimingFunction: transaction['regstrateparaaccederacancionesypodcastsilimitadosconalgunosanunciosnonecesitastarjetadecrdito']?.timingFunction }} onClick={ props.RegstrateparaaccederacancionesypodcastsilimitadosconalgunosanunciosNonecesitastarjetadecrditoonClick } onMouseEnter={ props.RegstrateparaaccederacancionesypodcastsilimitadosconalgunosanunciosNonecesitastarjetadecrditoonMouseEnter } onMouseOver={ props.RegstrateparaaccederacancionesypodcastsilimitadosconalgunosanunciosNonecesitastarjetadecrditoonMouseOver } onKeyPress={ props.RegstrateparaaccederacancionesypodcastsilimitadosconalgunosanunciosNonecesitastarjetadecrditoonKeyPress } onDrag={ props.RegstrateparaaccederacancionesypodcastsilimitadosconalgunosanunciosNonecesitastarjetadecrditoonDrag } onMouseLeave={ props.RegstrateparaaccederacancionesypodcastsilimitadosconalgunosanunciosNonecesitastarjetadecrditoonMouseLeave } onMouseUp={ props.RegstrateparaaccederacancionesypodcastsilimitadosconalgunosanunciosNonecesitastarjetadecrditoonMouseUp } onMouseDown={ props.RegstrateparaaccederacancionesypodcastsilimitadosconalgunosanunciosNonecesitastarjetadecrditoonMouseDown } onKeyDown={ props.RegstrateparaaccederacancionesypodcastsilimitadosconalgunosanunciosNonecesitastarjetadecrditoonKeyDown } onChange={ props.RegstrateparaaccederacancionesypodcastsilimitadosconalgunosanunciosNonecesitastarjetadecrditoonChange } ondelay={ props.RegstrateparaaccederacancionesypodcastsilimitadosconalgunosanunciosNonecesitastarjetadecrditoondelay } >{props.RegstrateparaaccederacancionesypodcastsilimitadosconalgunosanunciosNonecesitastarjetadecrdito0 || `Regístrate para acceder a canciones y podcasts ilimitados con algunos anuncios. No necesitas tarjeta de crédito.`}</span>

            </CSSTransition>
          </div>

        </CSSTransition>
        <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['principalbutton']?.animationClass || {}}>
          <PrincipalButton { ...{ ...props, style:false } } variant={'Property 1=Default'} PrincipalButtonText0={ props.PrincipalButtonText0 || "Registrate Gratis" } PrincipalButtonTextStyle={{"height":"14px"}} cssClass={"C_fourthreeeight_onesevenseventwoo "}  />
    </CSSTransition >
            
            </>
        }
        </div>
    
    </CSSTransition >
            </>
        
    ) 
}

Framethreefourone.propTypes = {
    style: PropTypes.any,
MuestradeSpotify0: PropTypes.any,
RegstrateparaaccederacancionesypodcastsilimitadosconalgunosanunciosNonecesitastarjetadecrdito0: PropTypes.any,
PrincipalButtonText0: PropTypes.any,
FramethreefouroneonClick: PropTypes.any,
FramethreefouroneonMouseEnter: PropTypes.any,
FramethreefouroneonMouseOver: PropTypes.any,
FramethreefouroneonKeyPress: PropTypes.any,
FramethreefouroneonDrag: PropTypes.any,
FramethreefouroneonMouseLeave: PropTypes.any,
FramethreefouroneonMouseUp: PropTypes.any,
FramethreefouroneonMouseDown: PropTypes.any,
FramethreefouroneonKeyDown: PropTypes.any,
FramethreefouroneonChange: PropTypes.any,
Framethreefouroneondelay: PropTypes.any,
FramethreeonefiveonClick: PropTypes.any,
FramethreeonefiveonMouseEnter: PropTypes.any,
FramethreeonefiveonMouseOver: PropTypes.any,
FramethreeonefiveonKeyPress: PropTypes.any,
FramethreeonefiveonDrag: PropTypes.any,
FramethreeonefiveonMouseLeave: PropTypes.any,
FramethreeonefiveonMouseUp: PropTypes.any,
FramethreeonefiveonMouseDown: PropTypes.any,
FramethreeonefiveonKeyDown: PropTypes.any,
FramethreeonefiveonChange: PropTypes.any,
Framethreeonefiveondelay: PropTypes.any,
MuestradeSpotifyonClick: PropTypes.any,
MuestradeSpotifyonMouseEnter: PropTypes.any,
MuestradeSpotifyonMouseOver: PropTypes.any,
MuestradeSpotifyonKeyPress: PropTypes.any,
MuestradeSpotifyonDrag: PropTypes.any,
MuestradeSpotifyonMouseLeave: PropTypes.any,
MuestradeSpotifyonMouseUp: PropTypes.any,
MuestradeSpotifyonMouseDown: PropTypes.any,
MuestradeSpotifyonKeyDown: PropTypes.any,
MuestradeSpotifyonChange: PropTypes.any,
MuestradeSpotifyondelay: PropTypes.any,
RegstrateparaaccederacancionesypodcastsilimitadosconalgunosanunciosNonecesitastarjetadecrditoonClick: PropTypes.any,
RegstrateparaaccederacancionesypodcastsilimitadosconalgunosanunciosNonecesitastarjetadecrditoonMouseEnter: PropTypes.any,
RegstrateparaaccederacancionesypodcastsilimitadosconalgunosanunciosNonecesitastarjetadecrditoonMouseOver: PropTypes.any,
RegstrateparaaccederacancionesypodcastsilimitadosconalgunosanunciosNonecesitastarjetadecrditoonKeyPress: PropTypes.any,
RegstrateparaaccederacancionesypodcastsilimitadosconalgunosanunciosNonecesitastarjetadecrditoonDrag: PropTypes.any,
RegstrateparaaccederacancionesypodcastsilimitadosconalgunosanunciosNonecesitastarjetadecrditoonMouseLeave: PropTypes.any,
RegstrateparaaccederacancionesypodcastsilimitadosconalgunosanunciosNonecesitastarjetadecrditoonMouseUp: PropTypes.any,
RegstrateparaaccederacancionesypodcastsilimitadosconalgunosanunciosNonecesitastarjetadecrditoonMouseDown: PropTypes.any,
RegstrateparaaccederacancionesypodcastsilimitadosconalgunosanunciosNonecesitastarjetadecrditoonKeyDown: PropTypes.any,
RegstrateparaaccederacancionesypodcastsilimitadosconalgunosanunciosNonecesitastarjetadecrditoonChange: PropTypes.any,
RegstrateparaaccederacancionesypodcastsilimitadosconalgunosanunciosNonecesitastarjetadecrditoondelay: PropTypes.any
}
export default Framethreefourone;