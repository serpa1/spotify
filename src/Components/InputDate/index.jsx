import React, { Suspense } from 'react';
import PropTypes from 'prop-types';
import * as Contexts  from 'Contexts';
import { OverlaySwapContext } from 'Components/OverlaySwapProvider';
import CorebooksAnimation from 'Components/CorebooksAnimation';
import CorebooksAnimationSingle from 'Components/CorebooksAnimationSingle';

import * as UtilsScripts  from 'utils/utils';
import { Route, useHistory, useParams } from "react-router-dom";
import SingletoneNavigation from 'Class/SingletoneNavigation';

import LoadingComponent from "Components/Loading";


import { Transition } from "react-transition-group";
import { CSSTransition } from "react-transition-group";
import { useAppContext, useSessionContext } from 'context/AppContext';
import './InputDate.css'





const InputDate = (props)=>{
    const [state,dispatch] = useAppContext();
    const [, setNameSelectedComponent] = React.useContext(OverlaySwapContext);
    
    const nodeRef = React.useRef(null);
    const history = useHistory();
    let RouterContext = useParams();
    
    
    
    const DeploymentContainerContext = React.useContext(Contexts.DeploymentContainer)
    
    
    const [globalStateForm, dispatchForm] = React.useState({});
    
    const [ _in, setIn ] = React.useState(false)
    const [ transaction, setTransaction ] = React.useState({}) 
    const [ animation, setAnimation ] = React.useState('')

    
    
    
    
    
    
    React.useEffect(()=>{
        
        
        
    },[]);
    
    
    return (
        <>
  <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['inputdate']?.animationClass || {}}>

    <div id="id_fourthreefour_onesevenzerofive" ref={nodeRef} className={` ${ props.onClick ? 'cursor' : '' } inputdate C_fourthreefour_onesevenzerofive ${ props.cssClass } ${ transaction['inputdate']?.type ? transaction['inputdate']?.type.toLowerCase() : '' }`} style={ { ...{ ...{}, transitionDuration: transaction['inputdate']?.duration, transitionTimingFunction: transaction['inputdate']?.timingFunction }, ...props.style }} onClick={ props.InputDateonClick } onMouseEnter={ props.InputDateonMouseEnter } onMouseOver={ props.InputDateonMouseOver } onKeyPress={ props.InputDateonKeyPress } onDrag={ props.InputDateonDrag } onMouseLeave={ props.InputDateonMouseLeave } onMouseUp={ props.InputDateonMouseUp } onMouseDown={ props.InputDateonMouseDown } onKeyDown={ props.InputDateonKeyDown } onChange={ props.InputDateonChange } ondelay={ props.InputDateondelay }>
      {
      props.children ?
      props.children :
      <>
        <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['hfive']?.animationClass || {}}>

          <div id="id_fourthreefour_onesixfivenigth" className={` frame hfive ${ props.onClick ? 'cursor' : '' } ${ transaction['hfive']?.type ? transaction['hfive']?.type.toLowerCase() : '' }`} style={ { ...{}, ...props.HFiveStyle , transitionDuration: transaction['hfive']?.duration, transitionTimingFunction: transaction['hfive']?.timingFunction } } onClick={ props.HFiveonClick } onMouseEnter={ props.HFiveonMouseEnter } onMouseOver={ props.HFiveonMouseOver } onKeyPress={ props.HFiveonKeyPress } onDrag={ props.HFiveonDrag } onMouseLeave={ props.HFiveonMouseLeave } onMouseUp={ props.HFiveonMouseUp } onMouseDown={ props.HFiveonMouseDown } onKeyDown={ props.HFiveonKeyDown } onChange={ props.HFiveonChange } ondelay={ props.HFiveondelay }>
            <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['hfivetext']?.animationClass || {}}>

              <span id="id_fourthreefour_onesixsixzero"  className={` text hfivetext    ${ props.onClick ? 'cursor' : ''}  ${ transaction['hfivetext']?.type ? transaction['hfivetext']?.type.toLowerCase() : '' }`} style={{ ...{"fontSize":"0.875rem"},  ...props.HfiveTextStyle , transitionDuration: transaction['hfivetext']?.duration, transitionTimingFunction: transaction['hfivetext']?.timingFunction }} onClick={ props.HfiveTextonClick } onMouseEnter={ props.HfiveTextonMouseEnter } onMouseOver={ props.HfiveTextonMouseOver } onKeyPress={ props.HfiveTextonKeyPress } onDrag={ props.HfiveTextonDrag } onMouseLeave={ props.HfiveTextonMouseLeave } onMouseUp={ props.HfiveTextonMouseUp } onMouseDown={ props.HfiveTextonMouseDown } onKeyDown={ props.HfiveTextonKeyDown } onChange={ props.HfiveTextonChange } ondelay={ props.HfiveTextondelay } >{props.HfiveText0 || `Día`}</span>

            </CSSTransition>
          </div>

        </CSSTransition>
        <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['deploygitinput']?.animationClass || {}}>
          <input id="id_fourthreefour_onesixsixnigth" className="deploygitinput   " onClick={ props.DeployGITInputonClick } onMouseEnter={ props.DeployGITInputonMouseEnter } onMouseOver={ props.DeployGITInputonMouseOver } onKeyPress={ props.DeployGITInputonKeyPress } onDrag={ props.DeployGITInputonDrag } onMouseLeave={ props.DeployGITInputonMouseLeave } onMouseUp={ props.DeployGITInputonMouseUp } onMouseDown={ props.DeployGITInputonMouseDown } onKeyDown={ props.DeployGITInputonKeyDown } onChange={ props.DeployGITInputonChange || function(e){ dispatchForm({ payload: { key: e.target.name, value: e.target.value } }) }} ondelay={ props.DeployGITInputondelay } style={ props.DeployGITInputStyle || {}} type="text" required="true" placeholder="Inserte el group id del su proyecto" value="GroupID" name="x_gitlab_id_group" value={globalStateForm['x_gitlab_id_group']} />
        </CSSTransition>

      </>
      }
    </div>

  </CSSTransition>
</>
    ) 
}

InputDate.propTypes = {
    style: PropTypes.any,
HfiveText0: PropTypes.any,
InputDateonClick: PropTypes.any,
InputDateonMouseEnter: PropTypes.any,
InputDateonMouseOver: PropTypes.any,
InputDateonKeyPress: PropTypes.any,
InputDateonDrag: PropTypes.any,
InputDateonMouseLeave: PropTypes.any,
InputDateonMouseUp: PropTypes.any,
InputDateonMouseDown: PropTypes.any,
InputDateonKeyDown: PropTypes.any,
InputDateonChange: PropTypes.any,
InputDateondelay: PropTypes.any,
HFiveonClick: PropTypes.any,
HFiveonMouseEnter: PropTypes.any,
HFiveonMouseOver: PropTypes.any,
HFiveonKeyPress: PropTypes.any,
HFiveonDrag: PropTypes.any,
HFiveonMouseLeave: PropTypes.any,
HFiveonMouseUp: PropTypes.any,
HFiveonMouseDown: PropTypes.any,
HFiveonKeyDown: PropTypes.any,
HFiveonChange: PropTypes.any,
HFiveondelay: PropTypes.any,
HfiveTextonClick: PropTypes.any,
HfiveTextonMouseEnter: PropTypes.any,
HfiveTextonMouseOver: PropTypes.any,
HfiveTextonKeyPress: PropTypes.any,
HfiveTextonDrag: PropTypes.any,
HfiveTextonMouseLeave: PropTypes.any,
HfiveTextonMouseUp: PropTypes.any,
HfiveTextonMouseDown: PropTypes.any,
HfiveTextonKeyDown: PropTypes.any,
HfiveTextonChange: PropTypes.any,
HfiveTextondelay: PropTypes.any,
DeployGITInputonClick: PropTypes.any,
DeployGITInputonMouseEnter: PropTypes.any,
DeployGITInputonMouseOver: PropTypes.any,
DeployGITInputonKeyPress: PropTypes.any,
DeployGITInputonDrag: PropTypes.any,
DeployGITInputonMouseLeave: PropTypes.any,
DeployGITInputonMouseUp: PropTypes.any,
DeployGITInputonMouseDown: PropTypes.any,
DeployGITInputonKeyDown: PropTypes.any,
DeployGITInputonChange: PropTypes.any,
DeployGITInputondelay: PropTypes.any
}
export default InputDate;