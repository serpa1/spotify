import React, { Suspense } from 'react';
import PropTypes from 'prop-types';
import * as Contexts  from 'Contexts';
import { OverlaySwapContext } from 'Components/OverlaySwapProvider';
import CorebooksAnimation from 'Components/CorebooksAnimation';
import CorebooksAnimationSingle from 'Components/CorebooksAnimationSingle';

import * as UtilsScripts  from 'utils/utils';
import { Route, useHistory, useParams } from "react-router-dom";
import SingletoneNavigation from 'Class/SingletoneNavigation';

import LoadingComponent from "Components/Loading";


import { Transition } from "react-transition-group";
import { CSSTransition } from "react-transition-group";
import { useAppContext, useSessionContext } from 'context/AppContext';
import './TextLinkonesixpx.css'





const TextLinkonesixpx = (props)=>{
    const [state,dispatch] = useAppContext();
    const [, setNameSelectedComponent] = React.useContext(OverlaySwapContext);
    
    const nodeRefpropertyonedefault = React.useRef(null);
const nodeRefpropertyonevarianttwoo = React.useRef(null);
    const history = useHistory();
    let RouterContext = useParams();
    
    
    
    
    
    
    const [globalStateForm, dispatchForm] = React.useState({});
    const [ variant, setvariant] = React.useState(props.variant || 'Property 1=Default')
    const [ _in, setIn ] = React.useState(false)
    const [ transaction, setTransaction ] = React.useState({}) 
    const [ animation, setAnimation ] = React.useState('')

    
    
    
    
    
    
    React.useEffect(()=>{
        
        
        
    },[]);
    
    
    React.useEffect(()=>{
        
        setIn(false);
        setTimeout(()=>setIn(true))
           
      },[variant]);
    const switchVariant = (value)=>{
        switch (value) {
            case 'Property 1=Default':{
            return (
            <>

  <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['propertyonedefault']?.animationClass || {}}>

    <div id="id_fourthreefour_twoofivefourfive" ref={nodeRefpropertyonedefault} className={` cursor ${ props.onClick ? 'cursor' : '' } textlinkonesixpxpropertyonedefault C_fourthreefour_twoofivefourfive ${ props.cssClass } ${ transaction['propertyonedefault']?.type ? transaction['propertyonedefault']?.type.toLowerCase() : '' }`} style={ { ...{ ...{}, transitionDuration: transaction['propertyonedefault']?.duration, transitionTimingFunction: transaction['propertyonedefault']?.timingFunction }, ...props.style }} onClick={ props.TextLinkonesixpxonClick } onMouseEnter={ props.TextLinkonesixpxonMouseEnter } onMouseOver={ props.TextLinkonesixpxonMouseOver || function(e){ setTransaction({ }); setvariant('Property 1=Variant2'); }} onKeyPress={ props.TextLinkonesixpxonKeyPress } onDrag={ props.TextLinkonesixpxonDrag } onMouseLeave={ props.TextLinkonesixpxonMouseLeave } onMouseUp={ props.TextLinkonesixpxonMouseUp } onMouseDown={ props.TextLinkonesixpxonMouseDown } onKeyDown={ props.TextLinkonesixpxonKeyDown } onChange={ props.TextLinkonesixpxonChange } ondelay={ props.TextLinkonesixpxondelay }>
      {
      props.children ?
      props.children :
      <>
        <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['subtilteonetext']?.animationClass || {}}>

          <span id="id_fourthreefour_twoofivethreesix"  className={` text textlinkonesixpxsubtilteonetext    ${ props.onClick ? 'cursor' : ''}  ${ transaction['subtilteonetext']?.type ? transaction['subtilteonetext']?.type.toLowerCase() : '' }`} style={{ ...{},  ...props.SubtilteoneTextStyle , transitionDuration: transaction['subtilteonetext']?.duration, transitionTimingFunction: transaction['subtilteonetext']?.timingFunction }} onClick={ props.SubtilteoneTextonClick } onMouseEnter={ props.SubtilteoneTextonMouseEnter } onMouseOver={ props.SubtilteoneTextonMouseOver } onKeyPress={ props.SubtilteoneTextonKeyPress } onDrag={ props.SubtilteoneTextonDrag } onMouseLeave={ props.SubtilteoneTextonMouseLeave } onMouseUp={ props.SubtilteoneTextonMouseUp } onMouseDown={ props.SubtilteoneTextonMouseDown } onKeyDown={ props.SubtilteoneTextonKeyDown } onChange={ props.SubtilteoneTextonChange } ondelay={ props.SubtilteoneTextondelay } >{props.SubtilteoneText0 || `¿Has olvidado tu contraseña?`}</span>

        </CSSTransition>

      </>
      }
    </div>

  </CSSTransition>

</>
            )
        }
case 'Property 1=Variant2':{
            return (
            <>

  <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['propertyonevarianttwoo']?.animationClass || {}}>

    <div id="id_fourthreeeight_onefivefiveone" ref={nodeRefpropertyonevarianttwoo} className={` cursor ${ props.onClick ? 'cursor' : '' } textlinkonesixpxpropertyonevarianttwoo C_fourthreeeight_onefivefiveone ${ props.cssClass } ${ transaction['propertyonevarianttwoo']?.type ? transaction['propertyonevarianttwoo']?.type.toLowerCase() : '' }`} style={ { ...{ ...{}, transitionDuration: transaction['propertyonevarianttwoo']?.duration, transitionTimingFunction: transaction['propertyonevarianttwoo']?.timingFunction }, ...props.style }} onClick={ props.TextLinkonesixpxonClick } onMouseEnter={ props.TextLinkonesixpxonMouseEnter } onMouseOver={ props.TextLinkonesixpxonMouseOver } onKeyPress={ props.TextLinkonesixpxonKeyPress } onDrag={ props.TextLinkonesixpxonDrag } onMouseLeave={ props.TextLinkonesixpxonMouseLeave || function(e){ setTransaction({ }); setvariant('Property 1=Default'); }} onMouseUp={ props.TextLinkonesixpxonMouseUp } onMouseDown={ props.TextLinkonesixpxonMouseDown } onKeyDown={ props.TextLinkonesixpxonKeyDown } onChange={ props.TextLinkonesixpxonChange } ondelay={ props.TextLinkonesixpxondelay }>
      {
      props.children ?
      props.children :
      <>
        <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['subtilteonetext']?.animationClass || {}}>

          <span id="id_fourthreeeight_onefivefivetwoo"  className={` text textlinkonesixpxsubtilteonetext    ${ props.onClick ? 'cursor' : ''}  ${ transaction['subtilteonetext']?.type ? transaction['subtilteonetext']?.type.toLowerCase() : '' }`} style={{ ...{},  ...props.SubtilteoneTextStyle , transitionDuration: transaction['subtilteonetext']?.duration, transitionTimingFunction: transaction['subtilteonetext']?.timingFunction }} onClick={ props.SubtilteoneTextonClick } onMouseEnter={ props.SubtilteoneTextonMouseEnter } onMouseOver={ props.SubtilteoneTextonMouseOver } onKeyPress={ props.SubtilteoneTextonKeyPress } onDrag={ props.SubtilteoneTextonDrag } onMouseLeave={ props.SubtilteoneTextonMouseLeave } onMouseUp={ props.SubtilteoneTextonMouseUp } onMouseDown={ props.SubtilteoneTextonMouseDown } onKeyDown={ props.SubtilteoneTextonKeyDown } onChange={ props.SubtilteoneTextonChange } ondelay={ props.SubtilteoneTextondelay } >{props.SubtilteoneText0 || `¿Has olvidado tu contraseña?`}</span>

        </CSSTransition>

      </>
      }
    </div>

  </CSSTransition>

</>
            )
        }
        }
    }

    return (
        <>
            {switchVariant(variant)}
        </>
    )
    
}

TextLinkonesixpx.propTypes = {
    style: PropTypes.any,
variant: PropTypes.any,
SubtilteoneText0: PropTypes.any,
TextLinkonesixpxonClick: PropTypes.any,
TextLinkonesixpxonMouseEnter: PropTypes.any,
TextLinkonesixpxonMouseOver: PropTypes.any,
TextLinkonesixpxonKeyPress: PropTypes.any,
TextLinkonesixpxonDrag: PropTypes.any,
TextLinkonesixpxonMouseLeave: PropTypes.any,
TextLinkonesixpxonMouseUp: PropTypes.any,
TextLinkonesixpxonMouseDown: PropTypes.any,
TextLinkonesixpxonKeyDown: PropTypes.any,
TextLinkonesixpxonChange: PropTypes.any,
TextLinkonesixpxondelay: PropTypes.any,
SubtilteoneTextonClick: PropTypes.any,
SubtilteoneTextonMouseEnter: PropTypes.any,
SubtilteoneTextonMouseOver: PropTypes.any,
SubtilteoneTextonKeyPress: PropTypes.any,
SubtilteoneTextonDrag: PropTypes.any,
SubtilteoneTextonMouseLeave: PropTypes.any,
SubtilteoneTextonMouseUp: PropTypes.any,
SubtilteoneTextonMouseDown: PropTypes.any,
SubtilteoneTextonKeyDown: PropTypes.any,
SubtilteoneTextonChange: PropTypes.any,
SubtilteoneTextondelay: PropTypes.any
}
export default TextLinkonesixpx;