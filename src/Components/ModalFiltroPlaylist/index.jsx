import React, { Suspense } from 'react';
import PropTypes from 'prop-types';
import * as Contexts  from 'Contexts';
import { OverlaySwapContext } from 'Components/OverlaySwapProvider';
import CorebooksAnimation from 'Components/CorebooksAnimation';
import CorebooksAnimationSingle from 'Components/CorebooksAnimationSingle';

import * as UtilsScripts  from 'utils/utils';
import { Route, useHistory, useParams } from "react-router-dom";
import SingletoneNavigation from 'Class/SingletoneNavigation';

import LoadingComponent from "Components/Loading";


import { Transition } from "react-transition-group";
import { CSSTransition } from "react-transition-group";
import { useAppContext, useSessionContext } from 'context/AppContext';
import './ModalFiltroPlaylist.css'





const ModalFiltroPlaylist = (props)=>{
    const [state,dispatch] = useAppContext();
    const [, setNameSelectedComponent] = React.useContext(OverlaySwapContext);
    
    const nodeRef = React.useRef(null);
    const history = useHistory();
    let RouterContext = useParams();
    
    
    
    
    
    
    const [globalStateForm, dispatchForm] = React.useState({});
    
    const [ _in, setIn ] = React.useState(false)
    const [ transaction, setTransaction ] = React.useState({}) 
    const [ animation, setAnimation ] = React.useState('')

    
    
    
    
    
    
    React.useEffect(()=>{
        
        
        
    },[]);
    
    
    return (
        <>
  <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['modalfiltroplaylist']?.animationClass || {}}>

    <div id="id_fourthreeeight_fiveonefourthree" ref={nodeRef} className={` ${ props.onClick ? 'cursor' : '' } modalfiltroplaylist ${ props.cssClass } ${ transaction['modalfiltroplaylist']?.type ? transaction['modalfiltroplaylist']?.type.toLowerCase() : '' }`} style={ { ...{ ...{}, transitionDuration: transaction['modalfiltroplaylist']?.duration, transitionTimingFunction: transaction['modalfiltroplaylist']?.timingFunction }, ...props.style }} onClick={ props.ModalFiltroPlaylistonClick } onMouseEnter={ props.ModalFiltroPlaylistonMouseEnter } onMouseOver={ props.ModalFiltroPlaylistonMouseOver } onKeyPress={ props.ModalFiltroPlaylistonKeyPress } onDrag={ props.ModalFiltroPlaylistonDrag } onMouseLeave={ props.ModalFiltroPlaylistonMouseLeave } onMouseUp={ props.ModalFiltroPlaylistonMouseUp } onMouseDown={ props.ModalFiltroPlaylistonMouseDown } onKeyDown={ props.ModalFiltroPlaylistonKeyDown } onChange={ props.ModalFiltroPlaylistonChange } ondelay={ props.ModalFiltroPlaylistondelay }>
      {
      props.children ?
      props.children :
      <>
        <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['framethreeeightnigth']?.animationClass || {}}>

          <div id="id_fourthreeeight_fiveonethreefour" className={` frame framethreeeightnigth ${ props.onClick ? 'cursor' : '' } ${ transaction['framethreeeightnigth']?.type ? transaction['framethreeeightnigth']?.type.toLowerCase() : '' }`} style={ { ...{}, ...props.FramethreeeightnigthStyle , transitionDuration: transaction['framethreeeightnigth']?.duration, transitionTimingFunction: transaction['framethreeeightnigth']?.timingFunction } } onClick={ props.FramethreeeightnigthonClick } onMouseEnter={ props.FramethreeeightnigthonMouseEnter } onMouseOver={ props.FramethreeeightnigthonMouseOver } onKeyPress={ props.FramethreeeightnigthonKeyPress } onDrag={ props.FramethreeeightnigthonDrag } onMouseLeave={ props.FramethreeeightnigthonMouseLeave } onMouseUp={ props.FramethreeeightnigthonMouseUp } onMouseDown={ props.FramethreeeightnigthonMouseDown } onKeyDown={ props.FramethreeeightnigthonKeyDown } onChange={ props.FramethreeeightnigthonChange } ondelay={ props.Framethreeeightnigthondelay }>
            <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['clasificarpor']?.animationClass || {}}>

              <span id="id_fourthreeeight_fiveonetwoonigth"  className={` text clasificarpor    ${ props.onClick ? 'cursor' : ''}  ${ transaction['clasificarpor']?.type ? transaction['clasificarpor']?.type.toLowerCase() : '' }`} style={{ ...{},  ...props.ClasificarporStyle , transitionDuration: transaction['clasificarpor']?.duration, transitionTimingFunction: transaction['clasificarpor']?.timingFunction }} onClick={ props.ClasificarporonClick } onMouseEnter={ props.ClasificarporonMouseEnter } onMouseOver={ props.ClasificarporonMouseOver } onKeyPress={ props.ClasificarporonKeyPress } onDrag={ props.ClasificarporonDrag } onMouseLeave={ props.ClasificarporonMouseLeave } onMouseUp={ props.ClasificarporonMouseUp } onMouseDown={ props.ClasificarporonMouseDown } onKeyDown={ props.ClasificarporonKeyDown } onChange={ props.ClasificarporonChange } ondelay={ props.Clasificarporondelay } >{props.Clasificarpor0 || `Clasificar por`}</span>

            </CSSTransition>
          </div>

        </CSSTransition>
        <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['framethreenigthzero']?.animationClass || {}}>

          <div id="id_fourthreeeight_fiveonethreefive" className={` frame framethreenigthzero ${ props.onClick ? 'cursor' : '' } ${ transaction['framethreenigthzero']?.type ? transaction['framethreenigthzero']?.type.toLowerCase() : '' }`} style={ { ...{}, ...props.FramethreenigthzeroStyle , transitionDuration: transaction['framethreenigthzero']?.duration, transitionTimingFunction: transaction['framethreenigthzero']?.timingFunction } } onClick={ props.FramethreenigthzeroonClick } onMouseEnter={ props.FramethreenigthzeroonMouseEnter } onMouseOver={ props.FramethreenigthzeroonMouseOver } onKeyPress={ props.FramethreenigthzeroonKeyPress } onDrag={ props.FramethreenigthzeroonDrag } onMouseLeave={ props.FramethreenigthzeroonMouseLeave } onMouseUp={ props.FramethreenigthzeroonMouseUp } onMouseDown={ props.FramethreenigthzeroonMouseDown } onKeyDown={ props.FramethreenigthzeroonKeyDown } onChange={ props.FramethreenigthzeroonChange } ondelay={ props.Framethreenigthzeroondelay }>
            <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['recientes']?.animationClass || {}}>

              <span id="id_fourthreeeight_fiveonethreesix"  className={` text recientes    ${ props.onClick ? 'cursor' : ''}  ${ transaction['recientes']?.type ? transaction['recientes']?.type.toLowerCase() : '' }`} style={{ ...{},  ...props.RecientesStyle , transitionDuration: transaction['recientes']?.duration, transitionTimingFunction: transaction['recientes']?.timingFunction }} onClick={ props.RecientesonClick } onMouseEnter={ props.RecientesonMouseEnter } onMouseOver={ props.RecientesonMouseOver } onKeyPress={ props.RecientesonKeyPress } onDrag={ props.RecientesonDrag } onMouseLeave={ props.RecientesonMouseLeave } onMouseUp={ props.RecientesonMouseUp } onMouseDown={ props.RecientesonMouseDown } onKeyDown={ props.RecientesonKeyDown } onChange={ props.RecientesonChange } ondelay={ props.Recientesondelay } >{props.Recientes0 || `Recientes`}</span>

            </CSSTransition>
          </div>

        </CSSTransition>
        <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['framethreenigthone']?.animationClass || {}}>

          <div id="id_fourthreeeight_fiveonethreeseven" className={` frame framethreenigthone ${ props.onClick ? 'cursor' : '' } ${ transaction['framethreenigthone']?.type ? transaction['framethreenigthone']?.type.toLowerCase() : '' }`} style={ { ...{}, ...props.FramethreenigthoneStyle , transitionDuration: transaction['framethreenigthone']?.duration, transitionTimingFunction: transaction['framethreenigthone']?.timingFunction } } onClick={ props.FramethreenigthoneonClick } onMouseEnter={ props.FramethreenigthoneonMouseEnter } onMouseOver={ props.FramethreenigthoneonMouseOver } onKeyPress={ props.FramethreenigthoneonKeyPress } onDrag={ props.FramethreenigthoneonDrag } onMouseLeave={ props.FramethreenigthoneonMouseLeave } onMouseUp={ props.FramethreenigthoneonMouseUp } onMouseDown={ props.FramethreenigthoneonMouseDown } onKeyDown={ props.FramethreenigthoneonKeyDown } onChange={ props.FramethreenigthoneonChange } ondelay={ props.Framethreenigthoneondelay }>
            <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['agregadosrecientemente']?.animationClass || {}}>

              <span id="id_fourthreeeight_fiveonethreeeight"  className={` text agregadosrecientemente    ${ props.onClick ? 'cursor' : ''}  ${ transaction['agregadosrecientemente']?.type ? transaction['agregadosrecientemente']?.type.toLowerCase() : '' }`} style={{ ...{},  ...props.AgregadosrecientementeStyle , transitionDuration: transaction['agregadosrecientemente']?.duration, transitionTimingFunction: transaction['agregadosrecientemente']?.timingFunction }} onClick={ props.AgregadosrecientementeonClick } onMouseEnter={ props.AgregadosrecientementeonMouseEnter } onMouseOver={ props.AgregadosrecientementeonMouseOver } onKeyPress={ props.AgregadosrecientementeonKeyPress } onDrag={ props.AgregadosrecientementeonDrag } onMouseLeave={ props.AgregadosrecientementeonMouseLeave } onMouseUp={ props.AgregadosrecientementeonMouseUp } onMouseDown={ props.AgregadosrecientementeonMouseDown } onKeyDown={ props.AgregadosrecientementeonKeyDown } onChange={ props.AgregadosrecientementeonChange } ondelay={ props.Agregadosrecientementeondelay } >{props.Agregadosrecientemente0 || `Agregados recientemente`}</span>

            </CSSTransition>
          </div>

        </CSSTransition>
        <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['framethreenigthtwoo']?.animationClass || {}}>

          <div id="id_fourthreeeight_fiveonethreenigth" className={` frame framethreenigthtwoo ${ props.onClick ? 'cursor' : '' } ${ transaction['framethreenigthtwoo']?.type ? transaction['framethreenigthtwoo']?.type.toLowerCase() : '' }`} style={ { ...{}, ...props.FramethreenigthtwooStyle , transitionDuration: transaction['framethreenigthtwoo']?.duration, transitionTimingFunction: transaction['framethreenigthtwoo']?.timingFunction } } onClick={ props.FramethreenigthtwooonClick } onMouseEnter={ props.FramethreenigthtwooonMouseEnter } onMouseOver={ props.FramethreenigthtwooonMouseOver } onKeyPress={ props.FramethreenigthtwooonKeyPress } onDrag={ props.FramethreenigthtwooonDrag } onMouseLeave={ props.FramethreenigthtwooonMouseLeave } onMouseUp={ props.FramethreenigthtwooonMouseUp } onMouseDown={ props.FramethreenigthtwooonMouseDown } onKeyDown={ props.FramethreenigthtwooonKeyDown } onChange={ props.FramethreenigthtwooonChange } ondelay={ props.Framethreenigthtwooondelay }>
            <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['alfabticamente']?.animationClass || {}}>

              <span id="id_fourthreeeight_fiveonefourzero"  className={` text alfabticamente    ${ props.onClick ? 'cursor' : ''}  ${ transaction['alfabticamente']?.type ? transaction['alfabticamente']?.type.toLowerCase() : '' }`} style={{ ...{},  ...props.AlfabticamenteStyle , transitionDuration: transaction['alfabticamente']?.duration, transitionTimingFunction: transaction['alfabticamente']?.timingFunction }} onClick={ props.AlfabticamenteonClick } onMouseEnter={ props.AlfabticamenteonMouseEnter } onMouseOver={ props.AlfabticamenteonMouseOver } onKeyPress={ props.AlfabticamenteonKeyPress } onDrag={ props.AlfabticamenteonDrag } onMouseLeave={ props.AlfabticamenteonMouseLeave } onMouseUp={ props.AlfabticamenteonMouseUp } onMouseDown={ props.AlfabticamenteonMouseDown } onKeyDown={ props.AlfabticamenteonKeyDown } onChange={ props.AlfabticamenteonChange } ondelay={ props.Alfabticamenteondelay } >{props.Alfabticamente0 || `Alfabéticamente`}</span>

            </CSSTransition>
          </div>

        </CSSTransition>
        <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['framethreenigththree']?.animationClass || {}}>

          <div id="id_fourthreeeight_fiveonefourone" className={` frame framethreenigththree ${ props.onClick ? 'cursor' : '' } ${ transaction['framethreenigththree']?.type ? transaction['framethreenigththree']?.type.toLowerCase() : '' }`} style={ { ...{}, ...props.FramethreenigththreeStyle , transitionDuration: transaction['framethreenigththree']?.duration, transitionTimingFunction: transaction['framethreenigththree']?.timingFunction } } onClick={ props.FramethreenigththreeonClick } onMouseEnter={ props.FramethreenigththreeonMouseEnter } onMouseOver={ props.FramethreenigththreeonMouseOver } onKeyPress={ props.FramethreenigththreeonKeyPress } onDrag={ props.FramethreenigththreeonDrag } onMouseLeave={ props.FramethreenigththreeonMouseLeave } onMouseUp={ props.FramethreenigththreeonMouseUp } onMouseDown={ props.FramethreenigththreeonMouseDown } onKeyDown={ props.FramethreenigththreeonKeyDown } onChange={ props.FramethreenigththreeonChange } ondelay={ props.Framethreenigththreeondelay }>
            <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['creador']?.animationClass || {}}>

              <span id="id_fourthreeeight_fiveonefourtwoo"  className={` text creador    ${ props.onClick ? 'cursor' : ''}  ${ transaction['creador']?.type ? transaction['creador']?.type.toLowerCase() : '' }`} style={{ ...{},  ...props.CreadorStyle , transitionDuration: transaction['creador']?.duration, transitionTimingFunction: transaction['creador']?.timingFunction }} onClick={ props.CreadoronClick } onMouseEnter={ props.CreadoronMouseEnter } onMouseOver={ props.CreadoronMouseOver } onKeyPress={ props.CreadoronKeyPress } onDrag={ props.CreadoronDrag } onMouseLeave={ props.CreadoronMouseLeave } onMouseUp={ props.CreadoronMouseUp } onMouseDown={ props.CreadoronMouseDown } onKeyDown={ props.CreadoronKeyDown } onChange={ props.CreadoronChange } ondelay={ props.Creadorondelay } >{props.Creador0 || `Creador`}</span>

            </CSSTransition>
          </div>

        </CSSTransition>

      </>
      }
    </div>

  </CSSTransition>
</>
    ) 
}

ModalFiltroPlaylist.propTypes = {
    style: PropTypes.any,
Clasificarpor0: PropTypes.any,
Recientes0: PropTypes.any,
Agregadosrecientemente0: PropTypes.any,
Alfabticamente0: PropTypes.any,
Creador0: PropTypes.any,
ModalFiltroPlaylistonClick: PropTypes.any,
ModalFiltroPlaylistonMouseEnter: PropTypes.any,
ModalFiltroPlaylistonMouseOver: PropTypes.any,
ModalFiltroPlaylistonKeyPress: PropTypes.any,
ModalFiltroPlaylistonDrag: PropTypes.any,
ModalFiltroPlaylistonMouseLeave: PropTypes.any,
ModalFiltroPlaylistonMouseUp: PropTypes.any,
ModalFiltroPlaylistonMouseDown: PropTypes.any,
ModalFiltroPlaylistonKeyDown: PropTypes.any,
ModalFiltroPlaylistonChange: PropTypes.any,
ModalFiltroPlaylistondelay: PropTypes.any,
FramethreeeightnigthonClick: PropTypes.any,
FramethreeeightnigthonMouseEnter: PropTypes.any,
FramethreeeightnigthonMouseOver: PropTypes.any,
FramethreeeightnigthonKeyPress: PropTypes.any,
FramethreeeightnigthonDrag: PropTypes.any,
FramethreeeightnigthonMouseLeave: PropTypes.any,
FramethreeeightnigthonMouseUp: PropTypes.any,
FramethreeeightnigthonMouseDown: PropTypes.any,
FramethreeeightnigthonKeyDown: PropTypes.any,
FramethreeeightnigthonChange: PropTypes.any,
Framethreeeightnigthondelay: PropTypes.any,
ClasificarporonClick: PropTypes.any,
ClasificarporonMouseEnter: PropTypes.any,
ClasificarporonMouseOver: PropTypes.any,
ClasificarporonKeyPress: PropTypes.any,
ClasificarporonDrag: PropTypes.any,
ClasificarporonMouseLeave: PropTypes.any,
ClasificarporonMouseUp: PropTypes.any,
ClasificarporonMouseDown: PropTypes.any,
ClasificarporonKeyDown: PropTypes.any,
ClasificarporonChange: PropTypes.any,
Clasificarporondelay: PropTypes.any,
FramethreenigthzeroonClick: PropTypes.any,
FramethreenigthzeroonMouseEnter: PropTypes.any,
FramethreenigthzeroonMouseOver: PropTypes.any,
FramethreenigthzeroonKeyPress: PropTypes.any,
FramethreenigthzeroonDrag: PropTypes.any,
FramethreenigthzeroonMouseLeave: PropTypes.any,
FramethreenigthzeroonMouseUp: PropTypes.any,
FramethreenigthzeroonMouseDown: PropTypes.any,
FramethreenigthzeroonKeyDown: PropTypes.any,
FramethreenigthzeroonChange: PropTypes.any,
Framethreenigthzeroondelay: PropTypes.any,
RecientesonClick: PropTypes.any,
RecientesonMouseEnter: PropTypes.any,
RecientesonMouseOver: PropTypes.any,
RecientesonKeyPress: PropTypes.any,
RecientesonDrag: PropTypes.any,
RecientesonMouseLeave: PropTypes.any,
RecientesonMouseUp: PropTypes.any,
RecientesonMouseDown: PropTypes.any,
RecientesonKeyDown: PropTypes.any,
RecientesonChange: PropTypes.any,
Recientesondelay: PropTypes.any,
FramethreenigthoneonClick: PropTypes.any,
FramethreenigthoneonMouseEnter: PropTypes.any,
FramethreenigthoneonMouseOver: PropTypes.any,
FramethreenigthoneonKeyPress: PropTypes.any,
FramethreenigthoneonDrag: PropTypes.any,
FramethreenigthoneonMouseLeave: PropTypes.any,
FramethreenigthoneonMouseUp: PropTypes.any,
FramethreenigthoneonMouseDown: PropTypes.any,
FramethreenigthoneonKeyDown: PropTypes.any,
FramethreenigthoneonChange: PropTypes.any,
Framethreenigthoneondelay: PropTypes.any,
AgregadosrecientementeonClick: PropTypes.any,
AgregadosrecientementeonMouseEnter: PropTypes.any,
AgregadosrecientementeonMouseOver: PropTypes.any,
AgregadosrecientementeonKeyPress: PropTypes.any,
AgregadosrecientementeonDrag: PropTypes.any,
AgregadosrecientementeonMouseLeave: PropTypes.any,
AgregadosrecientementeonMouseUp: PropTypes.any,
AgregadosrecientementeonMouseDown: PropTypes.any,
AgregadosrecientementeonKeyDown: PropTypes.any,
AgregadosrecientementeonChange: PropTypes.any,
Agregadosrecientementeondelay: PropTypes.any,
FramethreenigthtwooonClick: PropTypes.any,
FramethreenigthtwooonMouseEnter: PropTypes.any,
FramethreenigthtwooonMouseOver: PropTypes.any,
FramethreenigthtwooonKeyPress: PropTypes.any,
FramethreenigthtwooonDrag: PropTypes.any,
FramethreenigthtwooonMouseLeave: PropTypes.any,
FramethreenigthtwooonMouseUp: PropTypes.any,
FramethreenigthtwooonMouseDown: PropTypes.any,
FramethreenigthtwooonKeyDown: PropTypes.any,
FramethreenigthtwooonChange: PropTypes.any,
Framethreenigthtwooondelay: PropTypes.any,
AlfabticamenteonClick: PropTypes.any,
AlfabticamenteonMouseEnter: PropTypes.any,
AlfabticamenteonMouseOver: PropTypes.any,
AlfabticamenteonKeyPress: PropTypes.any,
AlfabticamenteonDrag: PropTypes.any,
AlfabticamenteonMouseLeave: PropTypes.any,
AlfabticamenteonMouseUp: PropTypes.any,
AlfabticamenteonMouseDown: PropTypes.any,
AlfabticamenteonKeyDown: PropTypes.any,
AlfabticamenteonChange: PropTypes.any,
Alfabticamenteondelay: PropTypes.any,
FramethreenigththreeonClick: PropTypes.any,
FramethreenigththreeonMouseEnter: PropTypes.any,
FramethreenigththreeonMouseOver: PropTypes.any,
FramethreenigththreeonKeyPress: PropTypes.any,
FramethreenigththreeonDrag: PropTypes.any,
FramethreenigththreeonMouseLeave: PropTypes.any,
FramethreenigththreeonMouseUp: PropTypes.any,
FramethreenigththreeonMouseDown: PropTypes.any,
FramethreenigththreeonKeyDown: PropTypes.any,
FramethreenigththreeonChange: PropTypes.any,
Framethreenigththreeondelay: PropTypes.any,
CreadoronClick: PropTypes.any,
CreadoronMouseEnter: PropTypes.any,
CreadoronMouseOver: PropTypes.any,
CreadoronKeyPress: PropTypes.any,
CreadoronDrag: PropTypes.any,
CreadoronMouseLeave: PropTypes.any,
CreadoronMouseUp: PropTypes.any,
CreadoronMouseDown: PropTypes.any,
CreadoronKeyDown: PropTypes.any,
CreadoronChange: PropTypes.any,
Creadorondelay: PropTypes.any
}
export default ModalFiltroPlaylist;