import React, { Suspense } from 'react';
import PropTypes from 'prop-types';
import * as Contexts  from 'Contexts';
import { OverlaySwapContext } from 'Components/OverlaySwapProvider';
import CorebooksAnimation from 'Components/CorebooksAnimation';
import CorebooksAnimationSingle from 'Components/CorebooksAnimationSingle';

import * as UtilsScripts  from 'utils/utils';
import { Route, useHistory, useParams } from "react-router-dom";
import SingletoneNavigation from 'Class/SingletoneNavigation';

import LoadingComponent from "Components/Loading";


import { Transition } from "react-transition-group";
import { CSSTransition } from "react-transition-group";
import { useAppContext, useSessionContext } from 'context/AppContext';
import './Imagetwoo.css'





const Imagetwoo = (props)=>{
    const [state,dispatch] = useAppContext();
    const [, setNameSelectedComponent] = React.useContext(OverlaySwapContext);
    
    const nodeRef = React.useRef(null);
    const history = useHistory();
    let RouterContext = useParams();
    
    
    
    
    
    
    const [globalStateForm, dispatchForm] = React.useState({});
    
    const [ _in, setIn ] = React.useState(false)
    const [ transaction, setTransaction ] = React.useState({}) 
    const [ animation, setAnimation ] = React.useState('')

    
    
    
    
    
    
    React.useEffect(()=>{
        
        
        
    },[]);
    
    
    return (
        <>
  <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['imagetwoo']?.animationClass || {}}>

    <div id="id_fourthreeeight_onethreethreeeight" ref={nodeRef} className={` ${ props.onClick ? 'cursor' : '' } imagetwoo ${ props.cssClass } ${ transaction['imagetwoo']?.type ? transaction['imagetwoo']?.type.toLowerCase() : '' }`} style={ { ...{ ...{}, transitionDuration: transaction['imagetwoo']?.duration, transitionTimingFunction: transaction['imagetwoo']?.timingFunction }, ...props.style }} onClick={ props.ImagetwooonClick } onMouseEnter={ props.ImagetwooonMouseEnter } onMouseOver={ props.ImagetwooonMouseOver } onKeyPress={ props.ImagetwooonKeyPress } onDrag={ props.ImagetwooonDrag } onMouseLeave={ props.ImagetwooonMouseLeave } onMouseUp={ props.ImagetwooonMouseUp } onMouseDown={ props.ImagetwooonMouseDown } onKeyDown={ props.ImagetwooonKeyDown } onChange={ props.ImagetwooonChange } ondelay={ props.Imagetwooondelay }>
      {
      props.children ?
      props.children :
      <>


      </>
      }
    </div>

  </CSSTransition>
</>
    ) 
}

Imagetwoo.propTypes = {
    style: PropTypes.any,
ImagetwooonClick: PropTypes.any,
ImagetwooonMouseEnter: PropTypes.any,
ImagetwooonMouseOver: PropTypes.any,
ImagetwooonKeyPress: PropTypes.any,
ImagetwooonDrag: PropTypes.any,
ImagetwooonMouseLeave: PropTypes.any,
ImagetwooonMouseUp: PropTypes.any,
ImagetwooonMouseDown: PropTypes.any,
ImagetwooonKeyDown: PropTypes.any,
ImagetwooonChange: PropTypes.any,
Imagetwooondelay: PropTypes.any
}
export default Imagetwoo;