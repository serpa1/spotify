import React, { Suspense } from 'react';
import PropTypes from 'prop-types';
import * as Contexts  from 'Contexts';
import { OverlaySwapContext } from 'Components/OverlaySwapProvider';
import CorebooksAnimation from 'Components/CorebooksAnimation';
import CorebooksAnimationSingle from 'Components/CorebooksAnimationSingle';

import * as UtilsScripts  from 'utils/utils';
import { Route, useHistory, useParams } from "react-router-dom";
import SingletoneNavigation from 'Class/SingletoneNavigation';

import LoadingComponent from "Components/Loading";


import { Transition } from "react-transition-group";
import { CSSTransition } from "react-transition-group";
import { useAppContext, useSessionContext } from 'context/AppContext';
import './Rectanglefour.css'





const Rectanglefour = (props)=>{
    const [state,dispatch] = useAppContext();
    const [, setNameSelectedComponent] = React.useContext(OverlaySwapContext);
    
    const nodeRefpropertyonedefault = React.useRef(null);
const nodeRefpropertyonevarianttwoo = React.useRef(null);
    const history = useHistory();
    let RouterContext = useParams();
    
    
    
    
    
    
    const [globalStateForm, dispatchForm] = React.useState({});
    const [ variant, setvariant] = React.useState(props.variant || 'Property 1=Default')
    const [ _in, setIn ] = React.useState(false)
    const [ transaction, setTransaction ] = React.useState({}) 
    const [ animation, setAnimation ] = React.useState('')

    
    
    
    
    
    
    React.useEffect(()=>{
        
        
        
    },[]);
    
    
    React.useEffect(()=>{
        
        setIn(false);
        setTimeout(()=>setIn(true))
           
      },[variant]);
    const switchVariant = (value)=>{
        switch (value) {
            case 'Property 1=Default':{
            return (
            <>

  <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['propertyonedefault']?.animationClass || {}}>

    <div id="id_fourthreeeight_onethreefourtwoo" ref={nodeRefpropertyonedefault} className={` cursor ${ props.onClick ? 'cursor' : '' } rectanglefourpropertyonedefault C_fourthreeeight_onethreefourtwoo ${ props.cssClass } ${ transaction['propertyonedefault']?.type ? transaction['propertyonedefault']?.type.toLowerCase() : '' }`} style={ { ...{ ...{}, transitionDuration: transaction['propertyonedefault']?.duration, transitionTimingFunction: transaction['propertyonedefault']?.timingFunction }, ...props.style }} onClick={ props.RectanglefouronClick || function(e){ setTransaction({ }); setvariant('Property 1=Variant2'); }} onMouseEnter={ props.RectanglefouronMouseEnter } onMouseOver={ props.RectanglefouronMouseOver } onKeyPress={ props.RectanglefouronKeyPress } onDrag={ props.RectanglefouronDrag } onMouseLeave={ props.RectanglefouronMouseLeave } onMouseUp={ props.RectanglefouronMouseUp } onMouseDown={ props.RectanglefouronMouseDown } onKeyDown={ props.RectanglefouronKeyDown } onChange={ props.RectanglefouronChange } ondelay={ props.Rectanglefourondelay }>
      {
      props.children ?
      props.children :
      <>
        <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['rectanglefour']?.animationClass || {}}>
          <img id="id_fourthreeeight_onethreefourzero" className={` rectangle rectanglefourrectanglefour ${ props.onClick ? 'cursor' : '' } ${ transaction['rectanglefour']?.type ? transaction['rectanglefour']?.type.toLowerCase() : '' }`} style={{ ...{},  ...props.RectanglefourStyle , transitionDuration: transaction['rectanglefour']?.duration, transitionTimingFunction: transaction['rectanglefour']?.timingFunction }} onClick={ props.RectanglefouronClick } onMouseEnter={ props.RectanglefouronMouseEnter } onMouseOver={ props.RectanglefouronMouseOver } onKeyPress={ props.RectanglefouronKeyPress } onDrag={ props.RectanglefouronDrag } onMouseLeave={ props.RectanglefouronMouseLeave } onMouseUp={ props.RectanglefouronMouseUp } onMouseDown={ props.RectanglefouronMouseDown } onKeyDown={ props.RectanglefouronKeyDown } onChange={ props.RectanglefouronChange } ondelay={ props.Rectanglefourondelay } src={props.Rectanglefour0 || "https://cdn.uncodie.com/tACQBrKCV97e0EqPE1XNLJ/6d6ac5d9aa3245a73df4a181a99bd5865dc268ee.png" } />
        </CSSTransition>

      </>
      }
    </div>

  </CSSTransition>

</>
            )
        }
case 'Property 1=Variant2':{
            return (
            <>

  <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['propertyonevarianttwoo']?.animationClass || {}}>

    <div id="id_fourthreeeight_onefoursevenone" ref={nodeRefpropertyonevarianttwoo} className={` cursor ${ props.onClick ? 'cursor' : '' } rectanglefourpropertyonevarianttwoo C_fourthreeeight_onefoursevenone ${ props.cssClass } ${ transaction['propertyonevarianttwoo']?.type ? transaction['propertyonevarianttwoo']?.type.toLowerCase() : '' }`} style={ { ...{ ...{}, transitionDuration: transaction['propertyonevarianttwoo']?.duration, transitionTimingFunction: transaction['propertyonevarianttwoo']?.timingFunction }, ...props.style }} onClick={ props.RectanglefouronClick || function(e){ setTransaction({ }); setvariant('Property 1=Default'); }} onMouseEnter={ props.RectanglefouronMouseEnter } onMouseOver={ props.RectanglefouronMouseOver } onKeyPress={ props.RectanglefouronKeyPress } onDrag={ props.RectanglefouronDrag } onMouseLeave={ props.RectanglefouronMouseLeave } onMouseUp={ props.RectanglefouronMouseUp } onMouseDown={ props.RectanglefouronMouseDown } onKeyDown={ props.RectanglefouronKeyDown } onChange={ props.RectanglefouronChange } ondelay={ props.Rectanglefourondelay }>
      {
      props.children ?
      props.children :
      <>
        <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['rectanglefour']?.animationClass || {}}>
          <img id="id_fourthreeeight_onefourseventwoo" className={` rectangle rectanglefourrectanglefour ${ props.onClick ? 'cursor' : '' } ${ transaction['rectanglefour']?.type ? transaction['rectanglefour']?.type.toLowerCase() : '' }`} style={{ ...{},  ...props.RectanglefourStyle , transitionDuration: transaction['rectanglefour']?.duration, transitionTimingFunction: transaction['rectanglefour']?.timingFunction }} onClick={ props.RectanglefouronClick } onMouseEnter={ props.RectanglefouronMouseEnter } onMouseOver={ props.RectanglefouronMouseOver } onKeyPress={ props.RectanglefouronKeyPress } onDrag={ props.RectanglefouronDrag } onMouseLeave={ props.RectanglefouronMouseLeave } onMouseUp={ props.RectanglefouronMouseUp } onMouseDown={ props.RectanglefouronMouseDown } onKeyDown={ props.RectanglefouronKeyDown } onChange={ props.RectanglefouronChange } ondelay={ props.Rectanglefourondelay } src={props.Rectanglefour0 || "https://cdn.uncodie.com/tACQBrKCV97e0EqPE1XNLJ/a3724415ecffbe1b9b02b5604aff256e3c41bc58.png" } />
        </CSSTransition>

      </>
      }
    </div>

  </CSSTransition>

</>
            )
        }
        }
    }

    return (
        <>
            {switchVariant(variant)}
        </>
    )
    
}

Rectanglefour.propTypes = {
    style: PropTypes.any,
variant: PropTypes.any,
Rectanglefour0: PropTypes.any,
RectanglefouronClick: PropTypes.any,
RectanglefouronMouseEnter: PropTypes.any,
RectanglefouronMouseOver: PropTypes.any,
RectanglefouronKeyPress: PropTypes.any,
RectanglefouronDrag: PropTypes.any,
RectanglefouronMouseLeave: PropTypes.any,
RectanglefouronMouseUp: PropTypes.any,
RectanglefouronMouseDown: PropTypes.any,
RectanglefouronKeyDown: PropTypes.any,
RectanglefouronChange: PropTypes.any,
Rectanglefourondelay: PropTypes.any
}
export default Rectanglefour;