import React, { Suspense } from 'react';
import PropTypes from 'prop-types';
import * as Contexts  from 'Contexts';
import { OverlaySwapContext } from 'Components/OverlaySwapProvider';
import CorebooksAnimation from 'Components/CorebooksAnimation';
import CorebooksAnimationSingle from 'Components/CorebooksAnimationSingle';

import * as UtilsScripts  from 'utils/utils';
import { Route, useHistory, useParams } from "react-router-dom";
import SingletoneNavigation from 'Class/SingletoneNavigation';

import LoadingComponent from "Components/Loading";


import { Transition } from "react-transition-group";
import { CSSTransition } from "react-transition-group";
import { useAppContext, useSessionContext } from 'context/AppContext';
import './TextLinkGreen.css'





const TextLinkGreen = (props)=>{
    const [state,dispatch] = useAppContext();
    const [, setNameSelectedComponent] = React.useContext(OverlaySwapContext);
    
    const nodeRefpropertyonedefault = React.useRef(null);
const nodeRefpropertyonevarianttwoo = React.useRef(null);
    const history = useHistory();
    let RouterContext = useParams();
    
    
    
    
    
    
    const [globalStateForm, dispatchForm] = React.useState({});
    const [ variant, setvariant] = React.useState(props.variant || 'Property 1=Default')
    const [ _in, setIn ] = React.useState(false)
    const [ transaction, setTransaction ] = React.useState({}) 
    const [ animation, setAnimation ] = React.useState('')

    
    
    
    
    
    
    React.useEffect(()=>{
        
        
        
    },[]);
    
    
    React.useEffect(()=>{
        
        setIn(false);
        setTimeout(()=>setIn(true))
           
      },[variant]);
    const switchVariant = (value)=>{
        switch (value) {
            case 'Property 1=Default':{
            return (
            <>

  <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['propertyonedefault']?.animationClass || {}}>

    <div id="id_fivesixnigth_twootwoonigththree" ref={nodeRefpropertyonedefault} className={` cursor ${ props.onClick ? 'cursor' : '' } textlinkgreenpropertyonedefault C_fivesixnigth_twootwoonigththree ${ props.cssClass } ${ transaction['propertyonedefault']?.type ? transaction['propertyonedefault']?.type.toLowerCase() : '' }`} style={ { ...{ ...{}, transitionDuration: transaction['propertyonedefault']?.duration, transitionTimingFunction: transaction['propertyonedefault']?.timingFunction }, ...props.style }} onClick={ props.TextLinkGreenonClick } onMouseEnter={ props.TextLinkGreenonMouseEnter } onMouseOver={ props.TextLinkGreenonMouseOver || function(e){ setTransaction({ }); setvariant('Property 1=Variant2'); }} onKeyPress={ props.TextLinkGreenonKeyPress } onDrag={ props.TextLinkGreenonDrag } onMouseLeave={ props.TextLinkGreenonMouseLeave } onMouseUp={ props.TextLinkGreenonMouseUp } onMouseDown={ props.TextLinkGreenonMouseDown } onKeyDown={ props.TextLinkGreenonKeyDown } onChange={ props.TextLinkGreenonChange } ondelay={ props.TextLinkGreenondelay }>
      {
      props.children ?
      props.children :
      <>
        <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['playstation']?.animationClass || {}}>

          <span id="id_fivesixnigth_twootwoonigthtwoo"  className={` text textlinkgreenplaystation    ${ props.onClick ? 'cursor' : ''}  ${ transaction['playstation']?.type ? transaction['playstation']?.type.toLowerCase() : '' }`} style={{ ...{},  ...props.PLAYSTATIONStyle , transitionDuration: transaction['playstation']?.duration, transitionTimingFunction: transaction['playstation']?.timingFunction }} onClick={ props.PLAYSTATIONonClick } onMouseEnter={ props.PLAYSTATIONonMouseEnter } onMouseOver={ props.PLAYSTATIONonMouseOver } onKeyPress={ props.PLAYSTATIONonKeyPress } onDrag={ props.PLAYSTATIONonDrag } onMouseLeave={ props.PLAYSTATIONonMouseLeave } onMouseUp={ props.PLAYSTATIONonMouseUp } onMouseDown={ props.PLAYSTATIONonMouseDown } onKeyDown={ props.PLAYSTATIONonKeyDown } onChange={ props.PLAYSTATIONonChange } ondelay={ props.PLAYSTATIONondelay } >{props.PLAYSTATION0 || `PLAYSTATION`}</span>

        </CSSTransition>
        <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['framefourtwooseven']?.animationClass || {}}>

          <div id="id_fivesixnigth_twootwooeightseven" className={` frame textlinkgreenframefourtwooseven ${ props.onClick ? 'cursor' : '' } ${ transaction['framefourtwooseven']?.type ? transaction['framefourtwooseven']?.type.toLowerCase() : '' }`} style={ { ...{}, ...props.FramefourtwoosevenStyle , transitionDuration: transaction['framefourtwooseven']?.duration, transitionTimingFunction: transaction['framefourtwooseven']?.timingFunction } } onClick={ props.FramefourtwoosevenonClick } onMouseEnter={ props.FramefourtwoosevenonMouseEnter } onMouseOver={ props.FramefourtwoosevenonMouseOver } onKeyPress={ props.FramefourtwoosevenonKeyPress } onDrag={ props.FramefourtwoosevenonDrag } onMouseLeave={ props.FramefourtwoosevenonMouseLeave } onMouseUp={ props.FramefourtwoosevenonMouseUp } onMouseDown={ props.FramefourtwoosevenonMouseDown } onKeyDown={ props.FramefourtwoosevenonKeyDown } onChange={ props.FramefourtwoosevenonChange } ondelay={ props.Framefourtwoosevenondelay }>
            <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['emptyname']?.animationClass || {}}>

              <span id="id_fivesixnigth_twootwoosevensix"  className={` text textlinkgreenemptyname    ${ props.onClick ? 'cursor' : ''}  ${ transaction['emptyname']?.type ? transaction['emptyname']?.type.toLowerCase() : '' }`} style={{ ...{},  ...props.emptyNameStyle , transitionDuration: transaction['emptyname']?.duration, transitionTimingFunction: transaction['emptyname']?.timingFunction }} onClick={ props.emptyNameonClick } onMouseEnter={ props.emptyNameonMouseEnter } onMouseOver={ props.emptyNameonMouseOver } onKeyPress={ props.emptyNameonKeyPress } onDrag={ props.emptyNameonDrag } onMouseLeave={ props.emptyNameonMouseLeave } onMouseUp={ props.emptyNameonMouseUp } onMouseDown={ props.emptyNameonMouseDown } onKeyDown={ props.emptyNameonKeyDown } onChange={ props.emptyNameonChange } ondelay={ props.emptyNameondelay } >{props.emptyName0 || `®`}</span>

            </CSSTransition>
            <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['rectangletwoozero']?.animationClass || {}}>
              <div id="id_fivesixnigth_twootwooeightsix" className={` rectangle textlinkgreenrectangletwoozero ${ props.onClick ? 'cursor' : '' } ${ transaction['rectangletwoozero']?.type ? transaction['rectangletwoozero']?.type.toLowerCase() : '' }`} style={{ ...{},  ...props.RectangletwoozeroStyle , transitionDuration: transaction['rectangletwoozero']?.duration, transitionTimingFunction: transaction['rectangletwoozero']?.timingFunction }} onClick={ props.RectangletwoozeroonClick } onMouseEnter={ props.RectangletwoozeroonMouseEnter } onMouseOver={ props.RectangletwoozeroonMouseOver } onKeyPress={ props.RectangletwoozeroonKeyPress } onDrag={ props.RectangletwoozeroonDrag } onMouseLeave={ props.RectangletwoozeroonMouseLeave } onMouseUp={ props.RectangletwoozeroonMouseUp } onMouseDown={ props.RectangletwoozeroonMouseDown } onKeyDown={ props.RectangletwoozeroonKeyDown } onChange={ props.RectangletwoozeroonChange } ondelay={ props.Rectangletwoozeroondelay }></div>
            </CSSTransition>
          </div>

        </CSSTransition>

      </>
      }
    </div>

  </CSSTransition>

</>
            )
        }
case 'Property 1=Variant2':{
            return (
            <>

  <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['propertyonevarianttwoo']?.animationClass || {}}>

    <div id="id_fivesixnigth_twootwoonigthfive" ref={nodeRefpropertyonevarianttwoo} className={` cursor ${ props.onClick ? 'cursor' : '' } textlinkgreenpropertyonevarianttwoo C_fivesixnigth_twootwoonigthfive ${ props.cssClass } ${ transaction['propertyonevarianttwoo']?.type ? transaction['propertyonevarianttwoo']?.type.toLowerCase() : '' }`} style={ { ...{ ...{}, transitionDuration: transaction['propertyonevarianttwoo']?.duration, transitionTimingFunction: transaction['propertyonevarianttwoo']?.timingFunction }, ...props.style }} onClick={ props.TextLinkGreenonClick } onMouseEnter={ props.TextLinkGreenonMouseEnter } onMouseOver={ props.TextLinkGreenonMouseOver } onKeyPress={ props.TextLinkGreenonKeyPress } onDrag={ props.TextLinkGreenonDrag } onMouseLeave={ props.TextLinkGreenonMouseLeave || function(e){ setTransaction({ }); setvariant('Property 1=Default'); }} onMouseUp={ props.TextLinkGreenonMouseUp } onMouseDown={ props.TextLinkGreenonMouseDown } onKeyDown={ props.TextLinkGreenonKeyDown } onChange={ props.TextLinkGreenonChange } ondelay={ props.TextLinkGreenondelay }>
      {
      props.children ?
      props.children :
      <>
        <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['playstation']?.animationClass || {}}>

          <span id="id_fivesixnigth_twootwoonigthsix"  className={` text textlinkgreenplaystation    ${ props.onClick ? 'cursor' : ''}  ${ transaction['playstation']?.type ? transaction['playstation']?.type.toLowerCase() : '' }`} style={{ ...{},  ...props.PLAYSTATIONStyle , transitionDuration: transaction['playstation']?.duration, transitionTimingFunction: transaction['playstation']?.timingFunction }} onClick={ props.PLAYSTATIONonClick } onMouseEnter={ props.PLAYSTATIONonMouseEnter } onMouseOver={ props.PLAYSTATIONonMouseOver } onKeyPress={ props.PLAYSTATIONonKeyPress } onDrag={ props.PLAYSTATIONonDrag } onMouseLeave={ props.PLAYSTATIONonMouseLeave } onMouseUp={ props.PLAYSTATIONonMouseUp } onMouseDown={ props.PLAYSTATIONonMouseDown } onKeyDown={ props.PLAYSTATIONonKeyDown } onChange={ props.PLAYSTATIONonChange } ondelay={ props.PLAYSTATIONondelay } >{props.PLAYSTATION0 || `PLAYSTATION`}</span>

        </CSSTransition>
        <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['framefourtwooseven']?.animationClass || {}}>

          <div id="id_fivesixnigth_twootwoonigthseven" className={` frame textlinkgreenframefourtwooseven ${ props.onClick ? 'cursor' : '' } ${ transaction['framefourtwooseven']?.type ? transaction['framefourtwooseven']?.type.toLowerCase() : '' }`} style={ { ...{}, ...props.FramefourtwoosevenStyle , transitionDuration: transaction['framefourtwooseven']?.duration, transitionTimingFunction: transaction['framefourtwooseven']?.timingFunction } } onClick={ props.FramefourtwoosevenonClick } onMouseEnter={ props.FramefourtwoosevenonMouseEnter } onMouseOver={ props.FramefourtwoosevenonMouseOver } onKeyPress={ props.FramefourtwoosevenonKeyPress } onDrag={ props.FramefourtwoosevenonDrag } onMouseLeave={ props.FramefourtwoosevenonMouseLeave } onMouseUp={ props.FramefourtwoosevenonMouseUp } onMouseDown={ props.FramefourtwoosevenonMouseDown } onKeyDown={ props.FramefourtwoosevenonKeyDown } onChange={ props.FramefourtwoosevenonChange } ondelay={ props.Framefourtwoosevenondelay }>
            <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['emptyname']?.animationClass || {}}>

              <span id="id_fivesixnigth_twootwoonigtheight"  className={` text textlinkgreenemptyname    ${ props.onClick ? 'cursor' : ''}  ${ transaction['emptyname']?.type ? transaction['emptyname']?.type.toLowerCase() : '' }`} style={{ ...{},  ...props.emptyNameStyle , transitionDuration: transaction['emptyname']?.duration, transitionTimingFunction: transaction['emptyname']?.timingFunction }} onClick={ props.emptyNameonClick } onMouseEnter={ props.emptyNameonMouseEnter } onMouseOver={ props.emptyNameonMouseOver } onKeyPress={ props.emptyNameonKeyPress } onDrag={ props.emptyNameonDrag } onMouseLeave={ props.emptyNameonMouseLeave } onMouseUp={ props.emptyNameonMouseUp } onMouseDown={ props.emptyNameonMouseDown } onKeyDown={ props.emptyNameonKeyDown } onChange={ props.emptyNameonChange } ondelay={ props.emptyNameondelay } >{props.emptyName0 || `®`}</span>

            </CSSTransition>
            <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['rectangletwoozero']?.animationClass || {}}>
              <div id="id_fivesixnigth_twootwoonigthnigth" className={` rectangle textlinkgreenrectangletwoozero ${ props.onClick ? 'cursor' : '' } ${ transaction['rectangletwoozero']?.type ? transaction['rectangletwoozero']?.type.toLowerCase() : '' }`} style={{ ...{},  ...props.RectangletwoozeroStyle , transitionDuration: transaction['rectangletwoozero']?.duration, transitionTimingFunction: transaction['rectangletwoozero']?.timingFunction }} onClick={ props.RectangletwoozeroonClick } onMouseEnter={ props.RectangletwoozeroonMouseEnter } onMouseOver={ props.RectangletwoozeroonMouseOver } onKeyPress={ props.RectangletwoozeroonKeyPress } onDrag={ props.RectangletwoozeroonDrag } onMouseLeave={ props.RectangletwoozeroonMouseLeave } onMouseUp={ props.RectangletwoozeroonMouseUp } onMouseDown={ props.RectangletwoozeroonMouseDown } onKeyDown={ props.RectangletwoozeroonKeyDown } onChange={ props.RectangletwoozeroonChange } ondelay={ props.Rectangletwoozeroondelay }></div>
            </CSSTransition>
          </div>

        </CSSTransition>

      </>
      }
    </div>

  </CSSTransition>

</>
            )
        }
        }
    }

    return (
        <>
            {switchVariant(variant)}
        </>
    )
    
}

TextLinkGreen.propTypes = {
    style: PropTypes.any,
variant: PropTypes.any,
PLAYSTATION0: PropTypes.any,
emptyName0: PropTypes.any,
Rectangletwoozero0: PropTypes.any,
TextLinkGreenonClick: PropTypes.any,
TextLinkGreenonMouseEnter: PropTypes.any,
TextLinkGreenonMouseOver: PropTypes.any,
TextLinkGreenonKeyPress: PropTypes.any,
TextLinkGreenonDrag: PropTypes.any,
TextLinkGreenonMouseLeave: PropTypes.any,
TextLinkGreenonMouseUp: PropTypes.any,
TextLinkGreenonMouseDown: PropTypes.any,
TextLinkGreenonKeyDown: PropTypes.any,
TextLinkGreenonChange: PropTypes.any,
TextLinkGreenondelay: PropTypes.any,
PLAYSTATIONonClick: PropTypes.any,
PLAYSTATIONonMouseEnter: PropTypes.any,
PLAYSTATIONonMouseOver: PropTypes.any,
PLAYSTATIONonKeyPress: PropTypes.any,
PLAYSTATIONonDrag: PropTypes.any,
PLAYSTATIONonMouseLeave: PropTypes.any,
PLAYSTATIONonMouseUp: PropTypes.any,
PLAYSTATIONonMouseDown: PropTypes.any,
PLAYSTATIONonKeyDown: PropTypes.any,
PLAYSTATIONonChange: PropTypes.any,
PLAYSTATIONondelay: PropTypes.any,
FramefourtwoosevenonClick: PropTypes.any,
FramefourtwoosevenonMouseEnter: PropTypes.any,
FramefourtwoosevenonMouseOver: PropTypes.any,
FramefourtwoosevenonKeyPress: PropTypes.any,
FramefourtwoosevenonDrag: PropTypes.any,
FramefourtwoosevenonMouseLeave: PropTypes.any,
FramefourtwoosevenonMouseUp: PropTypes.any,
FramefourtwoosevenonMouseDown: PropTypes.any,
FramefourtwoosevenonKeyDown: PropTypes.any,
FramefourtwoosevenonChange: PropTypes.any,
Framefourtwoosevenondelay: PropTypes.any,
emptyNameonClick: PropTypes.any,
emptyNameonMouseEnter: PropTypes.any,
emptyNameonMouseOver: PropTypes.any,
emptyNameonKeyPress: PropTypes.any,
emptyNameonDrag: PropTypes.any,
emptyNameonMouseLeave: PropTypes.any,
emptyNameonMouseUp: PropTypes.any,
emptyNameonMouseDown: PropTypes.any,
emptyNameonKeyDown: PropTypes.any,
emptyNameonChange: PropTypes.any,
emptyNameondelay: PropTypes.any,
RectangletwoozeroonClick: PropTypes.any,
RectangletwoozeroonMouseEnter: PropTypes.any,
RectangletwoozeroonMouseOver: PropTypes.any,
RectangletwoozeroonKeyPress: PropTypes.any,
RectangletwoozeroonDrag: PropTypes.any,
RectangletwoozeroonMouseLeave: PropTypes.any,
RectangletwoozeroonMouseUp: PropTypes.any,
RectangletwoozeroonMouseDown: PropTypes.any,
RectangletwoozeroonKeyDown: PropTypes.any,
RectangletwoozeroonChange: PropTypes.any,
Rectangletwoozeroondelay: PropTypes.any
}
export default TextLinkGreen;