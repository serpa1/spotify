import React, { Suspense } from 'react';
import PropTypes from 'prop-types';
import * as Contexts  from 'Contexts';
import { OverlaySwapContext } from 'Components/OverlaySwapProvider';
import CorebooksAnimation from 'Components/CorebooksAnimation';
import CorebooksAnimationSingle from 'Components/CorebooksAnimationSingle';

import * as UtilsScripts  from 'utils/utils';
import { Route, useHistory, useParams } from "react-router-dom";
import SingletoneNavigation from 'Class/SingletoneNavigation';

import LoadingComponent from "Components/Loading";


import { Transition } from "react-transition-group";
import { CSSTransition } from "react-transition-group";
import { useAppContext, useSessionContext } from 'context/AppContext';
import './Framefourzeroeight.css'





const Framefourzeroeight = (props)=>{
    const [state,dispatch] = useAppContext();
    const [, setNameSelectedComponent] = React.useContext(OverlaySwapContext);
    
    const nodeRef = React.useRef(null);
    const history = useHistory();
    let RouterContext = useParams();
    
    
    
    
    
    
    const [globalStateForm, dispatchForm] = React.useState({});
    
    const [ _in, setIn ] = React.useState(false)
    const [ transaction, setTransaction ] = React.useState({}) 
    const [ animation, setAnimation ] = React.useState('')

    
    
    
    
    
    
    React.useEffect(()=>{
        
        
        
    },[]);
    
    
    return (
        <>
  <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['framefourzeroeight']?.animationClass || {}}>

    <div id="id_fivefivenigth_twoozerofourfive" ref={nodeRef} className={` ${ props.onClick ? 'cursor' : '' } framefourzeroeight ${ props.cssClass } ${ transaction['framefourzeroeight']?.type ? transaction['framefourzeroeight']?.type.toLowerCase() : '' }`} style={ { ...{ ...{}, transitionDuration: transaction['framefourzeroeight']?.duration, transitionTimingFunction: transaction['framefourzeroeight']?.timingFunction }, ...props.style }} onClick={ props.FramefourzeroeightonClick } onMouseEnter={ props.FramefourzeroeightonMouseEnter } onMouseOver={ props.FramefourzeroeightonMouseOver } onKeyPress={ props.FramefourzeroeightonKeyPress } onDrag={ props.FramefourzeroeightonDrag } onMouseLeave={ props.FramefourzeroeightonMouseLeave } onMouseUp={ props.FramefourzeroeightonMouseUp } onMouseDown={ props.FramefourzeroeightonMouseDown } onKeyDown={ props.FramefourzeroeightonKeyDown } onChange={ props.FramefourzeroeightonChange } ondelay={ props.Framefourzeroeightondelay }>
      {
      props.children ?
      props.children :
      <>
        <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['rectangleonefour']?.animationClass || {}}>
          <img id="id_fivefivenigth_twoozerofourtwoo" className={` rectangle rectangleonefour ${ props.onClick ? 'cursor' : '' } ${ transaction['rectangleonefour']?.type ? transaction['rectangleonefour']?.type.toLowerCase() : '' }`} style={{ ...{},  ...props.RectangleonefourStyle , transitionDuration: transaction['rectangleonefour']?.duration, transitionTimingFunction: transaction['rectangleonefour']?.timingFunction }} onClick={ props.RectangleonefouronClick } onMouseEnter={ props.RectangleonefouronMouseEnter } onMouseOver={ props.RectangleonefouronMouseOver } onKeyPress={ props.RectangleonefouronKeyPress } onDrag={ props.RectangleonefouronDrag } onMouseLeave={ props.RectangleonefouronMouseLeave } onMouseUp={ props.RectangleonefouronMouseUp } onMouseDown={ props.RectangleonefouronMouseDown } onKeyDown={ props.RectangleonefouronKeyDown } onChange={ props.RectangleonefouronChange } ondelay={ props.Rectangleonefourondelay } src={props.Rectangleonefour0 || "https://cdn.uncodie.com/tACQBrKCV97e0EqPE1XNLJ/51ea1c978619648dd8daaa749683d661acd6b539.png" } />
        </CSSTransition>
        <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['rectangleonefive']?.animationClass || {}}>
          <img id="id_fivefivenigth_twoozerofourthree" className={` rectangle rectangleonefive ${ props.onClick ? 'cursor' : '' } ${ transaction['rectangleonefive']?.type ? transaction['rectangleonefive']?.type.toLowerCase() : '' }`} style={{ ...{},  ...props.RectangleonefiveStyle , transitionDuration: transaction['rectangleonefive']?.duration, transitionTimingFunction: transaction['rectangleonefive']?.timingFunction }} onClick={ props.RectangleonefiveonClick } onMouseEnter={ props.RectangleonefiveonMouseEnter } onMouseOver={ props.RectangleonefiveonMouseOver } onKeyPress={ props.RectangleonefiveonKeyPress } onDrag={ props.RectangleonefiveonDrag } onMouseLeave={ props.RectangleonefiveonMouseLeave } onMouseUp={ props.RectangleonefiveonMouseUp } onMouseDown={ props.RectangleonefiveonMouseDown } onKeyDown={ props.RectangleonefiveonKeyDown } onChange={ props.RectangleonefiveonChange } ondelay={ props.Rectangleonefiveondelay } src={props.Rectangleonefive0 || "https://cdn.uncodie.com/tACQBrKCV97e0EqPE1XNLJ/02d3f90340ebbac79c44204e5053f2a131dbe420.png" } />
        </CSSTransition>
        <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['rectangleonesix']?.animationClass || {}}>
          <img id="id_fivefivenigth_twoozerofourfour" className={` rectangle rectangleonesix ${ props.onClick ? 'cursor' : '' } ${ transaction['rectangleonesix']?.type ? transaction['rectangleonesix']?.type.toLowerCase() : '' }`} style={{ ...{},  ...props.RectangleonesixStyle , transitionDuration: transaction['rectangleonesix']?.duration, transitionTimingFunction: transaction['rectangleonesix']?.timingFunction }} onClick={ props.RectangleonesixonClick } onMouseEnter={ props.RectangleonesixonMouseEnter } onMouseOver={ props.RectangleonesixonMouseOver } onKeyPress={ props.RectangleonesixonKeyPress } onDrag={ props.RectangleonesixonDrag } onMouseLeave={ props.RectangleonesixonMouseLeave } onMouseUp={ props.RectangleonesixonMouseUp } onMouseDown={ props.RectangleonesixonMouseDown } onKeyDown={ props.RectangleonesixonKeyDown } onChange={ props.RectangleonesixonChange } ondelay={ props.Rectangleonesixondelay } src={props.Rectangleonesix0 || "https://cdn.uncodie.com/tACQBrKCV97e0EqPE1XNLJ/f34c2c772bd25da7b91747e7d6d38e2529bd76ee.png" } />
        </CSSTransition>

      </>
      }
    </div>

  </CSSTransition>
</>
    ) 
}

Framefourzeroeight.propTypes = {
    style: PropTypes.any,
Rectangleonefour0: PropTypes.any,
Rectangleonefive0: PropTypes.any,
Rectangleonesix0: PropTypes.any,
FramefourzeroeightonClick: PropTypes.any,
FramefourzeroeightonMouseEnter: PropTypes.any,
FramefourzeroeightonMouseOver: PropTypes.any,
FramefourzeroeightonKeyPress: PropTypes.any,
FramefourzeroeightonDrag: PropTypes.any,
FramefourzeroeightonMouseLeave: PropTypes.any,
FramefourzeroeightonMouseUp: PropTypes.any,
FramefourzeroeightonMouseDown: PropTypes.any,
FramefourzeroeightonKeyDown: PropTypes.any,
FramefourzeroeightonChange: PropTypes.any,
Framefourzeroeightondelay: PropTypes.any,
RectangleonefouronClick: PropTypes.any,
RectangleonefouronMouseEnter: PropTypes.any,
RectangleonefouronMouseOver: PropTypes.any,
RectangleonefouronKeyPress: PropTypes.any,
RectangleonefouronDrag: PropTypes.any,
RectangleonefouronMouseLeave: PropTypes.any,
RectangleonefouronMouseUp: PropTypes.any,
RectangleonefouronMouseDown: PropTypes.any,
RectangleonefouronKeyDown: PropTypes.any,
RectangleonefouronChange: PropTypes.any,
Rectangleonefourondelay: PropTypes.any,
RectangleonefiveonClick: PropTypes.any,
RectangleonefiveonMouseEnter: PropTypes.any,
RectangleonefiveonMouseOver: PropTypes.any,
RectangleonefiveonKeyPress: PropTypes.any,
RectangleonefiveonDrag: PropTypes.any,
RectangleonefiveonMouseLeave: PropTypes.any,
RectangleonefiveonMouseUp: PropTypes.any,
RectangleonefiveonMouseDown: PropTypes.any,
RectangleonefiveonKeyDown: PropTypes.any,
RectangleonefiveonChange: PropTypes.any,
Rectangleonefiveondelay: PropTypes.any,
RectangleonesixonClick: PropTypes.any,
RectangleonesixonMouseEnter: PropTypes.any,
RectangleonesixonMouseOver: PropTypes.any,
RectangleonesixonKeyPress: PropTypes.any,
RectangleonesixonDrag: PropTypes.any,
RectangleonesixonMouseLeave: PropTypes.any,
RectangleonesixonMouseUp: PropTypes.any,
RectangleonesixonMouseDown: PropTypes.any,
RectangleonesixonKeyDown: PropTypes.any,
RectangleonesixonChange: PropTypes.any,
Rectangleonesixondelay: PropTypes.any
}
export default Framefourzeroeight;