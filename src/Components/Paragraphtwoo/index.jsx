import React, { Suspense } from 'react';
import PropTypes from 'prop-types';
import * as Contexts  from 'Contexts';
import { OverlaySwapContext } from 'Components/OverlaySwapProvider';
import CorebooksAnimation from 'Components/CorebooksAnimation';
import CorebooksAnimationSingle from 'Components/CorebooksAnimationSingle';

import * as UtilsScripts  from 'utils/utils';
import { Route, useHistory, useParams } from "react-router-dom";
import SingletoneNavigation from 'Class/SingletoneNavigation';

import LoadingComponent from "Components/Loading";


import { Transition } from "react-transition-group";
import { CSSTransition } from "react-transition-group";
import { useAppContext, useSessionContext } from 'context/AppContext';
import './Paragraphtwoo.css'





const Paragraphtwoo = (props)=>{
    const [state,dispatch] = useAppContext();
    const [, setNameSelectedComponent] = React.useContext(OverlaySwapContext);
    
    const nodeRef = React.useRef(null);
    const history = useHistory();
    let RouterContext = useParams();
    
    
    
    
    
    
    const [globalStateForm, dispatchForm] = React.useState({});
    
    const [ _in, setIn ] = React.useState(false)
    const [ transaction, setTransaction ] = React.useState({}) 
    const [ animation, setAnimation ] = React.useState('')

    
    
    
    
    
    
    React.useEffect(()=>{
        
        
        
    },[]);
    
    
    return (
        <>
  <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['paragraphtwoo']?.animationClass || {}}>

    <div id="id_oneseven_twoosixfour" ref={nodeRef} className={` ${ props.onClick ? 'cursor' : '' } paragraphtwoo C_oneseven_twoosixfour ${ props.cssClass } ${ transaction['paragraphtwoo']?.type ? transaction['paragraphtwoo']?.type.toLowerCase() : '' }`} style={ { ...{ ...{}, transitionDuration: transaction['paragraphtwoo']?.duration, transitionTimingFunction: transaction['paragraphtwoo']?.timingFunction }, ...props.style }} onClick={ props.ParagraphtwooonClick } onMouseEnter={ props.ParagraphtwooonMouseEnter } onMouseOver={ props.ParagraphtwooonMouseOver } onKeyPress={ props.ParagraphtwooonKeyPress } onDrag={ props.ParagraphtwooonDrag } onMouseLeave={ props.ParagraphtwooonMouseLeave } onMouseUp={ props.ParagraphtwooonMouseUp } onMouseDown={ props.ParagraphtwooonMouseDown } onKeyDown={ props.ParagraphtwooonKeyDown } onChange={ props.ParagraphtwooonChange } ondelay={ props.Paragraphtwooondelay }>
      {
      props.children ?
      props.children :
      <>
        <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['paragraphtwootext']?.animationClass || {}}>

          <span id="id_oneseven_twoosixthree"  className={` text paragraphtwootext    ${ props.onClick ? 'cursor' : ''}  ${ transaction['paragraphtwootext']?.type ? transaction['paragraphtwootext']?.type.toLowerCase() : '' }`} style={{ ...{},  ...props.ParagraphtwooTextStyle , transitionDuration: transaction['paragraphtwootext']?.duration, transitionTimingFunction: transaction['paragraphtwootext']?.timingFunction }} onClick={ props.ParagraphtwooTextonClick } onMouseEnter={ props.ParagraphtwooTextonMouseEnter } onMouseOver={ props.ParagraphtwooTextonMouseOver } onKeyPress={ props.ParagraphtwooTextonKeyPress } onDrag={ props.ParagraphtwooTextonDrag } onMouseLeave={ props.ParagraphtwooTextonMouseLeave } onMouseUp={ props.ParagraphtwooTextonMouseUp } onMouseDown={ props.ParagraphtwooTextonMouseDown } onKeyDown={ props.ParagraphtwooTextonKeyDown } onChange={ props.ParagraphtwooTextonChange } ondelay={ props.ParagraphtwooTextondelay } >{props.ParagraphtwooText0 || `Peacefull Piano`}</span>

        </CSSTransition>

      </>
      }
    </div>

  </CSSTransition>
</>
    ) 
}

Paragraphtwoo.propTypes = {
    style: PropTypes.any,
ParagraphtwooText0: PropTypes.any,
ParagraphtwooonClick: PropTypes.any,
ParagraphtwooonMouseEnter: PropTypes.any,
ParagraphtwooonMouseOver: PropTypes.any,
ParagraphtwooonKeyPress: PropTypes.any,
ParagraphtwooonDrag: PropTypes.any,
ParagraphtwooonMouseLeave: PropTypes.any,
ParagraphtwooonMouseUp: PropTypes.any,
ParagraphtwooonMouseDown: PropTypes.any,
ParagraphtwooonKeyDown: PropTypes.any,
ParagraphtwooonChange: PropTypes.any,
Paragraphtwooondelay: PropTypes.any,
ParagraphtwooTextonClick: PropTypes.any,
ParagraphtwooTextonMouseEnter: PropTypes.any,
ParagraphtwooTextonMouseOver: PropTypes.any,
ParagraphtwooTextonKeyPress: PropTypes.any,
ParagraphtwooTextonDrag: PropTypes.any,
ParagraphtwooTextonMouseLeave: PropTypes.any,
ParagraphtwooTextonMouseUp: PropTypes.any,
ParagraphtwooTextonMouseDown: PropTypes.any,
ParagraphtwooTextonKeyDown: PropTypes.any,
ParagraphtwooTextonChange: PropTypes.any,
ParagraphtwooTextondelay: PropTypes.any
}
export default Paragraphtwoo;