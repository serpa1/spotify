import React, { Suspense } from 'react';
import PropTypes from 'prop-types';
import * as Contexts  from 'Contexts';
import { OverlaySwapContext } from 'Components/OverlaySwapProvider';
import CorebooksAnimation from 'Components/CorebooksAnimation';
import CorebooksAnimationSingle from 'Components/CorebooksAnimationSingle';

import * as UtilsScripts  from 'utils/utils';
import { Route, useHistory, useParams } from "react-router-dom";
import SingletoneNavigation from 'Class/SingletoneNavigation';

import LoadingComponent from "Components/Loading";


import { Transition } from "react-transition-group";
import { CSSTransition } from "react-transition-group";
import { useAppContext, useSessionContext } from 'context/AppContext';
import './Framefour.css'





const Framefour = (props)=>{
    const [state,dispatch] = useAppContext();
    const [, setNameSelectedComponent] = React.useContext(OverlaySwapContext);
    
    const nodeRef = React.useRef(null);
    const history = useHistory();
    let RouterContext = useParams();
    
    
    
    
    
    
    const [globalStateForm, dispatchForm] = React.useState({});
    
    const [ _in, setIn ] = React.useState(false)
    const [ transaction, setTransaction ] = React.useState({}) 
    const [ animation, setAnimation ] = React.useState('')

    
    
    
    
    
    
    React.useEffect(()=>{
        
        
        
    },[]);
    
    
    return (
        <>
  <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['framefour']?.animationClass || {}}>

    <div id="id_onesixnigth_nigtheightsix" ref={nodeRef} className={` ${ props.onClick ? 'cursor' : '' } framefour ${ props.cssClass } ${ transaction['framefour']?.type ? transaction['framefour']?.type.toLowerCase() : '' }`} style={ { ...{ ...{}, transitionDuration: transaction['framefour']?.duration, transitionTimingFunction: transaction['framefour']?.timingFunction }, ...props.style }} onClick={ props.FramefouronClick } onMouseEnter={ props.FramefouronMouseEnter } onMouseOver={ props.FramefouronMouseOver } onKeyPress={ props.FramefouronKeyPress } onDrag={ props.FramefouronDrag } onMouseLeave={ props.FramefouronMouseLeave } onMouseUp={ props.FramefouronMouseUp } onMouseDown={ props.FramefouronMouseDown } onKeyDown={ props.FramefouronKeyDown } onChange={ props.FramefouronChange } ondelay={ props.Framefourondelay }>
      {
      props.children ?
      props.children :
      <>
        <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['vector']?.animationClass || {}}>
          <svg id="id_onesixnigth_nigtheightseven" className="vector  " style={{}} onClick={ props.VectoronClick } onMouseEnter={ props.VectoronMouseEnter } onMouseOver={ props.VectoronMouseOver } onKeyPress={ props.VectoronKeyPress } onDrag={ props.VectoronDrag } onMouseLeave={ props.VectoronMouseLeave } onMouseUp={ props.VectoronMouseUp } onMouseDown={ props.VectoronMouseDown } onKeyDown={ props.VectoronKeyDown } onChange={ props.VectoronChange } ondelay={ props.Vectorondelay } width="16" height="15.49951171875">
            <path d="M7.21127 0.75076C7.21092 0.602306 7.25459 0.457097 7.33673 0.33357C7.41887 0.210043 7.53578 0.113771 7.67261 0.0569776C7.80944 0.000184264 7.96002 -0.0145684 8.10523 0.0145928C8.25044 0.043754 8.38373 0.115514 8.48818 0.22076L16 7.74076L8.48818 15.2618C8.36748 15.3912 8.20498 15.4735 8.02948 15.4943C7.85398 15.5151 7.67681 15.473 7.52934 15.3753C7.38188 15.2777 7.27365 15.1309 7.22383 14.961C7.17401 14.791 7.18582 14.6088 7.25716 14.4468C7.29817 14.3549 7.3572 14.2723 7.43074 14.2038L13.1369 8.48976L0.786813 8.48976C0.685442 8.49501 0.584065 8.47953 0.488852 8.44426C0.393639 8.409 0.306581 8.35467 0.232976 8.28461C0.159371 8.21454 0.100759 8.13019 0.0607068 8.03669C0.0206546 7.9432 2.76886e-17 7.84251 0 7.74076C-7.61436e-17 7.63901 0.0206546 7.53832 0.0607068 7.44483C0.100759 7.35133 0.159371 7.26698 0.232976 7.19691C0.306581 7.12685 0.393639 7.07253 0.488852 7.03726C0.584065 7.00199 0.685442 6.98651 0.786813 6.99176L13.1369 6.99176L7.43074 1.27976C7.3612 1.21022 7.30602 1.12761 7.26836 1.03666C7.2307 0.945721 7.2113 0.849227 7.21127 0.75076Z" />
          </svg>
        </CSSTransition>

      </>
      }
    </div>

  </CSSTransition>
</>
    ) 
}

Framefour.propTypes = {
    style: PropTypes.any,
FramefouronClick: PropTypes.any,
FramefouronMouseEnter: PropTypes.any,
FramefouronMouseOver: PropTypes.any,
FramefouronKeyPress: PropTypes.any,
FramefouronDrag: PropTypes.any,
FramefouronMouseLeave: PropTypes.any,
FramefouronMouseUp: PropTypes.any,
FramefouronMouseDown: PropTypes.any,
FramefouronKeyDown: PropTypes.any,
FramefouronChange: PropTypes.any,
Framefourondelay: PropTypes.any,
VectoronClick: PropTypes.any,
VectoronMouseEnter: PropTypes.any,
VectoronMouseOver: PropTypes.any,
VectoronKeyPress: PropTypes.any,
VectoronDrag: PropTypes.any,
VectoronMouseLeave: PropTypes.any,
VectoronMouseUp: PropTypes.any,
VectoronMouseDown: PropTypes.any,
VectoronKeyDown: PropTypes.any,
VectoronChange: PropTypes.any,
Vectorondelay: PropTypes.any
}
export default Framefour;