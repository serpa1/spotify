import React, { Suspense } from 'react';
import PropTypes from 'prop-types';
import * as Contexts  from 'Contexts';
import { OverlaySwapContext } from 'Components/OverlaySwapProvider';
import CorebooksAnimation from 'Components/CorebooksAnimation';
import CorebooksAnimationSingle from 'Components/CorebooksAnimationSingle';

import * as UtilsScripts  from 'utils/utils';
import { Route, useHistory, useParams } from "react-router-dom";
import SingletoneNavigation from 'Class/SingletoneNavigation';

import LoadingComponent from "Components/Loading";


import { Transition } from "react-transition-group";
import { CSSTransition } from "react-transition-group";
import { useAppContext, useSessionContext } from 'context/AppContext';
import './InputLogin.css'





const InputLogin = (props)=>{
    const [state,dispatch] = useAppContext();
    const [, setNameSelectedComponent] = React.useContext(OverlaySwapContext);
    
    const nodeRef = React.useRef(null);
    const history = useHistory();
    let RouterContext = useParams();
    
    
    
    const DeploymentContainerContext = React.useContext(Contexts.DeploymentContainer)
    
    
    const [globalStateForm, dispatchForm] = React.useState({});
    
    const [ _in, setIn ] = React.useState(false)
    const [ transaction, setTransaction ] = React.useState({}) 
    const [ animation, setAnimation ] = React.useState('')

    
    
    
    
    
    
    React.useEffect(()=>{
        
        
        
    },[]);
    
    
    return (
        <>
  <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['inputlogin']?.animationClass || {}}>

    <div id="id_fourthreefour_twoothreefivezero" ref={nodeRef} className={` ${ props.onClick ? 'cursor' : '' } inputlogin C_fourthreefour_twoothreefivezero ${ props.cssClass } ${ transaction['inputlogin']?.type ? transaction['inputlogin']?.type.toLowerCase() : '' }`} style={ { ...{ ...{}, transitionDuration: transaction['inputlogin']?.duration, transitionTimingFunction: transaction['inputlogin']?.timingFunction }, ...props.style }} onClick={ props.InputLoginonClick } onMouseEnter={ props.InputLoginonMouseEnter } onMouseOver={ props.InputLoginonMouseOver } onKeyPress={ props.InputLoginonKeyPress } onDrag={ props.InputLoginonDrag } onMouseLeave={ props.InputLoginonMouseLeave } onMouseUp={ props.InputLoginonMouseUp } onMouseDown={ props.InputLoginonMouseDown } onKeyDown={ props.InputLoginonKeyDown } onChange={ props.InputLoginonChange } ondelay={ props.InputLoginondelay }>
      {
      props.children ?
      props.children :
      <>
        <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['deploygitlabel']?.animationClass || {}}>

          <div id="id_fourthreefour_twoothreefourzero" className={` frame deploygitlabel ${ props.onClick ? 'cursor' : '' } ${ transaction['deploygitlabel']?.type ? transaction['deploygitlabel']?.type.toLowerCase() : '' }`} style={ { ...{}, ...props.DeployGITLabelStyle , transitionDuration: transaction['deploygitlabel']?.duration, transitionTimingFunction: transaction['deploygitlabel']?.timingFunction } } onClick={ props.DeployGITLabelonClick } onMouseEnter={ props.DeployGITLabelonMouseEnter } onMouseOver={ props.DeployGITLabelonMouseOver } onKeyPress={ props.DeployGITLabelonKeyPress } onDrag={ props.DeployGITLabelonDrag } onMouseLeave={ props.DeployGITLabelonMouseLeave } onMouseUp={ props.DeployGITLabelonMouseUp } onMouseDown={ props.DeployGITLabelonMouseDown } onKeyDown={ props.DeployGITLabelonKeyDown } onChange={ props.DeployGITLabelonChange } ondelay={ props.DeployGITLabelondelay }>
            <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['deploygitlabeltext']?.animationClass || {}}>

              <span id="id_fourthreefour_twoothreefourone"  className={` text deploygitlabeltext    ${ props.onClick ? 'cursor' : ''}  ${ transaction['deploygitlabeltext']?.type ? transaction['deploygitlabeltext']?.type.toLowerCase() : '' }`} style={{ ...{},  ...props.DeployGITLabelTextStyle , transitionDuration: transaction['deploygitlabeltext']?.duration, transitionTimingFunction: transaction['deploygitlabeltext']?.timingFunction }} onClick={ props.DeployGITLabelTextonClick } onMouseEnter={ props.DeployGITLabelTextonMouseEnter } onMouseOver={ props.DeployGITLabelTextonMouseOver } onKeyPress={ props.DeployGITLabelTextonKeyPress } onDrag={ props.DeployGITLabelTextonDrag } onMouseLeave={ props.DeployGITLabelTextonMouseLeave } onMouseUp={ props.DeployGITLabelTextonMouseUp } onMouseDown={ props.DeployGITLabelTextonMouseDown } onKeyDown={ props.DeployGITLabelTextonKeyDown } onChange={ props.DeployGITLabelTextonChange } ondelay={ props.DeployGITLabelTextondelay } >{props.DeployGITLabelText0 || `Contraseña`}</span>

            </CSSTransition>
          </div>

        </CSSTransition>
        <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['deploygitinput']?.animationClass || {}}>
          <input id="id_fourthreefour_twoothreefourtwoo" className="deploygitinput   " onClick={ props.DeployGITInputonClick } onMouseEnter={ props.DeployGITInputonMouseEnter } onMouseOver={ props.DeployGITInputonMouseOver } onKeyPress={ props.DeployGITInputonKeyPress } onDrag={ props.DeployGITInputonDrag } onMouseLeave={ props.DeployGITInputonMouseLeave } onMouseUp={ props.DeployGITInputonMouseUp } onMouseDown={ props.DeployGITInputonMouseDown } onKeyDown={ props.DeployGITInputonKeyDown } onChange={ props.DeployGITInputonChange || function(e){ dispatchForm({ payload: { key: e.target.name, value: e.target.value } }) }} ondelay={ props.DeployGITInputondelay } style={ props.DeployGITInputStyle || {}} type="text" required="true" placeholder="Inserte el group id del su proyecto" value="GroupID" name="x_gitlab_id_group" value={globalStateForm['x_gitlab_id_group']} />
        </CSSTransition>

      </>
      }
    </div>

  </CSSTransition>
</>
    ) 
}

InputLogin.propTypes = {
    style: PropTypes.any,
DeployGITLabelText0: PropTypes.any,
InputLoginonClick: PropTypes.any,
InputLoginonMouseEnter: PropTypes.any,
InputLoginonMouseOver: PropTypes.any,
InputLoginonKeyPress: PropTypes.any,
InputLoginonDrag: PropTypes.any,
InputLoginonMouseLeave: PropTypes.any,
InputLoginonMouseUp: PropTypes.any,
InputLoginonMouseDown: PropTypes.any,
InputLoginonKeyDown: PropTypes.any,
InputLoginonChange: PropTypes.any,
InputLoginondelay: PropTypes.any,
DeployGITLabelonClick: PropTypes.any,
DeployGITLabelonMouseEnter: PropTypes.any,
DeployGITLabelonMouseOver: PropTypes.any,
DeployGITLabelonKeyPress: PropTypes.any,
DeployGITLabelonDrag: PropTypes.any,
DeployGITLabelonMouseLeave: PropTypes.any,
DeployGITLabelonMouseUp: PropTypes.any,
DeployGITLabelonMouseDown: PropTypes.any,
DeployGITLabelonKeyDown: PropTypes.any,
DeployGITLabelonChange: PropTypes.any,
DeployGITLabelondelay: PropTypes.any,
DeployGITLabelTextonClick: PropTypes.any,
DeployGITLabelTextonMouseEnter: PropTypes.any,
DeployGITLabelTextonMouseOver: PropTypes.any,
DeployGITLabelTextonKeyPress: PropTypes.any,
DeployGITLabelTextonDrag: PropTypes.any,
DeployGITLabelTextonMouseLeave: PropTypes.any,
DeployGITLabelTextonMouseUp: PropTypes.any,
DeployGITLabelTextonMouseDown: PropTypes.any,
DeployGITLabelTextonKeyDown: PropTypes.any,
DeployGITLabelTextonChange: PropTypes.any,
DeployGITLabelTextondelay: PropTypes.any,
DeployGITInputonClick: PropTypes.any,
DeployGITInputonMouseEnter: PropTypes.any,
DeployGITInputonMouseOver: PropTypes.any,
DeployGITInputonKeyPress: PropTypes.any,
DeployGITInputonDrag: PropTypes.any,
DeployGITInputonMouseLeave: PropTypes.any,
DeployGITInputonMouseUp: PropTypes.any,
DeployGITInputonMouseDown: PropTypes.any,
DeployGITInputonKeyDown: PropTypes.any,
DeployGITInputonChange: PropTypes.any,
DeployGITInputondelay: PropTypes.any
}
export default InputLogin;