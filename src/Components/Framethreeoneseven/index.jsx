import React, { Suspense } from 'react';
import PropTypes from 'prop-types';
import * as Contexts  from 'Contexts';
import { OverlaySwapContext } from 'Components/OverlaySwapProvider';
import CorebooksAnimation from 'Components/CorebooksAnimation';
import CorebooksAnimationSingle from 'Components/CorebooksAnimationSingle';

import * as UtilsScripts  from 'utils/utils';
import { Route, useHistory, useParams } from "react-router-dom";
import SingletoneNavigation from 'Class/SingletoneNavigation';

import LoadingComponent from "Components/Loading";


import { Transition } from "react-transition-group";
import { CSSTransition } from "react-transition-group";
import { useAppContext, useSessionContext } from 'context/AppContext';
import './Framethreeoneseven.css'





const Framethreeoneseven = (props)=>{
    const [state,dispatch] = useAppContext();
    const [, setNameSelectedComponent] = React.useContext(OverlaySwapContext);
    
    const nodeRef = React.useRef(null);
    const history = useHistory();
    let RouterContext = useParams();
    
    
    
    
    
    
    const [globalStateForm, dispatchForm] = React.useState({});
    
    const [ _in, setIn ] = React.useState(false)
    const [ transaction, setTransaction ] = React.useState({}) 
    const [ animation, setAnimation ] = React.useState('')

    
    
    
    
    
    
    React.useEffect(()=>{
        
        
        
    },[]);
    
    
    return (
        <>
  <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['framethreeoneseven']?.animationClass || {}}>

    <div id="id_fourthreethree_onezerofivetwoo" ref={nodeRef} className={` ${ props.onClick ? 'cursor' : '' } framethreeoneseven C_fourthreethree_onezerofivetwoo ${ props.cssClass } ${ transaction['framethreeoneseven']?.type ? transaction['framethreeoneseven']?.type.toLowerCase() : '' }`} style={ { ...{ ...{}, transitionDuration: transaction['framethreeoneseven']?.duration, transitionTimingFunction: transaction['framethreeoneseven']?.timingFunction }, ...props.style }} onClick={ props.FramethreeonesevenonClick } onMouseEnter={ props.FramethreeonesevenonMouseEnter } onMouseOver={ props.FramethreeonesevenonMouseOver } onKeyPress={ props.FramethreeonesevenonKeyPress } onDrag={ props.FramethreeonesevenonDrag } onMouseLeave={ props.FramethreeonesevenonMouseLeave } onMouseUp={ props.FramethreeonesevenonMouseUp } onMouseDown={ props.FramethreeonesevenonMouseDown } onKeyDown={ props.FramethreeonesevenonKeyDown } onChange={ props.FramethreeonesevenonChange } ondelay={ props.Framethreeonesevenondelay }>
      {
      props.children ?
      props.children :
      <>
        <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['honetext']?.animationClass || {}}>

          <span id="id_fourthreethree_onezerofourtwoo"  className={` text honetext    ${ props.onClick ? 'cursor' : ''}  ${ transaction['honetext']?.type ? transaction['honetext']?.type.toLowerCase() : '' }`} style={{ ...{},  ...props.HoneTextStyle , transitionDuration: transaction['honetext']?.duration, transitionTimingFunction: transaction['honetext']?.timingFunction }} onClick={ props.HoneTextonClick } onMouseEnter={ props.HoneTextonMouseEnter } onMouseOver={ props.HoneTextonMouseOver } onKeyPress={ props.HoneTextonKeyPress } onDrag={ props.HoneTextonDrag } onMouseLeave={ props.HoneTextonMouseLeave } onMouseUp={ props.HoneTextonMouseUp } onMouseDown={ props.HoneTextonMouseDown } onKeyDown={ props.HoneTextonKeyDown } onChange={ props.HoneTextonChange } ondelay={ props.HoneTextondelay } >{props.HoneText0 || `Ayuda rápida`}</span>

        </CSSTransition>

      </>
      }
    </div>

  </CSSTransition>
</>
    ) 
}

Framethreeoneseven.propTypes = {
    style: PropTypes.any,
HoneText0: PropTypes.any,
FramethreeonesevenonClick: PropTypes.any,
FramethreeonesevenonMouseEnter: PropTypes.any,
FramethreeonesevenonMouseOver: PropTypes.any,
FramethreeonesevenonKeyPress: PropTypes.any,
FramethreeonesevenonDrag: PropTypes.any,
FramethreeonesevenonMouseLeave: PropTypes.any,
FramethreeonesevenonMouseUp: PropTypes.any,
FramethreeonesevenonMouseDown: PropTypes.any,
FramethreeonesevenonKeyDown: PropTypes.any,
FramethreeonesevenonChange: PropTypes.any,
Framethreeonesevenondelay: PropTypes.any,
HoneTextonClick: PropTypes.any,
HoneTextonMouseEnter: PropTypes.any,
HoneTextonMouseOver: PropTypes.any,
HoneTextonKeyPress: PropTypes.any,
HoneTextonDrag: PropTypes.any,
HoneTextonMouseLeave: PropTypes.any,
HoneTextonMouseUp: PropTypes.any,
HoneTextonMouseDown: PropTypes.any,
HoneTextonKeyDown: PropTypes.any,
HoneTextonChange: PropTypes.any,
HoneTextondelay: PropTypes.any
}
export default Framethreeoneseven;