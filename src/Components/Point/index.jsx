import React, { Suspense } from 'react';
import PropTypes from 'prop-types';
import * as Contexts  from 'Contexts';
import { OverlaySwapContext } from 'Components/OverlaySwapProvider';
import CorebooksAnimation from 'Components/CorebooksAnimation';
import CorebooksAnimationSingle from 'Components/CorebooksAnimationSingle';

import * as UtilsScripts  from 'utils/utils';
import { Route, useHistory, useParams } from "react-router-dom";
import SingletoneNavigation from 'Class/SingletoneNavigation';

import LoadingComponent from "Components/Loading";


import { Transition } from "react-transition-group";
import { CSSTransition } from "react-transition-group";
import { useAppContext, useSessionContext } from 'context/AppContext';
import './Point.css'





const Point = (props)=>{
    const [state,dispatch] = useAppContext();
    const [, setNameSelectedComponent] = React.useContext(OverlaySwapContext);
    
    const nodeRef = React.useRef(null);
    const history = useHistory();
    let RouterContext = useParams();
    
    
    
    
    
    
    const [globalStateForm, dispatchForm] = React.useState({});
    
    const [ _in, setIn ] = React.useState(false)
    const [ transaction, setTransaction ] = React.useState({}) 
    const [ animation, setAnimation ] = React.useState('')

    
    
    
    
    
    
    React.useEffect(()=>{
        
        
        
    },[]);
    
    
    return (
        <>
  <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['point']?.animationClass || {}}>

    <div id="id_fivesixnigth_twootwoothreethree" ref={nodeRef} className={` ${ props.onClick ? 'cursor' : '' } point C_fivesixnigth_twootwoothreethree ${ props.cssClass } ${ transaction['point']?.type ? transaction['point']?.type.toLowerCase() : '' }`} style={ { ...{ ...{}, transitionDuration: transaction['point']?.duration, transitionTimingFunction: transaction['point']?.timingFunction }, ...props.style }} onClick={ props.PointonClick } onMouseEnter={ props.PointonMouseEnter } onMouseOver={ props.PointonMouseOver } onKeyPress={ props.PointonKeyPress } onDrag={ props.PointonDrag } onMouseLeave={ props.PointonMouseLeave } onMouseUp={ props.PointonMouseUp } onMouseDown={ props.PointonMouseDown } onKeyDown={ props.PointonKeyDown } onChange={ props.PointonChange } ondelay={ props.Pointondelay }>
      {
      props.children ?
      props.children :
      <>
        <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['ellipseone']?.animationClass || {}}>
          <div id="id_fivesixnigth_twooonenigtheight" className={` ellipse ellipseone ${ props.onClick ? 'cursor' : '' } ${ transaction['ellipseone']?.type ? transaction['ellipseone']?.type.toLowerCase() : '' }`} style={{ ...{"borderRadius":"50%"},  ...props.EllipseoneStyle , transitionDuration: transaction['ellipseone']?.duration, transitionTimingFunction: transaction['ellipseone']?.timingFunction }} onClick={ props.EllipseoneonClick } onMouseEnter={ props.EllipseoneonMouseEnter } onMouseOver={ props.EllipseoneonMouseOver } onKeyPress={ props.EllipseoneonKeyPress } onDrag={ props.EllipseoneonDrag } onMouseLeave={ props.EllipseoneonMouseLeave } onMouseUp={ props.EllipseoneonMouseUp } onMouseDown={ props.EllipseoneonMouseDown } onKeyDown={ props.EllipseoneonKeyDown } onChange={ props.EllipseoneonChange } ondelay={ props.Ellipseoneondelay }></div>
        </CSSTransition>

      </>
      }
    </div>

  </CSSTransition>
</>
    ) 
}

Point.propTypes = {
    style: PropTypes.any,
Ellipseone0: PropTypes.any,
PointonClick: PropTypes.any,
PointonMouseEnter: PropTypes.any,
PointonMouseOver: PropTypes.any,
PointonKeyPress: PropTypes.any,
PointonDrag: PropTypes.any,
PointonMouseLeave: PropTypes.any,
PointonMouseUp: PropTypes.any,
PointonMouseDown: PropTypes.any,
PointonKeyDown: PropTypes.any,
PointonChange: PropTypes.any,
Pointondelay: PropTypes.any,
EllipseoneonClick: PropTypes.any,
EllipseoneonMouseEnter: PropTypes.any,
EllipseoneonMouseOver: PropTypes.any,
EllipseoneonKeyPress: PropTypes.any,
EllipseoneonDrag: PropTypes.any,
EllipseoneonMouseLeave: PropTypes.any,
EllipseoneonMouseUp: PropTypes.any,
EllipseoneonMouseDown: PropTypes.any,
EllipseoneonKeyDown: PropTypes.any,
EllipseoneonChange: PropTypes.any,
Ellipseoneondelay: PropTypes.any
}
export default Point;