import React, { Suspense } from 'react';
import PropTypes from 'prop-types';
import * as Contexts  from 'Contexts';
import { OverlaySwapContext } from 'Components/OverlaySwapProvider';
import CorebooksAnimation from 'Components/CorebooksAnimation';
import CorebooksAnimationSingle from 'Components/CorebooksAnimationSingle';

import * as UtilsScripts  from 'utils/utils';
import { Route, useHistory, useParams } from "react-router-dom";
import SingletoneNavigation from 'Class/SingletoneNavigation';

import LoadingComponent from "Components/Loading";


import { Transition } from "react-transition-group";
import { CSSTransition } from "react-transition-group";
import { useAppContext, useSessionContext } from 'context/AppContext';
import './Rectangleoneseven.css'





const Rectangleoneseven = (props)=>{
    const [state,dispatch] = useAppContext();
    const [, setNameSelectedComponent] = React.useContext(OverlaySwapContext);
    
    const nodeRef = React.useRef(null);
    const history = useHistory();
    let RouterContext = useParams();
    
    
    
    
    
    
    const [globalStateForm, dispatchForm] = React.useState({});
    
    const [ _in, setIn ] = React.useState(false)
    const [ transaction, setTransaction ] = React.useState({}) 
    const [ animation, setAnimation ] = React.useState('')

    
    
    
    
    
    
    React.useEffect(()=>{
        
        
        
    },[]);
    
    
    return (
        <>
  <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['rectangleoneseven']?.animationClass || {}}>

    <div id="id_fivefivenigth_twoozerosixone" ref={nodeRef} className={` ${ props.onClick ? 'cursor' : '' } rectangleoneseven ${ props.cssClass } ${ transaction['rectangleoneseven']?.type ? transaction['rectangleoneseven']?.type.toLowerCase() : '' }`} style={ { ...{ ...{}, transitionDuration: transaction['rectangleoneseven']?.duration, transitionTimingFunction: transaction['rectangleoneseven']?.timingFunction }, ...props.style }} onClick={ props.RectangleonesevenonClick } onMouseEnter={ props.RectangleonesevenonMouseEnter } onMouseOver={ props.RectangleonesevenonMouseOver } onKeyPress={ props.RectangleonesevenonKeyPress } onDrag={ props.RectangleonesevenonDrag } onMouseLeave={ props.RectangleonesevenonMouseLeave } onMouseUp={ props.RectangleonesevenonMouseUp } onMouseDown={ props.RectangleonesevenonMouseDown } onKeyDown={ props.RectangleonesevenonKeyDown } onChange={ props.RectangleonesevenonChange } ondelay={ props.Rectangleonesevenondelay }>
      {
      props.children ?
      props.children :
      <>


      </>
      }
    </div>

  </CSSTransition>
</>
    ) 
}

Rectangleoneseven.propTypes = {
    style: PropTypes.any,
RectangleonesevenonClick: PropTypes.any,
RectangleonesevenonMouseEnter: PropTypes.any,
RectangleonesevenonMouseOver: PropTypes.any,
RectangleonesevenonKeyPress: PropTypes.any,
RectangleonesevenonDrag: PropTypes.any,
RectangleonesevenonMouseLeave: PropTypes.any,
RectangleonesevenonMouseUp: PropTypes.any,
RectangleonesevenonMouseDown: PropTypes.any,
RectangleonesevenonKeyDown: PropTypes.any,
RectangleonesevenonChange: PropTypes.any,
Rectangleonesevenondelay: PropTypes.any
}
export default Rectangleoneseven;