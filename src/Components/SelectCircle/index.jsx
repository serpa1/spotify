import React, { Suspense } from 'react';
import PropTypes from 'prop-types';
import * as Contexts  from 'Contexts';
import { OverlaySwapContext } from 'Components/OverlaySwapProvider';
import CorebooksAnimation from 'Components/CorebooksAnimation';
import CorebooksAnimationSingle from 'Components/CorebooksAnimationSingle';

import * as UtilsScripts  from 'utils/utils';
import { Route, useHistory, useParams } from "react-router-dom";
import SingletoneNavigation from 'Class/SingletoneNavigation';

import LoadingComponent from "Components/Loading";


import { Transition } from "react-transition-group";
import { CSSTransition } from "react-transition-group";
import { useAppContext, useSessionContext } from 'context/AppContext';
import './SelectCircle.css'





const SelectCircle = (props)=>{
    const [state,dispatch] = useAppContext();
    const [, setNameSelectedComponent] = React.useContext(OverlaySwapContext);
    
    const nodeRef = React.useRef(null);
    const history = useHistory();
    let RouterContext = useParams();
    
    
    
    
    
    
    const [globalStateForm, dispatchForm] = React.useState({});
    
    const [ _in, setIn ] = React.useState(false)
    const [ transaction, setTransaction ] = React.useState({}) 
    const [ animation, setAnimation ] = React.useState('')

    
    
    
    
    
    
    React.useEffect(()=>{
        
        
        
    },[]);
    
    
    return (
        <>
  <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['selectcircle']?.animationClass || {}}>

    <div id="id_fourthreeeight_onethreefourfive" ref={nodeRef} className={` ${ props.onClick ? 'cursor' : '' } selectcircle ${ props.cssClass } ${ transaction['selectcircle']?.type ? transaction['selectcircle']?.type.toLowerCase() : '' }`} style={ { ...{ ...{}, transitionDuration: transaction['selectcircle']?.duration, transitionTimingFunction: transaction['selectcircle']?.timingFunction }, ...props.style }} onClick={ props.SelectCircleonClick } onMouseEnter={ props.SelectCircleonMouseEnter } onMouseOver={ props.SelectCircleonMouseOver } onKeyPress={ props.SelectCircleonKeyPress } onDrag={ props.SelectCircleonDrag } onMouseLeave={ props.SelectCircleonMouseLeave } onMouseUp={ props.SelectCircleonMouseUp } onMouseDown={ props.SelectCircleonMouseDown } onKeyDown={ props.SelectCircleonKeyDown } onChange={ props.SelectCircleonChange } ondelay={ props.SelectCircleondelay }>
      {
      props.children ?
      props.children :
      <>
        <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['selectcirclevector']?.animationClass || {}}>
          <div id="id_fourthreeeight_onethreefoursix" className={` rectangle selectcirclevector ${ props.onClick ? 'cursor' : '' } ${ transaction['selectcirclevector']?.type ? transaction['selectcirclevector']?.type.toLowerCase() : '' }`} style={{ ...{},  ...props.SelectCircleVectorStyle , transitionDuration: transaction['selectcirclevector']?.duration, transitionTimingFunction: transaction['selectcirclevector']?.timingFunction }} onClick={ props.SelectCircleVectoronClick } onMouseEnter={ props.SelectCircleVectoronMouseEnter } onMouseOver={ props.SelectCircleVectoronMouseOver } onKeyPress={ props.SelectCircleVectoronKeyPress } onDrag={ props.SelectCircleVectoronDrag } onMouseLeave={ props.SelectCircleVectoronMouseLeave } onMouseUp={ props.SelectCircleVectoronMouseUp } onMouseDown={ props.SelectCircleVectoronMouseDown } onKeyDown={ props.SelectCircleVectoronKeyDown } onChange={ props.SelectCircleVectoronChange } ondelay={ props.SelectCircleVectorondelay }></div>
        </CSSTransition>

      </>
      }
    </div>

  </CSSTransition>
</>
    ) 
}

SelectCircle.propTypes = {
    style: PropTypes.any,
SelectCircleVector0: PropTypes.any,
SelectCircleonClick: PropTypes.any,
SelectCircleonMouseEnter: PropTypes.any,
SelectCircleonMouseOver: PropTypes.any,
SelectCircleonKeyPress: PropTypes.any,
SelectCircleonDrag: PropTypes.any,
SelectCircleonMouseLeave: PropTypes.any,
SelectCircleonMouseUp: PropTypes.any,
SelectCircleonMouseDown: PropTypes.any,
SelectCircleonKeyDown: PropTypes.any,
SelectCircleonChange: PropTypes.any,
SelectCircleondelay: PropTypes.any,
SelectCircleVectoronClick: PropTypes.any,
SelectCircleVectoronMouseEnter: PropTypes.any,
SelectCircleVectoronMouseOver: PropTypes.any,
SelectCircleVectoronKeyPress: PropTypes.any,
SelectCircleVectoronDrag: PropTypes.any,
SelectCircleVectoronMouseLeave: PropTypes.any,
SelectCircleVectoronMouseUp: PropTypes.any,
SelectCircleVectoronMouseDown: PropTypes.any,
SelectCircleVectoronKeyDown: PropTypes.any,
SelectCircleVectoronChange: PropTypes.any,
SelectCircleVectorondelay: PropTypes.any
}
export default SelectCircle;