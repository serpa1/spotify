import { GraphQLClient } from 'graphql-request';
import axios from 'axios';
import { gql } from 'graphql-request';

const getUser = async () => {
    try {
        const reponseToken = await axios.post("/get-token-auth0", {});
        return reponseToken.data.access_token;
    } catch (error) {
        return ``;
    }
    
}

const getId = async (email) =>{
    const query = gql`query DataBinderUtilsUserQuery($email:String!){
        users:res_users(where:{login:{_eq: $email}}){
            _id
            login
        }
    }`
    try{
        const headers = {"Content-Type":"application/json","x-hasura-admin-secret": process.env.REACT_APP_GRAPHQL_SECRET};
        const graphQLClient = new GraphQLClient(process.env.REACT_APP_GRAPHQL_URL);
        const data = await graphQLClient.request(query, { email }, headers);
        if(data.users?.length)
            return data.users[0]._id ;
        return 0;
    }catch(e){
        return 0
    }

}
export const getUserData = async (email) =>{
    try {
        const token = await getUser();
        const id = await getId(email);
        return { token, id };
    } catch (error) {
        return {};
    }
    
}
