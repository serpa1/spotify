import React, { Suspense } from 'react';
import {
    Router,
    Switch,
    Route,
    useLocation,
    Link
  } from "react-router-dom";




import LoadingComponent from "Components/Loading";

import Logo from 'Components/Logo';
import Buttons from 'Components/Buttons';
import Icons from 'Components/Icons';
import Framethree from 'Components/Framethree';
import Framefour from 'Components/Framefour';
import Frametwoo from 'Components/Frametwoo';
import Frameone from 'Components/Frameone';
import Text from 'Components/Text';
import SubtitleTwoo from 'Components/SubtitleTwoo';
import CardPlaylistImgCont from 'Components/CardPlaylistImgCont';
import Framethreeoneseven from 'Components/Framethreeoneseven';
import Menu from 'Components/Menu';
import FooterMobile from 'Components/FooterMobile';
import Footer from 'Components/Footer';
import Header from 'Components/Header';
import Frametwooeightthree from 'Components/Frametwooeightthree';
import Framethreezerothree from 'Components/Framethreezerothree';
import CardPrice from 'Components/CardPrice';
import IMAGE from 'Components/IMAGE';
import Framethreetwooone from 'Components/Framethreetwooone';
import Framethreethreetwoo from 'Components/Framethreethreetwoo';
import Framethreefourzero from 'Components/Framethreefourzero';
import InputCreate from 'Components/InputCreate';
import InputCreateText from 'Components/InputCreateText';
import Frame from 'Components/Frame';
import InputDate from 'Components/InputDate';
import SelectCircle from 'Components/SelectCircle';
import SelectLoginSex from 'Components/SelectLoginSex';
import SelectRectangle from 'Components/SelectRectangle';
import SelectCreate from 'Components/SelectCreate';
import InputLogin from 'Components/InputLogin';
import Frameojotwoo from 'Components/Frameojotwoo';
import Framethreefourone from 'Components/Framethreefourone';
import ModalMenuAnvorguesa from 'Components/ModalMenuAnvorguesa';
import ModalClickDerecho from 'Components/ModalClickDerecho';
import Search from 'Components/Search';
import Searchtwoo from 'Components/Searchtwoo';
import ModalFiltroPlaylist from 'Components/ModalFiltroPlaylist';
import ColorPalette from 'Components/ColorPalette';
import Framefourzerofive from 'Components/Framefourzerofive';
import Framefourzerosix from 'Components/Framefourzerosix';
import Framefourzeroseven from 'Components/Framefourzeroseven';
import Framefourzeroeight from 'Components/Framefourzeroeight';
import Framefouronetwoo from 'Components/Framefouronetwoo';
import Point from 'Components/Point';
import onesixpx from 'Components/onesixpx';
import Typgraphy from 'Components/Typgraphy';
import TipographyWrapper from 'Components/TipographyWrapper';
import onesixPX from 'Components/onesixPX';
import oneeightPX from 'Components/oneeightPX';
import onefourpx from 'Components/onefourpx';
import WrapperRoute from 'Layouts/WrapperRoute';




const Layout = (props)=>{
    let location = useLocation();
    React.useEffect(() => {
        window.scrollTo(0, 0)
    }, [location]);
    return (
        
        
          
                     
        <Switch>
         <Route    component={Logo} path='/logo' exact={true} />
 <Route    component={Buttons} path='/buttons' exact={true} />
 <Route    component={Icons} path='/icons' exact={true} />
 <Route    component={Framethree} path='/framethree' exact={true} />
 <Route    component={Framefour} path='/framefour' exact={true} />
 <Route    component={Frametwoo} path='/frametwoo' exact={true} />
 <Route    component={Frameone} path='/frameone' exact={true} />
 <Route    component={Text} path='/text' exact={true} />
 <Route    component={SubtitleTwoo} path='/subtitletwoo' exact={true} />
 <Route    component={CardPlaylistImgCont} path='/cardplaylistimgcont' exact={true} />
 <Route    component={Framethreeoneseven} path='/framethreeoneseven' exact={true} />
 <Route    component={Menu} path='/menu' exact={true} />
 <Route    component={FooterMobile} path='/footermobile' exact={true} />
 <Route    component={Footer} path='/footer' exact={true} />
 <Route    component={Header} path='/header' exact={true} />
 <Route    component={Frametwooeightthree} path='/frametwooeightthree' exact={true} />
 <Route    component={Framethreezerothree} path='/framethreezerothree' exact={true} />
 <Route    component={CardPrice} path='/cardprice' exact={true} />
 <Route    component={IMAGE} path='/image' exact={true} />
 <Route    component={Framethreetwooone} path='/framethreetwooone' exact={true} />
 <Route    component={Framethreethreetwoo} path='/framethreethreetwoo' exact={true} />
 <Route    component={Framethreefourzero} path='/framethreefourzero' exact={true} />
 <Route    component={InputCreate} path='/inputcreate' exact={true} />
 <Route    component={InputCreateText} path='/inputcreatetext' exact={true} />
 <Route    component={Frame} path='/frame' exact={true} />
 <Route    component={InputDate} path='/inputdate' exact={true} />
 <Route    component={SelectCircle} path='/selectcircle' exact={true} />
 <Route    component={SelectLoginSex} path='/selectloginsex' exact={true} />
 <Route    component={SelectRectangle} path='/selectrectangle' exact={true} />
 <Route    component={SelectCreate} path='/selectcreate' exact={true} />
 <Route    component={InputLogin} path='/inputlogin' exact={true} />
 <Route    component={Frameojotwoo} path='/frameojotwoo' exact={true} />
 <Route    component={Framethreefourone} path='/framethreefourone' exact={true} />
 <Route    component={ModalMenuAnvorguesa} path='/modalmenuanvorguesa' exact={true} />
 <Route    component={ModalClickDerecho} path='/modalclickderecho' exact={true} />
 <Route    component={Search} path='/search' exact={true} />
 <Route    component={Searchtwoo} path='/searchtwoo' exact={true} />
 <Route    component={ModalFiltroPlaylist} path='/modalfiltroplaylist' exact={true} />
 <Route    component={ColorPalette} path='/colorpalette' exact={true} />
 <Route    component={Framefourzerofive} path='/framefourzerofive' exact={true} />
 <Route    component={Framefourzerosix} path='/framefourzerosix' exact={true} />
 <Route    component={Framefourzeroseven} path='/framefourzeroseven' exact={true} />
 <Route    component={Framefourzeroeight} path='/framefourzeroeight' exact={true} />
 <Route    component={Framefouronetwoo} path='/framefouronetwoo' exact={true} />
 <Route    component={Point} path='/point' exact={true} />
 <Route    component={onesixpx} path='/onesixpx' exact={true} />
 <Route    component={Typgraphy} path='/typgraphy' exact={true} />
 <Route    component={TipographyWrapper} path='/tipographywrapper' exact={true} />
 <Route    component={onesixPX} path='/onesixpx' exact={true} />
 <Route    component={oneeightPX} path='/oneeightpx' exact={true} />
 <Route    component={onefourpx} path='/onefourpx' exact={true} />
 <Route    component={WrapperRoute} path='/' exact={false} />             
        </Switch>   
        
         
        
        
   
    )
};


const MobileLayout = (props)=>{
    let location = useLocation();
    React.useEffect(() => {
        window.scrollTo(0, 0)
    }, [location]);
    return (
        
        
          
                     
        <Switch>
                     
        </Switch>
            
        
         
        
        
    )
};

export default Layout;
