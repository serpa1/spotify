import React, { Suspense } from 'react';
import PropTypes from 'prop-types';
import * as Contexts  from 'Contexts';
import { OverlaySwapContext } from 'Components/OverlaySwapProvider';
import CorebooksAnimation from 'Components/CorebooksAnimation';
import CorebooksAnimationSingle from 'Components/CorebooksAnimationSingle';

import * as UtilsScripts  from 'utils/utils';
import { Route, useHistory, useParams } from "react-router-dom";
import SingletoneNavigation from 'Class/SingletoneNavigation';

import LoadingComponent from "Components/Loading";


import { Transition } from "react-transition-group";
import { CSSTransition } from "react-transition-group";
import WrapperProjectCont from 'Components/WrapperProjectCont'
import LanguageButton from 'Components/LanguageButton'
import SubtitleTwoo from 'Components/SubtitleTwoo'
import SecundaryButton from 'Components/SecundaryButton'
import Subtitleone from 'Components/Subtitleone'
import IconCreate from 'Components/IconCreate'
import HFive from 'Components/HFive'
import MenuSearch from 'Components/MenuSearch'
import MenuHome from 'Components/MenuHome'
import { useAppContext, useSessionContext } from 'context/AppContext';
import './WrapperRoute.css'
const Home = React.lazy(() => import('Components/Home'));
const Premium = React.lazy(() => import('Components/Premium'));
const Premium = React.lazy(() => import('Components/Premium'));
const Help = React.lazy(() => import('Components/Help'));
const CrearCuenta = React.lazy(() => import('Components/CrearCuenta'));
const Login = React.lazy(() => import('Components/Login'));




const WrapperRoute = (props)=>{
    const [state,dispatch] = useAppContext();
    const [, setNameSelectedComponent] = React.useContext(OverlaySwapContext);
    
    const nodeRef = React.useRef(null);
    const history = useHistory();
    let RouterContext = useParams();
    
    
    
    
    
    
    const [globalStateForm, dispatchForm] = React.useState({});
    
    const [ _in, setIn ] = React.useState(false)
    const [ transaction, setTransaction ] = React.useState({}) 
    const [ animation, setAnimation ] = React.useState('')

    
    
    
    
    
    
    React.useEffect(()=>{
        
        
        
    },[]);
    
    
    return (
        <>
  <CSSTransition nodeRef={nodeRef} in={true} appear={true} timeout={400} classNames={SingletoneNavigation.getTransitionInstance()?.WrapperRoute?.cssClass || '' }>

    <div id="id_onefoureight_onezerofivetwoo" ref={nodeRef} className={` ${ props.onClick ? 'cursor' : '' } wrapperroute ${ props.cssClass } ${ transaction['wrapperroute']?.type ? transaction['wrapperroute']?.type.toLowerCase() : '' }`} style={ { ...{ ...{"maxHeight":"100vh","width":"100%"}, transitionDuration: `${((SingletoneNavigation.getTransitionInstance()?.WrapperRoute?.duration || 0) * 1000).toFixed(0)}ms` }, ...props.style }} onClick={ props.WrapperRouteonClick } onMouseEnter={ props.WrapperRouteonMouseEnter } onMouseOver={ props.WrapperRouteonMouseOver } onKeyPress={ props.WrapperRouteonKeyPress } onDrag={ props.WrapperRouteonDrag } onMouseLeave={ props.WrapperRouteonMouseLeave } onMouseUp={ props.WrapperRouteonMouseUp } onMouseDown={ props.WrapperRouteonMouseDown } onKeyDown={ props.WrapperRouteonKeyDown } onChange={ props.WrapperRouteonChange } ondelay={ props.WrapperRouteondelay }>
      {
      props.children ?
      props.children :
      <>
        <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['menu']?.animationClass || {}}>

          <div id="id_fourthreeeight_fourzeroonenigth" className={` frame menu hide-xs hide-s hide-m show-l show-xl ${ props.onClick ? 'cursor' : '' } ${ transaction['menu']?.type ? transaction['menu']?.type.toLowerCase() : '' }`} style={ { ...{"height":"100%","minWidth":"280px"}, ...props.MenuStyle , transitionDuration: transaction['menu']?.duration, transitionTimingFunction: transaction['menu']?.timingFunction } } onClick={ props.MenuonClick } onMouseEnter={ props.MenuonMouseEnter } onMouseOver={ props.MenuonMouseOver } onKeyPress={ props.MenuonKeyPress } onDrag={ props.MenuonDrag } onMouseLeave={ props.MenuonMouseLeave } onMouseUp={ props.MenuonMouseUp } onMouseDown={ props.MenuonMouseDown } onKeyDown={ props.MenuonKeyDown } onChange={ props.MenuonChange } ondelay={ props.Menuondelay }>
            <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['menucardonecont']?.animationClass || {}}>

              <div id="id_fourthreeeight_fourzerotwoozero" className={` frame menucardonecont ${ props.onClick ? 'cursor' : '' } ${ transaction['menucardonecont']?.type ? transaction['menucardonecont']?.type.toLowerCase() : '' }`} style={ { ...{}, ...props.MenuCardOneContStyle , transitionDuration: transaction['menucardonecont']?.duration, transitionTimingFunction: transaction['menucardonecont']?.timingFunction } } onClick={ props.MenuCardOneContonClick } onMouseEnter={ props.MenuCardOneContonMouseEnter } onMouseOver={ props.MenuCardOneContonMouseOver } onKeyPress={ props.MenuCardOneContonKeyPress } onDrag={ props.MenuCardOneContonDrag } onMouseLeave={ props.MenuCardOneContonMouseLeave } onMouseUp={ props.MenuCardOneContonMouseUp } onMouseDown={ props.MenuCardOneContonMouseDown } onKeyDown={ props.MenuCardOneContonKeyDown } onChange={ props.MenuCardOneContonChange } ondelay={ props.MenuCardOneContondelay }>
                <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['menucardone']?.animationClass || {}}>

                  <div id="id_fourthreeeight_fourzerotwooone" className={` frame menucardone ${ props.onClick ? 'cursor' : '' } ${ transaction['menucardone']?.type ? transaction['menucardone']?.type.toLowerCase() : '' }`} style={ { ...{}, ...props.MenuCardOneStyle , transitionDuration: transaction['menucardone']?.duration, transitionTimingFunction: transaction['menucardone']?.timingFunction } } onClick={ props.MenuCardOneonClick } onMouseEnter={ props.MenuCardOneonMouseEnter } onMouseOver={ props.MenuCardOneonMouseOver } onKeyPress={ props.MenuCardOneonKeyPress } onDrag={ props.MenuCardOneonDrag } onMouseLeave={ props.MenuCardOneonMouseLeave } onMouseUp={ props.MenuCardOneonMouseUp } onMouseDown={ props.MenuCardOneonMouseDown } onKeyDown={ props.MenuCardOneonKeyDown } onChange={ props.MenuCardOneonChange } ondelay={ props.MenuCardOneondelay }>
                    <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['menuhome']?.animationClass || {}}>
                      <MenuHome { ...{ ...props, style:false } } variant={'Property 1=Default'} cssClass={"C_fourthreeeight_fourzerotwootwoo "}  />
    </CSSTransition >
<CSSTransition 
        in={_in}
        appear={true} 
        timeout={100}
        classNames={transaction['menusearch']?.animationClass || {}}
    >
    <MenuSearch { ...{ ...props, style:false } }  variant={'Property 1=Default'} HfiveText0={ props.HfiveText0 || " Buscar"} HfiveTextStyle={{"fontSize":"0.875rem"}} cssClass={"C_fourthreeeight_fouroneoneeight "}  />
    </CSSTransition >
                    </div>
               
    </CSSTransition >
                    </div>
               
    </CSSTransition >
<CSSTransition 
        in={_in}
        appear={true} 
        timeout={100}
        classNames={transaction['menudatabuttoncont']?.animationClass || {}}
    >
    
                    <div id="id_fourthreeeight_fourzerotwoofour" className={` frame menudatabuttoncont ${ props.onClick ? 'cursor' : '' } ${ transaction['menudatabuttoncont']?.type ? transaction['menudatabuttoncont']?.type.toLowerCase() : '' }`} style={ { ...{}, ...props.MenuDataButtonContStyle , transitionDuration: transaction['menudatabuttoncont']?.duration, transitionTimingFunction: transaction['menudatabuttoncont']?.timingFunction } } onClick={ props.MenuDataButtonContonClick } onMouseEnter={ props.MenuDataButtonContonMouseEnter } onMouseOver={ props.MenuDataButtonContonMouseOver } onKeyPress={ props.MenuDataButtonContonKeyPress } onDrag={ props.MenuDataButtonContonDrag } onMouseLeave={ props.MenuDataButtonContonMouseLeave } onMouseUp={ props.MenuDataButtonContonMouseUp } onMouseDown={ props.MenuDataButtonContonMouseDown } onKeyDown={ props.MenuDataButtonContonKeyDown } onChange={ props.MenuDataButtonContonChange } ondelay={ props.MenuDataButtonContondelay }>
                        <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['menudatacont']?.animationClass || {}}>

                          <div id="id_fourthreeeight_fourzerotwoofive" className={` frame menudatacont ${ props.onClick ? 'cursor' : '' } ${ transaction['menudatacont']?.type ? transaction['menudatacont']?.type.toLowerCase() : '' }`} style={ { ...{}, ...props.MenuDataContStyle , transitionDuration: transaction['menudatacont']?.duration, transitionTimingFunction: transaction['menudatacont']?.timingFunction } } onClick={ props.MenuDataContonClick } onMouseEnter={ props.MenuDataContonMouseEnter } onMouseOver={ props.MenuDataContonMouseOver } onKeyPress={ props.MenuDataContonKeyPress } onDrag={ props.MenuDataContonDrag } onMouseLeave={ props.MenuDataContonMouseLeave } onMouseUp={ props.MenuDataContonMouseUp } onMouseDown={ props.MenuDataContonMouseDown } onKeyDown={ props.MenuDataContonKeyDown } onChange={ props.MenuDataContonChange } ondelay={ props.MenuDataContondelay }>
                            <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['menulibrarycreate']?.animationClass || {}}>

                              <div id="id_fourthreeeight_fourzerotwoosix" className={` frame menulibrarycreate ${ props.onClick ? 'cursor' : '' } ${ transaction['menulibrarycreate']?.type ? transaction['menulibrarycreate']?.type.toLowerCase() : '' }`} style={ { ...{}, ...props.MenuLibraryCreateStyle , transitionDuration: transaction['menulibrarycreate']?.duration, transitionTimingFunction: transaction['menulibrarycreate']?.timingFunction } } onClick={ props.MenuLibraryCreateonClick } onMouseEnter={ props.MenuLibraryCreateonMouseEnter } onMouseOver={ props.MenuLibraryCreateonMouseOver } onKeyPress={ props.MenuLibraryCreateonKeyPress } onDrag={ props.MenuLibraryCreateonDrag } onMouseLeave={ props.MenuLibraryCreateonMouseLeave } onMouseUp={ props.MenuLibraryCreateonMouseUp } onMouseDown={ props.MenuLibraryCreateonMouseDown } onKeyDown={ props.MenuLibraryCreateonKeyDown } onChange={ props.MenuLibraryCreateonChange } ondelay={ props.MenuLibraryCreateondelay }>
                                <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['menulibrary']?.animationClass || {}}>

                                  <div id="id_fourthreeeight_fourzerosevenseven" className={` frame menulibrary ${ props.onClick ? 'cursor' : '' } ${ transaction['menulibrary']?.type ? transaction['menulibrary']?.type.toLowerCase() : '' }`} style={ { ...{}, ...props.MenuLibraryStyle , transitionDuration: transaction['menulibrary']?.duration, transitionTimingFunction: transaction['menulibrary']?.timingFunction } } onClick={ props.MenuLibraryonClick } onMouseEnter={ props.MenuLibraryonMouseEnter } onMouseOver={ props.MenuLibraryonMouseOver } onKeyPress={ props.MenuLibraryonKeyPress } onDrag={ props.MenuLibraryonDrag } onMouseLeave={ props.MenuLibraryonMouseLeave } onMouseUp={ props.MenuLibraryonMouseUp } onMouseDown={ props.MenuLibraryonMouseDown } onKeyDown={ props.MenuLibraryonKeyDown } onChange={ props.MenuLibraryonChange } ondelay={ props.MenuLibraryondelay }>
                                    <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['frame']?.animationClass || {}}>

                                      <div id="id_fourthreeeight_fourzerooneseven" className={` frame frame ${ props.onClick ? 'cursor' : '' } ${ transaction['frame']?.type ? transaction['frame']?.type.toLowerCase() : '' }`} style={ { ...{}, ...props.FrameStyle , transitionDuration: transaction['frame']?.duration, transitionTimingFunction: transaction['frame']?.timingFunction } } onClick={ props.FrameonClick } onMouseEnter={ props.FrameonMouseEnter } onMouseOver={ props.FrameonMouseOver } onKeyPress={ props.FrameonKeyPress } onDrag={ props.FrameonDrag } onMouseLeave={ props.FrameonMouseLeave } onMouseUp={ props.FrameonMouseUp } onMouseDown={ props.FrameonMouseDown } onKeyDown={ props.FrameonKeyDown } onChange={ props.FrameonChange } ondelay={ props.Frameondelay }>
                                        <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['vector']?.animationClass || {}}>
                                          <svg id="id_fourthreeeight_fourzerooneeight" className="vector  " style={{}} onClick={ props.VectoronClick } onMouseEnter={ props.VectoronMouseEnter } onMouseOver={ props.VectoronMouseOver } onKeyPress={ props.VectoronKeyPress } onDrag={ props.VectoronDrag } onMouseLeave={ props.VectoronMouseLeave } onMouseUp={ props.VectoronMouseUp } onMouseDown={ props.VectoronMouseDown } onKeyDown={ props.VectoronKeyDown } onChange={ props.VectoronChange } ondelay={ props.Vectorondelay } width="20" height="20">
                                            <path d="M1 20C0.734784 20 0.48043 19.8946 0.292893 19.7071C0.105357 19.5196 6.66134e-16 19.2652 0 19L0 1C0 0.734784 0.105357 0.48043 0.292893 0.292893C0.48043 0.105357 0.734784 4.44089e-16 1 0C1.26522 4.44089e-16 1.51957 0.105357 1.70711 0.292893C1.89464 0.48043 2 0.734784 2 1L2 19C2 19.2652 1.89464 19.5196 1.70711 19.7071C1.51957 19.8946 1.26522 20 1 20ZM13.5 0.134C13.348 0.0462328 13.1755 2.66256e-05 13 2.55108e-05C12.8245 2.43959e-05 12.652 0.0462283 12.5 0.133994C12.348 0.221759 12.2218 0.347993 12.134 0.500009C12.0462 0.652025 12 0.824466 12 1L12 19C12 19.2652 12.1054 19.5196 12.2929 19.7071C12.4804 19.8946 12.7348 20 13 20L19 20C19.2652 20 19.5196 19.8946 19.7071 19.7071C19.8946 19.5196 20 19.2652 20 19L20 4.464C20 4.28847 19.9538 4.11603 19.866 3.96401C19.7782 3.812 19.652 3.68577 19.5 3.598L13.5 0.134ZM7 0C6.73478 4.44089e-16 6.48043 0.105357 6.29289 0.292893C6.10536 0.48043 6 0.734784 6 1L6 19C6 19.2652 6.10536 19.5196 6.29289 19.7071C6.48043 19.8946 6.73478 20 7 20C7.26522 20 7.51957 19.8946 7.70711 19.7071C7.89464 19.5196 8 19.2652 8 19L8 1C8 0.734784 7.89464 0.48043 7.70711 0.292893C7.51957 0.105357 7.26522 6.66134e-16 7 0Z" />
                                          </svg>
                                        </CSSTransition>
                                      </div>

                                    </CSSTransition>
                                    <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['hfive']?.animationClass || {}}>
                                      <HFive { ...{ ...props, style:false } } HfiveText0={ props.HfiveText0 || "Tu biblioteca" } HfiveText0={ props.HfiveText0 || "Tu biblioteca" } HfiveTextStyle={{"fontSize":"0.875rem"}} cssClass={"C_fourthreeeight_fourzerosevennigth "}  />
    </CSSTransition >
                    </div>
               
    </CSSTransition >
<CSSTransition 
        in={_in}
        appear={true} 
        timeout={100}
        classNames={transaction['iconcreate']?.animationClass || {}}
    >
    <IconCreate { ...{ ...props, style:false } }  variant={'Property 1=Default'}  cssClass={" C_fourthreeeight_fourzerotwooeight cursor "}  />
    </CSSTransition >
                    </div>
               
    </CSSTransition >
<CSSTransition 
        in={_in}
        appear={true} 
        timeout={100}
        classNames={transaction['menucardtwo']?.animationClass || {}}
    >
    
                    <div id="id_fourthreeeight_fourzerotwoonigth" className={` frame menucardtwo ${ props.onClick ? 'cursor' : '' } ${ transaction['menucardtwo']?.type ? transaction['menucardtwo']?.type.toLowerCase() : '' }`} style={ { ...{}, ...props.MenuCardTwoStyle , transitionDuration: transaction['menucardtwo']?.duration, transitionTimingFunction: transaction['menucardtwo']?.timingFunction } } onClick={ props.MenuCardTwoonClick } onMouseEnter={ props.MenuCardTwoonMouseEnter } onMouseOver={ props.MenuCardTwoonMouseOver } onKeyPress={ props.MenuCardTwoonKeyPress } onDrag={ props.MenuCardTwoonDrag } onMouseLeave={ props.MenuCardTwoonMouseLeave } onMouseUp={ props.MenuCardTwoonMouseUp } onMouseDown={ props.MenuCardTwoonMouseDown } onKeyDown={ props.MenuCardTwoonKeyDown } onChange={ props.MenuCardTwoonChange } ondelay={ props.MenuCardTwoondelay }>
                                        <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['menucardtwodata']?.animationClass || {}}>

                                          <div id="id_fourthreeeight_fourzerothreezero" className={` frame menucardtwodata ${ props.onClick ? 'cursor' : '' } ${ transaction['menucardtwodata']?.type ? transaction['menucardtwodata']?.type.toLowerCase() : '' }`} style={ { ...{}, ...props.MenuCardTwoDataStyle , transitionDuration: transaction['menucardtwodata']?.duration, transitionTimingFunction: transaction['menucardtwodata']?.timingFunction } } onClick={ props.MenuCardTwoDataonClick } onMouseEnter={ props.MenuCardTwoDataonMouseEnter } onMouseOver={ props.MenuCardTwoDataonMouseOver } onKeyPress={ props.MenuCardTwoDataonKeyPress } onDrag={ props.MenuCardTwoDataonDrag } onMouseLeave={ props.MenuCardTwoDataonMouseLeave } onMouseUp={ props.MenuCardTwoDataonMouseUp } onMouseDown={ props.MenuCardTwoDataonMouseDown } onKeyDown={ props.MenuCardTwoDataonKeyDown } onChange={ props.MenuCardTwoDataonChange } ondelay={ props.MenuCardTwoDataondelay }>
                                            <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['subtitleone']?.animationClass || {}}>
                                              <Subtitleone { ...{ ...props, style:false } } SubtilteoneText0={ props.SubtilteoneText0 || "Crea tu primera playlist" } cssClass={"C_fourthreeeight_fourzerothreeone "}  />
    </CSSTransition >
<CSSTransition 
        in={_in}
        appear={true} 
        timeout={100}
        classNames={transaction['hfive']?.animationClass || {}}
    >
    <HFive { ...{ ...props, style:false } }   HfiveText0={ props.HfiveText1 || " ¡Es muy fácil! Te vamos a ayudar"} HfiveTextStyle={{"fontSize":"0.875rem"}} cssClass={"C_fourthreeeight_fourzerothreetwoo "}  />
    </CSSTransition >
                    </div>
               
    </CSSTransition >
<CSSTransition 
        in={_in}
        appear={true} 
        timeout={100}
        classNames={transaction['secundarybuttoncont']?.animationClass || {}}
    >
    
                    <div id="id_fourthreeeight_fourzerothreethree" className={` frame secundarybuttoncont ${ props.onClick ? 'cursor' : '' } ${ transaction['secundarybuttoncont']?.type ? transaction['secundarybuttoncont']?.type.toLowerCase() : '' }`} style={ { ...{}, ...props.SecundaryButtonContStyle , transitionDuration: transaction['secundarybuttoncont']?.duration, transitionTimingFunction: transaction['secundarybuttoncont']?.timingFunction } } onClick={ props.SecundaryButtonContonClick } onMouseEnter={ props.SecundaryButtonContonMouseEnter } onMouseOver={ props.SecundaryButtonContonMouseOver } onKeyPress={ props.SecundaryButtonContonKeyPress } onDrag={ props.SecundaryButtonContonDrag } onMouseLeave={ props.SecundaryButtonContonMouseLeave } onMouseUp={ props.SecundaryButtonContonMouseUp } onMouseDown={ props.SecundaryButtonContonMouseDown } onKeyDown={ props.SecundaryButtonContonKeyDown } onChange={ props.SecundaryButtonContonChange } ondelay={ props.SecundaryButtonContondelay }>
                                                <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['secundarybutton']?.animationClass || {}}>
                                                  <SecundaryButton { ...{ ...props, style:false } } variant={'Property 1=Default'} SecundaryButtonText0={ props.SecundaryButtonText0 || "Crear Playlist" } cssClass={"C_fourthreeeight_fourzerothreefour cursor "}  />
    </CSSTransition >
                    </div>
               
    </CSSTransition >
                    </div>
               
    </CSSTransition >
<CSSTransition 
        in={_in}
        appear={true} 
        timeout={100}
        classNames={transaction['menucardthree']?.animationClass || {}}
    >
    
                    <div id="id_fourthreeeight_fourzerothreefive" className={` frame menucardthree ${ props.onClick ? 'cursor' : '' } ${ transaction['menucardthree']?.type ? transaction['menucardthree']?.type.toLowerCase() : '' }`} style={ { ...{}, ...props.MenuCardThreeStyle , transitionDuration: transaction['menucardthree']?.duration, transitionTimingFunction: transaction['menucardthree']?.timingFunction } } onClick={ props.MenuCardThreeonClick } onMouseEnter={ props.MenuCardThreeonMouseEnter } onMouseOver={ props.MenuCardThreeonMouseOver } onKeyPress={ props.MenuCardThreeonKeyPress } onDrag={ props.MenuCardThreeonDrag } onMouseLeave={ props.MenuCardThreeonMouseLeave } onMouseUp={ props.MenuCardThreeonMouseUp } onMouseDown={ props.MenuCardThreeonMouseDown } onKeyDown={ props.MenuCardThreeonKeyDown } onChange={ props.MenuCardThreeonChange } ondelay={ props.MenuCardThreeondelay }>
                                                    <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['menucardthreedata']?.animationClass || {}}>

                                                      <div id="id_fourthreeeight_fourzerothreesix" className={` frame menucardthreedata ${ props.onClick ? 'cursor' : '' } ${ transaction['menucardthreedata']?.type ? transaction['menucardthreedata']?.type.toLowerCase() : '' }`} style={ { ...{}, ...props.MenuCardThreeDataStyle , transitionDuration: transaction['menucardthreedata']?.duration, transitionTimingFunction: transaction['menucardthreedata']?.timingFunction } } onClick={ props.MenuCardThreeDataonClick } onMouseEnter={ props.MenuCardThreeDataonMouseEnter } onMouseOver={ props.MenuCardThreeDataonMouseOver } onKeyPress={ props.MenuCardThreeDataonKeyPress } onDrag={ props.MenuCardThreeDataonDrag } onMouseLeave={ props.MenuCardThreeDataonMouseLeave } onMouseUp={ props.MenuCardThreeDataonMouseUp } onMouseDown={ props.MenuCardThreeDataonMouseDown } onKeyDown={ props.MenuCardThreeDataonKeyDown } onChange={ props.MenuCardThreeDataonChange } ondelay={ props.MenuCardThreeDataondelay }>
                                                        <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['subtitleone']?.animationClass || {}}>
                                                          <Subtitleone { ...{ ...props, style:false } } SubtilteoneText0={ props.SubtilteoneText1 || "Busquemos algunos podcast para seguir" } cssClass={"C_fourthreeeight_fourzerothreeseven "}  />
    </CSSTransition >
<CSSTransition 
        in={_in}
        appear={true} 
        timeout={100}
        classNames={transaction['hfive']?.animationClass || {}}
    >
    <HFive { ...{ ...props, style:false } }   HfiveText0={ props.HfiveText2 || " Te mantendremos al tanto de los nuevos episodios"} cssClass={"C_fourthreeeight_fourzerothreeeight "}  />
    </CSSTransition >
                    </div>
               
    </CSSTransition >
<CSSTransition 
        in={_in}
        appear={true} 
        timeout={100}
        classNames={transaction['secundarybuttoncont']?.animationClass || {}}
    >
    
                    <div id="id_fourthreeeight_fourzerothreenigth" className={` frame secundarybuttoncont ${ props.onClick ? 'cursor' : '' } ${ transaction['secundarybuttoncont']?.type ? transaction['secundarybuttoncont']?.type.toLowerCase() : '' }`} style={ { ...{}, ...props.SecundaryButtonContStyle , transitionDuration: transaction['secundarybuttoncont']?.duration, transitionTimingFunction: transaction['secundarybuttoncont']?.timingFunction } } onClick={ props.SecundaryButtonContonClick } onMouseEnter={ props.SecundaryButtonContonMouseEnter } onMouseOver={ props.SecundaryButtonContonMouseOver } onKeyPress={ props.SecundaryButtonContonKeyPress } onDrag={ props.SecundaryButtonContonDrag } onMouseLeave={ props.SecundaryButtonContonMouseLeave } onMouseUp={ props.SecundaryButtonContonMouseUp } onMouseDown={ props.SecundaryButtonContonMouseDown } onKeyDown={ props.SecundaryButtonContonKeyDown } onChange={ props.SecundaryButtonContonChange } ondelay={ props.SecundaryButtonContondelay }>
                                                            <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['secundarybutton']?.animationClass || {}}>
                                                              <SecundaryButton { ...{ ...props, style:false } } variant={'Property 1=Default'} SecundaryButtonText0={ props.SecundaryButtonText1 || "Explorar podcasts" } cssClass={"C_fourthreeeight_fourzerofourzero cursor "}  />
    </CSSTransition >
                    </div>
               
    </CSSTransition >
                    </div>
               
    </CSSTransition >
                    </div>
               
    </CSSTransition >
<CSSTransition 
        in={_in}
        appear={true} 
        timeout={100}
        classNames={transaction['menulegacybutton']?.animationClass || {}}
    >
    
                    <div id="id_fourthreeeight_fourzerofourone" className={` frame menulegacybutton ${ props.onClick ? 'cursor' : '' } ${ transaction['menulegacybutton']?.type ? transaction['menulegacybutton']?.type.toLowerCase() : '' }`} style={ { ...{}, ...props.MenuLegacyButtonStyle , transitionDuration: transaction['menulegacybutton']?.duration, transitionTimingFunction: transaction['menulegacybutton']?.timingFunction } } onClick={ props.MenuLegacyButtononClick } onMouseEnter={ props.MenuLegacyButtononMouseEnter } onMouseOver={ props.MenuLegacyButtononMouseOver } onKeyPress={ props.MenuLegacyButtononKeyPress } onDrag={ props.MenuLegacyButtononDrag } onMouseLeave={ props.MenuLegacyButtononMouseLeave } onMouseUp={ props.MenuLegacyButtononMouseUp } onMouseDown={ props.MenuLegacyButtononMouseDown } onKeyDown={ props.MenuLegacyButtononKeyDown } onChange={ props.MenuLegacyButtononChange } ondelay={ props.MenuLegacyButtonondelay }>
                                                                <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['menulegacy']?.animationClass || {}}>

                                                                  <div id="id_fourthreeeight_fourzerofourtwoo" className={` frame menulegacy ${ props.onClick ? 'cursor' : '' } ${ transaction['menulegacy']?.type ? transaction['menulegacy']?.type.toLowerCase() : '' }`} style={ { ...{}, ...props.MenuLegacyStyle , transitionDuration: transaction['menulegacy']?.duration, transitionTimingFunction: transaction['menulegacy']?.timingFunction } } onClick={ props.MenuLegacyonClick } onMouseEnter={ props.MenuLegacyonMouseEnter } onMouseOver={ props.MenuLegacyonMouseOver } onKeyPress={ props.MenuLegacyonKeyPress } onDrag={ props.MenuLegacyonDrag } onMouseLeave={ props.MenuLegacyonMouseLeave } onMouseUp={ props.MenuLegacyonMouseUp } onMouseDown={ props.MenuLegacyonMouseDown } onKeyDown={ props.MenuLegacyonKeyDown } onChange={ props.MenuLegacyonChange } ondelay={ props.MenuLegacyondelay }>
                                                                    <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['menulegacyone']?.animationClass || {}}>

                                                                      <div id="id_fourthreeeight_fourzerofourthree" className={` frame menulegacyone ${ props.onClick ? 'cursor' : '' } ${ transaction['menulegacyone']?.type ? transaction['menulegacyone']?.type.toLowerCase() : '' }`} style={ { ...{}, ...props.MenuLegacyOneStyle , transitionDuration: transaction['menulegacyone']?.duration, transitionTimingFunction: transaction['menulegacyone']?.timingFunction } } onClick={ props.MenuLegacyOneonClick } onMouseEnter={ props.MenuLegacyOneonMouseEnter } onMouseOver={ props.MenuLegacyOneonMouseOver } onKeyPress={ props.MenuLegacyOneonKeyPress } onDrag={ props.MenuLegacyOneonDrag } onMouseLeave={ props.MenuLegacyOneonMouseLeave } onMouseUp={ props.MenuLegacyOneonMouseUp } onMouseDown={ props.MenuLegacyOneonMouseDown } onKeyDown={ props.MenuLegacyOneonKeyDown } onChange={ props.MenuLegacyOneonChange } ondelay={ props.MenuLegacyOneondelay }>
                                                                        <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['subtitletwoo']?.animationClass || {}}>
                                                                          <SubtitleTwoo { ...{ ...props, style:false } } SubtitletwooText0={ props.SubtitletwooText0 || "Legal" } SubtitletwooText0={ props.SubtitletwooText0 || "Legal" } cssClass={"C_fourthreeeight_fourzerofourfour "}  />
    </CSSTransition >
<CSSTransition 
        in={_in}
        appear={true} 
        timeout={100}
        classNames={transaction['subtitletwoo']?.animationClass || {}}
    >
    <SubtitleTwoo { ...{ ...props, style:false } }   SubtitletwooText0={ props.SubtitletwooText1 || " Centro de privacidad"} cssClass={"C_fourthreeeight_fourzerofourfive "}  />
    </CSSTransition >
                    </div>
               
    </CSSTransition >
<CSSTransition 
        in={_in}
        appear={true} 
        timeout={100}
        classNames={transaction['menulegacytwo']?.animationClass || {}}
    >
    
                    <div id="id_fourthreeeight_fourzerofoursix" className={` frame menulegacytwo ${ props.onClick ? 'cursor' : '' } ${ transaction['menulegacytwo']?.type ? transaction['menulegacytwo']?.type.toLowerCase() : '' }`} style={ { ...{}, ...props.MenuLegacyTwoStyle , transitionDuration: transaction['menulegacytwo']?.duration, transitionTimingFunction: transaction['menulegacytwo']?.timingFunction } } onClick={ props.MenuLegacyTwoonClick } onMouseEnter={ props.MenuLegacyTwoonMouseEnter } onMouseOver={ props.MenuLegacyTwoonMouseOver } onKeyPress={ props.MenuLegacyTwoonKeyPress } onDrag={ props.MenuLegacyTwoonDrag } onMouseLeave={ props.MenuLegacyTwoonMouseLeave } onMouseUp={ props.MenuLegacyTwoonMouseUp } onMouseDown={ props.MenuLegacyTwoonMouseDown } onKeyDown={ props.MenuLegacyTwoonKeyDown } onChange={ props.MenuLegacyTwoonChange } ondelay={ props.MenuLegacyTwoondelay }>
                                                                            <CSSTransition in={_in} appear={true} timeout={100} classNames={transaction['subtitletwoo']?.animationClass || {}}>
                                                                              <SubtitleTwoo { ...{ ...props, style:false } } SubtitletwooText0={ props.SubtitletwooText2 || "Políticas de Privacidad" } cssClass={"C_fourthreeeight_fourzerofourseven "}  />
    </CSSTransition >
<CSSTransition 
        in={_in}
        appear={true} 
        timeout={100}
        classNames={transaction['subtitletwoo']?.animationClass || {}}
    >
    <SubtitleTwoo { ...{ ...props, style:false } }   SubtitletwooText0={ props.SubtitletwooText3 || " Cookies"} cssClass={"C_fourthreeeight_fourzerofoureight "}  />
    </CSSTransition >
                    </div>
               
    </CSSTransition >
<CSSTransition 
        in={_in}
        appear={true} 
        timeout={100}
        classNames={transaction['subtitletwoo']?.animationClass || {}}
    >
    <SubtitleTwoo { ...{ ...props, style:false } }   SubtitletwooText0={ props.SubtitletwooText4 || " Sobre los anuncios"} cssClass={"C_fourthreeeight_fourzerofournigth "}  />
    </CSSTransition >
<CSSTransition 
        in={_in}
        appear={true} 
        timeout={100}
        classNames={transaction['subtitletwoo']?.animationClass || {}}
    >
    <SubtitleTwoo { ...{ ...props, style:false } }   SubtitletwooText0={ props.SubtitletwooText5 || " Cookies"} cssClass={"C_fourthreeeight_fourzerofivezero "}  />
    </CSSTransition >
                    </div>
               
    </CSSTransition >
<CSSTransition 
        in={_in}
        appear={true} 
        timeout={100}
        classNames={transaction['languagebutton']?.animationClass || {}}
    >
    <LanguageButton { ...{ ...props, style:false } }  variant={'Property 1=Default'} LanguageButtonText0={ props.LanguageButtonText0 || " Español de Latinoamérica"} style={{"maxWidth":"202px"}} VectorStyle={{"width":"16px","height":"16px"}} cssClass={"C_fourthreeeight_fourzerofiveone cursor "}  />
    </CSSTransition >
                    </div>
               
    </CSSTransition >
                    </div>
               
    </CSSTransition >
                    </div>
               
    </CSSTransition >
<CSSTransition 
        in={_in}
        appear={true} 
        timeout={100}
        classNames={transaction['wrapperprojectcont']?.animationClass || {}}
    >
    <WrapperProjectCont { ...{ ...props, style:false } }    cssClass={" C_onefoureight_onezeroeightfour "}  />
    </CSSTransition >
            
            </>
        }
        </div>
    
</CSSTransition>
            </>
        
    ) 
}

WrapperRoute.propTypes = {
    style: PropTypes.any,
HfiveText0: PropTypes.any,
SubtilteoneText0: PropTypes.any,
HfiveText1: PropTypes.any,
SecundaryButtonText0: PropTypes.any,
SubtilteoneText1: PropTypes.any,
HfiveText2: PropTypes.any,
SecundaryButtonText1: PropTypes.any,
SubtitletwooText0: PropTypes.any,
SubtitletwooText1: PropTypes.any,
SubtitletwooText2: PropTypes.any,
SubtitletwooText3: PropTypes.any,
SubtitletwooText4: PropTypes.any,
SubtitletwooText5: PropTypes.any,
LanguageButtonText0: PropTypes.any,
WrapperRouteonClick: PropTypes.any,
WrapperRouteonMouseEnter: PropTypes.any,
WrapperRouteonMouseOver: PropTypes.any,
WrapperRouteonKeyPress: PropTypes.any,
WrapperRouteonDrag: PropTypes.any,
WrapperRouteonMouseLeave: PropTypes.any,
WrapperRouteonMouseUp: PropTypes.any,
WrapperRouteonMouseDown: PropTypes.any,
WrapperRouteonKeyDown: PropTypes.any,
WrapperRouteonChange: PropTypes.any,
WrapperRouteondelay: PropTypes.any,
MenuonClick: PropTypes.any,
MenuonMouseEnter: PropTypes.any,
MenuonMouseOver: PropTypes.any,
MenuonKeyPress: PropTypes.any,
MenuonDrag: PropTypes.any,
MenuonMouseLeave: PropTypes.any,
MenuonMouseUp: PropTypes.any,
MenuonMouseDown: PropTypes.any,
MenuonKeyDown: PropTypes.any,
MenuonChange: PropTypes.any,
Menuondelay: PropTypes.any,
MenuCardOneContonClick: PropTypes.any,
MenuCardOneContonMouseEnter: PropTypes.any,
MenuCardOneContonMouseOver: PropTypes.any,
MenuCardOneContonKeyPress: PropTypes.any,
MenuCardOneContonDrag: PropTypes.any,
MenuCardOneContonMouseLeave: PropTypes.any,
MenuCardOneContonMouseUp: PropTypes.any,
MenuCardOneContonMouseDown: PropTypes.any,
MenuCardOneContonKeyDown: PropTypes.any,
MenuCardOneContonChange: PropTypes.any,
MenuCardOneContondelay: PropTypes.any,
MenuCardOneonClick: PropTypes.any,
MenuCardOneonMouseEnter: PropTypes.any,
MenuCardOneonMouseOver: PropTypes.any,
MenuCardOneonKeyPress: PropTypes.any,
MenuCardOneonDrag: PropTypes.any,
MenuCardOneonMouseLeave: PropTypes.any,
MenuCardOneonMouseUp: PropTypes.any,
MenuCardOneonMouseDown: PropTypes.any,
MenuCardOneonKeyDown: PropTypes.any,
MenuCardOneonChange: PropTypes.any,
MenuCardOneondelay: PropTypes.any,
MenuDataButtonContonClick: PropTypes.any,
MenuDataButtonContonMouseEnter: PropTypes.any,
MenuDataButtonContonMouseOver: PropTypes.any,
MenuDataButtonContonKeyPress: PropTypes.any,
MenuDataButtonContonDrag: PropTypes.any,
MenuDataButtonContonMouseLeave: PropTypes.any,
MenuDataButtonContonMouseUp: PropTypes.any,
MenuDataButtonContonMouseDown: PropTypes.any,
MenuDataButtonContonKeyDown: PropTypes.any,
MenuDataButtonContonChange: PropTypes.any,
MenuDataButtonContondelay: PropTypes.any,
MenuDataContonClick: PropTypes.any,
MenuDataContonMouseEnter: PropTypes.any,
MenuDataContonMouseOver: PropTypes.any,
MenuDataContonKeyPress: PropTypes.any,
MenuDataContonDrag: PropTypes.any,
MenuDataContonMouseLeave: PropTypes.any,
MenuDataContonMouseUp: PropTypes.any,
MenuDataContonMouseDown: PropTypes.any,
MenuDataContonKeyDown: PropTypes.any,
MenuDataContonChange: PropTypes.any,
MenuDataContondelay: PropTypes.any,
MenuLibraryCreateonClick: PropTypes.any,
MenuLibraryCreateonMouseEnter: PropTypes.any,
MenuLibraryCreateonMouseOver: PropTypes.any,
MenuLibraryCreateonKeyPress: PropTypes.any,
MenuLibraryCreateonDrag: PropTypes.any,
MenuLibraryCreateonMouseLeave: PropTypes.any,
MenuLibraryCreateonMouseUp: PropTypes.any,
MenuLibraryCreateonMouseDown: PropTypes.any,
MenuLibraryCreateonKeyDown: PropTypes.any,
MenuLibraryCreateonChange: PropTypes.any,
MenuLibraryCreateondelay: PropTypes.any,
MenuLibraryonClick: PropTypes.any,
MenuLibraryonMouseEnter: PropTypes.any,
MenuLibraryonMouseOver: PropTypes.any,
MenuLibraryonKeyPress: PropTypes.any,
MenuLibraryonDrag: PropTypes.any,
MenuLibraryonMouseLeave: PropTypes.any,
MenuLibraryonMouseUp: PropTypes.any,
MenuLibraryonMouseDown: PropTypes.any,
MenuLibraryonKeyDown: PropTypes.any,
MenuLibraryonChange: PropTypes.any,
MenuLibraryondelay: PropTypes.any,
FrameonClick: PropTypes.any,
FrameonMouseEnter: PropTypes.any,
FrameonMouseOver: PropTypes.any,
FrameonKeyPress: PropTypes.any,
FrameonDrag: PropTypes.any,
FrameonMouseLeave: PropTypes.any,
FrameonMouseUp: PropTypes.any,
FrameonMouseDown: PropTypes.any,
FrameonKeyDown: PropTypes.any,
FrameonChange: PropTypes.any,
Frameondelay: PropTypes.any,
VectoronClick: PropTypes.any,
VectoronMouseEnter: PropTypes.any,
VectoronMouseOver: PropTypes.any,
VectoronKeyPress: PropTypes.any,
VectoronDrag: PropTypes.any,
VectoronMouseLeave: PropTypes.any,
VectoronMouseUp: PropTypes.any,
VectoronMouseDown: PropTypes.any,
VectoronKeyDown: PropTypes.any,
VectoronChange: PropTypes.any,
Vectorondelay: PropTypes.any,
MenuCardTwoonClick: PropTypes.any,
MenuCardTwoonMouseEnter: PropTypes.any,
MenuCardTwoonMouseOver: PropTypes.any,
MenuCardTwoonKeyPress: PropTypes.any,
MenuCardTwoonDrag: PropTypes.any,
MenuCardTwoonMouseLeave: PropTypes.any,
MenuCardTwoonMouseUp: PropTypes.any,
MenuCardTwoonMouseDown: PropTypes.any,
MenuCardTwoonKeyDown: PropTypes.any,
MenuCardTwoonChange: PropTypes.any,
MenuCardTwoondelay: PropTypes.any,
MenuCardTwoDataonClick: PropTypes.any,
MenuCardTwoDataonMouseEnter: PropTypes.any,
MenuCardTwoDataonMouseOver: PropTypes.any,
MenuCardTwoDataonKeyPress: PropTypes.any,
MenuCardTwoDataonDrag: PropTypes.any,
MenuCardTwoDataonMouseLeave: PropTypes.any,
MenuCardTwoDataonMouseUp: PropTypes.any,
MenuCardTwoDataonMouseDown: PropTypes.any,
MenuCardTwoDataonKeyDown: PropTypes.any,
MenuCardTwoDataonChange: PropTypes.any,
MenuCardTwoDataondelay: PropTypes.any,
SecundaryButtonContonClick: PropTypes.any,
SecundaryButtonContonMouseEnter: PropTypes.any,
SecundaryButtonContonMouseOver: PropTypes.any,
SecundaryButtonContonKeyPress: PropTypes.any,
SecundaryButtonContonDrag: PropTypes.any,
SecundaryButtonContonMouseLeave: PropTypes.any,
SecundaryButtonContonMouseUp: PropTypes.any,
SecundaryButtonContonMouseDown: PropTypes.any,
SecundaryButtonContonKeyDown: PropTypes.any,
SecundaryButtonContonChange: PropTypes.any,
SecundaryButtonContondelay: PropTypes.any,
MenuCardThreeonClick: PropTypes.any,
MenuCardThreeonMouseEnter: PropTypes.any,
MenuCardThreeonMouseOver: PropTypes.any,
MenuCardThreeonKeyPress: PropTypes.any,
MenuCardThreeonDrag: PropTypes.any,
MenuCardThreeonMouseLeave: PropTypes.any,
MenuCardThreeonMouseUp: PropTypes.any,
MenuCardThreeonMouseDown: PropTypes.any,
MenuCardThreeonKeyDown: PropTypes.any,
MenuCardThreeonChange: PropTypes.any,
MenuCardThreeondelay: PropTypes.any,
MenuCardThreeDataonClick: PropTypes.any,
MenuCardThreeDataonMouseEnter: PropTypes.any,
MenuCardThreeDataonMouseOver: PropTypes.any,
MenuCardThreeDataonKeyPress: PropTypes.any,
MenuCardThreeDataonDrag: PropTypes.any,
MenuCardThreeDataonMouseLeave: PropTypes.any,
MenuCardThreeDataonMouseUp: PropTypes.any,
MenuCardThreeDataonMouseDown: PropTypes.any,
MenuCardThreeDataonKeyDown: PropTypes.any,
MenuCardThreeDataonChange: PropTypes.any,
MenuCardThreeDataondelay: PropTypes.any,
MenuLegacyButtononClick: PropTypes.any,
MenuLegacyButtononMouseEnter: PropTypes.any,
MenuLegacyButtononMouseOver: PropTypes.any,
MenuLegacyButtononKeyPress: PropTypes.any,
MenuLegacyButtononDrag: PropTypes.any,
MenuLegacyButtononMouseLeave: PropTypes.any,
MenuLegacyButtononMouseUp: PropTypes.any,
MenuLegacyButtononMouseDown: PropTypes.any,
MenuLegacyButtononKeyDown: PropTypes.any,
MenuLegacyButtononChange: PropTypes.any,
MenuLegacyButtonondelay: PropTypes.any,
MenuLegacyonClick: PropTypes.any,
MenuLegacyonMouseEnter: PropTypes.any,
MenuLegacyonMouseOver: PropTypes.any,
MenuLegacyonKeyPress: PropTypes.any,
MenuLegacyonDrag: PropTypes.any,
MenuLegacyonMouseLeave: PropTypes.any,
MenuLegacyonMouseUp: PropTypes.any,
MenuLegacyonMouseDown: PropTypes.any,
MenuLegacyonKeyDown: PropTypes.any,
MenuLegacyonChange: PropTypes.any,
MenuLegacyondelay: PropTypes.any,
MenuLegacyOneonClick: PropTypes.any,
MenuLegacyOneonMouseEnter: PropTypes.any,
MenuLegacyOneonMouseOver: PropTypes.any,
MenuLegacyOneonKeyPress: PropTypes.any,
MenuLegacyOneonDrag: PropTypes.any,
MenuLegacyOneonMouseLeave: PropTypes.any,
MenuLegacyOneonMouseUp: PropTypes.any,
MenuLegacyOneonMouseDown: PropTypes.any,
MenuLegacyOneonKeyDown: PropTypes.any,
MenuLegacyOneonChange: PropTypes.any,
MenuLegacyOneondelay: PropTypes.any,
MenuLegacyTwoonClick: PropTypes.any,
MenuLegacyTwoonMouseEnter: PropTypes.any,
MenuLegacyTwoonMouseOver: PropTypes.any,
MenuLegacyTwoonKeyPress: PropTypes.any,
MenuLegacyTwoonDrag: PropTypes.any,
MenuLegacyTwoonMouseLeave: PropTypes.any,
MenuLegacyTwoonMouseUp: PropTypes.any,
MenuLegacyTwoonMouseDown: PropTypes.any,
MenuLegacyTwoonKeyDown: PropTypes.any,
MenuLegacyTwoonChange: PropTypes.any,
MenuLegacyTwoondelay: PropTypes.any
}
export default WrapperRoute;