![corebooks_logo](Logo_V2.png)

# BINDER by Corebooks

Binder es el resultado del esfuerzo del equipo de ingenieros de corebooks para acelerar el desarrollo de herramientas web. Binder aprovecha la filosofía de React y la sintaxis de JS para generar Queary containers y estrucutras de código.

## Antecendetes:

Binder no es una nueva tecnología de busca reemplazar a React o sus alternativas mas bien debe considerarse como una herramienta que busca reducir los tiempos para desarrollar aplicaciones usando tecnologías de desarrollo web como:

* HTML/CSS
* GraphQL
* JavaScript
* React

Por lo que es necesario tener al menos un poco de experiencia en el uso de GraphQL, React y JS además es muy recomendable conocer la sintaxis de XML y JSON.

## Instalación

Binder no necesita ser instalado como tal pero requiere tener instalado <a href="https://es.reactjs.org/docs/getting-started.html">React</a> y <a href="https://graphql.org/graphql-js/">GraphQL</a>. 

Una vez configurado su entorno instale las siguiente dependencias usando los comandos mostrados.

**Graphqurl**

```bash
npm install -g graphqurl
```

**Binder**

```bash
git clone git@gitlab.com:ccorebooks/eduardo-cli.git
cd binder-cli
sudo npm i -g .
```

Si ha llegado hasta este punto sin ningún error esta listo para trabajar con binder.