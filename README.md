# Binder template

Pasos para descargar el proyecto e iniciar a configurar

## Dependencies to work

**Graphqurl**

```bash
npm install -g graphqurl
```

**Binder**

```bash
git clone git@gitlab.com:ccorebooks/eduardo-cli.git
cd binder-cli
sudo npm i -g .
```

**Request files to put inside the src folder**

- Config.js
- Env.json
- Environment.js
- EnvironmentMutation.js
- EnvMutation.json

**Request gq project and admin secret for later use**

## Initial project

```bash
git clone git@gitlab.com:templates_books/binder.git
cd binder
npm i
gq https://_PROYECT_.haaaasura.app/v1/graphql -H "X-Hasura-Admin-Secret: _ADMIN_SECRET_" --introspect > schema.graphql
npm run start
```

Si no hay error se puede comenzar a configurar una pantalla

## Binder pantalla

1. Crear un folder llamado Pages (Aquí ira la configuración de todas las pantallas que se vayan a binder)
2. Crear un archivo llamado "Binder.js"
    * query: El query que se utiliza para obtener información de hasura (se hace la consulta primero en hasura y luego se copia aca). Tiene como prefijo el nombre de la carpeta (Pages/FindWorkshopCategory) más la palabra Query
    * config: Toda la configuración para que Binder pueda interpretar las instrucciones
    * SourceFile: Componente que va a ser encapsulado por el archivo generado por binder
    * Name: Igual que el nombre de la carpeta
    * NameOutput: Igual que el nombre de la carpeta
    * PathEnvironment: Archivo que se utiliza para realizar la petición (default)
    * FileNameOutput: Ubicación donde se genera el archivo que encapsula el componente de anima y que hace la petición a hasura
    * Variebles: 
    * Query: La petición de graphql que se hace en hasura
    *  SourceErrorComponent: Componente que se muestra cuando hay un error.
    *  SourceLoadingComponent: Componente que se muestra al esperar a que termine de cargarse la información
    *  Component: Nos permite añadir propiedades al archivo que se genera y encapsula. Se puede revisar lo que regresa props para entender el resultado
        * Attrs: Nos permite añadir propiedades de cualquier tipo
        * Props: Nos permite añadir propiedades pero ya directamente desde el objeto que retorna la consulta de graphql (x_workshop.name para poner una propiedad con el valor de name)
     * Bind: Se añaden los componentes que van a cambiar las propiedades default que entrega la maqueta por un valor dinámica de la base de datos (<MiComponente name="Test"/> al momento de ponerlo en Bind cambiar por <MiComponente name={x_workshop.name}/>

    ```
    const query = `\`query FindWorkshopCategoryQuery($idCategory: Int!, $idState: Int! ) { 
        workshops:x_workshop(where: {x_subcategory_id: {_eq: $idCategory}, x_state: {_eq: $idState}}) {
            _id
            limitShedule
            name:x_name
            price:x_price
        }
    }   
    \``;
    const config = () => ({
      SourceFile: "Components/Search/index.jsx",
      Name:"FindWorkshopCategory",
      NameOutput : "FindWorkshopCategory",
      PathEnvironment: "Environment",
      FileNameOutput:"Pages/FindWorkshopCategory/index.js",
      PathEnvironment: "Environment",
      Variebles:{},
      Query: `graphql${query}`,
      SourceErrorComponent: "Components/ErrorComponent.jsx",
      SourceLoadingComponent: "Components/LoadingComponent",
      Component:{
        "Attrs": {
            isWorshop:'true'
        }
      },
      "Bind":[ ]
    });
    module.exports = config;
    ```

3. Abrir terminal y dirigirte a la carpeta src del proyecto.
4. Ejecutar el comando binder:

    ```
    binder
    ```

5. Dentro de programa ejecuta el comando:
   
    ```
    binder$ binder-app
    ```

6. Si no hay ni un mensaje de error se habrá ejecutado con éxito, se crearon los archivos y carpetas.
7. Se ejecuta el proyecto nuevamente para verificar que se haya exportado de manera correcta:
   
    ```
    npm run start
    ```
